// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import java.util.Iterator;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class StringListBasedComparator.
 */
public abstract class StringListBasedComparator extends LocalizedLineComparator {

	/**
	 * Instantiates a new string list based comparator.
	 *
	 * @param name the name
	 */
	StringListBasedComparator(String name) {
		super(name);
	}

	/**
	 * Compare list.
	 *
	 * @param l1 the l1
	 * @param l2 the l2
	 * @return the int
	 */
	protected int compareList(List<String> l1, List<String> l2) {
		Iterator<String> it1 = l1.iterator();
		Iterator<String> it2 = l2.iterator();

		while (it1.hasNext() && it2.hasNext()) {
			int c = collator.compare(it1.next(), it2.next());
			if (c != 0)
				return c;
		}
		if (it1.hasNext())
			return 1; // it2 is a prefix of it1
		if (it2.hasNext())
			return -1; // it1 is a prefix of it2
		return 0;
	}
}
