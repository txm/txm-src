// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.messages.ConcordanceCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * return the order of the text.
 *
 * @author mdecorde
 */
public class WordPositionComparator extends LocalizedLineComparator {

	/** The Constant NAME. */
	private static final String NAME = ConcordanceCoreMessages.wordCorpusOrder;

	/**
	 * Instantiates a new null comparator.
	 */
	public WordPositionComparator() {
		super(NAME);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Line l1, Line l2) {
		return l1.getKeywordPosition() - l2.getKeywordPosition();
	}
}
