// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.concordance.core.functions.comparators;

import java.util.HashMap;
import java.util.List;

import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.messages.ConcordanceCoreMessages;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;

// TODO: Auto-generated Javadoc
/**
 * Compare two values of keyword of a concordance line
 * 
 * @author mdecorde.
 */
public class LexicographicKeywordComparator extends LocalizedLineComparator {

	/** The Constant NAME. */
	private static final String NAME = ConcordanceCoreMessages.keyword;

	private Concordance conc;

	private List<WordProperty> props;

	/**
	 * Instantiates a new lexicographic keyword comparator.
	 */
	public LexicographicKeywordComparator() {
		super(NAME);
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Line l1, Line l2) {
		int rez = 0;
		int i = 0;
		int j = 0;
		HashMap<Property, List<String>> values1 = l1.getKeywordsAnalysisProperty();
		HashMap<Property, List<String>> values2 = l2.getKeywordsAnalysisProperty();
		this.conc = l1.getConcordance();
		this.props = conc.getKeywordAnalysisProperties();
		while (rez == 0) {
			List<String> vals1 = values1.get(props.get(i));
			List<String> vals2 = values2.get(props.get(i));
			if (vals1.size() <= j && vals2.size() <= j)
				return 0; // end of line 1
			if (vals1.size() <= j)
				return -1; // end of line 1
			if (vals2.size() <= j)
				return 1; // end of line 2
			rez = collator.compare(vals1.get(j), vals2.get(j));
			if (++i >= props.size()) { // iterates over properties before tokens
				i = 0;
				j++;
			}
		}

		return rez;
	}
}
