/*******************************************************************************
 *  Copyright (c) 2007, 2010 IBM Corporation and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.equinox.internal.p2.ui.dialogs;

import org.eclipse.equinox.internal.p2.ui.ProvUIMessages;
import org.eclipse.equinox.internal.p2.ui.model.IUElementListRoot;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.metadata.IUpdateDescriptor;
import org.eclipse.equinox.p2.operations.UpdateOperation;
import org.eclipse.equinox.p2.ui.ProvisioningUI;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.swt.widgets.TreeItem;

public class UpdateWizardPage extends SizeComputingWizardPage {

	public UpdateWizardPage(ProvisioningUI ui, ProvisioningOperationWizard wizard, IUElementListRoot root, UpdateOperation operation) {
		super(ui, wizard, root, operation);
		//System.out.println("Init, root="+Arrays.asList(root));
		setTitle(ProvUIMessages.UpdateWizardPage_Title);
		setDescription(ProvUIMessages.UpdateWizardPage_Description);
	}

	/**
	 * Specialized version for TXM that shows the feature description instead of the product description
	 */
	protected String getIUDescription(IInstallableUnit iu) {

		if (iu != null) {
			//System.out.println("IInstallableUnit.getId() " +iu.getId());
			if (iu.getId().equals("org.txm.rcp.product")) { // TXM product is selected
				//System.out.println("TXM product is selected");
				TreeItem[] treeitems = treeViewer.getTree().getSelection();
				for (TreeItem item : treeitems) { // find it in the Tree selection
					Object data = item.getData();
					//System.out.println("TreeElem: "+data+" class="+data.getClass());
					if (data instanceof org.eclipse.equinox.internal.p2.ui.model.AvailableUpdateElement) {
						org.eclipse.equinox.internal.p2.ui.model.AvailableUpdateElement iunit = (org.eclipse.equinox.internal.p2.ui.model.AvailableUpdateElement) data;
						//System.out.println(" iunit found "+iunit);
						if (iunit.getIU().getId().equals(iu.getId())) { // this is the TXM product
							//System.out.println(" TXM product found in Tree "+iunit);
							//System.out.println("Input: "+treeViewer.getInput());
							IContentProvider cp = treeViewer.getContentProvider();
							//System.out.println("content provider: "+treeViewer.getContentProvider());
							org.eclipse.equinox.internal.p2.ui.viewers.ProvElementContentProvider pecp = (org.eclipse.equinox.internal.p2.ui.viewers.ProvElementContentProvider)cp;
							Object[] children = pecp.getChildren(data);
							//System.out.println("Children: "+Arrays.asList(children));
							for (Object child : children) {
								//System.out.println("Child: "+child);
								//if (child != null) System.out.println("Child class: "+child.getClass());
								if (child instanceof org.eclipse.equinox.internal.p2.ui.model.AvailableIUElement) {
									org.eclipse.equinox.internal.p2.ui.model.AvailableIUElement riu = (org.eclipse.equinox.internal.p2.ui.model.AvailableIUElement) child;
									//System.out.println("riu found "+riu);
									if (riu.getIU().getId().equals("org.txm.rcp.feature.feature.group")) {
										//System.out.println("org.txm.rcp.feature.feature.group description found "+riu.getIU().getUpdateDescriptor().getDescription());
										String desc = riu.getIU().getUpdateDescriptor().getDescription();
										if (desc != null) {
											return desc;
										} else {
											return super.getIUDescription(riu.getIU());
										}
									}
								}
							}


						}
					}
				}
			} else {
				IUpdateDescriptor updateDescriptor = iu.getUpdateDescriptor();
				if (updateDescriptor != null && updateDescriptor.getDescription() != null && updateDescriptor.getDescription().length() > 0) {
					return updateDescriptor.getDescription();
				}
			}
		}
		//String defaultDesc = super.getIUDescription(iu);
		return super.getIUDescription(iu);
	}

	protected String getOperationLabel() {
		return ProvUIMessages.UpdateIUOperationLabel;
	}

	protected String getOperationTaskName() {
		return ProvUIMessages.UpdateIUOperationTask;
	}
}
