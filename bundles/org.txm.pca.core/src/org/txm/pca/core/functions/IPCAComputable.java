package org.txm.pca.core.functions;

import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;

// FIXME: SJ: this interface is no more needed. Need to fix code in org.txm.querycooccurrences.rcp then remove it
@Deprecated
public interface IPCAComputable {

	public PCA toPCA() throws Exception;

	public CQPCorpus getCorpus();

	public Partition getPartition();
}
