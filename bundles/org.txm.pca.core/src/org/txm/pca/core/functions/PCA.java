package org.txm.pca.core.functions;
// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté

// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $
//


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.functions.CA;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Correspondence Analysis of a lexical table.
 *
 * @author mdecorde
 * @author sjacquot
 * @author sloiseau
 * 
 */
public class PCA extends CA {

	/**
	 * Creates a new empty CA.
	 *
	 * @param lexicalTable the lexical table
	 */
	public PCA(LexicalTable lexicalTable) {

		this(null, lexicalTable);
	}

	/**
	 * Creates a new empty CA.
	 *
	 */
	public PCA(String parametersNodePath) {

		this(parametersNodePath, null);
	}

	/**
	 * Creates a new empty CA.
	 *
	 * @param lexicalTable the lexical table
	 */
	public PCA(String parametersNodePath, LexicalTable lexicalTable) {

		super(parametersNodePath, lexicalTable);

		this.needsToSquareOff = true;
	}

	public String[] getExportTXTExtensions() {
		return new String[] {
				"*.txt", "*.Rdata", "*.rds"
		};
	}

	@Override
	public void clean() {

		try {
			if (this.getCA() != null) {
				RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(this.getCA().getSymbol());
			}
		}
		catch (RWorkspaceException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {

		try {
			if (this.getLexicalTable().getNRows() == 0 || this.getLexicalTable().getNColumns() == 0) {
				Log.warning("** Empty Lexical table. Computing aborted.");
				return false;
			}

			// clear cache
			this.colnames = null;
			this.rownames = null;

			try {
				if (this.getCA() != null) {
					RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(this.getCA().getSymbol());
				}
			}
			catch (RWorkspaceException e) {
				e.printStackTrace();
			}

			if (this.useFactoMineR) {
				this.r_ca = new FactoMineRPCA(this.getLexicalTable().getData());
			}
			else {
				//this.r_ca = new org.txm.ca.core.statsengine.r.functions.CA(this.getLexicalTable().getData());
				Log.warning("PCA without FactoMineR is not implemented");
				return false;
			}

			this.r_ca.compute(pRowSupNames, pColSupNames, pNFactorsMemorized, pMirroredDimensions, true);

			return true;
		}
		catch (Exception e) {
			Log.severe(NLS.bind("Failed to compute CA: {0}", e));
			Log.printStackTrace(e);
			return false;
		}

	}

	public void displayOptions(String col, String row, float cex) {

		this.cex = Math.abs(cex);
		if (col.matches(pattern)) {
			this.colfilter = col;
		}
		else {
			Log.severe(NLS.bind("Columns filter malformed follow this pattern {0}.", pattern));
		}

		if (row.matches(pattern)) {
			this.rowfilter = row;
		}
		else {
			Log.severe(NLS.bind("Rows filter malformed follow this pattern {0}.", pattern));
		}
	}

	/**
	 * Gets the CA R object.
	 *
	 * @return the ca
	 */
	public org.txm.ca.core.statsengine.r.functions.ICA getCA() {

		return r_ca;
	}

	public float getCex() {

		return cex;
	}


	/**
	 * 
	 * @return the column filter -> only used by FactoMineR
	 */
	public String getColFilter() {

		return colfilter;
	}

	/**
	 * Gets the column names as array.
	 *
	 * @return the col names
	 * @throws StatException the stat exception
	 */
	public String[] getColNames() throws StatException {

		if (colnames == null) {
			colnames = r_ca.getColNames().asStringsArray();
		}
		return colnames;
	}

	/**
	 * Gets the columns coordinates as array.
	 *
	 * @return the columns coordinates
	 */
	public double[][] getColsCoords() {

		return this.r_ca.getColsCoords();
	}

	/**
	 * Gets the columns distances as array.
	 *
	 * @return the cols dist
	 * @throws StatException the stat exception
	 */
	public double[] getColsDist() throws StatException {

		return this.r_ca.getColDist();
	}

	/**
	 * Get the columns inertia as array.
	 *
	 * @return the cols inertia
	 * @throws StatException the stat exception
	 */
	public double[] getColsInertia() throws StatException {

		return this.r_ca.getColsInertia();
	}

	/**
	 * Gets the cols mass.
	 *
	 * @return the cols mass
	 * @throws StatException the stat exception
	 */
	public double[] getColsMass() throws StatException {

		return this.r_ca.getColsMass();
	}

	/**
	 * Gets the number of columns.
	 * 
	 * @return the number of columns
	 * @throws REXPMismatchException
	 * @throws RWorkspaceException
	 */
	public int getColumnsCount() throws RWorkspaceException, REXPMismatchException {

		return this.r_ca.getColumnsCount();
	}


	/**
	 * Gets the first dimension.
	 * 
	 * @return the first dimension
	 */
	public int getFirstDimension() {

		return firstDimension;
	}

	/**
	 * Gets the lexical table used to compute the CA.
	 *
	 * @return the lexical table
	 */
	public LexicalTable getLexicalTable() {

		return (LexicalTable) this.parent;
	}

	/**
	 * Get the row contributions as array.
	 *
	 * @return the row contrib
	 * @throws StatException
	 */
	public double[][] getRowContrib() throws StatException {

		return r_ca.getRowContrib();
	}

	/**
	 * Get the col contributions as array.
	 *
	 * @return the row contrib
	 * @throws StatException
	 */
	public double[][] getColContrib() throws StatException {

		return r_ca.getColContrib();
	}

	/**
	 * Get the row cos2 as array.
	 *
	 * @return the row cos2
	 * @throws StatException
	 */
	public double[][] getRowCos2() throws StatException {

		return r_ca.getRowCos2();
	}

	/**
	 * Get the col cos2 as array.
	 *
	 * @return the columns cos2 values
	 * @throws StatException
	 */
	public double[][] getColCos2() throws StatException {

		return r_ca.getColCos2();
	}

	public String getRowFilter() {

		return rowfilter;
	}

	/**
	 * Get the row names as array.
	 *
	 * @return the row names
	 * @throws StatException the stat exception
	 */
	public String[] getRowNames() throws Exception {

		if (rownames == null) {
			//rownames = this.getLexicalTable().getRowNames().asStringsArray();
			rownames = r_ca.getRowNames().asStringsArray();
		}
		return rownames;
	}

	/**
	 * Get the rows coordinates as array of arrays
	 *
	 * @return the rows coords
	 */
	public double[][] getRowsCoords() {

		return this.r_ca.getRowsCoords();
	}

	/**
	 * Gets the number of rows.
	 * 
	 * @return the number of rows
	 * @throws REXPMismatchException
	 * @throws RWorkspaceException
	 */
	public int getRowsCount() throws RWorkspaceException, REXPMismatchException {

		return this.r_ca.getRowsCount();
	}

	/**
	 * Get the rows distances as array.
	 *
	 * @return the rows dist
	 * @throws StatException the stat exception
	 */
	public double[] getRowsDist() throws StatException {

		return this.r_ca.getRowDist();
	}

	/**
	 * Get the rows inertia as array.
	 *
	 * @return the rows inertia
	 * @throws StatException the stat exception
	 */
	public double[] getRowsInertia() throws StatException {

		return this.r_ca.getRowsInertia();
	}

	/**
	 * Get the rows mass as array.
	 *
	 * @return the rows mass
	 * @throws StatException the stat exception
	 */
	public double[] getRowsMass() throws StatException {

		return this.r_ca.getRowsMass();
	}

	// /**
	// * step 1.
	// *
	// * @throws StatException the stat exception
	// * @throws CqiClientException the cqi client exception
	// */
	// public boolean stepLexicalTable() throws Exception
	// {
	// table = LexicalTableFactory.getLexicalTable(partition, pAnalysisProperty, fmin);
	// if (table == null) {
	// System.out.println("Error: Lexical table is null.");
	// return false;
	// }
	// partition.addResult(table);
	// table.addResult(this);
	//
	// return true;
	// }

	// /**
	// * step 2.
	// */
	// public boolean stepSortTableLexical()
	// {
	// try {
	// table.getData().filter(vmax, fmin);
	// } catch (Exception e) {
	// System.out.println("Error: "+e.getLocalizedMessage());
	// org.txm.utils.logger.Log.printStackTrace(e);
	// return false;
	// }
	// return true;
	// }

	/**
	 * Gets the second dimension.
	 * 
	 * @return the second dimension
	 */
	public int getSecondDimension() {

		return secondDimension;
	}

	@Override
	public String getSimpleName() {

		try {
			return this.getParent().getProperty().asString() + " (" + this.firstDimension + "," + this.secondDimension + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (Exception e) {
			return "";
		}
	}

	@Override
	public String getName() {

		return this.getParent().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + " (" + this.firstDimension + "," + this.secondDimension + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ ;
	}


	@Override
	public String getDetails() {

		try {
			return this.getSimpleName() + " " + NLS.bind(TXMCoreMessages.common_fMinEqualsP0, this.getLexicalTable().getFMin());
		}
		catch (Exception e) {
			return "";
		}
	}


	@Override
	public String getComputingStartMessage() {

		// from lexical table
		if (this.parent.isVisible()) {
			return TXMCoreMessages.bind(CACoreMessages.info_caOfTheP0LexcialTable, this.getParent().getName());
		}
		// from partition index
		else if (this.getParent().getParent() instanceof PartitionIndex) {
			return TXMCoreMessages.bind(CACoreMessages.info_caOfTheP0PartitionIndex, this.getParent().getParent().getName());

		}
		// from partition
		else if (this.getParent().getPartition() != null) {
			return TXMCoreMessages.bind(CACoreMessages.info_caOfTheP0PartitionCommaP1Property, this.getParent().getPartition().getName(), this.getParent().getProperty().asString());
		}
		// from sub-corpus lexical table
		// FIXME: SJ: this special case is possible when creating Specificities then calling the command on the invisible Lexical Table
		// need to if we need a special log message here
		else {
			return "";
		}


	}


	/**
	 * Get the singular values as array.
	 *
	 * @return the singular values
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double[] getSingularValues() throws StatException, REXPMismatchException {

		return r_ca.getSingularValues();
	}

	/**
	 * Get the singular values infos as List of Lists
	 *
	 * @return the singular values infos
	 * @throws REXPMismatchException
	 */
	public List<List<Object>> getSingularValuesInfos() throws REXPMismatchException {

		List<List<Object>> svtabledata = new ArrayList<>();

		double[] sv = null;
		try {
			sv = getValeursPropres();
		}
		catch (StatException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return svtabledata;
		}

		double sigmaVp;
		try {
			sigmaVp = getValeursPropresSum();
		}
		catch (StatException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return svtabledata;
		}

		double cumul = 0;
		double percent = 0;

		for (int i = 0; i < sv.length; i++) {
			percent = 100 * sv[i] / sigmaVp;
			cumul += percent;

			List<Object> entry = new ArrayList<>(4);
			entry.add(new Integer(i + 1));	// Factor
			entry.add(new Double(sv[i]));	// Singular value
			entry.add(percent);				// Percent
			entry.add(cumul);				// Cumul
			entry.add(percent);				// For bar plot direct drawing in the table

			entry.add(""); // add an empty column to fix a display bug in table in Linux

			svtabledata.add(entry);
		}

		return svtabledata;
	}

	@Override
	public String getRSymbol() {

		return r_ca.getSymbol();
	}

	/**
	 * Gets the sqrt of singular values as array.
	 *
	 * @return the valeurs propres
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double[] getValeursPropres() throws StatException, REXPMismatchException {

		return r_ca.getEigenvalues();
	}

	/**
	 * Gets the 'valeurs propres' sum.
	 *
	 * @return the valeurs propres sum
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	public double getValeursPropresSum() throws StatException, REXPMismatchException {

		double sum = 0;
		for (double d : getValeursPropres()) {
			sum += d;
		}
		return sum;
	}

	/**
	 * Exports CA R data to a file
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	// FIXME: to move in an exporter extension
	@Deprecated
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {

		acquireSemaphore();

		boolean rez = false;
		if (outfile.getName().endsWith(".txt")) {
			rez = r_ca.toTxt(outfile, encoding, colseparator, txtseparator);
		}
		else {
			try {
				rez = RResult.exportRdataOrRDS(this, outfile);
			}
			catch (RWorkspaceException e) {
				e.printStackTrace();
				rez = false;
			}
		}

		releaseSemaphore();

		return rez;
	}


	/**
	 * Checks if the CA uses the FactoMineR R package.
	 * 
	 * @return <code>true</code> if the CA uses the FactoMineR R package otherwise <code>false</code>
	 */
	public boolean useFactoMineR() {

		return this.useFactoMineR;
	}

	@Override
	public boolean canCompute() {

		try {
			return this.getLexicalTable().getNColumns() > 3
					&& this.firstDimension > 0
					&& this.secondDimension > 0
					&& this.firstDimension != this.secondDimension;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean saveParameters() {

		// nothing to do
		return true;
	}

	@Override
	public boolean loadParameters() {

		// nothing to do
		return true;
	}

	/**
	 * @return the showIndividuals
	 */
	public boolean isShowIndividuals() {

		return showIndividuals;
	}

	/**
	 * @param showIndividuals the showIndividuals to set
	 */
	public void setShowIndividuals(boolean showIndividuals) {

		this.showIndividuals = showIndividuals;
	}

	/**
	 * @return the showVariables
	 */
	public boolean isShowVariables() {

		return showVariables;
	}

	/**
	 * @param showVariables the showVariables to set
	 */
	public void setShowVariables(boolean showVariables) {

		this.showVariables = showVariables;
	}

	/**
	 * @return the showPointShapes
	 */
	public boolean isShowPointShapes() {

		return showPointShapes;
	}

	/**
	 * @param showPointShapes the showPointShapes to set
	 */
	public void setShowPointShapes(boolean showPointShapes) {

		this.showPointShapes = showPointShapes;
	}

	/**
	 * @param firstDimension the firstDimension to set
	 */
	public void setFirstDimension(int firstDimension) {

		this.firstDimension = firstDimension;
	}

	/**
	 * @param secondDimension the secondDimension to set
	 */
	public void setSecondDimension(int secondDimension) {

		this.secondDimension = secondDimension;
	}

	@Override
	public String getResultType() {

		return CACoreMessages.RESULT_TYPE;
	}


	/**
	 * Gets the unit property.
	 * 
	 * @return the unitProperty
	 */
	public Property getUnitProperty() {

		return this.getLexicalTable().getProperty();
	}


	@Override
	public LexicalTable getParent() {
		if (parent instanceof LexicalTable) return (LexicalTable) this.parent;
		return null;
	}

	// // FIXME: SJ: fix to save the eigenvalues when the CA is saved
	// // FIXME: SJ: temporary fix to save the Eigenvalues. This method should be removed when the CAEditor will be managed in a better way
	// @Override
	// public void setUserPersistable(boolean userPersistable) {
	// super.setUserPersistable(userPersistable); // can not call super method, lead to infinite loop
	//
	// // deep persistence
	// for (TXMResult child : getChildren()) {
	// child.setUserPersistable(userPersistable);
	// }
	// }


}
