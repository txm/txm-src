// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.pca.core.functions;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.ca.core.statsengine.r.functions.ICA;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.data.QuantitativeDataStructureImpl;
import org.txm.statsengine.r.core.data.VectorImpl;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

/**
 * Access to the R implementation of the Correspondance Analysis of the FactoMineR library
 *
 * @author mdecorde
 */
public class FactoMineRPCA implements ICA {

	/** The singular values. */
	private double[] eigenvalues = null;

	/** The singular values2. */
	private double[] singularValues = null;

	/** The rowmass. */
	private double[] rowmass = null;

	/** The rowdist. */
	private double[] rowdist = null;

	/** The rowinertia. */
	private double[] rowinertia = null;

	/** The colmass. */
	private double[] colmass = null;

	/** The coldist. */
	private double[] coldist = null;

	/** The colinertia. */
	private double[] colinertia = null;

	/** The rowcos2. */
	private double[][] rowcos2 = null;

	/** The rowcontrib. */
	private double[][] rowcontrib = null;

	/** The colcos2. */
	private double[][] colcos2 = null;

	/** The colcontrib. */
	private double[][] colcontrib = null;

	/** The rw. */
	private final RWorkspace rw = RWorkspace.getRWorkspaceInstance();

	/** The symbol. */
	private final String symbol = QuantitativeDataStructureImpl.createSymbole(FactoMineRPCA.class);

	/** The rowscoords. */
	private double[][] rowscoords;

	/** The colscoords. */
	private double[][] colscoords;

	private ILexicalTable table;

	/**
	 * Create a correspondence analysis on the given table. The correspondence analysis is computed as soon as the object is created.
	 *
	 * @param table the table
	 * @throws StatException the stat exception
	 */
	public FactoMineRPCA(ILexicalTable table) throws StatException {

		loadLibrary();
		this.table = table;
	}

	@Override
	public void compute(List<String> pRowSupNames, List<String> pColSupNames, int nCoordsSaved, List<Integer> mirroredDimensions, boolean doPCA) throws StatException {

		// reset cache
		rowscoords = null;
		colscoords = null;
		eigenvalues = null;
		singularValues = null;
		rowmass = null;
		rowdist = null;
		rowinertia = null;
		colmass = null;
		coldist = null;
		colinertia = null;
		rowcos2 = null;
		rowcontrib = null;
		colcos2 = null;
		colcontrib = null;

		if (!rw.containsVariable(table.getSymbol())) {
			throw new StatException(CACoreMessages.error_lexicalTableNotFoundInWorkspace);
		}
		try {
			// rw.callFunctionAndAffect("FactoMineR:CA", new String[] { table.getSymbol() }, symbol); //$NON-NLS-1$
			// rw.eval(symbol+" <- FactoMineR::CA("+table.getSymbol()+", graph=FALSE)"); //$NON-NLS-1$ //$NON-NLS-2$
			StringBuilder script = new StringBuilder();
			script.append(symbol + " <- FactoMineR::PCA(" + table.getSymbol() + "[,colSums(" + table.getSymbol() + ") != 0]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			if (nCoordsSaved >= 3) {
				script.append(", ncp=" + nCoordsSaved);
			}

			if (pRowSupNames != null && pRowSupNames.size() > 0) {
				script.append(",row.sup=Filter(Negate(is.null), which(rownames(" + table.getSymbol() + ") == c(\"" + StringUtils.join(pRowSupNames, "\", \"") + "\")))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				//which(rownames(children) == c("money", "fear"))
			}

			if (pColSupNames != null && pColSupNames.size() > 0) {
				script.append(",col.sup=Filter(Negate(is.null), which(colnames(" + table.getSymbol() + ") == c(\"" + StringUtils.join(pColSupNames, "\", \"") + "\")))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			script.append(", graph=FALSE)"); //$NON-NLS-1$
			rw.eval(script.toString()); //$NON-NLS-1$ //$NON-NLS-2$

			if (mirroredDimensions != null) {
				for (int d : mirroredDimensions) {
					rw.voidEval(symbol + "$col$coord[," + d + "] = -" + symbol + "$col$coord[," + d + "]"); // ca$row$coord[,1] = -ca$row$coord[,1]
					rw.voidEval(symbol + "$row$coord[," + d + "] = -" + symbol + "$row$coord[," + d + "]");
				}
			}

		}
		catch (RWorkspaceException e) {
			throw new StatException(NLS.bind(CACoreMessages.error_errorWhileComputingCAP0, e.getMessage()), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getColContrib()
	 */
	@Override
	public double[][] getColContrib() throws StatException {
		if (colcontrib == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "col$contrib"); //$NON-NLS-1$
				colcontrib = sv.asDoubleMatrix();
				// Arrays.sort(rowdist);
			}
			catch (RWorkspaceException e) {
				throw new StatException(NLS.bind(CACoreMessages.error_failedToGetContributionColumnP0, e.getMessage()), e);
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return colcontrib;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getColCos2()
	 */
	@Override
	public double[][] getColCos2() throws StatException {
		if (colcos2 == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "col$cos2"); //$NON-NLS-1$
				colcos2 = sv.asDoubleMatrix();
				// Arrays.sort(rowdist);
			}
			catch (RWorkspaceException e) {
				throw new StatException(NLS.bind(CACoreMessages.error_failedToGetCos2ColumnP0, e.getMessage()), e);
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return colcos2;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the col dist
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getColDist() throws StatException {
		if (coldist == null) {
			double[][] colcoords = getColsCoords();
			coldist = new double[colcoords.length];
			for (int i = 0; i < colcoords.length; i++) {
				for (int c = 0; c < colcoords[i].length; c++) {
					coldist[i] += Math.pow(colcoords[i][c], 2);
				}
			}
		}
		return coldist;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getColsCoords()
	 */
	@Override
	public double[][] getColsCoords() {
		if (colscoords == null) {
			try {
				REXP exp = rw.extractItemFromListByName(symbol, "col$coord"); //$NON-NLS-1$
				colscoords = exp.asDoubleMatrix();

				// System.out.println("Avant:");
				// for (int i = 0; i < colscoords.length; i++)
				// {
				// for (int c = 0; c < colscoords[i].length; c++)
				// {
				// System.out.println(" "+colscoords[i][c]);
				// }
				// System.out.println();
				// }

				// multiply coords by sv
				// this.getSingularValues();
				// for (int i = 0; i < colscoords.length; i++)
				// for (int c = 0; c < colscoords[i].length; c++)
				// colscoords[i][c] = colscoords[i][c] * singularValues[c];
				//
				// System.out.println("Apres:");
				// for (int i = 0; i < colscoords.length; i++)
				// {
				// for (int c = 0; c < colscoords[i].length; c++)
				// {
				// System.out.println(" "+colscoords[i][c]);
				// }
				// System.out.println();
				// }

			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return colscoords;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the cols inertia
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getColsInertia() throws StatException {
		if (colinertia == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "col$contrib"); //$NON-NLS-1$
				colinertia = RWorkspace.toDouble(sv);
				// Arrays.sort(colinertia);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return colinertia;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the cols mass
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getColsMass() throws StatException {
		if (colmass == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "call$marge.col"); //$NON-NLS-1$
				colmass = RWorkspace.toDouble(sv);
				// Arrays.sort(colmass);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return colmass;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getRowContrib()
	 */
	@Override
	public double[][] getRowContrib() throws StatException {
		// System.out.println("FACTO: getRowContrib");
		if (rowcontrib == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "row$contrib"); //$NON-NLS-1$
				rowcontrib = sv.asDoubleMatrix();
				for (int i = 0; i < rowcontrib.length; i++)
					for (int j = 0; j < rowcontrib[i].length; j++)
						rowcontrib[i][j] = rowcontrib[i][j] / 100.0;
				// Arrays.sort(rowdist);
			}
			catch (RWorkspaceException e) {
				throw new StatException(NLS.bind(CACoreMessages.error_failedToGetConstributionRowP0, e.getMessage()), e);
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return rowcontrib;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getRowCos2()
	 */
	@Override
	public double[][] getRowCos2() throws StatException {
		if (rowcos2 == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "row$cos2"); //$NON-NLS-1$
				rowcos2 = sv.asDoubleMatrix();
				// Arrays.sort(rowdist);
			}
			catch (RWorkspaceException e) {
				throw new StatException(NLS.bind(CACoreMessages.error_failedToGetCos2RowP0, e.getMessage()), e);
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return rowcos2;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the row dist
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getRowDist() throws StatException {
		if (rowdist == null) {
			double[][] rowcoords = getRowsCoords();
			rowdist = new double[rowcoords.length];
			for (int i = 0; i < rowcoords.length; i++) {
				for (int c = 0; c < rowcoords[i].length; c++) {
					rowdist[i] += Math.pow(rowcoords[i][c], 2);
				}
			}
		}
		return rowdist;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getRowsCoords()
	 */
	@Override
	public double[][] getRowsCoords() {
		if (rowscoords == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "row$coord"); //$NON-NLS-1$
				rowscoords = sv.asDoubleMatrix();

				// this.getSingularValues();
				// //System.out.println("Apres:");
				// for (int i = 0; i < rowscoords.length; i++)
				// {
				// for (int c = 0; c < rowscoords[i].length; c++)
				// {
				// rowscoords[i][c] = rowscoords[i][c] * singularValues[c];
				// // System.out.println(" "+rowscoords[i][c]);
				// }
				// //System.out.println();
				// }

			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return rowscoords;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the rows inertia
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getRowsInertia() throws StatException {
		if (rowinertia == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "row$contrib"); //$NON-NLS-1$
				rowinertia = RWorkspace.toDouble(sv);
				// Arrays.sort(rowinertia);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return rowinertia;
	}

	/**
	 * Get the rows mass.
	 *
	 * @return the rows mass
	 * @throws StatException the stat exception
	 */
	@Override
	public double[] getRowsMass() throws StatException {
		if (rowmass == null) {
			try {
				REXP sv = rw.extractItemFromListByName(symbol, "call$marge.row"); //$NON-NLS-1$
				rowmass = RWorkspace.toDouble(sv);
				// Arrays.sort(rowmass);
			}
			catch (RWorkspaceException e) {
				throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			}
		}
		return rowmass;
	}

	/**
	 * Get the singular values.
	 *
	 * @return the singular values
	 * @throws StatException the stat exception
	 * @throws REXPMismatchException
	 */
	@Override
	public double[] getSingularValues() throws StatException, REXPMismatchException {
		if (singularValues == null) {
			getEigenvalues();
			singularValues = new double[eigenvalues.length];
			for (int i = 0; i < eigenvalues.length; i++) {
				singularValues[i] = Math.pow(eigenvalues[i], 0.5);
			}
		}
		return singularValues;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#getSymbol()
	 */
	@Override
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Gets the valeurs propres.
	 *
	 * @return the singular values pow 2
	 * @throws RWorkspaceException
	 * @throws REXPMismatchException
	 */
	@Override
	public double[] getEigenvalues() throws RWorkspaceException, REXPMismatchException {
		if (eigenvalues == null) {
			// try {
			REXP sv = rw.eval(symbol + "$eig[,1]"); //$NON-NLS-1$
			eigenvalues = RWorkspace.toDouble(sv);
			// remove last singular value
			if (eigenvalues.length == this.getColumnsCount()) {
				eigenvalues = Arrays.copyOfRange(eigenvalues, 0, eigenvalues.length - 1);
			}
			// Arrays.sort(singularValues);
			// }
			// catch (Exception|StatException e) {
			// throw new StatException(CACoreMessages.bind(CACoreMessages.error_unableToExtractSingularValuesP0, e.getMessage()), e);
			// }
		}
		return eigenvalues;

	}

	/**
	 * Gets the valeurs propres.
	 *
	 * @return the singular values pow 2
	 * @throws StatException the stat exception
	 */
	@Override
	public String getEigenvaluesRSymbol() throws StatException {
		return symbol + "$eig[,1]"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.engine.r.function.ICA#loadLibrary()
	 */
	@Override
	public boolean loadLibrary() {
		try {
			rw.voidEval("library(FactoMineR)"); //$NON-NLS-1$
		}
		catch (RWorkspaceException e) {
			System.out.println(NLS.bind(CACoreMessages.error_canNotLoadCALibraryP0, e));
			return false;
		}
		return true;
	}

	/**
	 * Draw in the given file a plot of the factorial map.
	 *
	 * @param file the file
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 */
	@Override
	public boolean toTxt(File file, String encoding, String colseparator, String txtseparator) {
		try {
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("sink(file=\"" + file.getAbsolutePath().replace("\\", "\\\\") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			rw.eval("print(" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$eig)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$col)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$col$coord)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$col$cos2)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$col$contrib)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$row)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$row$coord)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$row$cos2)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$row$contrib)"); //$NON-NLS-1$ //$NON-NLS-2$
			// rw.eval("print("+symbol+"$call)"); //$NON-NLS-1$ //$NON-NLS-2$
			// rw.eval("print("+symbol+"$call$marge.col)"); //$NON-NLS-1$ //$NON-NLS-2$
			// rw.eval("print("+symbol+"$call$marge.row)"); //$NON-NLS-1$ //$NON-NLS-2$
			rw.eval("sink()"); //$NON-NLS-1$
		}
		catch (StatException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	@Override
	public int getRowsCount() throws RWorkspaceException, REXPMismatchException {
		return rw.eval("length(" + this.symbol + "$call$marge.row)").asInteger(); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public int getColumnsCount() throws RWorkspaceException, REXPMismatchException {
		return rw.eval("length(" + this.symbol + "$call$marge.col)").asInteger(); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public Vector getColNames() throws RWorkspaceException {
		return new VectorImpl("names(" + this.symbol + "$call$marge.col)"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public Vector getRowNames() throws RWorkspaceException {
		return new VectorImpl("names(" + this.symbol + "$call$marge.row)"); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
