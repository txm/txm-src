package org.txm.specificities.chart.callback;

import java.awt.AWTEvent;
import java.awt.event.MouseEvent;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.txm.chartsengine.rcp.events.EventCallBack;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.handlers.ComputeConcordance;
import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.specificities.core.functions.SpecificitiesSelection;
import org.txm.specificities.rcp.editors.SpecificitiesSelectionEditor;


public class SpecificitiesSelectionChartCallback extends EventCallBack<SpecificitiesSelectionEditor> {

	public SpecificitiesSelectionChartCallback() {
		// TODO Auto-generated constructor stub
	}

	public void processEvent(final Object event, final int eventArea, final Object o) {

		// Need to run this in the SWT UI thread because it's called from the AWT UI thread in TBX charts engine layer

		chartEditor.getComposite().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {

				SpecificitiesSelection ss = chartEditor.getResult();
				TXMResult parent3X = ss.getParent().getParent().getParent();
				if (!(parent3X instanceof Partition partition)) {
					return;
				}

				AWTEvent awtEvent = (AWTEvent) event;

				// Mouse event
				if (event instanceof MouseEvent) {

					MouseEvent mouseEvent = (MouseEvent) event;

					// return if it's not the left mouse button
					if (mouseEvent.getButton() != MouseEvent.BUTTON1) {
						return;
					}

					ChartMouseEvent chartEvent = (ChartMouseEvent) o;
					ChartEntity entity = chartEvent.getEntity();
					//System.out.println("entity: "+entity+" area: "+eventArea);
					if (awtEvent.getID() == MouseEvent.MOUSE_CLICKED && mouseEvent.getClickCount() == 2) {

						//TODO replace XYEntity and all JFC stuff with an abstract event api
						if (entity instanceof XYItemEntity xyentity) {
							int item = xyentity.getItem();
							int series = xyentity.getSeriesIndex();
							System.out.println("series: " + series + " item: " + item);
							try {
								SpecificitiesSelection pd = chartEditor.getResult();
								//System.out.println("Open concordance of the "+(item+1)+"th bar of "+pd.getSelectedTypeNames() +" and "+Arrays.toString(pd.getSelectedPartNames()));
								String partName, lineName;
								if (pd.isGroupingByLines()) {

									partName = pd.getSelectedPartNames()[series];
									lineName = pd.getSelectedTypeNames().get(item);
								}
								else {
									partName = pd.getSelectedPartNames()[item];
									lineName = pd.getSelectedTypeNames().get(series);
								}

								Part part = partition.getPartForName(partName);

								Concordance concordance = part.getFirstChild(Concordance.class);

								if (concordance == null) {
									concordance = new Concordance(part);
									System.out.println("NEW " + concordance);
								}
								else {
									System.out.println("REUSING " + concordance);
								}
								concordance.setQuery(new CQLQuery(lineName));

								ConcordanceEditor editor = ComputeConcordance.openEditor(concordance);
								if (editor != null) {
									editor.updateEditorFromResult(true);
									editor.compute(true);
								}
							}
							catch (Exception e) {
								e.printStackTrace();
							}
						}

					}
				}

			}
		});
	}

}
