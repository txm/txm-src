// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.specificities.rcp.preferences;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.rcp.jface.DoubleFieldEditor;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;
import org.txm.specificities.rcp.adapters.SpecificitiesAdapterFactory;
import org.txm.specificities.rcp.messages.SpecificitiesUIMessages;

/**
 * Specificities preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SpecificitiesPreferencePage extends TXMPreferencePage {



	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {

		StringFieldEditor field = new StringFieldEditor(SpecificitiesPreferences.FORMAT, SpecificitiesUIMessages.scoresFormat, this.getFieldEditorParent());
		field.setToolTipText(SpecificitiesUIMessages.scoresFormatDefaultIsPercent1f);
		this.addField(field);

		IntegerFieldEditor maxScoreField = new IntegerFieldEditor(SpecificitiesPreferences.MAX_SCORE, SpecificitiesUIMessages.maximumScore, this.getFieldEditorParent());
		maxScoreField.setToolTipText(SpecificitiesUIMessages.conventionalValueOfBanality);
		maxScoreField.setTextLimit(4);
		maxScoreField.setValidRange(10, 1000);
		this.addField(maxScoreField);

		// Charts rendering
		Composite chartsTab = SWTChartsComponentsProvider.createChartsRenderingPreferencesTabFolderComposite(this.getFieldEditorParent());

		DoubleFieldEditor dfe = new DoubleFieldEditor(SpecificitiesPreferences.CHART_BANALITY, SpecificitiesUIMessages.banalityThreshold, chartsTab);
		dfe.setToolTipText(SpecificitiesUIMessages.banalityThresholdForCharts);
		this.addField(dfe);

		this.addField(new BooleanFieldEditor(SpecificitiesPreferences.CHART_GROUP_BY_LINES, SpecificitiesUIMessages.groupBarsByLines, chartsTab));
		
		// Create a group for columns management of mixed boolean fields and field/text input fields
		Group booleansGroup = new Group(chartsTab, SWT.NONE);
		booleansGroup.setText(TXMUIMessages.display);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 2;
		booleansGroup.setLayoutData(gridData);
		
		this.addField(new BooleanFieldEditor(SpecificitiesPreferences.CHART_DRAW_LINES, SpecificitiesUIMessages.lines, booleansGroup));
		this.addField(new BooleanFieldEditor(SpecificitiesPreferences.CHART_DRAW_BARS, SpecificitiesUIMessages.bars, booleansGroup));

		// other shared preferences
		SWTChartsComponentsProvider.createChartsRenderingPreferencesFields(this, chartsTab);

	}


	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(SpecificitiesPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(SpecificitiesUIMessages.specificities);
		this.setImageDescriptor(SpecificitiesAdapterFactory.ICON);
	}



}
