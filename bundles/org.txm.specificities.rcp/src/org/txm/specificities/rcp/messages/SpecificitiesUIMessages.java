package org.txm.specificities.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Specificities UI messages.
 * 
 * @author sjacquot
 *
 */
public class SpecificitiesUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.specificities.rcp.messages.messages"; //$NON-NLS-1$

	public static String specificities;

	public static String scoresFormat;
	public static String scoresFormatDefaultIsPercent1f;

	public static String groupBarsByLines;

	public static String drawLines;

	public static String drawBars;
	
	public static String lines;

	public static String bars;

	public static String banality;

	public static String banalityThreshold;

	public static String EditorIsNotASpecificitiesEditorP0;

	public static String ErrorWhileComputingSpecificitiesBarChartP0;

	public static String maximumScore;

	public static String failedToComputeSpecificitiesP0;

	public static String t;

	public static String index;

	public static String maximumScoreToDisplay;

	public static String conventionalValueOfBanality;
	public static String banalityThresholdForCharts;
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, SpecificitiesUIMessages.class);
	}

	private SpecificitiesUIMessages() {
	}
}
