package org.txm.specificities.rcp.handlers;

import java.util.Arrays;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.specificities.core.functions.Specificities;
import org.txm.specificities.core.functions.SpecificitiesSelection;
import org.txm.specificities.rcp.editors.SpecifLine;
import org.txm.specificities.rcp.editors.SpecificitiesEditor;
import org.txm.specificities.rcp.messages.SpecificitiesUIMessages;
import org.txm.utils.logger.Log;



/**
 * Computes a specificities chart from a selection of lines of a specificities result table.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ComputeSpecifictiesSelectionChart extends BaseAbstractHandler {



	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object selection = this.getCorporaViewSelectedObject(event);
		SpecificitiesSelection specificitiesSelection = null;

		// Reopening from existing result
		if (selection instanceof SpecificitiesSelection && CorporaView.isActive()) { // test if the corpora view is active (otherwise, if a SpecificitiesSelection is selected, the creation from table
																					// lines bugs )
			specificitiesSelection = (SpecificitiesSelection) selection;
			open(specificitiesSelection);
		}
		// Creates from SpecificitiesEditor selected table lines
		else {
			try {
				IWorkbenchWindow iww = HandlerUtil.getActiveWorkbenchWindow(event);
				IWorkbenchPage iwp = iww.getActivePage();
				IEditorPart editor = iwp.getActiveEditor();
				if (!(editor instanceof SpecificitiesEditor)) {
					Log.warning(NLS.bind(SpecificitiesUIMessages.EditorIsNotASpecificitiesEditorP0, editor.getClass()));
					return null;
				}

				open((SpecificitiesEditor) editor);
			}
			catch (Exception e) {
				Log.warning(NLS.bind(SpecificitiesUIMessages.ErrorWhileComputingSpecificitiesBarChartP0, e.getLocalizedMessage()));
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}

		return null;
	}

	public static void open(SpecificitiesEditor editor) {

		try {

			Specificities specificities = editor.getResult();

			IStructuredSelection tableSelection = (IStructuredSelection) ((SpecificitiesEditor) editor).getTableSelection();
			Object[] tableLines = tableSelection.toArray();

			// no line selected
			if (tableSelection.isEmpty()) {
				return;
			}

			final String[] selectedTypeNames = new String[tableLines.length];
			String[] partNames = specificities.getColumnsNames();
			final double[][] selectedSpecificitiesIndices = new double[tableLines.length][partNames.length];
			for (int i = 0; i < tableLines.length; i++) {
				Object o = tableLines[i];
				SpecifLine line = (SpecifLine) o;
				selectedTypeNames[i] = line.name;
				selectedSpecificitiesIndices[i] = line.specifs;
			}

			SpecificitiesSelection specificitiesSelection = new SpecificitiesSelection(specificities);

			// Set the data to focus on
			specificitiesSelection.setSelectedTypeNames(Arrays.asList(selectedTypeNames));

			open(specificitiesSelection);
		}
		catch (Exception e) {
			Log.warning(NLS.bind(SpecificitiesUIMessages.ErrorWhileComputingSpecificitiesBarChartP0, e.getLocalizedMessage()));
			org.txm.utils.logger.Log.printStackTrace(e);
		}

	}

	public static void open(SpecificitiesSelection specificitiesSelection) {

		ChartEditor.openEditor(specificitiesSelection);
	}

}
