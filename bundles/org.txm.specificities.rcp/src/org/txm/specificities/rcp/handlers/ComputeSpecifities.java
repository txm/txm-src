// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.specificities.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.specificities.core.functions.Specificities;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.specificities.rcp.editors.SpecificitiesEditor;
import org.txm.utils.logger.Log;

/**
 * If the selection is a Partition: opens the dialog for partition,
 * if the selection is a Subcorpus: opens the dialog for subcorpus,
 * then opens the result in the Specificities Editor.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ComputeSpecifities extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		try {
			Object selection = this.getCorporaViewSelectedObject(event);
			Specificities specificities = null;
			LexicalTable lexicalTable = null;

			// Reopening from existing result
			if (selection instanceof Specificities) {
				specificities = (Specificities) selection;
				// lexicalTable = (LexicalTable) ((Specificities) selection).getParent();
			}
			// Creating
			else {

				// creating from Partition
				if (selection instanceof Partition) {
					lexicalTable = new LexicalTable((Partition) selection);
				}
				// creating from Subcorpus
				else if (selection instanceof Subcorpus) {
					lexicalTable = new LexicalTable((Subcorpus) selection);
				}
				// creating from Partition index
				else if (selection instanceof PartitionIndex) {
					lexicalTable = new LexicalTable((PartitionIndex) selection);
				}
				// creating from Lexical Table
				else if (selection instanceof LexicalTable) {
					lexicalTable = (LexicalTable) selection;
				}

				if (lexicalTable == null) {
					return super.logCanNotExecuteCommand(selection);
				}
				specificities = new Specificities(lexicalTable);

				if (!(selection instanceof LexicalTable)) {
					// show the lexical table only if the command was called from a lexical table
					lexicalTable.setVisible(false);
					// also change the default fmin/vmax parameters to cover whole range
					lexicalTable.setFMinFilter(1);
					lexicalTable.setVMaxFilter(Integer.MAX_VALUE);
				}
			}

			TXMEditor.openEditor(specificities, SpecificitiesEditor.ID);

		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(SpecificitiesCoreMessages.error_errorWhileComputingSpecificitiesP0P0, e));
			Log.printStackTrace(e);
			return null;
		}
		return null;
	}
}
