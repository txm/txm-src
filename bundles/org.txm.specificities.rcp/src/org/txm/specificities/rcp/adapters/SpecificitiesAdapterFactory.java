// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.specificities.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;
import org.txm.specificities.core.functions.Specificities;
import org.txm.specificities.core.functions.SpecificitiesSelection;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class SpecificitiesAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(SpecificitiesAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(SpecificitiesAdapterFactory.class).getSymbolicName() + "/icons/functions/specificities.png"); //$NON-NLS-1$ //$NON-NLS-2$

	public static final ImageDescriptor ICON_SELECTION = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(SpecificitiesAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(SpecificitiesAdapterFactory.class).getSymbolicName() + "/icons/functions/specificities_selection.png"); //$NON-NLS-1$ //$NON-NLS-2$


	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType)) {
			// Specificities
			if (adaptableObject instanceof Specificities) {
				return new TXMResultAdapter() {

					@Override
					public ImageDescriptor getImageDescriptor(Object object) {
						return ICON;
					}
				};
			}
			// SpecificitiesSelection
			else if (adaptableObject instanceof SpecificitiesSelection) {
				return new TXMResultAdapter() {

					@Override
					public ImageDescriptor getImageDescriptor(Object object) {
						return ICON_SELECTION;
					}
				};
			}
		}
		return null;
	}


}
