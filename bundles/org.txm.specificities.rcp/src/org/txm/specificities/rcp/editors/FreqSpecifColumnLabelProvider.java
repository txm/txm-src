// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.specificities.rcp.editors;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;

/**
 * The Class SpecificitiesColumnLabelProvider
 * 
 * create the specificites column label providers
 */
class FreqSpecifColumnLabelProvider extends ColumnLabelProvider {

	SpecificitiesEditor editor;

	int columnIndex;

	boolean score = false;

	private String format;

	/**
	 * Instantiates a new specificites table label provider.
	 *
	 * @param editor the specif editor
	 */
	public FreqSpecifColumnLabelProvider(SpecificitiesEditor editor, int columnIndex, boolean score) {
		// this.partindexes = partindexes;
		// showPValues = "true".equals(Toolbox.getParam(SpecificitiesPreferencePage.SPECIF_PVALUES)); //$NON-NLS-1$
		this.editor = editor;
		this.columnIndex = columnIndex;
		this.score = score;
		this.format = SpecificitiesPreferences.getInstance().getString(SpecificitiesPreferences.FORMAT);
		if (format == null || format.trim().length() == 0) {
			format = "%,.1f"; //$NON-NLS-1$
		}
	}

	public static final String EMPTY = ""; //$NON-NLS-1$

	@Override
	public String getText(Object element) {

		SpecifLine line = (SpecifLine) element;

		if (score) { // SCORE COLUMN
			double s = line.specifs[columnIndex];
			return String.format(format, s);
		}
		else { // FREQ COLUMN
			return Integer.toString(line.freqs[columnIndex]);
		}
	}
}
