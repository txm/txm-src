// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.specificities.rcp.editors;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.jface.TableViewerSeparatorColumn;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.rcp.swt.widget.ThresholdsGroup;
import org.txm.rcp.swt.widget.structures.PropertiesComboViewer;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.specificities.core.functions.Specificities;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;
import org.txm.specificities.rcp.handlers.ComputeSpecifictiesSelectionChart;
import org.txm.specificities.rcp.messages.SpecificitiesUIMessages;
import org.txm.statsengine.r.core.messages.RCoreMessages;
import org.txm.utils.logger.Log;

/**
 * Displays a table editor with specificities indexes.
 * 
 * @author mdecorde
 * @author sjacquot
 * @author sloiseau
 * 
 */
public class SpecificitiesEditor extends TXMEditor<Specificities> implements TableResultEditor {

	public final static String ID = SpecificitiesEditor.class.getName();

	private TableViewer leftViewer;

	private TableViewer rightViewer;

	/**
	 * Viewer comparator for sorting.
	 */
	protected SpecificitiesLinesViewerComparator viewerComparator;


	TableViewerColumn typeFrequencyColumn;


	/**
	 * Unit property.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTY)
	protected PropertiesComboViewer unitPropertyComboViewer;

	/**
	 * Maximum score.
	 */
	@Parameter(key = SpecificitiesPreferences.MAX_SCORE)
	protected Spinner maxScoreFilter;

	/**
	 * Minimum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MIN)
	protected Spinner fMinSpinner;

	/**
	 * Maximum frequency filtering spinner.
	 */
	@Parameter(key = TXMPreferences.F_MAX)
	protected Spinner fMaxSpinner;

	/**
	 * Maximum number of lines filtering.
	 */
	@Parameter(key = TXMPreferences.V_MAX)
	protected Spinner vMaxSpinner;

	/**
	 * Switch between contrasts
	 */
	@Parameter(key = SpecificitiesPreferences.CONTRAST_SCRIPT)
	protected Combo modeText;

	/**
	 * Switch between filters
	 */
	@Parameter(key = SpecificitiesPreferences.FILTER_SCRIPT)
	protected Combo filterText;

	private TableKeyListener tableKeyListener;

	public static final String EMPTY = "";

	@Override
	public void _createPartControl() {

		try {
			// Main parameters
			GLComposite mainParametersArea = this.getMainParametersComposite();
			mainParametersArea.getLayout().numColumns = 2;

			// parent parameters
			if (!this.getResult().getParent().isVisible()) {
				// unit property
				new Label(mainParametersArea, SWT.NONE).setText(TXMCoreMessages.common_property);

				ArrayList<WordProperty> availableProperties = new ArrayList<>();
				availableProperties.addAll(CQPCorpus.getFirstParentCorpus(this.getResult()).getOrderedProperties());
				availableProperties.addAll(CQPCorpus.getFirstParentCorpus(this.getResult()).getVirtualProperties());
				this.unitPropertyComboViewer = new PropertiesComboViewer(mainParametersArea, this, false, availableProperties, this.getResult().getUnitProperty(), false);
				unitPropertyComboViewer.getCombo().setToolTipText("Structural unit used to build the lexical table");
			}


			// Extended parameters
			Composite extendedParametersArea = this.getExtendedParametersGroup();

			// max score
			TXMParameterSpinner maxScore = new TXMParameterSpinner(extendedParametersArea, this, SpecificitiesUIMessages.maximumScore, SpecificitiesUIMessages.maximumScoreToDisplay);
			this.maxScoreFilter = maxScore.getControl();
			this.maxScoreFilter.setMinimum(0);
			this.maxScoreFilter.setMaximum(100000);

			if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {

				// MD tests: switching contrast methods. Allow to compare the contrast methods
				new Label(extendedParametersArea, SWT.NONE).setText("Contrast");

				modeText = new Combo(extendedParametersArea, SWT.BORDER | SWT.SINGLE); // not yet ready
				modeText.setItems("specif", "relative", "apply(SYMBOL, 2, function(x) x / sum(x) * 100)");
				modeText.addKeyListener(new ComputeKeyListener(this, false));
				modeText.setToolTipText("Compute score lines using a different method."
						+ "\n'specif' the usual specificities scores. 'relative' relative frequencies."
						+ "\nOr use our own method using R code and 'SYMBOL' as the **lexical table** R symbol.");

				// MD tests: filtering options based on the BasicVocabulary and TopSpecificitiesByPart
				new Label(extendedParametersArea, SWT.NONE).setText("Filter");

				filterText = new Combo(extendedParametersArea, SWT.BORDER | SWT.SINGLE); // not yet ready
				filterText.setItems("", "basic_max", "basic_sums", "basic_means", "top_max", "top_sums", "top_means", "SYMBOL[apply(abs(SYMBOL), 1, max) < 2,]");
				filterText.addKeyListener(new ComputeKeyListener(this, false));
				filterText.setToolTipText("Filter score lines after they are computed."
						+ "\n'basic' show lines below a score level. 'top' show lines above a score level."
						+ "\n'*_max' uses the maximum value of the line scores. '*_means' uses the mean value of the line scores. '*_sums' uses the sum of the line scores."
						+ "\nOr use our own filter using R code and 'SYMBOL' as the **specificities** R symbol.");
			}

			// Result area
			GLComposite resultArea = this.getResultArea();
			resultArea.getLayout().numColumns = 2;
			//			SashForm sash = new SashForm(resultArea, SWT.HORIZONTAL);
			//			sash.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));

			// create table viewer
			leftViewer = new TableViewer(resultArea, SWT.VIRTUAL | SWT.MULTI | SWT.FULL_SELECTION);
			rightViewer = new TableViewer(resultArea, SWT.VIRTUAL | SWT.MULTI | SWT.FULL_SELECTION);
			leftViewer.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
			rightViewer.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

			tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(leftViewer, rightViewer);
			tableKeyListener.installSearchGroup(this);
			tableKeyListener.installMenuContributions(this);
			TableUtils.installColumnResizeMouseWheelEvents(leftViewer, rightViewer);
			TableUtils.installTooltipsOnSelectionChangedEvent(leftViewer, rightViewer);
			TableUtils.installVerticalSynchronisation(leftViewer, rightViewer);

			leftViewer.getTable().setLinesVisible(true);
			leftViewer.getTable().setHeaderVisible(true);
			leftViewer.setLabelProvider(new SpecificitiesTableLabelProvider(this));
			leftViewer.setContentProvider(ArrayContentProvider.getInstance());

			rightViewer.getTable().setLinesVisible(true);
			rightViewer.getTable().setHeaderVisible(true);
			rightViewer.setLabelProvider(new SpecificitiesTableLabelProvider(this));
			rightViewer.setContentProvider(ArrayContentProvider.getInstance());

			//			String fontName = getResult().getProject().getFont();
			//			if (fontName != null && fontName.length() > 0) {
			//				Font old = viewer.getTable().getFont();
			//				FontData fD = old.getFontData()[0];
			//				// Font f = new Font(old.getDevice(), corpus.getFont(), fD.getHeight(), fD.getStyle());
			//				Font font = new Font(Display.getCurrent(), fontName, fD.getHeight(), fD.getStyle());
			//				viewer.getTable().setFont(font);
			//			}

			// creates the viewer comparator
			this.viewerComparator = new SpecificitiesLinesViewerComparator(Toolbox.getCollator(this.getResult()));
			leftViewer.setComparator(this.viewerComparator);
			rightViewer.setComparator(this.viewerComparator);

			// create 1st column: Unit
			TableViewerColumn unitColumn = new TableViewerColumn(leftViewer, SWT.NONE);
			unitColumn.getColumn().setText(TableUtils.headersNewLine + TXMCoreMessages.common_units);
			unitColumn.getColumn().setToolTipText(TXMCoreMessages.common_units);
			unitColumn.getColumn().setAlignment(SWT.RIGHT);
			unitColumn.getColumn().setWidth((int) (unitColumn.getColumn().getWidth() * 1.5));
			unitColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					if (element == null) return EMPTY;
					if (element instanceof SpecifLine line) {
						return line.name;
					}
					return EMPTY;
				}
			});
			viewerComparator.addSelectionAdapter(leftViewer, unitColumn.getColumn(), 0);

			// create 2nd column: Frequency
			this.typeFrequencyColumn = new TableViewerColumn(leftViewer, SWT.NONE);
			this.typeFrequencyColumn.getColumn().setAlignment(SWT.RIGHT);
			this.typeFrequencyColumn.getColumn().setResizable(false);
			this.typeFrequencyColumn.getColumn().setText(TXMCoreMessages.common_frequency);
			typeFrequencyColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					if (element == null) return EMPTY;
					if (element instanceof SpecifLine line) {
						return Integer.toString(line.f);
					}
					return EMPTY;
				}
			});
			viewerComparator.addSelectionAdapter(leftViewer, typeFrequencyColumn.getColumn(), 1);

			// add a separator empty column
			TableViewerSeparatorColumn.newSeparator(leftViewer);

			MouseListener doubleclickListener = new MouseListener() {

				@Override
				public void mouseUp(MouseEvent e) {
				}

				@Override
				public void mouseDown(MouseEvent e) {
				}

				@Override
				public void mouseDoubleClick(MouseEvent e) {

					ComputeSpecifictiesSelectionChart.open(SpecificitiesEditor.this);
				}
			};
			leftViewer.getTable().addMouseListener(doubleclickListener);
			rightViewer.getTable().addMouseListener(doubleclickListener);

			// Horizontal bar on second table takes up a little extra space.
			// To keep vertical scroll bars in sink, force table1 to end above
			// horizontal scrollbar
			ScrollBar hBarRight = leftViewer.getTable().getHorizontalBar();

			// Register the context menu
			TXMEditor.initContextMenu(this.leftViewer.getTable(), this.getSite(), this.leftViewer);
			TXMEditor.initContextMenu(this.rightViewer.getTable(), this.getSite(), this.rightViewer);

			// adjust UI to the original sorting
			//leftViewer.getTable().setSortColumn(unitColumn);
			//leftViewer.getTable().setSortDirection(SWT.UP);

			if (!this.getResult().isParentVisible()) {

				ThresholdsGroup thresholdsGroup = new ThresholdsGroup(this.getExtendedParametersGroup(), SWT.NONE, this, true, true);
				this.fMinSpinner = thresholdsGroup.getFMinSpinner();
				this.fMaxSpinner = thresholdsGroup.getFMaxSpinner();
				this.vMaxSpinner = thresholdsGroup.getVMaxSpinner();
				this.vMaxSpinner.setMinimum(2);
				CQPCorpus corpus = getResult().getFirstParent(MainCorpus.class);
				if (corpus != null) {
					this.vMaxSpinner.setMaximum(corpus.getSize());
					this.fMaxSpinner.setMaximum(corpus.getSize());
					this.fMinSpinner.setMaximum(corpus.getSize());
				}
			}
		}
		catch (Exception e) {
			Log.severe(NLS.bind(SpecificitiesUIMessages.failedToComputeSpecificitiesP0, e.getMessage()));
			Log.printStackTrace(e);
		}
	}



	@Override
	public void updateEditorFromResult(boolean update) {
		try {
			leftViewer.getTable().setRedraw(false); // AVOID SLOW DISPLAY
			rightViewer.getTable().setRedraw(false); // AVOID SLOW DISPLAY

			String[] lexicalTableColumnNames = null;
			int[] lexicalTableColumnTotalFrequencies = null;

			try {
				lexicalTableColumnNames = this.getResult().getColumnsNames();
				lexicalTableColumnTotalFrequencies = this.getResult().getColumnsFrequenciesSums();
			}
			catch (Exception e) {
				Log.severe(NLS.bind(RCoreMessages.error_unexpectedErrorInRStatisticsEngineP0, e));
				Log.printStackTrace(e);
				return;
			}

			int len = lexicalTableColumnNames.length;
			if (len == 2 && this.getResult().getParent().getParent() instanceof Subcorpus) {
				len--; // show only the first column specifs since they are the same (modulo signus)
			}

			int MAX_NUMBER_OF_COLUMNS = RCPPreferences.getInstance().getInt(RCPPreferences.MAX_NUMBER_OF_COLUMNS);

			if (!update
			// //&& this.getResult().hasBeenComputedOnce()
			) {

				for (int i = 0; i < 2 * len; i = i + 2) {

					if (rightViewer.getTable().getColumnCount() >= MAX_NUMBER_OF_COLUMNS) {
						Log.warning(NLS.bind("Warning The number of columns exceeds {0} ({1}). the table has been cut. You can, change the maximum in the TXM > User preference page",
								MAX_NUMBER_OF_COLUMNS, 2 * len));
						break;
					}

					// FREQ COLUMN
					TableViewerColumn freqpartTableColumn = new TableViewerColumn(rightViewer, SWT.NONE);
					freqpartTableColumn.setLabelProvider(new FreqSpecifColumnLabelProvider(null, i / 2, false));
					viewerComparator.addSelectionAdapter(rightViewer, freqpartTableColumn.getColumn(), 3 + i);

					// SCORE COLUMN
					TableViewerColumn specifPartColumn = new TableViewerColumn(rightViewer, SWT.NONE);
					specifPartColumn.getColumn().setText(TableUtils.headersNewLine + SpecificitiesUIMessages.index);
					specifPartColumn.getColumn().setAlignment(SWT.RIGHT);
					specifPartColumn.setLabelProvider(new FreqSpecifColumnLabelProvider(null, i / 2, true));
					viewerComparator.addSelectionAdapter(rightViewer, specifPartColumn.getColumn(), 3 + i + 1);
				}

				// add a separator empty column
				TableViewerSeparatorColumn.newSeparator(rightViewer);
			}

			if (!this.getResult().isDirty()) {

				String[] typeNames = this.getResult().getRowNames(); // units
				int[] typeFreq = this.getResult().getFormFrequencies(); // units' total freq
				double[][] specIndex = this.getResult().getSpecificitesIndices(); // units' index for each part
				int[][] specFreqs = this.getResult().getFrequencies();

				SpecifLine[] tableLines = new SpecifLine[typeNames.length];
				for (int j = 0; j < tableLines.length; j++) {
					tableLines[j] = new SpecifLine(typeNames[j], typeFreq[j], specFreqs[j], specIndex[j]);
				}
				leftViewer.setInput(tableLines);
				rightViewer.setInput(tableLines);
			}
			else {
				rightViewer.setInput(null);
				rightViewer.setInput(null);
			}

			// Updating the main frequency table header
			String text = "";
			if (this.getResult().hasBeenComputedOnce()) {
				text += SpecificitiesUIMessages.t + "=" + this.getResult().getFrequenciesSum() + TableUtils.headersNewLine; //$NON-NLS-1$ //$NON-NLS-2$
			}
			text += "F"; //$NON-NLS-1$
			typeFrequencyColumn.getColumn().setText(text);
			typeFrequencyColumn.getColumn().setToolTipText(text);
			//typeFrequencyColumn.pack();

			// Updating the table headers from lexical table values
			for (int i = 0, firstLexicalTableColumnIndex = 3; i < len; i++, firstLexicalTableColumnIndex += 2) {

				if (i + 3 >= MAX_NUMBER_OF_COLUMNS) { // stop the loop
					break;
				}

				if (rightViewer.getTable().getColumnCount() < firstLexicalTableColumnIndex - 3) { // Table has been cut
					break;
				}

				// frequency columns
				TableColumn freqpartTableColumn = rightViewer.getTable().getColumn(firstLexicalTableColumnIndex - 3);
				text = "";
				if (this.getResult().hasBeenComputedOnce()) {
					text += "t=" + lexicalTableColumnTotalFrequencies[i] + TableUtils.headersNewLine; //$NON-NLS-1$
				}
				text += lexicalTableColumnNames[i];
				freqpartTableColumn.setText(text);
				freqpartTableColumn.setToolTipText(freqpartTableColumn.getText());
				freqpartTableColumn.setAlignment(SWT.RIGHT);
			}

			// Pack the columns so the label of the sort column is not truncated due of the sorting order arrow display
			TableUtils.packColumns(leftViewer);
			TableUtils.packColumns(rightViewer);

			getResultArea().layout();

			leftViewer.getTable().setRedraw(true); // AVOID SLOW DISPLAY
			rightViewer.getTable().setRedraw(true); // AVOID SLOW DISPLAY
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
	}

	@Override
	public void _setFocus() {
		this.leftViewer.getTable().setFocus();
	}

	/**
	 * Gets the current selected lines in table.
	 * 
	 * @return
	 */
	public ISelection getTableSelection() {
		return this.leftViewer.getSelection();
	}

	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}

	public TableViewer getTableViewer() {

		return leftViewer;
	}

	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { leftViewer, rightViewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}
}
