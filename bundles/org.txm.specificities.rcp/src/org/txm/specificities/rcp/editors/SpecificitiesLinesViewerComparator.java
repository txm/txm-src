/**
 * 
 */
package org.txm.specificities.rcp.editors;

import java.text.Collator;
import java.util.HashSet;
import java.util.LinkedHashSet;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.rcp.editors.TableLinesViewerComparator;
import org.txm.rcp.editors.TableUtils;

/**
 * Viewer comparator for the lines sorting.
 * 
 * @author sjacquot
 *
 */
public class SpecificitiesLinesViewerComparator extends TableLinesViewerComparator {


	private LinkedHashSet<TableViewer> viewers = new LinkedHashSet<TableViewer>();

	/**
	 * 
	 * @param collator
	 */
	public SpecificitiesLinesViewerComparator(Collator collator) {

		super(collator);
	}


	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {

		SpecifLine line1 = (SpecifLine) e1;
		SpecifLine line2 = (SpecifLine) e2;

		int result = 0;

		switch (this.lastColumnIndex) {
			// type names
			case 0:
				result = this.collator.compare(line1.name, line2.name);
				break;
			// total frequencies
			case 1:
				result = line1.f - line2.f;
				break;
			// single part frequencies or single part scores
			default: // lastColumnIndex > 3 are the freqs and specifs columns
				int dataArrayRowIndex = (this.lastColumnIndex - 3) / 2; // 3 is the index of the column of the first part

				// part score
				if ((this.lastColumnIndex % 2) == 0) {
					result = new Double(line1.specifs[dataArrayRowIndex]).compareTo(new Double(line2.specifs[dataArrayRowIndex]));
				}
				// part frequency
				else {
					result = new Integer(line1.freqs[dataArrayRowIndex]).compareTo(new Integer(line2.freqs[dataArrayRowIndex]));
				}
		}

		// reverse if needed
		if (this.direction == DESCENDING) {
			result = -result;
		}
		return result;
	}

	@Override
	public void addSelectionAdapter(final TableViewer viewer, final TableColumn column, final int index) {

		viewers.add(viewer);

		column.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {


				HashSet<SpecifLine> oldSelection = new HashSet<>(viewer.getStructuredSelection().toList());
				StructuredSelection newSelection = new StructuredSelection(oldSelection);
				HashSet<String> oldSelectionNames = new HashSet<String>();
				for (SpecifLine i : oldSelection) {
					oldSelectionNames.add(i.name);
				}

				// System.out.println("select column="+column.getText());
				boolean partScoreFirstSort = (index != 0 && (index % 2) == 0 && lastColumnIndex != index);

				setColumn(index);

				// force descending order on part score column first sorting
				if (partScoreFirstSort) {
					setDirection(DESCENDING);
				}

				for (TableViewer other : viewers) {
					other.getTable().setSortDirection(getDirection());
					other.getTable().setSortColumn(null);
				}
				viewer.getTable().setSortDirection(getDirection());
				viewer.getTable().setSortColumn(column);

				// long t = System.currentTimeMillis();
				TableUtils.refreshColumns(viewer);
				// System.out.println("T="+(System.currentTimeMillis()-t));

				viewer.getTable().deselectAll();
				if (oldSelectionNames.size() > 0) {
					//SpecifLine[] newLines = (SpecifLine[]) viewer.getInput();
					//					TableItem[] items = viewer.getTable().getItems();
					//					for (int i = 0 ; i < items.length ; i++) {
					//						SpecifLine line = (SpecifLine) items[i].getData();
					//						if (oldSelectionNames.contains(line.name)) {
					//							viewer.getTable().select(i);
					//						}
					//					}

					//					SpecifLine[] newLines = (SpecifLine[]) viewer.getInput();
					//					for (int i = 0 ; i < newLines.length ; i++) {
					//						SpecifLine line = newLines[i];
					//						if (oldSelectionNames.contains(line.name)) {
					//							viewer.getTable().select(i);
					//						}
					//					}

					// FIXME setSelection dont work, is it because the table is virtual ?
					//viewer.setSelection(newSelection);
				}

				for (TableViewer other : viewers) {
					if (!other.equals(viewer)) {
						other.getTable().deselectAll();
						other.refresh();
						//						if (oldSelectionNames.size() > 0) {
						//							SpecifLine[] newLines = (SpecifLine[]) other.getInput();
						//							for (int i = 0 ; i < newLines.length ; i++) {
						//								
						//								if (oldSelectionNames.contains(newLines[i].name)) {
						//									other.getTable().select(i);
						//								}
						//							}
						//						}
					}
				}
			}
		});
	}

}
