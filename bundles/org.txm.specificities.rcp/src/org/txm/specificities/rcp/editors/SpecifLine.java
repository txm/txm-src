package org.txm.specificities.rcp.editors;


public class SpecifLine {

	public String name;

	public int f;

	public int[] freqs;

	public double[] specifs;

	public SpecifLine(String name, int f, int[] freqs, double[] specifs) {
		this.name = name;
		this.f = f;
		this.freqs = freqs;
		this.specifs = specifs;
	}

	@Override
	public String toString() {
		return name + "\t" + f; //$NON-NLS-1$
	}
}
