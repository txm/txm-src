// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.specificities.rcp.editors;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;

/**
 * The Class SpecificitesTableLabelProvider.
 * create the specificites table (as many columns as parts)
 */
class SpecificitiesTableLabelProvider extends LabelProvider implements ITableLabelProvider {

	/** The partindexes. */
	// int[] partindexes;

	private boolean showPValues;

	private String format;

	private SpecificitiesEditor editor;

	private int separatorOffset;

	/**
	 * Instantiates a new specificites table label provider.
	 *
	 * @param editor the specif editor
	 */
	public SpecificitiesTableLabelProvider(SpecificitiesEditor editor) {
		// this.partindexes = partindexes;
		// showPValues = "true".equals(Toolbox.getParam(SpecificitiesPreferencePage.SPECIF_PVALUES)); //$NON-NLS-1$
		this.editor = editor;
		this.format = SpecificitiesPreferences.getInstance().getString(SpecificitiesPreferences.FORMAT);
		if (format == null || format.trim().length() == 0) {
			format = "%,.1f"; //$NON-NLS-1$
		}

		separatorOffset = 2;
		if (editor.getResult().getParent().getParent() instanceof Subcorpus) {
			separatorOffset = 1;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	public static final String EMPTY = ""; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	@Override
	public String getColumnText(Object element, int columnIndex) {

		//if (1 == 1 ) return "tmp:"+element;

		SpecifLine line = (SpecifLine) element;

		//		int[] freqs = (int[]) line[2];// get the freqs
		// line content : {unit, freq, freqs[], scores[]}

		if (columnIndex == 0) {
			return line.name;
		}
		else if (columnIndex == 1) {
			return Integer.toString(line.f);
		}
		else if (columnIndex == 2) { // separator column
			return EMPTY;
		}
		else if (columnIndex == editor.getTableViewer().getTable().getColumnCount() - 1) { // last column
			return EMPTY;
		}
		else if (columnIndex == (line.freqs.length * separatorOffset) + 3) { // separator column
			return EMPTY;
		}
		else {
			columnIndex = columnIndex - 3;// this.partindexes[columnIndex - 3];
			if (columnIndex % 2 == 0) { // FREQ COLUMN
				columnIndex = columnIndex / 2; // because there is 2 col per part
				return Integer.toString(line.freqs[columnIndex]);//String.valueOf(line.freqs[columnIndex]);
			}
			else { // SCORE COLUMN
				columnIndex = columnIndex / 2; // because there is 2 cols per part
				//				Object specif = line[3]; // get the scores
				//				double[] specifs = (double[]) specif;

				double s = line.specifs[columnIndex];
				// if (-2 < s && s < 2) return "";
				return String.format(format, s);
				// if (showPValues) {
				// return String.valueOf();
				// } else {
				// return String.valueOf((int) specifs[columnIndex]);
				// }
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.BaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

}
