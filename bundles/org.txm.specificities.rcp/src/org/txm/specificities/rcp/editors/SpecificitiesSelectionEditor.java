package org.txm.specificities.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.core.results.Parameter;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.widget.FloatSpinner;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.specificities.core.functions.SpecificitiesSelection;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;
import org.txm.specificities.rcp.handlers.ComputeSpecifities;
import org.txm.specificities.rcp.messages.SpecificitiesUIMessages;

/**
 * 
 * Editor of the SpecificitiesSelection chart.
 * 
 * @author sjacquot
 *
 */
public class SpecificitiesSelectionEditor extends ChartEditor<SpecificitiesSelection> {

	/**
	 * Group by lines / transpose matrix.
	 */
	@Parameter(key = SpecificitiesPreferences.CHART_GROUP_BY_LINES, type = Parameter.RENDERING)
	protected ToolItem groupBarsByLines;

	/**
	 * Banality threshold.
	 */
	@Parameter(key = SpecificitiesPreferences.CHART_BANALITY, type = Parameter.RENDERING)
	protected FloatSpinner banalitySpinner;


	@Override
	public void __createPartControl() {

		// Computing listeners
		// FIXME: SJ: temporary disabled to fix a bug: sometimes we can't change values with the spinner arrows when auto-computing 
		//ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, true);
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, false);

		// Group bars by lines
		this.groupBarsByLines = new ToolItem(this.getToolBar(), SWT.CHECK);
		this.groupBarsByLines.setToolTipText(SpecificitiesUIMessages.groupBarsByLines);
		this.groupBarsByLines.setImage(IImageKeys.getImage(ComputeSpecifities.class, "icons/silk_group_bars_by_lines.png")); //$NON-NLS-1$

		// Banality
		Composite extendedParametersArea = this.getExtendedParametersGroup();

		TXMParameterSpinner banality = new TXMParameterSpinner(extendedParametersArea, this, new FloatSpinner(extendedParametersArea, SWT.BORDER), SpecificitiesUIMessages.banality,
				SpecificitiesUIMessages.banalityThreshold, true);
		this.banalitySpinner = (FloatSpinner) banality.getControl();
		this.banalitySpinner.setMaximumAsFloat(200);

		this.groupBarsByLines.addSelectionListener(computeSelectionListener);
		this.banalitySpinner.addSelectionListener(computeSelectionListener);
	}


	@Override
	public void updateEditorFromChart(boolean update) {
		// nothing to do
	}

}
