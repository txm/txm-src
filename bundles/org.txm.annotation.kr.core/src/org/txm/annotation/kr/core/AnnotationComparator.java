package org.txm.annotation.kr.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.StringTokenizer;

public class AnnotationComparator implements Comparator<Annotation> {

	public int compare(Annotation a1, Annotation a2) {
		// comparer e1 et e2

		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$
		// System.out.println("Date 1 : "+a1.getDate() + " => "+dateformat.format(a1.getDate()));
		// System.out.println("Date 2 : "+a2.getDate() + " => "+dateformat.format(a2.getDate()));
		StringTokenizer tokenizer1 = new StringTokenizer(dateformat.format(a1.getDate()), "-"); //$NON-NLS-1$
		StringTokenizer tokenizer2 = new StringTokenizer(dateformat.format(a2.getDate()), "-"); //$NON-NLS-1$

		for (int i = 0; i < 2 && tokenizer1.hasMoreTokens() && tokenizer2.hasMoreTokens(); ++i) {
			String token1 = tokenizer1.nextToken();
			String token2 = tokenizer2.nextToken();
			int valint1 = Integer.parseInt(token1);
			int valint2 = Integer.parseInt(token2);
			// System.out.println(valint1+" | "+valint2);
			if (valint1 < valint2) {
				return -1;
			}
			else {
				if (valint1 > valint2) {
					return 1;
				}
				else {
					// System.out.println("idem ["+i+"]");
				}

			}
		}

		return 0;

	}

	public static void main(String[] args) {
		AnnotationComparator comp = new AnnotationComparator();
		Annotation a1 = new Annotation("truc", "bidule", 3, 10); //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("truc", "bidule", 6, 8); //$NON-NLS-1$ //$NON-NLS-2$
		int ret = comp.compare(a1, a2);
		System.out.println("Le retour est : " + ret); //$NON-NLS-1$
	}
}
