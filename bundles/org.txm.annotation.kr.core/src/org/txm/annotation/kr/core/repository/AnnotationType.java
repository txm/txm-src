package org.txm.annotation.kr.core.repository;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * An annotation typed defined in a named Knowledge repository
 * 
 * @author mdecorde
 *
 */
@Entity
public class AnnotationType implements Serializable {

	private static final long serialVersionUID = -4968506428668515803L;

	@Id
	String id;

	String name;

	String webaccess;

	ValuesSize size = ValuesSize.SMALL;

	String krname;

	AnnotationEffect effect = AnnotationEffect.SEGMENT;

	public enum ValuesSize {
		SMALL, AVERAGE, LARGE
	};

	public AnnotationType() {

	}

	public AnnotationEffect setEffect(AnnotationEffect e) {
		effect = e;
		return e;
	}

	public AnnotationEffect getEffect() {
		return effect;
	}

	public ValuesSize getSize() {
		return size;
	}

	public void setSize(ValuesSize size) {
		this.size = size;
	}

	public void setSize(String size) {
		this.size = ValuesSize.valueOf(size);
	}

	public AnnotationType(String krname, String id, String name) {
		this.krname = krname;
		this.id = id;
		this.name = name;
		// System.out.println("New Annotation Type : "+name+" / "+id);
	}

	public AnnotationType(String krname, String id, String name, AnnotationEffect effect) {
		this.krname = krname;
		this.id = id;
		this.name = name;
		this.effect = effect;
		// System.out.println("New Annotation Type : "+name+" / "+id);
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getKnowledgeRepository() {
		return krname;
	}

	public String toString() {
		return name + " (" + effect + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public void setURL(String webaccess) {
		this.webaccess = webaccess;
	}

	public String getURL() {
		return this.webaccess;
	}

	public String toHumanString() {
		return this.getName() + " (" + this.getId() + "): " + this.webaccess; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public TypedValue getTypedValue(String value) {
		KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(krname);
		return kr.getValue(this, value);
	}

}
