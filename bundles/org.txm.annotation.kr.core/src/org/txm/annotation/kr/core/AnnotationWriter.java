package org.txm.annotation.kr.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.osgi.service.prefs.BackingStoreException;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.annotation.kr.core.repository.AnnotationEffect;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.importer.ValidateXml;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;
import org.txm.utils.zip.Zip;


public class AnnotationWriter {

	boolean debug = false;

	MainCorpus corpus;

	private List<AnnotationType> types;

	private KnowledgeRepository defaultKR;

	public AnnotationWriter(MainCorpus corpus) throws BackingStoreException {

		this.corpus = corpus;
		List<String> krnames = KRAnnotationEngine.getKnowledgeRepositoryNames(corpus);
		if (krnames.size() == 0) {
			Log.severe(NLS.bind(Messages.errorNoKRFoundInP0Corpus, corpus));
			throw new IllegalArgumentException("No kr in " + corpus); //$NON-NLS-1$
		}
		String firtsKRName = krnames.get(0);
		defaultKR = KRAnnotationEngine.getKnowledgeRepository(corpus, firtsKRName);
		if (defaultKR == null) {
			Log.severe(NLS.bind(Messages.errorNoKRP0FoundInP1Corpus, defaultKR, corpus));
			throw new IllegalArgumentException("No kr " + defaultKR + " in " + corpus); //$NON-NLS-1$ //$NON-NLS-2$
		}

		types = new ArrayList<AnnotationType>();
		for (AnnotationType type : defaultKR.getAllAnnotationTypes()) {
			if (type.getEffect().equals(AnnotationEffect.SEGMENT)) {
				types.add(type);
			}
		}
	}

	/**
	 * process a text to build standoff files
	 * 
	 * @param textid
	 * @param currentXMLFile
	 * @param currentXMLStandoffFile
	 * @param xmlStandOffDirectory
	 * @return
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 * @throws InvalidCqpIdException
	 * @throws XMLStreamException
	 */
	protected boolean writeTextAnnotationToSyMoGIH(String textid, File currentXMLFile, File currentXMLStandoffFile, File xmlStandOffDirectory)
			throws IOException, CqiServerError, CqiClientException, InvalidCqpIdException, XMLStreamException {

		Log.info(NLS.bind(Messages.textP0, textid));
		boolean show_debug = Log.getLevel().intValue() < Level.INFO.intValue();

		AnnotationSyMoGIHWriter annotationstdoff = new AnnotationSyMoGIHWriter(textid, currentXMLFile, xmlStandOffDirectory, types, show_debug);

		/// rather test on the new xml standoff files
		if (annotationstdoff.process(currentXMLStandoffFile)) {
			if (ValidateXml.test(currentXMLStandoffFile)) { // TODO ALSO check if annotations are well-written
				return true;
			}
			else {
				Log.warning(NLS.bind(Messages.ErrorTheP0ResultFileIsMalformed, currentXMLStandoffFile));
			}
		}
		else {
			Log.warning(NLS.bind(Messages.ErrorWhileProcessingTheP0FileInStandOffDir, currentXMLStandoffFile));
		}
		return false;

	}

	/**
	 * Writing annotations in standoff files for each text of the corpus
	 * 
	 * @return
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 * @throws InvalidCqpIdException
	 * @throws XMLStreamException
	 */
	public boolean writeAnnotationsInStandoff(File resultZipFile) throws IOException, CqiServerError, CqiClientException, InvalidCqpIdException, XMLStreamException {

		List<String> textsIds = Arrays.asList(corpus.getCorpusTextIdsList());
		Log.info(NLS.bind(Messages.ExportingAnnotationsOfTheP0Texts, StringUtils.join(textsIds, ", "))); //$NON-NLS-2$

		File resultDirectory = new File(Toolbox.getTxmHomePath(), "results/" + corpus.getID() + "_annotations"); //$NON-NLS-1$ //$NON-NLS-2$
		DeleteDir.deleteDirectory(resultDirectory);
		resultDirectory.mkdirs();
		if (!(resultDirectory.exists() && resultDirectory.canWrite())) {
			Log.warning(NLS.bind(Messages.ErrorCouldNotCreateWriteTemporaryDirectoryP0, resultDirectory));
			return false;
		}

		File inputDirectory = corpus.getProjectDirectory();
		File corpusTxmDirectory = new File(inputDirectory, "txm/" + corpus.getID()); //$NON-NLS-1$

		Log.info(NLS.bind(Messages.WritingAnnotationsXMLFilesInP0, resultDirectory));
		for (String textid : textsIds) {
			File currentXMLFile = new File(corpusTxmDirectory, textid + ".xml"); //$NON-NLS-1$
			if (!currentXMLFile.exists()) {
				Log.warning(NLS.bind(Messages.ErrorCannotFoundAXMLTXMFileForTextWithTheP0Id, textid));
				return false;
			}
			File currentXMLStandoffFile = new File(resultDirectory, textid + ".xml"); // To Be Changed ? //$NON-NLS-1$
			if (!writeTextAnnotationToSyMoGIH(textid, currentXMLFile, currentXMLStandoffFile, resultDirectory)) {
				Log.warning(NLS.bind(Messages.ErrorWhileWritingAnnotationsOfTextInP0, currentXMLStandoffFile));
				return false;
			}
		}

		Zip.compress(resultDirectory, resultZipFile, new ConsoleProgressBar(1));
		DeleteDir.deleteDirectory(resultDirectory);

		Log.info(NLS.bind(Messages.AnnotationsSavedInP0, resultZipFile.getAbsolutePath()));
		return resultZipFile.exists();
	}

	/**
	 * 
	 * @param allCorpusAnnotations ordered annotations
	 * @param monitor
	 * @return
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 * @throws InvalidCqpIdException
	 * @throws XMLStreamException
	 * @throws BackingStoreException
	 */
	public boolean writeAnnotations(List<Annotation> allCorpusAnnotations, IProgressMonitor monitor) throws IOException, CqiServerError, CqiClientException, InvalidCqpIdException, XMLStreamException,
			BackingStoreException {
		// MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus(c.getName());
		Log.info(NLS.bind(Messages.savingP0Annotations, allCorpusAnnotations.size()));

		int[] end_limits = corpus.getTextEndLimits();
		int[] start_limits = corpus.getTextStartLimits();
		List<String> textsIds = Arrays.asList(corpus.getCorpusTextIdsList());

		File inputDirectory = corpus.getProjectDirectory();
		File txmDirectory = new File(inputDirectory, "txm/" + corpus.getID()); //$NON-NLS-1$

		ArrayList<Annotation> textAnnotations = new ArrayList<Annotation>();
		HashMap<String, ArrayList<Annotation>> annotationsPerTexts = new HashMap<String, ArrayList<Annotation>>();

		int currentText = 0;
		File currentXMLFile = new File(txmDirectory, textsIds.get(currentText) + ".xml"); //$NON-NLS-1$

		textAnnotations = new ArrayList<Annotation>();
		annotationsPerTexts.put(textsIds.get(currentText), textAnnotations);

		allCorpusAnnotations.sort(new Comparator<Annotation>() {

			@Override
			public int compare(Annotation arg0, Annotation arg1) {
				int r = arg0.getStart() - arg1.getStart();
				if (r == 0) {
					return arg0.getEnd() - arg1.getEnd();
				}
				else {
					return r;
				}
			}
		});

		// group annotations per text ; annotations are now sorted by position
		for (Annotation currentAnnot : allCorpusAnnotations) { // parse all annotations
			// System.out.println(" Annotation: "+currentAnnot);
			int pos = currentAnnot.getPK().getEndPosition();

			while (pos > end_limits[currentText]) { // while pos is not in the currentText.end
				currentText++;
				textAnnotations = new ArrayList<Annotation>();
				annotationsPerTexts.put(textsIds.get(currentText), textAnnotations);
			}

			textAnnotations.add(currentAnnot);
		}

		File tmpXMLTXMDirectory = new File(txmDirectory.getAbsolutePath() + "_tmp"); //$NON-NLS-1$
		DeleteDir.deleteDirectory(tmpXMLTXMDirectory);
		tmpXMLTXMDirectory.mkdirs();
		if (!(tmpXMLTXMDirectory.exists() && tmpXMLTXMDirectory.canWrite())) {
			Log.warning(NLS.bind(Messages.ErrorCouldNotCreateDirectoryP0, tmpXMLTXMDirectory));
			return false;
		}

		File previousXMLTXMDirectory = new File(txmDirectory.getAbsolutePath() + "_previous"); //$NON-NLS-1$
		// DeleteDir.deleteDirectory(tmpXMLTXMDirectory);
		previousXMLTXMDirectory.mkdirs();
		if (!(previousXMLTXMDirectory.exists() && previousXMLTXMDirectory.canWrite())) {
			Log.warning(NLS.bind(Messages.ErrorCouldNotCreateDirectoryP0, previousXMLTXMDirectory));
			return false;
		}

		Log.fine("Annotations grouped per text for " + annotationsPerTexts.size() + " text" + (annotationsPerTexts.size() > 0 ? "s" : "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		Log.fine(" - Writing temporary XML files in: " + tmpXMLTXMDirectory); //$NON-NLS-1$
		Log.fine(" - Copying previous version of XML files in: " + previousXMLTXMDirectory); //$NON-NLS-1$
		// for all annotation PER TEXT, update the XML-TXM files
		currentText = 0;
		ConsoleProgressBar cpb = new ConsoleProgressBar(end_limits.length);
		while (currentText < end_limits.length) { // end limits : 10, 30, 45, 55, 103
			cpb.tick();
			currentXMLFile = new File(txmDirectory, textsIds.get(currentText) + ".xml"); //$NON-NLS-1$
			ArrayList<Annotation> allAnnotations = annotationsPerTexts.get(textsIds.get(currentText));
			if (allAnnotations != null && allAnnotations.size() > 0) {
				ArrayList<Annotation> allSegmentAnnotations = new ArrayList<Annotation>();
				ArrayList<Annotation> allTokenAnnotations = new ArrayList<Annotation>();

				// System.out.println("Using KR="+defaultKR);
				for (Annotation a : allAnnotations) {
					AnnotationType type = defaultKR.getType(a.getType());
					if (type != null) {
						if (AnnotationEffect.SEGMENT.equals(type.getEffect())) {
							allSegmentAnnotations.add(a);
						}
						else if (AnnotationEffect.TOKEN.equals(type.getEffect())) {
							allTokenAnnotations.add(a);
						}
						else {
							Log.fine("Annotation " + a + " with type=" + a.getType() + " not found in default KR=" + defaultKR); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						}
					}
					else {
						Log.warning(NLS.bind(Messages.WarningUnknownTypeP0, a.getType()));
					}
				}

				if (!writeAnnotationsInFile(currentXMLFile, start_limits[currentText],
						allSegmentAnnotations, allTokenAnnotations,
						tmpXMLTXMDirectory, previousXMLTXMDirectory)) {
					Log.severe(NLS.bind(Messages.ErrorWhileWritingAnnotationsOfTextP0, currentXMLFile));
					return false;
				}
				else {
					if (monitor != null) {
						monitor.worked(allSegmentAnnotations.size() + allTokenAnnotations.size());
					}
				}
			}
			currentText++;

		}
		cpb.done();

		return true;
	}

	// writeAnnotationInStandoffFile(currentXMLStandoffFile, allAnnotations, annotator, tmpXMLTXMDirectory, previousXMLTXMDirectory))

	protected boolean writeAnnotationsInFile(File xmlFile, int text_start_position,
			ArrayList<Annotation> segmentAnnotations, ArrayList<Annotation> tokenAnnotations, File tmpXMLTXMDirectory, File previousXMLTXMDirectory) throws CqiClientException, IOException,
			CqiServerError, InvalidCqpIdException, XMLStreamException {

		Log.fine("Writing annotations for text " + xmlFile + " segment annotations=" + segmentAnnotations.size() + " token annotations=" + tokenAnnotations.size()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (debug) System.out.println(segmentAnnotations);
		if (debug) System.out.println(tokenAnnotations);

		boolean show_debug = Log.getLevel().intValue() < Level.INFO.intValue();
		AnnotationInjector annotationInjector = new AnnotationInjector(xmlFile, segmentAnnotations, tokenAnnotations, text_start_position, show_debug);

		File tmpfile = new File(tmpXMLTXMDirectory, xmlFile.getName());
		File previousfile = new File(previousXMLTXMDirectory, xmlFile.getName());

		if (annotationInjector.process(tmpfile)) {
			if (ValidateXml.test(tmpfile)) { // TODO ALSO check if annotations are well-written
				previousfile.delete(); // in case there is one
				if (!previousfile.exists() && xmlFile.renameTo(previousfile)) {
					tmpfile.renameTo(xmlFile);
					return true;
				}
				else {
					Log.severe(NLS.bind(Messages.ErrorCouldNotReplaceXMLTXMFileP0WithP1, xmlFile, tmpfile));
				}
			}
			else {
				Log.severe(NLS.bind(Messages.ErrorResultFileP0IsMalformed, tmpfile));
			}
		}
		else {
			Log.severe(NLS.bind(Messages.ErrorWhileProcessingP0InTempDir, xmlFile));
		}
		return false;
	}
}
