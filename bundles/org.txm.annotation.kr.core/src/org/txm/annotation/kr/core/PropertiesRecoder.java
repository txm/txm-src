package org.txm.annotation.kr.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.xml.stream.XMLStreamException;

import org.txm.importer.StaxIdentityParser;

public class PropertiesRecoder extends StaxIdentityParser {

	public PropertiesRecoder(File xmlFile, HashSet<String> fromProperties, HashSet<String> toProperties) throws IOException, XMLStreamException {
		super(xmlFile);
	}

	boolean startW = false, startAna = false;

	ArrayList<String[]> anaValues = new ArrayList<String[]>();

	String[] currentAnaValues = { "", "", "" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	StringBuilder anaValue = new StringBuilder();

	String EMPTY = ""; //$NON-NLS-1$

	@Override
	public void processStartElement() throws XMLStreamException, IOException {
		if (localname.equals("w")) { //$NON-NLS-1$
			startW = true;
			anaValues.clear();
			super.processStartElement();
		}
		else if (localname.equals("ana")) { //$NON-NLS-1$
			if (startW) {
				startAna = true;
				anaValue.setLength(0);
				currentAnaValues[0] = EMPTY;
				currentAnaValues[1] = EMPTY;
				currentAnaValues[2] = EMPTY;
				for (int i = 0; i < parser.getAttributeCount(); i++) {
					if (parser.getAttributeLocalName(i).equals("resp")) { //$NON-NLS-1$
						currentAnaValues[0] = parser.getAttributeValue(i);
					}
					else if (parser.getAttributeLocalName(i).equals("type")) { //$NON-NLS-1$
						currentAnaValues[1] = parser.getAttributeValue(i);
					}
				}
			}
		}
		else {
			super.processStartElement();
		}
	}

	@Override
	public void processCharacters() throws XMLStreamException {
		if (startAna) {
			anaValue.append(parser.getText());
		}
		else {
			super.processCharacters();
		}
	};

	@Override
	public void processEndElement() throws XMLStreamException {
		if (localname.equals("w")) { //$NON-NLS-1$
			startW = false;

			for (String[] values : anaValues) {
				try {
					writer.writeStartElement("txm:ana"); //$NON-NLS-1$
					writer.writeAttribute("resp", values[0]); //$NON-NLS-1$
					writer.writeAttribute("type", values[1]); //$NON-NLS-1$
					writer.writeCharacters(values[2]);
					writer.writeEndElement();
				}
				catch (XMLStreamException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			super.processEndElement();
		}
		else if (localname.equals("ana")) { //$NON-NLS-1$
			if (startW) {
				currentAnaValues[2] = anaValue.toString();
				anaValues.add(currentAnaValues);
				startAna = false;
			}
		}
		else {
			super.processEndElement();
		}
	}

	public static void main(String args[]) {
		try {
			File xmlFile = new File("/home/mdecorde/TEMP/test.xml"); //$NON-NLS-1$
			File outfile = new File("/home/mdecorde/TEMP/test-o.xml"); //$NON-NLS-1$
			HashSet<String> fromProperties = new HashSet<String>();
			HashSet<String> toProperties = new HashSet<String>();

			PropertiesRecoder recoder = new PropertiesRecoder(xmlFile, fromProperties, toProperties);
			System.out.println("Result: " + recoder.process(outfile)); //$NON-NLS-1$
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
