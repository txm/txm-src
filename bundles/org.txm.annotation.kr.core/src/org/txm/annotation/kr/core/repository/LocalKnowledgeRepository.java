package org.txm.annotation.kr.core.repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;

import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.DatabasePersistenceManager;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.sql.SQLConnection;
import org.txm.utils.logger.Log;

public class LocalKnowledgeRepository extends KnowledgeRepository {

	private File currentDirectory;

	public LocalKnowledgeRepository(String name) {
		super(name);
	}

	public String getAccessType() {
		return DatabasePersistenceManager.ACCESS_FILE;
	}

	@Override
	public boolean buildValues() {

		String path = accessProperties.get(SQLConnection.SQL_ADDRESS);
		if (path == null || path.trim().length() == 0) { // no address, forge one
			path = Toolbox.getTxmHomePath() + "/repositories/" + getName(); //$NON-NLS-1$
			Log.finest("No address given to build values. No values created."); //$NON-NLS-1$
			return true;
		}
		currentDirectory = new File(path);

		if (!currentDirectory.exists()) {
			Log.finest("Values address given does not exists: " + currentDirectory); //$NON-NLS-1$
			return false;
		}
		try {
			for (String type : fieldsTypesProperties.keySet()) {
				File f = new File(currentDirectory, type);
				BufferedReader reader = new BufferedReader(new FileReader(f));
				String line = reader.readLine();
				int i = 0;
				jpaem.getTransaction().begin();
				HashSet<String> values = new HashSet<String>();
				while (line != null) {
					if (line.length() > 0) {
						String split[] = line.split("\t", 2); //$NON-NLS-1$
						TypedValue val = null;
						if (split.length == 2) {
							val = new TypedValue(split[1], split[0], type);
						}
						else if (split.length == 1) {
							val = new TypedValue(split[0], split[0], type);
						}

						if (values.contains(val.getPK().getId())) {
							Log.warning(TXMCoreMessages.bind(Messages.WarningDuplicatedP0ValueInP1OfTheP2KRIgnoringTheValue, val.getId(), type, name)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						}
						else {
							values.add(val.getPK().getId());
							jpaem.persist(val);
						}
					}
					line = reader.readLine();
					i++;
					if ((i % 10000) == 0) {
						jpaem.flush();
						jpaem.clear();
					}
				}
				reader.close();
				jpaem.getTransaction().commit();
			}
			return true;
		}
		catch (Exception e) {
			Log.severe(NLS.bind(Messages.ErrorWhileLoadingTypesFromFileP0, currentDirectory)); //$NON-NLS-1$
			e.printStackTrace();
			return false;
		}
	}

	static final boolean[] FALSES = { false, false };

	@Override
	public boolean[] mustLoginToKnowledgeRepository() {
		return FALSES;

	}

	@Override
	public boolean checkConnection() {
		return true; // no connection :-)
	}


	public boolean addTypeDeclaration(AnnotationType type) {
		// What's the use ? Persistence
		return false;
	}
}
