package org.txm.annotation.kr.core.conversion;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.txm.importer.StaxIdentityParser;
import org.txm.utils.logger.Log;

public class XMLTXMFileRuledConversion extends StaxIdentityParser {

	public boolean debug = false;

	protected LinkedHashMap<Pattern, String> rules = new LinkedHashMap<>();

	protected String elementName = null;

	boolean fixingElement = false; // true when the current element needs to be fixed

	protected String oldType;

	protected String newType;

	public static final String DELETE = "supprimer"; //$NON-NLS-1$

	public static final String COPY = "copier"; //$NON-NLS-1$

	public static final String ABANDON = "abandon"; //$NON-NLS-1$

	HashSet<String> noMatchValues = new HashSet<>();

	/**
	 * 
	 * @param infile the file to read
	 * @param rules the conversion rules
	 * @param elementPath if null, the conversion happens
	 * @param oldType word type or element attribute to read
	 * @param newType word type or element attribute to write
	 * @param mode XMLTXMFileRuledConversion.DELETE or XMLTXMFileRuledConversion.COPY or XMLTXMFileRuledConversion.ABANDON
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public XMLTXMFileRuledConversion(File infile, LinkedHashMap<Pattern, String> rules, String elementPath, String oldType, String newType, String mode) throws IOException, XMLStreamException {
		super(infile);
		this.rules = rules;
		this.oldType = oldType;
		this.newType = newType;

		this.mode = mode;

		if (elementPath != null && elementPath.length() > 0) {
			this.elementName = elementPath;
		}

		if (elementPath == null) { // where fixing txm:words
			if (!this.newType.startsWith("#")) this.newType = "#" + this.newType; //$NON-NLS-1$ //$NON-NLS-2$
			if (!this.oldType.startsWith("#")) this.oldType = "#" + this.oldType; //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	boolean inW = false, inAna = false, inForm;

	LinkedHashMap<String, String> anaValues = new LinkedHashMap<>();

	LinkedHashMap<String, String> anaResps = new LinkedHashMap<>();

	String typeName = null;

	String respName = null;

	String formValue, typeValue = null;

	private Object mode;

	public static class Converter {

		public Converter() {

		}

		public String getValue(XMLStreamReader parser, String localname, String attribute, String value) {
			return value;
		}
	}

	private Converter converter = null;

	@Override
	protected void writeAttributes() throws XMLStreamException {
		if (fixingElement) {
			String newValue = null;
			for (int i = 0; i < parser.getAttributeCount(); i++) {
				String att = parser.getAttributeLocalName(i);
				String value = parser.getAttributeValue(i);
				if (oldType != null && oldType.equals(att)) {
					if (converter != null) {
						if (debug) Log.finer("CALL CONVERTER with attribute " + att); //$NON-NLS-1$
						newValue = converter.getValue(parser, localname, att, value);
					}
					else {
						newValue = getValueIfMatch(value);
					}
				}
				if (!newType.equals(att)) { // don't write the newType, it will be done at the end
					writeAttribute(parser.getAttributePrefix(i), att, value);
				}
			}

			if (oldType == null && converter != null) {
				if (debug) Log.finer("CALL CONVERTER with no attribute set"); //$NON-NLS-1$
				newValue = converter.getValue(parser, localname, null, null);
			}

			if (newValue != null) {
				if (debug) Log.finer("WRITE NEWTYPE: " + newType + "=" + newValue); //$NON-NLS-1$ //$NON-NLS-2$
				writeAttribute(null, newType, newValue);
			}
		}
		else {
			super.writeAttributes();
		}
	}

	@Override
	public void processStartElement() throws XMLStreamException, IOException {


		if (elementName != null) {
			if (this.localname.equals(elementName)) {
				fixingElement = true;
			}

			super.processStartElement();
			fixingElement = false;
		}
		else {

			if (!inW) super.processStartElement(); // don't write W content

			if (localname.equals("w")) { //$NON-NLS-1$
				inW = true;
				anaValues.clear();
				anaResps.clear();

				// initialize the new type to a empty value in case there is transformation rule
				anaValues.put(newType, ""); //$NON-NLS-1$
				anaResps.put(newType, "#txm_recode"); //$NON-NLS-1$
			}
			else if (localname.equals("ana")) { //$NON-NLS-1$
				inAna = true;
				typeName = parser.getAttributeValue(null, "type"); //$NON-NLS-1$
				respName = parser.getAttributeValue(null, "resp"); //$NON-NLS-1$
				anaResps.put(typeName, respName);
				// if (typeName != null) typeName = typeName.substring(1); // remove #
				typeValue = ""; //$NON-NLS-1$
			}
			else if (localname.equals("form")) { //$NON-NLS-1$
				inForm = true;
				formValue = ""; //$NON-NLS-1$
			}
		}
	}

	@Override
	public void processCharacters() throws XMLStreamException {
		if (elementName != null) {
			super.processCharacters();
		}
		else {
			if (inW && inAna) typeValue += parser.getText();
			else if (inW && inForm) formValue += parser.getText();
			else super.processCharacters();
		}
	}

	@Override
	public void processEndElement() throws XMLStreamException {

		if (elementName != null) {
			super.processEndElement();
		}
		else {

			if (localname.equals("w")) { //$NON-NLS-1$
				inW = false;

				// write W content
				try {
					// get the value to test
					String value = null;
					if (oldType.equals("word")) { //$NON-NLS-1$
						value = formValue;
					}
					else {
						value = anaValues.get(oldType);
					}

					if (newType.equals("word")) { // update form property //$NON-NLS-1$
						updateFormValueIfMatch(value);
					}
					else { // update a ana property
						if (value != null) {
							updateAnaValuesIfMatch(value);
						}
					}

					// write the word element
					writer.writeStartElement("txm:form"); //$NON-NLS-1$
					writer.writeCharacters(formValue);
					writer.writeEndElement();

					for (String k : anaValues.keySet()) {
						String resp = anaResps.get(k);
						if (resp == null) resp = "#txm_recode"; //$NON-NLS-1$

						writer.writeStartElement("txm:ana"); //$NON-NLS-1$
						writer.writeAttribute("resp", resp); //$NON-NLS-1$
						writer.writeAttribute("type", k); //$NON-NLS-1$
						writer.writeCharacters(anaValues.get(k));
						writer.writeEndElement();
					}
				}
				catch (XMLStreamException e) {
					e.printStackTrace();
				}
			}
			else if (localname.equals("ana")) { //$NON-NLS-1$
				anaValues.put(typeName, typeValue);
				inAna = false;
			}
			else if (localname.equals("form")) { //$NON-NLS-1$
				inForm = false;
			}

			if (!inW) super.processEndElement(); // don't write W content
		}
	}

	protected String getValueIfMatch(String value) {
		for (Pattern rule : rules.keySet()) {
			if (rule.matcher(value).matches()) {
				return rules.get(rule); // ok stop
			}
		}

		return value;
	}

	protected void updateFormValueIfMatch(String value) {

		if (converter != null) {
			String v = converter.getValue(parser, localname, null, value);
			if (v != null) {
				formValue = v;
				return; // ok stop
			}
		}
		else {
			for (Pattern rule : rules.keySet()) {
				if (rule.matcher(value).matches()) {
					formValue = rules.get(rule);
					return; // ok stop
				}
			}
		}


		noMatchValues.add(value);
	}

	protected void updateAnaValuesIfMatch(String value) {

		if (converter != null) {
			String v = converter.getValue(parser, localname, null, value);
			if (v != null) {
				anaValues.put(newType, v); // do a replace if newType == oldType :-)
				anaResps.put(newType, "#txm_recode"); //$NON-NLS-1$
				return; // ok stop
			}
		}
		else {

			for (Pattern rule : rules.keySet()) {
				if (rule.matcher(value).matches()) {
					value = rules.get(rule);
					anaValues.put(newType, value); // do a replace if newType == oldType :-)
					anaResps.put(newType, "#txm_recode"); //$NON-NLS-1$
					return;
				}
			}
		}
		if (DELETE.equals(mode)) anaValues.put(newType, ""); // do a replace if newType == oldType :-) //$NON-NLS-1$
		else if (ABANDON.equals(mode)) anaValues.put(newType, "ERROR(" + value + ")"); // do a replace if newType == oldType :-) //$NON-NLS-1$ //$NON-NLS-2$

		noMatchValues.add(value);
	}

	public HashSet<String> getNoMatchValues() {
		return noMatchValues;
	}

	public static void main(String args[]) {
		try {
			File xmlFile = new File("/home/mdecorde/xml/recode/test.xml"); //$NON-NLS-1$
			File tmpFile = new File("/home/mdecorde/xml/recode/test-o.xml"); //$NON-NLS-1$
			String oldType = "myattr"; //$NON-NLS-1$
			String newType = "myattr"; //$NON-NLS-1$
			LinkedHashMap<Pattern, String> rules = new LinkedHashMap<>();
			rules.put(Pattern.compile(".+"), "NEWW"); //$NON-NLS-1$ //$NON-NLS-2$
			rules.put(Pattern.compile("x.+"), "XWORD"); //$NON-NLS-1$ //$NON-NLS-2$
			rules.put(Pattern.compile("y"), "YWORD"); //$NON-NLS-1$ //$NON-NLS-2$
			rules.put(Pattern.compile("y.*"), "YMULTIWORD"); //$NON-NLS-1$ //$NON-NLS-2$
			XMLTXMFileRuledConversion converter = new XMLTXMFileRuledConversion(xmlFile, rules, "w", oldType, newType, ABANDON); //$NON-NLS-1$
			converter.converter = new Converter() {

				@Override
				public String getValue(XMLStreamReader parser, String localname, String attribute, String value) {
					return "NEW" + value; //$NON-NLS-1$
				}
			};
			System.out.println(converter.process(tmpFile));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
