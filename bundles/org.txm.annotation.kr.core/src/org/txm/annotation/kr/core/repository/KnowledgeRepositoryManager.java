package org.txm.annotation.kr.core.repository;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import org.eclipse.osgi.util.NLS;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.DatabasePersistenceManager;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.objects.Project;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.sql.SQLConnection;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Element;


public class KnowledgeRepositoryManager extends DatabasePersistenceManager {

	// private static KnowledgeRepositoryManager INSTANCE = null;
	/**
	 * repositories cache. shared between corpus
	 */
	private static HashMap<String, KnowledgeRepository> repositories = new HashMap<>();

	// public static KnowledgeRepositoryManager getInstance(){
	// if (INSTANCE == null) {
	// INSTANCE = new KnowledgeRepositoryManager();
	// }
	// return INSTANCE;
	// }

	/**
	 * Lazy load KR.
	 * 
	 * @param name
	 * @param element
	 * @return
	 */
	public static KnowledgeRepository createKnowledgeRepository(String name, Element element) {
		HashMap<String, HashMap<String, ?>> conf = KRAnnotationEngine.getKnowledgeRepositoryConfiguration(name, element);
		return createKnowledgeRepository(name, conf);
	}

	/**
	 * Lazy load KR.
	 * 
	 * @param name
	 * @param preferences
	 * @return
	 * @throws BackingStoreException
	 */
	public static KnowledgeRepository createKnowledgeRepository(String name, Preferences preferences) throws BackingStoreException {
		HashMap<String, HashMap<String, ?>> conf = KRAnnotationEngine.getKnowledgeRepositoryConfiguration(name, preferences);
		return createKnowledgeRepository(name, conf);
	}

	/**
	 * Lazy load KR.
	 * 
	 * @param name
	 * @param conf
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static KnowledgeRepository createKnowledgeRepository(String name, HashMap<String, HashMap<String, ?>> conf) {

		// System.out.println("Creating Knowledge repository ["+name+"] with configuration="+conf);

		HashMap<String, String> access = (HashMap<String, String>) conf.get(KRAnnotationEngine.KNOWLEDGE_ACCESS);
		HashMap<String, HashMap<String, String>> fields = (HashMap<String, HashMap<String, String>>) conf.get(KRAnnotationEngine.KNOWLEDGE_TYPES);
		HashMap<String, HashMap<String, String>> strings = (HashMap<String, HashMap<String, String>>) conf.get(KRAnnotationEngine.KNOWLEDGE_STRINGS);
		String access_type = access.get("mode"); //$NON-NLS-1$

		KnowledgeRepository kr = null;
		if (!access_type.equals(DatabasePersistenceManager.ACCESS_SQL)
				&& !access_type.equals(DatabasePersistenceManager.ACCESS_FILE)) {
			Log.fine("Unknowned repository type: " + access_type + ". Assuming it's a 'file' KR."); //$NON-NLS-1$
			access.put("mode", "file"); //$NON-NLS-1$
			access_type = "file"; //$NON-NLS-1$
		}

		if (access_type.equals(DatabasePersistenceManager.ACCESS_SQL)) {
			kr = new SQLKnowledgeRepository(name);
			kr.setCredencialsNeeded("true".equals(access.get(SQLConnection.SQL_USER)), "true".equals(access.get(SQLConnection.SQL_PASSWORD))); //$NON-NLS-1$
		}
		else if (access_type.equals(DatabasePersistenceManager.ACCESS_FILE)) {
			kr = new LocalKnowledgeRepository(name);
		}
		else {
			Log.warning(Messages.bind(Messages.unknownRepositoryTypeP0, access_type));
		}

		if (kr != null) {
			// System.out.println("------- KnowledgeRepositoryManager : KR is been created "+kr.getName()+"--------");
			kr.setAccessProperties(access);
			kr.setStrings(strings);
			kr.setFieldsTypesProperties(fields);
			EntityManager em = kr.initializeEntityManager();
			if (em == null) return null;
			kr.buildTypesAndValues();
		}
		else {
			Log.warning(Messages.bind(Messages.errorKRNotCreatedP0, name+" "+conf)); // $NON-NLS-1$
		}

		return kr;
	}

	/**
	 * get all KR stored
	 * 
	 * @return
	 */
	public static KnowledgeRepository[] getKnowledgeRepositories() {
		return repositories.values().toArray(new KnowledgeRepository[repositories.size()]);
	}

	/**
	 * Creates&Registerd a simple KR with a given name
	 * 
	 * @param name
	 * @return a KR. If the KR already exists returns the KR
	 */
	public static KnowledgeRepository createSimpleKnowledgeRepository(String name) {
		if (getKnowledgeRepository(name) != null) {
			return getKnowledgeRepository(name);
		}

		HashMap<String, HashMap<String, ?>> conf = new HashMap<>();
		HashMap<String, String> access = new HashMap<>();
		conf.put(KRAnnotationEngine.KNOWLEDGE_ACCESS, access);
		HashMap<String, HashMap<String, String>> fields = new HashMap<>();
		conf.put(KRAnnotationEngine.KNOWLEDGE_TYPES, fields);
		HashMap<String, HashMap<String, String>> strings = new HashMap<>();
		conf.put(KRAnnotationEngine.KNOWLEDGE_STRINGS, strings);

		access.put("mode", DatabasePersistenceManager.ACCESS_FILE); //$NON-NLS-1$
		access.put("version", "0"); //$NON-NLS-1$ //$NON-NLS-2$
		KnowledgeRepository currentKnowledgeRepository = KnowledgeRepositoryManager.createKnowledgeRepository(name, conf);
		KnowledgeRepositoryManager.registerKnowledgeRepository(currentKnowledgeRepository);
		// KRAnnotationEngine.registerKnowledgeRepositoryName(corpus, corpus.getMainCorpus().getName());
		return currentKnowledgeRepository;
	}

	public static KnowledgeRepository getKnowledgeRepository(String name) {
		KnowledgeRepository kr = null;
		File krconf = new File(Toolbox.getTxmHomePath(), "repositories/" + name + "/conf.xml"); //$NON-NLS-1$
		if (repositories.containsKey(name)) {
			kr = repositories.get(name);
		}
		else {
			if (krconf.exists()) {
				try {
					kr = createKnowledgeRepository(name, DomUtils.load(krconf).getDocumentElement());
					if (kr == null) return null;
					repositories.put(name, kr);
				}
				catch (Exception e) {
					Log.warning(Messages.bind(Messages.failToLoadTheP0KRFromP1P2, name, krconf, e));
					Log.printStackTrace(e);
				}
			}
		}
		return kr;
	}

	/**
	 * get and lazy load a stored KR (initialized and all)
	 * 
	 * @param name
	 * @return
	 * @throws BackingStoreException
	 */
	public static KnowledgeRepository getKnowledgeRepository(String name, MainCorpus corpus) throws BackingStoreException {
		KnowledgeRepository kr = null;

		File krconf = new File(Toolbox.getTxmHomePath(), "repositories/" + name + "/conf.xml"); //$NON-NLS-1$

		if (repositories.containsKey(name)) { // kr already loaded
			kr = repositories.get(name);
		}
		else {
			if (krconf.exists()) {
				try {
					kr = createKnowledgeRepository(name, DomUtils.load(krconf).getDocumentElement());
					if (kr != null) {
						repositories.put(name, kr);
					}
					else {
						Log.warning(NLS.bind(Messages.errorKRNotCreatedP0, name));
						return null;
					}
				}
				catch (Exception e) {
					Log.warning(Messages.bind(Messages.failToLoadTheP0KRFromP1P2, name, krconf, e));
					Log.printStackTrace(e);
				}
			}
		}

		// test if the corpus has a new configuration for this KR
		// FIXME read KR configuration from Project preferences
		Project project = corpus.getProject();
		Preferences corpusprefkrconf = project.getPreferencesScope().getNode("KnowledgeRepository").node(name); //$NON-NLS-1$
		String sKRCorpusVersion = corpusprefkrconf.get("version", "0"); //$NON-NLS-1$
		if (kr != null) {
			int version = kr.getVersion();

			int krCorpusVersion = 0;
			if (sKRCorpusVersion.length() > 0)
				krCorpusVersion = Integer.parseInt(sKRCorpusVersion);
			// System.out.println("Versions KR : "+version+" >= "+krCorpusVersion);
			if (version >= krCorpusVersion) { // version is more recent
				return kr;
			}
			else {
				// replace the KR
				Log.fine("REPLACE OLD KR=" + kr + " (version=" + version + ") WITH corpus KR configuration (version=" + krCorpusVersion + ")"); //$NON-NLS-1$
				KnowledgeRepository krNew = createKnowledgeRepository(name, corpusprefkrconf);

				try { // write the new configuration file in krconf File
					krNew.saveConfiguration(krconf);
				}
				catch (Exception e) {
					Log.warning(Messages.bind(Messages.failToLoadTheP0KRFromP1P2, name, corpusprefkrconf, e));
					Log.printStackTrace(e);
					return kr;
				}
				if (krNew != null) {
					repositories.put(name, krNew);
					kr = krNew;
				}
			}
		}
		else {
			// replace the KR
			// System.out.println("CREATE KR="+kr+" WITH corpus KR configuration");
			KnowledgeRepository krNew = createKnowledgeRepository(name, corpusprefkrconf);

			if (krNew != null) {
				repositories.put(name, krNew);
				kr = krNew;
			}
			else {
				Log.warning(NLS.bind("Internal error: the new {0} KR is null.", name)); //$NON-NLS-1$
				return null;
			}

			try {
				krNew.saveConfiguration(krconf);
			}
			catch (Exception e) {
				Log.warning(Messages.bind(Messages.failToLoadTheP0KRFromP1P2, name, corpusprefkrconf, e));
				Log.printStackTrace(e);
				return kr;
			}
		}

		return kr;
	}

	public static void registerKnowledgeRepository(KnowledgeRepository kr) {
		repositories.put(kr.getName(), kr);
	}

	public static boolean[] mustLoginToKnowledgeRepository(String kr_name,
			MainCorpus corpus) {

		KnowledgeRepository kr;
		try {
			kr = getKnowledgeRepository(kr_name, corpus);
		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return KnowledgeRepository.FALSES;
		}
		if (kr == null) return KnowledgeRepository.FALSES;

		return kr.mustLoginToKnowledgeRepository();
	}

	private static String generateTemplateURLPage() {
		String html = "<html><head><title$title</title></head><body>$body</body></html>"; //$NON-NLS-1$
		return html;
	}

	public static String generateLocalURLPage(String krname, String name, String id) {

		KnowledgeRepository kr = getKnowledgeRepository(krname);
		String path = Toolbox.getTxmHomePath() + "/repositories/" + krname; //$NON-NLS-1$
		// File htmlTemplateFile = new File(path+"/template.html");
		File newHtmlFile = null;
		String htmlString;

		if (kr != null) {
			AnnotationType t = kr.getType(id);
			try {
				// htmlString = FileUtils.readFileToString(htmlTemplateFile);
				htmlString = generateTemplateURLPage();
				String title = "<p>" + name + " (" + id + ")</p>"; //$NON-NLS-1$
				String body = ""; //$NON-NLS-1$
				if (t != null) {
					List<TypedValue> values = kr.getValues(t);

					for (TypedValue value : values) {
						body += "<p>" + value.toHumanString() + "</p>"; //$NON-NLS-1$
					}

				}
				htmlString = htmlString.replace("$title", title); //$NON-NLS-1$
				htmlString = htmlString.replace("$body", body); //$NON-NLS-1$
				newHtmlFile = new File(path + "/" + id + ".html"); //$NON-NLS-1$
				IOUtils.write(newHtmlFile, htmlString);
			}
			catch (Exception e) {
				Log.warning(Messages.bind(Messages.errorwhileGeneratingTheHTMLContentOfAnnotationIDP1andErrorP0, id, e));
				Log.printStackTrace(e);
			}

		}
		else {
			Log.warning(NLS.bind("Error: the {0} KR is null !", krname)); //$NON-NLS-1$
		}

		return newHtmlFile.getAbsolutePath();
	}

	public static void deleteKnowledgeRepository(KnowledgeRepository kr) {
		// Delete all Annotation Types and Typed Values
		List<AnnotationType> annotTypes = kr.getAllAnnotationTypes();
		for (AnnotationType annotType : annotTypes) {
			kr.deleteType(annotType);
		}
		repositories.remove(kr.getName());
		kr.close();
	}


}
