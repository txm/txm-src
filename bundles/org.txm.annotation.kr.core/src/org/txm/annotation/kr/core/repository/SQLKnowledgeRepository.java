package org.txm.annotation.kr.core.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.persistence.EntityManager;

import org.eclipse.osgi.util.NLS;
import org.txm.annotation.kr.core.DatabasePersistenceManager;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.sql.SQLConnection;
import org.txm.utils.logger.Log;

public class SQLKnowledgeRepository extends KnowledgeRepository {

	private static final long serialVersionUID = 3813987566658055166L;

	protected boolean isConnected = false;

	public SQLKnowledgeRepository(String name) {
		super(name);
	}

	@Override
	public String getAccessType() {
		return DatabasePersistenceManager.ACCESS_SQL;
	}



	@Override
	public boolean buildValues() {
		return true; // values are lazily created
	}


	public TypedValue getValue(AnnotationType type, String annotationValue) {
		// System.out.println("get value KR distant: "+type+"="+annotationValue);
		String krname = type.getKnowledgeRepository();
		KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(krname);
		EntityManager em = kr.getJPAManager();

		TypedValue value = super.getValue(type, annotationValue);

		if (value != null) {
			// System.out.println("Value found in persistence.");
			return value;
		}

		// Looking for the annotation value in the KR remote SQL database and building the value as a TypedValue obj.
		HashMap<String, String> accessProperties = kr.getAccessProperties();
		HashMap<String, HashMap<String, String>> fieldsByType = this.getFieldsTypesProperties();

		String user = null;
		String password = null;

		if (kr.isLoginNeeded()) user = System.getProperty(LOGIN_KEY + kr.getName());
		if (kr.isPasswordNeeded()) password = System.getProperty(PASSWORD_KEY + kr.getName());

		Connection con = null;
		try {
			con = SQLConnection.createConnection(accessProperties.get(SQLConnection.SQL_ADDRESS),
					accessProperties.get(SQLConnection.SQL_DRIVER),
					user,
					password);
		}
		catch (Exception e) {
			Log.warning(NLS.bind("Fails to get SQL connection with {0} with error: {1}.", accessProperties, e)); //$NON-NLS-1$ //$NON-NLS-2$
			return null;
		}

		try {
			Statement st = con.createStatement();

			HashMap<String, String> typeFieldsContent = fieldsByType.get(type.getId());

			String typeQuery = typeFieldsContent.get(KnowledgeRepository.TYPE_SQLFIELD_QUERY);
			String name = typeFieldsContent.get(KnowledgeRepository.TYPE_SQLFIELD_NAME);
			String publicid = typeFieldsContent.get(KnowledgeRepository.TYPE_SQLFIELD_ID);
			typeQuery += " WHERE " + publicid + " LIKE '" + annotationValue + "'"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			ResultSet rs = st.executeQuery(typeQuery);
			// System.out.println("SQL query : "+typeQuery); //$NON-NLS-1$
			// System.out.println("Keys for types : " +fieldsByType.keySet().toString()); //$NON-NLS-1$
			em.getTransaction().begin();
			int i = 0;
			while (rs.next()) {
				HashMap<String, String> fields = fieldsByType.get(type.getId());
				for (String fieldKey : fields.keySet()) {
					if (fieldKey.equals(KnowledgeRepository.TYPE_SQLFIELD_NAME)) {
						name = rs.getString(fields.get(KnowledgeRepository.TYPE_SQLFIELD_NAME));
					}
					if (fieldKey.equals(KnowledgeRepository.TYPE_SQLFIELD_ID)) {
						publicid = rs.getString(fields.get(KnowledgeRepository.TYPE_SQLFIELD_ID));
					}
				}
				value = new TypedValue(name, publicid, type.getId());
				em.persist(value);
				// System.out.println("Value : "+annotationValue+" ("+publicid+"/"+name+") is going to be persisted !"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				if (i > 1) {
					Log.warning("KR should not return more than one value !"); //$NON-NLS-1$
				}
				i++;
			}
			em.getTransaction().commit();

		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		isConnected = true;
		SQLConnection.closeConnection(con);
		return value;
	}

	@Override
	public boolean[] mustLoginToKnowledgeRepository() {
		boolean[] ret = { false, false };

		if (isConnected) return ret;
		Log.warning(Messages.KRIsNotConnectedCheckingCredentialRequirement); //$NON-NLS-1$
		if (DatabasePersistenceManager.ACCESS_SQL.equals(accessProperties.get("mode"))) { //$NON-NLS-1$
			ret[0] = "true".equals(accessProperties.get(SQLConnection.SQL_USER)); //$NON-NLS-1$
			ret[1] = "true".equals(accessProperties.get(SQLConnection.SQL_PASSWORD)); //$NON-NLS-1$
		}
		return ret;
	}

	@Override
	public boolean checkConnection() {
		Connection con = null;
		try {
			String user = accessProperties.get(SQLConnection.SQL_USER);
			String password = accessProperties.get(SQLConnection.SQL_PASSWORD);

			// get user+password values stores in System.properties. these values must be set before
			if (isLoginNeeded()) user = System.getProperty(LOGIN_KEY + getName());
			if (isPasswordNeeded()) password = System.getProperty(PASSWORD_KEY + getName());

			con = SQLConnection.createConnection(accessProperties.get(SQLConnection.SQL_ADDRESS),
					accessProperties.get(SQLConnection.SQL_DRIVER),
					user,
					password);
			isConnected = true;
			con.close();
			return true;
		}
		catch (Exception e) {
			Log.warning(NLS.bind(Messages.FailToGetSQLConnectionWithTheP0PropertiesErrorP1, accessProperties, e)); //$NON-NLS-1$
		}
		isConnected = false;
		return false;
	}
}
