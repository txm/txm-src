package org.txm.annotation.kr.core;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.osgi.util.NLS;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.importer.StaxIdentityParser;
import org.txm.utils.logger.Log;

/**
 * The Class AnnotationStandoffInjector.
 *
 * @author sgedzelman, mdecorde
 *
 *         copy a XML-TXM file without annotations elements
 *         and creates annotations XML-TEI-SymoGIH annotation files for each annotation author
 * 
 */
public class AnnotationSyMoGIHWriter extends StaxIdentityParser {

	File xmlStandOffDirectory;

	boolean debug = false;

	String currentRef;

	String currentAuthor;

	String currentDate;

	String currentStartPos;

	String currentEndPos;

	// read xmlFile, to find annotations and update/write to xmlstandofffile
	// order annotations by annotator
	////// order annotations by date
	HashSet<String> types = new HashSet<String>();

	ArrayList<String> positions;

	HashMap<String, ArrayList<String>> annotationsPositions;

	XMLStreamWriter currentWriter;

	XMLStreamWriter standoffWriter;

	String currentType;

	boolean startAnnotation = false;

	private String filename;

	// author -> date -> annotation_values
	HashMap<String, HashMap<String, ArrayList<String[]>>> allannotations = new HashMap<String, HashMap<String, ArrayList<String[]>>>();

	HashMap<String, HashMap<String, ArrayList<ArrayList<String>>>> allannotationspositions = new HashMap<String, HashMap<String, ArrayList<ArrayList<String>>>>();

	private String textid;

	/**
	 * 
	 * @param debug
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public AnnotationSyMoGIHWriter(String textid, File xmlFile, File xmlStandOffDirectory, List<AnnotationType> types, boolean debug) throws IOException, XMLStreamException {
		super(xmlFile.toURI().toURL()); // init reader and writer
		this.textid = textid;
		this.filename = xmlFile.getName();
		this.debug = debug;
		this.xmlStandOffDirectory = xmlStandOffDirectory;
		factory = XMLInputFactory.newInstance();
		annotationsPositions = new HashMap<String, ArrayList<String>>();
		// System.out.println("AnnotationStandoff - ");
		for (AnnotationType type : types) {
			this.types.add(type.getId()); // .getName().toLowerCase());
			// System.out.println("Annotation Types in TXM : "+type.getName().toLowerCase() +" vs "+ type.getId());
		}
	}

	/*
	 * <TEI xmlns="http://www.tei-c.org/ns/1.0">
	 * <teiHeader>
	 * <fileDesc>
	 * <titleStmt>
	 * <title>Title</title>
	 * </titleStmt>
	 * <publicationStmt>
	 * <p>Publication Information</p>
	 * </publicationStmt>
	 * <sourceDesc>
	 * <p>Ce document permet l'annotation sémantique de tous les textes concernant l'association avec des unités de connaissance</p>
	 * </sourceDesc>
	 * </fileDesc>
	 * </teiHeader>
	 * <text>
	 * <body>
	 * <div>
	 * <div>
	 * <!-- La date dans le header indique la date d'annotation -->
	 * <head>
	 * <date type="annotation_date" when="2016-06-16"/>
	 * </head>
	 * <span type="identification d'entités nommées" ana="CoAc56389"
	 * target="#w_article_baip_1254-0714_1850_num_01_005_974_tei_2152
	 * #w_article_baip_1254-0714_1850_num_01_005_974_tei_2153
	 * #w_article_baip_1254-0714_1850_num_01_005_974_tei_2154
	 * #w_article_baip_1254-0714_1850_num_01_005_974_tei_2155
	 * #w_article_baip_1254-0714_1850_num_01_005_974_tei_2156" />
	 * </div>
	 * </div>
	 * </body>
	 * </text>
	 * </TEI>
	 */

	protected void processStartElement() throws XMLStreamException, IOException {
		// <coac author="gazelledess" ref="CoAc397" start="5" end="5">

		/*
		 * if(localname.startsWith("actr")){
		 * System.out.println("Check existence of actr in Corpus !!!! "+localname);
		 * }
		 */
		boolean foundAnnot = false;

		if (types.contains(localname) && parser.getPrefix().equals("txm")) { // don't write txm annotation elements //$NON-NLS-1$
			// System.out.println(" START "+ localname);
			foundAnnot = true;
			currentType = localname;
			// <txm:actr author="gazelledess" ref="PhileasFogg" date="2016-09-05" start="56" end="57">
			currentAuthor = parser.getAttributeValue(null, "author"); //$NON-NLS-1$
			currentRef = parser.getAttributeValue(null, "ref"); //$NON-NLS-1$
			currentStartPos = parser.getAttributeValue(null, "start"); //$NON-NLS-1$
			currentEndPos = parser.getAttributeValue(null, "end"); //$NON-NLS-1$
			currentDate = parser.getAttributeValue(null, "date"); //$NON-NLS-1$
			// annotation is here
			startAnnotation = true;
			positions = new ArrayList<String>();
			annotationsPositions.put(currentType, positions);

			// initialize allannotations
			if (!allannotations.containsKey(currentAuthor)) {
				allannotations.put(currentAuthor, new HashMap<String, ArrayList<String[]>>());
				allannotationspositions.put(currentAuthor, new HashMap<String, ArrayList<ArrayList<String>>>());
			}
			HashMap<String, ArrayList<String[]>> authorsAnnotation = allannotations.get(currentAuthor);
			HashMap<String, ArrayList<ArrayList<String>>> authorsAnnotationPositions = allannotationspositions.get(currentAuthor);
			if (!authorsAnnotation.containsKey(currentDate)) {
				authorsAnnotation.put(currentDate, new ArrayList<String[]>());
				authorsAnnotationPositions.put(currentDate, new ArrayList<ArrayList<String>>());
			}

			storeAnnotation();
		}

		if (!foundAnnot) {
			super.processStartElement(); /// continue writing in file all elements, except the tags that are now in stand-off files

			// get words ids of the current annotations
			if (localname.equals("w") && startAnnotation) { //$NON-NLS-1$
				String id = parser.getAttributeValue(null, "id"); //$NON-NLS-1$
				for (String typeIn : annotationsPositions.keySet()) {
					positions = annotationsPositions.get(typeIn);
					positions.add(id);
					// System.out.println("Positions of w id="+posW+" for ["+typeIn+"] ");
				}
			}
		}
	}


	/**
	 * ends the current author stand-off file
	 * 
	 * @param currentWriter
	 */
	private void writeEndStandOffFile(XMLStreamWriter currentWriter) {
		// System.out.println("writeEndStandOffFile ...");

		try {
			currentWriter.writeEndElement();
			currentWriter.writeEndDocument();

			currentWriter.flush();
			currentWriter.close();
		}
		catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the stand-off file for one author
	 * 
	 * @param file
	 * @return
	 */
	private XMLStreamWriter writeStartStandOffFile(File file) {
		// System.out.println("writeStartStandOffFile ...");

		String ns = "http://www.tei-c.org/ns/1.0"; //$NON-NLS-1$
		XMLOutputFactory output = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = null;
		try {
			writer = output.createXMLStreamWriter(new FileWriter(file));
			writer.writeStartDocument();
			writer.setPrefix("tei", ns); //$NON-NLS-1$
			writer.setDefaultNamespace(ns);

			writer.writeStartElement("TEI"); //$NON-NLS-1$

			writer.writeStartElement("teiHeader"); //$NON-NLS-1$
			writer.writeStartElement("fileDesc"); //$NON-NLS-1$

			writer.writeStartElement("titleStmt"); //$NON-NLS-1$
			writer.writeStartElement("title"); //$NON-NLS-1$
			writer.writeCharacters(textid);
			writer.writeEndElement(); // title
			writer.writeEndElement(); // titleStmt

			writer.writeStartElement("publicationStmt"); //$NON-NLS-1$
			writer.writeStartElement("p"); //$NON-NLS-1$
			writer.writeCharacters("PUBLICATION INFO à renseigner"); //$NON-NLS-1$
			writer.writeEndElement(); // p
			writer.writeEndElement(); // publicationStmt

			writer.writeStartElement("sourceDesc"); //$NON-NLS-1$
			writer.writeStartElement("p"); //$NON-NLS-1$
			writer.writeCharacters("Ce document permet l'annotation sémantique de tous les textes, par auteur"); //$NON-NLS-1$
			writer.writeEndElement(); // p
			writer.writeEndElement(); // sourceDesc

			writer.writeEndElement(); // </fileDesc>
			writer.writeStartElement("encodingDesc"); //$NON-NLS-1$
			writer.writeStartElement("projectDesc"); //$NON-NLS-1$
			writer.writeStartElement("p"); //$NON-NLS-1$
			writer.writeCharacters("Annotations created by " + currentAuthor + ", for the use in Symogih XML platform"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeEndElement(); // p
			writer.writeEndElement(); // </projectDesc>
			writer.writeEndElement(); // </encodingDesc>
			writer.writeEndElement(); // </teiHeader>


			writer.writeStartElement("text"); //$NON-NLS-1$
			writer.writeStartElement("body"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeStartElement("div"); //$NON-NLS-1$
			writer.writeCharacters("\n"); //$NON-NLS-1$
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return writer;
	}

	private void storeAnnotation() {
		allannotations.get(currentAuthor).get(currentDate).add(new String[] { currentDate, currentRef, currentType });
	}

	private void storeAnnotationPositions() {
		allannotationspositions.get(currentAuthor).get(currentDate).add(positions);
	}

	/**
	 * write stand-off annotation
	 */
	private void writeStartAnnotationToStandoffFile(String[] data, ArrayList<String> positions) {
		// System.out.println("writeStartAnnotationToStandoffFile ...");
		try {
			currentWriter.writeStartElement("span"); //$NON-NLS-1$
			currentWriter.writeAttribute("type", data[2]); //$NON-NLS-1$
			currentWriter.writeAttribute("ana", data[1]); //$NON-NLS-1$
			StringBuffer listWids = new StringBuffer();
			for (String posW : positions) {
				listWids.append("#" + posW + " "); //$NON-NLS-1$ //$NON-NLS-2$
			}
			currentWriter.writeAttribute("target", listWids.toString().trim()); //$NON-NLS-1$
			currentWriter.writeEndElement(); // span
			currentWriter.writeCharacters("\n"); //$NON-NLS-1$
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void processEndElement() throws XMLStreamException {
		boolean foundAnnot = false;
		if (types.contains(localname) && parser.getPrefix().equals("txm")) { // skip annotation end element //$NON-NLS-1$
			// System.out.println(" END "+ localname);
			foundAnnot = true;
			// annotation ends here
			storeAnnotationPositions();

			if (annotationsPositions.containsKey(localname)) {
				annotationsPositions.remove(localname);
			}

			startAnnotation = false;
		}

		if (!foundAnnot) {
			if (localname.equals("TEI")) { //$NON-NLS-1$
				try {
					for (String author : allannotations.keySet()) {
						Log.info("    author=" + author); //$NON-NLS-1$
						File currentXmlFile = new File(xmlStandOffDirectory, filename.substring(0, filename.length() - 4) + "_" + currentAuthor + "_annotations.xml"); //$NON-NLS-1$ //$NON-NLS-2$

						// write start of the stand-off file
						currentWriter = writeStartStandOffFile(currentXmlFile);

						// write annotations grouped per date
						HashMap<String, ArrayList<String[]>> dates = allannotations.get(author);
						HashMap<String, ArrayList<ArrayList<String>>> datesPositions = allannotationspositions.get(author);
						for (String date : datesPositions.keySet()) {
							ArrayList<String[]> datas = dates.get(date);
							ArrayList<ArrayList<String>> positions = datesPositions.get(date);

							currentWriter.writeStartElement("div"); //$NON-NLS-1$
							currentWriter.writeCharacters("\n"); //$NON-NLS-1$
							currentWriter.writeStartElement("head"); //$NON-NLS-1$
							currentWriter.writeStartElement("date"); //$NON-NLS-1$
							currentWriter.writeAttribute("type", "annotation_date"); //$NON-NLS-1$ //$NON-NLS-2$
							currentWriter.writeAttribute("when", date); //$NON-NLS-1$
							currentWriter.writeEndElement(); // date
							currentWriter.writeEndElement(); // head
							currentWriter.writeCharacters("\n"); //$NON-NLS-1$

							for (int i = 0; i < datas.size(); i++) {
								writeStartAnnotationToStandoffFile(datas.get(i), positions.get(i));
							}

							currentWriter.writeEndElement(); // div
							currentWriter.writeCharacters("\n"); //$NON-NLS-1$
						}

						// write the end of the stand-off file
						writeEndStandOffFile(currentWriter);
					}
				}
				catch (Exception e) {
					Log.warning(NLS.bind(Messages.ErrorWithCurrentWriterP0, currentWriter));
					Log.printStackTrace(e);
				}
			}
			super.processEndElement();
		}
	}

	public static void main(String strs[]) {
		try {
			File xmlFile = new File("/home/mdecorde/TXM/corpora/TDM80J/txm/TDM80J", "tdm80j.xml"); //$NON-NLS-1$ //$NON-NLS-2$
			File outfile = new File(xmlFile.getParentFile(), "tdm80j-tei.xml"); //$NON-NLS-1$

			File xmlStandOffDirectory = xmlFile.getParentFile();
			List<AnnotationType> types = new ArrayList<AnnotationType>();
			types.add(new AnnotationType("local", "actr_id", "actr")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			AnnotationSyMoGIHWriter annotStdff;
			try {
				annotStdff = new AnnotationSyMoGIHWriter("TDBM80J", xmlFile, xmlStandOffDirectory, types, true); //$NON-NLS-1$
				annotStdff.process(outfile);
			}
			catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
