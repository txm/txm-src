package org.txm.annotation.kr.core;

import java.util.HashMap;

import javax.persistence.EntityManager;

public class DatabasePersistenceManager {

	// Object can be Corpus or KnowledgeRepository
	protected HashMap<Object, EntityManager> managers;

	public static final String PERSISTENCE_UNIT_NAME = "HSQLKRPERSISTENCE"; //$NON-NLS-1$

	public static String ACCESS_SQL = "sql"; //$NON-NLS-1$

	public static String ACCESS_FILE = "file"; //$NON-NLS-1$

	public static String ACCESS_SPARQL = "sparql"; //$NON-NLS-1$

	/**
	 * Instantiates a new database manager.
	 */
	public DatabasePersistenceManager() {
		managers = new HashMap<Object, EntityManager>();
	}

	/**
	 * The Object can be a Corpus or a KnowledgeRepository
	 * 
	 * @param obj
	 * @return
	 */
	public EntityManager getJPAEntityManager(Object obj) {
		if (managers.containsKey(obj)) {
			return managers.get(obj);
		}
		return null;
	}

	public void closeManager(Object key) {
		if (!managers.keySet().contains(key)) return;

		managers.get(key).close();
		managers.remove(key);
	}

	public void closeAllManagers() {
		for (Object key : managers.keySet()) {
			EntityManager m = managers.get(key);
			m.flush();
			m.createNativeQuery("SHUTDOWN;"); //$NON-NLS-1$
			m.close();
		}
		managers.clear();
	}
}
