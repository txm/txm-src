package org.txm.annotation.kr.core.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.annotation.kr.core.AnnotationScope;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 *
 */
public class KRAnnotationPreferences extends TXMPreferences {

	public static final String UPDATE_EDITION = "udpate_edition"; //$NON-NLS-1$

	public static final String UPDATE = "udpate"; //$NON-NLS-1$

	public static final String PRESERVE_ANNOTATIONS = "reset_temporary_annotations_when leaving_txm"; //$NON-NLS-1$

	public static final String DEFAULT_SCOPE = "default_scope"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {

		if (!TXMPreferences.instances.containsKey(KRAnnotationPreferences.class)) {
			new KRAnnotationPreferences();
		}
		return TXMPreferences.instances.get(KRAnnotationPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {

		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(UPDATE_EDITION, true);
		preferences.putBoolean(UPDATE, true);
		preferences.putBoolean(PRESERVE_ANNOTATIONS, false);
		preferences.put(DEFAULT_SCOPE, AnnotationScope.FIRSTTARGET.label());
	}
}
