package org.txm.annotation.kr.core.storage.temporary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.jpa.PersistenceProvider;
import org.txm.annotation.kr.core.Annotation;
import org.txm.annotation.kr.core.AnnotationPK;
import org.txm.annotation.kr.core.DatabasePersistenceManager;
import org.txm.annotation.kr.core.preferences.KRAnnotationPreferences;
import org.txm.annotation.kr.core.repository.AnnotationEffect;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.objects.Match;
import org.txm.objects.Match2P;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

public class TemporaryAnnotationManager {

	/**
	 * 
	 * @param clazz persistence.xml location depends on the classloader
	 * @param corpus
	 * @return
	 */
	public static HashMap<String, Object> getInitialisationProperties(Class<?> clazz, MainCorpus corpus) {
		File path = new File(corpus.getProjectDirectory(), "temporary_annotations/" + corpus.getID() + "/db"); //$NON-NLS-1$ //$NON-NLS-2$
		path.getParentFile().mkdirs();

		HashMap<String, Object> properties = new HashMap<>();
		// System.out.println("CLASSLOADER: "+Toolbox.class.getClassLoader());
		properties.put(PersistenceUnitProperties.CLASSLOADER, clazz.getClassLoader());
		properties.put("javax.persistence.jdbc.driver", "org.hsqldb.jdbcDriver"); //$NON-NLS-1$ //$NON-NLS-2$

		String urlProperty = "jdbc:hsqldb:file:" + path + ";shutdown=true;hsqldb.write_delay=false;hsqldb.lock_file=false;"; //$NON-NLS-1$ //$NON-NLS-2$
		if (Log.getLevel().intValue() < Level.INFO.intValue()) {
			urlProperty += ""; //$NON-NLS-1$
		}
		else {
			urlProperty += "hsqldb.applog=0;hsqldb.sqllog=0"; //$NON-NLS-1$
		}
		properties.put("javax.persistence.jdbc.url", urlProperty); //$NON-NLS-1$
		properties.put("javax.persistence.jdbc.username", "SA"); //$NON-NLS-1$ //$NON-NLS-2$
		properties.put(PersistenceUnitProperties.DDL_GENERATION_MODE, "database"); //$NON-NLS-1$
		if (KRAnnotationPreferences.getInstance().getBoolean(KRAnnotationPreferences.PRESERVE_ANNOTATIONS)) {
			properties.put(PersistenceUnitProperties.DDL_GENERATION, "create-or-extend-tables"); // create&update table if needed, drop-and-create-tables to reset all //$NON-NLS-1$
		}
		else {
			properties.put(PersistenceUnitProperties.DDL_GENERATION, "drop-and-create-tables"); // drop-and-create-tables to reset all //$NON-NLS-1$
		}

		if (Log.getLevel().intValue() < Level.INFO.intValue()) {
			properties.put(PersistenceUnitProperties.LOGGING_LEVEL, "INFO"); //$NON-NLS-1$
			properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "true"); //$NON-NLS-1$
		}
		else {
			properties.put(PersistenceUnitProperties.LOGGING_LEVEL, "OFF"); //$NON-NLS-1$
			properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "false"); //$NON-NLS-1$
		}

		return properties;
	}

	private EntityManager em;

	private MainCorpus corpus;

	private EntityManagerFactory emf;

	public TemporaryAnnotationManager(MainCorpus corpus) {
		this.corpus = corpus;
		em = initTemporaryAnnotationDatabase();
	}

	public TemporaryAnnotationManager(MainCorpus corpus, HashMap<String, Object> properties) {
		this.corpus = corpus;
		em = initTemporaryAnnotationDatabase(properties);
	}

	public void checkData() throws Exception {
		List<Annotation> annots = getAnnotations();
		System.out.println("ID \t Type \t Val \t startPos \t startEnd \t Text "); //$NON-NLS-1$
		System.out.println("----------------------------------------------------"); //$NON-NLS-1$
		for (Annotation an : annots) {
			System.out.print("\t " + an.getPK().getRefType()); //$NON-NLS-1$
			System.out.print("\t " + an.getValue()); //$NON-NLS-1$
			System.out.print("\t " + an.getPK().getStartPosition()); //$NON-NLS-1$
			System.out.print("\t " + an.getPK().getEndPosition() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	public void close() {
		if (em != null) {
			em.close();
		}
		if (emf != null) {
			emf.close();
		}
	}


	public List<Annotation> createAnnotation(AnnotationType type, TypedValue value, int startPos, int endPos) throws Exception {
		em.getTransaction().begin();
		List<Annotation> a = createAnnotationNoCommit(type, value, startPos, endPos);
		em.getTransaction().commit();
		return a;
	}

	/**
	 * update or create an annotation BUT em.getTransaction().begin(); and em.getTransaction().commit();
	 * must be called before and after !!
	 * 
	 * @param type
	 * @param value
	 * @param startPos
	 * @param endPos
	 * @return overlapping annotations
	 * @throws Exception
	 */
	public List<Annotation> createAnnotationNoCommit(AnnotationType type, TypedValue value, int startPos, int endPos) throws Exception {
		AnnotationPK pkannot = new AnnotationPK(startPos, endPos, type.getId());
		Annotation annot = em.find(Annotation.class, pkannot);
		List<Annotation> overlapingAnnotations = new ArrayList<>();
		if (annot == null) { // create annotation

			if (type.getEffect().equals(AnnotationEffect.SEGMENT)) {
				// check if it does not already exist, with either one of the positions (overlaps)
				// isOverlapAnnots = isOverlapAnnotationCritical(refType, refVal, startPos, endPos);
				ArrayList<Match> matches = new ArrayList<>();
				matches.add(new Match2P(startPos, endPos));
				overlapingAnnotations = getAnnotations(null, matches, null, false, true); // getAll annotation ovelapping with startPos-endPos
				for (int i = 0; i < overlapingAnnotations.size(); i++) { // parse overlapping annotations to remove wrapping annotation with different type and annotation with same type and same
																		// position
					annot = overlapingAnnotations.get(i);
					if (annot.getType().equals(type.getId())) {
						if (annot.getStart() == startPos && annot.getEnd() == endPos) {
							overlapingAnnotations.remove(i);
							i--;
						}
					}
					else {
						// System.out.println("Pas le même TYPE !!!!! DONC on exige que seuls INNER et OUTTER overlaps sont admis");
						if ((annot.getStart() <= startPos && endPos <= annot.getEnd() ||
								(startPos <= annot.getStart() && annot.getEnd() <= endPos))) {
							/*
							 * System.out.println("Taille du tableau annotations AVANT : "+overlapingAnnotations.size());
							 * System.out.println(annot.getStart()+" <= " +startPos+" && "+endPos+" <= "+annot.getEnd()+" OU ");
							 * System.out.println(startPos+" <= " +annot.getStart()+" && "+annot.getEnd()+" <= "+endPos);
							 */
							overlapingAnnotations.remove(i);
							// System.out.println("Taille du tableau annotations APRES : "+overlapingAnnotations.size());
							i--;
						}
						else {
							// System.out.println(annot.getStart()+"-"+annot.getEnd()+" // "+startPos+"-"+endPos);
						}
					}
				}
			}
			else {
				// no need to check collision
			}

			if (overlapingAnnotations.size() > 0) { // don't create the annotation
				// for (Annotation overlapAnnot : overlapingAnnotations) {
				// System.out.println("---Overlap with : "+overlapAnnot.toString());
				// }
			}
			else { // we can create the annotation \o/
				annot = new Annotation(type.getId(), value.getId(), startPos, endPos);

				em.persist(annot);
				// System.out.println("--- Create : "+annot.toString());
			}
		}
		else { // update annotation
				// UPDATE
				// System.out.println("--- Update annot : "+annot.toString()+" ----- ");
			annot.setReferentielVal(value.getId());
		}
		return overlapingAnnotations;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Annotation> createAnnotations(AnnotationType type, TypedValue value, List<Match> matches) throws Exception {
		List<Annotation> isOverlappedWithAnnots = new ArrayList<>();
		em.getTransaction().begin(); // mandatory ! :@
		for (Match m : matches) {
			int t = m.getTarget();
			List<Annotation> annotsOverlapped = null;
			if (t >= 0) {
				annotsOverlapped = createAnnotationNoCommit(type, value, t, t);
			}
			else {
				annotsOverlapped = createAnnotationNoCommit(type, value, m.getStart(), m.getEnd());
			}

			if (annotsOverlapped != null) {
				isOverlappedWithAnnots.addAll(annotsOverlapped);
			}
		}
		em.getTransaction().commit(); // mandatory ! :@
		return isOverlappedWithAnnots;
	}

	public void deleteAnnotation(AnnotationType type, int start, int end) throws Exception {
		em.getTransaction().begin();
		deleteAnnotationNoCommit(type, start, end);
		em.getTransaction().commit();
	}

	public void deleteAnnotation(AnnotationType type, Match m) throws Exception {
		deleteAnnotation(type, m.getStart(), m.getEnd());
	}

	/**
	 * Warning this method must be called ONLY if a transaction has been start AND dont forget to call commit after
	 * 
	 * @param type
	 * @param start
	 * @param end
	 */
	public void deleteAnnotationNoCommit(AnnotationType type, int start, int end) {
		List<Annotation> annots = getAnnotations(type, start, end);
		for (Annotation annot : annots) {
			em.remove(annot);
		}
	}

	public void deleteAnnotations() throws Exception {
		List<Annotation> annots = getAnnotations();
		em.getTransaction().begin();
		for (Annotation annot : annots) {
			em.remove(annot);
		}
		em.getTransaction().commit();
	}

	public void deleteAnnotations(AnnotationType refType, List<Match> matches) throws Exception {
		em.getTransaction().begin();
		for (Match m : matches) {
			deleteAnnotationNoCommit(refType, m.getStart(), m.getEnd());
		}
		em.getTransaction().commit();
	}

	/**
	 * returns all annotations with no particular constraints
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Annotation> getAnnotations() throws Exception {

		TypedQuery<Annotation> query = em.createQuery("SELECT annot FROM Annotation AS annot", Annotation.class); //$NON-NLS-1$
		return query.getResultList();
	}

	/**
	 * returns all annotations with no particular constraints
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Annotation> getAnnotations(AnnotationType type) throws Exception {
		return getAnnotations(type, false);
	}

	/**
	 * returns all annotations with no particular constraints
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Annotation> getAnnotations(AnnotationType type, boolean ordered) throws Exception {
		return getAnnotations(type, -1, -1, null);
	}

	public List<Annotation> getAnnotations(AnnotationType type, int start, int end) {
		return getAnnotations(type, start, end, null);
	}

	/**
	 * returns annotation with all available constraints
	 * 
	 * @return
	 */
	public List<Annotation> getAnnotations(AnnotationType refType, int startPosition, int endPosition, String value) {
		String query = "SELECT annot FROM Annotation AS annot WHERE annot.PK.refType LIKE '" + refType.getId() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		if (startPosition >= 0)
			query += " AND annot.PK.startpos = " + startPosition; //$NON-NLS-1$

		if (endPosition >= 0)
			query += " AND annot.PK.endpos = " + endPosition; //$NON-NLS-1$

		if (value != null)
			query += " AND annot.refVal LIKE ':" + value + "' "; //$NON-NLS-1$ //$NON-NLS-2$

		TypedQuery<Annotation> typedQuery = em.createQuery(query, Annotation.class);
		return typedQuery.getResultList();
	}


	/**
	 * returns all annotations given a series of positions (start and end)
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Annotation> getAnnotations(AnnotationType type, List<? extends Match> matches, String value, boolean ordered, boolean overlap) throws Exception {
		String query = "SELECT annot FROM Annotation AS annot"; //$NON-NLS-1$
		boolean where = false; // TODO: need to replace this

		String queryPos = " "; //$NON-NLS-1$
		int i = 0;

		for (Match match : matches) {
			int start, end;
			if (match.getTarget() >= 0) {
				start = end = match.getTarget();
			}
			else {
				start = match.getStart();
				end = match.getEnd();
			}

			if (i == 0) {
				if (overlap) {
					queryPos += " ( (" + overlapString(start, end, 0) + ")" //$NON-NLS-1$ //$NON-NLS-2$
							+ " OR (" + overlapString(start, end, 1) + ") "; //$NON-NLS-1$ //$NON-NLS-2$
					// String overlapBesides = "annot.PK.endpos < "+match.getStart()+" OR annot.PK.startpos > "+match.getEnd()+"";
					// queryPos += " AND NOT ( "+overlapBesides+" ) ) ";
					queryPos += " OR ( annot.PK.startpos <= " + start + " AND " + end + " <= annot.PK.endpos ) )"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				else {
					queryPos += " (annot.PK.startpos = " + start + " AND annot.PK.endpos = " + end + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
			}
			else {
				if (overlap) {
					queryPos += " OR ( (" + overlapString(start, end, 0) + ")" //$NON-NLS-1$ //$NON-NLS-2$
							+ " OR (" + overlapString(start, end, 1) + ") "; //$NON-NLS-1$ //$NON-NLS-2$
					// String overlapBesides = "annot.PK.endpos < "+match.getStart()+" OR annot.PK.startpos > "+match.getEnd()+"";
					// queryPos += " AND NOT ( "+overlapBesides+" ) ) ";
					queryPos += " OR ( annot.PK.startpos <= " + start + " AND " + end + " <= annot.PK.endpos ) )"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
				else {
					queryPos += " OR (annot.PK.startpos = " + start + " AND annot.PK.endpos = " + end + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
			}
			++i;
		}

		if (matches.size() > 0) {
			if (where)
				query += " AND ( " + queryPos + " )"; //$NON-NLS-1$ //$NON-NLS-2$
			else {
				query += " WHERE ( " + queryPos + " )"; //$NON-NLS-1$ //$NON-NLS-2$
				where = true;
			}
		}

		if (type != null) {
			if (where)
				query += " AND annot.PK.refType LIKE '" + type.getId() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			else {
				query += " WHERE annot.PK.refType LIKE '" + type.getId() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
				where = true;
			}
		}

		if (value != null) {
			if (where)
				query += " AND annot.PK.refVal LIKE '" + value + "'"; //$NON-NLS-1$ //$NON-NLS-2$
			else {
				query += " WHERE annot.PK.refVal LIKE '" + value + "'"; //$NON-NLS-1$ //$NON-NLS-2$
				where = true;
			}
		}
		// System.out.println("Query : "+ query);
		// System.out.println("STOP GET ANNOTATIONS @"+i);
		TypedQuery<Annotation> typedQuery = em.createQuery(query, Annotation.class);
		return typedQuery.getResultList();
	}

	public List<Annotation> getAnnotationsForMatches(List<Match> sortedMatches, List<Annotation> sortedAnnotations, boolean overlap) {

		List<Annotation> annotsList = new ArrayList<>();
		//		matches = new ArrayList<>(matches);
		//		Collections.sort(matches);
		int iMatch = 0;
		int iAnnotation = 0;
		int nMatch = sortedMatches.size();
		int nAnnotations = sortedAnnotations.size();

		Match match = null;
		Annotation annotation = null;
		while (iMatch < nMatch && iAnnotation < nAnnotations) {

			match = sortedMatches.get(iMatch);
			int start, end;
			if (match.getTarget() >= 0) {
				start = end = match.getTarget();
			}
			else {
				start = match.getStart();
				end = match.getEnd();
			}

			annotation = sortedAnnotations.get(iAnnotation);
			int astart = annotation.getStart(), aend = annotation.getEnd();

			if (overlap) {
				if (end < astart) {
					iMatch++;
					annotsList.add(null); // nothing for this match
				}
				else if (aend < start) {
					iAnnotation++;
				}
				else {
					annotsList.add(annotation);
					iMatch++;
				}
			}
			else {
				if (end < astart) {
					iMatch++;
					annotsList.add(null); // nothing for this match
				}
				else if (aend < start) {
					iAnnotation++;
				}
				else if (astart == start &&
						aend == end) {
							annotsList.add(annotation);
							iMatch++;
							iAnnotation++;
						}
				else {
					iAnnotation++;
				}
			}
		}

		while (annotsList.size() < sortedMatches.size())
			annotsList.add(null); // fill matches that were not process due to no more annotations

		return annotsList;
	}

	/**
	 * You know what you're doing
	 * 
	 * @return the EntityManager that manages the DB
	 */
	public EntityManager getEntityManager() {
		return em;
	}

	public boolean hasChanges() {
		try {
			if (em == null) return false;
			TypedQuery<Annotation> query = em.createQuery("SELECT annot FROM Annotation AS annot", Annotation.class); //$NON-NLS-1$
			// System.out.println("hasChanges query rez: "+query.getResultList());
			return query.setMaxResults(1).getResultList().size() > 0; // get only the first match -> faster
		}
		catch (Exception e) {
			Log.severe("** Error while testing corpus annotations state: " + e); //$NON-NLS-1$
		}
		return false;
	}

	public EntityManager initTemporaryAnnotationDatabase() {
		HashMap<String, Object> properties = TemporaryAnnotationManager.getInitialisationProperties(this.getClass(), corpus);

		return initTemporaryAnnotationDatabase(properties);
	}

	public EntityManager initTemporaryAnnotationDatabase(HashMap<String, Object> properties) {

		if (em != null) return em;

		PersistenceProvider pp = new PersistenceProvider();
		// System.out.println("Check for provider: "+pp.checkForProviderProperty(properties));

		// System.out.println("Generate schema: "+pp.generateSchema(PERSISTENCE_UNIT_NAME, properties));
		// SEPersistenceUnitInfo infos = new SEPersistenceUnitInfo();
		// infos.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
		// infos.setTransactionType(PersistenceUnitTransactionType.RESOURCE_LOCAL);
		// infos.setPersistenceProviderClassName("org.eclipse.persistence.jpa.PersistenceProvider");
		// infos.getManagedClassNames().add("org.txm.annotation.Annotation");
		// infos.getManagedClassNames().add("org.txm.annotation.TypedValue");
		// pp.generateSchema(infos, properties);

		emf = pp.createEntityManagerFactory(DatabasePersistenceManager.PERSISTENCE_UNIT_NAME, properties);

		em = emf.createEntityManager();

		return em;
	}

	private String overlapString(int startPos, int endPos, int typeOverlap) {
		String overlap = ""; //$NON-NLS-1$
		switch (typeOverlap) {
			case 0:
				return overlap += "annot.PK.startpos >= " + startPos + " AND annot.PK.startpos <= " + endPos; // OverlapOnStart //$NON-NLS-1$ //$NON-NLS-2$
			case 1:
				return overlap += "annot.PK.endpos >= " + startPos + " AND annot.PK.endpos <= " + endPos + ""; // OverlapOnEnd //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			default:
				return overlap;
		}

	}

	/**
	 * returns annotation with all available constraints
	 * 
	 * @param typeId
	 * @return
	 * @throws Exception
	 */
	// A REVOIR avec le tuto : http://stackoverflow.com/questions/2772305/jpql-in-clause-java-arrays-or-lists-sets
	/*
	 * public List<Annotation> getAnnotations(AnnotationType refType, int[] startPositions, int[] endPositions, String value, boolean ordered) {
	 * EntityManager em = getJPAManager(corpus);
	 * String query = "SELECT annot FROM Annotation AS annot "
	 * + "WHERE annot.PK.refType LIKE '"+refType.getId()+"'";
	 * if (startPositions.length > 0)
	 * query += " AND annot.PK.startpos IN :startpositions ";
	 * if (endPositions.length > 0)
	 * query += " AND annot.PK.endpos IN :endpositions ";
	 * if (value != null)
	 * query += " AND annot.refVal LIKE ':"+value+"' ";
	 * if (ordered)
	 * query += " ORDER BY annot.refVal, annot.PK.startpos ASC";
	 * TypedQuery<Annotation> typedQuery = em.createQuery(query, Annotation.class);
	 * List<String> positionsOfStart = Arrays.asList(startPositions.toString());
	 * List<String> positionsOfEnd = Arrays.asList(endPositions.toString());
	 * typedQuery.setParameter("startpositions", positionsOfStart);
	 * typedQuery.setParameter("endpositions", positionsOfEnd);
	 * return typedQuery.getResultList();
	 * }
	 */

	// /**
	// * Should not
	// * @param corpus
	// * @param refType
	// * @param refVal
	// * @param startPos
	// * @param endPos
	// * @return
	// */
	// public List<Annotation> isOverlapAnnotationCritical(AnnotationType refType, TypedValue refVal, int startPos, int endPos){
	// EntityManager em = getJPAManager(corpus);
	//
	// String query = "SELECT annot FROM Annotation AS annot "
	//
	// + "WHERE ( annot.PK.refType not LIKE '"+refType.getId()+"' ";
	// //String overlapInside = "annot.PK.startpos >= "+startPos+" OR annot.PK.endpos <= "+endPos+"";
	// //String overlapOutside = "annot.PK.startpos <= "+startPos+" OR annot.PK.endpos >= "+endPos+"";
	// //query += " AND ( ( "+overlapInside+" ) OR ( "+overlapOutside+" ) ) AND NOT ( "+overlapBesides+" ) ) ";
	// String overlapOnEnd = "annot.PK.startpos >= "+startPos+" AND annot.PK.startpos <= "+endPos +" AND annot.PK.endpos >= "+endPos+"";
	// String overlapOnStart = "annot.PK.startpos <= "+startPos+" AND annot.PK.endpos >= "+startPos+" AND annot.PK.endpos <= "+endPos+"";
	// String overlapBesides = "annot.PK.endpos < "+startPos+" OR annot.PK.startpos > "+endPos+"";
	// query += " AND ( ( "+overlapOnEnd+" ) OR ( "+overlapOnStart+" ) ) ) ";
	// query += " OR ( annot.PK.refType LIKE '"+refType.getId()+"' ";
	// query += " AND NOT ( "+overlapBesides+" ) ) ";
	//
	// System.out.println("Query for OVERLAP : "+query);
	// TypedQuery<Annotation> typedQuery = em.createQuery(query, Annotation.class);
	// return typedQuery.getResultList();
	// }

	// Not necessary for the moment (@finnishgazelle, 22/10/2015)

	/*
	 * private void modifyAnnotations(int typeModif, List<int[]> positions, String refVal, String newrefVal, String refType, String newrefType) throws Exception{
	 * switch(typeModif){
	 * case MODIFY_TYPEVAL : modifyAnnotations(positions, refType, refVal, newrefType, newrefVal) ;
	 * break;
	 * case MODIFY_VAL : modifyAnnotations(positions, refVal, newrefVal);
	 * break;
	 * default: ;
	 * break;
	 * }
	 * }
	 * private void modifyAnnotations(List<int[]> positions, String refVal, String newrefVal) throws Exception{
	 * //List<Annotation> annots = getAnnotationsByRefValue(refVal);
	 * for (int[] position : positions){
	 * Annotation annot = entityManager.find(Annotation.class, position[0]+"_"+position[1]);
	 * if(annot.getReferentielVal().equals(refVal)){
	 * entityManager.getTransaction().begin();
	 * annot.setReferentielVal(newrefVal);
	 * entityManager.getTransaction().commit();
	 * }
	 * }
	 * }
	 * private void modifyAnnotations(List<int[]> positions, String refType, String refVal, String newrefType, String newrefVal) throws Exception{
	 * for (int[] position : positions){
	 * Annotation annot = entityManager.find(Annotation.class, position[0]+"_"+position[1]);
	 * if(annot.getReferentielVal().equals(refVal) && annot.getReferentielType().equals(refType)){
	 * entityManager.getTransaction().begin();
	 * annot.setReferentielVal(newrefVal);
	 * annot.setReferentielVal(newrefType);
	 * entityManager.getTransaction().commit();
	 * }
	 * }
	 * }
	 */

}
