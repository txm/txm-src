package org.txm.annotation.kr.core.repository;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 * A value defined in a Knowledge repository
 * 
 * @author mdecorde
 *
 */
@Entity
public class TypedValue implements Serializable {

	private static final long serialVersionUID = 7158271997193537962L;

	// primaryKey corresponds to the publicId and the type name
	@EmbeddedId
	private TypedValuePK PK;

	private String name;

	public TypedValue() {
	}

	// ex: standardName = "Municipalité de Lyon" publicid = "CoAc1349", type = "CoAc"
	public TypedValue(String name, String publicId, String typeId) {
		this.PK = new TypedValuePK(publicId, typeId);
		this.name = name;
	}

	protected TypedValuePK getPK() {
		return PK;
	}

	public String getId() {
		return PK.getId();
	}

	public String getTypeID() {
		return PK.getType();
	}

	public String getStandardName() {
		return name;
	}

	public String toString() {
		return PK.toString() + "(" + name + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public String toHumanString() {
		return name + "(" + PK.getId() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}
}
