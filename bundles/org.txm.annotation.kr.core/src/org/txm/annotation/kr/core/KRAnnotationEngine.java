package org.txm.annotation.kr.core;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.osgi.util.NLS;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.annotation.core.AnnotationEngine;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.annotation.kr.core.preferences.KRAnnotationPreferences;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.SQLKnowledgeRepository;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.sql.SQLConnection;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class KRAnnotationEngine extends AnnotationEngine {

	static HashMap<MainCorpus, AnnotationManager> ams = new HashMap<>();

	public static String KNOWLEDGE_ACCESS = "access"; //$NON-NLS-1$

	public static String KNOWLEDGE_STRINGS = "strings"; //$NON-NLS-1$

	public static String KNOWLEDGE_TYPEQUERIES = "queries"; //$NON-NLS-1$

	public static String KNOWLEDGE_TYPES = "fields"; //$NON-NLS-1$

	public static String KNOWLEDGE_TYPEWEBACCESS = "typeaccess"; //$NON-NLS-1$

	public static boolean canBeAnnotated(MainCorpus corpus) {
		return getKnowledgeRepositoryNames(corpus).size() > 0;
	}

	public static AnnotationManager getAnnotationManager(CQPCorpus corpus) throws Exception {

		return getAnnotationManager(corpus.getMainCorpus());
	}

	/**
	 * get the corpus Annotation Manager and Lazy load if created
	 * 
	 * @param corpus
	 * @return
	 * @throws Exception
	 */
	public static AnnotationManager getAnnotationManager(MainCorpus corpus) throws Exception {

		AnnotationManager am = ams.get(corpus);
		if (am == null) {
			am = new AnnotationManager(corpus);
			am.initialize();
			ams.put(corpus, am);
		}
		return am;
	}

	// protected static Element getKnowledgeRepositoriesElement(Element corpusElement) {
	// NodeList textsList = corpusElement.getElementsByTagName("knowledgeRepositories");
	// if (textsList.getLength() == 0) {
	// Element knowledgeRepositories = corpusElement.getOwnerDocument().createElement("knowledgeRepositories");
	// corpusElement.appendChild(knowledgeRepositories);
	// return knowledgeRepositories;
	// } else {
	// return (Element) textsList.item(0);
	// }
	// }

	/**
	 * 
	 * Lazy load KR
	 * 
	 * @param name KnowledgeRepository's name
	 * @return may return null if the KnowledgeRepository does not exist
	 * @throws BackingStoreException
	 */
	public static KnowledgeRepository getKnowledgeRepository(CQPCorpus corpus, String name) throws BackingStoreException {

		return KnowledgeRepositoryManager.getKnowledgeRepository(name, corpus.getMainCorpus());
	}

	/**
	 * Utility method to get a knowledge repository configuration
	 * 
	 * @param name the repository name
	 * @return the KR configuration map
	 */
	public static HashMap<String, HashMap<String, ?>> getKnowledgeRepositoryConfiguration(String name, Element e) {

		HashMap<String, HashMap<String, ?>> repConfiguration = new HashMap<>();

		HashMap<String, String> access = new HashMap<>();
		HashMap<String, HashMap<String, String>> strings = new HashMap<>();
		HashMap<String, HashMap<String, String>> fields = new HashMap<>();

		if (e == null) return null;

		access.put(SQLKnowledgeRepository.NAME, e.getAttribute(SQLKnowledgeRepository.NAME));
		access.put("version", e.getAttribute("version")); //$NON-NLS-1$ //$NON-NLS-2$
		access.put("mode", e.getAttribute("mode")); //$NON-NLS-1$ //$NON-NLS-2$
		access.put(SQLKnowledgeRepository.TYPE_URL, e.getAttribute(SQLKnowledgeRepository.TYPE_URL));
		access.put(SQLKnowledgeRepository.TYPE_RESURL, e.getAttribute(SQLKnowledgeRepository.TYPE_RESURL));
		access.put(SQLKnowledgeRepository.TYPE_TYPEURL, e.getAttribute(SQLKnowledgeRepository.TYPE_TYPEURL));
		access.put(SQLConnection.SQL_ADDRESS, e.getAttribute(SQLConnection.SQL_ADDRESS));
		access.put(SQLConnection.SQL_DRIVER, e.getAttribute(SQLConnection.SQL_DRIVER));
		access.put(SQLConnection.SQL_PORT, e.getAttribute(SQLConnection.SQL_PORT));
		access.put(SQLConnection.SQL_USER, e.getAttribute(SQLConnection.SQL_USER));
		access.put(SQLConnection.SQL_PASSWORD, e.getAttribute(SQLConnection.SQL_PASSWORD));

		// <type id="" name> ... </type>
		NodeList typesList = e.getElementsByTagName("type"); //$NON-NLS-1$
		for (int i = 0; i < typesList.getLength(); i++) {
			// <field type="xxx">yyy</field>
			Element typeElement = (Element) typesList.item(i);
			String type_id = "" + typeElement.getAttribute(SQLKnowledgeRepository.TYPE_ID); //$NON-NLS-1$

			String type_name = typeElement.getAttribute(SQLKnowledgeRepository.NAME);
			if (type_name == null || type_name.length() == 0) type_name = type_id;

			String type_url = "" + typeElement.getAttribute(SQLKnowledgeRepository.TYPE_URL); //$NON-NLS-1$

			String type_size = typeElement.getAttribute(SQLKnowledgeRepository.TYPE_SIZE);
			if (type_size == null || type_size.length() == 0) type_size = "SMALL"; // show all by default //$NON-NLS-1$
			type_size = type_size.toUpperCase();

			String type_effect = typeElement.getAttribute(SQLKnowledgeRepository.TYPE_EFFECT);
			if (type_effect == null || type_effect.length() == 0) type_effect = "SEGMENT"; // segment annotation by default //$NON-NLS-1$
			type_effect = type_effect.toUpperCase();

			HashMap<String, String> hashFields = new HashMap<>();
			NodeList fieldsList = typeElement.getElementsByTagName("field"); // contains KR type specific properties //$NON-NLS-1$
			for (int j = 0; j < fieldsList.getLength(); j++) {
				Element fieldElement = (Element) fieldsList.item(j);
				hashFields.put(fieldElement.getAttribute("type"), fieldElement.getTextContent()); //$NON-NLS-1$
			}

			hashFields.put(SQLKnowledgeRepository.TYPE_URL, type_url);
			hashFields.put(SQLKnowledgeRepository.NAME, type_name);
			hashFields.put(SQLKnowledgeRepository.TYPE_SIZE, type_size);
			hashFields.put(SQLKnowledgeRepository.TYPE_EFFECT, type_effect);
			fields.put(type_id, hashFields);
		}

		NodeList stringsList = e.getElementsByTagName("strings"); //$NON-NLS-1$
		for (int i = 0; i < stringsList.getLength(); i++) {
			Element stringsElement = (Element) stringsList.item(i);
			String lang = stringsElement.getAttribute("lang"); //$NON-NLS-1$
			if (lang == null) lang = "en"; // default lang is "en" //$NON-NLS-1$

			HashMap<String, String> values = new HashMap<>();
			strings.put(lang, values);

			NodeList stringList = stringsElement.getElementsByTagName("string"); //$NON-NLS-1$
			for (int j = 0; j < stringList.getLength(); j++) {
				Element stringElement = (Element) stringList.item(j);
				String key = stringElement.getAttribute("key"); //$NON-NLS-1$
				if (key != null) {
					values.put(key, stringElement.getTextContent());
				}
			}
		}

		repConfiguration.put(KNOWLEDGE_ACCESS, access);
		repConfiguration.put(KNOWLEDGE_TYPES, fields);
		repConfiguration.put(KNOWLEDGE_STRINGS, strings);

		return repConfiguration;
	}

	/**
	 * Utility method to get a knowledge repository configuration
	 * 
	 * @param name the repository name
	 * @return the KR configuration map
	 * @throws BackingStoreException
	 */
	public static HashMap<String, HashMap<String, ?>> getKnowledgeRepositoryConfiguration(String name, Preferences preference) throws BackingStoreException {
		HashMap<String, HashMap<String, ?>> repConfiguration = new HashMap<>();

		HashMap<String, String> access = new HashMap<>();
		HashMap<String, HashMap<String, String>> strings = new HashMap<>();
		HashMap<String, HashMap<String, String>> fields = new HashMap<>();

		if (preference == null) return null;

		access.put(SQLKnowledgeRepository.NAME, preference.get(SQLKnowledgeRepository.NAME, "default")); //$NON-NLS-1$
		access.put("version", preference.get("version", "0")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		access.put("mode", preference.get("mode", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		access.put(SQLKnowledgeRepository.TYPE_URL, preference.get(SQLKnowledgeRepository.TYPE_URL, "")); //$NON-NLS-1$
		access.put(SQLKnowledgeRepository.TYPE_RESURL, preference.get(SQLKnowledgeRepository.TYPE_RESURL, "")); //$NON-NLS-1$
		access.put(SQLKnowledgeRepository.TYPE_TYPEURL, preference.get(SQLKnowledgeRepository.TYPE_TYPEURL, "")); //$NON-NLS-1$
		access.put(SQLConnection.SQL_ADDRESS, preference.get(SQLConnection.SQL_ADDRESS, "")); //$NON-NLS-1$
		access.put(SQLConnection.SQL_DRIVER, preference.get(SQLConnection.SQL_DRIVER, "")); //$NON-NLS-1$
		access.put(SQLConnection.SQL_PORT, preference.get(SQLConnection.SQL_PORT, "")); //$NON-NLS-1$
		access.put(SQLConnection.SQL_USER, preference.get(SQLConnection.SQL_USER, "")); //$NON-NLS-1$
		access.put(SQLConnection.SQL_PASSWORD, preference.get(SQLConnection.SQL_PASSWORD, "")); //$NON-NLS-1$

		// <type id="" name> ... </type>
		Preferences typesList = preference.node("type"); //$NON-NLS-1$
		for (String type_id : typesList.childrenNames()) {
			// <field type="xxx">yyy</field>
			// Element typeElement = (Element)typesList.item(i);
			// String type_id = ""+typeElement.getAttribute(SQLKnowledgeRepository.TYPE_ID);

			Preferences typeElement = typesList.node(type_id);

			String type_name = typeElement.get(SQLKnowledgeRepository.NAME, type_id);

			String type_url = "" + typeElement.get(SQLKnowledgeRepository.TYPE_URL, ""); //$NON-NLS-1$ //$NON-NLS-2$

			String type_size = typeElement.get(SQLKnowledgeRepository.TYPE_SIZE, "SMALL"); //$NON-NLS-1$
			type_size = type_size.toUpperCase();

			String type_effect = typeElement.get(SQLKnowledgeRepository.TYPE_EFFECT, "SEGMENT"); //$NON-NLS-1$
			type_effect = type_effect.toUpperCase();

			HashMap<String, String> hashFields = new HashMap<>();
			Preferences fieldsList = typeElement.node("field"); // contains KR type specific properties //$NON-NLS-1$
			for (String field_type : fieldsList.keys()) {
				hashFields.put(field_type, fieldsList.get(field_type, "")); //$NON-NLS-1$
			}

			hashFields.put(SQLKnowledgeRepository.TYPE_URL, type_url);
			hashFields.put(SQLKnowledgeRepository.NAME, type_name);
			hashFields.put(SQLKnowledgeRepository.TYPE_SIZE, type_size);
			hashFields.put(SQLKnowledgeRepository.TYPE_EFFECT, type_effect);
			fields.put(type_id, hashFields);
		}

		Preferences stringsList = preference.node("strings"); //$NON-NLS-1$
		for (String lang : stringsList.childrenNames()) {
			Preferences stringsElement = stringsList.node(lang);
			// String lang = stringsElement.getAttribute("lang");
			// if (lang == null) lang = "en"; // default lang is "en"

			HashMap<String, String> values = new HashMap<>();
			strings.put(lang, values);

			for (String key : stringsElement.keys()) {
				values.put(key, stringsElement.get(key, "")); //$NON-NLS-1$
			}
		}

		repConfiguration.put(KNOWLEDGE_ACCESS, access);
		repConfiguration.put(KNOWLEDGE_TYPES, fields);
		repConfiguration.put(KNOWLEDGE_STRINGS, strings);

		return repConfiguration;
	}

	// public static Element getKnowledgeRepositoryElement(BaseOldParameters params, String name) {
	// Element rElement = getKnowledgeRepositoriesElement(params.getCorpusElement());
	// NodeList repositoriesList = rElement.getElementsByTagName("repository");
	// for (int i = 0 ; i < repositoriesList.getLength() ; i++) {
	// Element e = ((Element)repositoriesList.item(i));
	// if (name.equals(e.getAttribute("name"))) {
	// return e;
	// }
	// }
	//
	// return null;
	// }


	// /**
	// * Utility method to get a knowledge repository configuration
	// *
	// * @param params
	// * @return the repository names
	// */
	// public static List<String> getKnowledgeRepositoryNames(BaseOldParameters params) {
	// ArrayList<String> names = new ArrayList<String>();
	// Element corpusElement = params.getCorpusElement();
	// if (corpusElement == null) return names;
	//
	// Element rElement = getKnowledgeRepositoriesElement(corpusElement);
	// if (rElement == null) return names;
	//
	// NodeList repositoriesList = rElement.getElementsByTagName("repository");
	// for (int i = 0 ; i < repositoriesList.getLength() ; i++) {
	// names.add(((Element)repositoriesList.item(i)).getAttribute("name"));
	// }
	//
	// if (names.size() == 0) names.add(params.getCorpusName()); // add default KR = properties & structure properties
	// return names;
	// }

	/**
	 * 
	 * @param corpus the corpus
	 * @return the repository names the corpus is using
	 */
	public static List<String> getKnowledgeRepositoryNames(CQPCorpus corpus) {
		if (corpus == null) return new ArrayList<>();
		Project base = corpus.getProject();
		if (base == null) return new ArrayList<>();

		String[] names;
		try {
			IEclipsePreferences node = base.getPreferencesScope().getNode("KnowledgeRepository"); //$NON-NLS-1$
			if (!node.nodeExists(corpus.getID())) {
				// create the "DEFAULT" KR configuration
				Preferences krconf = node.node(corpus.getID());
				krconf.put("name", corpus.getID()); //$NON-NLS-1$
			}
			names = node.childrenNames();
			return Arrays.asList(names); // getKnowledgeRepositoryNames(params);
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	/**
	 * 
	 * @param corpus
	 * @return true if there are annotations to save for this corpus
	 */
	public static boolean needToSaveAnnotations(MainCorpus corpus) {
		AnnotationManager am = ams.get(corpus);
		if (am != null)
			return am.isDirty();
		else
			return false;
	}

	public void closeAnnotationManager(MainCorpus corpus) {
		AnnotationManager am = ams.get(corpus);
		if (am != null) {
			// System.out.println(" Closing AM...");
			am.closeAll();
		}
	}

	// public static Element createKnowledgeRepositoryElement(BaseOldParameters params, String name) {
	// Element rElement = getKnowledgeRepositoriesElement(params.getCorpusElement());
	// Element e = rElement.getOwnerDocument().createElement("repository");
	// e.setAttribute("name", name);
	// e.setAttribute("mode", "file");
	// e.setAttribute("user", "false");
	// e.setAttribute("password", "false");
	// e.setAttribute("version", "0");
	//
	// rElement.appendChild(e);
	// return e;
	// }

	@Override
	public String getName() {
		return "CQP"; //$NON-NLS-1$
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() { // nothing to load right now, the kr and am are lazy
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean[] mustLoginToKnowledgeRepository(CQPCorpus corpus, String kr_name) {
		return KnowledgeRepositoryManager.mustLoginToKnowledgeRepository(kr_name, corpus.getMainCorpus());
	}

	@Override
	public boolean start(IProgressMonitor monitor) {
		ams = new HashMap<>();
		return true;
	}

	@Override
	public boolean stop() {
		boolean success = true;
		for (KnowledgeRepository kr : KnowledgeRepositoryManager.getKnowledgeRepositories()) {
			try {
				if (kr.getJPAManager().isOpen())
					kr.getJPAManager().close();
			}
			catch (Exception e) {
				Log.warning(NLS.bind(Messages.errorCannotCloseTheP0KRP1, kr, e.getLocalizedMessage()));
				Log.printStackTrace(e);
				success = false;
			}
		}
		for (AnnotationManager am : ams.values()) {
			try {
				if (am.isOpen())
					am.closeAll();
			}
			catch (Exception e) {
				Log.warning(NLS.bind(Messages.errorCannotCloseTheP0AMP1, am, e.getLocalizedMessage()));
				Log.printStackTrace(e);
				success = false;
			}
		}
		ams.clear();
		return success;
	}

	@Override
	public void notify(TXMResult r, String state) {

		if (r instanceof MainCorpus && "before_clean".equals(state)) { //$NON-NLS-1$

			MainCorpus c = (MainCorpus) r;

			if (c.getProject() == null || c.getProject().getDoUpdate()) {
				return;
			}

			for (String krname : KRAnnotationEngine.getKnowledgeRepositoryNames(c)) {
				Log.fine("cleaning KR: " + krname); //$NON-NLS-1$
				KRAnnotationEngine.unregisterKnowledgeRepositoryName(c, krname);
			}

			if (c.getProjectDirectory() == null || !c.getProjectDirectory().exists()) {
				return;
			}

			if (ams.get(c) != null && ams.get(c).tempManager != null && ams.get(c).tempManager.getEntityManager() != null) {
				Log.fine("closing temporary annotations database of: " + c); //$NON-NLS-1$
				ams.get(c).tempManager.close(); // free files
			}

			File buildDirectory = new File(c.getProjectDirectory(), "temporary_annotations/" + c.getID()); //$NON-NLS-1$
			if (buildDirectory.exists()) {
				DeleteDir.deleteDirectory(buildDirectory);
			}
		}
		else if (r instanceof MainCorpus && "clean".equals(state)) { //$NON-NLS-1$

			MainCorpus c = (MainCorpus) r;

			if (c.getProject() == null || c.getProject().getDoUpdate()) {
				return;
			}

			for (String krname : KRAnnotationEngine.getKnowledgeRepositoryNames(c)) {
				Log.fine("cleaning KR: " + krname); //$NON-NLS-1$
				KRAnnotationEngine.unregisterKnowledgeRepositoryName(c, krname);
			}



			if (c.getProjectDirectory() == null || !c.getProjectDirectory().exists()) {
				return;
			}

			if (ams.get(c) != null && ams.get(c).tempManager != null && ams.get(c).tempManager.getEntityManager() != null) {
				Log.fine("closing temporary annotations databse of: " + c); //$NON-NLS-1$
				ams.get(c).tempManager.close(); // free files
			}

			File buildDirectory = new File(c.getProjectDirectory(), "temporary_annotations/" + c.getID()); //$NON-NLS-1$
			if (buildDirectory.exists()) {
				DeleteDir.deleteDirectory(buildDirectory);
			}
		}
		else if (r instanceof Project && "clean".equals(state)) { //$NON-NLS-1$

			Project p = (Project) r;
			for (MainCorpus c : p.getChildren(MainCorpus.class)) { // if any MainCorpus is remaining, clean it
				for (String krname : KRAnnotationEngine.getKnowledgeRepositoryNames(c)) {
					Log.fine("cleaning KR: " + krname); //$NON-NLS-1$
					KRAnnotationEngine.unregisterKnowledgeRepositoryName(c, krname);
				}
			}
			File buildDirectory = new File(p.getProjectDirectory(), "temporary_annotations"); //$NON-NLS-1$
			if (buildDirectory.exists()) {
				DeleteDir.deleteDirectory(buildDirectory);
			}
		}
	}

	@Override
	public String getDetails() {
		return ""; //$NON-NLS-1$
	}

	public static void registerKnowledgeRepositoryName(CQPCorpus corpus, String name) {
		if (corpus == null) return;
		Project project = corpus.getProject();
		if (project == null) return;
		try {
			IEclipsePreferences node = project.getPreferencesScope().getNode("KnowledgeRepository"); //$NON-NLS-1$
			if (!node.nodeExists(name)) {
				// create the "DEFAULT" KR configuration
				Preferences krconf = node.node(name);
				krconf.put("name", name); //$NON-NLS-1$
			}
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}

	public static void unregisterKnowledgeRepositoryName(CQPCorpus corpus, String name) {
		if (corpus == null) return;
		Project project = corpus.getProject();
		if (project == null) return;
		try {
			IEclipsePreferences node = project.getPreferencesScope().getNode("KnowledgeRepository"); //$NON-NLS-1$
			if (node.nodeExists(name)) {
				Preferences krconf = node.node(name);
				krconf.removeNode();
			}

			KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(name);
			if (kr != null) {
				Log.fine("Deleting KR database: " + kr); //$NON-NLS-1$
				KnowledgeRepositoryManager.deleteKnowledgeRepository(kr);
			}
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean hasAnnotationsToSave(CorpusBuild corpus) {

		if (KRAnnotationPreferences.getInstance().getBoolean(KRAnnotationPreferences.PRESERVE_ANNOTATIONS)) return false;

		if (!ams.containsKey(corpus)) return false; // no annotation manager -> not annotations to save
		return ams.get(corpus).hasChanges();
	}

	@Override
	public String hasAdditionalDetailsForResult(TXMResult result) {
		if (result instanceof MainCorpus corpus) {

			if (!ams.containsKey(corpus)) return null; // no annotation manager -> not annotations to save
			return Messages.cqpAnnotations;
		}
		return null;
	}

	@Override
	public String getAdditionalDetailsForResult(TXMResult result) {
		StringBuffer buffer = new StringBuffer();
		if (result instanceof MainCorpus corpus) {

			if (!ams.containsKey(corpus)) return ""; // no annotation manager -> not annotations to save
			AnnotationManager am = ams.get(corpus);
			List<String> krnames = getKnowledgeRepositoryNames(corpus);
			for (String krname : krnames) {
				try {
					KnowledgeRepository kr = getKnowledgeRepository(corpus, krname);
					buffer.append(Messages.annotatedProperties+" <ul><li>" + StringUtils.join(kr.getAllAnnotationTypes(), "<br><li>"));

					if (kr.getAllAnnotationTypes().size() > 0) {
						buffer.append("</ul>"+Messages.annotations+"<ul>"); //$NON-NLS-1$
						int size = buffer.length();
						for (AnnotationType type : kr.getAllAnnotationTypes()) {

							List<Annotation> annotations = am.getTemporaryManager().getAnnotations(type);
							if (annotations.size() == 0) continue; // next ! nothing to print
							buffer.append("<br><li>" + type + "(" + annotations.size() + "): "); //$NON-NLS-1$
							int n = 10;
							for (Annotation annotation : annotations) {
								if (n != 10) buffer.append(", "); //$NON-NLS-1$
								buffer.append(annotation.toString());
								n--;
								if (n == 0 && annotations.size() > 10) {
									buffer.append("..."); //$NON-NLS-1$
									break;
								}
							}
							buffer.append("</li><br>"); //$NON-NLS-1$
						}
						if (size == buffer.length()) {
							buffer.append(Messages.noAnnotationToSave+"<br>"); //$NON-NLS-1$
						}
					}

					buffer.append("</ul></li><br>"); //$NON-NLS-1$
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return buffer.toString();
	}
}
