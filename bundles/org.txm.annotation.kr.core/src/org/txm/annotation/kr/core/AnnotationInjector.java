package org.txm.annotation.kr.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.importer.StaxIdentityParser;
import org.txm.importer.ValidateXml;
import org.txm.utils.logger.Log;

/**
 * The Class AnnotationInjection.
 *
 * @author mdecorde
 *
 *         inject annotation from a stand-off file into a xml-tei-txm file "id"
 */

public class AnnotationInjector extends StaxIdentityParser {

	File xmlFile;

	HashMap<Integer, List<Annotation>> annotationsToAddByStartPos; // contains  annotation to write

	HashMap<Integer, List<Annotation>> annotationsToAddByEndPos;

	List<Annotation> currentStartAnnotations;

	List<Annotation> currentEndAnnotations;

	LinkedHashMap<String, Annotation> currentTokenAnnotations = new LinkedHashMap<>();

	int n = 0;

	boolean debug = false;

	String data; // the word id properties to add/replace

	String newform = null;

	String wordId;

	boolean inW = false;

	String anaType;

	int position_counter = 0;

	String closeNext = null; // the next close tag to delete

	ArrayList<String> openedElements = new ArrayList<>(); // to ensure to
	// delete the
	// right element

	private List<Annotation> previousEndAnnotations;

	private ArrayList<Annotation> tokenAnnotations;

	String user;

	/**
	 * 
	 * @param xmlFile
	 * @param tokenAnnotations
	 * @param corpus_start_position
	 * @param debug
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public AnnotationInjector(File xmlFile, List<Annotation> segmentAnnotations, ArrayList<Annotation> tokenAnnotations, int corpus_start_position, boolean debug)
			throws IOException, XMLStreamException {
		super(xmlFile.toURI().toURL()); // init reader and writer

		this.user = System.getProperty("user.name"); //$NON-NLS-1$

		//this.debug = debug;
		this.xmlFile = xmlFile;
		this.n = 0;
		// println ""+records.size()+" lines to process..."
		this.position_counter = corpus_start_position;
		factory = XMLInputFactory.newInstance();

		// preparing annotations to being written in the right inclusion order
		annotationsToAddByStartPos = new HashMap<>();
		annotationsToAddByEndPos = new HashMap<>();

		for (Annotation a : segmentAnnotations) {

			if (!annotationsToAddByStartPos.containsKey(a.getStart()))
				annotationsToAddByStartPos.put(a.getStart(), new ArrayList<Annotation>());

			if (!annotationsToAddByEndPos.containsKey(a.getEnd()))
				annotationsToAddByEndPos.put(a.getEnd(), new ArrayList<Annotation>());

			annotationsToAddByStartPos.get(a.getStart()).add(a);
			annotationsToAddByEndPos.get(a.getEnd()).add(a);
		}

		for (int i : annotationsToAddByStartPos.keySet()) {
			List<Annotation> a = annotationsToAddByStartPos.get(i);
			Collections.sort(a, new Comparator<Annotation>() { // reverse sort
				// annotation -> write the smaller in the bigger

				@Override
				public int compare(Annotation arg0, Annotation arg1) {
					return arg1.getEnd() - arg0.getEnd();
				}
			});
		}
		for (int i : annotationsToAddByEndPos.keySet()) {
			List<Annotation> a = annotationsToAddByEndPos.get(i);
			Collections.sort(a, new Comparator<Annotation>() { // reverse sort
				// annotation -> write the smaller in the bigger

				@Override
				public int compare(Annotation arg0, Annotation arg1) {
					return arg1.getStart() - arg0.getStart();
				}
			});
		}

		// sorting token annotations
		this.tokenAnnotations = tokenAnnotations;
		Collections.sort(tokenAnnotations, new Comparator<Annotation>() {

			@Override
			public int compare(Annotation arg0, Annotation arg1) {
				return arg0.getStart() - arg1.getStart();
			}
		});

		if (debug) {
			Log.finer("annotations for " + xmlFile); //$NON-NLS-1$
			Log.finer(" segment grouped and ordered by start position: " + annotationsToAddByStartPos); //$NON-NLS-1$
			Log.finer(" segment grouped and ordered by end position: " + annotationsToAddByEndPos); //$NON-NLS-1$
			Log.finer(" token ordered by start position: " + tokenAnnotations); //$NON-NLS-1$
		}
	}

	boolean mustChangeAnaValue = false;

	boolean inAna = false;

	private boolean inForm;

	private boolean mustChangeWordValue;

	@Override
	protected void processStartElement() throws XMLStreamException, IOException {

		currentStartAnnotations = annotationsToAddByStartPos.get(position_counter);
		currentEndAnnotations = annotationsToAddByEndPos.get(position_counter);
		// System.out.println("A Starts: "+currentStartAnnotations+" "+currentEndAnnotations);
		if (debug) System.out.println("----- " + localname + "@" + position_counter + " START ANNOT = " + currentStartAnnotations.size()); //$NON-NLS-1$
		if ("teiHeader".equals(localname)) { //$NON-NLS-1$
			super.processStartElement();
			goToEnd("teiHeader"); //$NON-NLS-1$ // skip teiHeader // will be closed in processEndElement()
		}
		else if ("w".equals(localname)) { //$NON-NLS-1$
			//if (debug) System.out.println("W START pos="+position_counter+" annots="+currentStartAnnotations);
			if (currentStartAnnotations != null) {// there are still annotations
				// to write, possible  several for a position
				//if (debug) System.out.println("WRITING START at "+position_counter+": "+currentStartAnnotations);
				for (Annotation a : currentStartAnnotations) {
					if (!"#del".equals(a.getValue())) { //$NON-NLS-1$
						if (debug) {
							Log.finer(" force write start annotation " + a); //$NON-NLS-1$
						}
						writeStartAnnotation(a);
					}
					else {
						if (debug)
							Log.finer(" no need to write start annotation " + a); //$NON-NLS-1$
						currentEndAnnotations = annotationsToAddByEndPos.get(a.getEnd());
						// System.out.println(" shall we need to change annotation attribute : "+a.getValue()+
						// " | currentEndAnnotations : "+currentEndAnnotations);
						if (currentEndAnnotations == null) {
							Log.finer("WARNING ERROR null pointer for end position of annotation " + a); //$NON-NLS-1$
						}
						else {
							currentEndAnnotations.remove(a);
						}
					}
				}
			}
			inW = true;

			// get token annotation if any
			currentTokenAnnotations.clear(); // current word annotations to write, may be empty
			Annotation a = null;

			ArrayList<Annotation> toRemove = new ArrayList<>();
			for (Annotation annot : tokenAnnotations) {
				a = annot;
				// System.out.println("for p="+position_counter+" next token annot="+a);

				if (a.getStart() <= position_counter && position_counter <= a.getEnd()) {
					// write the annotation
					currentTokenAnnotations.put(a.getType(), a);
					if (a.getEnd() == position_counter) { // its the last word that needs to write this annotation
						toRemove.add(annot);
					}
				}
			}
			tokenAnnotations.removeAll(toRemove);

			super.processStartElement(); // write the tag

		}
		else if ("form".equals(localname) && inW) { //$NON-NLS-1$
			if (currentTokenAnnotations.containsKey("word")) { //$NON-NLS-1$
				Annotation a = currentTokenAnnotations.get("word"); //$NON-NLS-1$
				// System.out.println("Updating token annotation with: " + a);

				String value = a.getValue();
				if ("#del".equals(value)) { //$NON-NLS-1$
					value = ""; //$NON-NLS-1$
				}
				if (value == null) {
					value = "ERROR"; //$NON-NLS-1$
				}
				try {
					writer.writeStartElement(TXMNS, "form"); //$NON-NLS-1$
					writer.writeCharacters(value);
					mustChangeWordValue = true;
				}
				catch (XMLStreamException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				currentTokenAnnotations.remove("word"); // annotation updated //$NON-NLS-1$
			}
			else {
				super.processStartElement();
			}
		}
		else if ("ana".equals(localname) && inW) { //$NON-NLS-1$
			inAna = true;
			String type = parser.getAttributeValue(null, "type").substring(1); //$NON-NLS-1$
			if (currentTokenAnnotations.containsKey(type)) {
				Annotation a = currentTokenAnnotations.get(type);
				// System.out.println("Updating token annotation with: " + a);

				String value = a.getValue();
				if ("#del".equals(value)) { //$NON-NLS-1$
					value = ""; //$NON-NLS-1$
				}
				if (value == null) {
					value = "ERROR"; //$NON-NLS-1$
				}

				try {
					writer.writeStartElement(TXMNS, "ana"); //$NON-NLS-1$
					writer.writeAttribute("type", "#" + type); //$NON-NLS-1$ //$NON-NLS-2$
					writer.writeAttribute("resp", "#" + a.getAnnotator()); // change //$NON-NLS-1$ //$NON-NLS-2$
					// resp
					writer.writeCharacters(value);
					mustChangeAnaValue = true;
				}
				catch (XMLStreamException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				currentTokenAnnotations.remove(type); // annotation updated
			}
			else {
				super.processStartElement();
			}
		}
		else if (currentStartAnnotations != null) { // is their any annotations to write ?
			// System.out.println("A START="+localname);

			String s_end = parser.getAttributeValue(null, "end"); //$NON-NLS-1$
			String s_start = parser.getAttributeValue(null, "start"); //$NON-NLS-1$
			String s_author = parser.getAttributeValue(null, "author"); //$NON-NLS-1$
			String s_ref = parser.getAttributeValue(null, "ref"); //$NON-NLS-1$

			boolean toDelete = false;

			int start = -1;
			int end = -1;
			try {
				start = Integer.parseInt(s_start);
				end = Integer.parseInt(s_end);
			}
			catch (Exception e) {

			}

			if (s_end == null || s_end.length() == 0 || s_start == null || s_start.length() == 0 || start < 0 || end < 0) {
				// this is not an annotation
			}
			else {

				for (int i = 0; i < currentStartAnnotations.size(); i++) {
					Annotation a = currentStartAnnotations.get(i);
					// if (debug) System.out.println("=============== Start annot : "+a);
					if (a.getType().equals(localname)) { // force delete, will be  written in the 'w'  case

						if (a.getStart() == start && a.getEnd() == end) { // this is the right annotation to process
							// updated = true;
							// if (debug)
							// System.out.println(" found existing annotation "+a);
							if ("#del".equals(a.getValue())) { //$NON-NLS-1$
								toDelete = true;
								// currentEndAnnotations.remove(a); // MAYBE NOT TO
								// DO THIS HERE ?
								if (debug) {
									System.out.println(" force delete start annotation " + a); //$NON-NLS-1$
								}
							}
							else { // update existing annotation, no need to store the end of annotation
								writeStartAnnotation(a);
								toDelete = true; // set true to avoid re-writing the annotation XML start element
								if (debug) {
									Log.finer(" update annotation " + a); //$NON-NLS-1$
								}
								currentEndAnnotations = annotationsToAddByEndPos.get(a.getEnd());// EL
								// NO
								// CAPITO
								// YET
								// !!
								// System.out.println(" shall we need to change annotation attribute : "+a.getValue()+
								// " | currentEndAnnotations : "+currentEndAnnotations);
								if (currentEndAnnotations == null) {
									Log.warning(NLS.bind(Messages.ErrorNPEForEndPositionOfAnnotationP0, a));
								}
								else {
									currentEndAnnotations.remove(a);
								}
							}

							currentStartAnnotations.remove(i);
							i--;

							break; // no need to continue
						}
					}
					else {
						//						if (debug) System.out.println("------- with same start pos");
						//						
						//						if (s_start != null && s_end != null && s_author != null && s_ref != null) {
						//							if (a.getEnd() >= end) { // must write a
						//								if (!"#del".equals(a.getValue())) { //$NON-NLS-1$
						//									if (debug) {
						//										System.out.println(NLS.bind(" writing of start annotation {0}.", a)); //$NON-NLS-1$
						//									}
						//									writeStartAnnotation(a);
						//									
						//								}
						//								else {
						//									toDelete = true;
						//									if (debug) {
						//										System.out.println(NLS.bind(" no writing of start annotation {0}.", a)); //$NON-NLS-1$
						//									}
						//									currentEndAnnotations = annotationsToAddByEndPos.get(a.getEnd());
						//									// System.out.println(" shall we need to change annotation attribute : "+a.getValue()+
						//									// " | currentEndAnnotations : "+currentEndAnnotations);
						//									if (currentEndAnnotations == null) {
						//										Log.warning(NLS.bind(Messages.ErrorNPEForEndPositionOfAnnotationP0, a));
						//									}
						//									else {
						//										currentEndAnnotations.remove(a);
						//									}
						//								}
						//								currentStartAnnotations.remove(i);
						//								i--;
						//								// break;
						//							}
						//						}
					}
				}
			}

			if (!toDelete) { // the structure or annotation was not modified
				super.processStartElement();
			}
		}
		else {
			// System.out.println("X START="+localname);
			super.processStartElement();
		}
	}

	@Override
	public void processCharacters() throws XMLStreamException {
		// System.out.println("processCharaters inAna="+inAna+" mustChangeAnaValue="+mustChangeAnaValue);
		if (inAna && mustChangeAnaValue) {
			// nothing content is already written
			// System.out.println("skip ana value because we replace it");
		}
		else if (inW && mustChangeWordValue) {
			// nothing content is already written
			// System.out.println("skip form value because we replace it");
		}
		else {
			super.processCharacters();
		}
	}

	@Override
	protected void processEndElement() throws XMLStreamException {
		// currentEndAnnotations =
		// annotationsToAddByEndPos.get(position_counter); // annotation to end
		previousEndAnnotations = annotationsToAddByEndPos.get(position_counter - 1); // existing
		// (or
		// not)
		// annotation
		// that
		// have
		// already
		// been
		// closed
		// if (debug)
		// System.out.println("----- "+localname+"@"+position_counter+" END ANNOT = "+currentEndAnnotations);

		if ("w".equals(localname)) { //$NON-NLS-1$
			// System.out.println("W END");

			for (String type : currentTokenAnnotations.keySet()) {
				try {
					writer.writeStartElement(TXMNS, "ana"); //$NON-NLS-1$
					writer.writeAttribute("type", "#" + type); //$NON-NLS-1$ //$NON-NLS-2$
					writer.writeAttribute("resp", "#" + currentTokenAnnotations.get(type).getAnnotator()); // change //$NON-NLS-1$ //$NON-NLS-2$
					// resp
					writer.writeCharacters(currentTokenAnnotations.get(type).getValue());
					writer.writeEndElement();
				}
				catch (XMLStreamException e) {
					e.printStackTrace();
				}
			}

			super.processEndElement(); // write word then close annotations
			inW = false;
			// force write All known annotation after the word
			if (currentEndAnnotations != null) {// there are still annotations
				// to write, possible several
				// for a position
				// if (debug)
				// System.out.println("WRITING END at "+position_counter+": "+currentEndAnnotations);
				for (Annotation a : currentEndAnnotations) {
					// if (debug)
					// System.out.println("=============== End annot : "+a);
					if (!"#del".equals(a.getValue())) { //$NON-NLS-1$
						if (debug) {
							Log.finer(" force write end annotation " + a); //$NON-NLS-1$
						}
						writeEndAnnotation(a);
					} /*
						 * else { if (debug)
						 * System.out.println(" no need to write end annotation " // $NON-NLS-1$
						 * +a); }
						 */

				}
			}

			position_counter++;
		}
		else if ("form".equals(localname) && inW) { //$NON-NLS-1$
			// if (!mustChangeAnaValue)
			super.processEndElement();
			mustChangeWordValue = false;
			inForm = false;
		}
		else if ("ana".equals(localname) && inW) { //$NON-NLS-1$
			// if (!mustChangeAnaValue)
			super.processEndElement();
			inAna = false;
			mustChangeAnaValue = false;
		}
		else if (previousEndAnnotations != null) { // force delete annotations
			// previously written in
			// the "w" case
			// System.out.println("previousEndAnnotations !!!!! "+previousEndAnnotations);
			boolean toDelete = false;
			for (int i = 0; i < previousEndAnnotations.size(); i++) {
				Annotation a = previousEndAnnotations.get(i);
				if (a.getType().equals(localname)) { // update the annotation
					// if (debug)
					// System.out.println(" found existing end annotation "+a);

					if ("#del".equals(a.getValue())) { // if //$NON-NLS-1$
						// (!"#del".equals(a.getValue()))
						// {
						toDelete = true;
						if (debug) {
							Log.finer(" force delete end annotation " + a); //$NON-NLS-1$
						}
					}

					previousEndAnnotations.remove(i);
					i--;
					break; // no need to continue
				}
			}

			if (!toDelete) { // the structure or annotation was not modified
				super.processEndElement();
			}
			else {
				// if (debug)
				// System.out.println("DELETING END at "+position_counter+" localname="+localname+": "+previousEndAnnotations);
			}
		}
		else {
			// System.out.println("X END="+localname);
			super.processEndElement();
		}
	}

	private void writeStartAnnotation(Annotation a) {
		try {
			writer.writeStartElement("txm:" + a.getType()); //$NON-NLS-1$
			writer.writeAttribute("author", "" + a.getAnnotator()); //$NON-NLS-1$ //$NON-NLS-2$
			writer.writeAttribute("ref", a.getValue()); //$NON-NLS-1$
			writer.writeAttribute("date", Toolbox.dateformat.format(new Date())); //$NON-NLS-1$
			writer.writeAttribute("start", Integer.toString(a.getStart())); //$NON-NLS-1$
			writer.writeAttribute("end", Integer.toString(a.getEnd())); //$NON-NLS-1$
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void writeEndAnnotation(Annotation a) {
		try {
			writer.writeEndElement();
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//////////// TESTS ///////////

	public static ArrayList<Annotation> testAdding() {

		Annotation a1 = new Annotation("Actr", "Actr100", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		return annotations;
	}

	public static ArrayList<Annotation> testAddingInclusive() {

		Annotation a1 = new Annotation("CoAc", "CoAc2093", 15, 19); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		return annotations;
	}

	public static ArrayList<Annotation> testAddingInclusive2() {

		Annotation a1 = new Annotation("Actr", "Actr100", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdating() {

		Annotation a1 = new Annotation("Actr", "Actr200", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingDeleting() {
		Annotation a1 = new Annotation("Actr", "Actr200", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("CoAc", "#del", 15, 19); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		return annotations;
	}

	public static ArrayList<Annotation> testAddDeleting2() {
		Annotation a1 = new Annotation("Actr", "#del", 15, 16); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("NaPl", "NaPl14554", 15, 19); // ADD //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingDeletingAdding() {
		Annotation a1 = new Annotation("Actr", "Actr200", 15, 16); // UPDATE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("NaPl", "NaPl14554", 15, 19); // ADD //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a3 = new Annotation("CoAc", "#del", 15, 19);	// DELETE //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		annotations.add(a3);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingDeletingAdding2() {	// NOT WORKING
		Annotation a1 = new Annotation("Actr", "#del", 15, 16); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("NaPl", "NaPl14554", 15, 19); // ADD same place as CoAc //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a3 = new Annotation("CoAc", "CoAc2091", 15, 19); // UPDATE //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		annotations.add(a3);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingDeletingAdding3() {	// NOT WORKING
		Annotation a1 = new Annotation("Actr", "#del", 15, 16); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("NaPl", "NaPl14554", 15, 16); // ADD same place as Actr //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a3 = new Annotation("CoAc", "CoAc2091", 15, 19); // UPDATE //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		annotations.add(a3);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingDeletingAdding4() {
		Annotation a1 = new Annotation("Actr", "#del", 15, 16); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("NaPl", "NaPl14554", 13, 20); // ADD Outside Actr and CoAc //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a3 = new Annotation("CoAc", "CoAc2091", 15, 19); // UPDATE //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		annotations.add(a3);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingDeletingAdding5() {
		Annotation a1 = new Annotation("Actr", "#del", 15, 16); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("NaPl", "NaPl14554", 15, 16); // ADD //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a3 = new Annotation("CoAc", "#del", 15, 19); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		annotations.add(a3);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingDeletingAdding6() {
		Annotation a1 = new Annotation("Actr", "#del", 15, 16); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("NaPl", "NaPl14554", 15, 16); // ADD //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a3 = new Annotation("CoAc", "#del", 15, 19); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a4 = new Annotation("CoAc", "CoAc1", 15, 18); // DELETE //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		annotations.add(a3);
		annotations.add(a4);
		return annotations;
	}

	public static ArrayList<Annotation> testUpdatingInclusive() {
		Annotation a1 = new Annotation("Actr", "Actr200", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("CoAc", "CoAc321", 15, 19); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		return annotations;
	}

	public static ArrayList<Annotation> testDeleting() {

		Annotation a1 = new Annotation("Actr", "#del", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		return annotations;
	}

	public static ArrayList<Annotation> testDeletingInclusive() {

		Annotation a1 = new Annotation("Actr", "#del", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		Annotation a2 = new Annotation("CoAc", "#del", 15, 16); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<Annotation> annotations = new ArrayList<>();
		annotations.add(a1);
		annotations.add(a2);
		return annotations;
	}

	static int ADD = 1;

	static int UPDATE = 2;

	static int DELETE = 3;

	static int ADD2 = 4;

	static int UPDATE2 = 5;

	static int DELETE2 = 6;

	static int DELETE3 = 7;

	static int DELETE4 = 8;

	static int ADD3 = 9;

	static int UPDATE3 = 10;

	static int UPDATEDELETE = 11;

	static int UPDATEDELETEADD = 12;

	static int UPDATEDELETEADD2 = 13;

	static int UPDATEDELETEADD3 = 14;

	static int UPDATEDELETEADD4 = 15;

	static int UPDATEDELETEADD5 = 16;

	static int UPDATEDELETEADD6 = 17;

	static int ADDDELETE2 = 18;

	public static void main2(String args[]) throws IOException, XMLStreamException {
		File xmlFile = null;
		int corpus_start_position = 10;

		ArrayList<Annotation> annotations = null;
		int test = UPDATEDELETEADD3;// AnnotationWriter.UPDATE;// AnnotationWriter.ADD;
		switch (test) {
			case 1:
				// AnnotationWriter.ADD
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testAdding(); /// OK
				break;
			case 2:
				// AnnotationWriter.UPDATE
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST2.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdating(); /// OK
				break;
			case 3:
				// AnnotationWriter.DELETE
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST4.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testDeleting(); /// OK
				break;
			case 4:
				// AnnotationWriter.ADD2
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST3.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testAddingInclusive(); /// OK
				break;
			case 5:
				// AnnotationWriter.UPDATE2
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdating(); /// OK
				break;
			case 6:
				// AnnotationWriter.DELETE2
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST6.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testDeleting();
				break;
			case 7:
				// AnnotationWriter.DELETE3
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST6.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testDeletingInclusive();
				break;
			case 8:
				// AnnotationWriter.DELETE4
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST7.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testDeletingInclusive();
				break;
			case 9:
				// AnnotationWriter.ADD3
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST3bis.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testAddingInclusive2(); /// OK
				break;
			case 10:
				// AnnotationWriter.UPDATE3
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingInclusive(); /// OK
				break;
			case 11:
				// AnnotationWriter.UPDATEDELETE
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingDeleting(); /// OK
				break;
			case 12:
				// AnnotationWriter.UPDATEDELETEADD
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingDeletingAdding(); /// NOT OK
				break;
			case 13:
				// AnnotationWriter.UPDATEDELETEADD2
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingDeletingAdding2(); /// OK
				break;
			case 14:
				// AnnotationWriter.UPDATEDELETEADD3
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingDeletingAdding3(); /// OK
				break;
			case 15:
				// AnnotationWriter.UPDATEDELETEADD4 -- add 1 annotation over the 2 others
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingDeletingAdding4(); /// OK
				break;
			case 16:
				// AnnotationWriter.UPDATEDELETEADD5 -- delete 2 annotations, add 1
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingDeletingAdding5(); /// OK
				break;
			case 17:
				// AnnotationWriter.UPDATEDELETEADD6 -- delete 2 annotations, add 2
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testUpdatingDeletingAdding6(); /// NOT OK
				break;
			case 18:
				// AnnotationWriter.ADDDELETE -- delete 1, add 1
				xmlFile = new File(System.getProperty("user.home"), "TXM/corpora/TEST/TEST5.xml"); //$NON-NLS-1$ //$NON-NLS-2$
				annotations = testAddDeleting2(); /// OK
				break;
			//
			default:
				break;
		}

		// no token annotations
		AnnotationInjector annotationInjector = new AnnotationInjector(xmlFile, annotations, new ArrayList<Annotation>(), corpus_start_position, true);
		File outfile = new File(xmlFile.getParentFile(), "result-" + xmlFile.getName()); //$NON-NLS-1$
		if (annotationInjector.process(outfile)) {
			if (!ValidateXml.test(outfile)) {
				System.out.println("FAIL"); //$NON-NLS-1$
			}
			else {
				System.out.println("SUCCESS ??"); //$NON-NLS-1$
			}
		}

	}

	public static void main(String[] args) {
		try {
			File xmlFile = new File(System.getProperty("user.home"), "TXM-0.8.x/corpora/XMLLINEBREAK/txm/XMLLINEBREAK/test.xml"); //$NON-NLS-1$ //$NON-NLS-2$
			File outfile = new File(System.getProperty("user.home"), "TXM-0.8.x/corpora/XMLLINEBREAK/txm/XMLLINEBREAK/test-out.xml"); //$NON-NLS-1$ //$NON-NLS-2$

			ArrayList<Annotation> segmentAnnotations = new ArrayList<>();
			// int starts[] = { 3, 6 };
			// int ends[] = { 5, 8 };
			// for (int i = 0; i < starts.length; i++) {
			// segmentAnnotations.add(new Annotation("UN", "un", starts[i], ends[i]));
			// segmentAnnotations.add(new Annotation("DEUX", "deux", starts[i], ends[i]));
			// }

			ArrayList<Annotation> tokenAnnotations = new ArrayList<>();
			int positions[] = { 1, 2 };
			int n = 1;
			for (int p : positions) {
				tokenAnnotations.add(new Annotation("frlemma", "" + (n++), p, p)); //$NON-NLS-1$ //$NON-NLS-2$
				tokenAnnotations.add(new Annotation("frpos", "" + (n++), p, p)); //$NON-NLS-1$ //$NON-NLS-2$
			}

			AnnotationInjector ai;

			ai = new AnnotationInjector(xmlFile, segmentAnnotations, tokenAnnotations, 0, true);
			System.out.println("start processing..."); //$NON-NLS-1$
			ai.process(outfile);
			System.out.println("Done."); //$NON-NLS-1$
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
