package org.txm.annotation.kr.core;


public enum AnnotationScope {

	FIRSTTARGET("first|target"), FIRST("first"), TARGET("target"), ALL("all"); // $NON-NLS-1$

	String label;

	AnnotationScope(String name) {
		this.label = name;
	}

	public String label() {
		return label;
	}

	public final static AnnotationScope fromLabel(String l) {
		if ("first|target".equals(l)) { // $NON-NLS-1$
			return FIRSTTARGET;
		}
		else if ("first".equals(l)) { // $NON-NLS-1$
			return FIRST;
		}
		else if ("target".equals(l)) { // $NON-NLS-1$
			return TARGET;
		}
		else {
			return ALL;
		}
	}
}
