package org.txm.annotation.kr.core.messages;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;


/**
 * KR annotation Core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class Messages extends TXMCoreMessages {

	private static final String BUNDLE_NAME = "org.txm.annotation.kr.core.messages.messages"; //$NON-NLS-1$

	public static String AnnotationsSavedInP0;

	public static String KRIsNotConnectedCheckingCredentialRequirement;

	public static String WarningDuplicatedP0ValueInP1OfTheP2KRIgnoringTheValue;

	public static String WarningUnknownTypeP0;

	public static String WritingAnnotationsXMLFilesInP0;

	public static String ErrorCannotFoundAXMLTXMFileForTextWithTheP0Id;

	public static String ErrorCouldNotCreateDirectoryP0;

	public static String ErrorCouldNotCreateWriteTemporaryDirectoryP0;

	public static String ErrorCouldNotReplaceXMLTXMFileP0WithP1;

	public static String ErrorNPEForEndPositionOfAnnotationP0;

	public static String ErrorResultFileP0IsMalformed;

	public static String ErrorTheP0ResultFileIsMalformed;

	public static String ErrorWhileLoadingTypesFromFileP0;

	public static String ErrorWhileProcessingP0InTempDir;

	public static String ErrorWhileProcessingTheP0FileInStandOffDir;

	public static String ErrorWhileWritingAnnotationsOfTextInP0;

	public static String ErrorWhileWritingAnnotationsOfTextP0;

	public static String ErrorWithCurrentWriterP0;

	public static String ExportingAnnotationsOfTheP0Texts;

	public static String savingP0Annotations;
	
	public static String savingP0AnnotationsByP1;

	public static String affectAnnotationCanceled;

	public static String annotationSuccesfullyWritten;

	public static String savingAnnotations;

	public static String annotationSuccesfullyWrittenIntheP0File;

	public static String ConversionRulesP0;

	public static String CouldNotReplaceOriginalFileWithTheP0ResultFileP1;

	public static String deleteAnnotationCanceled;

	public static String errorAnnotationFileHeaderFormatIsNotWekkfomattedP0ItShouldBeP1;

	public static String errorCannotCloseTheP0AMP1;

	public static String errorCannotCloseTheP0KRP1;

	public static String errorDuringAnnotationCreationP0;

	public static String errorNoAnnotationLineFound;

	public static String errorNoKRFoundInP0Corpus;

	public static String errorNoKRP0FoundInP1Corpus;

	public static String exportingP0Annotations;

	public static String failToClearTheAnnotationManagerInstanceP0;

	public static String FailToProcessTheP0XMLFile;

	public static String MoreErrorsSeeTheP0ErrorFile;

	public static String NoFileFoundInTheP0Directory;

	public static String SomeValuesDidNotMatchRule;

	public static String textP0;

	public static String TheP0CorpusDirectoryWasNotFoundInP1;

	public static String ThxTXMDirectoryWasNotFoundInP0;

	public static String FailToGetSQLConnectionWithTheP0PropertiesErrorP1;

	public static String importingP0Annotations;

	public static String noAnnotationToSaveAborting;

	public static String noMatchFoundIdP0InTextP1;

	public static String warningNoAnnotationsToExportAborting;

	public static String writingAnnotationsInXMLTXMFiles;

	public static String cqpAnnotations;

	public static String annotatedProperties;

	public static String annotations;

	public static String noAnnotationToSave;

	public static String unknownRepositoryTypeP0;

	public static String errorKRNotCreatedP0;

	public static String failToLoadTheP0KRFromP1P2;

	public static String errorwhileGeneratingTheHTMLContentOfAnnotationIDP1andErrorP0;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {

	}
}
