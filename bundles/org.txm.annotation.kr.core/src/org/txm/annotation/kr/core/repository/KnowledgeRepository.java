package org.txm.annotation.kr.core.repository;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.eclipse.osgi.util.NLS;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.jpa.PersistenceProvider;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.DatabasePersistenceManager;
import org.txm.utils.logger.Log;

public abstract class KnowledgeRepository { // extends Vector<AnnotationType>

	protected String values_base_url = ""; //$NON-NLS-1$

	protected String types_base_url = ""; //$NON-NLS-1$

	protected String url = ""; //$NON-NLS-1$

	protected String dbPath = ""; // the KR database path //$NON-NLS-1$

	protected String name;

	protected int version = 0;

	protected boolean loginNeeded = false;

	protected boolean passwordNeeded = false;

	protected EntityManager jpaem = null;

	protected HashMap<String, HashMap<String, String>> fieldsTypesProperties;

	protected String accessType;

	protected HashMap<String, String> accessProperties;

	private EntityManagerFactory emf;

	public final static String NAME = "name"; //$NON-NLS-1$

	public final static String TYPE_URL = "url"; //$NON-NLS-1$

	public final static String TYPE_RESURL = "resurl"; //$NON-NLS-1$

	public final static String TYPE_TYPEURL = "typeurl"; //$NON-NLS-1$

	public final static String TYPE_ID = "id"; //$NON-NLS-1$

	public final static String TYPE_SIZE = "size"; //$NON-NLS-1$

	public final static String TYPE_EFFECT = "effect"; //$NON-NLS-1$

	public final static String TYPE_SQLFIELD_NAME = "sql_name"; //$NON-NLS-1$

	public final static String TYPE_SQLFIELD_ID = "sql_id"; //$NON-NLS-1$

	public final static String TYPE_SQLFIELD_QUERY = "sql_query"; //$NON-NLS-1$

	public final static String LOGIN_KEY = "org.txm.repository.credencies.login."; //$NON-NLS-1$

	public final static String PASSWORD_KEY = "org.txm.repository.credencies.password."; //$NON-NLS-1$

	public static final boolean[] FALSES = { false, false };

	public KnowledgeRepository(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * Initialize the JPA persistence database
	 * 
	 * @return
	 */
	public EntityManager initializeEntityManager() {
		this.dbPath = new File(Toolbox.getTxmHomePath(), "repositories/" + name).getAbsolutePath(); //$NON-NLS-1$
		// System.out.println("KnowledgeRepository.initializeEntityManager [DB @ "+dbPath+"]");

		HashMap<String, Object> properties = new HashMap<String, Object>();
		properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass().getClassLoader());
		// ClassLoader loader = BundleUtils.getLoader("org.txm.annotation.core");
		// URL res = loader.getResource("META-INF/persistence.xml");
		//
		// properties.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_XML, "/home/mdecorde/workspace079/org.txm.annotation.core/META-INF");
		// //properties.put("javax.persistence.jdbc.driver", "org.hsqldb.jdbcDriver");
		String urlProperty = "jdbc:hsqldb:file:" + dbPath + "/db;shutdown=true;hsqldb.lock_file=false;"; //$NON-NLS-1$ //$NON-NLS-2$
		if (Log.getLevel().intValue() < Level.INFO.intValue()) {
			urlProperty += ""; //$NON-NLS-1$
		}
		else {
			urlProperty += "hsqldb.applog=0;hsqldb.sqllog=0"; //$NON-NLS-1$
		}
		properties.put("javax.persistence.jdbc.url", urlProperty); //$NON-NLS-1$
		// properties.put("javax.persistence.jdbc.username", "SA");
		// properties.put(PersistenceUnitProperties.DDL_GENERATION_MODE, "database");
		properties.put(PersistenceUnitProperties.DDL_GENERATION, "create-or-extend-tables"); // create&update table if needed //$NON-NLS-1$
		// properties.put(PersistenceUnitProperties.DDL_GENERATION, "drop-and-create-tables"); // drop all and recreate
		// if (Log.getLevel().intValue() < Level.WARNING.intValue()) {
		// properties.put(PersistenceUnitProperties.LOGGING_LEVEL, "WARNING");
		// properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "true");
		// } else {
		// properties.put(PersistenceUnitProperties.LOGGING_LEVEL, "OFF");
		// properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "false");
		// }

		// Logger.getLogger("org").setLevel(Level.ALL);
		PersistenceProvider pp = new PersistenceProvider();
		emf = pp.createEntityManagerFactory(DatabasePersistenceManager.PERSISTENCE_UNIT_NAME, properties);
		if (emf == null) {
			Log.warning(NLS.bind("Error: could not create the ''{0}'' EntityManagerFactory and PersistenceProvider={1}.", DatabasePersistenceManager.PERSISTENCE_UNIT_NAME, pp)); //$NON-NLS-1$
			return null;
		}
		EntityManager entityManager = emf.createEntityManager();
		this.jpaem = entityManager;

		return entityManager;
	}


	public void setAccessProperties(HashMap<String, String> accessProperties) {
		this.accessProperties = accessProperties;

		String sVersion = accessProperties.get("version"); //$NON-NLS-1$
		if (sVersion.length() > 0) {
			this.version = Integer.parseInt(sVersion);
		}
		this.url = accessProperties.get(TYPE_URL);
		this.values_base_url = accessProperties.get(TYPE_RESURL);
		this.types_base_url = accessProperties.get(TYPE_TYPEURL);

	}

	public HashMap<String, String> getAccessProperties() {
		return accessProperties;
	}

	public boolean isLoginNeeded() {
		return loginNeeded;
	}

	public boolean isPasswordNeeded() {
		return passwordNeeded;
	}

	public void setCredencialsNeeded(boolean login, boolean password) {
		this.loginNeeded = login;
		this.passwordNeeded = password;
	}

	public EntityManager getJPAManager() {
		return this.jpaem;
	}

	public abstract String getAccessType(); // local file, sql ...

	public String getName() {
		return name;
	}

	public List<TypedValue> getValues(AnnotationType type) throws Exception {
		return getValues(type, -1);
	}

	/**
	 * returns a resource URL for a defined TypedValue (for instance to display in a html viewer)
	 * 
	 * @param typedValue
	 * @return
	 */
	public String getValueURL(TypedValue typedValue) {
		return values_base_url + typedValue.getId();
	}

	/**
	 * returns a resource URL for a defined TypedValue (for instance to display in a html viewer)
	 * 
	 * @return
	 */
	public String getTypeURL(AnnotationType type) {
		String url = type.getURL();
		if (url == null || url.length() == 0) {
			if (types_base_url != null && types_base_url.length() > 0) {
				url = types_base_url + type.getId();
			}
		}
		return url;
	}

	/**
	 * returns a URL for a defined KnowledgeRepository (for instance to display in a html viewer)
	 * 
	 * @return
	 */
	public String getURL() {
		return url;
	}

	public HashMap<String, HashMap<String, String>> getFieldsTypesProperties() {
		return fieldsTypesProperties;
	}

	public String toHumanString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.getName() + " version=" + version + " (" + this.getURL() + "): " + this.getAccessType() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		for (AnnotationType type : getAllAnnotationTypes()) {
			buffer.append("\t" + type.toHumanString() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return buffer.toString();
	}

	public void reload() {
		Log.fine("Reloading KR " + name); //$NON-NLS-1$

		clearTypes();
		buildTypesAndValues();
	}

	/**
	 * returns all typed values with a specified type
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<TypedValue> getValues(AnnotationType type, int limit) throws Exception {

		TypedQuery<TypedValue> query = jpaem.createQuery("SELECT t FROM TypedValue AS t " //$NON-NLS-1$
				+ "WHERE t.PK.typeId LIKE '" + type.getId() + "'", TypedValue.class); //$NON-NLS-1$ //$NON-NLS-2$
		if (limit != -1) {
			query.setMaxResults(limit);
		}

		return query.getResultList();
	}

	/**
	 * returns all typed values with a specified type
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean hasValues(AnnotationType type) throws Exception {
		TypedQuery<TypedValue> query = jpaem.createQuery("SELECT t.name FROM TypedValue AS t " //$NON-NLS-1$
				+ "WHERE t.PK.typeId LIKE '" + type.getId() + "'", TypedValue.class); //$NON-NLS-1$ //$NON-NLS-2$
		query.setMaxResults(1);

		return query.getResultList().size() > 0;
	}

	public TypedValue getValue(AnnotationType type, String id) {
		TypedValuePK pkval = new TypedValuePK(id, type.getId());
		return jpaem.find(TypedValue.class, pkval);
	}

	// public TypedValue getValue(String typeId, String id) {
	// TypedValuePK pkval = new TypedValuePK(id, typeId);
	// return jpaem.find(TypedValue.class, pkval);
	// }

	public boolean clearTypes() {
		// clear type list
		// super.clear(); // clear vector

		// clear all persisted values
		if (jpaem != null) {
			jpaem.getTransaction().begin();
			Query query = jpaem.createQuery("DELETE FROM TypedValue"); //$NON-NLS-1$
			query.executeUpdate();
			jpaem.getTransaction().commit();
		}

		return true;
	}

	/**
	 * Populate the database with typed values if they do not exist yet
	 * 
	 * @return
	 */
	public boolean buildTypesAndValues() {
		// build from the database (synchronizing with external repository)
		// will probably modify or erase all types from the cache (database)

		List<AnnotationType> annotTypes = getAllAnnotationTypes();
		if (annotTypes != null) {
			if (annotTypes.isEmpty()) {
				buildTypes();
			}
			else {
				//				Log.warning("KR Annotation Types already exist:");
				//				for (AnnotationType annotType : annotTypes) {
				//					Log.warning(NLS.bind(" - Type : {0}", annotType.getName()));
				//				}
			}
		}
		else {
			buildTypes();
		}


		return buildValues();
	}

	private void buildTypes() {
		// System.out.println("KR build types ... : "+fieldsTypesProperties.size());
		if (fieldsTypesProperties != null && fieldsTypesProperties.size() != 0) {
			for (String type : fieldsTypesProperties.keySet()) {
				HashMap<String, String> typeFieldsContent = fieldsTypesProperties.get(type);

				String name = typeFieldsContent.get(KnowledgeRepository.NAME);
				String url = typeFieldsContent.get(KnowledgeRepository.TYPE_URL);
				String size = typeFieldsContent.get(KnowledgeRepository.TYPE_SIZE);
				String s_effect = typeFieldsContent.get(KnowledgeRepository.TYPE_EFFECT);
				if (s_effect == null) s_effect = "SEGMENT"; //$NON-NLS-1$

				for (String typeField : typeFieldsContent.keySet()) {
					// System.out.println("Type field : "+typeField+" value : "+typeFieldsContent.get(typeField));
					if (typeField.equals(KnowledgeRepository.NAME)) {
						name = typeFieldsContent.get(typeField);
					}
					else if (typeField.equals(KnowledgeRepository.TYPE_URL)) {
						url = typeFieldsContent.get(typeField);
					}
				}

				AnnotationType annotType = addType(name, type, url, AnnotationEffect.valueOf(s_effect));
				annotType.setSize(size);
			}
		}
		else {
			// System.out.println("KR should build type (undef) when expert mode is off !!");
			// AnnotationType annotType = addType("undef", "undef", "", AnnotationEffect.valueOf("SEGMENT"));
			// annotType.setSize("SMALL");
		}
	}

	/**
	 * Add and create a type with a default URL
	 * 
	 * @param name
	 * @param id
	 * @return
	 */
	public AnnotationType addType(String name, String id) {
		return addType(name, id, this.url + "/" + id); //$NON-NLS-1$
	}

	/**
	 * Add and create a type
	 * 
	 * @param name
	 * @param id
	 * @param url
	 * @return
	 */
	public AnnotationType addType(String name, String id, String url) {
		return addType(name, id, url, AnnotationEffect.SEGMENT);
	}

	/**
	 * Add and create a type
	 * 
	 * @param name
	 * @param id
	 * @param url
	 * @return
	 */
	public AnnotationType addType(String name, String id, String url, AnnotationEffect ae) {
		AnnotationType annotType = new AnnotationType(this.getName(), id, name, ae);
		annotType.setURL(url);
		// System.out.println("KnowledgeRepository.addType () "+ annotType.toHumanString() +" ---- will be persisted");
		jpaem.getTransaction().begin();
		jpaem.persist(annotType);
		jpaem.getTransaction().commit();
		return annotType;
	}

	public TypedValue addValue(String name, String id, String typeId) {
		TypedValue val = new TypedValue(name, id, typeId);

		jpaem.getTransaction().begin();
		jpaem.persist(val);
		jpaem.getTransaction().commit();
		return val;
	}

	public List<AnnotationType> getAllAnnotationTypes() {
		TypedQuery<AnnotationType> query = jpaem.createQuery("SELECT t FROM AnnotationType AS t ", AnnotationType.class); //$NON-NLS-1$
		return query.getResultList();
	}

	public List<TypedValue> getAllTypedValuesForType(String idType) {
		TypedQuery<TypedValue> query = jpaem.createQuery("SELECT t FROM TypedValue AS t WHERE t.PK.typeId LIKE '" + idType + "'", TypedValue.class); //$NON-NLS-1$ //$NON-NLS-2$
		return query.getResultList();
	}

	/**
	 * returns all typed values with a specified type and a specified value
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<TypedValue> findTypedValuesWithPrefix(AnnotationType type, String prefix) throws Exception {
		TypedQuery<TypedValue> query = jpaem.createQuery("SELECT t FROM TypedValue AS t " //$NON-NLS-1$
				+ "WHERE t.PK.typeId LIKE '" + type.getId() + "'" //$NON-NLS-1$ //$NON-NLS-2$
				+ " AND t.name LIKE '" + prefix + "%'", TypedValue.class); //$NON-NLS-1$ //$NON-NLS-2$
		// System.out.println("Query for "+prefix+" has nb results = "+query.getResultList().size());
		return query.getResultList();
	}


	public void setFieldsTypesProperties(HashMap<String, HashMap<String, String>> fields) {
		this.fieldsTypesProperties = fields;
	}

	public abstract boolean buildValues();

	public int getVersion() {
		return version;
	}

	public abstract boolean[] mustLoginToKnowledgeRepository();

	public abstract boolean checkConnection();

	public void deleteType(AnnotationType t) {
		// Remove all values first
		List<TypedValue> values;
		try {
			values = this.getValues(t);
			for (TypedValue val : values) {
				deleteValue(val);
			}

			jpaem.getTransaction().begin();
			jpaem.remove(t);
			jpaem.getTransaction().commit();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteValue(TypedValue v) {
		jpaem.getTransaction().begin();
		jpaem.remove(v);
		jpaem.getTransaction().commit();
	}

	public AnnotationType getType(String id) {
		return jpaem.find(AnnotationType.class, id);
	}

	HashMap<String, HashMap<String, String>> strings = new HashMap<String, HashMap<String, String>>();

	public String getString(String lang, String key) {
		HashMap<String, String> values = null;
		if (strings.containsKey(lang)) {
			values = strings.get(lang);
		}
		if (values == null) { // get default lang = 'en'
			values = strings.get("en"); //$NON-NLS-1$
		}
		if (values == null) { // fail
			return null;
		}
		return values.get(key);
	}

	public void setStrings(HashMap<String, HashMap<String, String>> strings) {
		this.strings = strings;
	}

	public void saveConfiguration(File krconf) {
		// TODO Auto-generated method stub

	}

	public void close() {

		if (jpaem != null) jpaem.close();
		if (emf != null) emf.close();
	}
}
