package org.txm.annotation.kr.core.repository;

/**
 * Defines is the annotation target is a segment, a single token or another annotation
 * 
 * @author mdecorde
 *
 */
public enum AnnotationEffect {

	SEGMENT, // annotate a token sequence
	TOKEN, // annotate a token position
	ANNOTATION; // annotate an annotation

	/**
	 * 
	 * 
	 * @return Effet suffix used to display the annotation type in messages
	 */
	public String toSuffix() {
		if (this.equals(TOKEN)) return " (T)"; //$NON-NLS-1$
		else if (this.equals(ANNOTATION)) return " (A)"; //$NON-NLS-1$
		else return ""; //$NON-NLS-1$
	}
}
