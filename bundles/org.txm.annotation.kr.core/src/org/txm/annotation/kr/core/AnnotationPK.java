package org.txm.annotation.kr.core;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class AnnotationPK implements Serializable {

	private static final long serialVersionUID = -2360693333015275209L;

	// corresponding to the start and end positions (in the corpus)
	private int startpos;

	private int endpos;

	private String refType;

	public AnnotationPK() {
	}

	public AnnotationPK(int start, int end, String refType) {
		this.startpos = start;
		this.endpos = end;
		this.refType = refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public void setStartPosition(int start) {
		this.startpos = start;
	}

	public void setEndPosition(int end) {
		this.endpos = end;
	}

	public String getRefType() {
		return refType;
	}

	public int getStartPosition() {
		return startpos;
	}

	public int getEndPosition() {
		return endpos;
	}

	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj == this) return true;
		if (!(obj instanceof AnnotationPK)) return false;

		AnnotationPK other = (AnnotationPK) obj;
		return startpos == other.startpos && endpos == other.endpos && refType.equals(other.refType);
	}

	public int hashCode() {
		return refType.hashCode() + startpos + endpos;
	}

	public String toString() {
		return getRefType() + "[" + getStartPosition() + "-" + getEndPosition() + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
}
