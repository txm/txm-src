package org.txm.annotation.kr.core;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import org.txm.Toolbox;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;

@Entity
public class Annotation implements Serializable {

	private static final long serialVersionUID = -1007684142118207359L;

	// primaryKey corresponds to the start and end positions (in the corpus) and the refType
	@EmbeddedId
	private AnnotationPK PK;

	// corresponding to the type and value of the KnowledgeRepository, used to annotate
	private String refVal = ""; //$NON-NLS-1$

	// user in the project, responsible for the annotation, maybe should be an integer
	private String annotatorId = ""; //$NON-NLS-1$

	private String date;

	public Annotation() {
	}


	public Annotation(String refType, String refVal, int CQPstartpos, int CQPendpos) {
		this.PK = new AnnotationPK(CQPstartpos, CQPendpos, refType);
		this.refVal = refVal;
		this.date = Toolbox.dateformat.format(new Date());

		String s = System.getProperty(KnowledgeRepository.LOGIN_KEY);
		if (s != null && s.length() > 0) {
			this.annotatorId = s;
		}
		else {
			this.annotatorId = System.getProperty("user.name"); //$NON-NLS-1$
		}
	}


	public AnnotationPK getPK() {
		return this.PK;
	}

	public int getStart() {
		return this.PK.getStartPosition();
	}

	public int getEnd() {
		return this.PK.getEndPosition();
	}

	public String getType() {
		return this.PK.getRefType();
	}

	public String getValue() {
		return this.refVal;
	}

	public String getDate() {
		return this.date;
	}

	// Maybe will be an integer ?
	public String getAnnotator() {
		return annotatorId;
	}


	public void setReferentielVal(String newRefVal) {
		this.refVal = newRefVal;
	}


	public String toString() {
		return getPK().toString() + "=" + getValue(); //$NON-NLS-1$
	}

}
