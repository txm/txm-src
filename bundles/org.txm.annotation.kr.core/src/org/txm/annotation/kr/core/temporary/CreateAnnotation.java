package org.txm.annotation.kr.core.temporary;

import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Test;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.storage.temporary.TemporaryAnnotationManager;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;

public class CreateAnnotation {

	@Test
	public void test() throws CqiClientException, InvalidCqpIdException {
		if (!Toolbox.isInitialized()) fail("Toolbox not initialized."); //$NON-NLS-1$
		MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus("VOEUX"); //$NON-NLS-1$
		if (corpus == null) fail("Corpus Voeux not loaded."); //$NON-NLS-1$

		HashMap<String, Object> properties = TemporaryAnnotationManager.getInitialisationProperties(this.getClass(), corpus);
		properties.put("eclipselink.persistencexml", System.getProperty("user.home") + "/workspace442/org.txm.core/META-INF/persistence.xml"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		TemporaryAnnotationManager tam = new TemporaryAnnotationManager(corpus, properties);
		System.out.println(tam);
	}

}
