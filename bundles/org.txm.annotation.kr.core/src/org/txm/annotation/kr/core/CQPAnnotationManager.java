package org.txm.annotation.kr.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.osgi.service.prefs.BackingStoreException;
import org.txm.annotation.kr.core.repository.AnnotationEffect;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.objects.Match;
import org.txm.objects.Match2P;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

public class CQPAnnotationManager {

	MainCorpus corpus;

	public static final String REF = "ref"; //$NON-NLS-1$

	public CQPAnnotationManager(MainCorpus corpus) {
		this.corpus = corpus;
	}

	/**
	 * returns all annotations for a given type
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Annotation> getAnnotations(AnnotationType type) throws Exception {
		return getAnnotations(type, false);
	}

	/**
	 * returns all annotations for a given type and overlap option
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Annotation> getAnnotations(AnnotationType type, boolean overlap) throws Exception {
		return getAnnotations(type, null, null, overlap);
	}

	public List<Annotation> getAnnotations(AnnotationType type, int start, int end) throws CqiClientException, IOException, CqiServerError {
		return getAnnotations(type, new Match2P(start, end), null, false);
	}

	public List<Annotation> getAnnotations(AnnotationType type, int start, int end, boolean overlap) throws CqiClientException, IOException, CqiServerError {
		return getAnnotations(type, new Match2P(start, end), null, overlap);
	}

	/**
	 * Return all annotations for ONE match
	 * can be use to find out overlap
	 * 
	 * @param match can be null
	 * @param value can be null
	 * @param overlap
	 * @return
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 */
	public List<Annotation> getAnnotations(AnnotationType type, Match match, String value, boolean overlap) throws CqiClientException, IOException, CqiServerError {
		if (type == null || type.getEffect().equals(AnnotationEffect.SEGMENT)) {
			return getSegmentAnnotations(type, match, value, overlap);
		}
		else {
			return getTokenAnnotations(type, match, value, overlap);
		}
	}

	/**
	 * 
	 * @param subsetMatch Ordered matches, not null
	 * @param type not null
	 * @param overlap use true to detect overlapping annotations
	 * @return ONE Annotation per Match for a given type
	 * 
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws CqiClientException
	 */
	public List<Annotation> getTokenAnnotations(AnnotationType type, Match subsetMatch, String value, boolean overlap) throws IOException, CqiServerError, CqiClientException {

		Property prop = corpus.getProperty(type.getId().toLowerCase());
		if (prop == null) return nullAnnotationList(0); // no property, no annotation :)

		AbstractCqiClient cqi = CQPSearchEngine.getCqiClient();

		int start = corpus.getFirstPosition();
		int n = corpus.getSize();

		if (subsetMatch != null) { // use a subset instead of all the corpus
			start = subsetMatch.getStart();
			n = subsetMatch.size();
		}

		int i = 0;
		int positions[] = new int[n];
		for (i = 0; i < n; i++) {
			positions[i] = start + i;
		}

		String[] strs = cqi.cpos2Str(prop.getQualifiedName(), positions);

		Pattern pattern = null;
		if (value != null) pattern = Pattern.compile(value);

		ArrayList<Annotation> annotations = new ArrayList<Annotation>();
		i = 0;
		for (String str : strs) {
			int p = positions[i++];
			if (value == null || pattern.matcher(str).matches()) {
				annotations.add(new CQPAnnotation(type.getId(), str, p, p));
			}
		}
		// Log.info("TOKEN ANNOTATION CQP VALUES: "+annotations);
		return annotations;
	}

	/**
	 * Return all annotations for ONE match
	 * can be use to find out overlap
	 * 
	 * @param match can be null
	 * @param value can be null
	 * @param overlap
	 * @return
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 */
	public List<Annotation> getSegmentAnnotations(AnnotationType type, Match match, String value, boolean overlap) throws CqiClientException, IOException, CqiServerError {
		HashMap<StructuralUnitProperty, AnnotationType> supList = new HashMap<StructuralUnitProperty, AnnotationType>();

		// TODO add a corpus parameters for structures that code annotations
		// TODO manage word properties annotations
		if (type == null) {

			// first get know annotations from corpus's KR
			for (String kr_name : KRAnnotationEngine.getKnowledgeRepositoryNames(corpus)) {
				KnowledgeRepository kr;
				try {
					kr = KRAnnotationEngine.getKnowledgeRepository(corpus, kr_name);
					if (kr == null) continue;
					List<AnnotationType> types = kr.getAllAnnotationTypes();
					for (AnnotationType t : types) {
						StructuralUnit su = corpus.getStructuralUnit(t.getId().toLowerCase());
						if (su != null) {
							StructuralUnitProperty sup = su.getProperty(REF);
							if (sup != null) {
								supList.put(sup, t);
							}
						}
					}
				}
				catch (BackingStoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			for (StructuralUnit su : corpus.getStructuralUnits()) {
				if (su.getName().equals("text") || su.getName().equals("txmcorpus") || //$NON-NLS-1$ //$NON-NLS-2$
						su.getName().equals("lb") || su.getName().equals("pb")) //$NON-NLS-1$//$NON-NLS-2$
					continue; // that's no annotation for sure ;)  

				StructuralUnitProperty sup = su.getProperty(REF);
				if (sup == null) sup = su.getProperty("n"); //$NON-NLS-1$
				if (sup != null) {
					if (!supList.containsKey(sup)) // don't override a know annotation
						supList.put(sup, null);
				}
			}

		}
		else {
			StructuralUnit su = corpus.getStructuralUnit(type.getId().toLowerCase());
			if (su == null) return new ArrayList<Annotation>(); // no property, no annotation :)

			StructuralUnitProperty sup = su.getProperty(REF);
			if (sup == null) return new ArrayList<Annotation>(); // no attribute REF, no annotation :)

			supList.put(sup, type);
		}

		ArrayList<Annotation> result = new ArrayList<Annotation>();
		for (StructuralUnitProperty sup : supList.keySet()) {
			String sup_name = sup.getStructuralUnit().getName();
			AnnotationType sup_type = supList.get(sup);
			if (sup_type != null) sup_name = sup_type.getId();

			int Nstruct = CQPSearchEngine.getCqiClient().attributeSize(sup.getQualifiedName());

			// TODO: can optimize this, to fetch only structure that matches
			int[] iStructs = new int[Nstruct];
			for (int iStruct = 0; iStruct < Nstruct; iStruct++)
				iStructs[iStruct] = iStruct;
			String svalues[] = CQPSearchEngine.getCqiClient().struc2Str(sup.getQualifiedName(), iStructs); // one position

			for (int iStruct = 0; iStruct < Nstruct; iStruct++) {
				int smatch[] = CQPSearchEngine.getCqiClient().struc2Cpos(sup.getQualifiedName(), iStruct);

				if (value != null) { // supplementary test if value is specified
					if (!value.equals(svalues[iStruct])) {
						continue; // next struct
					}
				}
				if (match != null) {
					if (overlap) {
						if (match.getEnd() < smatch[0]) {
							break; // no need to continue, structures are ordered
						}
						else if (smatch[1] < match.getStart()) {
							continue; // next structure
						}
						else {
							result.add(new Annotation(sup_name, svalues[iStruct], smatch[0], smatch[1]));
						}
					}
					else {
						if (match.getStart() == smatch[0] && match.getEnd() == smatch[1]) {
							result.add(new Annotation(sup_name, svalues[iStruct], smatch[0], smatch[1]));
						}
					}
				}
				else { // get all annotations
					result.add(new Annotation(sup_name, svalues[iStruct], smatch[0], smatch[1]));
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @param matches Ordered matches, not null
	 * @param type not null
	 * @param overlap use true to detect overlapping annotations
	 * @return ONE Annotation per Match for a given type
	 * 
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws CqiClientException
	 */
	public List<? extends Annotation> getAnnotationsForMatches(List<Match> matches, AnnotationType type, boolean overlap) throws IOException, CqiServerError, CqiClientException {
		if (type.getEffect().equals(AnnotationEffect.SEGMENT)) {
			return getSegmentAnnotationsForMatches(matches, type, overlap);
		}
		else {
			return getTokenAnnotationsForMatches(matches, type, overlap);
		}
	}

	/**
	 * 
	 * @param matches Ordered matches, not null
	 * @param type not null
	 * @param overlap use true to detect overlapping annotations
	 * @return ONE Annotation per Match for a given type
	 * 
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws CqiClientException
	 */
	public List<? extends Annotation> getTokenAnnotationsForMatches(List<Match> matches, AnnotationType type, boolean overlap) throws IOException, CqiServerError, CqiClientException {

		Property prop = corpus.getProperty(type.getId().toLowerCase());
		if (prop == null) return nullAnnotationList(matches.size()); // no property, no annotation :)

		AbstractCqiClient cqi = CQPSearchEngine.getCqiClient();

		int positions[] = new int[matches.size()];
		int i = 0;
		for (Match m : matches) {
			if (m.getTarget() >= 0) {
				positions[i++] = m.getTarget();
			}
			else {
				positions[i++] = m.getStart();
			}
		}

		String[] strs = cqi.cpos2Str(prop.getQualifiedName(), positions);

		ArrayList<CQPAnnotation> annotations = new ArrayList<CQPAnnotation>();
		i = 0;
		for (String str : strs) {
			int p = positions[i++];
			annotations.add(new CQPAnnotation(type.getId(), str, p, p));
		}
		// Log.info("TOKEN ANNOTATION CQP VALUES: "+annotations);
		return annotations;
	}

	/**
	 * 
	 * @param matches Ordered matches, not null
	 * @param type not null
	 * @param overlap use true to detect overlapping annotations
	 * @return ONE Annotation per Match for a given type
	 * 
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws CqiClientException
	 */
	public List<Annotation> getSegmentAnnotationsForMatches(List<Match> matches, AnnotationType type, boolean overlap) throws IOException, CqiServerError, CqiClientException {

		StructuralUnit su = corpus.getStructuralUnit(type.getId().toLowerCase());
		if (su == null) return nullAnnotationList(matches.size()); // no property, no annotation :)

		StructuralUnitProperty sup = su.getProperty(REF);
		if (sup == null) return nullAnnotationList(matches.size()); // no attribute REF, no annotation :)

		int Nstruct = CQPSearchEngine.getCqiClient().attributeSize(sup.getQualifiedName());
		int iMatch = 0;
		int iStruct = 0;
		int Nmatches = matches.size();
		ArrayList<Integer> values = new ArrayList<Integer>(); // contains structure numbers
		ArrayList<int[]> annotationsStartEnd = new ArrayList<int[]>(); // contains structure numbers

		if (Nstruct == 0) return new ArrayList<Annotation>(matches.size()); // no annotation
		// System.out.println("NUMBER OF STRUCT "+sup+" "+Nstruct);
		// System.out.println("MATCHES "+matches);
		int smatch[] = CQPSearchEngine.getCqiClient().struc2Cpos(sup.getQualifiedName(), iStruct);
		// System.out.println(" TEST WITH "+smatch[0]+"-"+smatch[1]);
		if (overlap) {
			for (iMatch = 0; iStruct < Nstruct && iMatch < Nmatches;) {
				Match m = matches.get(iMatch);
				int start, end;
				if (m.getTarget() >= 0) {
					start = end = m.getTarget();
				}
				else {
					start = m.getStart();
					end = m.getEnd();
				}
				if (smatch[1] < start) { // next struct
					iStruct++;
					if (iStruct < Nstruct)
						smatch = CQPSearchEngine.getCqiClient().struc2Cpos(sup.getQualifiedName(), iStruct);
					// System.out.println(" NEXT STRUCT TEST WITH "+smatch[0]+"-"+smatch[1]);
				}
				else if (end < smatch[0]) { // next match
					values.add(null);
					annotationsStartEnd.add(null);
					iMatch++;
					// System.out.println(" NEXT MATCH of ["+m.getStart()+"-"+m.getEnd()+"] match TEST WITH "+smatch[0]+"-"+smatch[1]);
				}
				else {
					// ((m.getStart() <= smatch[0] && smatch[0] <= m.getEnd()) ||
					// (m.getStart() <= smatch[1] && smatch[1] <= m.getEnd()) ||
					// (smatch[0] <= m.getStart() && m.getEnd() <= smatch[1])) {
					iMatch++;
					values.add(iStruct);
					annotationsStartEnd.add(smatch);
					// System.out.println(" FOUND annotation "+smatch[0]+"-"+smatch[1]);
				}
			}
		}
		else { // strict matches
			for (iMatch = 0; iStruct < Nstruct && iMatch < Nmatches;) {
				Match m = matches.get(iMatch);
				int start, end;
				if (m.getTarget() >= 0) {
					start = end = m.getTarget();
				}
				else {
					start = m.getStart();
					end = m.getEnd();
				}

				if (smatch[0] == start && smatch[1] == end) {
					values.add(iStruct);
					annotationsStartEnd.add(smatch);
					iMatch++;
					iStruct++;
					if (iStruct < Nstruct)
						smatch = CQPSearchEngine.getCqiClient().struc2Cpos(sup.getQualifiedName(), iStruct);
				}
				else if (smatch[1] < start) { // next struct
					iStruct++;
					if (iStruct < Nstruct)
						smatch = CQPSearchEngine.getCqiClient().struc2Cpos(sup.getQualifiedName(), iStruct);
				}
				else if (end < smatch[0]) { // next match
					values.add(null);
					annotationsStartEnd.add(null);
					iMatch++;
				}
				else {
					values.add(null);
					annotationsStartEnd.add(null);
					iMatch++;
				}
			}
		}

		while (values.size() < matches.size()) {
			values.add(null); // if matches have not been processed due to no more struct available
			annotationsStartEnd.add(null);
		}

		int tmp_strucs[] = new int[values.size()]; // create the structure numbers array that CQP.struc2Str needs
		for (int i = 0; i < values.size(); i++) {
			if (values.get(i) == null) {
				tmp_strucs[i] = 0; // add a fake value, remove after CQP call
			}
			else {
				tmp_strucs[i] = values.get(i);
			}
		}

		String[] svalues = CQPSearchEngine.getCqiClient().struc2Str(sup.getQualifiedName(), tmp_strucs);
		ArrayList<Annotation> annotations = new ArrayList<Annotation>();
		for (int i = 0; i < values.size(); i++) { // remove the fake values
			if (values.get(i) == null) {
				annotations.add(null);
			}
			else {
				annotations.add(new Annotation(type.getId(), svalues[i], annotationsStartEnd.get(i)[0], annotationsStartEnd.get(i)[1]));
			}
		}

		// Log.info("SEGMENT ANNOTATION CQP VALUES: "+annotations);
		return annotations;
	}

	private ArrayList<Annotation> nullAnnotationList(int size) {
		ArrayList<Annotation> a = new ArrayList<Annotation>(size);
		for (int i = 0; i < size; i++) {
			a.add(null);
		}
		return a;
	}

	/**
	 * @param start
	 * @param end
	 * @param type
	 * @return value for a match and a StructuralUnit
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 */
	public String getCQPAnnotationValue(int start, int end, AnnotationType type) throws IOException, CqiServerError, CqiClientException {

		StructuralUnit su = corpus.getStructuralUnit(type.getId().toLowerCase());
		if (su == null) return null;

		StructuralUnitProperty sup = su.getProperty(REF);
		if (sup == null) return null;
		int[] cpos = { start, end };
		int[] struc = CQPSearchEngine.getCqiClient().cpos2Struc(sup.getQualifiedName(), cpos);

		if (struc[0] >= 0) {
			// System.out.println("struct for positions "+start+"-"+end+" -> "+struc[0]+"-"+struc[1]);
			if (struc[0] == struc[1]) { // même structure
				String[] struc_str = CQPSearchEngine.getCqiClient().struc2Str(sup.getQualifiedName(), struc);
				return struc_str[0];
			}
			else { // should not happens
				Log.warning("WARNING: FOUND EXISTING ANNOTATION FOR TYPE=" + type + " START=" + start + " END=" + end); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				return null;
			}
		}
		else {
			return null;
		}
	}

	public void close() {

	}
}
