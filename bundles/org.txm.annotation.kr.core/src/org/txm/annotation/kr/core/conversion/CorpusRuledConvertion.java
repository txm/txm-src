package org.txm.annotation.kr.core.conversion;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class CorpusRuledConvertion {

	LinkedHashMap<Pattern, String> rules = new LinkedHashMap<>();

	private String oldType;

	private String newType;

	public CorpusRuledConvertion(File conversionFile, String oldType, String newType) throws IOException {
		this.oldType = oldType;
		this.newType = newType;

		BufferedReader reader = IOUtils.getReader(conversionFile);
		String line = reader.readLine();
		while (line != null) {
			int idx = line.indexOf("\t"); //$NON-NLS-1$
			if (idx > 0) {
				String k = line.substring(0, idx);
				String v = line.substring(idx + 1);
				rules.put(Pattern.compile(k), v);
			}
			line = reader.readLine();
		}

		Log.info(NLS.bind(Messages.ConversionRulesP0, rules));
	}

	public CorpusRuledConvertion(LinkedHashMap<Pattern, String> rules,
			String oldType, String newType) {
		this.oldType = oldType;
		this.newType = newType;

		this.rules = rules;
	}

	public boolean process(MainCorpus corpus) throws XMLStreamException, IOException {
		File binaryCorpusDirectory = corpus.getProjectDirectory();
		File txmDirectory = new File(binaryCorpusDirectory, "txm"); //$NON-NLS-1$
		if (!txmDirectory.exists()) {
			Log.warning(NLS.bind(Messages.ThxTXMDirectoryWasNotFoundInP0, binaryCorpusDirectory.getAbsolutePath()));
			return false;
		}
		File txmCorpusDirectory = new File(txmDirectory, corpus.getID());
		if (!txmCorpusDirectory.exists()) {
			Log.warning(NLS.bind(Messages.TheP0CorpusDirectoryWasNotFoundInP1, corpus.getName(), txmDirectory.getAbsolutePath()));
			return false;
		}
		File[] files = txmCorpusDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
		if (files == null || files.length == 0) {
			Log.warning(NLS.bind(Messages.NoFileFoundInTheP0Directory, txmCorpusDirectory));
			return false;
		}
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.length);
		for (File xmlFile : files) {
			cpb.tick();
			if (xmlFile.isDirectory()) continue;
			if (xmlFile.isHidden()) continue;
			if (!xmlFile.getName().endsWith(".xml")) continue; //$NON-NLS-1$

			File tmpFile = new File(xmlFile.getParentFile(), "tmp_" + xmlFile.getName()); //$NON-NLS-1$
			XMLTXMFileRuledConversion converter = new XMLTXMFileRuledConversion(xmlFile, rules, null, oldType, newType, XMLTXMFileRuledConversion.ABANDON);
			if (converter.process(tmpFile)) {
				xmlFile.delete();
				// try {
				// FileCopy.copy(tmpFile, new File("/tmp/"+tmpFile.getName()));
				// } catch (IOException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				HashSet<String> errors = converter.getNoMatchValues();
				if (errors.size() > 0) {
					Log.warning(Messages.SomeValuesDidNotMatchRule);
					int i = 0;
					for (String error : errors) {
						Log.warning("\t" + error); //$NON-NLS-1$
						if (i >= 10) break;
					}
					if (errors.size() > 10) {
						try {
							File errorFile = new File(Toolbox.getTxmHomePath(), "errors.txt"); //$NON-NLS-1$
							IOUtils.write(errorFile, StringUtils.join(errors, "\t")); //$NON-NLS-1$
							Log.warning(NLS.bind(Messages.MoreErrorsSeeTheP0ErrorFile, errorFile.getAbsolutePath()));
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
					return false;
				}

				tmpFile.renameTo(xmlFile);
				if (tmpFile.exists()) {
					Log.warning(NLS.bind(Messages.CouldNotReplaceOriginalFileWithTheP0ResultFileP1, xmlFile, tmpFile));
					return false;
				}
			}
			else {
				Log.warning(NLS.bind(Messages.FailToProcessTheP0XMLFile, xmlFile));
				return false;
			}
		}
		cpb.done();
		return true;
	}
}
