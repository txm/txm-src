package org.txm.annotation.kr.core.repository;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TypedValuePK implements Serializable {

	public static final long serialVersionUID = -2624411297196922947L;

	private String id;

	private String typeId;

	public TypedValuePK() {

	}

	public TypedValuePK(String publicId, String typeId) {
		this.typeId = typeId;
		this.id = publicId;
	}


	public void setType(String typeId) {
		this.typeId = typeId;
	}

	public void setPublicId(String publicId) {
		this.id = publicId;
	}

	public String getType() {
		return typeId;
	}

	public String getId() {
		return this.id;
	}

	public int hashCode() {
		return id.hashCode() + typeId.hashCode();
	}

	public String toString() {
		return typeId + "=" + id; //$NON-NLS-1$
	}
}
