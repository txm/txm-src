package org.txm.annotation.kr.core.temporary;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.txm.StartToolbox;

@RunWith(Suite.class)
@SuiteClasses({ StartToolbox.class, CreateAnnotation.class, DeleteAnnotation.class,
		UpdateAnnotation.class })
public class AllTests {

}
