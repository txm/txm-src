package org.txm.annotation.kr.core;

import java.io.File;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.annotation.kr.core.messages.Messages;
import org.txm.annotation.kr.core.repository.AnnotationEffect;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.core.storage.temporary.TemporaryAnnotationManager;
import org.txm.core.preferences.TBXPreferences;
import org.txm.objects.Match;
import org.txm.objects.Match2P;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.CsvReader;
import org.txm.utils.OSDetector;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Manage annotations and is able to return annotation saved in JPA
 * 
 * @author mdecorde
 *
 */
public class AnnotationManager {

	MainCorpus corpus;

	TemporaryAnnotationManager tempManager;

	CQPAnnotationManager cqpManager;

	boolean dirty = false;

	public AnnotationManager(MainCorpus mainCorpus) {
		this.corpus = mainCorpus;
	}

	public TemporaryAnnotationManager getTemporaryManager() {
		return tempManager;
	}

	public CQPAnnotationManager getCQPManager() {
		return cqpManager;
	}

	public boolean saveAnnotations(IProgressMonitor monitor) throws Exception {

		List<Annotation> annots = tempManager.getAnnotations();
		if (annots.isEmpty()) {
			Log.info(Messages.noAnnotationToSaveAborting);
			dirty = false;
			return true;
		}

		if (monitor != null) {
			monitor.beginTask(Messages.savingAnnotations, annots.size());
			monitor.setTaskName(Messages.writingAnnotationsInXMLTXMFiles);
			monitor.slice(annots.size());
		}

		AnnotationWriter writer = new AnnotationWriter(corpus);
		if (writer.writeAnnotations(annots, monitor)) {
			Log.info(Messages.annotationSuccesfullyWritten);
			corpus.getProject().appendToLogs(Messages.bind(Messages.savingP0AnnotationsByP1, annots.size(), OSDetector.getUserAndOSInfos()));
			tempManager.deleteAnnotations();
			dirty = false;
			return true;
		}
		return false;
	}

	public boolean exportAnnotationsToSyMoGIH(File resultZipFile) throws Exception {
		AnnotationWriter writer = new AnnotationWriter(corpus);

		if (writer.writeAnnotationsInStandoff(resultZipFile)) {
			Log.info(NLS.bind(Messages.annotationSuccesfullyWrittenIntheP0File, resultZipFile));
			return true;
		}
		return false;
	}

	// TODO: not ended?
	/**
	 * Deletes the annotations stored in the temporary annotation manager
	 * 
	 * @param type
	 * @param job
	 * @return
	 * @throws Exception
	 */
	public boolean deleteAnnotations(AnnotationType type, IProgressMonitor job) throws Exception {
		List<Annotation> temporaryAnnotations = null;
		List<Annotation> cqpAnnotations = null;
		try {
			temporaryAnnotations = tempManager.getAnnotations(type);
			tempManager.getEntityManager().getTransaction().begin();
			for (Annotation a : temporaryAnnotations) {
				if (job != null && job.isCanceled()) {
					Log.info(Messages.deleteAnnotationCanceled);
					return false;
				}
				tempManager.deleteAnnotation(type, a.getStart(), a.getEnd());
			}
			tempManager.getEntityManager().getTransaction().commit();

			cqpAnnotations = cqpManager.getAnnotations(type);
			tempManager.getEntityManager().getTransaction().begin();
			for (Annotation a : cqpAnnotations) {
				if (job != null && job.isCanceled()) {
					Log.info(Messages.deleteAnnotationCanceled);
					return false;
				}
				String value = cqpManager.getCQPAnnotationValue(a.getStart(), a.getEnd(), type);
				if (value != null) {
					tempManager.createAnnotationNoCommit(type, new TypedValue("#del", "#del", type.getId()), a.getStart(), a.getEnd()); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					tempManager.deleteAnnotationNoCommit(type, a.getStart(), a.getEnd());
				}
			}
			dirty = true;
			tempManager.getEntityManager().getTransaction().commit();
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean deleteAnnotations(AnnotationType type, List<? extends Match> matches, IProgressMonitor job) throws Exception {
		try {
			tempManager.getEntityManager().getTransaction().begin();
			for (Match match : matches) {

				int start, end;
				if (match.getTarget() >= 0) {
					start = end = match.getTarget();
				}
				else {
					start = match.getStart();
					end = match.getEnd();
					if (type.getEffect().equals(AnnotationEffect.TOKEN)) {
						end = start;
					}
				}

				if (job != null && job.isCanceled()) {
					Log.info(Messages.deleteAnnotationCanceled);
					return false;
				}

				String value = cqpManager.getCQPAnnotationValue(start, end, type);

				if (value != null) {
					tempManager.createAnnotationNoCommit(type, new TypedValue("#del", "#del", type.getId()), start, end); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					tempManager.deleteAnnotationNoCommit(type, start, end);
				}
			}
			dirty = true;
			tempManager.getEntityManager().getTransaction().commit();
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * Returns the annotation saved in the temporary database and in the CQP corpus indexes
	 * 
	 * CQP Annotations must be shadowed by temporary annotations of the same type and positions
	 */
	public List<Annotation> getAnnotationsForMatches(AnnotationType type, List<Match> subsetMatches, boolean overlap) {

		List<Annotation> temporaryAnnotations = null;
		List<Annotation> resultAnnotations = new ArrayList<>();
		try {
			temporaryAnnotations = tempManager.getAnnotations(type, subsetMatches, null, false, overlap);
			temporaryAnnotations = tempManager.getAnnotationsForMatches(subsetMatches, temporaryAnnotations, overlap);

			List<? extends Annotation> cqpAnnotations = cqpManager.getAnnotationsForMatches(subsetMatches, type, overlap);

			// System.out.println("Temporary annotations: "+temporaryAnnotations);
			// System.out.println("CQP annotations: "+cqpAnnotations);
			if (cqpAnnotations.size() != subsetMatches.size() || temporaryAnnotations.size() != subsetMatches.size()) {
				Log.info("ERROR in getAnnotationsForMatches methods! "); //$NON-NLS-1$
				return new ArrayList<>(subsetMatches.size());
			}
			// merge the 2 results
			for (int i = 0; i < subsetMatches.size(); i++) {
				if (cqpAnnotations.get(i) == null && temporaryAnnotations.get(i) == null) {
					resultAnnotations.add(null);
				}
				else if (temporaryAnnotations.get(i) != null) {
					resultAnnotations.add(temporaryAnnotations.get(i));
				}
				else if (cqpAnnotations.get(i) != null) {
					resultAnnotations.add(cqpAnnotations.get(i));
				}
				else {
					resultAnnotations.add(null);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>(subsetMatches.size());
		}
		return resultAnnotations;
	}

	public void clearInstance() {
		try {
			tempManager.close();
		}
		catch (Exception e) {
			Log.warning(NLS.bind(Messages.failToClearTheAnnotationManagerInstanceP0, e));
			Log.printStackTrace(e);
		}
	}

	public void checkData() {
		try {
			tempManager.checkData();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean hasChanges() {
		return tempManager.hasChanges();
	}

	/**
	 * 
	 * @param annotSelectedType not null
	 * @param annotSelectedTypedValue not null
	 * @param matches not null
	 * @param job may be null
	 * @return
	 */
	public HashMap<Match, List<Annotation>> createAnnotations(AnnotationType annotSelectedType,
			TypedValue annotSelectedTypedValue, List<? extends Match> matches, IProgressMonitor job) {
		return createAnnotations(annotSelectedType, annotSelectedTypedValue, matches, job, AnnotationScope.FIRSTTARGET);
	}

	/**
	 * 
	 * @param annotSelectedType not null
	 * @param annotSelectedTypedValue not null
	 * @param matches not null
	 * @param job may be null
	 * @return
	 */
	public HashMap<Match, List<Annotation>> createAnnotations(AnnotationType annotSelectedType,
			TypedValue annotSelectedTypedValue, List<? extends Match> matches, IProgressMonitor job, AnnotationScope scope) {

		HashMap<Match, List<Annotation>> allAnnotationsThatCollides = new HashMap<>();
		tempManager.getEntityManager().getTransaction().begin(); // warning
		int nCreation = 0;
		ConsoleProgressBar cpb = new ConsoleProgressBar(matches.size());
		long t = System.currentTimeMillis();
		for (Match match : matches) {
			cpb.tick();
			allAnnotationsThatCollides.put(match, new ArrayList<Annotation>());

			int start, end;

			if (scope == AnnotationScope.FIRSTTARGET) { // use target if any

				if (match.getTarget() >= 0) {
					start = end = match.getTarget();
				}
				else {
					start = match.getStart();
					end = match.getEnd();
				}
			}
			else if (scope == AnnotationScope.TARGET) {
				if (match.getTarget() >= 0) {
					start = end = match.getTarget();
				}
				else {
					continue; // don't create annotation because there is not target
				}
			}
			else { // FIRST ALL: same behavior
				start = match.getStart();
				end = match.getEnd();
			}

			if (job != null && job.isCanceled()) { // check if user canceled the job
				Log.info(Messages.affectAnnotationCanceled);
				return null;
			}

			try {
				List<Annotation> cqpAnnotations = null;
				if (annotSelectedType.getEffect().equals(AnnotationEffect.SEGMENT)) { // Test possible collisions
					cqpAnnotations = cqpManager.getAnnotations(null, match, null, true); // get all annotations
					// remove A)the wrapping annotations and B) the annotation with same type and same positions
					for (int i = 0; i < cqpAnnotations.size(); i++) {
						Annotation a = cqpAnnotations.get(i);

						// exact match + exact type
						if (a.getType().equals(annotSelectedType.getId()) && a.getStart() == start && a.getEnd() == end) {
							cqpAnnotations.remove(i);
							i--;
						}
						else if (!a.getType().equals(annotSelectedType.getId()) && (// different type and inner or outer wrap
						(a.getStart() <= start && end <= a.getEnd()) ||
								(start <= a.getStart() && a.getEnd() <= end))) {
									cqpAnnotations.remove(i);
									i--;
								}
					}
				}
				else { // no need to test collision (AnnotationType=TOKEN)
					cqpAnnotations = new ArrayList<>();
				}

				if (cqpAnnotations.size() > 0) {
					allAnnotationsThatCollides.get(match).addAll(cqpAnnotations);
					continue; // don't create annotation, process next match
				}
				else { // test with temporary annotation manager
					List<Annotation> tempAnnotations = null;
					if (annotSelectedType.getEffect() == AnnotationEffect.TOKEN) { // only annotate the first word
						if (scope == AnnotationScope.ALL) {
							tempAnnotations = tempManager.createAnnotationNoCommit(annotSelectedType, annotSelectedTypedValue, start, end);
						}
						else { // start have been tempered if necessary
							tempAnnotations = tempManager.createAnnotationNoCommit(annotSelectedType, annotSelectedTypedValue, start, start);
						}
					}
					else {
						tempAnnotations = tempManager.createAnnotationNoCommit(annotSelectedType, annotSelectedTypedValue, start, end);
					}
					if (tempAnnotations.size() > 0)
						allAnnotationsThatCollides.get(match).addAll(tempAnnotations);
				}

				nCreation++;

				if ((nCreation % 1000) == 0) {
					tempManager.getEntityManager().flush(); // warning
					tempManager.getEntityManager().clear(); // warning
				}
				if ((nCreation % 20000) == 0) {
					tempManager.getEntityManager().getTransaction().commit(); // warning
					tempManager.getEntityManager().getTransaction().begin(); // warning
				}

			}
			catch (Throwable e) {
				Log.info(NLS.bind(Messages.errorDuringAnnotationCreationP0, e));
				Log.printStackTrace(e);

				if (tempManager.getEntityManager().getTransaction().isActive()) {
					tempManager.getEntityManager().getTransaction().rollback(); // reset state
				}
			}

			if (allAnnotationsThatCollides.get(match).size() == 0) allAnnotationsThatCollides.remove(match); // keep only colision lists
		}

		cpb.done();
		// System.out.println("T=" + (System.currentTimeMillis() - t));

		dirty = true;
		tempManager.getEntityManager().getTransaction().commit(); // warning
		return allAnnotationsThatCollides;
	}

	public List<Annotation> getAnnotations(AnnotationType type, int start, int end, boolean overlap) {
		List<Annotation> temporaryAnnotations = null;
		List<Annotation> cqpAnnotations = null;
		try {
			temporaryAnnotations = tempManager.getAnnotations(type, Arrays.asList(new Match2P(start, end)), null, false, overlap);
			cqpAnnotations = cqpManager.getAnnotations(type, start, end, overlap);

			int i = 0;
			for (Annotation a : cqpAnnotations) {
				while (temporaryAnnotations.get(i).getStart() < a.getStart()) {
					i++;
				}
				temporaryAnnotations.add(i, a);
			}
		}
		catch (Exception e) {

		}
		return temporaryAnnotations;
	}

	public List<Annotation> getAnnotations(AnnotationType type, boolean overlap) {
		List<Annotation> temporaryAnnotations = null;
		List<Annotation> cqpAnnotations = null;
		try {
			temporaryAnnotations = tempManager.getAnnotations(type, true);
			cqpAnnotations = cqpManager.getAnnotations(type, overlap);

			int i = 0;
			for (Annotation a : cqpAnnotations) {
				while (temporaryAnnotations.get(i).getStart() < a.getStart()) {
					i++;
				}
				temporaryAnnotations.add(i, a);
			}
		}
		catch (Exception e) {

		}
		return temporaryAnnotations;
	}

	public List<Annotation> getAnnotations(AnnotationType type, int start, int end) {
		return getAnnotations(type, start, end, false);
	}

	public void closeAll() {
		Log.fine("Closing annotation manager of " + corpus); //$NON-NLS-1$
		tempManager.close();
		cqpManager.close();
	}

	public boolean isOpen() {
		return tempManager.getEntityManager() != null && tempManager.getEntityManager().isOpen();
	}


	public boolean initialize() throws Exception {
		tempManager = new TemporaryAnnotationManager(corpus);
		dirty = tempManager.getAnnotations().size() > 0;
		cqpManager = new CQPAnnotationManager(corpus);
		return false;
	}

	/**
	 * 
	 * @return true if some annotations are not saved
	 */
	public boolean isDirty() {
		return dirty;
	}

	public boolean exportAnnotationsToTable(File resultFile) throws Exception {

		final String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
		String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
		String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);

		List<String> krnames = KRAnnotationEngine.getKnowledgeRepositoryNames(corpus);
		if (krnames.size() == 0) {
			Log.severe(NLS.bind(Messages.errorNoKRFoundInP0Corpus, corpus));
			throw new IllegalArgumentException("No kr in " + corpus); //$NON-NLS-1$
		}
		String firtsKRName = krnames.get(0);
		KnowledgeRepository defaultKR = KRAnnotationEngine.getKnowledgeRepository(corpus, firtsKRName);
		if (defaultKR == null) {
			Log.severe(NLS.bind(Messages.errorNoKRP0FoundInP1Corpus, defaultKR, corpus));
			throw new IllegalArgumentException("No kr " + defaultKR + " in " + corpus); //$NON-NLS-1$ //$NON-NLS-2$
		}

		PrintWriter writer = IOUtils.getWriter(resultFile, encoding, false);
		String cols[] = { "text_id", "id", "end_id", "start", "end", "type", "value", "mode", "date", "annotator" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
		writer.println(StringUtils.join(cols, colseparator));

		if (this.getTemporaryManager().getAnnotations().size() == 0) {
			Log.warning(Messages.warningNoAnnotationsToExportAborting);
			return false;
		}

		String[] textsIds = corpus.getCorpusTextIdsList();
		int[] textsEndPositions = corpus.getTextEndLimits();

		HashMap<Integer, String> positions2Ids = new HashMap<Integer, String>();
		HashMap<Integer, String> positions2TextIds = new HashMap<Integer, String>();
		for (Annotation a : this.getTemporaryManager().getAnnotations()) {
			positions2Ids.put(a.getStart(), null);
			positions2Ids.put(a.getEnd(), null);
		}
		int[] positions = new int[positions2Ids.keySet().size()];
		int i = 0;
		for (int p : positions2Ids.keySet()) {
			positions[i++] = p;
		}
		String values[] = corpus.getProperty("id").cpos2Str(positions); //$NON-NLS-1$
		for (i = 0; i < positions.length; i++) {
			positions2Ids.put(positions[i], values[i]);
		}

		List<Annotation> annotations = this.getTemporaryManager().getAnnotations();
		annotations.sort(new Comparator<Annotation>() {

			@Override
			public int compare(Annotation arg0, Annotation arg1) {
				return arg0.getStart() - arg1.getStart();
			}
		});

		Log.info(NLS.bind(Messages.exportingP0Annotations, annotations.size()));
		ConsoleProgressBar cpb = new ConsoleProgressBar(annotations.size());

		int currentText = 0;
		for (Annotation a : annotations) {
			cpb.tick();
			// text_id	id	start	end	type	value	effect	date	annotator
			String effect = defaultKR.getType(a.getType()).getEffect().name();
			String startId = positions2Ids.get(a.getStart());
			String endId = positions2Ids.get(a.getEnd());

			while (textsEndPositions[currentText] < a.getStart()) {
				// System.out.println("currentText="+currentText+" a="+a.getStart()+" text start="+textsEndPositions[currentText]+" text id="+textsIds[currentText]);
				currentText++;// go to next text
			}
			String textid = textsIds[currentText];

			writer.println(
					txtseparator + textid.replace(txtseparator, txtseparator + txtseparator) + txtseparator
							+ colseparator + txtseparator + startId.replace(txtseparator, txtseparator + txtseparator) + txtseparator
							+ colseparator + txtseparator + endId.replace(txtseparator, txtseparator + txtseparator) + txtseparator
							+ colseparator + txtseparator + a.getStart() + txtseparator
							+ colseparator + txtseparator + a.getEnd() + txtseparator
							+ colseparator + txtseparator + a.getType().replace(txtseparator, txtseparator + txtseparator) + txtseparator
							+ colseparator + txtseparator + a.getValue().replace(txtseparator, txtseparator + txtseparator) + txtseparator
							+ colseparator + txtseparator + effect.replace(txtseparator, txtseparator + txtseparator) + txtseparator
							+ colseparator + txtseparator + a.getDate().replace(txtseparator, txtseparator + txtseparator) + txtseparator
							+ colseparator + txtseparator + a.getAnnotator().replace(txtseparator, txtseparator + txtseparator) + txtseparator);
		}
		writer.close();
		cpb.done();
		return true;
	}

	public boolean importAnnotationsFromTable(File annotationsFile) throws Exception {

		if (!isOpen()) {
			this.initialize();
		}

		final String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
		String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
		String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);

		List<String> krnames = KRAnnotationEngine.getKnowledgeRepositoryNames(corpus);
		if (krnames.size() == 0) {
			Log.severe(NLS.bind(Messages.errorNoKRFoundInP0Corpus, corpus));
			throw new IllegalArgumentException("No kr in " + corpus); //$NON-NLS-1$
		}
		String firtsKRName = krnames.get(0);
		KnowledgeRepository defaultKR = KRAnnotationEngine.getKnowledgeRepository(corpus, firtsKRName);
		if (defaultKR == null) {
			Log.severe(NLS.bind(Messages.errorNoKRP0FoundInP1Corpus, defaultKR, corpus));
			throw new IllegalArgumentException("No KR " + defaultKR + " in " + corpus); //$NON-NLS-1$ //$NON-NLS-2$
		}

		CsvReader reader = new CsvReader(annotationsFile.getAbsolutePath(), colseparator.charAt(0), Charset.forName(encoding));
		if (txtseparator.length() > 0) reader.setTextQualifier(txtseparator.charAt(0));
		reader.readHeaders();
		String headers[] = reader.getHeaders();
		String cols[] = { "text_id", "id", "end_id", "start", "end", "type", "value", "mode", "date", "annotator" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
		if (headers.length != cols.length) {
			Log.warning(NLS.bind(Messages.errorAnnotationFileHeaderFormatIsNotWekkfomattedP0ItShouldBeP1, StringUtils.join(headers, ", "), StringUtils.join(cols, ", "))); //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			return false;
		}
		for (int i = 0; i < cols.length; i++) {
			if (!(headers[i].equals(cols[i]))) {
				Log.warning(NLS.bind(Messages.errorAnnotationFileHeaderFormatIsNotWekkfomattedP0ItShouldBeP1, StringUtils.join(headers, ", "), StringUtils.join(cols, ", "))); //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				return false;
			}
		}

		int nLines = 0;
		while (reader.readRecord()) {
			nLines++;
		}
		reader.close();

		if (nLines == 0) {
			Log.warning(Messages.errorNoAnnotationLineFound);
			return false;
		}

		Log.info(NLS.bind(Messages.importingP0Annotations, nLines));
		reader = new CsvReader(annotationsFile.getAbsolutePath(), colseparator.charAt(0), Charset.forName(encoding));
		if (txtseparator.length() > 0) reader.setTextQualifier(txtseparator.charAt(0));
		reader.readHeaders();
		ConsoleProgressBar cpb = new ConsoleProgressBar(nLines);
		this.tempManager.getEntityManager().getTransaction().begin();
		while (reader.readRecord()) {
			cpb.tick();
			String[] record = reader.getValues();
			String typeName = record[5].trim();
			String typeEffect = record[7].trim();
			if (defaultKR.getType(typeName) == null) {
				defaultKR.addType(typeName, typeName, null, AnnotationEffect.valueOf(typeEffect));
			}
			AnnotationType type = defaultKR.getType(typeName);
			String startId = record[1].trim();
			String endId = record[2].trim();
			int startPos = -1;
			int endPos = -1;
			if (startId.length() > 0 && endId.length() > 0) { // use the word id if present
				String textId = record[0].trim();
				if (startId.equals(endId)) {
					CQLQuery cql = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", startId, CQLQuery.addBackSlash(textId))); //$NON-NLS-1$
					QueryResult qr = corpus.query(cql, "TMP", false); //$NON-NLS-1$
					if (qr.getNMatch() == 0) {
						Log.warning(NLS.bind(Messages.noMatchFoundIdP0InTextP1, cql));
						continue;
					}
					startPos = qr.getMatch(0).getStart();
					endPos = startPos;
				}
				else {
					CQLQuery cql = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", startId, CQLQuery.addBackSlash(textId))); //$NON-NLS-1$
					QueryResult qr = corpus.query(cql, "TMP", false); //$NON-NLS-1$
					if (qr.getNMatch() == 0) {
						Log.warning(NLS.bind(Messages.noMatchFoundIdP0InTextP1, cql));
						continue;
					}
					startPos = qr.getMatch(0).getStart();
					qr.drop();
					cql = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", endId, CQLQuery.addBackSlash(textId))); //$NON-NLS-1$
					qr = corpus.query(cql, "TMP", false); //$NON-NLS-1$
					if (qr.getNMatch() == 0) {
						Log.warning(NLS.bind(Messages.noMatchFoundIdP0InTextP1, cql));
						continue;
					}
					endPos = qr.getMatch(0).getStart();
					qr.drop();
				}
			}
			else {
				startPos = Integer.parseInt(record[3]);
				endPos = Integer.parseInt(record[4]);

			}
			if (type.getTypedValue(record[6]) == null) {
				defaultKR.addValue(record[6], record[6], typeName);
			}
			TypedValue value = type.getTypedValue(record[6]);
			List<Annotation> a = this.tempManager.createAnnotationNoCommit(type, value, startPos, endPos);
		}
		this.tempManager.getEntityManager().getTransaction().commit();
		cpb.done();

		return true;
	}
}
