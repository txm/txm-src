package org.txm.utils;

import org.apache.tools.ant.taskdefs.condition.Os;
import org.txm.utils.messages.UtilsCoreMessages;

/**
 * Utility class dedicated to operating system detection of the runtime target.
 * 
 * @author sjacquot, mdecorde
 *
 */
public class OSDetector {

	/**
	 * 
	 * @return User name + OS infos
	 */
	public static String getUserAndOSInfos() {
		return UtilsCoreMessages.bind(UtilsCoreMessages.P0OnP1, System.getProperty("user.name"), System.getProperty("os.name") + " " + System.getProperty("os.version") + " " + System.getProperty("os.arch")); //$NON-NLS-1$
	}

	/**
	 * Checks if the operating system of the runtime target is of Windows family
	 * type.
	 * 
	 * @return true if the operating system of the runtime target is of Windows
	 *         family type
	 */
	public static boolean isFamilyWindows() {
		return Os.isFamily(Os.FAMILY_WINDOWS);
	}

	/**
	 * Checks if the operating system of the runtime target is of Unix family
	 * type.
	 * 
	 * @return true if the operating system of the runtime target is of Unix
	 *         family type
	 */
	public static boolean isFamilyUnix() {
		return Os.isFamily(Os.FAMILY_UNIX);
	}

	/**
	 * Checks if the operating system of the runtime target is of Mac family
	 * type.
	 * 
	 * @return true if the operating system of the runtime target is of Mac
	 *         family type
	 */
	public static boolean isFamilyMac() {
		return Os.isFamily(Os.FAMILY_MAC);
	}

	/**
	 * Checks if the operating system of the runtime target is 64bit
	 * architecture.
	 * 
	 * @return
	 */
	public static boolean isArch64() {
		return System.getProperty("java.vm.name").toLowerCase().indexOf("64") >= 0; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Test method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Runtime OS name: " + System.getProperty("os.name")); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("Runtime OS arch: " + System.getProperty("os.arch")); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("Runtime OS version: " + System.getProperty("os.version")); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("Is arch 64bit ?: " + isArch64()); //$NON-NLS-1$
		System.out.println("Windows OS family: " + isFamilyWindows()); //$NON-NLS-1$
		System.out.println("Unix OS family: " + isFamilyUnix()); //$NON-NLS-1$
		System.out.println("Mac OS family: " + isFamilyMac()); //$NON-NLS-1$
	}

}
