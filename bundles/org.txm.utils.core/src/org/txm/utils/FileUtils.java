package org.txm.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;

public class FileUtils {

	public static boolean isFilenameValid(String file) {
	    File f = new File(file);
	    try {
	       f.getCanonicalPath();
	       return true;
	    }
	    catch (IOException e) {
	       return false;
	    }
	  }
	
	public static boolean isExtension(File f, String ext) {
		return FilenameUtils.isExtension(f.getName().toLowerCase(), ext.toLowerCase());
	}

	public static boolean isExactlyExtension(File f, String ext) {
		return FilenameUtils.isExtension(f.getName(), ext);
	}

	public static boolean isXMLFile(File f) {
		return isExtension(f, "xml"); //$NON-NLS-1$
	}

	public static boolean isTXTFile(File f) {
		return isExtension(f, "txt"); //$NON-NLS-1$
	}

	public static boolean isXSLFile(File f) {
		return isExtension(f, "xsl"); //$NON-NLS-1$
	}

	public static boolean isJARFile(File f) {
		return isExtension(f, "jar"); //$NON-NLS-1$
	}

	public static String getExtension(File f) {
		return FilenameUtils.getExtension(f.getName());
	}

	
	public static String getExtension(String filepath) {
		return FilenameUtils.getExtension(filepath);
	}

	/**
	 * 
	 * @param f
	 * @return the file name without its extension
	 */
	public static String stripExtension(File f) {
		return FilenameUtils.getBaseName(f.getName());
	}

	/**
	 * 
	 * @param f
	 * @return the file name without its extension
	 */
	public static String stripExtension(String filename) {
		return FilenameUtils.getBaseName(filename);
	}

	/**
	 * select file and directories ignore hidden files
	 * 
	 * @param file a directory or a file (its parent directory will be used)
	 * @return list of files, null if the file does not exists
	 */
	public static File[] listFiles(File file) {
		return listFiles(file, null, true, false, true);
	}
	
	/**
	 * select file and directories ignore hidden files
	 * 
	 * @param file
	 *            a directory or a file (its parent directory will be used)
	 * @return list of files, null if the file does not exists
	 */
	public static File[] listFiles(File file, String pattern) {
		return listFiles(file, pattern, true, false, true);
	}
	
	/**
	 * select file and directories ignore hidden files
	 * 
	 * @param file a directory or a file (its parent directory will be used)
	 * @return list of directories, null if the file does not exists
	 */
	public static File[] listDirectories(File file) {
		return listFiles(file, null, false, true, true); 
	}

	/**
	 * select file and directories ignore hidden files
	 * 
	 * @param file a directory or a file (its parent directory will be used)
	 * @return list of directories, null if the file does not exists
	 */
	public static File[] listDirectories(File file, String pattern) {
		return listFiles(file, pattern, false, true, true); 
	}
	
	/**
	 * select only files not hidden
	 */
	public static final FileFilter REMOVE_HIDDEN_FILES_FILTER = new FileFilter() {
		@Override
		public boolean accept(File file) {
			return !file.isHidden() && !file.getName().startsWith(".") && !file.getName().endsWith("~"); //$NON-NLS-1$
		}
	};

	/**
	 * 
	 * @return a list containing the files not hidden
	 */
	public static List<File> listFilesAsList(File dir) {

		File[] files = dir.listFiles(REMOVE_HIDDEN_FILES_FILTER);
		if (files == null)
			return new ArrayList<File>();

		return Arrays.asList(files);
	}

	/**
	 * 
	 * @param file a directory or a file (its parent directory will be used)
	 * @param namePattern null or filename regex
	 * @param fileOnly
	 * @return list of files, null if the file does not exists
	 */
	public static File[] listFiles(File file, String namePattern, boolean listFiles, boolean listDirectories, boolean ignoreHiddenFiles) {

		if (!file.isDirectory()) {
			file = file.getParentFile();
		}
		if (file == null) {
			return new File[0];
		}

		File[] files = file.listFiles(new PatternFilter(namePattern, listFiles, listDirectories, ignoreHiddenFiles));

		if (files != null) {
			Arrays.sort(files); // always sort files by name
			return files;
		} else {
			return new File[0];
		}
	}

	public static class PatternFilter implements FileFilter {

		public Pattern mNamePattern = null;
		boolean listFiles, listDirectories;
		boolean ignoreHiddenFiles;

		public PatternFilter(String pattern, boolean listFiles, boolean listDirectories, boolean ignoreHiddenFiles) {
			if (pattern != null && pattern.length() > 0) {
				mNamePattern = Pattern.compile(pattern);
			}
			this.ignoreHiddenFiles = ignoreHiddenFiles;
			this.listFiles = listFiles;
			this.listDirectories = listDirectories;
		}

		@Override
		public boolean accept(File file) {
			
			if (!ignoreHiddenFiles && !REMOVE_HIDDEN_FILES_FILTER.accept(file))
				return false; // hidden test failed

			if (mNamePattern != null && !mNamePattern.matcher(file.getName()).matches())
				return false; // name test failed

			if (file.isFile() && listFiles) return true;
			else if (file.isDirectory() && listDirectories) return true;

			return false; // test the whole filename
		}
	}

	public static void main(String[] args) {
		File f = new File("test.gz.Txt"); //$NON-NLS-1$
		System.out.println("base=" + stripExtension(f)); //$NON-NLS-1$
		System.out.println("ext=" + getExtension(f)); //$NON-NLS-1$
		System.out.println("is txt=" + isExtension(f, "txt")); //$NON-NLS-1$
		
		System.out.println("List files=" + Arrays.toString( listFiles(new File(System.getProperty("user.home"))))); //$NON-NLS-1$
		System.out.println("List files '*.txt'=" + Arrays.toString( listFiles(new File(System.getProperty("user.home")), ".*\\.txt"))); //$NON-NLS-1$
		System.out.println("List directories=" + Arrays.toString( listDirectories(new File(System.getProperty("user.home"))))); //$NON-NLS-1$
		
	}
}
