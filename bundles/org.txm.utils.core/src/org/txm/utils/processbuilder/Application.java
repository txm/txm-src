// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu., 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.utils.processbuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

// TODO: Auto-generated Javadoc
/**
 * The Class Application.
 */
public class Application {

	/** The definition file. */
	File definitionFile;

	/** The name. */
	String name;

	/** The version. */
	String version;

	/** The desc. */
	String desc;

	/** The params. */
	HashSet<Param> params = new HashSet<Param>();

	/** The executables. */
	HashMap<String, Executable> executables = new HashMap<String, Executable>();

	/**
	 * Instantiates a new application.
	 *
	 * @param definitionFile
	 *            the definition file
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws SAXException
	 *             the sAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Application(File definitionFile) throws ParserConfigurationException, SAXException, IOException {
		this.definitionFile = definitionFile;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(definitionFile);
		Element root = doc.getDocumentElement();

		name = root.getAttribute("name"); //$NON-NLS-1$
		version = root.getAttribute("version"); //$NON-NLS-1$
		desc = root.getAttribute("desc"); //$NON-NLS-1$

		// pr chq exec
		// lit param
		NodeList nodeLst = doc.getElementsByTagName("prog"); //$NON-NLS-1$
		for (int s = 0; s < nodeLst.getLength(); s++) {
			Element prog = (Element) nodeLst.item(s);
			Executable exec = new Executable(this, prog);
			executables.put(exec.name, exec);
			params.addAll(exec.params);
		}
	}

	/**
	 * Builds the.
	 *
	 * @param outFile
	 *            the out file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws SAXException
	 *             the sAX exception
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 */
	public void build(File outFile) throws IOException, SAXException, ParserConfigurationException {
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"); //$NON-NLS-1$

		writer.write("import java.util.Date;\n"); //$NON-NLS-1$
		writer.write("import java.util.Locale;\n"); //$NON-NLS-1$
		writer.write("import java.text.DateFormat;\n"); //$NON-NLS-1$
		writer.write("import java.io.BufferedReader;\n"); //$NON-NLS-1$
		writer.write("import java.io.IOException;\n"); //$NON-NLS-1$
		writer.write("import java.io.InputStream;\n"); //$NON-NLS-1$
		writer.write("import java.io.InputStreamReader;\n"); //$NON-NLS-1$
		writer.write("import java.util.ArrayList;\n"); //$NON-NLS-1$

		writer.write("\n// " + desc + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("class " + name + "\n{\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\tpublic String binpath = \"\";\n"); //$NON-NLS-1$
		writer.write("\tboolean debug = false;\n"); //$NON-NLS-1$
		writer.write("\tpublic  String version = \"" + version + "\";\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\tpublic String desc = \"" + desc + "\";\n"); //$NON-NLS-1$ //$NON-NLS-2$

		writer.write("\n\tpublic " + name + "(String binpath){this.binpath=binpath;}\n\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\tpublic void debug(boolean b){debug=b;};\n\n"); //$NON-NLS-1$

		writer.write("//PARAMETERS DECLARATION\n"); //$NON-NLS-1$
		for (Param p : params) {
			p.declare(writer);
		}
		writer.write("//EXECUTABLES DECLARATION\n"); //$NON-NLS-1$
		for (Executable exec : executables.values()) {
			exec.build(writer);
		}

		writer.write("\tpublic static void main(String[] args) \n"); //$NON-NLS-1$
		writer.write("\t{\n"); //$NON-NLS-1$
		writer.write("\t\t" + name + " tt = new " + name + "(\"thepathtobinariesfolder\");\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		writer.write("\t}\n"); //$NON-NLS-1$

		writer.write("}"); //$NON-NLS-1$
		writer.close();
	}

	/**
	 * Describe.
	 *
	 * @return the string
	 */
	public String describe() {
		StringBuffer buff = new StringBuffer();
		buff.append("App: " + name + ", version: " + version + ", desc: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ desc + "\n"); //$NON-NLS-1$
		buff.append(" All Params: " + params + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		for (Executable exec : executables.values())
			buff.append(exec.describe());
		return buff.toString();
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String args[]) {
		try {
			Application app = new Application(new File("~/workspace/org.txm.core/src/groovy/org/textometrie/scripts/ls-wrapper-definition.xml")); //$NON-NLS-1$
			app.build(new File("~/Bureau/testClix.java")); //$NON-NLS-1$
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
