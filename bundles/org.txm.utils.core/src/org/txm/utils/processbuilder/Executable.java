// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.processbuilder;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// TODO: Auto-generated Javadoc
/**
 * The Class Executable.
 */
public class Executable {

	/** The root. */
	Element root;

	/** The app. */
	Application app;

	/** The name. */
	String name;

	/** The version. */
	String version;

	/** The desc. */
	String desc;

	/** The params. */
	ArrayList<Param> params = new ArrayList<Param>();

	/** The optionals. */
	HashSet<Param> optionals = new HashSet<Param>();

	/** The musts. */
	HashSet<Param> musts = new HashSet<Param>();

	/** The multiples. */
	HashSet<Param> multiples = new HashSet<Param>();

	/**
	 * Instantiates a new executable.
	 *
	 * @param app
	 *            the app
	 * @param root
	 *            the root
	 */
	public Executable(Application app, Element root) {
		this.root = root;
		this.app = app;

		name = root.getAttribute("exec"); //$NON-NLS-1$
		version = root.getAttribute("version"); //$NON-NLS-1$
		desc = root.getAttribute("desc"); //$NON-NLS-1$

		NodeList argss = root.getElementsByTagName("args"); //$NON-NLS-1$
		Node args = argss.item(0);
		NodeList childs = args.getChildNodes();

		for (int s = 0; s < childs.getLength(); s++) {
			Node child = childs.item(s);
			if (child.getNodeType() == 1) {
				Element param = (Element) child;
				Param p = new Param(this, param);
				addParam(p);
			}
		}
	}

	/**
	 * Adds the param.
	 *
	 * @param param
	 *            the param
	 */
	public void addParam(Param param) {
		params.add(param);
		if (param.state.equals("must")) //$NON-NLS-1$
		{
			musts.add(param);
		} else if (param.state.equals("optional")) //$NON-NLS-1$
		{
			optionals.add(param);
		} else if (param.state.equals("multiple")) //$NON-NLS-1$
		{
			multiples.add(param);
		}
	}

	/**
	 * Builds the.
	 *
	 * @param writer
	 *            the writer
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String build(Writer writer) throws IOException {
		// NodeList nodeLst = doc.getElementsByTagName("prog"); //$NON-NLS-1$
		ArrayList<String> dejavu = new ArrayList<String>();

		HashMap<String, String> atester = new HashMap<String, String>();

		// Function header
		String func = "\t// " + desc; //$NON-NLS-1$
		for (Param p : musts)
			func += "\n\t// arg " + p.name + ": " + p.desc + "\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		func += "\n\tpublic void " + name.replace("-", "").replace(".", "") + "("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		for (Param p : musts)
			func = func + p.type + " " + p.name.replace("-", "") + ", "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		if (musts.size() > 0)
			func = func.substring(0, func.length() - 2) + ")"; //$NON-NLS-1$
		else
			func = func + ")"; //$NON-NLS-1$

		// musts
		for (Param p : musts) {
			writer.write("\t\targs.add(\"\"+" + p.name.replace("-", "") + ");\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}

		writer.write("\n" + func + " throws IOException\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\t{\n"); //$NON-NLS-1$
		writer.write("\t\tArrayList<String> args = new ArrayList<String>();\n"); //$NON-NLS-1$
		writer.write("\t\targs.add(binpath+\"" + name + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$

		// test set parameters
		// optionals&multiple
		for (Param p : params) {
			System.out.println("test param " + p.state); //$NON-NLS-1$
			if (p.state.equals("optional")) { //$NON-NLS-1$
				writer.write("\t\tif (is" + p.name.replace("-", "") + ")\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				if (!p.type.equals("none")) { //$NON-NLS-1$
					writer.write("\t\t\t{args.add(\"-" + p.name + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$
					writer.write("\t\t\targs.add(\"\"+" + p.name.replace("-", "") + ");}\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				} else {
					writer.write("\t\t\targs.add(\"-" + p.name + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$
				}
			} else if (p.state.equals("multiple")) { //$NON-NLS-1$
				writer.write("\t\tif (is" + p.name.replace("-", "") + ")\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				writer.write("\t\t\tfor(int c = 0;c < " + p.name.replace("-", "") + ".size();c++)\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				writer.write("\t\t\t{args.add(\"-" + p.name + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$
				writer.write("\t\t\targs.add(\"\"+" + p.name.replace("-", "") + ".get(c));}\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
		}

		writer.write("\n\t\tProcessBuilder pb = new ProcessBuilder(args);\n" + //$NON-NLS-1$
				"\t\tpb.redirectErrorStream(true);\n" + //$NON-NLS-1$
				"\t\tProcess process = null;\n" + //$NON-NLS-1$
				"\t\ttry {\n\t\t\tprocess = pb.start();\n\t\t} catch (IOException e) {\n\t\t\tSystem.err.println(e);\n\t\t}\n"); //$NON-NLS-1$
		writer.write("\t\tInputStream is = process.getInputStream();\n"); //$NON-NLS-1$
		writer.write("\t\tInputStreamReader isr = new InputStreamReader(is);\n"); //$NON-NLS-1$
		writer.write("\t\tBufferedReader br = new BufferedReader(isr);\n"); //$NON-NLS-1$
		writer.write("\t\tString line;\n"); //$NON-NLS-1$
		writer.write("\t\twhile ((line = br.readLine()) != null)\n\t\t{\n\t\t\tif (debug)\n\t\t\t\tSystem.out.println(line);\n\t\t}\n"); //$NON-NLS-1$
		writer.write("\t\tint e=0;\n\t\ttry {e = process.waitFor();}catch(Exception err){}\n\t\tif (e != 0) {\n\t\t\t" + //$NON-NLS-1$
				"System.err.println(\"Process exited abnormally with code \"+e+\" at \"+" //$NON-NLS-1$
				+ "DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date()));"); //$NON-NLS-1$
		writer.write("\n\t\t\t"); //$NON-NLS-1$
		writer.write("for(int c=0;c <args.size();c++)System.out.print(\"\"+args.get(c)+\" \");\n"); //$NON-NLS-1$
		writer.write("\t\t\tSystem.out.println();\n\t\t}\n\t}\n"); //$NON-NLS-1$

		return ""; //$NON-NLS-1$
	}

	/**
	 * Describe.
	 *
	 * @return the string
	 */
	public String describe() {
		StringBuffer buff = new StringBuffer();
		buff.append(" Exec: " + name + ", version: " + version + ", desc: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ desc + "\n"); //$NON-NLS-1$
		buff.append("  Optionals: " + optionals + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buff.append("  Musts: " + musts + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buff.append("  Multiples: " + multiples + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		for (Param param : params)
			buff.append(param.describe());
		return buff.toString();
	}
}
