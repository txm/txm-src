// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.processbuilder;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.w3c.dom.Element;

// TODO: Auto-generated Javadoc
/**
 * The Class Param.
 */
public class Param {

	/** The root. */
	Element root;

	/** The exec. */
	Executable exec;

	/** The name. */
	String name;

	/** The type. */
	String type;

	/** The state. */
	String state;

	/** The desc. */
	String desc;

	/** The prefix. */
	String prefix;

	/**
	 * Instantiates a new param.
	 *
	 * @param exec
	 *            the exec
	 * @param root
	 *            the root
	 */
	public Param(Executable exec, Element root) {

		this.root = root;
		this.exec = exec;

		name = root.getAttribute("name"); //$NON-NLS-1$
		type = root.getAttribute("type"); //$NON-NLS-1$
		state = root.getAttribute("state");//$NON-NLS-1$
		desc = root.getAttribute("desc"); //$NON-NLS-1$
		prefix = root.getAttribute("prefix");//$NON-NLS-1$
	}

	/**
	 * Builds the.
	 *
	 * @param writer
	 *            the writer
	 */
	public void build(Writer writer) {
		/*
		 * for(int i=0;i < children.getLength();i++) { Element child =
		 * (Element)children.item(i); String name = child.getAttribute("name");
		 * //$NON-NLS-1$ String type = child.getAttribute("type"); //$NON-NLS-1$
		 * String desc = child.getAttribute("desc"); //$NON-NLS-1$ String state
		 * = child.getAttribute("state"); //$NON-NLS-1$ if
		 * (!order.contains(name)) order.add(name);
		 * 
		 * }
		 */
	}

	/**
	 * Write test.
	 *
	 * @return the string
	 */
	public String writeTest() {
		return ""; //$NON-NLS-1$
	}

	/**
	 * Write definition.
	 *
	 * @return the string
	 */
	public String writeDefinition() {
		return ""; //$NON-NLS-1$

	}

	/**
	 * Write xml report.
	 *
	 * @return the string
	 */
	public String writeXmlReport() {
		return ""; //$NON-NLS-1$
	}

	/**
	 * Describe.
	 *
	 * @return the string
	 */
	public String describe() {
		/*
		 * StringBuffer buff = new StringBuffer();
		 * buff.append("App: "+name+", version: "+version+", desc: "+desc+"\n");
		 * for(Executable exec : executables.values())
		 * buff.append(exec.describe()); return buff.toString();
		 */
		return "  Param: " + prefix + name + ", type: " + type + ", state: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ state + ",desc: " + desc + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Equals.
	 *
	 * @param param
	 *            the param
	 * @return true, if successful
	 */
	public boolean equals(Param param) {
		return this.name.equals(param.name) && this.type.equals(param.type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.prefix + this.name + ":" + this.type; //$NON-NLS-1$
	}

	/**
	 * Declare.
	 *
	 * @param writer
	 *            the writer
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void declare(OutputStreamWriter writer) throws IOException {
		if (state.equals("optional")) //$NON-NLS-1$
		{
			writer.write("\t// " + desc + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.write("\tprivate Boolean is" + name.replace("-", "") + " = false;\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			if (!type.equals("none")) //$NON-NLS-1$
			{
				writer.write("\tprivate " + type + " " + name.replace("-", "") + ";\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				writer.write("\tpublic void set" + name.replace("-", "") + "(" + type + " arg)\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
						"\tthis.is" //$NON-NLS-1$
						+ name.replace("-", "") + "=true;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"\t\tthis." //$NON-NLS-1$
						+ name.replace("-", "") + "=arg;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"\t}\n" //$NON-NLS-1$
				);
			} else {
				writer.write("\tpublic void set" + name.replace("-", "") + "(){" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						"\tthis.is" //$NON-NLS-1$
						+ name.replace("-", "") + "=true;\t}\n" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				);
			}

			writer.write("\tpublic void unset" + name.replace("-", "") + "(){" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					"\tthis.is" + name.replace("-", "") + "=false;\t}\n" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			);
		} else if (state.equals("multiple")) //$NON-NLS-1$
		{
			writer.write("\n\t// " + desc + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.write("\tprivate Boolean is" + name.replace("-", "") + " = false;\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			if (!type.equals("none")) //$NON-NLS-1$
			{
				writer.write("\tprivate List<" + type + "> " + name + ";\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				writer.write("\tpublic void set" + name.replace("-", "") + "(List<" + type + "> arg)\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
						"\tthis.is" //$NON-NLS-1$
						+ name.replace("-", "") + "=true;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"\t\tthis." //$NON-NLS-1$
						+ name.replace("-", "") + "=arg;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"\t}\n" //$NON-NLS-1$
				);
			} else {
				writer.write("\tpublic void set" + name.replace("-", "") + "(){" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						"\tthis.is" //$NON-NLS-1$
						+ name.replace("-", "") + "=true;\t}\n" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				);
			}

			writer.write("\tpublic void unset" + name.replace("-", "") + "(){" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					"\tthis.is" + name.replace("-", "") + "=false;\t}\n" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			);
		}
		writer.write("\n"); //$NON-NLS-1$
	}
}
