// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.processbuilder;

import java.io.File;

import org.txm.utils.ProcessBuilderBuilder;

import groovy.lang.GroovyClassLoader;

/**
 * creates a wrapper of a program.
 *
 * @author mdecorde
 */
public class ProcessBuilder {
	public static Object wrapProgram(File definitionFile, File classFile, File binariesDirectory) {
		try {
			ProcessBuilderBuilder.build(definitionFile, classFile);
			// Application app = new Application(definitionFile);
			// app.build(classFile);
		} catch (Exception e) {
			System.err.println(e);
			return null;
		}

		GroovyClassLoader gcl = new GroovyClassLoader();
		Object aProgram;

		try {
			gcl.addClasspath("."); //$NON-NLS-1$
			Class<?> clazz = gcl.parseClass(classFile);
			aProgram = clazz.getConstructor(String.class).newInstance(binariesDirectory.getAbsolutePath()); // path
																											// to
																											// the
																											// directory
																											// containing
																											// binaries
																											// //$NON-NLS-1$

		} catch (Exception e) {
			System.err.println(e);
			return null;
		}
		return aProgram;
	}
}
