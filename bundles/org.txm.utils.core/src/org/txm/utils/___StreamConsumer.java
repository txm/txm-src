package org.txm.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.txm.utils.logger.Log;

/**
 * Consumer for dumping stdout and stderr from a native process.
 * 
 * @author sjacquot
 *
 */
public class ___StreamConsumer extends Thread {

	InputStream is;
	int type;

	public ___StreamConsumer(InputStream is, int type) {
		this.is = is;
		this.type = type;
	}

	@Override
	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(this.is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				if (this.type == 0) {
					Log.finest(line);
				} else {
					Log.severe(line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}