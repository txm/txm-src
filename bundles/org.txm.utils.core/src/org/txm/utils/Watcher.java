// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class Watcher.
 *
 * @author mdecorde
 * 
 *         Watch for an external tool (such as CWB or R) to be OK and try to
 *         restart it
 */
public abstract class Watcher implements Runnable {

	/** The process. */
	boolean process = true;

	/** The processname. */
	protected String processname = "noname"; //$NON-NLS-1$

	/** The restartcount. */
	int restartcount = 0;

	/** The MAXRESTARTCOUNT. */
	static int MAXRESTARTCOUNT = 10;

	/** The DELAY. */
	static int DELAY = 10000;

	/** The DEBUG. */
	static boolean DEBUG = true;

	/**
	 * Sets the debug.
	 *
	 * @param state
	 *            the new debug
	 */
	public static void setDebug(boolean state) {
		DEBUG = state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		while (process) {
			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
				return;
			}
			if (DEBUG) {
				System.out.println("check " + processname); //$NON-NLS-1$
			}
			if (!check()) {
				if (DEBUG) {
					System.out.println("restart " + processname); //$NON-NLS-1$
				}
				if (!restart()) {
					System.out.println("Failed to restart : " + processname); //$NON-NLS-1$
					return;
				}

				restartcount++;
				if (restartcount >= MAXRESTARTCOUNT) {
					System.out.println(processname + " have been restarted " + MAXRESTARTCOUNT + " times, You should restart TXM."); //$NON-NLS-1$ //$NON-NLS-2$
					return;
				}
			}
		}
	}

	/**
	 * Check.
	 *
	 * @return true, if successful
	 */
	abstract protected boolean check();

	/**
	 * Restart.
	 *
	 * @return true, if successful
	 */
	abstract protected boolean restart();
}
