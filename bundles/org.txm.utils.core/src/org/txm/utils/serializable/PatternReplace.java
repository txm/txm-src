package org.txm.utils.serializable;

import java.io.Serializable;
import java.util.regex.Pattern;

public record PatternReplace(Pattern pattern, String replace) implements Serializable {
		
		@Override
		public String toString() {
			return pattern.pattern() + " -> "+replace; //$NON-NLS-N$
		}
	};