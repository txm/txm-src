package org.txm.utils.serializable;

import java.io.Serializable;

public record MinMax(Number min, Number max) implements Serializable {
};