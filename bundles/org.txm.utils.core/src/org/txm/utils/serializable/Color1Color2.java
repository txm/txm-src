package org.txm.utils.serializable;

import java.awt.Color;
import java.io.Serializable;

public record Color1Color2(Color color1, Color color2) implements Serializable {
};