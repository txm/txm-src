package org.txm.utils.serializable;

import java.io.Serializable;

public record StringStringTest(String value, String pattern) implements Serializable {
};