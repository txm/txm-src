package org.txm.utils.serializable;

import java.io.Serializable;

public record ValueTest(Number value, Number test) implements Serializable {
};