package org.txm.utils.serializable;

import java.io.Serializable;

public record StringStringTestReplace(String value, String pattern, String replace) implements Serializable {
};