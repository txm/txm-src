package org.txm.utils.serializable;

import java.io.Serializable;

public record TestReplace(String pattern, String replace) implements Serializable {
};