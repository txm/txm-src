package org.txm.utils.serializable;

import java.io.Serializable;

public record ValueMinMax(Number value, Number min, Number max) implements Serializable {
};