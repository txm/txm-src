package org.txm.utils.serializable;

import java.io.Serializable;

public record Any() implements Serializable {
};