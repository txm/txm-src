package org.txm.utils.serializable;

import java.io.Serializable;
import java.util.regex.Pattern;

public record StringPatternReplace(String value, Pattern pattern, String replace) implements Serializable {
};