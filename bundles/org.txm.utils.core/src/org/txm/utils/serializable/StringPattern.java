package org.txm.utils.serializable;

import java.io.Serializable;
import java.util.regex.Pattern;

public record StringPattern(String value, Pattern pattern) implements Serializable {
	};