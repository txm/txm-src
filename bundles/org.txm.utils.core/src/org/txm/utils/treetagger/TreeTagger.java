// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.treetagger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.txm.utils.logger.Log;

// Tag files
/**
 * The Class TreeTagger.
 */
public class TreeTagger {

	/** The binpath. */
	String binpath = ""; //$NON-NLS-1$
	String[] options;

	/**
	 * Instantiates a new tree tagger.
	 *
	 * @param binpath
	 *            the binpath
	 */
	public TreeTagger(String binpath, String[] options) {
		this.binpath = binpath;
		this.options = options;
	}

	/** The version. */
	String version = "0.0.0"; //$NON-NLS-1$

	/** The desc. */
	String desc = "Tag files"; //$NON-NLS-1$

	/** The debug. */
	boolean debug = false;

	/**
	 * Debug.
	 *
	 * @param b
	 *            the b
	 */
	public void debug(boolean b) {
		debug = b;
	};

	// Print the token
	/** The istoken. */
	private Boolean istoken = false;

	/**
	 * Settoken.
	 */
	public void settoken() {
		this.istoken = true;
	}

	/**
	 * Unsettoken.
	 */
	public void unsettoken() {
		this.istoken = false;
	}

	// Print the lemma
	/** The islemma. */
	private Boolean islemma = false;

	/**
	 * Setlemma.
	 */
	public void setlemma() {
		this.islemma = true;
	}

	/**
	 * Unsetlemma.
	 */
	public void unsetlemma() {
		this.islemma = false;
	}

	// Don't tag SGML annotations
	/** The issgml. */
	private Boolean issgml = false;

	/**
	 * Setsgml.
	 */
	public void setsgml() {
		this.issgml = true;
	}

	/**
	 * Unsetsgml.
	 */
	public void unsetsgml() {
		this.issgml = false;
	}

	// Print all tags of a word with a probability higher than p times the
	// largest probability
	/** The isthreshold. */
	private Boolean isthreshold = false;

	/** The threshold. */
	private float threshold;

	/**
	 * Sets the threshold.
	 *
	 * @param arg
	 *            the new threshold
	 */
	public void setthreshold(float arg) {
		this.isthreshold = true;
		this.threshold = arg;
	}

	/**
	 * Unsetthreshold.
	 */
	public void unsetthreshold() {
		this.isthreshold = false;
	}

	// Print tag probabilities
	/** The isprob. */
	private Boolean isprob = false;

	/**
	 * Setprob.
	 */
	public void setprob() {
		this.isprob = true;
	}

	/**
	 * Unsetprob.
	 */
	public void unsetprob() {
		this.isprob = false;
	}

	// Ignore prefix when guessing pos for unknown words
	/** The isignoreprefix. */
	private Boolean isignoreprefix = false;

	/**
	 * Setignoreprefix.
	 */
	public void setignoreprefix() {
		this.isignoreprefix = true;
	}

	/**
	 * Unsetignoreprefix.
	 */
	public void unsetignoreprefix() {
		this.isignoreprefix = false;
	}

	// Print the token rather than [unknown] for unknown lemma
	/** The isnounknown. */
	private Boolean isnounknown = false;

	/**
	 * Setnounknown.
	 */
	public void setnounknown() {
		this.isnounknown = true;
	}

	/**
	 * Unsetnounknown.
	 */
	public void unsetnounknown() {
		this.isnounknown = false;
	}

	// Turn on the heuristics fur guessing the parts of speech of unknown
	// hyphenated words
	/** The ishyphenheuristics. */
	private Boolean ishyphenheuristics = false;

	/**
	 * Sethyphenheuristics.
	 */
	public void sethyphenheuristics() {
		this.ishyphenheuristics = true;
	}

	/**
	 * Unsethyphenheuristics.
	 */
	public void unsethyphenheuristics() {
		this.ishyphenheuristics = false;
	}

	// quiet mode
	/** The isquiet. */
	private Boolean isquiet = false;

	/**
	 * Setquiet.
	 */
	public void setquiet() {
		this.isquiet = true;
	}

	/**
	 * Unsetquiet.
	 */
	public void unsetquiet() {
		this.isquiet = false;
	}

	// pretagging with probabilities
	/** The isptwithprob. */
	private Boolean isptwithprob = false;

	/**
	 * Setptwithprob.
	 */
	public void setptwithprob() {
		this.isptwithprob = true;
	}

	/**
	 * Unsetptwithprob.
	 */
	public void unsetptwithprob() {
		this.isptwithprob = false;
	}

	// pretagging with lemmata
	/** The isptwithlemma. */
	private Boolean isptwithlemma = false;

	/**
	 * Setptwithlemma.
	 */
	public void setptwithlemma() {
		this.isptwithlemma = true;
	}

	/**
	 * Unsetptwithlemma.
	 */
	public void unsetptwithlemma() {
		this.isptwithlemma = false;
	}

	// Print lexical information for each word
	/** The isproto. */
	private Boolean isproto = false;

	/**
	 * Setproto.
	 */
	public void setproto() {
		this.isproto = true;
	}

	/**
	 * Unsetproto.
	 */
	public void unsetproto() {
		this.isproto = false;
	}

	// Use only lexical probabilities for tagging
	/** The isbase. */
	private Boolean isbase = false;

	/**
	 * Setbase.
	 */
	public void setbase() {
		this.isbase = true;
	}

	/**
	 * Unsetbase.
	 */
	public void unsetbase() {
		this.isbase = false;
	}

	// Set minimal tag frequency to [epsilon]
	/** The iseps. */
	private Boolean iseps = false;

	/** The eps. */
	private float eps;

	/**
	 * Sets the eps.
	 *
	 * @param arg
	 *            the new eps
	 */
	public void seteps(float arg) {
		this.iseps = true;
		this.eps = arg;
	}

	/**
	 * Unseteps.
	 */
	public void unseteps() {
		this.iseps = false;
	}

	// Read auxiliary lexicon entries from file
	/** The islex. */
	private Boolean islex = false;

	/** The lex. */
	private String lex;

	/**
	 * Sets the lex.
	 *
	 * @param arg
	 *            the new lex
	 */
	public void setlex(String arg) {
		this.islex = true;
		this.lex = arg;
	}

	/**
	 * Unsetlex.
	 */
	public void unsetlex() {
		this.islex = false;
	}

	// Read a word-class automaton from file
	/** The iswc. */
	private Boolean iswc = false;

	/** The wc. */
	private String wc;

	/**
	 * Sets the wc.
	 *
	 * @param arg
	 *            the new wc
	 */
	public void setwc(String arg) {
		this.iswc = true;
		this.wc = arg;
	}

	/**
	 * Unsetwc.
	 */
	public void unsetwc() {
		this.iswc = false;
	}

	// The SGML tag [tag] signals the end of a sentence. this function implies
	// the -sgml option
	/** The iseostag. */
	private Boolean iseostag = false;

	/** The eostag. */
	private String eostag;

	/**
	 * Sets the eostag.
	 *
	 * @param arg
	 *            the new eostag
	 */
	public void seteostag(String arg) {
		this.iseostag = true;
		this.eostag = arg;
	}

	/**
	 * Unseteostag.
	 */
	public void unseteostag() {
		this.iseostag = false;
	}

	// Tag
	/**
	 * Treetagger.
	 *
	 * @param modelfile
	 *            the modelfile
	 * @param infile
	 *            the infile
	 * @param outfile
	 *            the outfile
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void treetagger(String modelfile, String infile, String outfile) throws IOException
	// arg : The model file
	// arg : The In file
	// arg : The Out file
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "tree-tagger"); //$NON-NLS-1$
		if (istoken)
			args.add("-token"); //$NON-NLS-1$
		if (islemma)
			args.add("-lemma"); //$NON-NLS-1$
		if (issgml)
			args.add("-sgml"); //$NON-NLS-1$
		if (isthreshold) {
			args.add("-threshold"); //$NON-NLS-1$
			args.add("" + threshold); //$NON-NLS-1$
		}
		if (isprob)
			args.add("-prob"); //$NON-NLS-1$
		if (isignoreprefix)
			args.add("-ignore-prefix"); //$NON-NLS-1$
		if (isnounknown)
			args.add("-no-unknown"); //$NON-NLS-1$
		if (ishyphenheuristics)
			args.add("-hyphen-heuristics"); //$NON-NLS-1$
		if (isquiet)
			args.add("-quiet"); //$NON-NLS-1$
		if (isptwithprob)
			args.add("-pt-with-prob"); //$NON-NLS-1$
		if (isptwithlemma)
			args.add("-pt-with-lemma"); //$NON-NLS-1$
		if (isproto)
			args.add("-proto"); //$NON-NLS-1$
		if (isbase)
			args.add("-base"); //$NON-NLS-1$
		if (iseps) {
			args.add("-eps"); //$NON-NLS-1$
			args.add("" + eps); //$NON-NLS-1$
		}
		if (islex) {
			args.add("-lex"); //$NON-NLS-1$
			args.add("" + lex); //$NON-NLS-1$
		}
		if (iswc) {
			args.add("-wc"); //$NON-NLS-1$
			args.add("" + wc); //$NON-NLS-1$
		}
		if (iseostag) {
			args.add("-eos-tag"); //$NON-NLS-1$
			args.add("" + eostag); //$NON-NLS-1$
		}

		for (String option : options) {
			// System.out.println("add manual option: "+option);
			if (option.trim().length() > 0)
				args.add(option);
		}

		args.add("" + modelfile); //$NON-NLS-1$
		args.add("" + infile); //$NON-NLS-1$
		args.add("" + outfile); //$NON-NLS-1$

		if (debug)
			System.out.println(StringUtils.join(args.toArray(), " ")); //$NON-NLS-1$

		Log.fine(StringUtils.join(args.toArray(), " ")); //$NON-NLS-1$

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code " + e + " at " + DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date())); //$NON-NLS-1$ //$NON-NLS-2$

			System.out.println("Args: "); //$NON-NLS-1$
			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " "); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println();
		}

		File rootFile = new File(infile).getParentFile().getParentFile();
		File report = new File(rootFile, "NLPToolsParameters.xml"); //$NON-NLS-1$
		// System.out.println("create txm report file
		// "+report.getAbsolutePath());
		Writer writer = new OutputStreamWriter(new FileOutputStream(report), "UTF-8"); //$NON-NLS-1$
		writer.write("<txm:commandLine>" + //$NON-NLS-1$
				"  <txm:os name=\"" //$NON-NLS-1$
				+ System.getProperty("os.name") + "\" arch=\"x86\"/>" + //$NON-NLS-1$ //$NON-NLS-2$
				"  <txm:osSeparator file=\"&#92;\" path=\"&#59;\" line=\"&#x0D;\"/>" //$NON-NLS-1$
				+ "  <txm:program name=\"tree-tagger\" path=\"~/TXM/treetagger/bin\" desc=\"TreeTagger is the name of the project TreeTagger\" version=\"3.2\"/>" //$NON-NLS-1$
				+ "  <txm:args>" //$NON-NLS-1$
				+ "    <txm:arg name=\"token\" type=\"none\"/>" //$NON-NLS-1$
				+ "    <txm:arg name=\"lemma\" type=\"none\"/>" //$NON-NLS-1$
				+ "    <txm:arg name=\"sgml\" type=\"none\"/>" //$NON-NLS-1$
				+ "    <txm:arg name=\"no-unknown\" type=\"none\"/>" //$NON-NLS-1$
				+ "    <txm:arg name=\"eos-tag\" type=\"String\">&lt;s></txm:arg>" //$NON-NLS-1$
				+ "    <txm:arg name=\"model\" type=\"String\">" //$NON-NLS-1$
				+ modelfile + "</txm:arg>" + //$NON-NLS-1$
				"    <txm:arg name=\"input\" type=\"String\">" + infile //$NON-NLS-1$
				+ "</txm:arg>" + //$NON-NLS-1$
				"    <txm:arg name=\"output\" type=\"String\">" //$NON-NLS-1$
				+ outfile + "</txm:arg>" + //$NON-NLS-1$
				"  </txm:args>" + //$NON-NLS-1$
				"</txm:commandLine>"); //$NON-NLS-1$
		writer.flush();
		writer.close();
	}

	// number of preceding words
	/** The iscl. */
	private Boolean iscl = false;

	/** The cl. */
	private int cl;

	/**
	 * Sets the cl.
	 *
	 * @param arg
	 *            the new cl
	 */
	public void setcl(int arg) {
		this.iscl = true;
		this.cl = arg;
	}

	/**
	 * Unsetcl.
	 */
	public void unsetcl() {
		this.iscl = false;
	}

	// min. decision tree gain
	/** The isdtg. */
	private Boolean isdtg = false;

	/** The dtg. */
	private float dtg;

	/**
	 * Sets the dtg.
	 *
	 * @param arg
	 *            the new dtg
	 */
	public void setdtg(float arg) {
		this.isdtg = true;
		this.dtg = arg;
	}

	/**
	 * Unsetdtg.
	 */
	public void unsetdtg() {
		this.isdtg = false;
	}

	// A smoothing weight parameter
	/** The issw. */
	private Boolean issw = false;

	/** The sw. */
	private float sw;

	/**
	 * Sets the sw.
	 *
	 * @param arg
	 *            the new sw
	 */
	public void setsw(float arg) {
		this.issw = true;
		this.sw = arg;
	}

	/**
	 * Unsetsw.
	 */
	public void unsetsw() {
		this.issw = false;
	}

	// affix tree gain
	/** The isatg. */
	private Boolean isatg = false;

	/** The atg. */
	private float atg;

	/**
	 * Sets the atg.
	 *
	 * @param arg
	 *            the new atg
	 */
	public void setatg(float arg) {
		this.isatg = true;
		this.atg = arg;
	}

	/**
	 * Unsetatg.
	 */
	public void unsetatg() {
		this.isatg = false;
	}

	// the end-of-sentence part-of-speech tag
	/** The isst. */
	private Boolean isst = false;

	/** The st. */
	private String st;

	/**
	 * Sets the st.
	 *
	 * @param arg
	 *            the new st
	 */
	public void setst(String arg) {
		this.isst = true;
		this.st = arg;
	}

	/**
	 * Unsetst.
	 */
	public void unsetst() {
		this.isst = false;
	}

	// weight of the equivalence class based probability estimates
	/** The isecw. */
	private Boolean isecw = false;

	/** The ecw. */
	private float ecw;

	/**
	 * Sets the ecw.
	 *
	 * @param arg
	 *            the new ecw
	 */
	public void setecw(float arg) {
		this.isecw = true;
		this.ecw = arg;
	}

	/**
	 * Unsetecw.
	 */
	public void unsetecw() {
		this.isecw = false;
	}

	// probability for lexical entries. default 0.001
	/** The islt. */
	private Boolean islt = false;

	/** The lt. */
	private float lt;

	/**
	 * Sets the lt.
	 *
	 * @param arg
	 *            the new lt
	 */
	public void setlt(float arg) {
		this.islt = true;
		this.lt = arg;
	}

	/**
	 * Unsetlt.
	 */
	public void unsetlt() {
		this.islt = false;
	}

	// assume that the data is encoded with UTF8
	/** The isutf8. */
	private Boolean isutf8 = false;

	/**
	 * Setutf8.
	 */
	public void setutf8() {
		this.isutf8 = true;
	}

	/**
	 * Unsetutf8.
	 */
	public void unsetutf8() {
		this.isutf8 = false;
	}

	// Train
	/**
	 * Traintreetagger.
	 *
	 * @param lexiconfile
	 *            the lexiconfile
	 * @param openclassfile
	 *            the openclassfile
	 * @param infile
	 *            the infile
	 * @param outfile
	 *            the outfile
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void traintreetagger(String lexiconfile, String openclassfile, String infile, String outfile) throws IOException
	// arg : The lexicon file
	// arg : The open class file
	// arg : The In file
	// arg : The Out file
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "train-tree-tagger"); //$NON-NLS-1$
		if (isquiet)
			args.add("-quiet"); //$NON-NLS-1$

		if (iscl) {
			args.add("-cl"); //$NON-NLS-1$
			args.add("" + cl); //$NON-NLS-1$
		}
		if (isdtg) {
			args.add("-dtg"); //$NON-NLS-1$
			args.add("" + dtg); //$NON-NLS-1$
		}
		if (issw) {
			args.add("-sw"); //$NON-NLS-1$
			args.add("" + sw); //$NON-NLS-1$
		}
		if (isatg) {
			args.add("-atg"); //$NON-NLS-1$
			args.add("" + atg); //$NON-NLS-1$
		}
		if (isst) {
			args.add("-st"); //$NON-NLS-1$
			args.add("" + st); //$NON-NLS-1$
		}
		if (isecw) {
			args.add("-ecw"); //$NON-NLS-1$
			args.add("" + ecw); //$NON-NLS-1$
		}
		if (islt) {
			args.add("-lt"); //$NON-NLS-1$
			args.add("" + lt); //$NON-NLS-1$
		}
		if (isutf8)
			args.add("-utf8"); //$NON-NLS-1$
		args.add("" + lexiconfile); //$NON-NLS-1$
		args.add("" + openclassfile); //$NON-NLS-1$
		args.add("" + infile); //$NON-NLS-1$
		args.add("" + outfile); //$NON-NLS-1$

		ProcessBuilder pb = new ProcessBuilder(args);
		if (debug)
			System.out.println(StringUtils.join(args.toArray(), " ")); //$NON-NLS-1$
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code " + e + " at " + DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date())); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println("Args: "); //$NON-NLS-1$
			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " "); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

	}

	public void kill() {
		// TODO uses TreeTagger4j and manage the process
	}
}