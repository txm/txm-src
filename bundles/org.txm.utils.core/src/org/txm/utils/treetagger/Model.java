// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.treetagger;

import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

// TODO: Auto-generated Javadoc
/**
 * A TreeTagger model varies on language and encoding @ author mdecorde.
 */
public class Model extends HashMap<String, String> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4490061845502678357L;

	/** The lang. */
	String lang;

	/** The country. */
	String country;

	/** The byversion. */
	HashMap<String, String> byversion = new HashMap<String, String>();

	/** The byencoding. */
	HashMap<String, String> byencoding = new HashMap<String, String>();

	/**
	 * Instantiates a new model.
	 *
	 * @param model
	 *            the model
	 */
	public Model(Element model) {
		lang = model.getAttribute("lang"); //$NON-NLS-1$
		country = model.getAttribute("country"); //$NON-NLS-1$

		NodeList files = model.getElementsByTagName("file"); //$NON-NLS-1$
		for (int i = 0; i < files.getLength(); i++) {
			Element file = (Element) files.item(i);
			this.put(file.getAttribute("encoding") //$NON-NLS-1$
					+ file.getAttribute("version"), file.getAttribute("name")); //$NON-NLS-1$ //$NON-NLS-2$
			this.byversion.put(file.getAttribute("version"), file //$NON-NLS-1$
					.getAttribute("name")); //$NON-NLS-1$
			this.byencoding.put(file.getAttribute("encoding"), file //$NON-NLS-1$
					.getAttribute("name")); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the lang.
	 *
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Describe.
	 */
	public void describe() {
		System.out.println(" Model: lang=" + lang + ", country=" + country); //$NON-NLS-1$ //$NON-NLS-2$
		for (String encoding : this.byencoding.keySet())
			System.out.println("  File: encoding=" + encoding + ", name=" //$NON-NLS-1$ //$NON-NLS-2$
					+ this.byencoding.get(encoding));
	}
}
