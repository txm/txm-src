// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu., 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.utils.treetagger;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

// TODO: Auto-generated Javadoc
/**
 * build links between a lang and encoding and model file @ author mdecorde.
 */
public class ModelCatalog extends HashMap<String, Model> {

	/** The default lang. */
	String defaultLang = "fr"; //$NON-NLS-1$

	/** The default encoding. */
	String defaultEncoding = "UTF-8"; //$NON-NLS-1$

	/** The default version. */
	String defaultVersion = "3.2"; //$NON-NLS-1$

	/** The modelsdescriptor. */
	File modelsdescriptor;

	/**
	 * Instantiates a new model catalog.
	 *
	 * @param modelsdescriptor
	 *            the modelsdescriptor
	 */
	public ModelCatalog(File modelsdescriptor) {
		this.modelsdescriptor = modelsdescriptor;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(modelsdescriptor);
			Element root = doc.getDocumentElement();

			NodeList models = root.getElementsByTagName("model"); //$NON-NLS-1$
			for (int i = 0; i < models.getLength(); i++) {
				Element model = (Element) models.item(i);
				this.put(model.getAttribute("lang"), new Model(model)); //$NON-NLS-1$
			}
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Describe.
	 */
	public void describe() {
		System.out.println("Model Catalog : " + modelsdescriptor); //$NON-NLS-1$
		for (Model m : this.values()) {
			m.describe();
		}
	}

	/**
	 * Gets the model.
	 *
	 * @param lang
	 *            the lang
	 * @param encoding
	 *            the encoding
	 * @param version
	 *            the version
	 * @return the model
	 */
	public String getModel(String lang, String encoding, String version) {
		Model m = this.get(lang);
		return m.get(encoding + version);
	}

	/**
	 * Gets the model by encoding.
	 *
	 * @param lang
	 *            the lang
	 * @param encoding
	 *            the encoding
	 * @return the model by encoding
	 */
	public String getModelByEncoding(String lang, String encoding) {
		Model m = this.get(lang);
		return m.byencoding.get(encoding);
	}

	/**
	 * Gets the model by version.
	 *
	 * @param lang
	 *            the lang
	 * @param version
	 *            the version
	 * @return the model by version
	 */
	public String getModelByVersion(String lang, String version) {
		Model m = this.get(lang);
		return m.byversion.get(version);
	}

	/**
	 * Gets the model.
	 *
	 * @param lang
	 *            the lang
	 * @return the model
	 */
	public String getModel(String lang) {
		return getModel(lang, defaultEncoding, defaultVersion);
	}

	/**
	 * Gets the default lang.
	 *
	 * @return the default lang
	 */
	public String getDefaultLang() {
		return defaultLang;
	}

	/**
	 * Gets the default encoding.
	 *
	 * @return the default encoding
	 */
	public String getDefaultEncoding() {
		return defaultEncoding;
	}

	/**
	 * Gets the default version.
	 *
	 * @return the default version
	 */
	public String getDefaultVersion() {
		return defaultVersion;
	}

	/**
	 * Sets the default lang.
	 *
	 * @param defaultLang
	 *            the new default lang
	 */
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}

	/**
	 * Sets the default encoding.
	 *
	 * @param defaultEncoding
	 *            the new default encoding
	 */
	public void setDefaultEncoding(String defaultEncoding) {
		this.defaultEncoding = defaultEncoding;
	}

	/**
	 * Sets the default version.
	 *
	 * @param defaultVersion
	 *            the new default version
	 */
	public void setDefaultVersion(String defaultVersion) {
		this.defaultVersion = defaultVersion;
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String args[]) {
		File modelsdescriptor = new File("C:\\Documents and Settings\\H\\Bureau\\eclipseRCP\\eclipse\\models.xml"); //$NON-NLS-1$
		ModelCatalog catalog = new ModelCatalog(modelsdescriptor);
		catalog.describe();
	}
}
