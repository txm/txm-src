package org.txm.utils;

import java.io.IOException;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class LocalXhtmlDtdEntityResolver implements EntityResolver {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
		String fileName = systemId.substring(systemId.lastIndexOf("/") + 1); //$NON-NLS-1$
		System.out.println("FILENAME: " + fileName); //$NON-NLS-1$

		return new InputSource(getClass().getClassLoader().getResourceAsStream(fileName));
	}

}