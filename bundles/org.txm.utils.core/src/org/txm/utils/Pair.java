// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils;

import java.util.HashMap;

import org.txm.utils.messages.UtilsCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * similar to a C++ pair @ author mdecorde.
 *
 * @param <T>
 *            the generic type
 * @param <V>
 *            the value type
 */
public class Pair<T, V> {

	/** The o1. */
	public T o1;

	/** The o2. */
	public V o2;

	/**
	 * Instantiates a new pair.
	 *
	 * @param o1
	 *            the o1
	 * @param o2
	 *            the o2
	 */
	public Pair(T o1, V o2) {
		this.o1 = o1;
		this.o2 = o2;
	}

	/**
	 * Same.
	 *
	 * @param o1
	 *            the o1
	 * @param o2
	 *            the o2
	 * @return true, if successful
	 */
	public static boolean same(Object o1, Object o2) {
		return o1 == null ? o2 == null : o1.equals(o2);
	}

	/**
	 * Gets the first.
	 *
	 * @return the first
	 */
	public T getFirst() {
		return o1;
	}

	/**
	 * Gets the second.
	 *
	 * @return the second
	 */
	public V getSecond() {
		return o2;
	}

	/**
	 * Sets the first.
	 *
	 * @param o
	 *            the new first
	 */
	public void setFirst(T o) {
		o1 = o;
	}

	/**
	 * Sets the second.
	 *
	 * @param o
	 *            the new second
	 */
	public void setSecond(V o) {
		o2 = o;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pair))
			return false;
		Pair<?, ?> p = (Pair<?, ?>) obj;
		return same(p.o1, this.o1) && same(p.o2, this.o2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Pair{" + o1 + ", " + o2 + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if (o1 == null && o2 == null)
			return 0;
		if (o1 != null && o2 != null)
			return this.o1.hashCode() * this.o2.hashCode();
		if (o1 != null)
			return this.o1.hashCode();
		else
			return this.o2.hashCode();
	}

	/**
	 * Simple example test program.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		HashMap<Pair, String> hashpair = new HashMap<Pair, String>();

		Pair<String, String> p1 = new Pair<String, String>("a", "b"); //$NON-NLS-1$ //$NON-NLS-2$
		Pair<String, String> p2 = new Pair<String, String>("a", null); //$NON-NLS-1$
		Pair<String, String> p3 = new Pair<String, String>("a", "b"); //$NON-NLS-1$ //$NON-NLS-2$
		Pair<String, String> p4 = new Pair<String, String>(null, null);
		System.out.println(p1.equals(new Pair<Integer, Integer>(new Integer(1), new Integer(2))) + UtilsCoreMessages.Pair_0);
		System.out.println(p4.equals(p2) + UtilsCoreMessages.Pair_0);
		System.out.println(p2.equals(p4) + UtilsCoreMessages.Pair_0);
		System.out.println(p1.equals(p3) + UtilsCoreMessages.Pair_1);
		System.out.println(p4.equals(p4) + UtilsCoreMessages.Pair_1);
		hashpair.containsKey(p1);
		hashpair.put(p1, "p1"); //$NON-NLS-1$
		hashpair.put(p2, "p2"); //$NON-NLS-1$
		hashpair.put(p3, "p3"); //$NON-NLS-1$
		hashpair.put(p4, "p4"); //$NON-NLS-1$

		System.out.println(hashpair);
		System.out.println((p1 == null ? p3 == null : p1.equals(p3)));
	}

}
