// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.utils;

import java.io.IOException;
import java.util.LinkedHashMap;

/**
 * abstract table reader class with simple methods to read a table like a Stream
 */
public abstract class AbstractTableReader {

	public abstract boolean readHeaders() throws IOException;

	public abstract boolean readRecord() throws IOException;
	
	public abstract LinkedHashMap<String, String> getRecord();
	
	public abstract String[] getHeaders();
	
	String EMPTY = ""; //$NON-NLS-1$
	public abstract String get(String h) throws IOException;
	
	public abstract void close() throws IOException;
}