package org.txm.utils.io;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.FileUtils;
import org.txm.utils.i18n.DetectBOM;
import org.txm.utils.logger.Log;
import org.txm.utils.messages.UtilsCoreMessages;

public class IOUtils {

	public static final String UTF8 = "UTF-8"; //$NON-NLS-1$

	public static ArrayList<String> findWithGroup(File file, String pattern) throws IOException {

		return findWithGroup(file, UTF8, pattern, false);
	}

	public static ArrayList<String> findWithGroup(File file, String pattern, boolean normalizeSeparators) throws IOException {

		return findWithGroup(file, UTF8, pattern, normalizeSeparators);
	}

	public static ArrayList<String> findWithGroup(File file, String encoding, String pattern) throws IOException {

		return findWithGroup(file, encoding, pattern, false);
	}

	/**
	 * select only files not hidden @deprecated
	 */
	public static final FileFilter HIDDENFILE_FILTER = FileUtils.REMOVE_HIDDEN_FILES_FILTER;

	public static ArrayList<String> findWithGroup(File file, String encoding, String pattern, boolean normalizeSeparators) throws IOException {

		ArrayList<String> matches = new ArrayList<String>();
		String text = IOUtils.getText(file, encoding);
		if (normalizeSeparators) {
			text = text.replaceAll("\\s", " "); //$NON-NLS-1$ //$NON-NLS-2$
			text = text.replaceAll("\\t", " "); //$NON-NLS-1$ //$NON-NLS-2$
			text = text.replaceAll("\\n", " "); //$NON-NLS-1$ //$NON-NLS-2$
			text = text.replaceAll("[ ]+", " "); //$NON-NLS-1$ //$NON-NLS-2$
		}

		Pattern test = Pattern.compile(pattern);
		Matcher m = test.matcher(text);
		while (m.find()) {
			matches.add(m.group(1));
		}
		return matches;
	}

	public static ArrayList<String> find(File file, String pattern) throws IOException {

		return find(file, UTF8, pattern, false);
	}

	public static ArrayList<String> find(File file, String pattern, boolean normalizeSeparators) throws IOException {

		return findWithGroup(file, UTF8, pattern, normalizeSeparators);
	}

	public static ArrayList<String> find(File file, String encoding, String pattern) throws IOException {

		return find(file, encoding, pattern, false);
	}

	public static ArrayList<String> find(File file, String encoding, String pattern, boolean normalizeSeparators) throws IOException {

		ArrayList<String> matches = new ArrayList<String>();
		String text = IOUtils.getText(file, encoding);
		// System.out.println(text);
		if (normalizeSeparators) {
			text = text.replaceAll("\\s", " "); //$NON-NLS-1$ //$NON-NLS-2$
			text = text.replaceAll("\\t", " "); //$NON-NLS-1$ //$NON-NLS-2$
			text = text.replaceAll("\\n", " "); //$NON-NLS-1$ //$NON-NLS-2$
			text = text.replaceAll("[ ]+", " "); //$NON-NLS-1$ //$NON-NLS-2$
		}

		Pattern test = Pattern.compile(pattern);
		Matcher m = test.matcher(text);
		while (m.find()) {
			matches.add(m.group());
		}
		return matches;
	}

	public static BufferedReader getReader(File file) throws UnsupportedEncodingException, FileNotFoundException {

		return getReader(file, UTF8);
	}

	/**
	 * 
	 * @param file
	 * @param encoding
	 *            if null the "UTF-8" encoding is used
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static BufferedReader getReader(File file, String encoding) throws UnsupportedEncodingException, FileNotFoundException {

		if (encoding == null) {
			encoding = UTF8; // prevent null exception
		}

		FileInputStream input = new FileInputStream(file);
		DetectBOM db = new DetectBOM(file);
		for (int ibom = 0; ibom < db.getBOMSize(); ibom++) {
			try {
				input.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new BufferedReader(new InputStreamReader(input, encoding));
	}

	public static BufferedReader getReader(String file) throws UnsupportedEncodingException, FileNotFoundException {

		return getReader(new File(file), UTF8);
	}

	public static BufferedReader getReader(URL url) throws IOException {

		return getReader(url, UTF8);
	}

	public static BufferedReader getReader(URL url, String encoding) throws IOException {

		if (encoding == null) {
			encoding = UTF8; // prevent null exception
		}

		URLConnection conn = url.openConnection();
		InputStream inputStream = conn.getInputStream();
		return new BufferedReader(new InputStreamReader(inputStream, encoding));
	}

	public static boolean download(URL url, File outfile) throws Exception {

		URLConnection conn = null;
		InputStream inputStream = null;
		OutputStream writer = null;
		try {
			conn = url.openConnection();
			inputStream = conn.getInputStream();
			writer = new FileOutputStream(outfile);
			writer.write(inputStream.readAllBytes());
			writer.close();
			inputStream.close();
		} catch (Exception e) {
			if (inputStream != null)
				inputStream.close();
			if (writer != null)
				writer.close();
			throw e;
		}

		return outfile.exists() && outfile.length() > 0;
	}

	public static void setText(File file, String text, String encoding) throws IOException {

		PrintWriter writer = getWriter(file, encoding);
		writer.print(text);
		writer.close();
	}

	public static String getText(URL url, String encoding) throws IOException {

		BufferedReader reader = getReader(url, encoding);
		StringBuilder builder = new StringBuilder();
		try {
			String line = reader.readLine();
			while (line != null) {
				builder.append(line);
				line = reader.readLine();
				if (line != null)
					builder.append("\n"); //$NON-NLS-1$
			}
		} finally {
			reader.close();
		}
		return builder.toString();
	}

	public static String getText(File file) throws IOException {

		return getText(file, UTF8);
	}

	public static String getText(File file, String encoding) throws IOException {

		BufferedReader reader = getReader(file, encoding);
		StringBuilder builder = new StringBuilder();
		try {
			String line = reader.readLine();
			while (line != null) {
				builder.append(line);
				line = reader.readLine();
				if (line != null) {
					builder.append("\n"); //$NON-NLS-1$
				}
			}
		} finally {
			reader.close();
		}
		return builder.toString();
	}

	public static PrintWriter getWriter(File file) throws UnsupportedEncodingException, FileNotFoundException {

		return getWriter(file, UTF8);
	}

	public static PrintWriter getWriter(File file, String encoding) throws UnsupportedEncodingException, FileNotFoundException {

		return getWriter(file, encoding, false);
	}

	public static PrintWriter getWriter(File file, boolean append) throws UnsupportedEncodingException, FileNotFoundException {

		return getWriter(file, UTF8, append);
	}

	public static PrintWriter getWriter(File file, String encoding, boolean append) throws UnsupportedEncodingException, FileNotFoundException {

		if (encoding == null) {
			encoding = UTF8; // prevent null exception
		}

		return new PrintWriter(new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(file, append)), encoding));
	}

	public static PrintWriter getWriter(String file) throws UnsupportedEncodingException, FileNotFoundException {

		return getWriter(new File(file), UTF8);
	}

	public static boolean write(File file, String str) {

		PrintWriter writer = null;
		try {
			writer = getWriter(file);
			writer.write(str);
		} catch (Exception e) {
			Log.warning(NLS.bind(UtilsCoreMessages.errorWritingInP0P1 , file, e));
			return false;
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
		return true;
	}

	public static ArrayList<String> getLines(File file, String encoding) {

		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = IOUtils.getReader(file, encoding);
			String line = reader.readLine();
			while (line != null) {
				lines.add(line);
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			Log.warning(NLS.bind(UtilsCoreMessages.errorReadingInP0P1 , file, e));
		}
		return lines;
	}

	/**
	 * 
	 * @return an InputStream, dont forget to close it
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static InputStream getInput(File file) throws MalformedURLException, IOException {

		return file.toURI().toURL().openStream();
	}

	public static OutputStream getOutputStream(File file) throws FileNotFoundException {

		return new BufferedOutputStream(new FileOutputStream(file, false));
	}

	public static OutputStream getOutputStream(File file, boolean append) throws FileNotFoundException {

		return new BufferedOutputStream(new FileOutputStream(file, append));
	}

	public static void replace(File file, String oldString, String newString) throws IOException {

		String text = getText(file, UTF8);
		text = text.replace(oldString, newString);
		IOUtils.write(file, text);
	}

	public static void replaceAll(File file, Pattern regex, String newString) throws IOException {

		String text = getText(file, UTF8);
		text = regex.matcher(text).replaceAll(newString);
		IOUtils.write(file, text);
	}

	public static String firstLine(File file) {
		try {
			BufferedReader reader = getReader(file);
			String line = reader.readLine();
			reader.close();
			return line;
		} catch (Exception e) {
			return null;
		}
	}
}
