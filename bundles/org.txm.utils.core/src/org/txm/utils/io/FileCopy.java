// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-11-17 17:01:31 +0100 (Thu, 17 Nov 2016) $
// $LastChangedRevision: 3341 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import org.txm.utils.messages.UtilsCoreMessages;

/**
 * File copy methods
 */
public class FileCopy {

	private static final boolean isWindows = System.getProperty("os.name").contains("Windows"); //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * replace the file if it exists
	 * 
	 * @param source
	 * @param dest
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(File source, File dest) throws IOException {

		return copy(source, dest, true);
	}

	/**
	 * copy one file with nio package to accelerate the copy
	 * 
	 * !!! on windows : max size = 64mo !!!.
	 * 
	 * resolve symbolic links
	 *
	 * @param source
	 *            the source
	 * @param dest
	 *            the dest
	 * @return true, if successful
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean copy(File source, File dest, boolean replace) throws IOException {

		if (dest.exists() && !replace)
			return true; // don't replace file

		if (isWindows || source.length() >= Integer.MAX_VALUE) {
			// System.out.println("COPY BIG FILE");
			FileInputStream instream = null;
			BufferedOutputStream outstream = null;
			try {
				// Open a stream of bytes to read the file bytes into the
				// program
				instream = new FileInputStream(source);
				// A stream of bytes from the program to the destination
				outstream = new BufferedOutputStream(new FileOutputStream(dest));
				byte[] buffer = new byte[4096];
				int bytesRead;
				// Write the bytes from the inputstream to the outputstream
				while ((bytesRead = instream.read(buffer)) != -1) {
					outstream.write(buffer, 0, bytesRead);
				}
			} catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				return false;
			} finally {
				if (instream != null)
					instream.close();
				if (outstream != null)
					outstream.close();
				instream = null;
				outstream = null;
			}
		} else {
			// System.out.println("COPY NORMAL FILE");
			FileChannel in = null, out = null;
			FileInputStream fis = null;
			FileOutputStream fos = null;
			try {
				fis = new FileInputStream(source);
				fos = new FileOutputStream(dest);
				in = fis.getChannel();
				out = fos.getChannel();

				long size = in.size();
				MappedByteBuffer buf = in.map(FileChannel.MapMode.READ_ONLY, 0, size);

				out.write(buf);
			} catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);

				if (fis != null)
					fis.close();
				if (fos != null)
					fos.close();
				if (in != null)
					in.close();
				if (out != null)
					out.close();

				return false;
			} finally {
				if (fis != null)
					fis.close();
				if (fos != null)
					fos.close();
				if (in != null)
					in.close();
				if (out != null)
					out.close();
			}
		}
		return true;
	}

	/**
	 * Copy files. Don't erase the destination directory and replace files if
	 * they already exists
	 * 
	 * warning: Symbolic links are not resolved and the link file is recreated
	 *
	 * @param src
	 *            the src directory
	 * @param dest
	 *            the dest directory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void copyFiles(File src, File dest) throws IOException {

		copyFiles(src, dest, true);
	}

	/**
	 * Copy files. Don't erase the destination directory and replace files if
	 * they already exists
	 *
	 * warning: Symbolic links are not resolved and the link file is recreated
	 *
	 * @param src
	 *            the src directory
	 * @param dest
	 *            the dest directory
	 * @param replace
	 *            replace files if true
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void copyFiles(File src, File dest, boolean replace) throws IOException {

		// Check to ensure that the source is valid...
		if (!src.exists()) {
			throw new IOException(UtilsCoreMessages.bind(UtilsCoreMessages.FileCopy_0P0, src.getAbsolutePath())); //$NON-NLS-1$
		} else if (!src.canRead()) { // check to ensure we have rights to the
			// source...
			throw new IOException(UtilsCoreMessages.bind(UtilsCoreMessages.FileCopy_2P0, src.getAbsolutePath())); //$NON-NLS-1$
		}

		if (Files.isSymbolicLink(src.toPath())) { // copy symbolic links
			Path target = Files.readSymbolicLink(src.toPath());
			Files.createSymbolicLink(dest.toPath(), target);
		} else if (src.isDirectory()) { // is it a directory ?

			if (!dest.exists()) { // does the destination already exist?
				// if not create the file hierarchy
				dest.mkdirs();
				if (!dest.exists()) {
					throw new IOException(UtilsCoreMessages.bind(UtilsCoreMessages.FileCopy_4P0, dest.getAbsolutePath())); //$NON-NLS-1$
				}
			}

			// get a listing of files...
			String list[] = src.list();
			// copy all the files in the list.
			for (int i = 0; i < list.length; i++) {
				File dest1 = new File(dest, list[i]);
				File src1 = new File(src, list[i]);
				copyFiles(src1, dest1);

			}
		} else {
			// This was not a directory, so lets just copy the file
			copy(src, dest, replace);
		}
	}

	public static void main(String[] args) throws IOException {
		// File test1 = File.createTempFile("source", ".large"); //$NON-NLS-1$
		// //$NON-NLS-2$
		// File test2 = File.createTempFile("source", ".verylarge");
		// //$NON-NLS-1$
		// //$NON-NLS-2$
		//
		// System.out.println("write "+test1); //$NON-NLS-1$
		// writeBigFile(test1, Integer.MAX_VALUE / 2, 1);
		// System.out.println("write "+test2); //$NON-NLS-1$
		// writeBigFile(test2, Integer.MAX_VALUE / 2, 3);
		//
		// System.out.println("test1: "+test1+" : "+test1.length());
		// //$NON-NLS-1$
		// //$NON-NLS-2$
		// System.out.println("test2: "+test2+" : "+test2.length());
		// //$NON-NLS-1$
		// //$NON-NLS-2$
		//
		// File copy1 = File.createTempFile("copy", ".large"); //$NON-NLS-1$
		// //$NON-NLS-2$
		// File copy2 = File.createTempFile("copy", ".verylarge"); //$NON-NLS-1$
		// //$NON-NLS-2$
		//
		// System.out.println("Copy1 "+copy1); //$NON-NLS-1$
		// FileCopy.copy(test1, copy1);
		// System.out.println("Copy2 "+copy2); //$NON-NLS-1$
		// FileCopy.copy(test2, copy2);
		//
		// System.out.println("copy1: "+copy1+" : "+copy1.length());
		// //$NON-NLS-1$
		// //$NON-NLS-2$
		// System.out.println("copy2: "+copy2+" : "+copy2.length());
		// //$NON-NLS-1$
		// //$NON-NLS-2$
		//
		// System.out.println("delete 1 "+test1.delete()); //$NON-NLS-1$
		// System.out.println("delete 2 "+test2.delete()); //$NON-NLS-1$
		// System.out.println("delete c1 "+copy1.delete()); //$NON-NLS-1$
		// System.out.println("delete c2 "+copy2.delete()); //$NON-NLS-1$

		File dir1 = new File("/home/mdecorde/TEMP/copydir"); //$NON-NLS-1$
		File dir2 = new File("/home/mdecorde/TEMP/copydir3"); //$NON-NLS-1$
		FileCopy.copyFiles(dir1, dir2);
	}
}
