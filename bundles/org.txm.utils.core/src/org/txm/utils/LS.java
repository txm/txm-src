package org.txm.utils;

import java.io.File;
import java.util.ArrayList;

import org.txm.utils.messages.UtilsCoreMessages;

class LS {

	/**
	 * Prints the Content of the Directory
	 *
	 * @param dir
	 *            Directory
	 * @param recur
	 *            Recursion
	 * @param hidden
	 *            Show hidden Files
	 */
	public static ArrayList<File> list(File dir, boolean recur, boolean hidden) {
		ArrayList<File> result = new ArrayList<File>();
		if ((dir != null) && dir.exists() && dir.isDirectory()) {
			File[] filelist = dir.listFiles();

			for (File file : filelist) {
				if (hidden || !file.isHidden()) {
					result.add(file);
					if (file.isDirectory() && recur) {
						result.addAll(list(file, recur, hidden));
					}
				}
			}
		} else {
			System.err.println(UtilsCoreMessages.bind(UtilsCoreMessages.LS_0P0, dir));
		}
		return result;
	}

	public static void main(String args[]) {
		ArrayList<File> files1 = LS.list(new File("~"), false, false); //$NON-NLS-1$
		System.out.println(files1.size());

		ArrayList<File> files2 = LS.list(new File("~"), true, false); //$NON-NLS-1$
		System.out.println(files2.size());

		ArrayList<File> files3 = LS.list(new File("~"), false, true); //$NON-NLS-1$
		System.out.println(files3.size());

		ArrayList<File> files4 = LS.list(new File("~"), true, true); //$NON-NLS-1$
		System.out.println(files4.size());
	}
}
