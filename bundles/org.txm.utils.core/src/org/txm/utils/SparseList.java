// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

// TODO: Auto-generated Javadoc
/**
 * The Class SparseList.
 *
 * @param <E>
 *            the element type
 */
public class SparseList<E> extends AbstractList<E> {

	/** The map. */
	private SortedMap<Integer, E> map;

	/**
	 * Instantiates a new sparse list.
	 */
	public SparseList() {
		map = new TreeMap<Integer, E>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractList#get(int)
	 */
	@Override
	public E get(int n) {
		return map.get(n);
	}

	/**
	 * Returns all the elements between from (included) and to(excluded). Values
	 * not in the list are returned as null
	 *
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 * @return A list whose size is to-form
	 */
	public List<E> getRange(int from, int to) {
		List<E> res = new ArrayList<E>();
		// if (from > to) System.out.println("ERROR: "+from+" > "+to);
		SortedMap<Integer, E> subMap = map.subMap(from, to);
		if (subMap.size() == (to - from))
			res.addAll(subMap.values());
		else
			for (int i = from; i < to; i++)
				res.add(map.get(i));
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractCollection#size()
	 */
	@Override
	public int size() {
		return map.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.AbstractList#add(int, java.lang.Object)
	 */
	@Override
	public void add(int n, E obj) {
		map.put(n, obj);
	}

	/**
	 * Gets the not null.
	 *
	 * @return the not null
	 */
	public List<Integer> getNotNull() {
		return new ArrayList<Integer>(map.keySet());
	}
}
