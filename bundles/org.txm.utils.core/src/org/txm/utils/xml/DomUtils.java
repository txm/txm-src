// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.utils.xml;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

// TODO: Auto-generated Javadoc
/**
 * The Class DomUtils.
 */
public class DomUtils {

	/**
	 * Empty document.
	 *
	 * @return the document
	 * @throws ParserConfigurationException
	 */
	public static Document emptyDocument() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setXIncludeAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.newDocument();
	}

	/**
	 * Load.
	 *
	 * @param xmlfile
	 *            the xmlfile
	 * @return the document
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 */
	public static Document load(File xmlfile) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setXIncludeAware(true);
		factory.setNamespaceAware(true); // never forget this!
		DocumentBuilder builder = factory.newDocumentBuilder();
		// builder.parse(xmlfile).getDocumentElement().getElementsByTagName(name)
		return builder.parse(xmlfile);
	}

	/**
	 * Save the doc Element to file
	 *
	 * @param writer
	 *            the writer
	 * @return true if successful
	 */
	public static boolean save(Document doc, Writer writer) {
		return save(doc.getDocumentElement(), writer);
	}

	public static boolean save(Element element, File outfile) throws UnsupportedEncodingException, FileNotFoundException {
		Writer writer = new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(outfile)), "UTF-8")); //$NON-NLS-1$
		return save(element, writer);
	}

	/**
	 * Save.
	 *
	 * @param element
	 *            the doc
	 * @param writer
	 *            the writer
	 * @return true, if successful
	 */
	public static boolean save(Element element, Writer writer) {
		try {
			// Création de la source DOM
			Source source = new DOMSource(element);

			// Création du fichier de sortie
			Result resultat = new StreamResult(writer);

			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
			transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8"); //$NON-NLS-1$

			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			return true;
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}

	/**
	 * Save.
	 *
	 * @param doc
	 *            the doc
	 * @param outfile
	 *            the output file
	 * @return true, if successful
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static boolean save(Document doc, File outfile) throws UnsupportedEncodingException, FileNotFoundException {
		Writer writer = new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(outfile)), "UTF-8")); //$NON-NLS-1$
		return save(doc, writer);
	}

	/**
	 * Save.
	 *
	 * @param doc
	 *            the doc
	 * @return true, if successful
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static boolean print(Document doc) throws UnsupportedEncodingException, FileNotFoundException {
		Writer writer = new PrintWriter(System.out);
		return save(doc, writer);
	}

}
