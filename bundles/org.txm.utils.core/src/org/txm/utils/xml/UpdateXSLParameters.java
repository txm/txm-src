package org.txm.utils.xml;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * change parameters value of an XSL file
 * 
 * @author mdecorde
 *
 */
public class UpdateXSLParameters {

	File xslFile;

	public UpdateXSLParameters(File xslFile) {
		this.xslFile = xslFile;
	}

	/**
	 * update the content of "param" elements which name attribute exists in the
	 * parameters map with the parameter map value.
	 * 
	 * @param parameters
	 * @return true if document if successfully saved
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * 
	 */
	public boolean process(HashMap<String, String> parameters) throws ParserConfigurationException, SAXException, IOException {
		if (!xslFile.exists()) {
			System.out.println("Error: $xslFile doest not exists. Aborting."); //$NON-NLS-1$
			return false;
		}

		Document doc = DomUtils.load(xslFile);
		NodeList list = doc.getElementsByTagName("xsl:param"); //$NON-NLS-1$
		for (int i = 0; i < list.getLength(); i++) {
			Element e = (Element) list.item(i);
			String name = e.getAttribute("name"); //$NON-NLS-1$

			if (parameters.keySet().contains(name)) {
				e.setTextContent(parameters.get(name));
			}
		}

		return DomUtils.save(doc, xslFile);
	}

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File xslFile = new File("/home/mdecorde/TEMP/testori/mss-dates/txm/mss-dates-w/xsl/4-edition/1-facsimile-pager.xsl"); //$NON-NLS-1$
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("image-directory", "/home/mdecorde/TEMP/testori/mss-dates/img222222222222"); //$NON-NLS-1$ //$NON-NLS-2$
		UpdateXSLParameters p = new UpdateXSLParameters(xslFile);
		if (!p.process(parameters)) {
			System.out.println("Fail"); //$NON-NLS-1$
		} else {
			System.out.println("Success"); //$NON-NLS-1$
		}
	}
}
