package org.txm.utils.image;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

public class BMPUtils {

	public static void toRLE8(File inputFile, File outputFile) throws Exception {
		fromTo(inputFile, outputFile, "BI_RLE8"); //$NON-NLS-1$
	}

	public static void toRLE4(File inputFile, File outputFile) throws Exception {
		fromTo(inputFile, outputFile, "BI_RLE4"); //$NON-NLS-1$
	}

	public static void emptySquare(File outputFile, int size, String comp) throws Exception {

		BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_BYTE_INDEXED);

		ImageWriter writer = ImageIO.getImageWritersByFormatName("bmp").next(); //$NON-NLS-1$
		ImageWriteParam param = writer.getDefaultWriteParam();

		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionType(comp);

		writer.setOutput(new FileImageOutputStream(outputFile));
		writer.write(null, new IIOImage(bi, null, null), param);
		writer.dispose();
	}

	public static void fromTo(File inputFile, File outputFile, String comp) throws Exception {

		BufferedImage bi = ImageIO.read(inputFile); // new BufferedImage(size,
													// size,
													// BufferedImage.TYPE_BYTE_INDEXED);

		ImageWriter writer = ImageIO.getImageWritersByFormatName("bmp").next(); //$NON-NLS-1$
		ImageWriteParam param = writer.getDefaultWriteParam();

		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionType(comp);

		writer.setOutput(new FileImageOutputStream(outputFile));
		writer.write(null, new IIOImage(bi, null, null), param);
		writer.dispose();
	}

	public static void main(String[] args) throws Exception {

		// toRLE4(new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 16x16 8-bit.bmp"),
		// new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 16x16 8-bit RLE8.bmp"));
		//
		// toRLE4(new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 48x48 8-bit.bmp"),
		// new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 48x48 8-bit RLE8.bmp"));
		//
		// toRLE4(new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 256x256 32-bit.bmp"),
		// new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 256x256 32-bit RLE8.bmp"));

		emptySquare(new File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM logo 32x32 8-bit RLE8.bmp"), //$NON-NLS-1$
				32, "BI_RLE8"); //$NON-NLS-1$

		// emptySquare(
		// new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 48x48 8-bit RLE8.bmp"),
		// 48, "BI_RLE8");
		//
		// emptySquare(
		// new
		// File("/home/mdecorde/eclipse-tycho2022-09/bundles/org.txm.rcp/icons/logo/TXM
		// logo 256x256 32-bit RLE8.bmp"),
		// 256, "BI_RLE8");
	}
}
