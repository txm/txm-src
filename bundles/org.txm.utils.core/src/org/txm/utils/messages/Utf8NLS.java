package org.txm.utils.messages;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.eclipse.osgi.util.NLS;

/**
 * Class that manages UTF-8 encoding for .properties messages files.
 * 
 * @author sjacquot
 *
 */
public class Utf8NLS extends NLS {

	private static final int MOD_EXPECTED = Modifier.PUBLIC | Modifier.STATIC;
	private static final int MOD_MASK = MOD_EXPECTED | Modifier.FINAL;

	/**
	 * initialize UTF-8 messages using the Message class package path as base
	 * name
	 * 
	 * @param clazz
	 */
	public static void initializeMessages(Class<?> clazz) {
		initializeMessages(clazz.getPackage().getName() + ".messages", clazz); //$NON-NLS-1$
	}

	/**
	 * Initializes messages using UTF-8 encoding.
	 * 
	 * @param baseName
	 * @param clazz
	 */
	public static void initializeMessages(final String baseName, final Class<?> clazz) {
		// initialize resource bundle
		NLS.initializeMessages(baseName, clazz);

		// re-encode the fields
		final Field[] fieldArray = clazz.getDeclaredFields();
		final int len = fieldArray.length;
		for (int i = 0; i < len; i++) {
			final Field field = (Field) fieldArray[i];

			if ((field.getModifiers() & MOD_MASK) != MOD_EXPECTED) // only init
																	// public
																	// static
																	// fields
				continue;

			if (field.getType() != java.lang.String.class)
				continue;

			if (!field.isAccessible())
				field.setAccessible(true);
			try {
				final String rawValue = (String) field.get(null);
				field.set(null, new String(rawValue.getBytes("ISO-8859-1"), "UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
	}

	/**
	 * Binds the given message's substitution locations with the given string
	 * values.
	 * 
	 * @param message
	 * @param bindings
	 * @return the given message's substitution locations with the given string
	 *         values.
	 */
	public static String bind(String message, Object... bindings) {
		return NLS.bind(message, bindings);
	}
}
