package org.txm.utils.messages;

import org.eclipse.osgi.util.NLS;

/**
 * Utils core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class UtilsCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.utils.messages.messages"; //$NON-NLS-1$

	public static String ExecTimer_0;
	public static String ExecTimer_1;
	public static String ExecTimer_2;
	public static String ExecTimer_3;
	public static String Localizer_0P0;
	public static String Localizer_1;
	public static String FileCopy_0P0;
	public static String FileCopy_2P0;
	public static String FileCopy_4P0;
	public static String LS_0P0;
	public static String LS_1;
	public static String Log_23;

	public static String Log_3P0;
	public static String Log_5;
	public static String Log_8;
	public static String MessageBox_0;
	public static String MessageBox_1;
	public static String MessageBox_2;
	public static String Pair_0;
	public static String Pair_1;
	public static String Sh_4P0AtP1;

	public static String errorWritingInP0P1;
	public static String errorReadingInP0P1;

	public static String P0OnP1;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, UtilsCoreMessages.class);
	}
}
