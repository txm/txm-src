package org.txm.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.knallgrau.utils.textcat.TextCategorizer;
import org.txm.utils.io.IOUtils;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

public class LangDetector {
	public static int MINIMALSIZE = 5000;

	String lang = System.getProperty("user.language"); //$NON-NLS-1$
	String encoding = System.getProperty("file.encoding"); //$NON-NLS-1$

	LinkedHashMap<String, Integer> langs = new LinkedHashMap<String, Integer>();
	static HashMap<String, String> translations = new HashMap<String, String>();
	static {
		translations.put("french", "fr"); //$NON-NLS-1$
		translations.put("english", "en"); //$NON-NLS-1$
		translations.put("spanish", "sp"); //$NON-NLS-1$
		translations.put("italian", "it"); //$NON-NLS-1$
		translations.put("danish", "da"); //$NON-NLS-1$
		translations.put("swedish", "sv"); //$NON-NLS-1$
		translations.put("hungarian", "hu"); //$NON-NLS-1$
		translations.put("slovakian", "sk"); //$NON-NLS-1$
		translations.put("german", "de"); //$NON-NLS-1$
		translations.put("dutch", "nl"); //$NON-NLS-1$
		translations.put("albanian", "al"); //$NON-NLS-1$
		translations.put("norwegian", "no"); //$NON-NLS-1$
		translations.put("finnish", "fi"); //$NON-NLS-1$
		translations.put("slovenian", "sl"); //$NON-NLS-1$
		translations.put("polish", "pl"); //$NON-NLS-1$
	}

	TextCategorizer guesser = new TextCategorizer();

	public LangDetector(File file) throws DOMException, IOException, ParserConfigurationException, SAXException {
		this(file, "UTF-8"); //$NON-NLS-1$
	}

	public LangDetector(File file, String encoding) throws DOMException, IOException, ParserConfigurationException, SAXException {
		if (file.isDirectory())
			processDirectory(file);
		else
			processFile(file);
	}

	private void processDirectory(File dir) throws IOException, DOMException, ParserConfigurationException, SAXException {
		// println "process $dir"
		if (dir.isDirectory()) {
			for (File file : dir.listFiles(IOUtils.HIDDENFILE_FILTER))
				if (file.isFile())
					processFile(file);
		} else {
			processFile(dir);
		}

	}

	private void processFile(File file) throws IOException, DOMException, ParserConfigurationException, SAXException {
		// println "process $file"
		String text = null;
		if (file.getName().endsWith(".xml") || file.getName().endsWith(".XML")) { //$NON-NLS-1$
			text = DomUtils.load(file).getDocumentElement().getTextContent();
		} else {
			text = IOUtils.getText(file, encoding);
		}
		// println "text: "+text.length()
		String langcode = guesser.categorize(text);
		String guessedLang = translations.get(langcode);
		if (guessedLang == null) {
			guessedLang = langcode;
		}

		// println "$file guessed $guessedLang"
		if (!langs.containsKey(guessedLang))
			langs.put(guessedLang, 0);
		langs.put(guessedLang, langs.get(guessedLang) + 1);
	}

	public String getLang() {
		int max = 0;
		// println "LANGS: "+langs
		for (String s : langs.keySet()) {
			if (max < langs.get(s)) {
				max = langs.get(s);
				lang = s;
			}
		}
		return lang;
	}

	public static void configFromModels(File dir) {
		File[] files = dir.listFiles(IOUtils.HIDDENFILE_FILTER);
		Arrays.sort(files);
		for (File f : files)
			if (f.getName().contains(".lm") && !f.getName().contains("-")) //$NON-NLS-1$
				System.out.println(f.getAbsolutePath() + "\t" + f.getName().substring(0, f.getName().length() - 3)); //$NON-NLS-1$
	}

	public HashMap<String, Integer> getLangs() {
		return langs;
	}
}
