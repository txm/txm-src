// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-10-27 16:30:30 +0200 (Thu, 27 Oct 2016) $
// $LastChangedRevision: 3331 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Utility methods to extract, compress, list files of ZIP files
 */
public class Zip {

	// /** The ZIP extension. */
	// @Deprecated
	// public static String ZIP_EXTENSION = ".odt"; // no more used, stay for
	// old Groovy script compatibility ?

	/** The DEFAUL level of compression. */
	public static int DEFAULT_LEVEL_COMPRESSION = Deflater.BEST_COMPRESSION;

	/**
	 * Compress.
	 *
	 * @param file
	 *            the file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final File file) throws IOException {
		compress(file, file, DEFAULT_LEVEL_COMPRESSION, null, null);
	}

	/**
	 * Compress.
	 *
	 * @param file
	 *            the file
	 * @param target
	 *            the target
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final File file, final File target) throws IOException {
		compress(file, target, DEFAULT_LEVEL_COMPRESSION, null, null);
	}

	/**
	 * Compress.
	 *
	 * @param file
	 *            the file
	 * @param targetZipFile
	 *            the Zip file to create
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final File file, final File targetZipFile, IProgressMonitor monitor) throws IOException {
		compress(file, targetZipFile, DEFAULT_LEVEL_COMPRESSION, monitor, null);
	}

	/**
	 * Compress.
	 *
	 * @param file
	 *            the file
	 * @param targetZipFile
	 *            the Zip file to create
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final File file, final File targetZipFile, IProgressMonitor monitor, HashSet<String> ignore) throws IOException {
		compress(file, targetZipFile, DEFAULT_LEVEL_COMPRESSION, monitor, ignore);
	}

	// Compresse un fichier à l'adresse pointée par le fichier cible.
	// Remplace le fichier cible s'il existe déjà.
	/**
	 * Compresse un fichier à l'adresse pointée par le fichier cible. Remplace
	 * le fichier cible s'il existe déjà..
	 *
	 * @param files
	 *            the files to process
	 * @param targetZipFile
	 *            the target
	 * @param compressionLevel
	 *            the compression level
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final File[] files, final File targetZipFile, final int compressionLevel, IProgressMonitor monitor, HashSet<String> ignore) throws IOException {

		// Création du fichier zip
		final ZipOutputStream out = new ZipOutputStream(new FileOutputStream(targetZipFile));
		out.setMethod(ZipOutputStream.DEFLATED);
		out.setLevel(compressionLevel);

		for (File file : files) {
			// Ajout du(es) fichier(s) au zip
			final File source = file.getCanonicalFile();
			compressFile(out, "", source, monitor, ignore);
		}
		out.close();
	}

	// Compresse un fichier à l'adresse pointée par le fichier cible.
	// Remplace le fichier cible s'il existe déjà.
	/**
	 * Compresse un fichier à l'adresse pointée par le fichier cible. Remplace
	 * le fichier cible s'il existe déjà..
	 *
	 * @param file
	 *            the file
	 * @param targetZipFile
	 *            the target
	 * @param compressionLevel
	 *            the compression level
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final File file, final File targetZipFile, final int compressionLevel, IProgressMonitor monitor, HashSet<String> ignore) throws IOException {
		compress(new File[] { file }, targetZipFile, compressionLevel, monitor, ignore);
	}

	/**
	 * Compress.
	 *
	 * @param file
	 *            the file
	 * @param compressionLevel
	 *            the compression level
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final File file, final int compressionLevel) throws IOException {
		compress(file, file, compressionLevel, null, null);
	}

	/**
	 * Compress.
	 *
	 * @param fileName
	 *            the file name
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final String fileName) throws IOException {
		compress(new File(fileName), new File(fileName), DEFAULT_LEVEL_COMPRESSION, null, null);
	}

	/**
	 * Compress.
	 *
	 * @param fileName
	 *            the file name
	 * @param compressionLevel
	 *            the compression level
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final String fileName, final int compressionLevel) throws IOException {
		compress(new File(fileName), new File(fileName), compressionLevel, null, null);
	}

	/**
	 * Compress.
	 *
	 * @param fileName
	 *            the file name
	 * @param targetName
	 *            the target name
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final String fileName, final String targetName) throws IOException {
		compress(new File(fileName), new File(targetName), DEFAULT_LEVEL_COMPRESSION, null, null);
	}

	/**
	 * Compress.
	 *
	 * @param fileName
	 *            the file name
	 * @param targetName
	 *            the target name
	 * @param compressionLevel
	 *            the compression level
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void compress(final String fileName, final String targetName, final int compressionLevel) throws IOException {
		compress(new File(fileName), new File(targetName), compressionLevel, null, null);
	}

	/**
	 * Compress file or directory into an archive.
	 *
	 * @param out
	 *            the out
	 * @param parentFolder
	 *            the parent folder
	 * @param file
	 *            the file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private final static void compressFile(final ZipOutputStream out, final String parentFolder, final File file, IProgressMonitor monitor, HashSet<String> ignore) throws IOException {
		final String zipName = new StringBuilder(parentFolder).append(file.getName()).append(file.isDirectory() ? '/' : "").toString(); //$NON-NLS-1$

		// Définition des attributs du fichier
		final ZipEntry entry = new ZipEntry(zipName);
		entry.setSize(file.length());
		entry.setTime(file.lastModified());
		out.putNextEntry(entry);

		// Traitement récursif s'il s'agit d'un dossier
		// System.out.println("COMPRESS "+file+" monitor="+monitor);
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			if (files != null) {
				if (monitor != null)
					monitor.beginTask("Compressing " + file + " directory (" + files.length + " files).", files.length); //$NON-NLS-1$
				for (final File f : files) {
					if (ignore != null && ignore.contains(f.getName())) {
						// continue; // file is ignored
					} else {
						compressFile(out, zipName.toString(), f, null, null);
					}

					if (monitor != null) {
						monitor.worked(1);
					}
				}
				if (monitor != null)
					monitor.done();
			}
		} else {
			// Ecriture du fichier dans le zip
			final InputStream in = new BufferedInputStream(new FileInputStream(file));

			try {
				if (monitor != null)
					monitor.beginTask("Compressing " + file + " file (" + file.length() + " bytes).", (int) file.length()); //$NON-NLS-1$
				final byte[] buf = new byte[8192];
				int bytesRead;
				while (-1 != (bytesRead = in.read(buf))) {
					out.write(buf, 0, bytesRead);
					if (monitor != null)
						monitor.worked(bytesRead);
				}
				if (monitor != null)
					monitor.done();
			} finally {
				in.close();
			}
		}
	}

	/**
	 * Decompress a ZIP file in the ZIP file parent directory
	 *
	 * @param zipfile
	 *            the ZIP file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final File zipfile) throws IOException {
		decompress(zipfile, zipfile.getCanonicalFile().getParentFile(), false, null);
	}

	/**
	 * Decompress.
	 *
	 * @param zipFile
	 *            the file
	 * @param deleteZipAfter
	 *            the delete zip after
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final File zipFile, final boolean deleteZipAfter) throws IOException {
		decompress(zipFile, zipFile.getCanonicalFile().getParentFile(), deleteZipAfter, null);
	}

	// Décompresse un fichier zip à l'adresse indiquée par le dossier
	/**
	 * Decompress.
	 *
	 * @param zipFile
	 *            the file
	 * @param folder
	 *            the folder
	 * @param deleteZipAfter
	 *            the delete zip after
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final File zipFile, final File folder, final boolean deleteZipAfter) throws IOException {
		decompress(zipFile, folder, deleteZipAfter, null);
	}

	// Décompresse un fichier zip à l'adresse indiquée par le dossier
	/**
	 * Decompress.
	 *
	 * @param zipFile
	 *            the file
	 * @param folder
	 *            the folder
	 * @param deleteZipAfter
	 *            the delete zip after
	 * @param monitor
	 *            change progress state
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final File zipFile, final File folder, final boolean deleteZipAfter, IProgressMonitor monitor) throws IOException {
		ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile.getCanonicalFile())));
		ZipEntry ze;
		int nEntries = 0;
		// System.out.println("count entries...");
		try {
			while (null != (ze = zis.getNextEntry())) {
				nEntries++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (zis != null)
				zis.close();
		}
		// System.out.println("count entries result: "+nEntries);

		zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile.getCanonicalFile())));
		// System.out.println("zis: "+zis);
		if (monitor != null) {
			monitor.beginTask("Extracting " + zipFile, nEntries); //$NON-NLS-1$
		}

		try {
			// Parcours tous les fichiers
			// System.out.println("start parsing zip file: "+file);
			while (null != (ze = zis.getNextEntry())) {
				if (monitor != null)
					monitor.worked(1);

				final File f = new File(folder.getCanonicalPath(), ze.getName());
				// System.out.println(f);
				if (f.exists())
					f.delete();

				// Création des dossiers
				if (ze.isDirectory()) {
					f.mkdirs();
					continue;
				}
				f.getParentFile().mkdirs();
				final OutputStream fos = new BufferedOutputStream(new FileOutputStream(f));

				// Ecriture des fichiers
				try {
					try {
						final byte[] buf = new byte[8192];
						int bytesRead;
						while (-1 != (bytesRead = zis.read(buf))) {
							fos.write(buf, 0, bytesRead);
						}
					} finally {
						fos.close();
					}
				} catch (final IOException ioe) {
					f.delete();
					throw ioe;
				}
			}
		} finally {
			zis.close();
			// if (monitor != null) monitor.done();
		}

		if (deleteZipAfter)
			zipFile.delete();
	}

	/**
	 * Decompress.
	 *
	 * @param fileName
	 *            the file name
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final String fileName) throws IOException {
		decompress(new File(fileName));
	}

	/**
	 * Decompress.
	 *
	 * @param zipFileName
	 *            the file name
	 * @param deleteZipAfter
	 *            the delete zip after
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final String zipFileName, final boolean deleteZipAfter) throws IOException {
		decompress(new File(zipFileName), deleteZipAfter);
	}

	/**
	 * Decompress.
	 *
	 * @param zipFileName
	 *            the file name
	 * @param folderName
	 *            the folder name
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final String zipFileName, final String folderName) throws IOException {
		decompress(new File(zipFileName), new File(folderName), false);
	}

	/**
	 * Decompress.
	 *
	 * @param zipFileName
	 *            the file name
	 * @param folderName
	 *            the folder name
	 * @param deleteZipAfter
	 *            the delete zip after
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final String zipFileName, final String folderName, final boolean deleteZipAfter) throws IOException {
		decompress(new File(zipFileName), new File(folderName), deleteZipAfter, null);
	}

	/**
	 * Decompress.
	 *
	 * @param zipFileName
	 *            the file name
	 * @param folderName
	 *            the folder name
	 * @param deleteZipAfter
	 *            the delete zip after
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void decompress(final String zipFileName, final String folderName, final boolean deleteZipAfter, IProgressMonitor monitor) throws IOException {
		decompress(new File(zipFileName), new File(folderName), deleteZipAfter, monitor);
	}

	/**
	 * Extract one file from an archive given a relative path in the archive
	 * 
	 * @param zipFile
	 *            the archive
	 * @param path
	 *            the relative path in the archive
	 * @param outputFile
	 *            the output file
	 * @return true if success
	 */
	public static boolean extractOneFile(File zipFile, String path, File outputFile) {
		return extractOneFile(zipFile, path, outputFile, false);
	}

	/**
	 * Extract one file from an archive given a relative path in the archive
	 * 
	 * @param zipFile
	 *            the archive
	 * @param path
	 *            the relative path in the archive of the file to extract (file
	 *            or directory)
	 * @param outputFile
	 *            the output file
	 * @param replaceOnlyIfNewer
	 *            if true the file is replaced only if the archive file is more
	 *            recent
	 * @return true if success
	 */
	public static boolean extractOneFile(File zipFile, String path, File outputFile, boolean replaceOnlyIfNewer) {

		boolean copySubEntries = false; // internal variable to copy entries of
										// a directory
		boolean pathFound = false; // internal variable to know if path was
									// found

		try {
			long oldFileTime = outputFile.lastModified();

			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile.getCanonicalFile())));
			ZipEntry ze;

			// Parcourt tous les fichiers
			while (null != (ze = zis.getNextEntry())) {
				// System.out.println("P="+ze.getName());
				String currentpath = ze.getName();
				if (currentpath.equals(path)) { // this is the file to extract !
					if (replaceOnlyIfNewer && ze.getTime() < oldFileTime) { // don't
																			// replace
																			// the
																			// file
						zis.close();
						return true;
					}
					if (ze.isDirectory()) { // the path point a directory
						copySubEntries = true;
						outputFile.mkdirs();
						pathFound = true;
					} else { // the path point a file
						writeFileEntry(zis, outputFile);
						pathFound = true;
					}
				} else if (currentpath.startsWith(path) && copySubEntries) {
					String subpath = currentpath.substring(path.length());

					if (ze.isDirectory()) { // a sub directory of the pointed
											// path
						new File(outputFile, subpath).mkdirs();
					} else { // a sub file of the pointed path (directory)
						writeFileEntry(zis, new File(outputFile, subpath));
					}
				} else {
					copySubEntries = false; // stop copying sub entries
				}
			}
			if (!pathFound) {
				System.out.println("Error: " + path + " not found."); //$NON-NLS-1$
			}

			if (zis != null) {
				try {
					zis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("Fail to extract " + path + " from " + zipFile + ": " + e); //$NON-NLS-1$
			Log.printStackTrace(e);
		}

		return pathFound; // no suitable entry found
	}

	/**
	 * Write the current Zis entry to a file
	 * 
	 * @param zis
	 * @param outputFile
	 * @throws IOException
	 */
	private static void writeFileEntry(ZipInputStream zis, File outputFile) throws IOException {
		OutputStream fos = new BufferedOutputStream(new FileOutputStream(outputFile));

		// Ecriture des fichiers
		try {
			try {
				final byte[] buf = new byte[8192];
				int bytesRead;
				while (-1 != (bytesRead = zis.read(buf)))
					fos.write(buf, 0, bytesRead);
			} finally {
				fos.close();
			}
		} catch (final IOException ioe) {
			throw ioe;
		}
	}

	/**
	 * Try to get the root folder of the archive. If there is no root folder,
	 * the Zip filename is used
	 * 
	 * @param zipFile
	 * @return the root directory of the archive
	 */
	public static String getRoot(File zipFile) {
		String root = null;
		for (String f : listFiles(zipFile)) {
			root = f;
			if (f.startsWith(".") && f.indexOf(".") > 0) //$NON-NLS-1$
				continue;
			break;
		}
		// System.out.println("BIN ROOT: "+root);
		if (root == null) { // no sub-directory found, we use the zip filename
			root = zipFile.getName();
			if (root.indexOf(".") > 0) // remove the extension //$NON-NLS-1$
				root = root.substring(0, root.indexOf(".")); //$NON-NLS-1$
		}

		int idx = root.indexOf("/"); //$NON-NLS-1$
		if (idx > 0)
			root = root.substring(0, idx);
		return root;
	}

	/**
	 * List the ZIP file content
	 * 
	 * @param zipFile
	 * @return the list of files in the archive
	 */
	public static ArrayList<String> listFiles(File zipFile) {
		ArrayList<String> list = new ArrayList<>();
		Enumeration<?> zipEntries;
		ZipFile zip = null;
		try {
			zip = new ZipFile(zipFile);
			zipEntries = zip.entries();
			while (zipEntries.hasMoreElements()) {
				list.add(((ZipEntry) zipEntries.nextElement()).getName());
			}
			Collections.sort(list);
		} catch (Exception e) {
			Log.warning("Fail to list files of " + zipFile + ": " + e); //$NON-NLS-1$
			Log.printStackTrace(e);
			return new ArrayList<>();
		} finally {
			if (zip != null) {
				try {
					zip.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws IOException {
		// File zipFile = new
		// File("/home/mdecorde/Téléchargements/bvhepistemon-bin.zip");
		// ArrayList<String> list = Zip.listFiles(zipFile);
		// System.out.println(list);
		// System.out.println("outdir: "+list.get(0));
		// System.out.println("outdir fixed: "+Zip.getRoot(zipFile));
		File dir = new File("workspace079/org.txm.setups/exportRCP/linux.gtk.x86_64/TXM/plugins"); //$NON-NLS-1$
		File jarFile = new File(dir, "org.txm.statsengine.r.core_1.0.0.201805091353.jar"); //$NON-NLS-1$
		File outDir = new File("/TEMP"); //$NON-NLS-1$
		// System.out.println(StringUtils.join(Zip.listFiles(jarFile), "\n"));
		System.out.println("Zip result: " + Zip.extractOneFile(jarFile, "R/", outDir)); //$NON-NLS-1$

	}

	public static boolean hasEntries(File zipFile, String... paths) {
		HashSet<String> toFind = new HashSet<String>(Arrays.asList(paths));
		try {

			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile.getCanonicalFile())));
			ZipEntry ze;

			// Parcours tous les fichiers
			while (null != (ze = zis.getNextEntry())) {
				String currentpath = ze.getName();
				// String filename = currentpath;
				// if (filename.endsWith("/")) {
				// filename = filename.substring(0, filename.length()-1);
				// }
				// if (filename.lastIndexOf("/") > 0) {
				// filename = filename.substring(filename.lastIndexOf("/")+1);
				// }
				if (toFind.contains(currentpath)) {
					toFind.remove(currentpath);
					if (toFind.size() == 0) {
						try {
							zis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						return true;
					}
				}
			}

			if (zis != null) {
				try {
					zis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("Fail to search for " + paths + " from " + zipFile + ": " + e); //$NON-NLS-1$
			Log.printStackTrace(e);
		}

		return toFind.size() == 0;
	}
}
