package org.txm.utils.zip;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZip {
	public static void compress(File infile, File gzFile) throws FileNotFoundException, IOException {

		FileInputStream finStream = new FileInputStream(infile);
		BufferedInputStream bufinStream = new BufferedInputStream(finStream);
		FileOutputStream outStream = new FileOutputStream(gzFile);
		GZIPOutputStream goutStream = new GZIPOutputStream(outStream);
		byte[] buf = new byte[1024];
		int i;
		while ((i = bufinStream.read(buf)) >= 0) {
			goutStream.write(buf, 0, i);
		}
		bufinStream.close();
		goutStream.close();
	}

	/**
	 * unzip using gz filename to name the output file
	 * 
	 * @param gzFile
	 * @param outDir
	 * @return the extracted file
	 * @throws IOException
	 */
	public static File uncompress(File gzFile, File outDir) throws IOException {
		// To Uncompress GZip File Contents we need to open the gzip file.....
		GZIPInputStream gzipInputStream = new GZIPInputStream(new FileInputStream(gzFile));

		String outFileName = gzFile.getName();
		outFileName = outFileName.substring(0, outFileName.length() - 3); // remove
																			// ".gz"
		File outFile = new File(outDir, outFileName);
		OutputStream out = new FileOutputStream(outFile);

		byte[] buf = new byte[1024]; // size can be

		int len;
		while ((len = gzipInputStream.read(buf)) > 0) {
			out.write(buf, 0, len);
		}

		gzipInputStream.close();
		out.close();

		return outFile;
	}
}
