package org.txm.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Download {
	public static long download(URL source, File output) throws IOException {
		ReadableByteChannel rbc = Channels.newChannel(source.openStream());
		FileOutputStream fos = new FileOutputStream(output);
		return fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
	}
}