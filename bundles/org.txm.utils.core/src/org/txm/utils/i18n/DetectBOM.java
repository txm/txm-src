package org.txm.utils.i18n;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Simple class to detect BOM in a file
 * 
 * @see "http://blog.ymango.com/2010/09/16/29"
 * 
 * @author mdecorde
 *
 */
public class DetectBOM {

	Charset enc = null;
	int n, unread, size;

	/**
	 * 
	 * @param srcFile
	 *            the file to test
	 */
	public DetectBOM(File srcFile) {
		// *** Use Default of Encoding.Default (Ansi CodePage)

		try {
			// *** Detect byte order mark if any - otherwise assume default
			byte[] buffer = new byte[4];
			FileInputStream file = new FileInputStream(srcFile);
			n = file.read(buffer, 0, buffer.length);
			file.close();

			String encoding = null;
			if ((buffer[0] == (byte) 0x00) && (buffer[1] == (byte) 0x00) && (buffer[2] == (byte) 0xFE) && (buffer[3] == (byte) 0xFF)) {
				encoding = "UTF-32BE"; //$NON-NLS-1$
				unread = n - 4;
				size = 4;
			} else if ((buffer[0] == (byte) 0xFF) && (buffer[1] == (byte) 0xFE) && (buffer[2] == (byte) 0x00) && (buffer[3] == (byte) 0x00)) {
				encoding = "UTF-32LE"; //$NON-NLS-1$
				unread = n - 4;
				size = 4;
			} else if ((buffer[0] == (byte) 0xEF) && (buffer[1] == (byte) 0xBB) && (buffer[2] == (byte) 0xBF)) {
				encoding = "UTF-8"; //$NON-NLS-1$
				unread = n - 3;
				size = 3;
			} else if ((buffer[0] == (byte) 0xFE) && (buffer[1] == (byte) 0xFF)) {
				encoding = "UTF-16BE"; //$NON-NLS-1$
				unread = n - 2;
				size = 2;
			} else if ((buffer[0] == (byte) 0xFF) && (buffer[1] == (byte) 0xFE)) {
				encoding = "UTF-16LE"; //$NON-NLS-1$
				unread = n - 2;
				size = 4;
			} else {
				// Unicode BOM mark not found, unread all bytes
				// encoding = "UTF-8";
				unread = n;
				size = 0;
			}
			if (encoding != null)
				enc = Charset.forName(encoding);

		} catch (FileNotFoundException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		// System.out.println("BOM size: "+size);
		// return enc;
	}

	/**
	 * 
	 * @return if the file has a BOM, return the encoding, else return file
	 *         system encoding
	 */
	public Charset getCharSet() {
		if (enc != null)
			return enc;
		else
			return Charset.forName(System.getProperty("file.encoding")); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return the number of byte to skip to read the file
	 */
	public int getBOMSize() {
		return size;
	}

	/**
	 * 
	 * @return true if the file has a BOM
	 */
	public boolean hasBOM() {
		return size > 0;
	}

	public static void main(String args[]) {
		DetectBOM db = new DetectBOM(new File(System.getProperty("user.home"), "xml/SUPPORT/CS/3-revues_eng_complet2.txt")); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println(db.getBOMSize());
		System.out.println(db.getCharSet());
	}
}
