// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.i18n;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.txm.utils.messages.UtilsCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * Format string using a Resource bundle mechanism for managing languages.
 * 
 * @author sloiseau
 */
public class Localizer {

	/** The cls. */
	private final Class<?> cls;

	/** The loc. */
	private final Locale loc;

	/** The bundle. */
	private ResourceBundle bundle;

	/** The bundle in default locale. */
	private ResourceBundle default_bundle;

	/**
	 * Instantiates a new localizer.
	 * 
	 * @param cls
	 *            the cls
	 */
	public Localizer(Class<?> cls) {
		this(cls, null);
	}

	/**
	 * Instantiates a new localizer.
	 *
	 * @param cls
	 *            the cls
	 * @param loc
	 *            the loc
	 */
	public Localizer(Class<?> cls, Locale loc) {
		this.cls = cls;
		this.loc = loc;
	}

	/**
	 * Message.
	 * 
	 * @param key
	 *            the key
	 * 
	 * @return the string
	 */
	public String message(String key) {
		return MessageFormat.format(getString(key), new Object[] {});
	}

	/**
	 * Message.
	 * 
	 * @param key
	 *            the key
	 * @param arg
	 *            the arg
	 * 
	 * @return the string
	 */
	public String message(String key, Object arg) {
		return MessageFormat.format(getString(key), new Object[] { arg });
	}

	/**
	 * Message.
	 * 
	 * @param key
	 *            the key
	 * @param arg1
	 *            the arg1
	 * @param arg2
	 *            the arg2
	 * 
	 * @return the string
	 */
	public String message(String key, Object arg1, Object arg2) {
		return MessageFormat.format(getString(key), new Object[] { arg1, arg2 });
	}

	/**
	 * Message.
	 * 
	 * @param key
	 *            the key
	 * @param args
	 *            the args
	 * 
	 * @return the string
	 */
	public String message(String key, Object[] args) {
		return MessageFormat.format(getString(key), args);
	}

	/**
	 * Query the bundle for the string corresponding to a key.
	 * 
	 * If a given key does not exist in the loaded bundle, this class look for
	 * the key in a bundle with locale "en", "US". It the key is still not
	 * found, it return the key itself.
	 *
	 * @param key
	 *            the key
	 * @return the string
	 */
	private String getString(String key) {
		try {
			return getBundle().getString(key);
		} catch (MissingResourceException e1) {
			try {
				return UtilsCoreMessages.bind(UtilsCoreMessages.Localizer_0P0, getDefaultBundle().getString(key));
			} catch (MissingResourceException e2) {
				return key;
			}
		}
	}

	/**
	 * Gets the bundle.
	 * 
	 * @return the bundle
	 */
	private ResourceBundle getBundle() {
		if (bundle == null) {
			String s = getRessourcePrefix();
			bundle = loc == null ? ResourceBundle.getBundle(s + UtilsCoreMessages.Localizer_1) : ResourceBundle.getBundle(s + UtilsCoreMessages.Localizer_1, loc);
		}
		return bundle;
	}

	/**
	 * Gets the default bundle.
	 * 
	 * @return the default bundle
	 */
	private ResourceBundle getDefaultBundle() {
		if (default_bundle == null) {
			String s = getRessourcePrefix();
			default_bundle = ResourceBundle.getBundle(s + UtilsCoreMessages.Localizer_1, new Locale("en", "US")); //$NON-NLS-1$//$NON-NLS-2$
		}
		return default_bundle;
	}

	/**
	 * Gets the ressource prefix.
	 *
	 * @return the ressource prefix
	 */
	private String getRessourcePrefix() {
		String s = cls.getName();
		int i = s.lastIndexOf('.');
		if (i > 0)
			s = s.substring(0, i + 1);
		else
			s = ""; //$NON-NLS-1$
		return s;
	}

}
