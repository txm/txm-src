// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2016-12-15 10:26:28 +0100 (Thu, 15 Dec 2016) $
// $LastChangedRevision: 3368 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.i18n;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

// TODO: Auto-generated Javadoc
/**
 * depending on the locale, remove or add spaces between words.
 *
 * @author mdecorde
 */
public class LangFormater {

	/** The FR no space before. */
	static List<String> FRNoSpaceBefore = Arrays.asList(",", ".", ")", "]", "}", "°"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$

	/** The FR no space after. */
	static List<String> FRNoSpaceAfter = Arrays.asList("’", "'", "(", "[", "{"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

	/** The FRO no space before. */
	static List<String> FRONoSpaceBefore = Arrays.asList(",", ".", ")", "]", "}", "°"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$

	/** The FRO no space after. */
	static List<String> FRONoSpaceAfter = Arrays.asList("’", "'", "(", "[", "{"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

	/** The EN no space before. */
	static List<String> ENNoSpaceBefore = Arrays.asList(":", "!", "?", ";", ",", ".", ")", "]", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$
			"}", "»", "°", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"'s", "'re", "'ve", "'d", "'m", "'em", "'ll", "n't"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$

	/** The EN no space after. */
	static List<String> ENNoSpaceAfter = Arrays.asList("'", "(", "[", "{", "«"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

	/**
	 * Gets the no space before.
	 *
	 * @param lang
	 *            the lang
	 * @return the no space before
	 */
	public static List<String> getNoSpaceBefore(String lang) {

		if (lang.startsWith("fr") || lang.startsWith("FR")) //$NON-NLS-1$ //$NON-NLS-2$
			return (FRNoSpaceBefore);
		else if (lang.startsWith("fro") || lang.startsWith("FRO")) //$NON-NLS-1$ //$NON-NLS-2$
			return (FRONoSpaceBefore);
		else if (lang.startsWith("en") || lang.startsWith("EN") //$NON-NLS-1$ //$NON-NLS-2$
				|| lang.startsWith("us") || lang.startsWith("US")) //$NON-NLS-1$ //$NON-NLS-2$
			return (ENNoSpaceBefore);
		else
			return (ENNoSpaceBefore);
	}

	/**
	 * Gets the no space after.
	 *
	 * @param lang
	 *            the lang
	 * @return the no space after
	 */
	public static List<String> getNoSpaceAfter(String lang) {

		if (lang.startsWith("fr") || lang.startsWith("FR")) //$NON-NLS-1$ //$NON-NLS-2$
			return (FRNoSpaceAfter);
		else if (lang.startsWith("fro") || lang.startsWith("FRO")) //$NON-NLS-1$ //$NON-NLS-2$
			return (FRONoSpaceAfter);
		else if (lang.startsWith("en") || lang.startsWith("EN") //$NON-NLS-1$ //$NON-NLS-2$
				|| lang.startsWith("us") || lang.startsWith("US")) //$NON-NLS-1$ //$NON-NLS-2$
			return (ENNoSpaceAfter);
		else
			return (ENNoSpaceAfter);
	}

	/**
	 * Format.
	 *
	 * @param str
	 *            the str
	 * @param lang
	 *            the lang
	 * @return the string
	 */
	public static String format(String str, String lang) {
		if (lang == null) {
			lang = Locale.getDefault().getLanguage();
		}

		for (String punc : getNoSpaceAfter(lang)) {
			str = str.replace(punc + " ", punc); //$NON-NLS-1$
		}

		for (String punc : getNoSpaceBefore(lang)) {
			str = str.replace(" " + punc, punc); //$NON-NLS-1$
		}

		return str;
	}

	public static boolean isSpaceAfterNotNeeded(String str, String lang) {
		for (String t : getNoSpaceAfter(lang)) {
			if (str.endsWith(t)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isSpaceBeforeNotNeeded(String str, String lang) {
		for (String t : getNoSpaceBefore(lang)) {
			if (str.startsWith(t)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Format.
	 *
	 * @param str
	 *            the str
	 * @param lang
	 *            the lang
	 * @return the string
	 */
	public static String format(String str, Object lang) {
		if (lang == null)
			lang = Locale.getDefault().getLanguage();
		return format(str, lang.toString());
	}
}
