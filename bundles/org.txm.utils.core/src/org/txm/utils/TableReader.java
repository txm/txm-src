package org.txm.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.txm.libs.msoffice.ReadExcel;
import org.txm.libs.office.ReadODS;

/**
 * Read a table from a TSV, XLSX or ODS files.
 * 
 * 1- TableReader reader = new TableReader(file) 2- reader.readHeaders() 3- def
 * headers = reader.getHeaders() 4- while (reader.readRecord) {
 * reader.getRecord()} 5- reader.close()
 * 
 * @author mdecorde
 *
 */
public class TableReader {

	File tableFile;
	String mode = "tsv"; //$NON-NLS-1$

	LinkedHashMap<String, String> record;
	CsvReader tsvReader = null;
	ReadExcel excelReader = null;
	ReadODS odsReader = null;
	TEITableReader teiReader = null;

	/**
	 * Read all the file and creates a list of lines
	 * 
	 * @param tableFile
	 * @return a list of the lines content
	 * @throws Exception
	 */
	public final static ArrayList<ArrayList<String>> readAsList(File tableFile) throws Exception {
		return readAsList(tableFile, null);
	}

	/**
	 * Read all the file and creates a list of lines
	 * 
	 * @param tableFile
	 * @param sheet
	 * @return a list of the lines content
	 * @throws Exception
	 */
	public static ArrayList<ArrayList<String>> readAsList(File tableFile, String sheet) throws Exception {
		ArrayList<ArrayList<String>> result = new ArrayList<>();

		TableReader reader = new TableReader(tableFile, sheet);
		reader.readHeaders();
		String[] headers = reader.getHeaders();
		result.add(new ArrayList<>(Arrays.asList(headers)));
		while (reader.readRecord()) {
			ArrayList<String> line = new ArrayList<>();
			for (String h : headers) {
				line.add(reader.getRecord().get(h));
			}
			result.add(line);
		}

		reader.close();
		return result;
	}

	/**
	 * Read all the file and creates a table of lines (String[][])
	 * 
	 * @param tableFile
	 * @return String[lines][columns]
	 * @throws Exception
	 */
	public static String[][] readAsTable(File tableFile) throws Exception {
		return readAsTable(tableFile, null);
	}

	/**
	 * Read all the file and creates a table of lines (String[][])
	 * 
	 * @param tableFile
	 * @param sheet
	 * @return String[lines][columns]
	 * @throws Exception
	 */
	public static String[][] readAsTable(File tableFile, String sheet) throws Exception {

		ArrayList<ArrayList<String>> list = readAsList(tableFile, sheet);

		if (list.size() == 0)
			return new String[0][0];

		String[][] result = new String[list.size()][list.get(0).size()];

		int n = 0;
		for (ArrayList<String> line : list) {
			int c = 0;
			for (String s : line) {
				result[n][c] = s;
				c++;
			}
			n++;
		}
		return result;
	}

	public TableReader(File tableFile) throws Exception {
		this(tableFile, null, null, null);
	}
	
	public TableReader(File tableFile, String sheet) throws Exception {
		this(tableFile, sheet, null, null);
	}

	public TableReader(File tableFile, String sheet, String colseparator, String encoding) throws Exception {
		this.tableFile = tableFile;

		if (tableFile.getName().toLowerCase().endsWith(".xlsx") || tableFile.getName().toLowerCase().endsWith(".xls")) { //$NON-NLS-1$
			mode = "excel"; //$NON-NLS-1$
			excelReader = new ReadExcel(tableFile, sheet, true);
		} else if (tableFile.getName().toLowerCase().endsWith(".ods")) { //$NON-NLS-1$
			mode = "ods"; //$NON-NLS-1$
			odsReader = new ReadODS(tableFile, sheet);
		} else if (tableFile.getName().toLowerCase().endsWith(".tsv")) { //$NON-NLS-1$
			tsvReader = new CsvReader(tableFile.getAbsolutePath(), "\t".charAt(0), Charset.forName("UTF-8"), sheet); //$NON-NLS-1$ //$NON-NLS-2$
			mode = "tsv"; //$NON-NLS-1$
		} else if (tableFile.getName().toLowerCase().endsWith(".csv")) { //$NON-NLS-1$
			if (colseparator == null) colseparator = ","; //$NON-NLS-1$
			if (encoding == null) encoding = "UTF-8"; //$NON-NLS-1$
			tsvReader = new CsvReader(tableFile.getAbsolutePath(), colseparator.charAt(0), Charset.forName(encoding), sheet); //$NON-NLS-1$ //$NON-NLS-2$
			mode = "tsv"; //$NON-NLS-1$
		} else if (tableFile.getName().toLowerCase().endsWith(".xml")) { //$NON-NLS-1$
			teiReader = new TEITableReader(tableFile, sheet); //$NON-NLS-1$ //$NON-NLS-2$
			mode = "tei"; //$NON-NLS-1$
		}
	}

	public String getMode() {
		return mode;
	}

	public boolean readHeaders() throws IOException {
		if (tsvReader != null) {
			return tsvReader.readHeaders();
		} else if (excelReader != null) {
			return excelReader.readHeaders();
		} else if (odsReader != null) {
			return odsReader.readHeaders();
		} else if (teiReader != null) {
			return teiReader.readHeaders();
		}

		return false;
	}

	/**
	 * read the next record
	 * 
	 * @return true if there is another record
	 * @throws IOException
	 */
	public boolean readRecord() throws IOException {
		if (tsvReader != null) {
			if (tsvReader.readRecord()) {
				record = tsvReader.getRecord();
				return true;
			} else {
				return false;
			}
		} else if (excelReader != null) {
			if (excelReader.readRecord()) {
				record = excelReader.getRecord();
				return true;
			} else {
				return false;
			}
		} else if (odsReader != null) {
			if (odsReader.readRecord()) {
				record = odsReader.getRecord();
				return true;
			} else {
				return false;
			}
		} else if (teiReader != null) {
			if (teiReader.readRecord()) {
				record = teiReader.getRecord();
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	public LinkedHashMap<String, String> getRecord() {
		return record;
	}

	public String[] getHeaders() throws IOException {
		if (tsvReader != null) {
			return tsvReader.getHeaders();
		} else if (excelReader != null) {
			return excelReader.getHeaders();
		} else if (odsReader != null) {
			return odsReader.getHeaders();
		} else if (teiReader != null) {
			return teiReader.getHeaders();
		}

		return null;
	}

	String EMPTY = ""; //$NON-NLS-1$

	public String get(String h) throws IOException {
		String s = getRecord().get(h);
		if (s == null)
			return EMPTY;

		return s;
	}

	public void close() throws IOException {
		if (tsvReader != null) {
			tsvReader.close();
		} else if (excelReader != null) {
			excelReader.close();
		} else if (odsReader != null) {
			odsReader.close();
		} else if (teiReader != null) {
			teiReader.close();
		}
	}

	public static void main(String[] args) {
		File tableFile = new File("txm-ressources-master-supports-corpus/supports/corpus/voeux-metadata/metadata.xlsx"); //$NON-NLS-1$
		try {
			TableReader.readAsList(tableFile, "metadata"); //$NON-NLS-1$
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main2(String[] args) {
		try {
			System.out.println("Loading..."); //$NON-NLS-1$
			TableReader reader = new TableReader(new File("txm-ressources-master-supports-corpus/supports/corpus/voeux-metadata/metadata.xlsx"), null); //$NON-NLS-1$
			System.out.println("Table loaded"); //$NON-NLS-1$

			System.out.println("Reading headers..."); //$NON-NLS-1$
			reader.readHeaders();
			System.out.println(Arrays.toString(reader.getHeaders()));

			while (reader.readRecord()) {
				System.out.println(reader.getRecord());
			}
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
