// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-12-09 09:48:50 +0100 (Wed, 09 Dec 2015) $
// $LastChangedRevision: 3084 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.commons.lang.SystemUtils;

/**
 * Utils to delete files from a directory.
 *
 * @author mdecorde
 */
public class DeleteDir {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String args[]) {
		deleteDirectory(new File(args[0]));
	}

	static public boolean deleteDirectory(File path) {
		return deleteDirectory(path, false);
	}

	/**
	 * Delete directory.
	 *
	 * @param path
	 *            the path to the directory to delete
	 * @param definitive
	 *            definitivelly delete the directory or move it to trash FIXME
	 *            NOT IMPLEMENTED YET
	 * @return true, if successful
	 */
	static public boolean deleteDirectory(File path, boolean definitive) {

		if (path == null) {
			return false;
		}
		if (!path.exists()) {
			return false;
		}
		try { // try using system for faster reply and prefer OS-dependent
				// directory removal tool
			if (SystemUtils.IS_OS_WINDOWS) {
				Runtime.getRuntime().exec(new String[] { "%ComSpec%", "/C", "RD /S /Q \"" + path + '"' }).waitFor(); // FIXME: //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
																														// SJ:
																														// "%ComSpec%"
																														// is
																														// not
																														// defined,
																														// at
																														// least
																														// on
																														// my
																														// Win7
			} else if (SystemUtils.IS_OS_UNIX) {
				if (definitive) {
					Runtime.getRuntime().exec(new String[] { "/bin/rm", "-rf", path.toString() }).waitFor(); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					Runtime.getRuntime().exec(new String[] { "gio", "trash", path.toString() }).waitFor(); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}

			if (!path.exists()) { // it worked
				return true;
			}
		} catch (Exception e) {
			// fallback to internal implementation on error
			// e.printStackTrace();
		}

		if (path.exists() && path.isDirectory()) {
			File[] files = path.listFiles();
			if (files != null)
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						deleteDirectory(files[i], definitive);
					} else {
						files[i].delete();
					}
				}
		}
		return (path.delete());
	}

	/**
	 * Recursively scan a directory files
	 *
	 * @param directory
	 *            the directory
	 * @param ignorecached
	 *            the ignorecached
	 * @return the array list of files
	 */
	public static ArrayList<File> scanDirectory(File directory, boolean ignorecached, boolean recursive) {
		// System.out.println("scan directory : "+directory.getAbsolutePath());
		ArrayList<File> dirfiles = new ArrayList<File>();
		LinkedList<File> files = new LinkedList<File>();
		files.add(directory);

		while (!files.isEmpty()) {
			File current = files.removeFirst();

			if (current.isDirectory()) {
				// System.out.println(current);
				for (File sfile : current.listFiles()) {
					// System.out.println(" "+sfile);
					if (sfile.isDirectory()) {
						if (!recursive)
							continue;

						if (ignorecached) {
							if (!sfile.getName().startsWith(".")) //$NON-NLS-1$
								files.add(sfile);
						} else
							files.add(sfile);
					} else {
						dirfiles.add(sfile);
					}
				}
			}
		}
		// System.out.println(dirfiles);
		return dirfiles;
	}
}