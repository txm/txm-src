package org.txm.utils;

import java.io.File;

/**
 * Thread named with a filename
 * 
 * @author mdecorde
 *
 */
public abstract class ThreadFile implements Runnable {

	protected File f;

	public ThreadFile(String name, File f) {
		this.f = f;
	}

	public File getFile() {
		return f;
	}
}