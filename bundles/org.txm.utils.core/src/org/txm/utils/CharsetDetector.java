package org.txm.utils;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

import org.mozilla.universalchardet.CharsetListener;
import org.mozilla.universalchardet.UniversalDetector;
import org.txm.utils.io.IOUtils;

public class CharsetDetector {
	public static int MINIMALSIZE = 2000;
	String encoding = System.getProperty("file.encoding"); //$NON-NLS-1$
	LinkedHashMap<String, Integer> encodings = new LinkedHashMap<String, Integer>();

	public CharsetDetector(File file) {
		if (file.isDirectory())
			processDirectory(file);
		else
			try {
				processFile(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
	}

	private void processDirectory(File dir) {
		for (File file : dir.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (file.isFile()) {
				try {
					processFile(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}
		}
	}

	String guessedCharset = System.getProperty("file.encoding"); //$NON-NLS-1$

	private void processFile(File file) throws IOException {
		guessedCharset = System.getProperty("file.encoding"); //$NON-NLS-1$
		UniversalDetector detector = new UniversalDetector(new CharsetListener() {
			@Override
			public void report(String name) {
				guessedCharset = name;
			}
		});

		byte[] buf = new byte[4096];
		java.io.FileInputStream fis = new java.io.FileInputStream(file);

		int nread;
		while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
			detector.handleData(buf, 0, nread);
		}
		detector.dataEnd();

		if (!encodings.containsKey(guessedCharset))
			encodings.put(guessedCharset, 0);
		encodings.put(guessedCharset, encodings.get(guessedCharset) + 1);
		fis.close();
	}

	public String getEncoding() {
		int max = 0;
		for (String s : encodings.keySet()) {
			if (max < encodings.get(s)) {
				max = encodings.get(s);
				encoding = s;
			}
		}
		return encoding;
	}

	public LinkedHashMap<String, Integer> getEncodings() {
		return encodings;
	}

	public static void main(String[] args) {
		CharsetDetector detector = new CharsetDetector(new File("/home/mdecorde/Bureau/testencoding")); //$NON-NLS-1$
		System.out.println(detector.getEncodings());
	}
}
