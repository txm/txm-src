package org.txm.utils;

public class PatternUtils {
	
	public static final String regexChars = "^$\\.*+?()[]{}|";
	public static final char regexQuoteChar = '\\';
	
	public static String quote(String str) {
		return quote(str, regexChars, regexQuoteChar);
	}
	
	/**
	 * 
	 * @return the str stripped of the quoted regex chars
	 */
	public static String quote(String str, String specialChars) {
		return quote(str, specialChars, regexQuoteChar);
	}
	
	public static String quote(String str, String specialChars, char quoteChar) {
		
		if (str.length() == 0) return str;
		
		StringBuffer buff = new StringBuffer();
		// for(char c : chars.)
		for (int i = 0; i < str.length(); i++) {
			int j = specialChars.indexOf(str.charAt(i));

			if (j >= 0) {
				buff.append("" + quoteChar + str.charAt(i)); //$NON-NLS-1$
			}
			else {
				buff.append(str.charAt(i));
			}
		}
		return buff.toString();
	}
	
	/**
	 * 
	 * @return the str stripped of the quoted regex chars
	 */
	public static String unquote(String str) {
		return unquote(str, regexChars, regexQuoteChar);
	}
	
	/**
	 * 
	 * @return the str stripped of the quoted regex chars
	 */
	public static String unquote(String str, String specialChars) {
		return unquote(str, specialChars, regexQuoteChar);
	}
	
	/**
	 * 
	 * @param str
	 * @param specialChars
	 * @return the str stripped of the quoted chars
	 */
	public static String unquote(String str, String specialChars, char quoteChar) {
		
		if (str.length() == 0) return str;
		
		StringBuffer buff = new StringBuffer();
		boolean writeNext = false;
		for (int i = 0; i < str.length() -1; i++) { // last char is not a special char
			
			writeNext = false;
			for (int j = 0 ; j < specialChars.length() ; j++) {
				if (str.charAt(i) == quoteChar && str.charAt(i+1) == specialChars.charAt(j)) {
					writeNext = true;
					break;
				}
			}
			
			if (writeNext) {
				i++;
				if (str.length() > i) {
					buff.append(str.charAt(i)); // write the next char
				}
			} else {
				buff.append(str.charAt(i)); // write this char
			}
		}
		
		if (!writeNext) {
			buff.append(str.charAt(str.length()-1)); // write the last char which is not a special char
		}
		
		return buff.toString();
	}
	
	public static void main(String[] args) {
		
		String s = "\\\\ Aujourd'hui : .+ Hallo!"; //$NON-NLS-1$
		String special = ":=!"; //$NON-NLS-1$
		char quoteChar = '\\'; //$NON-NLS-1$
		
		String quoted = PatternUtils.quote(s, special, quoteChar);
		String unquoted = PatternUtils.unquote(quoted, special, quoteChar);
		
		System.out.println("S="+s); //$NON-NLS-1$
		System.out.println("Q="+quoted); //$NON-NLS-1$
		System.out.println("U="+unquoted); //$NON-NLS-1$
		System.out.println("S == U : "+ s.equals(unquoted)); //$NON-NLS-1$
	}
}
