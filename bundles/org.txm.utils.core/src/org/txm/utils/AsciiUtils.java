// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.utils;

import com.ibm.icu.text.Transliterator;

/**
 * The Class AsciiUtils.
 * 
 * FR:
 * 
 * AsciiUtils.convertNonAscii(str) supprime les accents
 * 
 * AsciiUtils.removePunct(str) supprime les ponctuations
 * 
 * AsciiUtils.buildWordId(str) créé un identifiant de mot CQP compatible
 * 
 * AsciiUtils.buildId(str) créé un identifiant compatible corpus CQP
 *
 * @see "http://www.rgagnon.com/javadetails/java-0456.html"
 */
public class AsciiUtils {

	/** The Constant PLAIN_ASCII. */
	private static final String PLAIN_ASCII = "AaEeIiOoUu" // grave //$NON-NLS-1$
			+ "AaEeIiOoUuYy" // acute //$NON-NLS-1$
			+ "AaEeIiOoUuYy" // circumflex //$NON-NLS-1$
			+ "AaOoNn" // tilde //$NON-NLS-1$
			+ "AaEeIiOoUuYy" // umlaut //$NON-NLS-1$
			+ "Aa" // ring //$NON-NLS-1$
			+ "Cc" // cedilla //$NON-NLS-1$
			+ "OoUu" // double acute //$NON-NLS-1$
	;

	/** The Constant UNICODE. */
	private static final String UNICODE = "\u00C0\u00E0\u00C8\u00E8\u00CC\u00EC\u00D2\u00F2\u00D9\u00F9" //$NON-NLS-1$
			+ "\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DD\u00FD" //$NON-NLS-1$
			+ "\u00C2\u00E2\u00CA\u00EA\u00CE\u00EE\u00D4\u00F4\u00DB\u00FB\u0176\u0177" //$NON-NLS-1$
			+ "\u00C3\u00E3\u00D5\u00F5\u00D1\u00F1" //$NON-NLS-1$
			+ "\u00C4\u00E4\u00CB\u00EB\u00CF\u00EF\u00D6\u00F6\u00DC\u00FC\u0178\u00FF" //$NON-NLS-1$
			+ "\u00C5\u00E5" //$NON-NLS-1$
			+ "\u00C7\u00E7" //$NON-NLS-1$
			+ "\u0150\u0151\u0170\u0171" //$NON-NLS-1$
	;

	// private constructor, can't be instanciated!
	/**
	 * Instantiates a new ascii utils.
	 */
	private AsciiUtils() {
	}

	public static Transliterator asciiFormmater = Transliterator.getInstance("Any-Latin; NFD; [^\\p{Alnum}\\p{p}] Remove"); //$NON-NLS-1$

	/**
	 * Convert non ascii characters. Warning punctuations are not removed
	 *
	 * @param s
	 *            the s
	 * @return the string
	 */
	public static String convertNonAscii(String s) {
		if (s == null)
			return null;
		// See also java.text.Normalizer
		String latin = asciiFormmater.transform(s);

		// StringBuilder sb = new StringBuilder();
		// int n = s.length();
		// for (int i = 0; i < n; i++) {
		// char c = s.charAt(i);
		// int pos = UNICODE.indexOf(c);
		// if (pos > -1) {
		// sb.append(PLAIN_ASCII.charAt(pos));
		// } else {
		// sb.append(c);
		// }
		// }
		return latin;
	}

	/**
	 * Removes the punct and empty spaces.
	 *
	 * @param s
	 *            the s
	 * @return the string
	 */
	public static String removePunct(String s) {
		return s.replaceAll("\\p{Space}", "").replaceAll("\\p{Punct}", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	/**
	 * Builds the word id.
	 *
	 * @param s
	 *            the s
	 * @return the string
	 */
	public static String buildWordId(String s) {

		if (s.length() == 0) {
			return s;
		}

		// ensure the "w_" prefix presence
		if (s.startsWith("w")) { //$NON-NLS-1$
			if (!s.startsWith("w_")) { //$NON-NLS-1$
				s = "w_" + s.substring(1); //$NON-NLS-1$
			}
		} else {
			s = "w_" + s; //$NON-NLS-1$
		}
		// System.out.println("first="+s);

		String rez = convertNonAscii(s);// .toLowerCase();
		// System.out.println("nonasscii="+rez);
		rez = rez.replaceAll("\\p{Space}++", "_"); //$NON-NLS-1$ //$NON-NLS-2$
		// System.out.println("spaces="+rez);
		rez = rez.replaceAll("[¤€§µ£°().,;:/?!@§%\\\\\"’ʹ'*+\\-}\\]\\[{#~&]", ""); //$NON-NLS-1$ //$NON-NLS-2$
																					// //
																					// "[^\\P{P}_]"
		// System.out.println("ponc="+rez);

		return rez;
	}

	/**
	 * Builds attribute id.
	 *
	 * @param s
	 *            the s
	 * @return the string
	 */
	public static String buildAttributeId(String s) {
		return buildId(s);
	}

	/**
	 * Builds the id.
	 *
	 * @param s
	 *            the s
	 * @return the string
	 */
	public static String buildId(String s) {
		// TODO: replace this with a lib managing the ID attribute format
		if (s.length() == 0) {
			return s;
		}
		String rez = s.trim();
		rez = rez.replaceAll("\\p{Space}++", "_"); //$NON-NLS-1$ //$NON-NLS-2$
		rez = rez.replaceAll("_", "-"); //$NON-NLS-1$ //$NON-NLS-2$
		rez = convertNonAscii(rez).toLowerCase();

		rez = rez.replaceAll("[¤€§µ£°().,;:/?!@§%\\\\\"’ʹ'*+\\}\\]\\[{#~&]", ""); //$NON-NLS-1$ //$NON-NLS-2$
		// remove first chars if number
		char c = rez.charAt(0);
		while (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9') {
			rez = rez.substring(1);
			if (rez.length() == 0) {
				return ""; //$NON-NLS-1$
			}
			c = rez.charAt(0);
		}

		return rez;
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String args[]) {

		String s = "01The result : - ·- _ тврьдо azertyuiopÈ,É,Ê,Ë,Û,Ù,Ï,Î,À,Â,Ô,è,é,ê,ë,û,ù,ï,î,à,â,ô,ç  0 1 2 3 4 5 6 7 8 9 10"; //$NON-NLS-1$
		System.out.println(s);
		System.out.println(AsciiUtils.convertNonAscii(s));
		System.out.println(s.replaceAll("[^\\p{ASCII}]", "")); //$NON-NLS-1$

		String s2 = "w_ТВРЬДОтврьдо_123&é\"'(-è_çà)=/*-+~#{[|`\\^@]}¤;:!§/.·?µ%£°"; //$NON-NLS-1$
		System.out.println(s2);
		System.out.println("convert ascii=" + AsciiUtils.convertNonAscii(s2)); //$NON-NLS-1$
		System.out.println("remove nonascii=" + s2.replaceAll("[^\\p{ASCII}]", "")); //$NON-NLS-1$
		System.out.println("word_id=" + AsciiUtils.buildWordId(s2)); //$NON-NLS-1$
		System.out.println("attribute_id=" + AsciiUtils.buildAttributeId(s2)); //$NON-NLS-1$
		// output :
		// The result : E,E,E,E,U,U,I,I,A,A,O,e,e,e,e,u,u,i,i,a,a,o,c

	}
}
