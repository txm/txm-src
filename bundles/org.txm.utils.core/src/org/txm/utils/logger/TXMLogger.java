// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-09-05 09:27:13 +0200 (jeu., 05 sept. 2013) $
// $LastChangedRevision: 2529 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.logger;

import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * no instanciation to do manage better Exceptions' log.
 *
 * @author mdecorde
 */
public class TXMLogger extends Logger {

	/** The caller information. */
	protected static String sourceMethodName;
	protected static String sourceClassName;

	/**
	 * Instantiates a new tXM logger.
	 *
	 * @param name
	 *            the name
	 * @param resourceBundleName
	 *            the resource bundle name
	 */
	protected TXMLogger(String name, String resourceBundleName) {
		super(name, resourceBundleName);
	}

	/**
	 * Info.
	 *
	 * @param e
	 *            the e
	 */
	public void info(Exception e) {
		this.info(exceptionToString(e));
	}

	/**
	 * Severe.
	 *
	 * @param e
	 *            the e
	 */
	public void severe(Exception e) {
		this.severe(exceptionToString(e));
	}

	/**
	 * Error.
	 *
	 * @param e
	 *            the e
	 */
	public void error(Exception e) {
		this.severe(exceptionToString(e));
	}

	/**
	 * Warning.
	 *
	 * @param e
	 *            the e
	 */
	public void warning(Exception e) {
		this.warning(exceptionToString(e));
	}

	/**
	 * Fine.
	 *
	 * @param e
	 *            the e
	 */
	public void fine(Exception e) {
		this.fine(exceptionToString(e));
	}

	/**
	 * Finer.
	 *
	 * @param e
	 *            the e
	 */
	public void finer(Exception e) {
		this.finer(exceptionToString(e));
	}

	/**
	 * Finest.
	 *
	 * @param e
	 *            the e
	 */
	public void finest(Exception e) {
		this.finest(exceptionToString(e));
	}

	/**
	 * Exception to string.
	 *
	 * @param e
	 *            the e
	 * @return the string
	 */
	public static String exceptionToString(Exception e) {
		StackTraceElement t = e.getStackTrace()[0];
		return t.getFileName() + ": " + t.getMethodName() + ", l." //$NON-NLS-1$ //$NON-NLS-2$
				+ t.getLineNumber() + " -> " + e.toString(); //$NON-NLS-1$
	}

	/**
	 * Method from Logger Create a LogRecord then calls the log method
	 * 
	 * @param level
	 * @param msg
	 */
	public void log(Level level, String msg) {
		int levelValue = this.getLevel().intValue();
		if (level.intValue() < levelValue || levelValue == Level.OFF.intValue()) {
			return;
		}
		LogRecord lr = new LogRecord(level, msg);
		inferCaller();
		lr.setSourceClassName(sourceClassName);
		lr.setSourceMethodName(sourceMethodName);
		log(lr);
	}

	/**
	 * Method from Logger to perform a log
	 * 
	 * @param record
	 */
	public void log(LogRecord record) {
		Filter theFilter = this.getFilter();
		if (theFilter != null && !theFilter.isLoggable(record)) {
			return;
		}

		// Post the LogRecord to all our Handlers, and then to
		// our parents' handlers, all the way up the tree.

		Logger logger = this;
		while (logger != null) {
			for (Handler handler : logger.getHandlers()) {
				handler.publish(record);
			}

			if (!logger.getUseParentHandlers()) {
				break;
			}

			logger = logger.getParent();
		}
	}

	/**
	 * Method from LogRecord to infer the caller's class and method names
	 */
	protected static final void inferCaller() {
		// JavaLangAccess access = SharedSecrets.getJavaLangAccess();

		Throwable throwable = new Throwable();
		StackTraceElement[] stack = throwable.getStackTrace();

		// int depth = access.getStackTraceDepth(throwable); // JAVA 7
		int depth = stack.length;

		boolean lookingForLogger = true;
		for (int ix = 0; ix < depth; ix++) {
			// Calling getStackTraceElement directly prevents the VM
			// from paying the cost of building the entire stack frame.

			// StackTraceElement frame = access.getStackTraceElement(throwable,
			// ix); //
			// JAVA7
			StackTraceElement frame = stack[ix];
			String cname = frame.getClassName();
			boolean isLoggerImpl = Log.isLoggerImplFrame(cname);
			if (lookingForLogger) {
				// Skip all frames until we have found the first logger frame.
				if (isLoggerImpl) {
					lookingForLogger = false;
				}
			} else {
				if (!isLoggerImpl) {
					// skip reflection call
					if (!cname.startsWith("java.lang.reflect.") && !cname.startsWith("sun.reflect.")) { //$NON-NLS-1$ //$NON-NLS-2$
						// We've found the relevant frame.
						sourceClassName = cname;
						sourceMethodName = frame.getMethodName();
						return;
					}
				}
			}
		}
		// We haven't found a suitable frame, so just punt. This is
		// OK as we are only committed to making a "best effort" here.
	}
}