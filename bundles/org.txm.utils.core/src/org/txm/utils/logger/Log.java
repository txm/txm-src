// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.logger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

import org.apache.commons.lang.StringUtils;
import org.txm.utils.messages.UtilsCoreMessages;

/**
 * The Class Log contains methods to print messages in the console depending on
 * a log level
 * 
 * see the severe(), warning(), info(), fine(), finer() and finest() methods
 * 
 * set the log level with Log.setLevel()
 */
public class Log {
	/** The class id. */
	public static final String id = "org.txm.utils.logger.Log"; //$NON-NLS-1$

	/** The txm Logger. */
	protected static TXMLogger txm;

	/** The fh. */
	protected static FileHandler fh;

	public static int MAX_LINE_LENGTH = 1000;

	public static int MAX_NEWLINES = 10;

	/**
	 * The CONSOLE flags indicates if messages with level inferior to INFO are
	 * printed in console
	 */
	public static boolean CONSOLE = false;

	/** The caller information. */
	protected static String sourceMethodName;
	protected static String sourceClassName;
	/** The logger frame list. */
	protected static List<String> frameList;

	static {
		// System.out.println("INIT TXMLOGGER");
		txm = new TXMLogger("TXMFileLogger", null); //$NON-NLS-1$
		txm.setLevel(Level.SEVERE); // default is to show errors
		txm.setUseParentHandlers(true);
		frameList = new ArrayList<>();
		frameList.add(id);
	}

	/**
	 * Sets the level.
	 * 
	 * @param l
	 *            the new level
	 */
	public static void setLevel(Level l) {
		txm.setLevel(l);
	}

	/**
	 * Sets the prints the in file.
	 * 
	 * @param b
	 *            the new prints the in file
	 */
	public static String setPrintInFile(boolean b) {
		// System.out.println("set print in file: "+b);
		String filename = null;
		if (b) {
			try {
				SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$

				filename = System.getProperty("java.io.tmpdir") + "/TXM-" + f.format(new Date()) + ".log"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

				fh = new FileHandler(filename, false);
				System.out.println(UtilsCoreMessages.bind(UtilsCoreMessages.Log_3P0, new File(filename).getAbsolutePath()));
				fh.setFormatter(new SimpleFormatter());
				fh.setEncoding("UTF-8"); //$NON-NLS-1$
				txm.addHandler(fh);
			} catch (SecurityException e) {
				printStackTrace(e);
			} catch (IOException e) {
				printStackTrace(e);
			}
		} else {
			txm.removeHandler(fh);
		}
		return filename;
	}

	static SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd"); //$NON-NLS-1$

	/**
	 * Sets the prints the in file.
	 * 
	 * @param b
	 *            the new prints the in file
	 */
	public static void setPrintInFile(boolean b, File logfiledir) {
		if (b) {
			if (txm.getHandlers().length == 0 && logfiledir.exists()) {
				try {

					fh = new FileHandler(logfiledir.getAbsolutePath() + "/TXM-" + f.format(new Date()) + ".log", false); //$NON-NLS-1$ //$NON-NLS-2$
					System.out.println(UtilsCoreMessages.bind(UtilsCoreMessages.Log_3P0, logfiledir.getAbsolutePath() + "TXM-" + f.format(new Date()) + ".log")); //$NON-NLS-2$ //$NON-NLS-1$
					fh.setFormatter(new SimpleFormatter());
					fh.setEncoding("UTF-8"); //$NON-NLS-1$
					txm.addHandler(fh);
				} catch (Exception e) {
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}
		} else {
			txm.removeHandler(fh);
		}
	}

	public static void addHandler(Handler handler) {
		txm.addHandler(handler);
	}

	public static void removeHandler(Handler handler) {
		txm.addHandler(handler);
	}

	/**
	 * To string.
	 * 
	 * @param e
	 *            the e
	 * @return the string
	 */
	public static String toString(Exception e) {
		try {
			StackTraceElement t = e.getStackTrace()[0];
			if (txm.getLevel().equals(Level.SEVERE) || txm.getLevel().equals(Level.ALL)) {
				StringBuffer message = new StringBuffer();

				message.append(t.getFileName() + ": " + t.getMethodName() + ", l." //$NON-NLS-1$ //$NON-NLS-2$
						+ t.getLineNumber() + " -> " + e.toString()); //$NON-NLS-1$

				if (log_stacktrace_boolean) {
					message.append(UtilsCoreMessages.Log_8);
					for (StackTraceElement se : e.getStackTrace())
						message.append("\t" + se.getFileName() + "\t" + se.getMethodName() + "\tl." + se.getLineNumber() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				}
				return message.toString();
			} else
				return t.getFileName() + ": " + t.getMethodName() + ", l." //$NON-NLS-1$ //$NON-NLS-2$
						+ t.getLineNumber() + " -> " + e.toString(); //$NON-NLS-1$
		} catch (Exception ex) {
			return e.getLocalizedMessage();
		}
	}

	/**
	 * Sets the prints the in console.
	 * 
	 * @param b
	 *            the new prints the in console
	 */
	public static void setPrintInConsole(boolean b) {
		CONSOLE = b;
	}

	/**
	 * Sets the prints the in console.
	 * 
	 */
	public static boolean getPrintInConsole() {
		return txm.getUseParentHandlers();
	}

	/**
	 * Log a severe message using the default flag
	 * 
	 * @param t
	 *            the error to print
	 */
	public static void severe(Throwable t) {
		severe(t.getLocalizedMessage());
		printStackTrace(t);
	}

	/**
	 * Log a severe message
	 * 
	 * @param message
	 */
	public static void severe(String message) {

		if (message.length() > MAX_LINE_LENGTH) { // cut if too long
			message = message.substring(0, MAX_LINE_LENGTH) + "[...]"; //$NON-NLS-1$
		}
		if (StringUtils.countMatches(message, "\n") > MAX_NEWLINES) { //$NON-NLS-1$
			message = message.replace("\n", "⏎"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		if (Level.SEVERE.intValue() >= txm.getLevel().intValue()) {
			System.out.println(message);

			StackTraceElement[] st = Thread.currentThread().getStackTrace();
			int stL = 0;
			int maxClassNameLength = 0;
			int maxMethodNameLength = 0;

			for (StackTraceElement t : st) {
				String cn = t.getClassName();
				if (cn.startsWith("org.txm.") && !cn.startsWith(Log.class.getName())) { //$NON-NLS-1$
					int cns = cn.length();
					if (cns > maxClassNameLength)
						maxClassNameLength = cns;
					int mns = t.getMethodName().length();
					if (mns > maxMethodNameLength)
						maxMethodNameLength = mns;
					stL++;
				}
			}

			int stLs = (int) Math.log10(stL) + 1;
			String format = String.format("[%%%dd]  %%%ds.%%%ds  %%s, %%s", stLs, maxClassNameLength, maxMethodNameLength); //$NON-NLS-1$

			System.out.println("Stacktrace: "); //$NON-NLS-1$
			int n = 1;
			for (StackTraceElement t : st) {
				if (t.getClassName().startsWith("org.txm.") && !t.getClassName().startsWith(Log.class.getName())) { //$NON-NLS-1$
					System.out.println(String.format(format, n++, t.getClassName(), t.getMethodName(), t.getFileName(), t.getLineNumber()));
				}
			}

			// System.out.println("Stacktrace: ");
			// int n = 1;
			// for (StackTraceElement t :
			// Thread.currentThread().getStackTrace()) {
			// if (t.getClassName().startsWith("org.txm.") &&
			// !t.getClassName().startsWith(Log.class.getName())) {
			// System.out.println(String.format("[%2d]\t",n++)+t);
			// }
			// }
		}

		txm.severe(message);
	}

	/**
	 * Log a warning message using the default flag
	 * 
	 * @param e
	 *            the error to print
	 */
	public static void warning(Throwable e) {
		warning(e.getLocalizedMessage());
		printStackTrace(e);
	}

	/**
	 * Log a warning message
	 * 
	 * @param message
	 */
	public static void warning(String message) {

		if (message.length() > MAX_LINE_LENGTH) { // cut if too long
			message = message.substring(0, MAX_LINE_LENGTH) + "[...]"; //$NON-NLS-1$
		}
		message = message.replace("\n", "⏎"); //$NON-NLS-1$ //$NON-NLS-2$

		if (Level.WARNING.intValue() >= txm.getLevel().intValue()) {
			System.out.println(message);
			// System.out.print("AT: ");
			// int n = 1;
			// for (StackTraceElement t :
			// Thread.currentThread().getStackTrace()) {
			// if (t.getClassName().startsWith("org.txm.") &&
			// !t.getClassName().startsWith(Log.class.getName())) {
			// System.out.println(t);
			// break;
			// }
			// }
		}
		txm.warning(message);
	}

	/**
	 * Log an info message using the default flag
	 * 
	 * @param t
	 *            the error to print
	 */
	public static void info(Throwable t) {
		info(t.getLocalizedMessage());
		printStackTrace(t);
	}

	/**
	 * Log an info message
	 * 
	 * @param message
	 */
	public static void info(String message) {
		info(message, true);
	}

	/**
	 * Log an info message
	 * 
	 * @param message
	 */
	public static void info(String message, boolean newline) {

		if (message.length() > MAX_LINE_LENGTH) { // cut if too long
			message = message.substring(0, MAX_LINE_LENGTH) + "[...]"; //$NON-NLS-1$
		}
		message = message.replace("\n", "⏎"); //$NON-NLS-1$ //$NON-NLS-2$

		if (Level.INFO.intValue() >= txm.getLevel().intValue()) {
			if (newline)
				System.out.println(message);
			else
				System.out.print(message);
		}
		txm.info(message);
	}

	/**
	 * Logs a finest level message.
	 * 
	 * @param message
	 */
	public static void finest(String message) {
		if (CONSOLE && Level.FINEST.intValue() >= txm.getLevel().intValue()) {
			System.out.println(message);
		}
		txm.finest(message);
	}

	/**
	 * Logs a finest level message adding the class and method of callers stack
	 * according to the specified trace depth.
	 * 
	 * @param message
	 * @param stackTraceDepth
	 */
	public static void finest(String message, int stackTraceDepth) {
		finest(message);

		// Log callers
		String lead = ""; //$NON-NLS-1$
		for (int i = 2; i < 10; i++) {
			lead += " "; //$NON-NLS-1$
			try {
				Log.finest(lead + Thread.currentThread().getStackTrace()[i].getClassName() + "." + Thread.currentThread().getStackTrace()[i].getMethodName()); //$NON-NLS-1$
			} catch (Exception e) {
				// nothing to do
			}
		}
	}

	/**
	 * Log an info message
	 * 
	 * @param message
	 */
	public static void fine(String message) {
		fine(message, true);
	}

	/**
	 * Log an info message
	 * 
	 * @param message
	 */
	public static void fine(String message, boolean newline) {
		if (CONSOLE && Level.FINE.intValue() >= txm.getLevel().intValue()) {
			if (newline)
				System.out.println(message);
			else
				System.out.print(message);
		}
		txm.fine(message);
	}

	/**
	 * Log an info message
	 * 
	 * @param message
	 */
	public static void finer(String message) {
		if (CONSOLE && Level.FINER.intValue() >= txm.getLevel().intValue()) {
			System.out.println(message);
		}
		txm.finer(message);
	}

	public static boolean log_stacktrace_boolean = true;

	/**
	 * Log a stacktrace if the 'Log.log_stacktrace_boolean' is set to true
	 * 
	 * @param e
	 *            the Throwable
	 */
	public static void printStackTrace(Throwable e) {
		if (log_stacktrace_boolean) {
			e.printStackTrace();
		}
	}

	public static Level getLevel() {
		return txm.getLevel();
	}

	public static boolean isPrintingErrors() {
		if (txm == null)
			return false;
		return txm.getLevel().intValue() <= Level.INFO.intValue();
	}

	/**
	 * Checks if the FINE level is activated.
	 * 
	 * @return
	 */
	public static boolean isLoggingFineLevel() {
		return Level.FINE.intValue() >= Log.getLevel().intValue();
	}

	/**
	 * Method from LogRecord to determine if a frame is a logger frame
	 * 
	 * @param cname
	 *            the frame
	 * @return
	 */
	protected static boolean isLoggerImplFrame(String cname) {
		for (String s : frameList) {
			if (cname.equals(s))
				return true;
		}
		return false;
	}

	public static void printStackTrace() {
		StackTraceElement[] st = Thread.currentThread().getStackTrace();
		int stL = 0;
		int maxClassNameLength = 0;
		int maxMethodNameLength = 0;

		for (StackTraceElement t : st) {
			String cn = t.getClassName();
			if (cn.startsWith("org.txm.") && !cn.startsWith(Log.class.getName())) { //$NON-NLS-1$
				int cns = cn.length();
				if (cns > maxClassNameLength)
					maxClassNameLength = cns;
				int mns = t.getMethodName().length();
				if (mns > maxMethodNameLength)
					maxMethodNameLength = mns;
				stL++;
			}
		}

		int stLs = (int) Math.log10(stL) + 1;
		String format = String.format("[%%%dd]  %%%ds.%%%ds  %%s, %%s", stLs, maxClassNameLength, maxMethodNameLength); //$NON-NLS-1$

		System.out.println("Stacktrace: "); //$NON-NLS-1$
		int n = 1;
		for (StackTraceElement t : st) {
			if (t.getClassName().startsWith(UtilsCoreMessages.Log_23) && !t.getClassName().startsWith(Log.class.getName())) {
				System.out.println(String.format(format, n++, t.getClassName(), t.getMethodName(), t.getFileName(), t.getLineNumber()));
			}
		}
	}
}
