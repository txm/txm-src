// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun., 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.logger;

/*
 import org.apache.log4j.BasicConfigurator;
 import org.apache.log4j.Level;
 import org.apache.log4j.Logger;
 */
import org.txm.utils.i18n.Localizer;

/**
 * A logger whose messages are translated unsing a {@link Localizer}.
 * 
 * @deprecated no more used
 * @author sloiseau
 */
@Deprecated
public class LocalizedLogger {
	/*
	 * private Logger logger; private Localizer localizer;
	 * 
	 * // TODO : manage elsewhere the log configuration! static {
	 * BasicConfigurator.configure();
	 * Logger.getRootLogger().setLevel(Level.INFO); }
	 * 
	 * public LocalizedLogger(Class<?> clazz) { logger =
	 * Logger.getLogger(clazz); localizer = new Localizer(clazz); }
	 * 
	 * public void info(String key, Object[] arg) {
	 * logger.info(localizer.message(key, arg)); }
	 * 
	 * public void info(String key, Object arg) {
	 * logger.info(localizer.message(key, arg)); }
	 * 
	 * public void info(String key) { logger.info(localizer.message(key)); }
	 * 
	 * public void debug(String key) { logger.debug(localizer.message(key)); }
	 * 
	 * public void debug(String key, Object[] arg) {
	 * logger.debug(localizer.message(key, arg)); }
	 * 
	 * public void debug(String key, Object arg) {
	 * logger.debug(localizer.message(key, arg)); }
	 * 
	 * public Localizer getLocalizer() { return this.localizer; }
	 * 
	 * public void error(String key) { logger.error(localizer.message(key)); }
	 * 
	 * public void error(String key, Throwable t) {
	 * logger.error(localizer.message(key),t); }
	 * 
	 * public void error(String key, Object[] arg) {
	 * logger.error(localizer.message(key, arg)); }
	 * 
	 * public void error(String key, Object[] arg, Throwable t) {
	 * logger.error(localizer.message(key, arg),t); }
	 * 
	 * public void error(String key, Object arg) {
	 * logger.error(localizer.message(key, arg)); }
	 * 
	 * public void error(String key, Object arg, Throwable t) {
	 * logger.error(localizer.message(key, arg), t); }
	 * 
	 * public void fatal(String key) { logger.fatal(localizer.message(key)); }
	 * 
	 * public void fatal(String key, Throwable t) {
	 * logger.fatal(localizer.message(key),t); }
	 * 
	 * public void fatal(String key, Object[] arg, Throwable t) {
	 * logger.fatal(localizer.message(key, arg),t); }
	 * 
	 * public void fatal(String key, Object[] arg) {
	 * logger.fatal(localizer.message(key, arg)); }
	 * 
	 * public void fatal(String key, Object arg, Throwable t) {
	 * logger.fatal(localizer.message(key, arg),t); }
	 * 
	 * public void fatal(String key, Object arg) {
	 * logger.fatal(localizer.message(key, arg)); }
	 */
}
