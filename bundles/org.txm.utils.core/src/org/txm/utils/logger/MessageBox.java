// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-07-04 16:11:30 +0200 (Mon, 04 Jul 2016) $
// $LastChangedRevision: 3257 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils.logger;

import java.util.logging.Level;

import javax.swing.SwingUtilities;

import org.txm.utils.messages.UtilsCoreMessages;

// TODO: Auto-generated Javadoc
/**
 * display alerts in a Swing popup.
 *
 * @author mdecorde
 */
public class MessageBox {

	/**
	 * Severe.
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	public static void severe(String title, String message) {
		if (Log.txm.getLevel().intValue() <= Level.SEVERE.intValue())
			show(title, message, 1000);
	}

	/**
	 * Warning.
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	public static void warning(String title, String message) {
		if (Log.txm.getLevel().intValue() < Level.WARNING.intValue())
			show(title, message, 900);
	}

	/**
	 * Info.
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	public static void info(String title, String message) {
		if (Log.txm.getLevel().intValue() <= Level.INFO.intValue())
			show(title, message, 800);
	}

	/**
	 * Fine.
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	public static void fine(String title, String message) {
		if (Log.txm.getLevel().intValue() <= Level.FINE.intValue())
			show(title, message, 700);
	}

	/**
	 * Finer.
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	public static void finer(String title, String message) {
		if (Log.txm.getLevel().intValue() <= Level.FINER.intValue())
			show(title, message, 600);
	}

	/**
	 * Finest.
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	public static void finest(String title, String message) {
		if (Log.txm.getLevel().intValue() <= Level.FINEST.intValue())
			show(title, message, 500);
	}

	/**
	 * Severe.
	 *
	 * @param message
	 *            the message
	 */
	public static void severe(String message) {
		severe(UtilsCoreMessages.MessageBox_0, message);
	}

	/**
	 * Error.
	 *
	 * @param message
	 *            the message
	 */
	public static void error(String message) {
		severe(message);
	}

	/**
	 * Error.
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 */
	public static void error(String title, String message) {
		severe(title, message);
	}

	/**
	 * Warning.
	 *
	 * @param message
	 *            the message
	 */
	public static void warning(String message) {
		warning(UtilsCoreMessages.MessageBox_1, message);
	}

	/**
	 * Info.
	 *
	 * @param message
	 *            the message
	 */
	public static void info(String message) {
		info(UtilsCoreMessages.MessageBox_2, message);
	}

	/**
	 * Fine.
	 *
	 * @param message
	 *            the message
	 */
	public static void fine(String message) {
		fine("", message); //$NON-NLS-1$
	}

	/**
	 * Finer.
	 *
	 * @param message
	 *            the message
	 */
	public static void finer(String message) {
		finer("", message); //$NON-NLS-1$
	}

	/**
	 * Finest.
	 *
	 * @param message
	 *            the message
	 */
	public static void finest(String message) {
		finest("", message); //$NON-NLS-1$
	}

	/**
	 * Displays a Swing alert pop up (in the Swing thread).
	 *
	 * @param title
	 *            the title
	 * @param message
	 *            the message
	 * @param level
	 *            the level
	 */
	public static void show(final String title, final String message, final int level) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				if (level > 900) {
					javax.swing.JOptionPane.showMessageDialog(null, message, title, javax.swing.JOptionPane.ERROR_MESSAGE);
				} else if (level > 800) {
					javax.swing.JOptionPane.showMessageDialog(null, message, title, javax.swing.JOptionPane.WARNING_MESSAGE);
				} else if (level > 700) {
					javax.swing.JOptionPane.showMessageDialog(null, message, title, javax.swing.JOptionPane.INFORMATION_MESSAGE);
				} else// if (level > 600)
				{
					javax.swing.JOptionPane.showMessageDialog(null, message, title, javax.swing.JOptionPane.CLOSED_OPTION);
				}
			}
		});

	}

}
