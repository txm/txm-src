// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Write a table in a TEI XML file. if given the sheet name should be written in the table element "id" attribute.
 * 
 * Start writing a table with writeTable(...), optionally write the header with writeHeaders(...), optionally write lines with writeLine(...), start another table with writeTable(...) and finally ends with close().
 */
public class TEITableWriter {

	String ROW = "row"; //$NON-NLS-1$
	String CELL = "cell"; //$NON-NLS-1$
	String TABLE = "table"; //$NON-NLS-1$

	String sheet = null;
	private File tableFile;

	protected XMLOutputFactory outfactory = XMLOutputFactory.newInstance();

	protected BufferedOutputStream output;

	protected XMLStreamWriter writer;
	private String[] headers;
	private boolean writingTable = false;

	public TEITableWriter(File tableFile, String sheet) throws MalformedURLException, IOException, XMLStreamException {

		this.tableFile = tableFile;
		this.sheet = sheet;

		output = new BufferedOutputStream(new FileOutputStream(tableFile), 16 * 1024);
		writer = outfactory.createXMLStreamWriter(output, "UTF-8");// create a new file //$NON-NLS-1$
		writer.writeStartElement("TEI"); //$NON-NLS-1$
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeStartElement("teiHeader"); //$NON-NLS-1$
		writer.writeEndElement(); // teiHeader
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeStartElement("text"); //$NON-NLS-1$
	}

	public File getTableFile() {
		return tableFile;
	}

	/**
	 * starts a new table
	 * 
	 * ends the previous if any 
	 * 
	 * @param id optional table name
	 * @return
	 * @throws XMLStreamException
	 */
	public boolean writeTable(String id) throws XMLStreamException {
		headers = null;

		if (writingTable) {
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement();
		}
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeStartElement("table"); //$NON-NLS-1$
		if (id != null) {
			writer.writeAttribute("id", id); //$NON-NLS-1$
		}
		writingTable = true;
		return true;
	}

	/**
	 * writes a header line
	 * @param headers
	 * @return
	 * @throws XMLStreamException
	 */
	public boolean writeHeader(String... headers) throws XMLStreamException {
		if (headers == null) {
			this.headers = headers;

			writeLine(headers);
			return true;
		}
		return false;
	}

	/**
	 * writes a line. If the header is set, the line length is tested.
	 * 
	 * @param line
	 * @return
	 * @throws XMLStreamException
	 */
	public boolean writeLine(String... line) throws XMLStreamException {
		if (headers != null) {
			if (headers.length != line.length) {
				return false;
			}
		}

		writer.writeCharacters("\n\t"); //$NON-NLS-1$
		writer.writeStartElement(ROW);

		for (String c : line) {
			writer.writeCharacters("\n\t\t"); //$NON-NLS-1$
			writer.writeStartElement(CELL);
			writer.writeCharacters(c);
			writer.writeEndElement();
		}
		writer.writeCharacters("\n\t"); //$NON-NLS-1$
		writer.writeEndElement();

		return true;
	}

	/**
	 * Ends the current table if any
	 * 
	 * Ends the writing
	 * 
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public void close() throws XMLStreamException, IOException {

		if (writingTable) {
			writer.writeCharacters("\n"); //$NON-NLS-1$
			writer.writeEndElement();
		}

		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeEndElement(); // text
		writer.writeCharacters("\n"); //$NON-NLS-1$
		writer.writeEndElement(); // TEI

		if (writer != null) writer.close();

		if (output != null) output.close();
	}


	public static void main(String[] args) {

		try {
			TEITableWriter reader = new TEITableWriter(new File("/home/mdecorde/teitable_out.xml"), null); //$NON-NLS-1$

			reader.writeTable(null);
			reader.writeHeader("col1", "col2", "col3"); //$NON-NLS-1$
			reader.writeLine("v1", "v2", "v3"); //$NON-NLS-1$
			reader.writeLine("v4", "v5", "v6"); //$NON-NLS-1$

			reader.writeTable("test"); //$NON-NLS-1$
			//			reader.writeHeader("test col1", "test col2", "test col3");
			reader.writeLine("test v1", "test v2", "test v3"); //$NON-NLS-1$
			reader.writeLine("test v4", "test v5", "test v6"); //$NON-NLS-1$
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}