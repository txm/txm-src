package org.txm.utils;

import java.util.Date;

import org.txm.utils.messages.UtilsCoreMessages;

public class ExecTimer {
	static long startTime = 0, endTime = 0;
	static String message = ""; //$NON-NLS-1$

	public static void start() {
		startTime = new Date().getTime();
	}

	public static String stop() {
		endTime = new Date().getTime();
		return setMessage(endTime - startTime);
	}

	static float t;

	private static String setMessage(float time) {
		t = time;
		if (t < 1000) {
			message = "" + ((int) t) + UtilsCoreMessages.ExecTimer_0; //$NON-NLS-1$
		} else {
			t = t / 1000;
			if (t < 3) {
				message = "" + t; //$NON-NLS-1$
				message = "" + (message.substring(0, 3)) + UtilsCoreMessages.ExecTimer_1; //$NON-NLS-1$
			} else if (t < 60)
				message = "" + ((int) t) + UtilsCoreMessages.ExecTimer_1; //$NON-NLS-1$
			else if (t < 3600)
				message = "" + ((int) t / 60) + UtilsCoreMessages.ExecTimer_2 + ((int) t % 60) + UtilsCoreMessages.ExecTimer_1; //$NON-NLS-1$
			else
				message = "" + ((int) t / 3600) + UtilsCoreMessages.ExecTimer_3 + ((int) (t % 3600) / 60) + UtilsCoreMessages.ExecTimer_2 + ((int) (t % 3600) % 60) + UtilsCoreMessages.ExecTimer_1; //$NON-NLS-1$
		}
		return message + " (" + (int) time + " ms)"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static String getMessage() {
		return message;
	}
}
