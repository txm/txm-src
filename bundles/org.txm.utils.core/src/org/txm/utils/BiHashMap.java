package org.txm.utils;

import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Bi directionnal hashmap
 * 
 * @author mdecorde
 */
public class BiHashMap<KeyType, ValueType> {

	private ConcurrentHashMap<KeyType, ValueType> keyToValueMap = new ConcurrentHashMap<KeyType, ValueType>();
	private ConcurrentHashMap<ValueType, KeyType> valueToKeyMap = new ConcurrentHashMap<ValueType, KeyType>();

	public BiHashMap() {

	}

	public BiHashMap(Properties props) {
		for (Object k : props.keySet()) {
			put((KeyType) k, (ValueType) props.get(k));
		}
	}

	synchronized public void put(KeyType key, ValueType value) {
		keyToValueMap.put(key, value);
		// FIXME clash because values are not unique
		valueToKeyMap.put(value, key);
	}

	synchronized public ValueType removeByKey(KeyType key) {
		ValueType removedValue = keyToValueMap.remove(key);
		valueToKeyMap.remove(removedValue);
		return removedValue;
	}

	synchronized public KeyType removeByValue(ValueType value) {
		KeyType removedKey = valueToKeyMap.remove(value);
		keyToValueMap.remove(removedKey);
		return removedKey;
	}

	public boolean containsKey(KeyType key) {
		return keyToValueMap.containsKey(key);
	}

	public boolean containsValue(ValueType value) {
		return keyToValueMap.containsValue(value);
	}

	public KeyType getKey(ValueType value) {
		return valueToKeyMap.get(value);
	}

	public ValueType get(KeyType key) {
		return keyToValueMap.get(key);
	}

	public Set<KeyType> getKeys() {
		return keyToValueMap.keySet();
	}

	public Set<ValueType> getValues() {
		return valueToKeyMap.keySet();
	}

	public String toString() {
		return keyToValueMap.toString();
	}
}