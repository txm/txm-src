// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2014-09-15 10:51:15 +0200 (Mon, 15 Sep 2014) $
// $LastChangedRevision: 2836 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.txm.utils.logger.Log;

/**
 * The StreamHog read the given InputStream in a Thread until the InputStream is
 * closed.
 * 
 * If a MessagePrinter is set, then the messages are displayed with the
 * MessagePrinter and not in sys.out
 */
public class StreamHog extends Thread {

	InputStream is;
	BufferedReader br;
	ArrayList<MessagePrinter> messagePrinters = new ArrayList<>();

	public String lastline;
	boolean log;

	/**
	 * Instantiates a new stream hog.
	 *
	 * @param is
	 *            the is
	 * @param log
	 *            the capture
	 */
	public StreamHog(InputStream is, boolean log) {
		this.setName(""); //$NON-NLS-1$
		this.is = is;
		this.log = log;
		start();
	}

	public String getLastMessage() {
		return lastline;
	}

	public InputStream getInputStream() {
		return is;
	}

	public BufferedReader getBufferedReader() {
		return br;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		try {
			br = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (log) { // we are supposed to capture the output from
					Log.info(this.getName() + ">" + line); //$NON-NLS-1$
				}

				printMessage(line);
				lastline = line;
			}
		} catch (Throwable e) {
			Log.fine("ERROR: broken process logging : " + e); //$NON-NLS-1$
			// org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * print the message to any registered MessagePrinter
	 * 
	 * @param msg
	 */
	public void printMessage(String msg) {
		if (messagePrinters.size() > 0) {
			for (MessagePrinter mp : messagePrinters) {
				mp.println(msg);
			}
		}
	}

	/**
	 * Use this method to retrieve messages
	 * 
	 * @param mp
	 */
	public void registerMessagePrinter(MessagePrinter mp) {
		this.messagePrinters.add(mp);
	}

	public ArrayList<MessagePrinter> getMessagePrinter() {
		return this.messagePrinters;
	}

	/**
	 * Sets the prints the.
	 *
	 * @param log
	 *            activate message logs if true
	 */
	public void setPrint(boolean log) {
		this.log = log;
	}
}
