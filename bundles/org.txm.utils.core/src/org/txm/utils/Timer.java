package org.txm.utils;

public class Timer {
	long start = System.currentTimeMillis();

	public long ellaspeTime() {
		return System.currentTimeMillis() - start;
	}

	public void reset() {
		start = System.currentTimeMillis();
	}

	public long getStart() {
		return start;
	}
}
