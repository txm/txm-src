package org.txm.utils;

import java.util.HashSet;

public class Tuple<T> {

	public T a, b;

	public Tuple(T a, T b) {
		this.a = a;
		this.b = b;
	}

	static String format = "%s %s"; //$NON-NLS-1$

	public String toString() {
		return String.format(format, a, b);
	}

	@Override
	public int hashCode() {
		return a.hashCode() * b.hashCode();
	}

	@Override
	public boolean equals(Object t) {
		if (t instanceof Tuple)
			return equals((Tuple<?>) t);
		else
			return super.equals(t);
	};

	public boolean equals(Tuple<Object> t) {
		return a.equals(t.a) & b.equals(t.b);
	};

	public static void main(String[] args) {
		Tuple<Integer> t1 = new Tuple<Integer>(1, 1);
		Tuple<Integer> t2 = new Tuple<Integer>(1, 2);
		Tuple<Integer> t3 = new Tuple<Integer>(1, 3);
		Tuple<Integer> t33 = new Tuple<Integer>(1, 3);

		HashSet<Tuple<Integer>> set = new HashSet<Tuple<Integer>>();
		set.add(t1);
		set.add(t2);
		set.add(t3);
		set.add(t33);

		System.out.println(set);
	}
}