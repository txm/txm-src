package org.txm.utils.network;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.eclipse.core.net.proxy.IProxyService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;
import org.txm.utils.logger.Log;

/**
 * This class implements various manner to retrieve the system proxy
 * configuration. It tries to get the configuration from the RCP store and from
 * the Java system properties.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SystemProxyDetector {

	public String proxy_user = ""; //$NON-NLS-N$
	public String proxy_password = ""; //$NON-NLS-N$
	public String proxy_host = ""; //$NON-NLS-N$
	public int proxy_port = 0;

	/**
	 * Creates a system proxy detector and tries to retrieve the system proxy
	 * configuration from the RCP store and from the Java system properties.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" }) //$NON-NLS-N$
	public SystemProxyDetector() {
		ServiceTracker proxyTracker = null;
		try {
			Bundle c = FrameworkUtil.getBundle(this.getClass());
			if (c == null)
				return; // abord;
			BundleContext bc = c.getBundleContext();
			if (bc == null)
				return; // abord;
			proxyTracker = new ServiceTracker(bc, IProxyService.class.getName(), null);
			proxyTracker.open();

			IProxyService proxyService = (IProxyService) proxyTracker.getService();
			if (proxyService.hasSystemProxies()) { // Always response false for
													// Mac OS X, because there
													// is no Eclipse plugin that
													// manages the system
													// proxies
				try {
					List<Proxy> proxies = ProxySelector.getDefault().select(new URI("http://textometry.org")); //$NON-NLS-N$
					if (proxies != null) {
						for (Proxy proxy : proxies) {
							if (proxy.type().equals(Proxy.Type.HTTP)) {
								InetSocketAddress addr = (InetSocketAddress) proxy.address();
								if (addr != null) {
									proxy_host = addr.getHostName();
									proxy_port = addr.getPort();
									break; // ok done
								}
							}
						}
					}
				} catch (URISyntaxException e) {
					Log.printStackTrace(e);
				}
			}

			// last resort to find out proxy configuration
			if (proxy_host == null || proxy_host.length() == 0) {
				proxy_host = System.getProperty("http.proxyHost"); //$NON-NLS-N$
				String s = System.getProperty("http.proxyPort"); //$NON-NLS-N$
				if (s != null)
					proxy_port = Integer.parseInt(s);
				proxy_password = System.getProperty("http.proxyPassword"); //$NON-NLS-N$
				proxy_user = System.getProperty("http.proxyUser"); //$NON-NLS-N$
			}

			// Log
			if (proxy_host != null && !proxy_host.isEmpty()) {
				Log.info("System proxy network configuration detected."); //$NON-NLS-N$
			}

			// TODO: useless ?
			// if (proxyTracker == null) {
			// IProxyData[] data = proxyService.getProxyData();
			// for (IProxyData d : data) {
			// proxy_host = d.getHost();
			// if (IProxyData.HTTP_PROXY_TYPE.equals(d.getType()) && proxy_host
			// != null &&
			// proxy_host.length() > 0) {
			// proxy_port = d.getPort();
			// proxy_password = d.getPassword();
			// proxy_user = d.getUserId();
			// //System.out.println("proxy_server="+proxy_server+"
			// proxy_port="+proxy_port);
			//
			// break; // ok done
			// } else {
			// proxy_host = null;
			// }
			// }
			// }
		} catch (Exception e) {
			Log.printStackTrace(e);
		} finally {
			if (proxyTracker != null)
				proxyTracker.close();
		}
	}

	/**
	 * Checks if a system proxy has been detected.
	 * 
	 * @return
	 */
	public boolean isSystemProxyDetected() {
		return proxy_host != null && !proxy_host.isEmpty();
	}

	/**
	 * Returns the HTTP system proxy configuration as URL if exists otherwise
	 * false.
	 * 
	 * @return
	 */
	public String getHttpProxyUrl() {

		String proxyUrl = null;
		String userData = "";

		if (this.isSystemProxyDetected()) {
			proxyUrl = proxy_host + ":" + proxy_port; //$NON-NLS-N$

			if (proxy_user != null && !proxy_user.isEmpty()) {
				userData = proxy_user;
				proxyUrl = "@" + proxyUrl; //$NON-NLS-1$
			}
			if (proxy_password != null && !proxy_password.isEmpty()) {
				userData += ":" + proxy_password; //$NON-NLS-1$
			}
		} else {
			Log.warning("HTTP system proxy configuration is not set."); //$NON-NLS-N$
		}

		if (proxyUrl != null) {
			proxyUrl = "http://" + userData + proxyUrl; //$NON-NLS-1$ //$NON-NLS-N$
		}

		return proxyUrl;
	}

	@Override
	public String toString() {
		return getHttpProxyUrl();
	}
}
