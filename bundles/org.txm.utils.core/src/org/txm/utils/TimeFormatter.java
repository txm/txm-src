package org.txm.utils;

/**
 * Format time durations to human strings
 * 
 * @author mdecorde
 *
 */
public class TimeFormatter {
	
	public static String formatTime(String time) {
		try {
			return formatTime(Float.parseFloat(time));
		} catch (Exception e) {
			try {
				return formatTime(Integer.parseInt(time));
			} catch (Exception e2) {
				return formatTime(0.0f);
			}
		}
	}

	/**
	 * 
	 * @param secondes
	 *            number of seconde
	 * @return
	 */
	public static String formatTime(int secondes) {
		String rez = " "; //$NON-NLS-1$
		float h = secondes / 3600;
		secondes = secondes % 3600;
		float min = (secondes % 3600) / 60;
		int sec = secondes % 60;
		if (min < 10)
			rez = "" + (int) h + ":0" + (int) min;// +":"+time%60; //$NON-NLS-1$
		else
			rez = "" + (int) h + ":" + (int) min;// +":"+time%60; //$NON-NLS-1$

		if (sec >= 10)
			rez += ":" + sec; //$NON-NLS-1$
		else
			rez += ":0" + sec; //$NON-NLS-1$

		return rez;
	}

	/**
	 * 
	 * @param time 0.00s
	 * @return
	 */
	public static String formatTime(float time) {
		String rez = " "; //$NON-NLS-1$
		float h = time / 3600;
		time = time % 3600;
		float min = (time % 3600) / 60;
		int sec = (int) time % 60;
		if (min < 10)
			rez = "" + (int) h + ":0" + (int) min;// +":"+time%60; //$NON-NLS-1$
		else
			rez = "" + (int) h + ":" + (int) min;// +":"+time%60; //$NON-NLS-1$

		if (sec >= 10)
			rez += ":" + sec; //$NON-NLS-1$
		else
			rez += ":0" + sec; //$NON-NLS-1$

		return rez;
	}

	/**
	 * 
	 * @param time
	 *            0.00s
	 * @return
	 */
	public static String formatTime(double time) {
		String rez = " "; //$NON-NLS-1$
		double h = time / 3600;
		time = time % 3600;
		double min = (time % 3600) / 60;
		int sec = (int) time % 60;
		if (min < 10)
			rez = "" + (int) h + ":0" + (int) min;// +":"+time%60; //$NON-NLS-1$
		else
			rez = "" + (int) h + ":" + (int) min;// +":"+time%60; //$NON-NLS-1$

		if (sec >= 10)
			rez += ":" + sec; //$NON-NLS-1$
		else
			rez += ":0" + sec; //$NON-NLS-1$

		return rez;
	}
}
