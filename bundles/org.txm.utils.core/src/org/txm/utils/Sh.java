// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2011-05-09 14:58:28 +0200 (lun., 09 mai 2011) $
// $LastChangedRevision: 1839 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.txm.utils.logger.Log;
import org.txm.utils.messages.UtilsCoreMessages;

// Shell
/**
 * The Class Sh.
 */
public class Sh {

	/** The binpath. */
	String binpath = ""; //$NON-NLS-1$

	/** The version. */
	String version = "0.0.0"; //$NON-NLS-1$

	/** The desc. */
	String desc = "Shell"; //$NON-NLS-1$

	/**
	 * Instantiates a new sh.
	 *
	 * @param binpath
	 *            the binpath
	 */
	public Sh(String binpath) {
		// System.out.println("set binpath "+binpath);
		this.binpath = binpath;
	}

	// execute command line in argument
	/** The isc. */
	private Boolean isc = false;

	/** The c. */
	private String c;

	/**
	 * Sets the isc.
	 *
	 * @param arg
	 *            the new isc
	 */
	public void setIsc(String arg) {
		this.isc = true;
		this.c = arg;
	}

	/**
	 * Unset isc.
	 */
	public void unsetIsc() {
		this.isc = false;
	}

	// Sh
	/**
	 * Shexe.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void shexe() throws IOException {
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "sh.exe"); //$NON-NLS-1$
		if (isc)
			args.add("-c " + c); //$NON-NLS-1$
		System.out.println(args);
		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e1);
		}
		if (e != 0) {
			Log.fine(UtilsCoreMessages.bind(UtilsCoreMessages.Sh_4P0AtP1, e, DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date())));
		}
	}

	// Sh
	/**
	 * Sh.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void sh() throws IOException {
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "sh"); //$NON-NLS-1$
		if (isc) {
			args.add("-c"); //$NON-NLS-1$
			args.add(c);
		}
		// System.out.println(args);
		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			Log.severe(Log.toString(e));
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			Log.info(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e1);
		}
		if (e != 0) {
			Log.fine(UtilsCoreMessages.bind(UtilsCoreMessages.Sh_4P0AtP1, e, DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date())));
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		// Sh tt = new Sh("");
		// tt.setIsc("kill `ps aux | grep cqpserver | awk '/-P 4877/ {print
		// \\$2}'`");//
		// try {
		// tt.sh();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// org.txm.utils.logger.Log.printStackTrace(e);
		// }
	}
}
