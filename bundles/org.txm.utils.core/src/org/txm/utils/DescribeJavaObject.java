package org.txm.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Print Java infos (constructors, methods, members) about any object
 * 
 * @author mdecorde
 *
 */
public class DescribeJavaObject {

	public static void main(String[] args) {

		String showPublic = "public"; //$NON-NLS-1$
		String returnTypeFilter = null;
		String parameterTypeFilter = null;
		String nameFilter = ".*Parameter.*"; //$NON-NLS-1$

		Class<?> clazz = Object.class;
		print(clazz, showPublic, returnTypeFilter, parameterTypeFilter, nameFilter);
	}

	public static void print(Class<?> clazz) {
		print(clazz, null, null, null, null);
	}

	public static void print(Class<?> clazz, String showPublic, String returnTypeFilter, String parameterTypeFilter, String nameFilter) {

		System.out.println("\nConstructors: "); //$NON-NLS-1$
		for (Constructor<?> c : clazz.getDeclaredConstructors()) {
			boolean display = true;
			String s = c.toString();

			display &= showPublic == null || s.substring(0, s.indexOf(" ")).matches(showPublic); //$NON-NLS-1$
			// display &= returnType == null ||
			// c.getParameterTypes().toString().matches(parameterType);
			display &= parameterTypeFilter == null || Arrays.toString(c.getParameterTypes()).matches(parameterTypeFilter);
			display &= nameFilter == null || c.getName().matches(nameFilter);
			if (display)
				System.out.println("\t" + c); //$NON-NLS-1$
		}

		System.out.println("\nFields: "); //$NON-NLS-1$
		for (Field c : clazz.getDeclaredFields()) {
			boolean display = true;
			String s = c.toString();

			display &= showPublic == null || s.substring(0, s.indexOf(" ")).matches(showPublic); //$NON-NLS-1$
			display &= returnTypeFilter == null || c.getType().toString().matches(parameterTypeFilter);
			// display &= parameterType == null ||
			// c.getParameterTypes().toString().matches(parameterType);
			display &= nameFilter == null || c.getName().matches(nameFilter);
			if (display)
				System.out.println("\t" + c); //$NON-NLS-1$
		}

		System.out.println("\nMethods: "); //$NON-NLS-1$
		for (Method c : clazz.getDeclaredMethods()) {
			boolean display = true;
			String s = c.toString();

			display &= showPublic == null || s.substring(0, s.indexOf(" ")).matches(showPublic); //$NON-NLS-1$
			display &= returnTypeFilter == null || c.getReturnType().toString().matches(returnTypeFilter);
			display &= parameterTypeFilter == null || Arrays.toString(c.getParameterTypes()).matches(parameterTypeFilter);
			display &= nameFilter == null || c.getName().matches(nameFilter);
			if (display)
				System.out.println("\t" + c); //$NON-NLS-1$
		}

	}
}
