// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Read a table from a TEI XML file. if given the sheet name should be the table element id. If no "sheet" is given, the first table is used.
 */
public class TEITableReader extends AbstractTableReader {

	String ROW = "row"; //$NON-NLS-1$
	String CELL = "cell"; //$NON-NLS-1$
	String TABLE = "table"; //$NON-NLS-1$
	
	String sheet = null;
	private File tableFile;

	protected InputStream inputData;

	protected XMLInputFactory factory;

	protected XMLStreamReader parser;
	private boolean ready = false;
	private String[] headers;
	private LinkedHashMap<String, String> record;

	public TEITableReader(File tableFile, String sheet) throws MalformedURLException, IOException, XMLStreamException {

		this.tableFile = tableFile;
		this.sheet = sheet;

		this.inputData = tableFile.toURI().toURL().openStream();
		this.factory = XMLInputFactory.newInstance();
		this.parser = factory.createXMLStreamReader(inputData);

		for (int event = parser.getEventType(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {

			// preparing process
			if (event == XMLStreamConstants.START_ELEMENT) {
				String id = "the id is not set"; //$NON-NLS-1$
				if (sheet != null) {
					for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
						if (parser.getAttributeLocalName(i).equals("id")) { //$NON-NLS-1$
							id = parser.getAttributeValue(i);
							break;
						}
					}
				}
				
				String localname = parser.getLocalName();
				if (sheet == null && localname.equals(TABLE)) { //$NON-NLS-1$
					ready = true;
					break;
				} else if (localname.equals(TABLE) && id.equals(sheet)) { //$NON-NLS-1$
					ready = true;
					break;
				}
			}
		}
	}
	
	public File getTableFile() {
		return tableFile;
	}

	@Override
	public boolean readHeaders() throws IOException {
		if (!ready) return false;
		
		headers = readTEIRow();
		if (headers == null) return false;
		
		return false;
	}

	@Override
	public boolean readRecord() throws IOException {
		if (!ready) return false;
		
		String[] cells = readTEIRow();
		record = null; // clear previous record
		if (cells == null) return false;
		
		record = new LinkedHashMap<>();
		
		if (headers == null) {
			for (int i = 0 ; i < cells.length ; i++) {
				record.put(Integer.toString(i + 1), cells[i]);
			}
		} else {
			for (int i = 0 ; i < headers.length ; i++) {
				if (cells.length < i + 1) {
					record.put(headers[i], EMPTY);
				} else {
					record.put(headers[i], cells[i]);
				}
			}
		}
		
		return true;
	}

	@Override
	public LinkedHashMap<String, String> getRecord() {
		if (!ready) return null;
		return record;
	}

	@Override
	public String[] getHeaders() {
		if (!ready) return null;
		
		return headers;
	}
	
	/**
	 * read until the end of the next 'row' element
	 * @return
	 * @throws XMLStreamException
	 */
	private String[] readTEIRow() throws IOException {
		ArrayList<String> list = new ArrayList<>();
		
		boolean inRow = false;
		boolean inCell = false;
		StringBuilder cellValue = null;
		
		try {
			for (int event = parser.getEventType(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {

				if (event == XMLStreamConstants.START_ELEMENT) {
					//System.out.println("start: "+parser.getLocalName());
					if (parser.getLocalName().equals(ROW)) {
						inRow = true;
					} else if (parser.getLocalName().equals(CELL) && inRow) {
						inCell = true;
						cellValue = new StringBuilder();
					}
				} else if (event == XMLStreamConstants.END_ELEMENT) {
					//System.out.println("end: "+parser.getLocalName());
					if (parser.getLocalName().equals(ROW)) {
						inRow = false;
						parser.next();
						return list.toArray(new String[list.size()]);
					} else if (parser.getLocalName().equals(CELL) && inRow) {
						list.add(cellValue.toString());
						inCell = false;
					} else if (parser.getLocalName().equals(TABLE)) {
						return null;
					}
				} else if (event == XMLStreamConstants.CHARACTERS) {
					
					if (inCell && inRow) {
						cellValue.append(parser.getText());
					}
				}
			}
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	String EMPTY = ""; //$NON-NLS-1$
	@Override
	public String get(String h) throws IOException {
		if (!ready) return null;
		String s = getRecord().get(h);
		if (s == null)
			return EMPTY;

		return s;
	}

	@Override
	public void close() throws IOException {
		if (parser != null) {
			try {
				parser.close();
			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		try {
			System.out.println("Loading..."); //$NON-NLS-1$
			TEITableReader reader = new TEITableReader(new File("teitable.xml"), null); //$NON-NLS-1$
			System.out.println("Table loaded"); //$NON-NLS-1$

			System.out.println("Reading headers..."); //$NON-NLS-1$
			reader.readHeaders();
			System.out.println(Arrays.toString(reader.getHeaders()));

			while (reader.readRecord()) {
				System.out.println(reader.getRecord());
			}
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}