// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu., 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The Class ProcessBuilderBuilder.
 *
 * @deprecated
 * 
 *             Reads a shell command line interface definition in an xml file
 *             (CLIX) then creates the corresponding java wrapper API code.
 * 
 *             example xml definition file : {@code
 *             <application name="progname" version="0.0.0" desc="..."> <progs>
 *             <prog exec="prog.exe" desc="..."> <args>
 *             <arg state="optional" type="none" name="thing" desc="the thing
 *             arg"/>
 *             <arg state="must" type="none" name="compulsorything" desc=""/>
 *             <arg state="multiple" type="none" name="onethingatleast" desc=
 *             ""/> </args> </prog> </progs> </application>
 *             }
 * @author mdecorde, sheiden
 */
@Deprecated
public class ProcessBuilderBuilder {

	/**
	 * build the java file from the definition file.
	 *
	 * @param xmlFile
	 *            the xml file
	 * @param outFile
	 *            the out file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws SAXException
	 *             the sAX exception
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 */
	public static void build(File xmlFile, File outFile) throws IOException, SAXException, ParserConfigurationException {
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"); //$NON-NLS-1$
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(xmlFile);
		Element root = doc.getDocumentElement();
		String appliname = root.getAttribute("name"); //$NON-NLS-1$
		String version = root.getAttribute("version"); //$NON-NLS-1$
		String applidesc = root.getAttribute("desc"); //$NON-NLS-1$
		writer.write("import java.util.Date;\n"); //$NON-NLS-1$
		writer.write("import java.util.Locale;\n"); //$NON-NLS-1$
		writer.write("import java.text.DateFormat;\n"); //$NON-NLS-1$
		writer.write("import java.io.BufferedReader;\n"); //$NON-NLS-1$
		writer.write("import java.io.IOException;\n"); //$NON-NLS-1$
		writer.write("import java.io.InputStream;\n"); //$NON-NLS-1$
		writer.write("import java.io.InputStreamReader;\n"); //$NON-NLS-1$
		writer.write("import java.util.ArrayList;\n"); //$NON-NLS-1$

		writer.write("\n// " + applidesc + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("class " + appliname + "\n{\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\tString binpath = \"\";\n"); //$NON-NLS-1$
		writer.write("\tpublic " + appliname + "(String bindir){this.binpath=bindir;}\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\tpublic " + appliname + "(File bindir){this.binpath=bindir.getAbsolutePath();}\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\tString version = \"" + version + "\";\n"); //$NON-NLS-1$ //$NON-NLS-2$
		writer.write("\tString desc = \"" + applidesc + "\";\n"); //$NON-NLS-1$ //$NON-NLS-2$

		writer.write("\tboolean debug = false;\n"); //$NON-NLS-1$
		writer.write("\tpublic void debug(boolean b){debug=b;};\n"); //$NON-NLS-1$

		NodeList nodeLst = doc.getElementsByTagName("prog"); //$NON-NLS-1$
		ArrayList<String> dejavu = new ArrayList<String>();

		for (int s = 0; s < nodeLst.getLength(); s++) {
			HashMap<String, String> atester = new HashMap<String, String>();
			ArrayList<String> order = new ArrayList<String>();
			ArrayList<String> musts = new ArrayList<String>();
			ArrayList<String> multiples = new ArrayList<String>();
			Element elem = (Element) nodeLst.item(s);
			NodeList children = elem.getElementsByTagName("arg"); //$NON-NLS-1$

			String execfile = elem.getAttribute("exec"); //$NON-NLS-1$
			String execfiledesc = elem.getAttribute("desc"); //$NON-NLS-1$
			String func = "\n\t// " + execfiledesc + //$NON-NLS-1$
					"\n\tpublic void " //$NON-NLS-1$
					+ execfile.replace("-", "").replace(".", "") + "("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			String argsdesc = ""; //$NON-NLS-1$
			for (int i = 0; i < children.getLength(); i++) {
				Element child = (Element) children.item(i);
				String name = child.getAttribute("name"); //$NON-NLS-1$
				String type = child.getAttribute("type"); //$NON-NLS-1$
				String desc = child.getAttribute("desc"); //$NON-NLS-1$
				String state = child.getAttribute("state"); //$NON-NLS-1$
				if (!order.contains(name))
					order.add(name);
				if (state.equals("optional")) //$NON-NLS-1$
				{
					if (!dejavu.contains(name)) {
						dejavu.add(name);
						writer.write("\n\t// " + desc + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
						writer.write("\tprivate Boolean is" + name.replace("-", "") + " = false;\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

						if (!type.equals("none")) //$NON-NLS-1$
						{
							writer.write("\tprivate " + type + " " + name.replace("-", "") + ";\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
							writer.write("\tpublic void set" + name.replace("-", "") + "(" + type + " arg)\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
									"\tthis.is" //$NON-NLS-1$
									+ name.replace("-", "") + "=true;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									"\t\tthis." //$NON-NLS-1$
									+ name.replace("-", "") + "=arg;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									"\t}\n" //$NON-NLS-1$
							);
						} else {
							writer.write("\tpublic void set" + name.replace("-", "") + "()\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
									"\tthis.is" //$NON-NLS-1$
									+ name.replace("-", "") + "=true;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									"\t}\n" //$NON-NLS-1$
							);
						}
						writer.write("\tpublic void unset" + name.replace("-", "") + "()\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
								"\tthis.is" //$NON-NLS-1$
								+ name.replace("-", "") + "=false;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								"\t}\n" //$NON-NLS-1$
						);
					}

					atester.put(name, type);
				} else if (state.equals("multiple")) //$NON-NLS-1$
				{
					if (!dejavu.contains(name)) {
						dejavu.add(name);
						writer.write("\n\t// " + desc + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
						writer.write("\tprivate Boolean is" + name.replace("-", "") + " = false;\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

						if (!type.equals("none")) //$NON-NLS-1$
						{
							writer.write("\tprivate List<" + type + "> " + name + ";\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							writer.write("\tpublic void set" + name.replace("-", "") + "(List<" + type + "> arg)\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
									"\tthis.is" //$NON-NLS-1$
									+ name.replace("-", "") + "=true;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									"\t\tthis." //$NON-NLS-1$
									+ name.replace("-", "") + "=arg;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									"\t}\n" //$NON-NLS-1$
							);
						} else {
							writer.write("\tpublic void set" + name.replace("-", "") + "()\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
									"\tthis.is" //$NON-NLS-1$
									+ name.replace("-", "") + "=true;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									"\t}\n" //$NON-NLS-1$
							);
						}
						writer.write("\tpublic void unset" + name.replace("-", "") + "()\n\t{" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
								"\tthis.is" //$NON-NLS-1$
								+ name.replace("-", "") + "=false;\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								"\t}\n" //$NON-NLS-1$
						);
					}
					multiples.add(name);
					atester.put(name, type);
				} else {
					func = func + type + " " + name.replace("-", "") + ", "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					argsdesc += "\t// arg : " + desc + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
					musts.add(name);

					if (!dejavu.contains(name))
						dejavu.add(name);
				}
			}
			if (musts.size() > 0)
				func = func.substring(0, func.length() - 2) + ")"; //$NON-NLS-1$
			else
				func = func + ")"; //$NON-NLS-1$

			writer.write("\n" + func + " throws IOException\n"); //$NON-NLS-1$ //$NON-NLS-2$
			writer.write(argsdesc);
			writer.write("\t{\n"); //$NON-NLS-1$
			writer.write("\t\tArrayList<String> args = new ArrayList<String>();\n"); //$NON-NLS-1$
			writer.write("\t\targs.add(binpath+\"/" + execfile + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$
			for (String name : order) {
				if (atester.containsKey(name)) {
					writer.write("\t\tif (is" + name.replace("-", "") + ")\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					if (!atester.get(name).equals("none")) //$NON-NLS-1$
					{
						if (multiples.contains(name)) {
							writer.write("\t\t\tfor(int c = 0;c < " + name.replace("-", "") + ".size();c++)\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
							writer.write("\t\t\t{args.add(\"-" + name + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$
							writer.write("\t\t\targs.add(\"\"+" + name.replace("-", "") + ".get(c));}\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						} else {
							writer.write("\t\t\t{args.add(\"-" + name + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$
							writer.write("\t\t\targs.add(\"\"+" + name.replace("-", "") + ");}\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						}
					} else {
						writer.write("\t\t\targs.add(\"-" + name + "\");\n"); //$NON-NLS-1$ //$NON-NLS-2$
					}
				} else {
					if (musts.contains(name))
						writer.write("\t\targs.add(\"\"+" + name.replace("-", "") + ");\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				}
			}

			writer.write("\n\t\tProcessBuilder pb = new ProcessBuilder(args);\n" + //$NON-NLS-1$
					"\t\tpb.redirectErrorStream(true);\n" + //$NON-NLS-1$
					"\t\tProcess process = null;\n" + //$NON-NLS-1$
					"\t\ttry {\n\t\t\tprocess = pb.start();\n\t\t} catch (IOException e) {\n\t\t\tSystem.err.println(e);\n\t\t}\n"); //$NON-NLS-1$
			writer.write("\t\tInputStream is = process.getInputStream();\n"); //$NON-NLS-1$
			writer.write("\t\tInputStreamReader isr = new InputStreamReader(is);\n"); //$NON-NLS-1$
			writer.write("\t\tBufferedReader br = new BufferedReader(isr);\n"); //$NON-NLS-1$
			writer.write("\t\tString line;\n"); //$NON-NLS-1$
			writer.write("\t\twhile ((line = br.readLine()) != null)\n\t\t{\n\t\t\tSystem.out.println(line);\n\t\t}\n"); //$NON-NLS-1$
			writer.write("\t\tint e=0;\n\ttry {e = process.waitFor();}catch(Exception err){}\n\t\tif (e != 0) {\n\t\t\t" + //$NON-NLS-1$
					"System.err.println(\"Process exited abnormally with code \"+e+\" at \"+" //$NON-NLS-1$
					+ "DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date()));\n\t\t\n"); //$NON-NLS-1$
			writer.write("\t\n\n"); //$NON-NLS-1$
			writer.write("for(int c=0;c <args.size();c++)System.out.print(\"\"+args.get(c)+\" \");\n"); //$NON-NLS-1$
			writer.write("System.out.println();\n}\n\t}\n"); //$NON-NLS-1$
		}

		writer.write("\tpublic static void main(String[] args) \n"); //$NON-NLS-1$
		writer.write("\t{\n"); //$NON-NLS-1$
		writer.write("\t\t" + appliname + " tt = new " + appliname + "(\"\");\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		writer.write("\t}\n"); //$NON-NLS-1$

		writer.write("}"); //$NON-NLS-1$
		writer.close();
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Need 2 args : xmlfile & outfile paths"); //$NON-NLS-1$
			return;
		}
		File xmlfile = new File(args[0]);
		File outfile = new File(args[1]);

		if (xmlfile.exists())
			System.out.println("xmlfile ok"); //$NON-NLS-1$
		try {
			ProcessBuilderBuilder.build(xmlfile, outfile);
		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		System.out.println("done"); //$NON-NLS-1$
	}
}
