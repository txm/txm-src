package org.txm.properties.core.functions;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.txm.properties.core.messages.PropertiesCoreMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Part;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.TXMProgressMonitor;

public class PartitionPropertiesComputer extends PropertiesComputer<Partition> {

	public PartitionPropertiesComputer(Properties props, Partition result) {
		super(props, result);
	}

	@Override
	public LinkedHashSet<String> getComputableInformations() {
		return new LinkedHashSet<>(Arrays.asList(""));
	}

	@Override
	public String getInformation(String name) {
		LinkedHashMap<String, String> data = new LinkedHashMap<>();
		data.put("", dump());
		return data.get(name);
	}

	@Override
	public String getInformationHTML(String info) {
		return htmlDump();
	}

	@Override
	public String getName() {
		return result.getName();
	}

	/**
	 * dump the result in the console.
	 *
	 * @return the string result
	 */
	public String dump() {
		StringBuffer buff = new StringBuffer();

		List<Part> parts = result.getParts();

		int n = 0;

		int total = 0;
		for (Part p : parts) {
			try {
				total += p.getSize();
			}
			catch (CqiClientException e) {
				e.printStackTrace();
			}
		}
		buff.append("" + parts.size() + " " + PropertiesCoreMessages.Parts); //$NON-NLS-1$
		buff.append("Total size: " + total); //$NON-NLS-1$

		for (Part p : parts) {

			if (n >= props.pMaxPropertiesToDisplay) {
				buff.append("\n\t..."); //$NON-NLS-1$
				break;
			}

			try {
				buff.append("\n\t" + p.getName() + " (" + p.getSize() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return buff.toString();
	}

	public String htmlDump() {
		StringBuffer buff = new StringBuffer();
		List<Part> parts = result.getParts();
		buff.append("<h3>" + parts.size() + " " + PropertiesCoreMessages.Parts + "</h3>"); //$NON-NLS-1$ //$NON-NLS-3$

		int total = 0;
		for (Part p : parts) {
			try {
				total += p.getSize();
			}
			catch (CqiClientException e) {
				e.printStackTrace();
			}
		}
		buff.append("<p>Total size: " + total + "</p>"); //$NON-NLS-1$

		buff.append("<table>"); //$NON-NLS-1$
		int n = 0;
		for (Part p : parts) {

			if (n >= props.pMaxPropertiesToDisplay) {
				buff.append("<tr><td>...</td></tr>"); //$NON-NLS-1$
				break;
			}

			try {
				buff.append("<tr><td>" + p.getName() + "</td> <td>" + p.getSize() + "</td></tr>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			catch (CqiClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			n++;
		}
		buff.append("</table>"); //$NON-NLS-1$

		return buff.toString();
	}

	@Override
	public boolean _compute(TXMProgressMonitor monitor) throws Exception {

		return true;
	}

}
