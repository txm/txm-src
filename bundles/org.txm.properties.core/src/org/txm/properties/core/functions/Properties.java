// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $
//
package org.txm.properties.core.functions;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedHashSet;

import org.txm.Toolbox;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.properties.core.messages.PropertiesCoreMessages;
import org.txm.properties.core.preferences.PropertiesPreferences;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.utils.AsciiUtils;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;

/**
 * Computes prperties of a Corpus, Subcorpus or Partition.
 * 
 * @author mdecorde
 */
public class Properties extends TXMResult {

	PropertiesComputer<? extends TXMResult> computer;

	/**
	 * The HTML file.
	 */
	protected File file;

	/**
	 * The maximum number of word property values to display.
	 */
	@Parameter(key = PropertiesPreferences.MAX_PROPERTIES_TO_DISPLAY)
	protected int pMaxPropertiesToDisplay;

	/**
	 * The maximum number of word property values to display.
	 */
	@Parameter(key = PropertiesPreferences.MAX_LINELENGTH_TO_DISPLAY)
	protected int pMaxLineLengthTodisplay;

	/**
	 * Instantiates a new Properties.
	 *
	 * @param selection
	 *            a MainCorpus or a SubCorpus
	 */
	public Properties(TXMResult selection) {
		this(null, selection);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Properties(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * 
	 * @param parametersNodePath
	 */
	public Properties(String parametersNodePath, TXMResult parent) {
		super(parametersNodePath, parent);

		this.setVisible(false);

		if (this.parent instanceof CQPCorpus) {
			computer = new CorpusPropertiesComputer(this, (CQPCorpus) this.parent);
		}
		else if (this.parent instanceof Partition) {
			computer = new PartitionPropertiesComputer(this, (Partition) this.parent);
		}
		else {
			computer = new PropertiesComputer<TXMResult>(this, this.parent) {

				@Override
				public LinkedHashSet<String> getComputableInformations() {
					return new LinkedHashSet<String>(Arrays.asList(PropertiesCoreMessages.details));
				}

				@Override
				public String getInformation(String name) {
					return result.getDetails();
				}

				@Override
				public String getInformationHTML(String info) {
					return result.getDetails();
				}

				@Override
				public boolean _compute(TXMProgressMonitor monitor) throws Exception {
					return true;
				}
			};
		}
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.txt", "*.html" };
	}

	@Override
	public boolean loadParameters() {
		return true;
	}

	/**
	 * Sets the maximum properties to display.
	 * 
	 * @param maxPropertiesToDisplay
	 */
	public void setParameters(int maxPropertiesToDisplay) {
		this.pMaxPropertiesToDisplay = maxPropertiesToDisplay;
		if (this.pMaxPropertiesToDisplay <= 0) {
			this.pMaxPropertiesToDisplay = 10;
		}
		this.setDirty();
	}



	// FIXME: useless?
	// /**
	// * Instantiates a new diagnostic.
	// *
	// * @param partition the partition
	// * @throws CqiClientException the cqi client exception
	// */
	// public Information(Partition partition) throws CqiClientException {
	// corpus = partition.getCorpus();
	// this.partition = partition;
	// int T = corpus.getSize();
	// System.out.println(InformationCoreMessages.Properties_12);
	// int nbParts = partition.getNPart();
	// List<String> partNames = partition.getPartNames();
	// List<Integer> partSizes = new ArrayList<Integer>();
	// List<List<Property>> partProperties = new ArrayList<List<Property>>();
	// List<String> partQueries = new ArrayList<String>();
	// for (Part part : partition.getParts()) {
	// partSizes.add(part.getSize());
	// partProperties.add(part.getProperties());
	// partQueries.add(part.getQuery().getQueryString());
	// }
	// }

	public String htmlDump() {

		StringBuffer buffer = new StringBuffer();

		buffer.append("<? version=\"1.0\" encoding=\"UTF-8\"?>\n" + //$NON-NLS-1$
				"<html>" + //$NON-NLS-1$
				"<head>" + //$NON-NLS-1$
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>" + //$NON-NLS-1$
				"</head>" + //$NON-NLS-1$
				"<body>\n"); //$NON-NLS-1$


		buffer.append("<h1 style'font-family:\"Arial\";'>" + PropertiesCoreMessages.bind(PropertiesCoreMessages.descriptionOfP0, this.getParent().getName()) + "</h1>\n"); //$NON-NLS-2$ //$NON-NLS-1$

		for (String info : computer.getComputableInformations()) {
			buffer.append("<div id=\"" + info + "\">\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buffer.append("<h2>" + info + "</h2>\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buffer.append(computer.getInformationHTML(info));
			buffer.append("</div>\n"); //$NON-NLS-1$
		}

		buffer.append("</body>\n"); //$NON-NLS-1$
		return buffer.toString();
	}

	public String dump(int maxvalue) {
		StringBuffer buffer = new StringBuffer();
		for (String info : computer.getComputableInformations()) {
			buffer.append(info + "\n"); //$NON-NLS-1$
			buffer.append(computer.getInformation(info).toString());
			buffer.append(info + "\n"); //$NON-NLS-1$
		}
		return buffer.toString();
	}

	/**
	 * dump the result in the console with maxvalue=30.
	 *
	 * @return the string
	 */
	public String dump() {
		return dump(30);
	}

	/**
	 * dump the diagnostic in a text file.
	 *
	 * @param outfile the outfile
	 * @param maxvalue the maxvalue
	 * @return true, if successful
	 */
	public boolean toTxt(File outfile, String encoding, int maxvalue) {

		PrintWriter writer = null;
		try {
			writer = IOUtils.getWriter(outfile, encoding);
			if (outfile.getName().endsWith(".txt")) {

			}
			else {
				writer.write(this.htmlDump());
			}
			writer.close();
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}

		return true;
	}

	/**
	 * dump the diagnostic in a HTML file.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	public boolean toHTML(File outfile) {
		outfile.getParentFile().mkdirs();
		PrintWriter writer = null;
		try {
			writer = IOUtils.getWriter(outfile); // $NON-NLS-1$
			writer.write(this.htmlDump());
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		finally {
			if (writer != null) {
				writer.close();
			}
		}

		return true;
	}

	/**
	 * dump the diagnostic in a text file, maxvalue = 100.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	public boolean toTxt(File outfile) {
		return toTxt(outfile, "UTF-8", pMaxPropertiesToDisplay); //$NON-NLS-1$
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return toTxt(outfile, encoding, 100);
	}

	@Override
	public String getSimpleName() {
		return this.getResultType();
	}

	@Override
	public String getDetails() {
		return computer.getComputableInformations().toString();
	}


	@Override
	public String getComputingStartMessage() {
		// do not display anything
		return ""; //$NON-NLS-1$
	}

	@Override
	public String getComputingDoneMessage() {
		// do not display anything
		return ""; //$NON-NLS-1$
	}


	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		if (computer._compute(monitor.createNewMonitor(100))) {

			String txmhome = Toolbox.getTxmHomePath();
			String filename = computer.getName();

			filename = AsciiUtils.buildId(filename);
			file = new File(txmhome, "results/" + filename + "-infos.html"); //$NON-NLS-1$ //$NON-NLS-2$

			return this.toHTML(file);
		}

		return false;
	}

	@Override
	public String getName() {
		return PropertiesCoreMessages.RESULT_TYPE;
	}

	/**
	 * Gets the HTML file.
	 * 
	 * @return the HTML file
	 */
	public File getHTMLFile() {
		return file;
	}

	@Override
	public boolean canCompute() {
		return this.parent != null;
	}

	@Override
	public boolean saveParameters() {
		// nothing to do -> all parameters are already managed by autoSaveParameters
		return true;
	}

	@Override
	public String getResultType() {
		return PropertiesCoreMessages.RESULT_TYPE;
	}
}
