package org.txm.properties.core.functions;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.EnginesManager;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.properties.core.messages.PropertiesCoreMessages;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

public class CorpusPropertiesComputer extends PropertiesComputer<CQPCorpus> {

	LinkedHashSet<String> computableInformations = new LinkedHashSet<String>(
			Arrays.asList(PropertiesCoreMessages.SummaryStatistics, PropertiesCoreMessages.LexicalUnitsProperties, PropertiesCoreMessages.StructuralUnitsProperties));
	//new ArrayList<String>(Arrays.asList("Statistiques Générales", "Propriétés des unités lexicales", "Propriétés des structures"));
	// Summary Statistics, Lexical Units properties, Structural Units properties

	LinkedHashMap<String, String> data = new LinkedHashMap<>();

	public CorpusPropertiesComputer(Properties props, CQPCorpus result) {
		super(props, result);

	}

	@Override
	public LinkedHashSet<String> getComputableInformations() {

		return computableInformations;
	}

	@Override
	public String getInformation(String name) {

		if (PropertiesCoreMessages.SummaryStatistics.equals(name)) {
			return dumpGeneralStats();
		}
		else if (PropertiesCoreMessages.LexicalUnitsProperties.equals(name)) {
			return dumpLexicalStats();
		}
		else if (PropertiesCoreMessages.StructuralUnitsProperties.equals(name)) {
			return dumpStrutureStats();
		}
		else if (PropertiesCoreMessages.description.equals(name)) {
			return dumpDescription();
		}
		else {
			return data.get(name);
		}
	}

	@Override
	public String getInformationHTML(String name) {

		if (PropertiesCoreMessages.SummaryStatistics.equals(name)) {
			return htmlDumpGeneralStats(null);
		}
		else if (PropertiesCoreMessages.LexicalUnitsProperties.equals(name)) {
			return htmlDumpLexicalProperties(null);
		}
		else if (PropertiesCoreMessages.StructuralUnitsProperties.equals(name)) {
			return htmlDumpStructureProperties(null);
		}
		else if (PropertiesCoreMessages.description.equals(name)) {
			return dumpDescription();
		}
		else {
			return getInformation(name);
		}

	}

	// Numbers
	/** The T. */
	int numberOfWords = 0; // number of word

	/** The N properties. */
	int NProperties = 0; // number of word properties
	// HashMap<String, Integer> propertiesCounts = new HashMap<String, Integer>();
	// // properties counts

	/** The properties values. */
	HashMap<String, List<String>> propertiesValues = new HashMap<>(); // properties values

	/** The N structures. */
	int NStructures = 0; // number of structures

	/** The structures counts. */
	HashMap<String, Integer> structuresCounts = new HashMap<>(); // structures counts

	/** The internal architecture. */
	List<String> internalArchitecture = new ArrayList<>(); // cqp structures = propertiesCounts keys but ordered

	/** The hierarchie counts. */
	HashMap<String, Integer> hierarchieCounts = new HashMap<>(); // hierarchic structures counts

	/** The internal architecture properties. */
	HashMap<String, HashMap<String, List<String>>> internalArchitectureProperties = new HashMap<>(); // hierarchic structures counts

	/** The internal architecture properties counts. */
	HashMap<String, HashMap<String, Integer>> internalArchitecturePropertiesCounts = new HashMap<>(); // hierarchic structures counts

	// Annotations description
	/** The annotations types. */
	HashMap<String, Integer> annotationsTypes = new HashMap<>(); // for each annotation its description

	/** The annotations origins. */
	HashMap<String, Integer> annotationsOrigins = new HashMap<>(); // for each annoation its origin description

	/** The properties. */
	List<WordProperty> properties;

	/** The structures. */
	List<StructuralUnit> structures;

	@Override
	public String getName() {
		String filename = result.getMainCorpus() + "-" + result.getName(); //$NON-NLS-1$
		if (result instanceof MainCorpus) {
			filename = result.getID(); // $NON-NLS-1$
		}
		return filename;
	}

	/**
	 * Step general infos.
	 */
	public void stepGeneralInfos() {
		try {
			properties = result.getOrderedProperties();

			List<WordProperty> pproperties = new ArrayList<>(properties);
			for (WordProperty p : properties) {
				if (p.getName().equals("id")) { //$NON-NLS-1$
					pproperties.remove(p);
				}
			}
			properties = pproperties;

			NProperties = properties.size();
		}
		catch (CqiClientException e) {
			Log.severe(TXMCoreMessages.bind(PropertiesCoreMessages.error_failedToAccessPropertieOfCorpusP0, result.getName()));
			Log.printStackTrace(e);
			return;
		}

		try {
			numberOfWords = result.getSize();// corpus.getLexicon(corpus.getProperty("id")).nbrOfToken();
		}
		catch (CqiClientException e) {
			Log.severe(TXMCoreMessages.bind(PropertiesCoreMessages.error_failedToAccessLexiconOfCorpusP0, result.getName()));
			Log.printStackTrace(e);
			return;
		}
	}

	/**
	 * step2. randomly position are choose
	 */
	public void stepLexicalProperties() {
		propertiesValues.clear();

		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();

		int s = Math.min(props.pMaxPropertiesToDisplay, numberOfWords);
		int[] positions = new int[s];
		int n = 0;
		List<? extends org.txm.objects.Match> matches = result.getMatches();
		for (org.txm.objects.Match m : matches) {
			if (n >= s) break;
			for (int i = m.getStart(); i < m.getEnd(); i++) {
				if (n >= s) break;
				positions[n++] = i;
			}
		}

		for (Property p : properties) {
			// int size;
			try {
				LinkedHashSet<String> values = new LinkedHashSet<>(cqiClient.getSingleData(p, positions));
				propertiesValues.put(p.getName(), new ArrayList<>(values));
			}
			catch (Exception e) {
				Log.printStackTrace(e);
			}
		}
	}

	/**
	 * Step structural units.
	 */
	public void stepStructuralUnits() {

		try {
			structures = result.getOrderedStructuralUnits();

			List<StructuralUnit> sstructures = new ArrayList<>(structures);
			for (StructuralUnit su : structures) {
				if (su.getName().equals("txmcorpus")) { //$NON-NLS-1$
					sstructures.remove(su);
				}
			}
			structures = sstructures;
		}
		catch (CqiClientException e) {
			Log.severe(TXMCoreMessages.bind(PropertiesCoreMessages.error_failedToAccessStructuresOfCorpusP0, result.getName()));
			Log.printStackTrace(e);
			return;
		}
		NStructures = structures.size();
		internalArchitecture.clear();
		for (StructuralUnit su : structures) { // for each structural unit
			internalArchitecture.add(su.getName());
			hierarchieCounts.put(su.getName(), -1);
			internalArchitectureProperties.put(su.getName(), new HashMap<String, List<String>>());
			internalArchitecturePropertiesCounts.put(su.getName(), new HashMap<String, Integer>());

			for (StructuralUnitProperty sup : su.getOrderedProperties()) {// for each of its property
				try {
					List<String> allvalues = sup.getFirstValues(result, props.pMaxPropertiesToDisplay);
					internalArchitectureProperties.get(su.getName()).put(sup.getName(), allvalues.subList(0, Math.min(allvalues.size(), props.pMaxPropertiesToDisplay)));
					internalArchitecturePropertiesCounts.get(su.getName()).put(sup.getName(), allvalues.size());
				}
				catch (Exception e) {
					ArrayList<String> list = new ArrayList<>(1);
					list.add(PropertiesCoreMessages.error);
					internalArchitectureProperties.get(su.getName()).put(sup.getName(), list);
					internalArchitecturePropertiesCounts.get(su.getName()).put(sup.getName(), -1);
				}
			}
		}

		HashMap<String, org.txm.utils.Tuple<Integer>> firstPositionOfStructures = new HashMap<>();
		for (String su : internalArchitecture) {
			try {
				QueryResult r = result.query(new CQLQuery("<" + su + ">[]+ </" + su + "> cut 1"), "tmp", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				if (r.getNMatch() > 0) {
					firstPositionOfStructures.put(su, new org.txm.utils.Tuple<Integer>(r.getStarts()[0], r.getEnds()[0]));
				}
				else {
					firstPositionOfStructures.put(su, new org.txm.utils.Tuple<Integer>(Integer.MAX_VALUE, 0));
				}
			}
			catch (Exception e) {
				Log.printStackTrace(e);
				firstPositionOfStructures.put(su, new org.txm.utils.Tuple<Integer>(Integer.MAX_VALUE, 0));
			}
		}
		//System.out.println(firstPositionOfStructures);
		//		internalArchitecture.sort(new Comparator<String>() {
		//			
		//			@Override
		//			public int compare(String arg0, String arg1) {
		//				
		//				int r = firstPositionOfStructures.get(arg0).a - firstPositionOfStructures.get(arg1).a;
		//				if (r == 0) {
		//					int r2 = -(firstPositionOfStructures.get(arg0).b - firstPositionOfStructures.get(arg1).b);
		//					if (r2 == 0) {
		//						return arg0.compareTo(arg1);
		//					}
		//					return r2;
		//				}
		//				return r;
		//			}
		//		});
	}

	public String htmlDumpGeneralStats(StringBuffer buff) {

		if (buff == null) {
			buff = new StringBuffer();
		}

		//buff.append("<h3 style'font-family:\"Arial\";'>" + PropertiesCoreMessages.generalStatistics_2 + "</h3>\n"); //$NON-NLS-1$ //$NON-NLS-2$

		// counts
		buff.append("<ul>\n"); //$NON-NLS-1$
		buff.append("<li>" + NLS.bind(PropertiesCoreMessages.numberOfWordsP0, NumberFormat.getInstance().format(numberOfWords)) + "</li>\n"); //$NON-NLS-1$ //$NON-NLS-2$

		buff.append("<li>" + NLS.bind(PropertiesCoreMessages.numberOfWordPropertiesP0P1, properties.size(), StringUtils.join(properties, ", ")) + "</li>\n"); //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-1$
		buff.append("<li>" + NLS.bind(PropertiesCoreMessages.numberOfStructuralUnitsP0P1, structures.size(), StringUtils.join(structures, ", ")) + "</li>\n"); //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-1$
		buff.append("</ul>\n"); //$NON-NLS-1$

		return buff.toString();
	}

	public String htmlDumpLexicalProperties(StringBuffer buff) {

		if (buff == null) {
			buff = new StringBuffer();
		}

		// Propriétés d'occurrences
		//buff.append("<h3 style'font-family:\"Arial\";'>" + NLS.bind(PropertiesCoreMessages.lexicalUnitsPropertiesMaxP0Values, props.pMaxPropertiesToDisplay) + "</h3>\n"); //$NON-NLS-1$ //$NON-NLS-2$
		ArrayList<String> properties = new ArrayList<>(propertiesValues.keySet());
		Collections.sort(properties);

		buff.append("<p>\n&nbsp;&nbsp;&nbsp;&nbsp;"); //$NON-NLS-1$
		for (String s : properties) {
			buff.append("<a href=\"#prop_" + s + "\">" + s + "</a> ");
		}
		buff.append("</p>\n"); //$NON-NLS-1$

		buff.append("<ul>\n"); //$NON-NLS-1$
		for (String s : properties) {
			if (!s.equals("id")) { //$NON-NLS-1$
				// buff.append("<li> " + s + " (" + propertiesCounts.get(s) + ") : ");
				// //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				buff.append("<li id=\"prop_" + s + "\"> " + s + " : "); //$NON-NLS-1$ //$NON-NLS-2$
				int size = props.pMaxLineLengthTodisplay;
				for (String v : propertiesValues.get(s)) {
					if (size != props.pMaxLineLengthTodisplay) buff.append(", ");
					if (v.equals(",")) v = "\"" + v + "\"";
					buff.append(v); //$NON-NLS-1$

					size = size - v.length() - 2;
					if (size < 0) {
						buff.append("..."); //$NON-NLS-1$
						break;
					}
				}
				if (propertiesValues.get(s).size() >= this.props.pMaxPropertiesToDisplay) {
					buff.append("..."); //$NON-NLS-1$
				}
				buff.append("</li>\n"); //$NON-NLS-1$
			}
		}
		buff.append("</ul>\n"); //$NON-NLS-1$

		return buff.toString();
	}

	public String htmlDumpStructureProperties(StringBuffer buff) {

		if (buff == null) {
			buff = new StringBuffer();
		}

		buff.append("<p>\n&nbsp;&nbsp;&nbsp;&nbsp;"); //$NON-NLS-1$
		for (String s : internalArchitecture) {
			buff.append("<a href=\"#struct_" + s + "\">" + s + "</a> ");
		}
		buff.append("</p>\n"); //$NON-NLS-1$

		// Propriété de structures
		//buff.append("<h3 style'font-family:\"Arial\";'>" + NLS.bind(PropertiesCoreMessages.structuralUnitsPropertiesMaxP0Values, props.pMaxPropertiesToDisplay) + "</h3>\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buff.append("<ul>\n"); //$NON-NLS-1$
		for (String s : internalArchitecture) {
			StringBuffer subbuffer = new StringBuffer();
			if (s.equals("txmcorpus")) //$NON-NLS-1$
				continue;// ignore the txmcorpus structure

			List<String> properties = new ArrayList<>(internalArchitectureProperties.get(s).keySet());

			subbuffer.append("<li id=\"struct_" + s + "\"> " + s + "\n<ul>\n"); //$NON-NLS-1$ //$NON-NLS-2$
			if (internalArchitectureProperties.get(s).keySet().size() == 0)
				subbuffer.append("<li>" + PropertiesCoreMessages.noProperty + "</li>\n"); //$NON-NLS-1$ //$NON-NLS-2$

			// System.out.println("struct: "+s+" >> "+properties);
			Collections.sort(properties);
			// System.out.println("struct: "+s+" >sort> "+properties);
			int nbOfValues = 0;
			for (String ps : properties) {
				if (ps.equals("project") || ps.equals("base")) continue; //$NON-NLS-1$ //$NON-NLS-2$

				// System.out.println("struct: "+s+" >> "+ps);
				int valuecount = internalArchitecturePropertiesCounts.get(s).get(ps);
				ArrayList<String> values = new ArrayList<>(internalArchitectureProperties.get(s).get(ps));
				subbuffer.append("<li> " + ps); //$NON-NLS-1$ // + " (" + valuecount + ") = " not working, because internalArchitecturePropertiescontains only the values to be displayed and not all available values of this corpus/subcorpus
				// System.out.println("VALUES: "+values);
				//				Collections.sort(values);
				
				if (values.size() > 0) {
					subbuffer.append(" : ");
				}
				int size = props.pMaxLineLengthTodisplay;
				int i = 0;
				for (; i < values.size(); i++) {
					nbOfValues++;
					String psv = values.get(i);
					if (psv.equals(",")) psv = "\"" + psv + "\"";

					if (psv != null && psv.length() > 0)
						subbuffer.append(psv.replace("<", "&lt;")); //$NON-NLS-1$ //$NON-NLS-2$
					else
						subbuffer.append("\"\""); //$NON-NLS-1$

					size = size - psv.length() - 2;
					if (size < 0) {
						subbuffer.append("..."); //$NON-NLS-1$
						break;
					}

					if (i != values.size() - 1)
						subbuffer.append(", "); //$NON-NLS-1$
				}
				if (i >= props.pMaxPropertiesToDisplay) {
					subbuffer.append("..."); //$NON-NLS-1$
				}
				subbuffer.append("</li>\n"); //$NON-NLS-1$

			}

			if (properties.size() == 0) {
				subbuffer.append("<p>--</p>\n"); //$NON-NLS-1$
			}

			subbuffer.append("</ul>\n</li>\n"); //$NON-NLS-1$

			if (nbOfValues > 0) {
				buff.append(subbuffer);
			}
		}
		buff.append("</ul>\n"); //$NON-NLS-1$

		return buff.toString();
	}

	/**
	 * dump the results of the diagnostic in a String containing HTML code.
	 *
	 * @return the string
	 */
	public String htmlDumpAll() {
		StringBuffer buff = new StringBuffer();

		// Project projet = this.result.getProject();
		// if (projet.getCreationDate() != null) {
		// buff.append(NLS.bind(PropertiesCoreMessages.createdTheP0, TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(projet.getCreationDate())));
		// }
		// if (projet.getLastComputingDate() != null) {
		// buff.append(NLS.bind(PropertiesCoreMessages.updatedTheP0, TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(projet.getLastComputingDate())));
		// }
		//
		// if (projet.getDescription() != null && projet.getDescription().length() > 0) {
		// buff.append("<p><h3>Description</h3>" + projet.getDescription() + "</p>"); //$NON-NLS-1$ //$NON-NLS-2$
		// }

		htmlDumpGeneralStats(buff);

		htmlDumpLexicalProperties(buff);

		htmlDumpStructureProperties(buff);

		buff.append("</body>\n"); //$NON-NLS-1$
		buff.append("</html>\n"); //$NON-NLS-1$

		return buff.toString();
	}


	public String dumpGeneralStats() {

		StringBuffer buff = new StringBuffer();

		buff.append(PropertiesCoreMessages.t + String.format(Locale.FRANCE, "%,d", numberOfWords) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		buff.append(PropertiesCoreMessages.wordProperties + String.format("%,d", NProperties) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$

		buff.append(PropertiesCoreMessages.s + String.format("%,d", NStructures) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		for (String s : structuresCounts.keySet()) {
			buff.append(" - " + s + "\n");// +structuresCounts.get(s)+"\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		return buff.toString();
	}

	public String dumpLexicalStats() {

		StringBuffer buff = new StringBuffer();

		ArrayList<String> properties = new ArrayList<>(propertiesValues.keySet());
		Collections.sort(properties);
		for (String s : properties) {
			if (!s.equals("id")) { //$NON-NLS-1$
				int size = props.pMaxLineLengthTodisplay;
				buff.append("- " + s + " : "); //$NON-NLS-1$ //$NON-NLS-2$
				int n = 0;
				for (String v : propertiesValues.get(s)) {
					if (n > 0) buff.append(", ");
					if (v.equals(",")) v = "\"" + v + "\"";
					buff.append(v); //$NON-NLS-1$
					size = size - v.length() - 2;
					if (size < 0) {
						buff.append("..."); //$NON-NLS-1$
						break;
					}
					n++;
				}
				if (propertiesValues.get(s).size() >= this.props.pMaxPropertiesToDisplay) {
					buff.append("..."); //$NON-NLS-1$
				}
				buff.append("\n"); //$NON-NLS-1$
			}
		}
		buff.append("\n"); //$NON-NLS-1$

		return buff.toString();
	}

	/**
	 * dump the result in the console.
	 *
	 * @return the string result
	 */
	public String dumpDescription() {
		return this.result.getProject().getDescription();
	}

	/**
	 * dump the result in the console.
	 *
	 * @return the string result
	 */
	public String dumpStrutureStats() {
		StringBuffer buff = new StringBuffer();

		buff.append("All: " + StringUtils.join(internalArchitecture, ", "));

		for (String s : internalArchitecture) {
			buff.append(" - " + s + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			ArrayList<String> list = new ArrayList<>(internalArchitectureProperties.get(s).keySet());
			Collections.sort(list);

			for (String ps : list) {
				if (!(s.equals("text") && (ps.equals("project") || ps.equals("base") || ps.equals("id")))) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				{
					buff.append("   - " + ps + "(" + internalArchitectureProperties.get(s).get(ps).size() + ") = "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

					int valuecount = 0;

					int size = props.pMaxLineLengthTodisplay;
					for (String psv : internalArchitectureProperties.get(s).get(ps)) {
						if (psv.equals(",")) psv = "\"" + psv + "\"";
						buff.append(psv);
						size = size - psv.length();
						if (size < 0) {
							buff.append("..."); //$NON-NLS-1$
							break;
						}
						if (valuecount++ >= numberOfWords) {
							buff.append("..."); //$NON-NLS-1$
							break;
						}
						if (valuecount < internalArchitectureProperties.get(s).get(ps).size())
							buff.append(", "); //$NON-NLS-1$
					}

					buff.append("\n"); //$NON-NLS-1$
				}
			}
		}

		return buff.toString();
	}


	/**
	 * Gets the t.
	 *
	 * @return the t
	 */
	public int getT() {
		return numberOfWords;
	}

	/**
	 * Gets the n properties.
	 *
	 * @return the n properties
	 */
	public int getNProperties() {
		return NProperties;
	}

	/**
	 * Gets the internal architecture.
	 *
	 * @return the internal architecture
	 */
	public List<String> getInternalArchitecture() {
		return internalArchitecture;
	}

	/**
	 * Gets the n structures.
	 *
	 * @return the n structures
	 */
	public int getNStructures() {
		return NStructures;
	}

	/**
	 * Gets the structures counts.
	 *
	 * @return the structures counts
	 */
	public HashMap<String, Integer> getStructuresCounts() {
		return structuresCounts;
	}

	/**
	 * Gets the annotations types.
	 *
	 * @return the annotations types
	 */
	public HashMap<String, Integer> getAnnotationsTypes() {
		return annotationsTypes;
	}

	/**
	 * Gets the annotations origins.
	 *
	 * @return the annotations origins
	 */
	public HashMap<String, Integer> getAnnotationsOrigins() {
		return annotationsOrigins;
	}

	@Override
	public boolean _compute(TXMProgressMonitor monitor) throws Exception {

		monitor.setTask(props.getComputingStartMessage());

		this.stepGeneralInfos();
		monitor.worked(34);

		this.stepLexicalProperties();
		monitor.worked(33);

		this.stepStructuralUnits();
		monitor.worked(33);

		if (result.getProject().getDescription().length() > 0 && !result.getProject().getDescription().startsWith("<pre>")) { // TODO do better, this is not very maintainable
			computableInformations = new LinkedHashSet<String>(Arrays.asList(PropertiesCoreMessages.description, PropertiesCoreMessages.SummaryStatistics,
					PropertiesCoreMessages.LexicalUnitsProperties, PropertiesCoreMessages.StructuralUnitsProperties));
		}
		else {
			computableInformations = new LinkedHashSet<String>(
					Arrays.asList(PropertiesCoreMessages.SummaryStatistics, PropertiesCoreMessages.LexicalUnitsProperties, PropertiesCoreMessages.StructuralUnitsProperties));
		}
		//
		data.clear();

		for (EngineType set : Toolbox.getEngineManagers().keySet()) {

			EnginesManager<?> se = Toolbox.getEngineManagers().get(set);
			for (Engine engine : se.getEngines().values()) {

				String info = engine.hasAdditionalDetailsForResult(result);
				if (info != null && info.length() > 0) {
					if (!data.containsKey(info)) {
						data.put(info, ""); //$NON-NLS-1$
					}
					data.put(info, data.get(info) + "\n" + engine.getAdditionalDetailsForResult(result)); //$NON-NLS-1$
					computableInformations.add(info);
				}
			}
		}
		//	System.out.println(data.keySet());
		return true;
	}

	public String getDetails() {
		if (numberOfWords >= 0) {
			return NLS.bind(PropertiesCoreMessages.numberOfWordsP0, numberOfWords);
		}
		else {
			return ""; //$NON-NLS-1$
		}
	}
}
