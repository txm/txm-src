package org.txm.properties.core.functions;

import java.util.LinkedHashSet;

import org.txm.core.results.TXMResult;
import org.txm.utils.TXMProgressMonitor;

/**
 * Properties use this existing class implementations to retrieve informations to show
 * 
 * @author mdecorde
 *
 */
public abstract class PropertiesComputer<T extends TXMResult> {

	protected T result;

	protected Properties props;

	public PropertiesComputer(Properties props, T result) {
		this.result = result;
		this.props = props;
	}

	/**
	 * The ordered list of informations to display
	 * 
	 * @return
	 */
	public abstract LinkedHashSet<String> getComputableInformations();

	/**
	 * raw informations
	 * 
	 * @param name
	 * @return
	 */
	public abstract String getInformation(String name);

	/**
	 * HTML formated informations to insert a HTML element
	 * 
	 * @return
	 */
	public abstract String getInformationHTML(String info);

	/**
	 * if the PropertiesComputer needs to prepare informations or the compute the available informations
	 * 
	 * @return
	 */
	public abstract boolean _compute(TXMProgressMonitor monitor) throws Exception;

	public String getName() {
		return result.getClass().getSimpleName() + "-" + result.getName(); //$NON-NLS-1$
	}
}
