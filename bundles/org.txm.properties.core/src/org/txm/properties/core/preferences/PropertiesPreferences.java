package org.txm.properties.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Information preferences initializer.
 * 
 * @author sjacquot
 *
 */
public class PropertiesPreferences extends TXMPreferences {

	public static final String PREFERENCES_PREFIX = "properties_"; //$NON-NLS-1$


	/**
	 * The maximum number of word property values to display.
	 */
	public static final String MAX_PROPERTIES_TO_DISPLAY = PREFERENCES_PREFIX + "max_properties_to_display"; //$NON-NLS-1$

	public static final String MAX_LINELENGTH_TO_DISPLAY = PREFERENCES_PREFIX + "max_linelength_to_display"; //$NON-NLS-1$


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putInt(MAX_PROPERTIES_TO_DISPLAY, 10);
		preferences.putInt(MAX_LINELENGTH_TO_DISPLAY, 200);

	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(PropertiesPreferences.class)) {
			new PropertiesPreferences();
		}
		return TXMPreferences.instances.get(PropertiesPreferences.class);
	}
}
