package org.txm.properties.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Properties core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class PropertiesCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.properties.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;



	public static String error_failedToAccessPropertieOfCorpusP0;

	public static String lexicalUnitsPropertiesMaxP0Values;

	public static String numberOfStructuralUnitsP0P1;

	public static String error;

	public static String structuralUnitsPropertiesMaxP0Values;

	public static String noProperty;

	public static String descriptionOfP0;

	public static String description;

	public static String details;


	public static String t;

	public static String wordProperties;

	public static String s;

	public static String generalStatistics_2;

	public static String numberOfWordsP0;

	public static String numberOfWordPropertiesP0P1;

	public static String error_failedToAccessLexiconOfCorpusP0;

	public static String error_failedToAccessStructuresOfCorpusP0;

	public static String updatedTheP0;

	public static String createdTheP0;

	public static String LexicalUnitsProperties;

	public static String Parts;



	public static String StructuralUnitsProperties;

	public static String SummaryStatistics;



	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, PropertiesCoreMessages.class);
	}

	private PropertiesCoreMessages() {
	}

}
