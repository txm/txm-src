package org.txm.conllu.rcp.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.conllu.core.preferences.UDTreePreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.jface.ComboFieldEditor;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * UD Tree preferences page
 * 
 * @author mdecorde
 *
 */
public class UDTreePreferencePage extends TXMPreferencePage {

	@Override
	public void createFieldEditors() {

		String[][] values = { { "DepTreeViz", UDTreePreferences.DEPTREEVIZ } //$NON-NLS-1$
							, { "Brat", UDTreePreferences.BRAT } }; //$NON-NLS-1$

		this.addField(new ComboFieldEditor(UDTreePreferences.VISUALISATION, "Visualisation", values, this.getFieldEditorParent()));

		this.addField(new BooleanFieldEditor(UDTreePreferences.PRINTCONLLUSENTENCES, "Print CoNLL-U sentence when browsing the matches", this.getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(UDTreePreferences.getInstance().getPreferencesNodeQualifier()));
		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/functions/UD.png")); //$NON-NLS-1$
	}
}
