package org.txm.conllu.rcp.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IWorkbench;
import org.txm.conllu.core.preferences.UDPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * UD preferences page
 * 
 * @author mdecorde
 *
 */
public class CoNLLUPreferencePage extends TXMPreferencePage {

	private StringFieldEditor udPrefixField;

	@Override
	public void createFieldEditors() {

		this.addField(new BooleanFieldEditor(UDPreferences.IMPORT_USE_NEW_DOC_ID, "Use new odc id when importing CoNLL-U files", this.getFieldEditorParent()));
		this.addField(new BooleanFieldEditor(UDPreferences.IMPORT_BUILD_TIGERSEARCH_INDEXES, "Build TIGERSearch indexes as well", this.getFieldEditorParent()));

		this.addField(new BooleanFieldEditor(UDPreferences.CONTRACTIONS_MANAGEMENT, "Keep multiword tokens when importing CoNLL-U files", this.getFieldEditorParent()));
		udPrefixField = new StringFieldEditor(UDPreferences.UDPREFIX, "UD properties prefix", this.getFieldEditorParent());
		this.addField(udPrefixField);
		this.addField(new StringFieldEditor(UDPreferences.IMPORT_HEAD_TO_PROJECT, "UD head properties to project (comma separated list)", this.getFieldEditorParent()));
		this.addField(new StringFieldEditor(UDPreferences.IMPORT_DEPS_TO_PROJECT, "UD deps properties to project (comma separated list)", this.getFieldEditorParent()));
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (udPrefixField.getStringValue().length() == 0) {
			setErrorMessage("UD prefix cannot be empty");
		}
	}

	@Override
	public boolean performOk() {

		return super.performOk();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(UDPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/functions/UD.png"));
	}
}
