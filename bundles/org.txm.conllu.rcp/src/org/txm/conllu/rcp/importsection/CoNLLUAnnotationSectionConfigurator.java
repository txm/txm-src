package org.txm.conllu.rcp.importsection;

import java.util.HashMap;

import org.txm.rcp.editors.imports.ImportEditorSectionConfigurator;
import org.txm.rcp.editors.imports.ImportModuleCustomization;


public class CoNLLUAnnotationSectionConfigurator extends ImportEditorSectionConfigurator {

	@Override
	public void installSections() {

		ImportModuleCustomization.addAdditionalSections("conllu", CoNLLUSection.class); //$NON-NLS-1$

		HashMap<String, Boolean> params = new HashMap<String, Boolean>(ImportModuleCustomization.getParameters("xtzLoader.groovy")); //$NON-NLS-1$
		params.put(ImportModuleCustomization.LANG, true);
		params.put(ImportModuleCustomization.XSLT, false);
		params.put(ImportModuleCustomization.WORDS, false);
		params.put(ImportModuleCustomization.ADVANCEDTOKENIZER, true);
		params.put(ImportModuleCustomization.EDITIONS_WORDSPERPAGE, true);
		params.put(ImportModuleCustomization.EDITIONS_PAGEELEMENT, false);
		params.put(ImportModuleCustomization.PATTRIBUTES, false);
		params.put(ImportModuleCustomization.SATTRIBUTES, false);
		params.put(ImportModuleCustomization.PREBUILD, false);
		params.put(ImportModuleCustomization.QUERIES, false);
		params.put(ImportModuleCustomization.UI, true);
		params.put(ImportModuleCustomization.TEXTUALPLANS, false);
		params.put(ImportModuleCustomization.STRUCTURES, false);
		params.put(ImportModuleCustomization.OPTIONS, true);
		ImportModuleCustomization.addNewImportModuleConfiguration("conlluLoader.groovy", "CoNLL-U + CSV", params); //$NON-NLS-1$
	}
}
