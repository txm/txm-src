package org.txm.conllu.rcp.importsection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.osgi.service.prefs.Preferences;
import org.txm.conllu.core.preferences.UDPreferences;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.editors.imports.sections.ImportEditorSection;
import org.txm.rcp.swt.widget.RadioGroup;

public class CoNLLUSection extends ImportEditorSection {

	String ID = CoNLLUSection.class.getSimpleName();

	private static final int SECTION_SIZE = 1;

	Button printNewLinesInEditionsButton;

	Button buildTIGERIndexesButton;

	Button useNewDocIdButton;

	RadioGroup keepWordContractionsButton;

	Text udPropertiesPrefixButton;

	private Text headPropertiesText;

	private Text depsPropertiesText;

	/**
	 * 
	 * @param toolkit2
	 * @param form2
	 * @param parent
	 */
	public CoNLLUSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {

		super(editor, toolkit2, form2, parent, style, "CoNLL-U");

		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		layout.numColumns = 1;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.numColumns = 4;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		buildTIGERIndexesButton = toolkit.createButton(sectionClient, "Build TIGERSearch indexes as well", SWT.CHECK);
		TableWrapData gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		buildTIGERIndexesButton.setLayoutData(gdata2);

		useNewDocIdButton = toolkit.createButton(sectionClient, "Use new doc id when importing CoNLL-U files", SWT.CHECK);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		useNewDocIdButton.setLayoutData(gdata2);

		keepWordContractionsButton = new RadioGroup(sectionClient, SWT.NONE, "Contractions management",
				new String[][] { { UDPreferences.SURFACE, "Surface" }, { UDPreferences.SYNTAX, "Syntax" }, { UDPreferences.ALL, "All" } });//toolkit.create toolkit.createButton(sectionClient, "Keep multiword tokens when importing CoNLL-U files", SWT.CHECK);
		keepWordContractionsButton.setToolTipText("Surface: only the surface is indexed, Syntax: Only the multiwords tokens are indexed, All: both are indexed");
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		keepWordContractionsButton.setLayoutData(gdata2);

		Label tmp5Label = toolkit.createLabel(sectionClient, "UD properties prefix");
		tmp5Label.setToolTipText("The prefix of the CQP properties containing the UD properties");
		tmp5Label.setLayoutData(getLabelGridData());

		udPropertiesPrefixButton = toolkit.createText(sectionClient, "UD properties prefix", SWT.BORDER);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 3; // one line
		udPropertiesPrefixButton.setLayoutData(gdata2);

		Label tmp4Label = toolkit.createLabel(sectionClient, "Head properties to project");
		tmp4Label.setToolTipText("Comma separated list of ud properties.");
		tmp4Label.setLayoutData(getLabelGridData());

		headPropertiesText = toolkit.createText(sectionClient, "UD head properties to project (comma separated list)", SWT.BORDER);
		gdata2 = getTextGridData();
		gdata2.colspan = 3; // one line
		headPropertiesText.setLayoutData(gdata2);

		tmp4Label = toolkit.createLabel(sectionClient, "Deps properties to project");
		tmp4Label.setToolTipText("Comma separated list of ud properties.");
		tmp4Label.setLayoutData(getLabelGridData());

		// build text edition or not button
		depsPropertiesText = toolkit.createText(sectionClient, "UD deps properties to project (comma separated list)", SWT.BORDER);
		gdata2 = getTextGridData();
		gdata2.colspan = 3; // one line
		depsPropertiesText.setLayoutData(gdata2);

		printNewLinesInEditionsButton = toolkit.createButton(sectionClient, "Format sentences in edition/Formatage des phrases dans l'édition", SWT.CHECK);
		gdata2 = getButtonLayoutData();
		gdata2.colspan = 4; // one line
		printNewLinesInEditionsButton.setLayoutData(gdata2);
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		Preferences customNode = project.getPreferencesScope().getNode(UDPreferences.getInstance().getPreferencesNodeQualifier());

		buildTIGERIndexesButton
				.setSelection(customNode.getBoolean(UDPreferences.IMPORT_BUILD_TIGERSEARCH_INDEXES, UDPreferences.getInstance().getBoolean(UDPreferences.IMPORT_BUILD_TIGERSEARCH_INDEXES))); //$NON-NLS-1$
		printNewLinesInEditionsButton
				.setSelection(customNode.getBoolean(UDPreferences.IMPORT_PRINT_NEWLINES_AFTER_SENTENCES, UDPreferences.getInstance().getBoolean(UDPreferences.IMPORT_PRINT_NEWLINES_AFTER_SENTENCES))); //$NON-NLS-1$
		useNewDocIdButton.setSelection(customNode.getBoolean(UDPreferences.IMPORT_USE_NEW_DOC_ID, UDPreferences.getInstance().getBoolean(UDPreferences.IMPORT_USE_NEW_DOC_ID))); //$NON-NLS-1$
		keepWordContractionsButton.setSelection(customNode.get(UDPreferences.CONTRACTIONS_MANAGEMENT, UDPreferences.getInstance().getString(UDPreferences.CONTRACTIONS_MANAGEMENT))); //$NON-NLS-1$
		udPropertiesPrefixButton.setText(customNode.get(UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX))); //$NON-NLS-1$
		headPropertiesText.setText(customNode.get(UDPreferences.IMPORT_HEAD_TO_PROJECT, UDPreferences.getInstance().getString(UDPreferences.IMPORT_HEAD_TO_PROJECT))); //$NON-NLS-1$
		depsPropertiesText.setText(customNode.get(UDPreferences.IMPORT_DEPS_TO_PROJECT, UDPreferences.getInstance().getString(UDPreferences.IMPORT_DEPS_TO_PROJECT))); //$NON-NLS-1$
	}

	@Override
	public boolean saveFields(Project project) {
		if (this.section != null && !this.section.isDisposed()) {

			Preferences customNode = project.getPreferencesScope().getNode(UDPreferences.getInstance().getPreferencesNodeQualifier());
			customNode.putBoolean(UDPreferences.IMPORT_BUILD_TIGERSEARCH_INDEXES, buildTIGERIndexesButton.getSelection());
			customNode.putBoolean(UDPreferences.IMPORT_PRINT_NEWLINES_AFTER_SENTENCES, printNewLinesInEditionsButton.getSelection());
			customNode.putBoolean(UDPreferences.IMPORT_USE_NEW_DOC_ID, useNewDocIdButton.getSelection());
			customNode.put(UDPreferences.CONTRACTIONS_MANAGEMENT, keepWordContractionsButton.getSelection());
			customNode.put(UDPreferences.UDPREFIX, udPropertiesPrefixButton.getText());
			customNode.put(UDPreferences.IMPORT_HEAD_TO_PROJECT, headPropertiesText.getText());
			customNode.put(UDPreferences.IMPORT_DEPS_TO_PROJECT, depsPropertiesText.getText());
		}
		return true;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
