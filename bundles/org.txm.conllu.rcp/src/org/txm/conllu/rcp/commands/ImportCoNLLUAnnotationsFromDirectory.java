// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.conllu.rcp.commands;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.conllu.core.function.ImportCoNLLUAnnotations;
import org.txm.conllu.core.preferences.UDPreferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.commands.workspace.UpdateCorpus;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

/**
 * Import CoNLLU annotations into a TXM corpus
 * 
 * If the corpus already contains CoNLLU annotations, they are replaced
 * 
 * @author mdecorde.
 */
public class ImportCoNLLUAnnotationsFromDirectory extends AbstractHandler {

	public static final String ID = ImportCoNLLUAnnotationsFromDirectory.class.getName();

	@Option(name = "conlluDirectory", usage = "conlluDirectory", widget = "Folder", required = true, def = "conllu-directory")
	File conlluDirectory;

	@Option(name = "propertiesPrefix", usage = "optional prefix for the properties to create", widget = "String", required = true, def = "ud-")
	String propertiesPrefix;

	@Option(name = "udPropertiesToImport", usage = "to create the ud properties", widget = "StringArrayMultiple", metaVar = "id	form	lemma	upos	xpos	feats	head	deprel	deps	misc",
			required = true, def = "form,lemma,upos,xpos,feats,head,deprel,deps,misc")
	String udPropertiesToImport;

	@Option(name = "overwrite_cqp_properties", usage = "if set the CQP properties are replaced", widget = "Boolean", required = true, def = "false")
	Boolean overwrite_cqp_properties;

	@Option(name = "normalize_word_ids", usage = "if set the CQP properties are replaced", widget = "Boolean", required = true, def = "false")
	Boolean normalize_word_ids;

	@Option(name = "import_conll_comment_properties", usage = "if set sentid, newdocid and parid will be imported in CQP properties", widget = "Boolean", required = true, def = "false")
	Boolean import_conll_comment_properties;

	@Option(name = "headPropertiesToProject", usage = "to create the head-XYZ properties from the word head", widget = "StringArrayMultiple",
			metaVar = "form	lemma	upos	xpos	feats	head	deprel	deps	misc", required = true, def = "deprel,upos") //$NON-NLS-1$
	String headPropertiesToProject;

	@Option(name = "depsPropertiesToProject", usage = "to create the dep-XYZ from the word dependancies", widget = "StringArrayMultiple",
			metaVar = "form	lemma	upos	xpos	feats	head	deprel	deps	misc", required = true, def = "deprel,upos") //$NON-NLS-1$
	String depsPropertiesToProject;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Object s = selection.getFirstElement();
		if (!(s instanceof MainCorpus)) {
			Log.warning("Selection is not a corpus. Aborting.");
			return null;
		}

		if (!ParametersDialog.open(this)) {
			return null;
		}
		if (conlluDirectory == null || !conlluDirectory.exists() || !conlluDirectory.isDirectory() || conlluDirectory.listFiles().length == 0) {
			Log.warning("Error: conllu directory is empty: " + conlluDirectory);
			return null;
		}

		CQPCorpus corpus = (CQPCorpus) s;
		MainCorpus mainCorpus = corpus.getMainCorpus();

		try {
			HashSet<String> test = new HashSet<>(); // will contains the CQP properties to update
			for (String p : udPropertiesToImport.split(",")) { // test the properties to import //$NON-NLS-1$
				if (mainCorpus.getProperty(propertiesPrefix + p) != null) {
					test.add(propertiesPrefix + p);
				}
			}
			if (test.size() > 0 && !overwrite_cqp_properties) {
				Log.warning(NLS.bind("Error: can't use the {0} prefix because some properties are already used: {1}", propertiesPrefix, test));
				return null;
			}

			return importAnnotations(mainCorpus, conlluDirectory, propertiesPrefix, normalize_word_ids, import_conll_comment_properties,
					new HashSet<String>(Arrays.asList(headPropertiesToProject.split(","))), //$NON-NLS-1$
					new HashSet<String>(Arrays.asList(depsPropertiesToProject.split(","))), //$NON-NLS-1$
					new HashSet<String>(Arrays.asList(udPropertiesToImport.split(",")))); //$NON-NLS-1$
		}
		catch (Exception e) {
			Log.warning(e);
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * if import CoNLLU annotations in the corpus with the same name already exists, it is replaced
	 * 
	 * @param mainCorpus
	 * @param conlluDirectory
	 * @param propertiesPrefix
	 * @return the number of imported annotations
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public static int importAnnotations(MainCorpus mainCorpus, File conlluDirectory, String propertiesPrefix, Boolean normalizeWordIds, Boolean importCommentProperties,
			Set<String> headPropertiesToProject, Set<String> depsPropertiesToProject, Set<String> udPropertiesToImport) throws IOException, CqiServerError, CqiClientException, XMLStreamException {

		Log.info(TXMCoreMessages.bind("Importing CONLL-U annotations of {0} in {1} using the ''{2}'' prefix...", conlluDirectory, mainCorpus, propertiesPrefix));

		File[] files = conlluDirectory.listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return file.isFile() && file.getName().endsWith(".conllu"); //$NON-NLS-1$
			}
		});

		int nTextProcessed = 0;
		int nWordsInserted = 0;
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.length);

		if (files.length == 0) {
			Log.warning(NLS.bind("** No *.conllu files found in {0}. Aborting.", conlluDirectory));
			return 0;
		}

		for (File coonluFile : files) {
			cpb.tick();
			nWordsInserted += ImportCoNLLUAnnotations._importAnnotations(coonluFile, mainCorpus, propertiesPrefix, null, normalizeWordIds, importCommentProperties, headPropertiesToProject,
					depsPropertiesToProject, udPropertiesToImport);
			nTextProcessed++;
		}
		cpb.done();

		if (nTextProcessed == 0) {
			Log.warning("** No text to process. Aborting.");
			return 0;
		}

		if (nWordsInserted == 0) {
			Log.warning("** No annotation imported. Aborting.");
			return 0;
		}

		mainCorpus.getProject().appendToLogs(
				"CoNLL-U annotations imported from " + conlluDirectory + " : " + nTextProcessed + " texts and " + nWordsInserted + " words processed " + " by " + OSDetector.getUserAndOSInfos());

		Log.info("XML-TXM source files updated. Updating indexes...");

		UDPreferences.getInstance().setProjectPreferenceValue(mainCorpus.getProject(), UDPreferences.UDPREFIX, propertiesPrefix);

		UpdateCorpus.update(mainCorpus);

		Log.info("Done.");

		return nWordsInserted;
	}

	/**
	 * 
	 * if import CoNLLU annotations in the corpus with the same name already exists, it is replaced
	 * 
	 * @param mainCorpus
	 * @param conlluFile
	 * @param propertiesPrefix
	 * @param normalize_word_ids
	 * @return the number of imported annotations
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public static int importAnnotationsFromCoNLLUFile(MainCorpus mainCorpus, File conlluFile, String propertiesPrefix, String textId, Boolean normalize_word_ids, Boolean importCommentProperties,
			Set<String> headPropertiesToProject, Set<String> depsPropertiesToProject, Set<String> udPropertiesToImport) throws IOException,
			CqiServerError, CqiClientException, XMLStreamException {
		Log.info(TXMCoreMessages.bind("Importing CONLL-u annotations of {0} in {1} using the ''{2}'' prefix...", conlluFile, mainCorpus, propertiesPrefix));

		int nWordsInserted = ImportCoNLLUAnnotations._importAnnotations(conlluFile, mainCorpus, propertiesPrefix, textId, normalize_word_ids, importCommentProperties, headPropertiesToProject,
				depsPropertiesToProject, udPropertiesToImport);

		if (nWordsInserted == 0) {
			Log.warning("** No annotation imported. Aborting.");
			return 0;
		}

		mainCorpus.getProject().appendToLogs("CoNLL-U annotations imported from " + conlluFile + " texts and " + nWordsInserted + " words processed." + " by " + OSDetector.getUserAndOSInfos());

		Log.info("XML-TXM source files updated. Updating indexes...");

		UDPreferences.getInstance().setProjectPreferenceValue(mainCorpus.getProject(), UDPreferences.UDPREFIX, propertiesPrefix);

		UpdateCorpus.update(mainCorpus);

		Log.info("Done.");

		return nWordsInserted;
	}

}
