// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.conllu.rcp.commands;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.conllu.core.function.ImportCoNLLUAnnotations;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

/**
 * Import CoNLLU annotations into a TXM corpus
 * 
 * IF the corpus already contains CoNLLU annotations, they are replaced
 * 
 * @author mdecorde.
 */
public class ImportCoNLLUAnnotationsFromFile extends AbstractHandler {

	public static final String ID = ImportCoNLLUAnnotationsFromFile.class.getName();

	@Option(name = "conlluFile", usage = "CoNLL-U file", widget = "FileOpen", required = true, def = "file.conllu")
	File conlluFile;

	@Option(name = "textId", usage = "Identifier of the text to update", widget = "String", required = true, def = "text-id")
	String textId;

	@Option(name = "propertiesPrefix", usage = "optional prefix for the properties to create", widget = "String", required = true, def = "ud-")
	String propertiesPrefix;

	@Option(name = "udPropertiesToImport", usage = "to create the ud properties", widget = "StringArrayMultiple", metaVar = "id	form	lemma	upos	xpos	feats	head	deprel	deps	misc",
			required = true, def = "form,lemma,upos,xpos,feats,head,deprel,deps,misc") //$NON-NLS-1$
	String udPropertiesToImport;

	@Option(name = "overwrite_cqp_properties", usage = "if set the CQP properties are replaced", widget = "Boolean", required = true, def = "false")
	Boolean overwrite_cqp_properties;

	@Option(name = "normalize_word_ids", usage = "if set the CQP properties are replaced", widget = "Boolean", required = true, def = "false")
	Boolean normalize_word_ids;

	@Option(name = "import_conll_comment_properties", usage = "if set sentid, newdocid and parid will be imported in CQP properties", widget = "Boolean", required = true, def = "false")
	Boolean import_conll_comment_properties;

	@Option(name = "headPropertiesToProject", usage = "to create the headXYZ properties", widget = "StringArrayMultiple",
			metaVar = "form	lemma	upos	xpos	feats	head	deprel	deps	misc", required = true, def = "deprel,upos") //$NON-NLS-1$
	String headPropertiesToProject;

	@Option(name = "depsPropertiesToProject", usage = "to create the depXYZ and outdeprel properties", widget = "StringArrayMultiple",
			metaVar = "form	lemma	upos	xpos	feats	head	deprel	deps	misc", required = true, def = "deprel,upos") //$NON-NLS-1$
	String depsPropertiesToProject;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		Object s = selection.getFirstElement();
		if (!(s instanceof MainCorpus)) {
			Log.warning("Selection is not a corpus. Aborting.");
			return null;
		}

		if (!ParametersDialog.open(this)) {
			return null;
		}
		if (conlluFile == null || !conlluFile.exists() || !conlluFile.isFile()) {
			Log.warning("Error: cannot access to the conllu file: " + conlluFile);
			return null;
		}

		CQPCorpus corpus = (CQPCorpus) s;
		MainCorpus mainCorpus = corpus.getMainCorpus();

		try {
			HashSet<String> test = new HashSet<>();
			for (String p : ImportCoNLLUAnnotations.UD_PROPERTY_NAMES) {
				if (mainCorpus.getProperty(propertiesPrefix + p) != null) {
					test.add(propertiesPrefix + p);
				}
			}
			if (test.size() > 0 && !overwrite_cqp_properties) {
				Log.warning(NLS.bind("Error: can't use the {0} prefix because some properties are already used: {1}", propertiesPrefix, test));
				return null;
			}

			return ImportCoNLLUAnnotationsFromDirectory.importAnnotationsFromCoNLLUFile(mainCorpus, conlluFile, propertiesPrefix, textId, normalize_word_ids, import_conll_comment_properties,
					new HashSet<String>(Arrays.asList(headPropertiesToProject.split(","))), //$NON-NLS-1$
					new HashSet<String>(Arrays.asList(depsPropertiesToProject.split(","))), //$NON-NLS-1$
					new HashSet<String>(Arrays.asList(udPropertiesToImport.split(",")))); //$NON-NLS-1$
		}
		catch (Exception e) {
			Log.warning(e);
			Log.printStackTrace(e);
		}

		return null;
	}
}
