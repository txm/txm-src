package org.txm.conllu.rcp.commands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.logger.Log;

public class ToCoNLL2009 {

	boolean debug = false;

	/**
	 * To CoNLL2009.
	 *
	 * @param corpus the corpus or sub-corpus to export
	 * @param sentenceUnit the StructuralUnitProperty that identify a sentence
	 * @param word the word property
	 * @param lemme the lemme property
	 * @param pos the pos property
	 * @param encoding the encoding of the outfile
	 * @return true, if successful
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws CqiClientException
	 */
	public boolean process(File outdir, CQPCorpus corpus, StructuralUnit sentenceUnit, Property word, Property lemme, Property pos, String encoding)
			throws CqiClientException, IOException, CqiServerError {

		String sstruct = ""; //$NON-NLS-1$
		if (sentenceUnit != null) {
			sstruct = "expand to " + sentenceUnit.getName(); //$NON-NLS-1$
		}

		String[] texts = corpus.getCorpusTextIdsList();
		WordProperty idProperty = corpus.getProperty("id"); //$NON-NLS-1$

		ConsoleProgressBar cpb = new ConsoleProgressBar(texts.length);
		Log.info("Writing CoNLLu files in " + outdir.getAbsolutePath());
		int nTokens = 0;
		for (String text : texts) {

			cpb.tick();

			List<Match> matches = corpus.query(new CQLQuery("[_.text_id=\"" + CQLQuery.addBackSlash(text) + "\"]" + sstruct), "TMPEXPORTCONLL", false).getMatches(); //$NON-NLS-1$
			if (debug) System.out.println(matches.size());
			int npositions = 0;
			for (Match match : matches)
				npositions += match.size() + 1;
			if (debug) System.out.println("npositions=" + npositions); //$NON-NLS-1$

			int[] positions = new int[npositions + 1];
			int i = 0;
			for (Match match : matches) {
				for (int p : match.getRange())
					positions[i++] = p;
			}

			String[] words = CorpusManager.getCorpusManager().getCqiClient().cpos2Str(word.getQualifiedName(), positions);
			nTokens += words.length;
			String[] idsList = CorpusManager.getCorpusManager().getCqiClient().cpos2Str(idProperty.getQualifiedName(), positions);

			String[] lemmes = null;
			if (lemme != null) {
				lemmes = CorpusManager.getCorpusManager().getCqiClient().cpos2Str(lemme.getQualifiedName(), positions);
			}

			String[] poss = null;
			if (pos != null) {
				poss = CorpusManager.getCorpusManager().getCqiClient().cpos2Str(pos.getQualifiedName(), positions);
			}

			File outfile = new File(outdir, text + ".conllu"); //$NON-NLS-1$
			Writer writer = new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8"); //$NON-NLS-1$;

			String lemmestr;
			String posstr;
			int id = 0;
			int iposition = 0;
			for (Match match : matches) {

				writer.write("\n"); //$NON-NLS-1$
				writer.write("# newdoc id=" + text + "\n"); //$NON-NLS-1$
				writer.write("# sent_id=" + match + "\n"); //$NON-NLS-1$

				id = 1; // first position in sentence
				for (i = match.getStart(); i <= match.getEnd(); i++) { // loop n times, n = mathc length

					if (lemme != null) {
						lemmestr = lemmes[iposition];
					}
					else {
						lemmestr = "_"; //$NON-NLS-1$
					}

					if (pos != null) {
						posstr = poss[iposition];
					}
					else {
						posstr = "_"; //$NON-NLS-1$
					}

					//print(""+id+"\t"+words.get(iposition)+"\t"+lemmestr+"\t_\t"+posstr+"\t_\t_\t_\t_\t_\t_\t_\t_\t_\t_\n");
					writer.write("" + id + "\t" + words[iposition] + "\t" + lemmestr + "\t_\t" + posstr + "\t_\t_\t_\t_\tXmlID=" + idsList[iposition] + "\n"); //$NON-NLS-1$

					id++; // next word
					iposition++; // nextposition
				}
			}
			writer.flush();
			writer.close();
		}

		cpb.done();
		Log.info("Done: " + texts.length + " texts and " + nTokens + " words.");
		return true;
	}

	public static void main(String[] args) throws CqiClientException, IOException, CqiServerError {

		CQPCorpus corpus = null;
		File outfile = new File("conn_export.tsv");
		String encoding = "UTF-8"; //$NON-NLS-1$

		String sentenceProperty = "s_id"; //$NON-NLS-1$
		String posProperty = "frpos"; //$NON-NLS-1$
		String lemmaProperty = "frlemma"; //$NON-NLS-1$

		String[] split = sentenceProperty.split("_", 2); //$NON-NLS-1$
		StructuralUnit s = null;//corpus.getStructuralUnit(split[0]).getProperty(split[1])
		Property word = null;//corpus.getProperty("word")
		Property lemma = null;//corpus.getProperty(lemmaProperty)
		Property pos = null;//corpus.getProperty(posProperty)

		//		if (s == null) { println "Error sentence property: $sentenceProperty"; return}
		//		if (word == null) { println "Error no word property"; return}
		//		if (pos == null) { println "Error pos property: $posProperty"; return}
		//		if (lemma == null) { println "Error lemma property: $lemmaProperty"; return}

		ToCoNLL2009 processor = new ToCoNLL2009();
		processor.process(outfile, corpus, s, word, lemma, pos, encoding);
	}
}
