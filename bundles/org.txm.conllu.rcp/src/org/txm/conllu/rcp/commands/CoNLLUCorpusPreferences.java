// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.conllu.rcp.commands;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osgi.service.prefs.Preferences;
import org.txm.conllu.core.preferences.UDPreferences;
import org.txm.rcp.swt.dialog.MultipleValueDialog;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.treesearch.preferences.TreeSearchPreferences;
import org.txm.utils.logger.Log;

/**
 * Export the conllu properties and CQP words into a conllu corpus of several files (one per text)
 * 
 * @author mdecorde.
 */
public class CoNLLUCorpusPreferences extends AbstractHandler {

	public static final String ID = CoNLLUCorpusPreferences.class.getName();


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		Object s = CorporaView.getFirstSelectedObject();

		if (!(s instanceof MainCorpus)) {
			Log.warning("Selection is not a corpus. Aborting.");
			return null;
		}

		MainCorpus c = (MainCorpus) s;

		Preferences customNode = c.getProject().getPreferencesScope().getNode(UDPreferences.getInstance().getPreferencesNodeQualifier());
		String udPrefix = customNode.get(UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));
		customNode = c.getProject().getPreferencesScope().getNode(TreeSearchPreferences.getInstance().getPreferencesNodeQualifier());
		String defaultEngine = customNode.get(TreeSearchPreferences.DEFAULT_VISUALISATION, TreeSearchPreferences.getInstance().getString(TreeSearchPreferences.DEFAULT_VISUALISATION));

		LinkedHashMap<String, String> valuesMap = new LinkedHashMap<String, String>();
		valuesMap.put(UDPreferences.UDPREFIX, udPrefix);
		valuesMap.put(TreeSearchPreferences.DEFAULT_VISUALISATION, defaultEngine);


		MultipleValueDialog multiInputddialog = new MultipleValueDialog(HandlerUtil.getActiveShell(event), "CoNLL-U Corpus preferences", valuesMap);
		if (multiInputddialog.open() == InputDialog.OK) {
			HashMap<String, String> newValues = multiInputddialog.getValues();

			customNode = c.getProject().getPreferencesScope().getNode(UDPreferences.getInstance().getPreferencesNodeQualifier());
			if (newValues.get(UDPreferences.UDPREFIX) == null || newValues.get(UDPreferences.UDPREFIX).isEmpty()) {
				Log.warning("UD prefix cannot be empty");
			}
			else {
				customNode.put(UDPreferences.UDPREFIX, newValues.get(UDPreferences.UDPREFIX));
			}

			customNode = c.getProject().getPreferencesScope().getNode(TreeSearchPreferences.getInstance().getPreferencesNodeQualifier());
			if (newValues.get(TreeSearchPreferences.DEFAULT_VISUALISATION) == null || newValues.get(TreeSearchPreferences.DEFAULT_VISUALISATION).isEmpty()) {
				Log.warning("Default tree representation cannot be empty");
			}
			else {
				customNode.put(TreeSearchPreferences.DEFAULT_VISUALISATION, newValues.get(TreeSearchPreferences.DEFAULT_VISUALISATION));
			}
		}

		//		InputDialog dialog = new InputDialog(HandlerUtil.getActiveShell(event), "CoNLL-U Corpus preferences", "Prefix of the CQP properties encoding the UD fields", udPrefix, null);
		//		
		//		if (dialog.open() == InputDialog.OK) {
		//			if (dialog.getValue().length() == 0) {
		//				
		//				return null;
		//			}
		//			customNode.put(UDPreferences.UDPREFIX, dialog.getValue());
		//			Log.info("Set "+c.getName()+" prefix to "+dialog.getValue());
		//			return s;
		//		}

		return null;
	}
}


