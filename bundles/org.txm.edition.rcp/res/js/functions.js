getNodeText = function getNodeText(node) {
	if (typeof node == 'string')
		return node;
	else if (typeof node.innerText != 'undefined')
		return node.innerText;
	else if (typeof node.InnerText != 'undefined')
		return node.InnerText;
	else if (typeof node.textContent != 'undefined')
		return node.textContent;
	else
		return null;
}
txmGetParent = function txmGetParent(node) {
	if (node == null) return;
	if (typeof node.parentNode != 'undefined')
		return node.parentNode;
	else
		return null;
}
txmGetChildren = function txmGetChildren(node) {
	if (typeof node == 'string')
		return 'STRING HTML';
	else if (typeof node.children != 'undefined')
		return node.children;
	else if (typeof node.ChildNodes != 'undefined')
		return node.ChildNodes;
	else if (typeof node.childNodes != 'undefined')
		return node.childNodes;
	else
		return null;
}
txmGetSelection = function txmGetSelection() {
	if (typeof window.getSelection != 'undefined') {
		return window.getSelection();
	} else if (typeof document.selection != 'undefined') {
		return document.selection;
	} else
		return 'NO SELECTION';
}
txmGetRangeSize = function txmGetRangeSize(selection) {
	if (typeof selection.rangeCount != 'undefined') {
		return selection.rangeCount;
	} else if (typeof selection.createRangeCollection != 'undefined') {
		return selection.createRangeCollection().length();
	} // IE5 has no multiple selection
}
txmGetRange = function txmGetRange(selection, i) {
	if (typeof selection.getRangeAt != 'undefined') {
		return selection.getRangeAt(i);
	} else if (typeof selection.createRangeCollection != 'undefined') {
		return selection.createRangeCollection().item(i);
	} else if (typeof selection.createRange != 'undefined') {
		return selection.createRange();
	} // IE5 has no multiple selection
	else
		return 'NO RANGE';
}
txmGetParentElementRange = function txmGetParentElementRange(range) {
	if (typeof range.parentElement != 'undefined') {
		return range.parentElement();
	} else if (typeof range.startContainer != 'undefined') {
		return range.startContainer.parentNode;
	} else
		return 'NO PARENT';
}
txmGetFragment = function txmGetFragment(range) {
	if (typeof range.cloneContents != 'undefined') {
		return range.cloneContents();
	} else if (typeof range.htmlText != 'undefined') {
		var node = document.createElement('sel');
		node.innerHTML = range.htmlText;
		return node;
	} else
		return 'NO FRAG';
}
txmGetTagName = function txmGetTagName(node) {
	if (typeof node.tagName != 'undefined') {
		return node.tagName;
	} else if (typeof node.nodeName != 'undefined') {
		return node.nodeName;
	} else if (typeof node.name != 'undefined') {
		return node.name;
	} else
		return 'NO TAGNAME';
}
findSpans = function findSpans(children, all) {
	for (var i = 0; i < children.length; i++) {
		var node = children.item(i);
		if (node.nodeType == 1) {
			var id = node.getAttribute('id');
			if (node.tagName == 'SPAN' && id != null && id.indexOf('w_') == 0
					&& getNodeText(node).length > 0) {
				if (id != null)
					all.push(id);
			} else {
				findSpans(txmGetChildren(node), all)
			}
		}
	}
}
findIdsInString = function findIdsInString(str, all) {
	for (var i = 0; i < children.length; i++) {
		var node = children.item(i);
		if (node.nodeType == 1) {
			var id = node.getAttribute('id');
			if (node.tagName == 'SPAN' && id != null && id.indexOf('w_') == 0
					&& getNodeText(node).length > 0) {
				if (id != null)
					all.push(id);
			} else {
				findSpans(txmGetChildren(node), all)
			}
		}
	}
}
get_type = function get_type(thing) {
	if (thing === null)
		return "[object Null]";
	return Object.prototype.toString.call(thing);
}

getDetailedObject = function getDetailedObject(inputObject) {
	var detailedObject = {};
	var properties;
	do {
		properties = Object.getOwnPropertyNames(inputObject);
		for ( var o in properties) {
			detailedObject[properties[o]] = inputObject[properties[o]];
		}
	} while (inputObject = Object.getPrototypeOf(inputObject));
	return detailedObject;
}

try {
	if ("undefined" == typeof sheet || "undefined" == typeof txmstyle) {
		txmstyle = document.createElement("style");
		txmstyle.title = "txm_btt"
		try {
			txmstyle.appendChild(document.createTextNode(""));
		} catch (e) {
		}
		
		document.body.appendChild(txmstyle);
		sheet = txmstyle.sheet;
	}
} catch (e) {
	sheet = 'error';
}

// try {

// if (!Element.prototype.scrollIntoViewIfNeeded) {
// Element.prototype.scrollIntoViewIfNeeded = Element.prototype.scrollIntoView;
// //Element.prototype.scrollIntoViewIfNeeded = function(centerIfNeeded) {
// //centerIfNeeded = arguments.length === 0 ? true : !!centerIfNeeded;
// //var parent = this.parentNode, parentComputedStyle = window
// //.getComputedStyle(parent, null), parentBorderTopWidth =
// parseInt(parentComputedStyle
// //.getPropertyValue('border-top-width')), parentBorderLeftWidth =
// parseInt(parentComputedStyle
// //.getPropertyValue('border-left-width')), overTop = this.offsetTop
// //- parent.offsetTop < parent.scrollTop, overBottom = (this.offsetTop
// //- parent.offsetTop + this.clientHeight - parentBorderTopWidth) >
// (parent.scrollTop + parent.clientHeight), overLeft = this.offsetLeft
// //- parent.offsetLeft < parent.scrollLeft, overRight = (this.offsetLeft
// //- parent.offsetLeft + this.clientWidth - parentBorderLeftWidth) >
// (parent.scrollLeft + parent.clientWidth), alignWithTop = overTop
// //&& !overBottom;
// //if ((overTop || overBottom) && centerIfNeeded) {
// //parent.scrollTop = this.offsetTop - parent.offsetTop
// //- parent.clientHeight / 2 - parentBorderTopWidth
// //+ this.clientHeight / 2;
// //}
// //if ((overLeft || overRight) && centerIfNeeded) {
// //parent.scrollLeft = this.offsetLeft - parent.offsetLeft
// //- parent.clientWidth / 2 - parentBorderLeftWidth
// //+ this.clientWidth / 2;
// //}
// //if ((overTop || overBottom || overLeft || overRight)
// //&& !centerIfNeeded) {
// //this.scrollIntoView(alignWithTop);
// //}
// //};
// }
// } catch (e) {
// Element.prototype.scrollIntoViewIfNeeded = Element.prototype.scrollIntoView
// }

showElementIfNeeded = function showElementIfNeeded(el) {
	if (el == null) return;
	if (!Element.prototype.scrollIntoViewIfNeeded) {
		if (!isAnyPartOfElementInViewport(el))
			el.scrollIntoView(false);
	} else {
		el.scrollIntoViewIfNeeded();
	}
}

isAnyPartOfElementInViewport = function isAnyPartOfElementInViewport(el) {
	try {
		if (el == null) return false;
		const rect = el.getBoundingClientRect();
		// DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108,
		// bottom: 108, left: 8 }
		const windowHeight = (window.innerHeight || document.documentElement.clientHeight);
		const windowWidth = (window.innerWidth || document.documentElement.clientWidth);

		// http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
		const vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
		const horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

		return (vertInView && horInView);
	} catch (e) {
		return false;
	}
}

return true