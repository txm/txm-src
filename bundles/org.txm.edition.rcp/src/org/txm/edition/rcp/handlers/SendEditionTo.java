// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.edition.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.osgi.util.NLS;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.links.rcp.handlers.SendSelectionToQueryable;
import org.txm.objects.Edition;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.rcp.editors.IEditionEditor;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.MatchUtils;
import org.txm.utils.logger.Log;

/**
 * Sends the selected text of an edition to compute another command.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class SendEditionTo extends SendSelectionToQueryable {

	@Override
	public Query createQuery(ExecutionEvent event, ISelection selection) {

		IEditionEditor editor = (IEditionEditor) SWTEditorsUtils.getActiveEditor(event);
		String query = null;
		if (editor instanceof SynopticEditionEditor) {
			SynopticEditionEditor seditor = (SynopticEditionEditor) editor;
			String[] wordids = seditor.getWordSelection();
			if (wordids == null) { // last resort
				query = editor.getTextSelection();
			}
			else {

				try {
					Edition edition = seditor.getEditionPanel(0).getEdition();
					Text text = edition.getText();
					Project project = text.getParent();
					String textid = edition.getText().getName();
					CQPCorpus corpus = seditor.getCorpus();// (CQPCorpus) project.getCorpusBuild(null, MainCorpus.class);
					if (corpus == null) {
						String ncorpus = edition.getStringParameterValue("corpus"); //$NON-NLS-1$
						if (ncorpus != null && ncorpus.length() > 0) {
							corpus = (CQPCorpus) project.getCorpusBuild(ncorpus);
						}
						else { // last resort
							corpus = (CQPCorpus) project.getCorpusBuild(null, MainCorpus.class);
						}
					}
					Match m = null;
					Match n = null;

					if (wordids[1] == null) {
						CQLQuery cqlQuery = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", wordids[0], CQLQuery.addBackSlash(textid))); //$NON-NLS-1$

						QueryResult rez = corpus.query(cqlQuery, "TMP", false); //$NON-NLS-1$

						if (rez.getNMatch() == 0) {
							Log.warning(NLS.bind(EditionUIMessages.WarningNoWordFoundForIdP0InTheP1Text, wordids[0], textid));
							return null;
						}
						else if (rez.getNMatch() > 1) {
							Log.warning(NLS.bind(EditionUIMessages.WarningFoundMoreThanOneWordForIdP0InTheP1Text, wordids[0], textid));
						}

						m = rez.getMatch(0);
						n = rez.getMatch(0);
					}
					else if (wordids[0] != null && wordids[1] != null) {
						CQLQuery cqlQuery = new CQLQuery(NLS.bind("[id=\"{0}\" & _.text_id=\"{1}\"]", wordids[0] + "|" + wordids[1], CQLQuery.addBackSlash(textid))); //$NON-NLS-1$ //$NON-NLS-2$
						QueryResult rez = corpus.query(cqlQuery, "TMP", false); //$NON-NLS-1$

						if (rez.getNMatch() != 2) {
							Log.warning(NLS.bind(EditionUIMessages.WarningFoundMoreThanOneWordForIdP0InTheP1Text, wordids[0] + ", " + wordids[1], textid)); //$NON-NLS-2$
							return null;
						}

						m = rez.getMatch(0);
						n = rez.getMatch(1);
					}

					int[] positions = MatchUtils.toPositions(m, n);
					if (positions.length > 0) {
						WordProperty p = corpus.getProperty("word"); //$NON-NLS-1$
						String[] words = CQPSearchEngine.getCqiClient().cpos2Str(p.getQualifiedName(), positions);
						if (words != null && words.length > 0) {
							query = ""; //$NON-NLS-1$
							for (String w : words) {
								query += " \"" + CQLQuery.addBackSlash(w) + "\""; //$NON-NLS-1$ //$NON-NLS-2$
							}
							query = query.substring(1);
						}
					}
				}
				catch (Exception e) {
					Log.warning(NLS.bind(EditionUIMessages.ErrorP0, e));
					Log.printStackTrace(e);
				}
			}
			return new CQLQuery(query);
		}
		else { // try using text selection of the current editor
			query = editor.getTextSelection();
			query = query.replaceAll("\n", "").trim(); //$NON-NLS-1$ //$NON-NLS-2$
			query = "\"" + CQLQuery.addBackSlash(query) + "\""; //$NON-NLS-1$ //$NON-NLS-2$
			return new CQLQuery(query);
		}
	}

	@Override
	public TXMResult getResultParent(ExecutionEvent event, String id) {
		return ((IEditionEditor) SWTEditorsUtils.getActiveEditor(event)).getCorpus();
	}
}
