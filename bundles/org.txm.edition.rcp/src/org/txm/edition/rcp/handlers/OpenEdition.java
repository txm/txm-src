// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.edition.rcp.handlers;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.edition.rcp.editors.SynopticEditorInput;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.logger.Log;

/**
 * Open a text edition from: a main corpus : the first text of the maincorpus a
 * sub-corpus : the first text of the subcorpus a partition : the user must
 * choose one of the text used in the partition @ author mdecorde.
 */
public class OpenEdition extends AbstractHandler {

	/** The Constant ID. */
	public final static String ID = "org.txm.synopticedition.rcp.handlers.OpenEdition"; //$NON-NLS-1$

	/** The lastopenedfile. */
	public static String lastopenedfile;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection isel = HandlerUtil.getCurrentSelection(event);

		if (isel == null) return null;
		if (!(isel instanceof IStructuredSelection)) return null;

		IStructuredSelection selection = (IStructuredSelection) isel;
		if (selection.getFirstElement() instanceof CQPCorpus) {
			try {
				CQPCorpus c = (CQPCorpus) selection.getFirstElement();
				// Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

				// EditionSelectorDialog d = new EditionSelectorDialog(shell, c, null);
				SynopticEditionEditor editor = null;
				// if (c.getEditionNames().size() > 1 && d.open() == Window.OK) {
				// Object[] rez = d.getResult();
				// String[] rezS = new String[rez.length];
				// for (int i =0 ; i < rez.length ; i++) rezS[i] = rez[i].toString();
				// editor = openEdition(c, rezS);
				// } else
				String[] editionsToOpen = getDefaultEditions(c);

				if (editionsToOpen.length == 0) { // the defaultEdition parameter is not set
					List<String> editionNames = c.getProject().getEditionNames();
					if (editionNames.size() > 0) { // use the first edition declared
						editionsToOpen = new String[1];
						editionsToOpen[0] = editionNames.get(0);
						Log.fine(NLS.bind(EditionUIMessages.openingWithFirstEditionNoDefaultEditionSetColonP0, editionNames.get(0)));
					}
					else { // no edition in the corpus
						Log.warning(EditionUIMessages.noEditionToOpen);
						editionsToOpen = new String[0];
						return false;
					}
				}
				else {
					Log.fine(NLS.bind(EditionUIMessages.openingDefaultEditionsColonP0, StringUtils.join(editionsToOpen, ", "))); //$NON-NLS-1$
				}

				editor = openEdition(c, editionsToOpen);

				// if (editor != null)
				// editor.firstPage();
			}
			catch (Exception e) {
				Log.severe(NLS.bind(EditionUIMessages.errorWhileOpeningEditionColonP0, e));
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		else {
			Log.warning(EditionUIMessages.abortingColonSelectionIsNotACorpus);
		}
		return null;
	}

	public static String[] getDefaultEditions(Project p) {
		if (p == null) return new String[0];

		return p.getDefaultEditionName().split(","); //$NON-NLS-1$
	}

	public static String[] getDefaultEditions(CQPCorpus c) {
		return getDefaultEditions(c.getProject());
	}

	/**
	 * Open edition, but don't show a page
	 *
	 * @param corpus the corpus
	 * @return the object
	 */
	public static SynopticEditionEditor openEdition(CQPCorpus corpus) {
		return openEdition(corpus, getDefaultEditions(corpus));
	}

	/**
	 * Open edition, but don't show a page
	 *
	 * @param corpus the corpus
	 * @return the object
	 */
	public static SynopticEditionEditor openEdition(CQPCorpus corpus, String[] editions) {
		return openEdition(corpus, Arrays.asList(editions));
	}

	/**
	 * Open edition, but don't show a page
	 *
	 * @param corpus the corpus
	 * @return the object
	 */
	public static SynopticEditionEditor openEdition(CQPCorpus corpus, List<String> editions) {
		return openEdition(corpus, editions, null, null, null);
	}

	/**
	 * Open edition, but don't show a page
	 *
	 * @param corpus the corpus
	 * @return the object
	 */
	public static SynopticEditionEditor openEdition(CQPCorpus corpus, String[] editions, String textId) {
		return openEdition(corpus, Arrays.asList(editions), textId, null, null);
	}

	/**
	 * Open edition, but don't show a page
	 *
	 * @param corpus the corpus
	 * @param editions the editions to show
	 * @param textId the ID of the Text to show
	 * @param pageId the ID of the page to show. textId must be specified to work
	 * @param wordIds the wordId of the word to focus&show. textId must be specified to work
	 * @return the SynopticEditionEditor (not shown yet)
	 */
	public static SynopticEditionEditor openEdition(CQPCorpus corpus, List<String> editions, String textId, String pageId, String wordIds) {

		Log.fine("Opening edition of " + corpus); //$NON-NLS-1$
		try {
			corpus.compute(false);

			String[] availables = corpus.getCorpusTextIdsList();
			if (availables.length == 0) {
				Log.warning(NLS.bind(EditionUIMessages.ErrorNoTextAvailableInTheP0Corpus, corpus));
				return null;
			}
			Text text = corpus.getProject().getText(availables[0]);
			if (text == null) {
				Log.warning(NLS.bind(EditionUIMessages.ErrorNoP0TextInTheP1Corpus, availables[0], corpus));
				return null;
			}
			// QueryResult result = corpus.query(new Query("<text> []"), "get_edition", false); //$NON-NLS-1$ //$NON-NLS-2$
			// StructuralUnit text_su = corpus.getStructuralUnit("text"); //$NON-NLS-1$
			// Property text_id = text_su.getProperty("id"); //$NON-NLS-1$

			// if (result.getNMatch() > 0) textid = result.getMatches(0, 1).get(0).getValueForProperty(text_id);
			// MainCorpus maincorpus = corpus.getMainCorpus();

			// if (textid != null) text = maincorpus.getText(textid);

			// String[] editions = {maincorpus.getDefaultEdition()};
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			IWorkbenchPage page = window.getActivePage();

			SynopticEditorInput editorInput = new SynopticEditorInput(text, corpus, editions, textId, pageId, wordIds);
			SynopticEditionEditor editor = (SynopticEditionEditor) page.findEditor(editorInput);
			if (editor == null) {
				editor = (SynopticEditionEditor) SWTEditorsUtils.openEditor(editorInput, SynopticEditionEditor.ID, true);
			}
			else {
				if (editions == null) {
					String[] defaultEditions = corpus.getProject().getDefaultEditionName().split(","); //$NON-NLS-1$
					editions = Arrays.asList(defaultEditions);
				}
				editor.removeHighlightWords();
				editor.openEditions(editions, textId, null, wordIds);
				editor.getSite().getPage().activate(editor);
			}

			return editor;

		}
		catch (Exception e) {
			Log.severe(NLS.bind(EditionUIMessages.errorWhileOpeningEditorColonP0, e));
			Log.printStackTrace(e);
			return null;
		}
	}


	/**
	 * Computes the needed type of split position according to the source editor dimensions.
	 *
	 * @param sourceEditor the editor from which the edition opening is called
	 * @return the EModelService.ABOVE if the editor size is horizontal or EModelService.RIGHT_OF if the editor size is vertical
	 */
	public static int computeSplitRatioPosition(TXMEditor sourceEditor) {
		int position = EModelService.RIGHT_OF;
		Point s = sourceEditor.getParent().getSize();
		if (s.x < s.y) {
			position = EModelService.ABOVE;
		}
		return position;
	}

}
