// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.edition.rcp.handlers;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.handlers.BackToTextCommand;
import org.txm.edition.rcp.editors.RGBA;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.edition.rcp.preferences.SynopticEditionPreferences;
import org.txm.objects.Match;
import org.txm.objects.Text;
import org.txm.rcp.StatusLine;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.utils.logger.Log;
import org.txm.utils.messages.Utf8NLS;

/**
 * Open a text edition from: a concordance Line
 *
 * Or Open a text edition from command parameters see plugin.xml
 *
 * @author mdecorde.
 */
public class BackToText extends BackToTextCommand {

	/** The Constant ID. */
	public final static String ID = "org.txm.synopticedition.rcp.handlers.BackToText"; //$NON-NLS-1$

	public final static RGBA lightred = new RGBA(249, 208, 208);

	public final static RGBA red = new RGBA(249, 160, 160);

	public final static RGBA lightblue = new RGBA(208, 208, 249);

	public final static RGBA blue = new RGBA(130, 130, 249);

	/**
	 * stores previous highlights to clean styling
	 */
	private static HashMap<ConcordanceEditor, List<String>> associatedEditorsPreviousOtherWordIDs = new HashMap<>();

	/**
	 * stores previous highlights to clean styling
	 */
	private static HashMap<ConcordanceEditor, List<String>> associatedEditorsPreviousLineids = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String wordid = event.getParameter("org.txm.edition.rcp.handlers.BackToText.wordid"); //$NON-NLS-1$
		String textid = event.getParameter("org.txm.edition.rcp.handlers.BackToText.textid"); //$NON-NLS-1$
		String editionid = event.getParameter("org.txm.edition.rcp.handlers.BackToText.editionid"); //$NON-NLS-1$
		String corpusid = event.getParameter("org.txm.edition.rcp.handlers.BackToText.corpusid"); //$NON-NLS-1$
		if (corpusid != null) { // minimal parameter is the corpus id
			try {
				IWorkbenchWindow wb = HandlerUtil.getActiveWorkbenchWindow(event);
				IEditorPart ieditor = wb.getActivePage().getActiveEditor();

				if (!backToText(ieditor, corpusid, Arrays.asList(editionid.split("||")), textid, wordid)) { //$NON-NLS-1$
					Log.severe(Utf8NLS.bind(EditionUIMessages.CouldNotOpenEditionForWordIdP0TextIdP1AndEditionP2, wordid, textid, editionid));
				}
			}
			catch (Exception e) {
				Log.severe(e);
				Log.printStackTrace(e);
			}
			return null;
		}

		IWorkbenchWindow wb = HandlerUtil.getActiveWorkbenchWindow(event);
		IEditorPart ieditor = wb.getActivePage().getActiveEditor();
		if (!(ieditor instanceof ConcordanceEditor)) {
			return null;
		}
		ConcordanceEditor concordanceEditor = (ConcordanceEditor) ieditor;
		// FIXME: SJ: new version
		concordanceEditor.backToText();
		// FIXME: SJ: old version
		// ISelection sel = wb.getSelectionService().getSelection();
		// if (sel instanceof IStructuredSelection) {
		// IStructuredSelection selection = (IStructuredSelection) sel;
		// if (selection.getFirstElement() instanceof Match) {
		// backToText(concordanceEditor, (Match)(selection.getFirstElement()));
		// } else {
		// System.out.println(SynopticEditionUIMessages.BackToText_1+selection);
		// }
		// } else {
		// System.out.println(SynopticEditionUIMessages.BackToText_2);
		// }
		return null;
	}

	private boolean backToText(IEditorPart ieditor, String corpusid, List<String> editions, String textid, String wordid) throws Exception {
		MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus(corpusid);

		if (corpus == null) {
			Log.warning(NLS.bind(EditionUIMessages.ErrorNoCorpusWithIDP0, corpusid));
		}

		if (editions != null && !corpus.getProject().getBuiltEditionNames().containsAll(editions)) {
			Log.warning(NLS.bind(EditionUIMessages.WarningCorpusHasNoEditionNamedP0, editions));
			editions = Arrays.asList("default"); //$NON-NLS-1$
		}

		if (!corpus.getProject().getBuiltEditionNames().containsAll(editions)) {
			Log.warning(EditionUIMessages.ErrorCorpusHasNoDefaultEdition);
			Log.warning(NLS.bind(EditionUIMessages.AvailablesP0, corpus.getProject().getEditionNames()));
			return false;
		}

		SynopticEditionEditor editor = null;
		//boolean newEditor = false;
		if (ieditor != null && ieditor instanceof SynopticEditionEditor) {
			editor = (SynopticEditionEditor) ieditor;
		}
		else {
			editor = OpenEdition.openEdition(corpus, editions);
			//newEditor = true;
		}

		Text text = null;
		if (textid != null) {
			text = corpus.getProject().getText(textid);
			if (text == null) {
				Log.warning(NLS.bind(EditionUIMessages.ErrorCorpusHasNoTextWithIDP0, textid));
				return false;
			}
		}
		else {
			text = corpus.getProject().getText(corpus.getCorpusTextIdsList()[0]);
			if (text == null) {
				Log.warning(EditionUIMessages.ErrorCorpusHasNoText);
				return false;
			}
		}

		editor.backToText(text, wordid);
		if (wordid != null) {
			editor.removeHighlightWords();
			editor.addHighlightWordsById(red, wordid);
		}
		editor.updateWordStyles();
		return false;
	}

	/**
	 * Open edition.
	 *
	 * @return the object
	 */
	@Override
	public boolean backToText(ConcordanceEditor editor, Match match) {
		Concordance conc = editor.getResult();
		CQPCorpus corpus = editor.getCorpus();
		//SearchEngine se = conc.getQuery().getSearchEngine();

		try {
			if (match == null) return false;

			StructuralUnit textS = null;
			String textid = ""; //$NON-NLS-1$
			String baseid = ""; //$NON-NLS-1$

			Property xProperty = corpus.getProperty("x"), yProperty, wProperty, hProperty; //$NON-NLS-1$
			String x = "0", y = "0", w = "0", h = "0"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			int[] pos = null;
			if (xProperty != null) {
				yProperty = corpus.getProperty("y"); //$NON-NLS-1$
				if (yProperty != null) {
					wProperty = corpus.getProperty("w"); //$NON-NLS-1$
					if (wProperty != null) {
						AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
						int[] cpos = { match.getStart() };
						hProperty = corpus.getProperty("h"); //$NON-NLS-1$
						if (hProperty != null) {
							x = CQI.cpos2Str(xProperty.getQualifiedName(), cpos)[0];
							y = CQI.cpos2Str(yProperty.getQualifiedName(), cpos)[0];
							w = CQI.cpos2Str(wProperty.getQualifiedName(), cpos)[0];
							h = CQI.cpos2Str(hProperty.getQualifiedName(), cpos)[0];

							pos = new int[4];
							pos[0] = Integer.parseInt(x);
							pos[1] = Integer.parseInt(y);
							pos[2] = Integer.parseInt(w);
							pos[3] = Integer.parseInt(h);
						}
					}
				}
			}

			String projectid = ""; //$NON-NLS-1$
			try {
				textS = corpus.getStructuralUnit("text"); //$NON-NLS-1$
				Property textP = textS.getProperty("id"); //$NON-NLS-1$
				textid = org.txm.searchengine.cqp.corpus.query.Match.getValueForProperty(textP, match.getStart());
			}
			catch (Exception e2) {
				Log.severe(NLS.bind("The corpus has no text_id property {0}.", e2));
				org.txm.utils.logger.Log.printStackTrace(e2);
				return false;
			}

			Log.fine("Loading line informations..."); //$NON-NLS-1$

			SynopticEditionEditor l_editor = editor.getLinkedEditor(SynopticEditionEditor.class);
			SynopticEditionEditor attachedBrowserEditor = null;
			if (l_editor != null) {
				attachedBrowserEditor = l_editor;// associatedEditors.get(editor);
			}

			if (attachedBrowserEditor != null && attachedBrowserEditor.isDisposed()) {
				associatedEditorsPreviousOtherWordIDs.put(editor, new ArrayList<String>());
				associatedEditorsPreviousLineids.put(editor, new ArrayList<String>());
			}
			List<String> previousOtherWordIDs = associatedEditorsPreviousOtherWordIDs.get(editor);
			List<String> previousLineids = associatedEditorsPreviousLineids.get(editor);

			if (attachedBrowserEditor == null || attachedBrowserEditor.isDisposed()) {
				String[] editionsToOpen = OpenEdition.getDefaultEditions(corpus);

				if (editionsToOpen.length == 0) { // the defaultEdition parameter is not set
					List<String> editionNames = corpus.getProject().getEditionNames();
					if (editionNames.size() > 0) { // use the first edition declared
						editionsToOpen = new String[1];
						editionsToOpen[0] = editionNames.get(0);
						Log.info(NLS.bind(EditionUIMessages.openingWithFirstEditionNoDefaultEditionSetColonP0, editionNames.get(0)));
					}
					else { // no edition in the corpus
						Log.info(EditionUIMessages.noEditionToOpen);
						editionsToOpen = new String[0];
						return false;
					}
				}
				else {
					Log.fine(NLS.bind(EditionUIMessages.openingDefaultEditionsColonP0, StringUtils.join(editionsToOpen, ", "))); //$NON-NLS-1$
				}

				attachedBrowserEditor = OpenEdition.openEdition(corpus, editionsToOpen);
				editor.addLinkedEditor(attachedBrowserEditor);

				int position = SynopticEditionPreferences.getInstance().getInt(SynopticEditionPreferences.BACKTOTEXT_POSITION);
				// manage the RATIO split position Edition preference
				if (position == -2) {
					position = OpenEdition.computeSplitRatioPosition(editor);
				}
				if (editor != null && attachedBrowserEditor != null && position >= 0) {
					SWTEditorsUtils.moveEditor(editor, attachedBrowserEditor, position);
				}
			}

			// remove previous highlighted words
			if (previousOtherWordIDs != null) {
				// System.out.println("remove previousOtherWordIDs="+previousOtherWordIDs);
				attachedBrowserEditor.removeHighlightWordsById(lightred, previousOtherWordIDs);
				associatedEditorsPreviousOtherWordIDs.remove(editor); // free mem
			}
			if (previousLineids != null) {
				// System.out.println("remove previousLineids="+previousLineids);
				attachedBrowserEditor.removeHighlightWordsById(red, previousLineids);
				associatedEditorsPreviousLineids.remove(editor); // free mem
			}

			// System.out.println("getKeywordsId from "+editor.getTopLine()+" to "+editor.getBottomLine() + 1);
			List<String> otherWordIDs = conc.getKeywordsId(textid, editor.getTopLine(), editor.getBottomLine() + 1);
			// System.out.println("getKeywordsId="+otherWordIDs);
			List<String> lineids = null;
			try {
				lineids = org.txm.searchengine.cqp.corpus.query.Match.getValuesForProperty(corpus.getProperty("id"), match); //$NON-NLS-1$
			}
			catch (Exception e1) {
				org.txm.utils.logger.Log.printStackTrace(e1);
				return false;
			}
			Log.fine(NLS.bind("Project id {0} Corpus (Base) id {1} Text id {2}", new Object[] { projectid, baseid, textid })); //$NON-NLS-1$

			otherWordIDs.removeAll(lineids); // remove the pivot from others words

			Text text = corpus.getProject().getText(textid);
			if (text == null) {
				StatusLine.setMessage(NLS.bind("BackToText: Text {0} is missing", textid)); //$NON-NLS-1$
				Log.severe(NLS.bind("BackToText: Text {0} is missing", textid)); //$NON-NLS-1$
				return false;
			}

			String line_wordid = lineids.get(0);
			if (line_wordid == null) {
				Log.severe(EditionUIMessages.errorColonCouldNotRetrieveLineWordIdsAbortingBackToText);
				return false;
			}


			String focusid = null;
			if (lineids.size() > 0) focusid = lineids.get(0);

			// System.out.println("add otherWordIDs="+otherWordIDs);
			attachedBrowserEditor.addHighlightWordsById(lightred, otherWordIDs);

			// System.out.println("add linesids="+lineids);
			attachedBrowserEditor.addHighlightWordsById(red, lineids);

			associatedEditorsPreviousOtherWordIDs.put(editor, otherWordIDs);
			associatedEditorsPreviousLineids.put(editor, lineids);

			HashSet<int[]> posses = new HashSet<>();
			posses.add(pos);
			attachedBrowserEditor.setHighlightedArea(posses);

			attachedBrowserEditor.setFocusedWordID(focusid);

			// reload the page and styles
			attachedBrowserEditor.backToText(text, line_wordid);// open the page(S) containing the word with id=line_wordid

			IWorkbenchPage attachedPage = attachedBrowserEditor.getEditorSite().getPage();
			attachedPage.bringToTop(attachedBrowserEditor);

			return true;

		}
		catch (Exception err) {
			Log.severe(NLS.bind("** Text display failed: {0}", err)); //$NON-NLS-1$
			Log.printStackTrace(err);
		}
		return false;
	}
}
