package org.txm.edition.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Edition UI messages.
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
public class EditionUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.edition.rcp.messages.messages"; //$NON-NLS-1$

	public static String openingWithFirstEditionNoDefaultEditionSetColonP0;

	public static String errorColonCouldNotRetrieveLineWordIdsAbortingBackToText;

	public static String noEditionToOpen;

	public static String openingDefaultEditionsColonP0;

	public static String editions;

	public static String selectOneOrMoreEditions;

	public static String controls;

	public static String CommandLink_empty;

	public static String CouldNotOpenEditionForWordIdP0TextIdP1AndEditionP2;

	public static String pageButtons;

	public static String textButtons;


	public static String warningColonUnknownedParameterIdEqualsP0;

	public static String error_while_readingP0P1;

	public static String back;

	public static String copyTextSelection;

	public static String forward;

	public static String reload;

	public static String errorWhileOpeningEditorColonP0;

	public static String errorWhileOpeningEditionColonP0;

	public static String abortingColonSelectionIsNotACorpus;

	public static String AvailablesP0;

	public static String BrowserEvaluateErrorP0;

	public static String BrowserExecuteErrorP0;

	public static String firstTextOfTheCorpus;

	public static String previousText;

	public static String nextText;

	public static String lastTextOfTheCorpus;

	public static String firstPageOfTheText;

	public static String previousPage;

	public static String errorWhileGettingAvailableEditionsColonP0;

	public static String nextPage;

	public static String lastPageOfTheText;

	public static String warningColonCouldNotFindEditionForNameEqualsP0;

	public static String noEditionWithNameEqualsP0AvailableForTextEqualsP1;

	public static String failToRetrieveFirstPageOfEditionEqualsP0AndTextEqualsP1;

	public static String pageNotFoundP0;

	public static String sCRIPTRETURNEDEqualsP0;

	public static String enableFastWordHighlight;

	public static String edition;

	public static String errorColonTxmeditioncorpusidEditionidTextidWordidCalledWithNoArguments;

	public static String EditionLink_1;

	public static String ErrorCorpusHasNoDefaultEdition;

	public static String ErrorCorpusHasNoText;

	public static String ErrorCorpusHasNoTextWithIDP0;

	public static String ErrorNoCorpusWithIDP0;

	public static String ErrorNoP0TextInTheP1Corpus;

	public static String ErrorNoTextAvailableInTheP0Corpus;


	public static String ErrorNoTextFoundWithTheP0Id;

	public static String ErrorP0;

	public static String ErrorP0IsNotReadyToUse;

	public static String InitialZoom;

	public static String LinkedEditorOpeningPosition;

	public static String P0NotFound;

	public static String OpeningConcordancesOfQueryP0;

	public static String P0Hits;

	public static String text;

	public static String page;

	public static String Search;

	public static String SearchingForP0InTheP1Corpus;

	public static String TXMCouldNotOpenTheSWTInternalBrowserP0;

	public static String WarningCorpusHasNoEditionNamedP0;

	public static String WarningFoundMoreThanOneWordForIdP0InTheP1Text;

	public static String WarningNoWordFoundForIdP0InTheP1Text;

	public static String ZoomIn;

	public static String ZoomOut;

	public static String preference_split_position_Above;
	public static String preference_split_position_Below;
	public static String preference_split_position_Left;
	public static String preference_split_position_Right;
	public static String preference_split_position_Over;
	public static String preference_split_position_Ratio;
	public static String preference_split_position_Description;

	public static String determinesThePositionOfTheNewWindow;




	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, EditionUIMessages.class);
	}

	private EditionUIMessages() {
	}
}
