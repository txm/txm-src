package org.txm.edition.rcp.editors;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.objects.Project;

public class EditionSelectorDialog extends ElementListSelectionDialog {

	public EditionSelectorDialog(Shell parent, Project project, List<String> defaultSelection) {
		super(parent, new ILabelProvider() {

			@Override
			public void removeListener(ILabelProviderListener listener) {
			}

			@Override
			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			@Override
			public void dispose() {
			}

			@Override
			public void addListener(ILabelProviderListener listener) {
			}

			@Override
			public Image getImage(Object element) {
				return null;
			}

			@Override
			public String getText(Object element) {
				return element.toString();
			}
		});

		setTitle(EditionUIMessages.editions);
		setMessage(EditionUIMessages.selectOneOrMoreEditions);
		setMultipleSelection(true);

		setElements(project.getBuiltEditionNames().toArray());
		setFilter("*"); //$NON-NLS-1$

		if (defaultSelection == null) {
			defaultSelection = Arrays.asList(project.getDefaultEditionName());
		}
		this.setInitialSelections(defaultSelection.toArray());
	}
}
