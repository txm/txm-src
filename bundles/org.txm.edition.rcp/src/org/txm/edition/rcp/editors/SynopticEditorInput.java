package org.txm.edition.rcp.editors;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.objects.Text;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

public class SynopticEditorInput extends TXMResultEditorInput<Text> implements IEditorInput {

	private CQPCorpus corpus;

	private List<String> editions;

	private String textId, pageId, wordId;

	/**
	 * Instantiates a new SynopticEditorInput
	 *
	 * @param corpus the corpus
	 * @param text the text to show
	 * @param editions the editions to show
	 */
	public SynopticEditorInput(Text text, CQPCorpus corpus, List<String> editions, String textId, String pageId, String wordIds) {
		super(text);

		this.corpus = corpus;
		this.editions = editions;
		this.textId = textId;
		this.pageId = pageId;
		this.wordId = wordIds;

		if (editions == null) {
			String[] defaultEditions = corpus.getProject().getDefaultEditionName().split(","); //$NON-NLS-1$
			this.editions = Arrays.asList(defaultEditions);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		return AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, IImageKeys.EDITION);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		return corpus.getName() + " - " + getResult() + " - " + editions;  //$NON-NLS-1$ //$NON-NLS-2$
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		return getName();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class adapter) {
		return null;
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return corpus;
	}

	public String getTextID() {
		return textId;
	}

	public String getPageID() {
		return pageId;
	}

	public String getWordId() {
		return wordId;
	}

	public Text getText() {
		return getResult();
	}

	public List<String> getEditions() {
		return editions;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		return false;
	}
}
