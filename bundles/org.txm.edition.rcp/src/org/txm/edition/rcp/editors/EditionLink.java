package org.txm.edition.rcp.editors;

import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.handlers.BackToText;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.rcp.editors.CommandLink;
import org.txm.rcp.editors.TXMEditor;
import org.txm.utils.logger.Log;

/**
 * Browser Javascript method to open a TXM edition given 1 to 4 parameters : corpusid, editionid, textid, wordid
 * 
 * @author mdecorde
 *
 */
public class EditionLink extends BrowserFunction {

	String data = ""; //$NON-NLS-1$

	Browser browser;

	TXMEditor<? extends TXMResult> synopticEditionEditor;

	public static final String FCT = "txmedition"; //$NON-NLS-1$

	EditionLink(TXMEditor<? extends TXMResult> synopticEditionEditor, Browser browser) {
		super(browser, FCT); //$NON-NLS-1$
		this.browser = browser;
		this.synopticEditionEditor = synopticEditionEditor;
	}

	@Override
	public Object function(Object[] arguments) { // corpus id, edition id, text id, word id

		Log.finer(EditionUIMessages.EditionLink_1 + Arrays.toString(arguments));

		HashMap<String, String> params = new HashMap<>();

		String id = BackToText.class.getName();

		if (arguments.length == 4) {
			params.put(id + ".corpusid", arguments[0].toString()); //$NON-NLS-1$
			params.put(id + ".editionid", arguments[1].toString()); //$NON-NLS-1$
			params.put(id + ".textid", arguments[2].toString()); //$NON-NLS-1$
			params.put(id + ".wordid", arguments[3].toString()); //$NON-NLS-1$
		}
		else if (arguments.length == 3) {
			params.put(id + ".corpusid", arguments[0].toString()); //$NON-NLS-1$
			params.put(id + ".editionid", arguments[1].toString()); //$NON-NLS-1$
			params.put(id + ".textid", arguments[2].toString()); //$NON-NLS-1$
			params.put(id + ".wordid", null); //$NON-NLS-1$
		}
		else if (arguments.length == 2) {
			params.put(id + ".corpusid", arguments[0].toString()); //$NON-NLS-1$
			params.put(id + ".editionid", arguments[1].toString()); //$NON-NLS-1$
			params.put(id + ".textid", null); //$NON-NLS-1$
			params.put(id + ".wordid", null); //$NON-NLS-1$
		}
		else if (arguments.length == 1) {
			params.put(id + ".corpusid", arguments[0].toString()); //$NON-NLS-1$
			params.put(id + ".editionid", null); //$NON-NLS-1$
			params.put(id + ".textid", null); //$NON-NLS-1$
			params.put(id + ".wordid", null); //$NON-NLS-1$
		}
		else {
			System.out.println(EditionUIMessages.errorColonTxmeditioncorpusidEditionidTextidWordidCalledWithNoArguments);
			return null;
		}

		return CommandLink.callTXMCommand(id, params);
	}
}
