package org.txm.edition.rcp.editors;

import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.objects.Match;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.swt.widget.AssistedChoiceQueryWidget;
import org.txm.rcp.swt.widget.NewNavigationWidget;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.MatchUtils;
import org.txm.utils.logger.Log;

public class SearchEditionToolbar {

	private GridData searchGdata;

	private GridData searchGdata2;

	private AssistedChoiceQueryWidget searchText;

	/**
	 * search result
	 */
	protected Selection searchResult;

	/**
	 * search result index
	 */
	protected int searchIndex;

	private NewNavigationWidget searchNavigation;

	Composite lowerControlsArea;

	CQPCorpus mcorpus;

	SynopticEditionEditor editor;

	private List<String> foundWordids;

	private List<String> selectedWordid;

	private Button searchButton;

	private Button endSearchButton;

	private GridData searchButtonGdata;

	private GridData concordanceButtonGdata;

	private Button concordanceButton;

	private RGBA selectionColor;

	private RGBA highlightColor;

	public void init(Composite lowerControlsArea, CQPCorpus mcorpus, SynopticEditionEditor editor, RGBA selectionColor, RGBA highlightColor) {

		this.lowerControlsArea = lowerControlsArea;
		this.mcorpus = mcorpus;
		this.editor = editor;

		this.selectionColor = selectionColor;
		this.highlightColor = highlightColor;

		endSearchButton = new Button(lowerControlsArea, SWT.PUSH);
		endSearchButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
		endSearchButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (searchGdata.widthHint > 0) {
					closeSearch();
				}
				else {
					openSearch(editor.getTextSelection());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		GridData searchGdata3 = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		// searchGdata3.minimumWidth = searchGdata3.widthHint = 0;
		endSearchButton.setLayoutData(searchGdata3);

		searchText = new AssistedChoiceQueryWidget(lowerControlsArea, SWT.NONE, mcorpus);
		searchGdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		searchGdata.minimumWidth = searchGdata.widthHint = 0;
		searchText.setLayoutData(searchGdata);
		searchText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {

					if (searchText.getQuery() != null) {
						search();
					}
				}
				else if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {

					if (e.keyCode == 'g') {

						if ((e.stateMask & SWT.SHIFT) == SWT.SHIFT) {

							backward();
						}
						else {
							forward();
						}
					}
				}
				else if (e.keyCode == SWT.ESC) {
					closeSearch();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		searchButton = new Button(lowerControlsArea, SWT.PUSH);
		searchButton.setImage(IImageKeys.getImage(IImageKeys.START));
		searchButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (searchText.getQuery() != null) {
					search();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		searchButtonGdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		searchButtonGdata.minimumWidth = searchButtonGdata.widthHint = 0;
		searchButton.setLayoutData(searchButtonGdata);

		searchNavigation = new NewNavigationWidget(lowerControlsArea, SWT.NONE) {

			@Override
			public void refresh() {
				super.refresh();
				int s = this.getCurrentPosition();
				if (s == searchIndex) {
					return; // no need to update
				}
				searchIndex = this.getCurrentPosition();
				showCurrentSearchMatch();
				// Log.info("showing the " + index + " match.");
			}
		};
		searchGdata2 = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		searchGdata2.minimumWidth = searchGdata2.widthHint = 0;
		searchNavigation.setLayoutData(searchGdata2);
		searchNavigation.setEnabled(false);

		//		concordanceButton = new Button(lowerControlsArea, SWT.PUSH);
		//		concordanceButton.setImage(IImageKeys.getImage("org.txm.concordance.rcp", IImageKeys.ACTION_CONCORDANCES));
		//		concordanceButton.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				if (searchText.getQuery() != null) {
		//					openConcordance();
		//				}
		//			}
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {}
		//		});
		//		concordanceButtonGdata = new GridData(GridData.CENTER, GridData.CENTER, false, false);
		//		concordanceButtonGdata.minimumWidth = concordanceButtonGdata.widthHint = 0;
		//		concordanceButton.setLayoutData(concordanceButtonGdata);
	}

	protected void forward() {
		if (searchResult == null) return;

		searchNavigation.setCurrentPosition(searchNavigation.getCurrentPosition() + 1);
		searchNavigation.refresh();
	}

	protected void backward() {
		if (searchResult == null) return;

		searchNavigation.setCurrentPosition(searchNavigation.getCurrentPosition() - 1);
		searchNavigation.refresh();
	}

	protected void openConcordance() {
		Log.info(NLS.bind(EditionUIMessages.OpeningConcordancesOfQueryP0, this.searchText.getQuery()));
	}

	protected void closeSearch() {

		clearSearch();

		// hide controls
		searchGdata.minimumWidth = searchGdata.widthHint = 0;
		searchGdata2.minimumWidth = searchGdata2.widthHint = 0;
		searchButtonGdata.minimumWidth = searchButtonGdata.widthHint = 0;
		if (concordanceButtonGdata != null) concordanceButtonGdata.minimumWidth = concordanceButtonGdata.widthHint = 0;
		lowerControlsArea.layout();
		endSearchButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
	}

	/**
	 * Open the search tool if not done
	 */
	public void openSearch(String value) {
		if (searchGdata.widthHint == 0) {
			searchGdata.minimumWidth = searchGdata.widthHint = 350; //
			searchGdata.minimumWidth = searchGdata.widthHint = 350; //
			searchGdata2.minimumWidth = searchGdata2.widthHint = SWT.DEFAULT; // several buttons
			searchButtonGdata.minimumWidth = searchButtonGdata.widthHint = SWT.DEFAULT; // button
			if (concordanceButtonGdata != null) concordanceButtonGdata.minimumWidth = concordanceButtonGdata.widthHint = SWT.DEFAULT; // button
			lowerControlsArea.layout();
			endSearchButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
		}

		if (value != null && value.length() > 0) {
			searchText.setText(value);
		}
		searchText.setFocus();
	}

	protected void search() {
		Log.info(NLS.bind(EditionUIMessages.SearchingForP0InTheP1Corpus, searchText.getQuery(), mcorpus.getName()));
		IQuery q = searchText.getQuery();
		try {

			Selection tmp = searchText.getQuery().getSearchEngine().query(mcorpus, q, "SEARCH", false); //$NON-NLS-1$
			if (tmp.getNMatch() > 0) {
				searchResult = tmp;

				searchText.memorize();

				searchIndex = 0;
				String wordid = editor.getEditionPanel(0).getCurrentPage().getWordId();
				String text = editor.getEditionPanel(0).getCurrentPage().getEdition().getText().getName();
				if (wordid != null && text != null) {
					QueryResult rez2 = mcorpus.query(new CQLQuery("[_.text_id=\"" + CQLQuery.addBackSlash(text) + "\" & id=\"" + wordid + "\"]"), "SEARCHINIT", false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					if (rez2.getNMatch() > 0) {
						int p = rez2.getMatch(0).getStart();
						List<? extends Match> matches = searchResult.getMatches();
						for (int i = 0; i < matches.size(); i++) {
							if (p < matches.get(i).getStart()) {
								searchIndex = i; // previous index was the one
								break;
							}
						}
					}
				}
				searchNavigation.setEnabled(true);
				if (concordanceButton != null) concordanceButton.setEnabled(true);
				searchNavigation.setCurrentPosition(searchIndex);
				searchNavigation.setMinPosition(1);
				searchNavigation.setMaxPosition(tmp.getNMatch());
				searchNavigation.refresh();
				searchText.setFocus();
				showFoundMatches();
				showCurrentSearchMatch();

				Log.info(NLS.bind(EditionUIMessages.P0Hits, searchResult.getNMatch())); //$NON-NLS-1$
			}
			else { // no result
				clearSearch();
			}
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private void clearSearch() {

		// disable highlights
		if (selectedWordid != null) {
			editor.removeHighlightWordsById(highlightColor, foundWordids);
			editor.removeHighlightWordsById(selectionColor, selectedWordid);
			editor.setFocusedWordID(null);
			editor.updateWordStyles();
		}

		searchIndex = 0;
		searchNavigation.setEnabled(false);
		if (concordanceButton != null) concordanceButton.setEnabled(false);
		searchNavigation.setCurrentPosition(0);
		searchNavigation.setMinPosition(0);
		searchNavigation.setMaxPosition(0);
		searchNavigation.refresh();
		Log.info(EditionUIMessages.bind(EditionUIMessages.P0NotFound, searchText.getQueryString()));
	}

	protected void showFoundMatches() {

		if (searchResult != null && searchIndex >= 0) {
			try {
				if (selectedWordid != null) {
					editor.removeHighlightWordsById(highlightColor, foundWordids);
					//editor.updateWordStyles();
				}

				foundWordids = CQPSearchEngine.getEngine().getValuesForProperty(searchResult.getStarts(), mcorpus.getProperty("id")); //$NON-NLS-1$
				editor.addHighlightWordsById(highlightColor, foundWordids);  // set found word color
				//editor.updateWordStyles();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	protected void showCurrentSearchMatch() {
		if (searchResult != null && searchIndex >= 0) {
			try {
				if (selectedWordid != null) {
					editor.removeHighlightWordsById(selectionColor, selectedWordid);
					editor.setFocusedWordID(null);
					//editor.updateWordStyles();
				}

				Match match = searchResult.getMatches().get(searchIndex);
				String text = CQPSearchEngine.getEngine().getValueForProperty(match, mcorpus.getStructuralUnitProperty("text_id")); //$NON-NLS-1$
				int[] positions = MatchUtils.toPositions(match);
				selectedWordid = CQPSearchEngine.getEngine().getValuesForProperty(positions, mcorpus.getProperty("id")); //$NON-NLS-1$
				if (selectedWordid.size() == 0) return;

				editor.addHighlightWordsById(selectionColor, selectedWordid.get(0)); // set selected word color
				editor.setFocusedWordID(selectedWordid.get(0)); // focus on the selected word

				editor.backToText(editor.getResult().getProject().getText(text), selectedWordid.get(0)); // show the right page
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
