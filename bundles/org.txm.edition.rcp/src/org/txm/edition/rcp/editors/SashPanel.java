package org.txm.edition.rcp.editors;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;

public class SashPanel extends Composite {

	//Composite parent;
	ArrayList<Composite> cells;

	public SashPanel(Composite parent, int style) {
		super(parent, style);
		//this.parent = parent;
		this.setLayout(new FormLayout());
	}

	public void init(int ncolumns) {
		if (cells != null) {
			for (Control c : this.getChildren())
				c.dispose();
			cells.clear();
			cells = new ArrayList<Composite>();
		}

		cells = new ArrayList<Composite>();
		Sash sash = null;
		FormData sashData = null;
		for (int i = 0; i < ncolumns; i++) {
			Composite container = new Composite(this, SWT.NONE);
			container.setLayout(new GridLayout());
			cells.add(container);

			FormData containerData = new FormData();
			containerData.width = 100;
			containerData.top = new FormAttachment(0, 0);
			containerData.bottom = new FormAttachment(100, 0);
			container.setLayoutData(containerData);// set new layout data

			if (sash != null) { // attached FormData to previous sash and vice-versa
				sashData.right = new FormAttachment(container, 0);
				containerData.left = new FormAttachment(sash, 0);
			}
			else if (i == 0) {
				containerData.left = new FormAttachment(0, 0); // first cell
			}

			if (i == ncolumns - 1) {
				containerData.right = new FormAttachment(100, 0); // last cell
			}
			else {
				sash = new Sash(this, SWT.VERTICAL);
				containerData.right = new FormAttachment(sash, 0);

				sashData = new FormData();
				sashData.left = new FormAttachment(container, 0);
				sashData.top = new FormAttachment(0, 0);
				sashData.bottom = new FormAttachment(100, 0);
				sash.setLayoutData(sashData);
			}
			sash.addListener(SWT.Selection, new Listener() {

				@Override
				public void handleEvent(Event e) {
					SashPanel.this.layout();
					//								e.getBounds()
					//								Rectangle sashRect = sash.getBounds();
					//								Rectangle shellRect = SashPanel.this.getClientArea();
					//								int right = shellRect.width - sashRect.width;
					//								e.x = Math.max(Math.min(e.x, right), 0);
					//								if (e.x != sashRect.x) {
					//									sashData.left = new FormAttachment(0, e.x);
					//									parentContainer.layout();
					//								}
				}
			});
		}

	}

	public Composite getContainer(int i) {
		return cells.get(i);
	}
}
