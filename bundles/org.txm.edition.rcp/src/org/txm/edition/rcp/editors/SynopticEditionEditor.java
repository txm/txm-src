// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.edition.rcp.editors;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.TXMAutoCompleteField;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.txm.edition.rcp.handlers.BackToText;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.IEditionEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.dialog.MultipleObjectSelectionDialog;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.logger.Log;

/**
 * Call the internal browser of RCP to display the Edition Pages of a Text
 * 
 * @author mdecorde.
 * 
 */
public class SynopticEditionEditor extends TXMEditor<Text> implements IEditionEditor {

	/** The Constant ID. */
	public final static String ID = SynopticEditionEditor.class.getName();

	LinkedHashMap<String, EditionPanel> editionPanels = new LinkedHashMap<>();

	/** The page_label: show current page name. */
	org.eclipse.swt.widgets.Text page_text;

	Label page_label;

	GLComposite editionsArea;

	private List<String> editionNames;

	private String textIdToOpen, pageIdToOpen, wordIdsToOpen;

	private CQPCorpus corpus;

	//private Text text;

	private Button editionsChooser;

	// private Label editionsLabel;
	private GLComposite lowerControlsArea;

	// private GLComposite annotationWidgetsArea;
	// private TXMEditorToolBar supplementaryButtonToolbar;
	private ISelectionProvider selProvider;

	/**
	 * null if the corpus has only one Text
	 */
	private org.eclipse.swt.widgets.Text text_text;

	SashForm sashForm;

	// private Label text_label;
	private TXMAutoCompleteField identifiantComboAutoCompleteField;

	private SearchEditionToolbar set;

	private Project project;

	private Combo urlHistoryText;

	private Combo okNumberHistorybutton;

	// TODO finish editor conversion and implement a TXMResult : EditionNavigation
	@Override
	public Text getResult() {

		for (EditionPanel panel : editionPanels.values()) {
			if (panel.getCurrentText() != null) {
				return panel.getCurrentText();
			}
		}

		if (textIdToOpen != null) {
			return this.getCorpus().getProject().getText(textIdToOpen);
		}
		else {
			return this.getCorpus().getProject().getFirstText();
		}
	}

	public SearchEditionToolbar getSearchEditionToolbar() {
		return set;
	}

	/**
	 * Instantiates a new txm browser.
	 */
	public SynopticEditionEditor() {

		selProvider = new ISelectionProvider() {

			@Override
			public void setSelection(ISelection selection) {
			}

			@Override
			public void removeSelectionChangedListener(ISelectionChangedListener listener) {
			}

			@Override
			public ISelection getSelection() {
				ITextSelection sel = new ITextSelection() {

					@Override
					public boolean isEmpty() {
						for (EditionPanel panel : editionPanels.values()) {
							String s = panel.getTextSelection();
							if (s.length() > 0) return false;
						}
						return true;
					}

					@Override
					public String getText() {
						for (EditionPanel panel : editionPanels.values()) {
							String s = panel.getTextSelection();
							if (s.length() > 0) return s;
						}
						return EditionUIMessages.CommandLink_empty;
					}

					@Override
					public int getStartLine() {
						return 0;
					}

					@Override
					public int getOffset() {
						return 0;
					}

					@Override
					public int getLength() {
						return 0;
					}

					@Override
					public int getEndLine() {
						return 0;
					}
				};
				return sel;
			}

			@Override
			public void addSelectionChangedListener(ISelectionChangedListener listener) {
			}
		};

	}

	public void setHighlightWordsById(RGBA color, HashSet<String> wordids) {
		for (EditionPanel panel : editionPanels.values()) {
			panel.setHighlightWordsById(color, wordids);
		}
	}

	public void addHighlightWordsById(RGBA color, String wordid) {
		for (EditionPanel panel : editionPanels.values()) {
			panel.addHighlightWordsById(color, wordid);
		}
	}

	public void addHighlightWordsById(RGBA color, Collection<String> wordids) {
		for (EditionPanel panel : editionPanels.values()) {
			panel.addHighlightWordsById(color, wordids);
		}
	}

	public void removeHighlightWordsById(RGBA color, Collection<String> wordids) {

		if (wordids != null) {
			for (EditionPanel panel : editionPanels.values()) {
				if (!panel.isDisposed()) {
					panel.removeHighlightWordsById(color, wordids);
				}
			}
		}
	}

	public void removeHighlightWordsById(RGBA color, String wordid) {
		for (EditionPanel panel : editionPanels.values()) {
			if (!panel.isDisposed()) {
				panel.removeHighlightWordsById(color, wordid);
			}
		}
	}

	// /**
	// *
	// * @param wordids2 all the words to highlight
	// * @param lineids2 the words to highlight better and to scroll to
	// */
	// public void highlightWordsById(List<String> wordids, List<String> lineids, int[] pos, String focusid) {
	// for(EditionPanel panel : editionPanels.values()) {
	// if (panel.getEdition().getName().startsWith("facs")) {
	// List<int[]> wordRectangles = new ArrayList<int[]>();
	// List<int[]> keywordRectangles = new ArrayList<int[]>();
	// if (pos != null) keywordRectangles.add(pos);
	// panel.highlightWordsByPositions(wordRectangles, keywordRectangles);
	// } else {
	// panel.highlightWordsById(wordids, lineids, focusid);
	// }
	// }
	//
	// updatePageLabel();
	//
	//
	// }

	/**
	 * First page.
	 */
	public void firstPage() {
		for (EditionPanel panel : editionPanels.values()) {
			panel.firstPage();
		}
		updatePageLabel();
	}

	/**
	 * Last page.
	 */
	public void lastPage() {
		for (EditionPanel panel : editionPanels.values()) {
			panel.lastPage();
		}
		updatePageLabel();
	}

	/**
	 * Previous page.
	 */
	public void previousPage() {
		for (EditionPanel panel : editionPanels.values()) {
			panel.previousPage();
		}
		updatePageLabel();
	}

	/**
	 * Next page.
	 */
	public void nextPage() {
		for (EditionPanel panel : editionPanels.values()) {
			panel.nextPage();
		}
		updatePageLabel();
	}

	/**
	 * Direct access to a page with a name.
	 */
	public void goToPage(String textId, String name) {

		for (EditionPanel panel : editionPanels.values()) {
			panel.goToPage(textId, name);
		}
		updatePageLabel();
	}

	/**
	 * Direct access to a text with a name.
	 */
	public void goToText(String textId) {
		Text newText = this.project.getText(textId);
		if (newText == null) {
			Log.warning(NLS.bind(EditionUIMessages.ErrorNoTextFoundWithTheP0Id, textId));
			return;
		}
		else {
			setText(newText, true);
		}
	}

	/**
	 * Previous text.
	 */
	public void previousText(boolean fromNextText) {
		for (EditionPanel panel : editionPanels.values()) {
			panel.previousText(fromNextText);
		}
		updatePageLabel();
	}

	/**
	 * Next text.
	 */
	public void nextText() {
		for (EditionPanel panel : editionPanels.values()) {
			panel.nextText();
		}
		updatePageLabel();
	}

	/**
	 * Previous text.
	 */
	public void firstText() {
		for (EditionPanel panel : editionPanels.values()) {
			panel.firstText();
		}
		updatePageLabel();
	}

	/**
	 * Next text.
	 */
	public void lastText() {
		for (EditionPanel panel : editionPanels.values()) {
			panel.lastText();
		}
		updatePageLabel();
	}

	public Composite getEditionArea() {
		return editionsArea;
	}

	// public Composite getUpperToolbarArea() {
	// return annotationWidgetsArea;
	// }

	public Composite getLowerToolbarArea() {
		return lowerControlsArea;
	}

	@Override
	public void _createPartControl() {

		// remove the compute button
		this.removeComputeButton();

		editionsArea = getResultArea();
		editionsArea.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println(e);
				if (e.character == 'f' && (e.stateMask & SWT.CTRL) != 0) {
					getSearchEditionToolbar().openSearch(getTextSelection());
				}
				else if (e.character == 'g' && (e.stateMask & SWT.CTRL | SWT.SHIFT) == (SWT.CTRL | SWT.SHIFT)) {
					getSearchEditionToolbar().backward();
				}
				else if (e.character == 'g' && (e.stateMask & SWT.CTRL) == (SWT.CTRL)) {
					getSearchEditionToolbar().forward();
				}
				else if (e.keyCode == SWT.ESC) {
					getSearchEditionToolbar().closeSearch();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		sashForm = new SashForm(editionsArea, SWT.HORIZONTAL);
		sashForm.SASH_WIDTH = 5;
		sashForm.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		Composite historyGroup = this.getTopToolbar().installGroup("Navigation", "History navigation controls", "platform:/plugin/org.txm.edition.rcp/icons/objects/clock.png",
				"platform:/plugin/org.txm.edition.rcp/icons/objects/clock.png", false, false);

		//GLComposite historyGroup = this.getTopToolbar().installGLComposite("History", 10, false);
		GridLayout gl = new GridLayout(20, false);
		gl.horizontalSpacing = 2;
		gl.marginWidth = 0;
		historyGroup.setLayout(gl);

		Button previousHistoryButton = new Button(historyGroup, SWT.PUSH);
		previousHistoryButton.setToolTipText("Reload panels with their previous history URL");
		previousHistoryButton.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		previousHistoryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				for (EditionPanel p : getEditionPanels().values()) {
					p.getBrowser().back();

				}
				urlHistoryText.setText(getEditionPanel(0).getURL());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button nextHistorybutton = new Button(historyGroup, SWT.PUSH);
		nextHistorybutton.setToolTipText("Reload panels with their next history URL");
		nextHistorybutton.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		nextHistorybutton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				for (EditionPanel p : getEditionPanels().values()) {
					p.getBrowser().forward();
				}
				urlHistoryText.setText(getEditionPanel(0).getURL());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button reloadHistoryButton = new Button(historyGroup, SWT.PUSH);
		reloadHistoryButton.setToolTipText("Reload panels with their current URL");
		reloadHistoryButton.setImage(IImageKeys.getImage(IImageKeys.REFRESH));
		reloadHistoryButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				for (EditionPanel p : getEditionPanels().values()) {
					p.getBrowser().refresh();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});


		urlHistoryText = new Combo(historyGroup, SWT.BORDER);
		urlHistoryText.setToolTipText("Current panel URLs");
		urlHistoryText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		urlHistoryText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					getEditionPanel(okNumberHistorybutton.getSelectionIndex()).setURL(urlHistoryText.getText());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		okNumberHistorybutton = new Combo(historyGroup, SWT.SINGLE | SWT.READ_ONLY);
		okNumberHistorybutton.setToolTipText("Select a panel to change URL");
		okNumberHistorybutton.setText("default");
		okNumberHistorybutton.setEnabled(false);

		Button okHistorybutton = new Button(historyGroup, SWT.PUSH);
		okHistorybutton.setToolTipText("Reload the selected panel with the URL set in the URL field");
		okHistorybutton.setImage(IImageKeys.getImage(IImageKeys.START));
		okHistorybutton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				getEditionPanel(okNumberHistorybutton.getSelectionIndex()).setURL(urlHistoryText.getText());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		lowerControlsArea = getBottomToolbar().installGLComposite(EditionUIMessages.controls, 15, false);
		lowerControlsArea.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		// spacer
		new Label(lowerControlsArea, SWT.NONE).setText("  "); //$NON-NLS-1$

		set = new SearchEditionToolbar();

		// MainCorpus mcorpus = (MainCorpus) project.getCorpusBuild(project.getName().toUpperCase(), MainCorpus.class);
		set.init(lowerControlsArea, corpus, this, BackToText.red, BackToText.lightred);

		//		set = new SearchEditionToolbar();
		//		
		//		// MainCorpus mcorpus = (MainCorpus) project.getCorpusBuild(project.getName().toUpperCase(), MainCorpus.class);
		//		set.init(lowerControlsArea, corpus, this, BackToText.red, BackToText.lightred);

		// Edition names label


		if (project.getBuiltEditionNames().size() > 1) {
			editionsChooser = new Button(lowerControlsArea, SWT.PUSH);
			editionsChooser.setText(StringUtils.join(editionNames, " | ")); //$NON-NLS-1$
			editionsChooser.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					try {
						List<String> availableEditions = project.getBuiltEditionNames();
						if (availableEditions.size() == 0) return;
						if (availableEditions.size() == 1) return;


						Shell shell = e.display.getActiveShell();
// Arrays.asList(project.getDefaultEditionName().split(","))
						MultipleObjectSelectionDialog<String> d = new MultipleObjectSelectionDialog<String>(shell, availableEditions, editionNames, -1, editionsChooser, null);
						int ret = d.open();
						if (ret == MultipleObjectSelectionDialog.OK) {
							editionNames = d.getSelection();
							//							editionNames = new ArrayList<>();
							//							for (int i = 0; i < rez.length; i++) {
							//								editionNames.add(rez[i].toString());
							//							}

							editionsChooser.setText(StringUtils.join(editionNames, " | ")); //$NON-NLS-1$
							openEditions(editionNames, null, null, null);
							firstPage();


						}
					}
					catch (Exception e2) {
						org.txm.utils.logger.Log.printStackTrace(e2);
						System.out.println(NLS.bind(EditionUIMessages.errorWhileGettingAvailableEditionsColonP0, e2));
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		// Navigation buttons
		GLComposite pageNavigationComposite = new GLComposite(lowerControlsArea, SWT.NONE, EditionUIMessages.pageButtons);
		pageNavigationComposite.getLayout().numColumns = 7;
		pageNavigationComposite.getLayout().horizontalSpacing = 1;
		Label l = new Label(pageNavigationComposite, SWT.NONE);
		l.setText(EditionUIMessages.page);
		Button first = new Button(pageNavigationComposite, SWT.FLAT | SWT.PUSH);
		Button previous = new Button(pageNavigationComposite, SWT.FLAT | SWT.PUSH);
		page_text = new org.eclipse.swt.widgets.Text(pageNavigationComposite, SWT.BORDER);
		page_label = new Label(pageNavigationComposite, SWT.NONE);
		Button next = new Button(pageNavigationComposite, SWT.FLAT | SWT.PUSH);
		Button last = new Button(pageNavigationComposite, SWT.FLAT | SWT.PUSH);
		pageNavigationComposite.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));

		if (project.getTexts().size() > 1) {
			GLComposite textNavigationComposite = new GLComposite(lowerControlsArea, SWT.NONE, EditionUIMessages.textButtons);
			textNavigationComposite.getLayout().numColumns = 8;
			textNavigationComposite.getLayout().horizontalSpacing = 1;
			Label l2 = new Label(textNavigationComposite, SWT.NONE);
			l2.setText(EditionUIMessages.text);
			Button firstText = new Button(textNavigationComposite, SWT.FLAT);
			Button previousText = new Button(textNavigationComposite, SWT.FLAT);
			text_text = new org.eclipse.swt.widgets.Text(textNavigationComposite, SWT.BORDER);
			KeyStroke keys = KeyStroke.getInstance(SWT.CONTROL, SWT.SPACE);
			try {
				String[] text_ids = corpus.getCorpusTextIdsList();
				GridData gdata = new GridData(GridData.CENTER, GridData.CENTER, true, false);
				gdata.minimumWidth = 1;
				for (String text_id : text_ids) {
					double s = text_id.length() * 7;
					if (s > gdata.minimumWidth) {
						gdata.minimumWidth = (int) s;
					}
				}
				gdata.widthHint = 200;
				text_text.setLayoutData(gdata);
				identifiantComboAutoCompleteField = new TXMAutoCompleteField(text_text, new TextContentAdapter(), text_ids, keys);
				// identifiantComboAutoCompleteField.setSelectionListener(new SelectionListener() {
				// @Override
				// public void widgetSelected(SelectionEvent e) {
				// goToText(text_text.getText());
				// }
				//
				// @Override
				// public void widgetDefaultSelected(SelectionEvent e) { }
				// });
				if (!text_text.isDisposed()) {
					Cursor cursor = text_text.getDisplay().getSystemCursor(SWT.CURSOR_SIZENS);
					text_text.setCursor(cursor);
				}
			}
			catch (Exception e2) {
				e2.printStackTrace();
			}

			// text_label = new Label(textNavigationComposite, SWT.NONE);
			Button nextText = new Button(textNavigationComposite, SWT.FLAT | SWT.PUSH);
			Button lastText = new Button(textNavigationComposite, SWT.FLAT | SWT.PUSH);
			textNavigationComposite.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, false));
			// Layouts data
			firstText.setImage(IImageKeys.getImage(IImageKeys.CTRLREWINDSTART));
			firstText.setToolTipText(EditionUIMessages.firstTextOfTheCorpus);
			previousText.setImage(IImageKeys.getImage(IImageKeys.CTRLREWIND));
			previousText.setToolTipText(EditionUIMessages.previousText);
			nextText.setImage(IImageKeys.getImage(IImageKeys.CTRLFASTFORWARD));
			nextText.setToolTipText(EditionUIMessages.nextText);
			lastText.setImage(IImageKeys.getImage(IImageKeys.CTRLFASTFORWARDEND));
			lastText.setToolTipText(EditionUIMessages.lastTextOfTheCorpus);

			nextText.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					nextText();
				}

				@Override
				public void widgetSelected(SelectionEvent e) {
					nextText();
				}
			});
			// set listeners
			previousText.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					previousText(false);
				}

				@Override
				public void widgetSelected(SelectionEvent e) {
					previousText(false);
				}
			});
			lastText.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					lastText();
				}

				@Override
				public void widgetSelected(SelectionEvent e) {
					lastText();
				}
			});
			// set listeners
			firstText.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					firstText();
				}

				@Override
				public void widgetSelected(SelectionEvent e) {
					firstText();
				}
			});

			text_text.addKeyListener(new KeyListener() {

				@Override
				public void keyReleased(KeyEvent e) {
					if (identifiantComboAutoCompleteField.isOpen()) return;

					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						if (identifiantComboAutoCompleteField.isOpen()) {
							return; // ignore keys when content assist is opened
						}

						goToText(text_text.getText());
					}
					else if (e.keyCode == SWT.ARROW_DOWN) {
						nextText();
					}
					else if (e.keyCode == SWT.ARROW_UP) {
						previousText(false);
					}
				}

				@Override
				public void keyPressed(KeyEvent e) {
				}
			});
			text_text.addMouseWheelListener(new MouseWheelListener() {

				@Override
				public void mouseScrolled(MouseEvent e) {
					if (identifiantComboAutoCompleteField.isOpen()) {
						return;
					}

					if (e.count > 0) {
						previousText(false);
					}
					else {
						nextText();
					}
					// toolbar.getEditor().updateWordStyles();
				}
			});
		}
		if (editionsChooser != null) editionsChooser.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));

		// GridData buttonGridData = new GridData(GridData.END,GridData.CENTER, true, false);
		// buttonGridData.widthHint = 30;
		// buttonGridData.heightHint = 30;
		// firstText.setLayoutData(buttonGridData);
		// buttonGridData = new GridData(GridData.CENTER,GridData.CENTER, false, false);
		// buttonGridData.widthHint = 30;
		// buttonGridData.heightHint = 30;
		// previousText.setLayoutData(buttonGridData);
		// GridData centerButtonGridData = new GridData(GridData.END,GridData.CENTER, true, true);
		// centerButtonGridData.widthHint = 30;
		// centerButtonGridData.heightHint = 30;
		// first.setLayoutData(centerButtonGridData);
		// previous.setLayoutData(buttonGridData);

		// set sizes
		GridData gdata = new GridData(GridData.END, GridData.CENTER, true, false);
		gdata.minimumWidth = 75;
		gdata.widthHint = 75;

		page_text.setLayoutData(gdata);
		// page_label.setLayoutData(new GridData(GridData.BEGINNING,GridData.CENTER, false, false));
		//
		// next.setLayoutData(buttonGridData);
		// centerButtonGridData = new GridData(GridData.BEGINNING, GridData.CENTER, true, true);
		// centerButtonGridData.widthHint = 30;
		// centerButtonGridData.heightHint = 30;
		// last.setLayoutData(centerButtonGridData);
		// nextText.setLayoutData(buttonGridData);
		// lastText.setLayoutData(buttonGridData);

		// if (editionsChooser != null)
		// editionsChooser.setLayoutData(new GridData(GridData.BEGINNING,GridData.CENTER, false, true));
		// else spacer.setLayoutData(new GridData(GridData.END,GridData.CENTER, true, true));

		// set labels
		page_label.setText(""); //$NON-NLS-1$
		// text_label.setText(""); //$NON-NLS-1$

		first.setImage(IImageKeys.getImage(IImageKeys.CTRLSTART));
		first.setToolTipText(EditionUIMessages.firstPageOfTheText);
		previous.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		previous.setToolTipText(EditionUIMessages.previousPage);
		next.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		next.setToolTipText(EditionUIMessages.nextPage);
		last.setImage(IImageKeys.getImage(IImageKeys.CTRLEND));
		last.setToolTipText(EditionUIMessages.lastPageOfTheText);

		page_text.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					goToPage(null, page_text.getText());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		// set listeners
		first.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				firstPage();
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				firstPage();
			}
		});
		previous.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				previousPage();
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				previousPage();
			}
		});
		next.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				nextPage();
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				nextPage();
			}
		});
		last.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				lastPage();
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				lastPage();
			}
		});


		try {
			// ensure all texts have build their page indexes
			for (Text t : project.getTexts()) {
				t.compute();
			}
			if (openEditions(editionNames, textIdToOpen, pageIdToOpen, wordIdsToOpen)) {
				//firstPage();
			}
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * update editor title using the first panel only
	 */
	private void updatePageLabel() {
		if (!urlHistoryText.isDisposed()) {

			String urls[] = new String[this.getEditionPanels().size()];
			int i = 0;
			for (EditionPanel e : this.getEditionPanels().values()) {
				urls[i++] = e.getURL();
			}
			urlHistoryText.setItems(urls);
			urlHistoryText.setText(this.getEditionPanel(0).getURL());

		}
		for (EditionPanel p : editionPanels.values()) {
			Page cpage = p.getCurrentPage();
			if (cpage == null) {
				continue;
			}
			Edition e = cpage.getEdition();
			if (e == null) {
				continue;
			}
			this.setPartName(e.getText().getName() + " - " + cpage.getName()); //$NON-NLS-1$

			if (page_text.isDisposed()) {
				continue;
			}
			page_text.setText(cpage.getName());
			page_label.setText(" / " + e.getNumPages()); //$NON-NLS-1$

			if (text_text != null) text_text.setText(p.getCurrentText().getName());
			// try {
			// text_label.setText(" / "+corpus.getCorpusTextIdsList().length);
			// } catch (Exception e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }

			lowerControlsArea.layout();
			return;
		}
	}

	// public boolean openEditions(CQPCorpus corpus, String textID, String... editionNames) {
	// Text text = corpus.getProject().getText(textID);
	// if (text == null) return false;
	// return openEditions(corpus, text, editionNames);
	// }

	// /**
	// * Opens a list of edition corpus
	// * @param corpus
	// * @param editionNames
	// */
	// public boolean openEditions(CQPCorpus corpus, Text text, String... editionNames) {
	// return openEditions(corpus, text, Arrays.asList(editionNames));
	// }

	/**
	 * Opens a list of edition corpus
	 * 
	 * @param editionNames
	 * @throws CqiServerError
	 * @throws IOException
	 * @throws CqiClientException
	 */
	public boolean openEditions(List<String> editionNames, String textId, String pageId, String wordIds) throws CqiClientException, IOException, CqiServerError {

		if (editionNames.size() == 0) return false;

		okNumberHistorybutton.setEnabled(editionNames.size() > 1);
		okNumberHistorybutton.setItems(editionNames.toArray(new String[editionNames.size()]));
		okNumberHistorybutton.setText(editionNames.get(0));

		boolean reloadPanels = true;// editionNames.size() != this.getEditionPanels().keySet().size(); // not working
		if (!reloadPanels) {
			int i = 0;
			for (String ed : this.getEditionPanels().keySet()) {
				if (!editionNames.get(i++).equals(ed)) {
					reloadPanels = true;
					break;
				}
			}
		}
		// remove previous EditionPanel
		if (reloadPanels) {
			for (EditionPanel panel : editionPanels.values()) {
				if (!panel.isDisposed()) {
					panel.dispose();
				}
			}
			editionPanels = new LinkedHashMap<>();

			//editionsArea.getLayout().numColumns = editionNames.size() * 2;

			for (int i = 0; i < editionNames.size(); i++) {
				String editionName = editionNames.get(i);
				// System.out.println("EDITION NAME: "+editionName);
				if (corpus.getProject().hasEditionDefinition(editionName)) {
					try {

						EditionPanel panel = new EditionPanel(sashForm, this, SWT.BORDER, editionName);
						panel.initSelectionProvider();

						if (i == 0) { // TODO: how to manage multiple menu manager -> the text selection does not work only the first panel selection is used
							panel.initMenu();
							getSite().registerContextMenu(panel.getMenuManager(), panel.getSelectionProvider());
							getSite().setSelectionProvider(panel.getSelectionProvider());
						}

						editionPanels.put(editionName, panel);
					}
					catch (SWTError e) {
						Log.severe(NLS.bind(EditionUIMessages.TXMCouldNotOpenTheSWTInternalBrowserP0, e));
						Log.printStackTrace(e);
					}
				}
				else {
					Log.warning(NLS.bind(EditionUIMessages.warningColonCouldNotFindEditionForNameEqualsP0, editionName));
				}
			}
			editionsArea.layout();
			sashForm.layout();
		}

		Text text = null;
		if (textId == null) {
			textId = this.corpus.getCorpusTextIdsList()[0];
		}
		text = this.corpus.getProject().getText(textId);
		if (text == null) {
			Log.warning(NLS.bind(EditionUIMessages.ErrorNoTextFoundWithTheP0Id, textId));
			return false;
		}
		try {
			text.compute();
			setText(text, false);
		}
		catch (Exception e) {
			System.out.println(NLS.bind(EditionUIMessages.ErrorP0IsNotReadyToUse, text.getName())); //$NON-NLS-2$ //$NON-NLS-1$
			return false;
		}

		if (pageId != null) {
			this.goToPage(textId, pageId);
		}

		if (wordIds != null && wordIds.length() > 0) {

			String[] wordIdsArray = wordIds.split(","); //$NON-NLS-1$

			if (pageId == null) {
				this.goToWord(textId, wordIdsArray[0]);
			}
			this.addHighlightWordsById(BackToText.lightred, Arrays.asList(wordIdsArray));
			this.setFocusedWordID(wordIds);
		}

		String url = this.editionPanels.get(editionNames.get(0)).getURL();
		if (url == null || url.equals("about:blank")) { //$NON-NLS-1$
			setText(text, true);
		}
		return true;
	}

	//	/**
	//	 * Returns the selection provider for a part.
	//	 */
	//	@Override
	//	public ISelectionProvider getSelectionProvider() {
	//		return getEditionPanel(0).getSelectionProvider();
	//	}

	/**
	 * 
	 * @param textId the text containing the word to focus
	 * @param wordId the word to focus ID
	 * @return
	 */
	private boolean goToWord(String textId, String wordId) {

		Text text = this.corpus.getProject().getText(textId);
		if (text == null) {
			Log.warning(NLS.bind(EditionUIMessages.ErrorNoTextFoundWithTheP0Id, textId));
			return false;
		}

		return backToText(text, wordId);
	}

	// /**
	// *
	// * //TODO: add getDefaultEditions to Corpus object
	// *
	// * @param corpus
	// * @return a list of default editions
	// */
	// public String[] getDefaultEditions(CQPCorpus corpus) {
	// String defaultEdition = corpus.getProject().getDefaultEdition();
	// if (defaultEdition.contains(",")) { //$NON-NLS-1$
	// return defaultEdition.split(","); //$NON-NLS-1$
	// } else {
	// return new String[]{defaultEdition};
	// }
	// }

	@Override
	public void doSaveAs() {
		return;
	}

	@Override
	public void _init() throws PartInitException {

		corpus = ((SynopticEditorInput) getEditorInput()).getCorpus(); // corpus focused when opening the editions
		try {
			corpus.compute(false);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		project = corpus.getProject();

		//text = ((SynopticEditorInput) input).getText();
		editionNames = ((SynopticEditorInput) getEditorInput()).getEditions();
		textIdToOpen = ((SynopticEditorInput) getEditorInput()).getTextID();
		if (textIdToOpen == null) {
			try {
				textIdToOpen = corpus.getCorpusTextIdsList()[0];
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		pageIdToOpen = ((SynopticEditorInput) getEditorInput()).getPageID();
		wordIdsToOpen = ((SynopticEditorInput) getEditorInput()).getWordId();
	}

	//	public boolean isDisposed() {
	//		return editionsArea.isDisposed();
	//	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void _setFocus() {
		if (this.getEditionPanel(0) == null) return;
		this.getEditionPanel(0).setFocus();
	}

	public boolean backToText(Text text, String line_wordid) {
		try {
			text.compute();
			//this.setText(text, false); // not necesasry if panel.backtotext is called after

			for (EditionPanel p : editionPanels.values()) {
				p.backToText(text, line_wordid);
			}
			updatePageLabel();

			//updateWordStyles();
			notifyExtensions("onBackToText"); //$NON-NLS-1$
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void reloadPage() {
		for (EditionPanel p : editionPanels.values()) {
			p.reloadPage();
		}
	}

	@Override
	public String getTextSelection() {
		String sel = null;
		for (EditionPanel p : editionPanels.values()) {
			sel = p.getTextSelection();
			if (sel != null) {
				return sel;
			}
		}
		return EditionUIMessages.CommandLink_empty;
	}

	@Override
	public CQPCorpus getCorpus() {
		return corpus;
	}

	public EditionPanel getEditionPanel(int i) {
		int n = 0;
		for (EditionPanel p : editionPanels.values()) {
			if (n == i) {
				return p;
			}
			n++;
		}
		return null;
	}

	/**
	 * 
	 * @return the first and last word ID covered by the text selection of the first EditionPanel
	 */
	public String[] getWordSelection() {
		for (EditionPanel p : editionPanels.values()) {
			return p.getWordSelection();
		}
		return null;
	}

	/**
	 * 
	 * @param newText the Text to set
	 * @param refresh if true set the first Text page
	 */
	public void setText(Text newText, boolean refresh) {
		for (EditionPanel p : editionPanels.values()) {
			p.setText(newText, refresh);
		}

		if (refresh) {
			updatePageLabel();
		}
	}

	public void setFocusedWordID(String focusid) {
		for (EditionPanel p : editionPanels.values()) {
			p.setFocusedWordID(focusid);
		}
		updatePageLabel();
	}

	/**
	 * TODO finish implementation in EditionPanel
	 * 
	 * @param positions
	 */
	public void setHighlightedArea(HashSet<int[]> positions) {
		for (EditionPanel p : editionPanels.values()) {
			p.setHighlightedArea(positions);
		}
	}

	public void updateWordStyles() {
		for (EditionPanel p : editionPanels.values()) {
			p.updateWordStyles();
		}
	}

	public void addFontWeightWordsById(String weight, List<String> previousSelectedUnitIDS) {
		for (EditionPanel p : editionPanels.values()) {
			p.addFontWeightWordsById(weight, previousSelectedUnitIDS);
		}
	}

	public void removeFontWeightWordsById(List<String> previousSelectedUnitIDS) {
		for (EditionPanel p : editionPanels.values()) {
			p.removeFontWeightWordsById(previousSelectedUnitIDS);
		}
	}

	public void fireIsDirty() {
		this.firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	/**
	 * Clears all highlighted words.
	 */
	public void removeHighlightWords() {
		for (EditionPanel p : editionPanels.values()) {
			p.removeHighlightWords();
		}
	}

	@Override
	public void updateResultFromEditor() {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateEditorFromResult(boolean update) throws Exception {
		//this.firstPage();
	}

	public LinkedHashMap<String, EditionPanel> getEditionPanels() {
		return editionPanels;
	}
}
