package org.txm.edition.rcp.editors;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.internal.browser.BrowserViewer;
import org.osgi.framework.Bundle;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.edition.rcp.preferences.SynopticEditionPreferences;
import org.txm.objects.Edition;
import org.txm.objects.Page;
import org.txm.objects.Project;
import org.txm.objects.Text;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.CommandLink;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.editors.menu.PagePropertiesMenu;
import org.txm.rcp.utils.IOClipboard;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class EditionPanel extends BrowserViewer implements ISelectionProvider {

	private ProgressListener progressListener;

	// private List<String> wordids;
	// private List<String> lineids;
	/** The edition. */
	Text currentText;

	String editionName;

	Edition currentEdition;

	Page currentPage;

	protected SynopticEditionEditor synopticEditionEditor;

	private ISelectionProvider selProvider;

	private MenuManager menuManager;

	private HashSet<int[]> highlightedAreas = null;

	// HashMap<String, ArrayList<RGBA>> highlightedColorSpans = new HashMap<String, ArrayList<RGBA>>(); // background color

	HashMap<String, ArrayList<RGBA>> highlightedColorPerWordIDS = new HashMap<>(); // background color

	public static final String STYLE_NORMAL = "normal"; //$NON-NLS-1$

	public static final String STYLE_ITALIC = "italic"; //$NON-NLS-1$

	public static final String STYLE_OBLIQUE = "oblique"; //$NON-NLS-1$

	HashMap<String, String> fontStylePerWordIDS = new HashMap<>();

	public static final String WEIGHT_NORMAL = "normal"; //$NON-NLS-1$

	public static final String WEIGHT_BOLD = "bold"; //$NON-NLS-1$

	HashMap<String, String> fontWeightPerWordIDS = new HashMap<>();

	HashMap<String, ArrayList<RGBA>> fontColorPerWordIDS = new HashMap<>();

	public static final String SIZE_SMALL = "small"; //$NON-NLS-1$

	public static final String SIZE_MEDIUM = "medium"; //$NON-NLS-1$

	public static final String SIZE_LARGE = "larger"; //$NON-NLS-1$

	public static final String SIZE_SMALLER = "smaller"; //$NON-NLS-1$

	public static final String SIZE_LARGER = "larger"; //$NON-NLS-1$

	HashMap<String, String> fontSizePerWordIDS = new HashMap<>();

	public static final String FAMILLY_TIMES = "\"Times New Roman\", Times"; //$NON-NLS-1$

	public static final String FAMILLY_ARIAL = "Arial"; //$NON-NLS-1$

	public static final String FAMILLY_COURIER = "\"Courier New\", Courier"; //$NON-NLS-1$

	HashMap<String, String> fontFamillyPerWordIDS = new HashMap<>();

	public static final String VARIANT_NORMAL = "normal"; //$NON-NLS-1$

	public static final String VARIANT_SMALLCAPS = "small-caps"; //$NON-NLS-1$

	HashMap<String, String> fontVariantPerWordIDS = new HashMap<>();

	String focusedWordID = null;

	public static final String highlightscript = "try { var elt = document.getElementById(\"%s\");" + //$NON-NLS-1$
			"elt.style.backgroundColor=\"rgb(%s,%s,%s)\";" + //$NON-NLS-1$
			// "elt.style.paddingLeft=\"3px\";" + //$NON-NLS-1$
			// "elt.style.paddingRight=\"3px\";" + //$NON-NLS-1$
			// "elt.style.paddingTop=\"1px\";" + //$NON-NLS-1$
			// "elt.style.paddingBottom=\"1px\";" + //$NON-NLS-1$
			"} catch (e) { };"; //$NON-NLS-1$

	public static final String highlightscriptRuleFast = "sheet.insertRule(\"#%s {background-color:rgba(%s,%s,%s,%s);}\", 0);"; //$NON-NLS-1$

	public static final String colorscriptRuleFast = "sheet.insertRule(\"#%s {color:rgba(%s,%s,%s,%s);}\", 0);"; //$NON-NLS-1$

	public static final String sizescriptRuleFast = "sheet.insertRule(\"#%s {font-size:%s;}\", 0);"; //$NON-NLS-1$

	public static final String stylescriptRuleFast = "sheet.insertRule(\"#%s {font-style:%s;}\", 0);"; //$NON-NLS-1$

	public static final String variantscriptRuleFast = "sheet.insertRule(\"#%s {font-variant:%s;}\", 0);"; //$NON-NLS-1$

	public static final String famillyscriptRuleFast = "sheet.insertRule(\"#%s {font-familly:%s;}\", 0);"; //$NON-NLS-1$

	public static final String weightscriptRuleFast = "sheet.insertRule(\"#%s {font-weight:%s;}\", 0);"; //$NON-NLS-1$

	public static final String highlightscriptRule = "sheet.insertRule(\"span[id=\\\"%s\\\"] {background-color:rgba(%s,%s,%s,%s); }\", 0);"; //$NON-NLS-1$

	public static final String clearhighlightscript = "try { var elt = document.getElementById(\"%s\"); elt.style.backgroundColor=\"white\";elt.style.fontWeight=\"\";" + //$NON-NLS-1$
			"} catch (e) { };"; //$NON-NLS-1$

	public String functions = ""// "alert(\"loading functions\");" //$NON-NLS-1$
			+ "\ngetNodeText = function getNodeText(node) {" //$NON-NLS-1$
			+ "\n	if (typeof node == 'string') return node;" //$NON-NLS-1$
			+ "\n	else if (typeof  node.innerText != 'undefined') return node.innerText;" //$NON-NLS-1$
			+ "\n	else if (typeof  node.InnerText != 'undefined') return node.InnerText;" //$NON-NLS-1$
			+ "\n	else if (typeof  node.textContent != 'undefined') return node.textContent;" //$NON-NLS-1$
			+ "\n	else return null;" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetParent = function txmGetParent(node) {" //$NON-NLS-1$
			+ "\n	if (typeof  node.parentNode != 'undefined') return node.parentNode;" //$NON-NLS-1$
			+ "\n	else return null;" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetChildren = function txmGetChildren(node) {" //$NON-NLS-1$
			+ "\n	if (typeof node == 'string') return 'STRING HTML';" //$NON-NLS-1$
			+ "\n	else if (typeof  node.children != 'undefined') return node.children;" //$NON-NLS-1$
			+ "\n	else if (typeof  node.ChildNodes != 'undefined') return node.ChildNodes;" //$NON-NLS-1$
			+ "\n	else if (typeof  node.childNodes != 'undefined') return node.childNodes;" //$NON-NLS-1$
			+ "\n	else return null;" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetSelection = function txmGetSelection() {" //$NON-NLS-1$
			+ "\n	if (typeof window.getSelection != 'undefined') {return window.getSelection();}" //$NON-NLS-1$
			+ "\n	else if (typeof document.selection != 'undefined') { return document.selection;}" //$NON-NLS-1$
			+ "\n	else return 'NO SELECTION';" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetRangeSize = function txmGetRangeSize(selection) {" //$NON-NLS-1$
			+ "\n	if (typeof selection.rangeCount != 'undefined') {return selection.rangeCount;}" //$NON-NLS-1$
			+ "\n	else if (typeof selection.createRangeCollection != 'undefined') { return selection.createRangeCollection().length();} // IE5 has no multiple selection" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetRange = function txmGetRange(selection, i) {" //$NON-NLS-1$
			+ "\n	if (typeof selection.getRangeAt != 'undefined') {return selection.getRangeAt(i);}" //$NON-NLS-1$
			+ "\n	else if (typeof selection.createRangeCollection != 'undefined') {return selection.createRangeCollection().item(i);}" // IE > IE5 //$NON-NLS-1$
			+ "\n	else if (typeof selection.createRange != 'undefined') { return selection.createRange();} // IE5 has no multiple selection" //$NON-NLS-1$
			+ "\n	else return 'NO RANGE';" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetParentElementRange = function txmGetParentElementRange(range) {" //$NON-NLS-1$
			+ "\n	if (typeof range.parentElement != 'undefined') {return range.parentElement();}" //$NON-NLS-1$
			+ "\n	else if (typeof range.startContainer != 'undefined') { return range.startContainer.parentNode;}" //$NON-NLS-1$
			+ "\n	else return 'NO PARENT';" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetFragment = function txmGetFragment(range) {" //$NON-NLS-1$
			+ "\n	if (typeof range.cloneContents != 'undefined') { return range.cloneContents();}" //$NON-NLS-1$
			+ "\n	else if (typeof range.htmlText != 'undefined') {var node = document.createElement('sel'); node.innerHTML = range.htmlText; 		return node;" //$NON-NLS-1$
			+ "\n	} else return 'NO FRAG';" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\ntxmGetTagName = function txmGetTagName(node) {" //$NON-NLS-1$
			+ "\n	if (typeof node.tagName != 'undefined') {return node.tagName;}" //$NON-NLS-1$
			+ "\n	else if (typeof node.nodeName != 'undefined') { return node.nodeName;}" //$NON-NLS-1$
			+ "\n	else if (typeof node.name != 'undefined') { return node.name;}" //$NON-NLS-1$
			+ "\n	else return 'NO TAGNAME';" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\nfindSpans = function findSpans(children, all) {" //$NON-NLS-1$
			+ "\n    for (var i = 0 ; i < children.length ; i++) {" //$NON-NLS-1$
			+ "\n    	var node = children.item(i);" //$NON-NLS-1$
			+ "\n		if (node.nodeType == 1) {" //$NON-NLS-1$
			+ "\n    	var id = node.getAttribute('id');" //$NON-NLS-1$
			+ "\n       		if (node.tagName == 'SPAN' && id != null && id.indexOf('w_') == 0 && getNodeText(node).length > 0) {" //$NON-NLS-1$
			+ "\n            	if (id != null) all.push(id);" //$NON-NLS-1$
			+ "\n        	} else {" //$NON-NLS-1$
			+ "\n        		findSpans(txmGetChildren(node), all)" //$NON-NLS-1$
			+ "\n        	}" //$NON-NLS-1$
			+ "\n		}" //$NON-NLS-1$
			+ "\n	}" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\nfindIdsInString = function findIdsInString(str, all) {" //$NON-NLS-1$
			+ "\n    for (var i = 0 ; i < children.length ; i++) {" //$NON-NLS-1$
			+ "\n    	var node = children.item(i);" //$NON-NLS-1$
			+ "\n		if (node.nodeType == 1) {" //$NON-NLS-1$
			+ "\n    	var id = node.getAttribute('id');" //$NON-NLS-1$
			+ "\n       		if (node.tagName == 'SPAN' && id != null && id.indexOf('w_') == 0 && getNodeText(node).length > 0) {" //$NON-NLS-1$
			+ "\n            	if (id != null) all.push(id);" //$NON-NLS-1$
			+ "\n        	} else {" //$NON-NLS-1$
			+ "\n        		findSpans(txmGetChildren(node), all)" //$NON-NLS-1$
			+ "\n        	}" //$NON-NLS-1$
			+ "\n		}" //$NON-NLS-1$
			+ "\n	}" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\nget_type = function get_type(thing){if(thing===null)return \"[object Null]\";  return Object.prototype.toString.call(thing);}\n" //$NON-NLS-1$
			+ "\ngetDetailedObject = function getDetailedObject(inputObject) { var detailedObject = {}; var properties; \n   do {        properties = Object.getOwnPropertyNames( inputObject );        for (var o in properties) {detailedObject[properties[o]] = inputObject[properties[o]];}} while ( inputObject = Object.getPrototypeOf(inputObject) );return detailedObject;}\n" //$NON-NLS-1$
			+ "\nvar sheet = null;" //$NON-NLS-1$
			+ "\ntry{" //$NON-NLS-1$
			+ "\n	var style = document.createElement(\"style\");" //$NON-NLS-1$
			+ "\n	try { style.appendChild(document.createTextNode(\"\")); } catch (e) {};"// webkit hack //$NON-NLS-1$
			+ "\n	document.body.appendChild(style);" //$NON-NLS-1$
			+ "\n	sheet = style.sheet;" //$NON-NLS-1$
			+ "\n} catch(e) {sheet='error'}" //$NON-NLS-1$
			+ "\ntry{" //$NON-NLS-1$
			+ "\n" // define scrollIntoViewIfNeeded if absent //$NON-NLS-1$
			+ "\nif (!Element.prototype.scrollIntoViewIfNeeded) {" //$NON-NLS-1$
			+ "\n	Element.prototype.scrollIntoViewIfNeeded = function (centerIfNeeded) {" //$NON-NLS-1$
			+ "\n		centerIfNeeded = arguments.length === 0 ? true : !!centerIfNeeded;" //$NON-NLS-1$
			+ "\n		var parent = this.parentNode," //$NON-NLS-1$
			+ "\n		parentComputedStyle = window.getComputedStyle(parent, null)," //$NON-NLS-1$
			+ "\n		parentBorderTopWidth = parseInt(parentComputedStyle.getPropertyValue('border-top-width'))," //$NON-NLS-1$
			+ "\n		parentBorderLeftWidth = parseInt(parentComputedStyle.getPropertyValue('border-left-width'))," //$NON-NLS-1$
			+ "\n		overTop = this.offsetTop - parent.offsetTop < parent.scrollTop," //$NON-NLS-1$
			+ "\n		overBottom = (this.offsetTop - parent.offsetTop + this.clientHeight - parentBorderTopWidth) > (parent.scrollTop + parent.clientHeight)," //$NON-NLS-1$
			+ "\n		overLeft = this.offsetLeft - parent.offsetLeft < parent.scrollLeft," //$NON-NLS-1$
			+ "\n		overRight = (this.offsetLeft - parent.offsetLeft + this.clientWidth - parentBorderLeftWidth) > (parent.scrollLeft + parent.clientWidth)," //$NON-NLS-1$
			+ "\n		alignWithTop = overTop && !overBottom;" //$NON-NLS-1$
			+ "\n		if ((overTop || overBottom) && centerIfNeeded) {" //$NON-NLS-1$
			+ "\n			parent.scrollTop = this.offsetTop - parent.offsetTop - parent.clientHeight / 2 - parentBorderTopWidth + this.clientHeight / 2;" //$NON-NLS-1$
			+ "\n		}" //$NON-NLS-1$
			+ "\n		if ((overLeft || overRight) && centerIfNeeded) {" //$NON-NLS-1$
			+ "\n			parent.scrollLeft = this.offsetLeft - parent.offsetLeft - parent.clientWidth / 2 - parentBorderLeftWidth + this.clientWidth / 2;" //$NON-NLS-1$
			+ "\n		}" //$NON-NLS-1$
			+ "\n		if ((overTop || overBottom || overLeft || overRight) && !centerIfNeeded) {" //$NON-NLS-1$
			+ "\n			this.scrollIntoView(alignWithTop);" //$NON-NLS-1$
			+ "\n		}" //$NON-NLS-1$
			+ "\n	};" //$NON-NLS-1$
			+ "\n}" //$NON-NLS-1$
			+ "\n} catch(e) {Element.prototype.scrollIntoViewIfNeeded = Element.prototype.scrollIntoView}"; //$NON-NLS-1$

	protected boolean fastWordHighLight = SynopticEditionPreferences.getInstance().getBoolean(SynopticEditionPreferences.FAST_HIGHLIGHT);

	private ArrayList<ProgressListener> beforeHighlighListeners;

	public ArrayList<ProgressListener> getBeforeHighlighListeners() {

		return beforeHighlighListeners;
	}

	private ArrayList<ProgressListener> afterHighlighListeners;

	private CommandLink cmdLink;

	private EditionLink editionLink;

	public ArrayList<ProgressListener> getAfterHighlighListeners() {

		return beforeHighlighListeners;
	}

	public Object evaluate(String code) {

		if (Log.getLevel().intValue() <= Level.FINEST.intValue()) System.out.println(code);

		try {
			if (getBrowser() == null) return null;
			return getBrowser().evaluate(code);
		}
		catch (Exception e) {
			Log.severe(NLS.bind(EditionUIMessages.BrowserEvaluateErrorP0, e));
			Log.printStackTrace(e);
		}
		return null;
	}

	public boolean execute(String code) {

		if (Log.getLevel().intValue() <= Level.FINEST.intValue()) System.out.println(code);
		try {
			//System.out.println("EXECUTE: "+code.replace("\n", " RET ").subSequence(0, Math.min(200, code.length())));
			return getBrowser().execute(code);
		}
		catch (Throwable e) {
			Log.severe(NLS.bind(EditionUIMessages.BrowserExecuteErrorP0, e));
			Log.printStackTrace(e);
		}
		return false;
	}

	public EditionPanel(Composite parent, SynopticEditionEditor synopticEditionEditor, int style, String editionName) {

		super(parent, style);
		this.synopticEditionEditor = synopticEditionEditor;
		this.editionName = editionName;
		// this.currentEdition = edition;
		// this.currentText = currentEdition.getText();

		File functionsFile = null;
		try {
			Bundle bundle = Platform.getBundle("org.txm.edition.rcp"); //$NON-NLS-1$
			URL fileURL = bundle.getEntry("res/js/functions.js"); //$NON-NLS-1$
			URL r = FileLocator.resolve(fileURL);
			Log.fine("Load JS functions from: " + r); //$NON-NLS-1$
			functions = IOUtils.getText(r, "UTF-8"); //$NON-NLS-1$
		}
		catch (Exception e) {
			Log.severe(EditionUIMessages.bind(EditionUIMessages.error_while_readingP0P1, functionsFile, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}

		beforeHighlighListeners = new ArrayList<>();
		afterHighlighListeners = new ArrayList<>();

		//		this.browser.addLocationListener(new LocationListener() {
		//			
		//			@Override
		//			public void changing(LocationEvent event) {
		//				
		//				// TODO Auto-generated method stub
		//				
		//			}
		//			
		//			@Override
		//			public void changed(LocationEvent event) {
		//				if (cmdLink != null) cmdLink.dispose();
		//				if (editionLink != null) cmdLink.dispose();
		//			}
		//		});


		this.addDisposeListener(new DisposeListener() { // clean the BrowserFunction s

			@Override
			public void widgetDisposed(DisposeEvent e) {
				//				System.out.println("dispose listener called");
				if (cmdLink != null) cmdLink.dispose();
				if (editionLink != null) editionLink.dispose();
				if (browser != null && !browser.isDisposed()) browser.dispose();
			}
		});

		if (cmdLink == null) {
			cmdLink = new CommandLink(synopticEditionEditor, browser);
		}
		if (editionLink == null) {
			editionLink = new EditionLink(synopticEditionEditor, browser);
		}
		//		
		//		browser.addLocationListener(new LocationListener() {
		//			
		//			@Override
		//			public void changing(LocationEvent event) { }
		//			
		//			@Override
		//			public void changed(LocationEvent event) { // need to update the BrowserFunctions when a page is 
		//				if (cmdLink != null) {
		//					cmdLink.dispose();
		//				}
		//				cmdLink = new CommandLink(synopticEditionEditor, browser);
		//				if (editionLink != null) {
		//					editionLink.dispose();
		//				}
		//				editionLink = new EditionLink(synopticEditionEditor, browser);
		//			}
		//		});

		progressListener = new ProgressListener() {

			@Override
			public void changed(ProgressEvent event) {
			}

			@Override
			public synchronized void completed(ProgressEvent event) {

				Browser browser = getBrowser();
				Object o = browser.evaluate("return typeof " + CommandLink.FCT + ";"); //$NON-NLS-1$ //$NON-NLS-2$
				if ("undefined".equals(o)) { //$NON-NLS-1$
					if (cmdLink != null) {
						cmdLink.dispose();
					}
					cmdLink = new CommandLink(synopticEditionEditor, browser);
					//System.out.println("rebuild cmdLink");
				}
				o = browser.evaluate("return typeof " + EditionLink.FCT + ";"); //$NON-NLS-1$ //$NON-NLS-2$
				if ("undefined".equals(o)) { //$NON-NLS-1$
					if (editionLink != null) {
						editionLink.dispose();
					}
					editionLink = new EditionLink(synopticEditionEditor, browser);
					//System.out.println("rebuild editionLink");
				}

				EditionPanel.this.getBrowser().execute("""
						document.addEventListener('mouseover', function (e) {
						   document.currentHoverElement = e.target
						});	""");



				Object rez = evaluate("return typeof sheet;"); //$NON-NLS-1$
				if ("undefined".equals(rez)) { //$NON-NLS-1$
					Object loadResult = evaluate(functions);
					Object testloadResult = evaluate("return typeof txmGetSelection;"); //$NON-NLS-1$
					if (loadResult == null || testloadResult == null || "undefined".equals(testloadResult.toString())) { // build the sheet if not present in the HTML DOM //$NON-NLS-1$
						Log.severe("JS execution (init) error with=" + functions); //$NON-NLS-1$
					}
					else {
						Log.finer("JS functions loaded."); //$NON-NLS-1$
					}
				}

				// do something before the highlight is done
				for (ProgressListener pl : beforeHighlighListeners) {
					pl.completed(event);
				}

				// System.out.println("highlight: "+highlightedColorPerWordIDS);
				StringBuilder buffer = new StringBuilder();
				// buffer.append("alert(\"\"+sheet);\n");
				if (!fastWordHighLight) {
					// for (String wordid : highlightedColorPerWordIDS.keySet()) {
					// buffer.append(String.format(clearhighlightscript, wordid)+"\n");
					// }
				}
				else {
					buffer.append("try { while(sheet.cssRules.length > 0) sheet.deleteRule(0); } catch (e) {};\n"); // empty style //$NON-NLS-1$
				}

				if (buffer.length() > 0) {
					if (!execute(buffer.toString())) {
						Log.fine("JS execution error (delete cssRules) with=" + buffer); //$NON-NLS-1$
					}
				}


				buffer = new StringBuilder();
				Log.finest("EditionPanel " + currentEdition + " reload " + currentPage); //$NON-NLS-1$ //$NON-NLS-2$
				if (currentPage != null && currentEdition != null) { // avoid null pointer

					String firstWord = currentPage.getWordId();
					Page nextPage = currentEdition.getNextPage(currentPage);
					String lastWord = "w_999999999"; //$NON-NLS-1$
					while (nextPage != null && currentPage != nextPage) {
						lastWord = nextPage.getWordId();
						if (!("w_0".equals(lastWord))) { // this is a word id //$NON-NLS-1$
							break; // ok
						}
						Page p = currentEdition.getNextPage(nextPage);
						if (p == nextPage) { // end of pages
							lastWord = "w_999999999"; //$NON-NLS-1$
							break;
						}
						nextPage = p;
					}
					// System.out.println("firstWord="+firstWord+" lastWord="+lastWord);
					// int n = 0;
					// long start = System.currentTimeMillis();

					for (String wordid : highlightedColorPerWordIDS.keySet()) {
						// Log.finest("words to highlight: "+highlightedColorPerWordIDS);
						if (highlightedColorPerWordIDS.get(wordid).size() == 0) continue; // error

						// if (!slowWordHighLight) { //TODO: this code must be replaced with word position instead of word id comparaison
						if (Edition.isFirstGTthanSecond(wordid, firstWord) < 0) continue; // skip the word to highlight
						if (Edition.isFirstGTthanSecond(lastWord, wordid) < 0) continue; // skip the word to highlight
						// }
						// n++;
						RGBA composite = new RGBA(0, 0, 0, 0f);
						int size = highlightedColorPerWordIDS.get(wordid).size();

						for (RGBA color : highlightedColorPerWordIDS.get(wordid)) {
							composite.r += color.r;
							composite.g += color.g;
							composite.b += color.b;
							composite.a += color.a;
						}

						composite.r /= size;
						composite.g /= size;
						composite.b /= size;

						String s = null;
						if (!fastWordHighLight) {
							// s = String.format(highlightscriptRule, wordid, composite.r, composite.g, composite.b, composite.a);
							s = "try { document.getElementById(\"" + wordid + "\").style.backgroundColor=\"rgba(" + composite.r + "," + composite.g + "," + composite.b + "," + composite.a + ")\";" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
									+
							// "elt.style.paddingLeft=\"3px\";" + //$NON-NLS-1$
							// "elt.style.paddingRight=\"3px\";" + //$NON-NLS-1$
							// "elt.style.paddingTop=\"1px\";" + //$NON-NLS-1$
							// "elt.style.paddingBottom=\"1px\";" + //$NON-NLS-1$
									"} catch (e) { };"; //$NON-NLS-1$
						}
						else {
							// TODO: replace this line with the next one for TXM 0.7.9
							s = String.format(highlightscriptRuleFast, wordid.replace(" ", "\\ "), composite.r, composite.g, composite.b, composite.a); //$NON-NLS-1$ //$NON-NLS-2$
							// s = String.format(highlightscriptRuleFast, wordid, composite.r, composite.g, composite.b, composite.a);
						}

						buffer.append(s + "\n"); //$NON-NLS-1$
					}
				}

				if (buffer.length() > 0) {
					if (!execute(buffer.toString())) {
						Log.fine("JS execution (highlight) error with=" + buffer); //$NON-NLS-1$
					}
				}


				buffer = new StringBuilder();

				// for (String wordidslist : highlightedColorSpans.keySet()) {
				// RGBA composite = new RGBA(0,0,0,0f);
				// int size = highlightedColorSpans.get(wordidslist).size();
				//
				// for (RGBA color : highlightedColorSpans.get(wordidslist)) {
				// composite.r += color.r;
				// composite.g += color.g;
				// composite.b += color.b;
				// composite.a += color.a;
				// }
				//
				// composite.r /= size;
				// composite.g /= size;
				// composite.b /= size;
				//
				// String cmd = "var l = "+wordidslist+";\n";
				// cmd += "wrap(l, \""+wordidslist+"\";\n";
				// buffer.append(cmd+"\n");
				// }

				for (String wordid : fontColorPerWordIDS.keySet()) {
					RGBA composite = new RGBA(0, 0, 0, 0f);
					int size = fontColorPerWordIDS.get(wordid).size();

					for (RGBA color : fontColorPerWordIDS.get(wordid)) {
						composite.r += color.r;
						composite.g += color.g;
						composite.b += color.b;
						composite.a += color.a;
					}

					composite.r /= size;
					composite.g /= size;
					composite.b /= size;

					String s = String.format(colorscriptRuleFast, wordid, composite.r, composite.g, composite.b, composite.a);
					buffer.append(s + "\n"); //$NON-NLS-1$
				}

				if (buffer.length() > 0) {
					if (!execute(buffer.toString())) {
						Log.fine("JS execution (color) error with=" + buffer); //$NON-NLS-1$
					}
				}


				buffer = new StringBuilder();

				for (String wordid : fontSizePerWordIDS.keySet()) {
					String s = String.format(sizescriptRuleFast, wordid, fontSizePerWordIDS.get(wordid));
					buffer.append(s + "\n"); //$NON-NLS-1$
				}

				for (String wordid : fontWeightPerWordIDS.keySet()) {
					String s = null;
					if (!fastWordHighLight) {
						// s = String.format(highlightscriptRule, wordid, composite.r, composite.g, composite.b, composite.a);
						s = "try { document.getElementById(\"" + wordid + "\").style.fontWeight=\"" + fontWeightPerWordIDS.get(wordid) + "\";" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								+
						// "elt.style.paddingLeft=\"3px\";" + //$NON-NLS-1$
						// "elt.style.paddingRight=\"3px\";" + //$NON-NLS-1$
						// "elt.style.paddingTop=\"1px\";" + //$NON-NLS-1$
						// "elt.style.paddingBottom=\"1px\";" + //$NON-NLS-1$
								"} catch (e) { };"; //$NON-NLS-1$
						// s = String.format(highlightscriptRule, wordid, composite.r, composite.g, composite.b, composite.a);
						// s = "try { var elt = document.getElementById(\"" + wordid + "\");" + //$NON-NLS-1$ //$NON-NLS-2$
						// "elt.style.fontWeight=\""+fontWeightPerWordIDS.get(wordid)+"\";" + //$NON-NLS-1$
						//// "elt.style.paddingLeft=\"3px\";" + //$NON-NLS-1$
						//// "elt.style.paddingRight=\"3px\";" + //$NON-NLS-1$
						//// "elt.style.paddingTop=\"1px\";" + //$NON-NLS-1$
						//// "elt.style.paddingBottom=\"1px\";" + //$NON-NLS-1$
						// "} catch (e) { };"; //$NON-NLS-1$
					}
					else {
						s = String.format(weightscriptRuleFast, wordid, fontWeightPerWordIDS.get(wordid));
					}
					buffer.append(s + "\n"); //$NON-NLS-1$
				}

				for (String wordid : fontFamillyPerWordIDS.keySet()) {
					String s = String.format(famillyscriptRuleFast, wordid, fontFamillyPerWordIDS.get(wordid));
					buffer.append(s + "\n"); //$NON-NLS-1$
				}

				if (buffer.length() > 0) {
					if (!execute(buffer.toString())) {
						Log.fine("JS execution (font) error with=" + buffer); //$NON-NLS-1$
					}
				}


				buffer = new StringBuilder();

				if (focusedWordID != null) {
					// System.out.println("Focus on: "+focusedWordID);
					String s = "try { showElementIfNeeded(document.getElementById(\"" + focusedWordID + "\")); } catch (e) {document.getElementById(\"" + focusedWordID + "\").scrollIntoView();};"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					//String s = "document.getElementById(\"" + focusedWordID + "\").scrollIntoView();"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					buffer.append(s + "\n"); //$NON-NLS-1$
				}

				if (buffer.length() > 0) {
					//Object rez2 = execute(buffer.toString());
					if (execute(buffer.toString())) {
						focusedWordID = null; // focus one time
					}
					else {
						Log.fine("JS execution (focus) error with script=" + buffer); //$NON-NLS-1$
					}
				}

				buffer = new StringBuilder();

				if (highlightedAreas != null) {
					for (int[] pos : highlightedAreas) {
						if (pos == null || pos.length != 4) continue;
						String s = "try {v.setFocusOn(" + pos[0] + ", " + pos[1] + ", " + pos[2] + ", " + pos[3] + ");} catch (e) { };"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
						buffer.append(s + "\n"); //$NON-NLS-1$
						break;// one word only for now
					}
				}

				// System.out.println(buffer);
				if (buffer.length() > 0) {
					if (!execute(buffer.toString())) {
						Log.fine("JS execution (area focus) error with=" + buffer); //$NON-NLS-1$
					}
				}
				// System.out.println("n="+n+" time="+(System.currentTimeMillis()-start));

				// do something after the highlight is done
				for (ProgressListener pl : afterHighlighListeners) {
					pl.completed(event);
				}
			}
		};

		getBrowser().addOpenWindowListener(new OpenWindowListener() {

			@Override
			public void open(WindowEvent event) {

				if (org.txm.utils.OSDetector.isFamilyUnix()) {
					event.browser = getBrowser();
					event.required = false;
				}
			}
		});

		getBrowser().addMouseListener(new MouseListener() {

			boolean dblClick = false;

			boolean debug = true;

			private int t, x, y;

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				if (debug) Log.finest("DOUBLE CLICK"); //$NON-NLS-1$
				dblClick = true;
			}

			@Override
			public void mouseDown(MouseEvent e) {

				if (debug) Log.finest("MOUSE DOWN"); //$NON-NLS-1$
				dblClick = false;
				t = e.time;
				x = e.x;
				y = e.y;
			}

			@Override
			public void mouseUp(MouseEvent e) {

				if (dblClick) { // doucle click raised by mouseDoubleClick
					dblClick = false;
					return; // stop right now ! :-o
				}
				if (debug) Log.finest("MOUSE UP"); //$NON-NLS-1$
				EditionPanel panel = EditionPanel.this;

				// filter click that are not a left simple click (no drag)
				// System.out.println("click count="+e.count+" button="+e.button+" time="+e.time+" diff="+(e.time-t)+" dist="+(Math.abs(e.x - x) + Math.abs(e.y - y)));
				if (e.count > 1) {
					if (debug) Log.finest(" DOUBLE CLICK"); //$NON-NLS-1$
					// System.out.println("not a simple click");
					return;
				}

				int dist = Math.abs(e.x - x) + Math.abs(e.y - y);
				int deltat = (e.time - t);
				// System.out.println("deltat ="+deltat);
				if (dist >= 0 && (deltat > 0 || dist > 0)) {
					// System.out.println(" DRAG dist="+dist+" deltat="+deltat);
					String[] ids = panel.getWordSelection(); // may be null

					panel.expandSelectionTo(ids);
					return;
				}
			}
		});

		browser.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						TXMBrowserEditor.zoomIn(getBrowser());
					}
					else {
						TXMBrowserEditor.zoomOut(getBrowser());
					}
				}
			}
		});

		KeyListener kListener = new KeyListener() {

			boolean debug = true;

			@Override
			public void keyReleased(KeyEvent e) {

				if (debug) Log.finest(" KEY RELEASED: " + e); //$NON-NLS-1$
			}

			@Override
			public void keyPressed(KeyEvent e) {

				if (debug) Log.finest(" KEY PRESSED: " + e); //$NON-NLS-1$
				if (e.keyCode == 'f' && (e.stateMask & SWT.CTRL) != 0) {
					synopticEditionEditor.getSearchEditionToolbar().openSearch(getTextSelection());
				}
			}
		};
		getBrowser().setCapture(true);
		getBrowser().addKeyListener(kListener);

		getBrowser().addProgressListener(progressListener);
	}

	/**
	 * Bypass BrowserViewer restriction on the getBrowser method
	 */
	@Override
	public Browser getBrowser() {

		return super.getBrowser();
	}

	/**
	 * Bypass BrowserViewer restriction on the getBrowser method
	 */
	public String getDOM() {

		return "";// execute("return document.innerHTML"; //$NON-NLS-1$
	}

	public ISelectionProvider getSelectionProvider() {

		return selProvider;
	}

	public void setHighlightWordsById(RGBA color, HashSet<String> wordids) {

		for (String wordid : wordids) {
			if (this.highlightedColorPerWordIDS.get(wordid) != null) {
				removeHighlightWordsById(color, wordid);
			}
			ArrayList<RGBA> hs = new ArrayList<>();
			this.highlightedColorPerWordIDS.put(wordid, hs);
			hs.add(color);
		}
	}

	public void addHighlightWordsById(RGBA color, String wordid) {

		if (!this.highlightedColorPerWordIDS.containsKey(wordid)) {
			this.highlightedColorPerWordIDS.put(wordid, new ArrayList<RGBA>());
		}
		this.highlightedColorPerWordIDS.get(wordid).add(color);
	}

	/**
	 * Can be called to refresh the page word styles but only when the page is loaded
	 */
	public void updateWordStyles() {

		progressListener.completed(null);
	}

	public void addHighlightWordsById(RGBA color, Collection<String> wordids) {

		for (String wordid : wordids) {
			addHighlightWordsById(color, wordid);
		}
	}

	public void removeHighlightWordsById(RGBA color, Collection<String> wordids) {

		// System.out.println("Call removeHighlightWordsById: "+wordids+" color="+color);
		StringBuffer buffer = new StringBuffer();
		for (String wordid : wordids) {
			removeHighlightWordsById(color, wordid);
			if (!fastWordHighLight) {
				buffer.append(String.format(clearhighlightscript, wordid) + "\n"); //$NON-NLS-1$
			}
		}

		if (buffer.length() > 0) execute(buffer.toString());
	}

	public void removeHighlightWordsById(RGBA color, String wordid) {

		if (highlightedColorPerWordIDS.get(wordid) == null) return; // nothing to do

		// System.out.println("remove color="+color+" from id="+wordid);
		this.highlightedColorPerWordIDS.get(wordid).remove(color);

		if (this.highlightedColorPerWordIDS.get(wordid).size() == 0) {
			this.highlightedColorPerWordIDS.remove(wordid);
		}
	}

	/**
	 * Open the page containing the word with ID=$line_wordid
	 * 
	 * @param line_wordid
	 * @return true if the page changed
	 */
	public boolean backToText(Text text, String line_wordid) {

		// String name = currentEdition.getName();
		Edition edition = text.getEdition(editionName);

		if (edition == null) {
			String s = EditionUIMessages.bind(EditionUIMessages.noEditionWithNameEqualsP0AvailableForTextEqualsP1, editionName, text);
			Log.warning(s);
			StatusLine.setMessage(s);
			Log.severe(s);
		}
		else {
			Page openedPage = null;
			if (line_wordid != null && line_wordid.length() > 0) {
				openedPage = edition.getPageForWordId(line_wordid);
			}
			else {
				openedPage = edition.getFirstPage();
			}
			if (currentPage == null || !openedPage.equals(currentPage)) {
				this.showPage(openedPage);
				return true;
			}
			else {
				updateWordStyles(); // no need to change the page, but still update the focus&styles
			}
		}

		return false;
	}

	/**
	 * First page.
	 */
	public void firstPage() {

		if (currentEdition == null) return;

		// System.out.println(Messages.TxmBrowser_1+currentPage);
		currentPage = currentEdition.getFirstPage();
		currentText = currentEdition.getText();
		if (currentPage == null) {
			System.out.println(EditionUIMessages.bind(EditionUIMessages.failToRetrieveFirstPageOfEditionEqualsP0AndTextEqualsP1, currentEdition.getName(), currentText.getName()));
			return;
		}

		reloadPage();

		// page_label.setText(makePageLabel());
	}

	public static String fromEncodeString = "\"-&-'-<->-`- -¡-¢-£-¥-§-©-«-®-°-±-´-µ-¶-·-»-¿-À-Á-Â-Ã-Ä-Æ-Ç-È-É-Ê-Ë-Ì-Í-Î-Ï-Ñ-Ò-Ó-Ô-Õ-Ö-Ø-Ù-Ú-Û-Ü-ß-à-á-â-ã-ä-å-æ-ç-è-é-ê-ë-ì-í-î-ï-ð-ñ-ò-ó-ô-õ-ö-÷-ø-ù-ú-û-ü-ý-ÿ-Ā-ā-ĉ-Č-č-ē-ĝ-ģ-ĥ-Ī-ī-ĵ-ő-œ-ŕ-ř-ŝ-Š-š-Ū-ū-ŭ-ű-ŵ-ŷ-π-ḧ-ẅ-ẍ-ẑ-ỳ- - -–-—-‘-’-•-′-″-€-™-←-↑-→-↓-↖-↗-↘-↙-⌘-⏏-☑-✔"; //$NON-NLS-1$

	public static String toEncodeString = "%22-%26-%27-%3C-%3E-%60-%C2%A0-%C2%A1-%C2%A2-%C2%A3-%C2%A5-%C2%A7-%C2%A9-%C2%AB-%C2%AE-%C2%B0-%C2%B1-%C2%B4-%C2%B5-%C2%B6-%C2%B7-%C2%BB-%C2%BF-%C3%80-%C3%81-%C3%82-%C3%83-%C3%84-%C3%86-%C3%87-%C3%88-%C3%89-%C3%8A-%C3%8B-%C3%8C-%C3%8D-%C3%8E-%C3%8F-%C3%91-%C3%92-%C3%93-%C3%94-%C3%95-%C3%96-%C3%98-%C3%99-%C3%9A-%C3%9B-%C3%9C-%C3%9F-%C3%A0-%C3%A1-%C3%A2-%C3%A3-%C3%A4-%C3%A5-%C3%A6-%C3%A7-%C3%A8-%C3%A9-%C3%AA-%C3%AB-%C3%AC-%C3%AD-%C3%AE-%C3%AF-%C3%B0-%C3%B1-%C3%B2-%C3%B3-%C3%B4-%C3%B5-%C3%B6-%C3%B7-%C3%B8-%C3%B9-%C3%BA-%C3%BB-%C3%BC-%C3%BD-%C3%BF-%C4%80-%C4%81-%C4%89-%C4%8C-%C4%8D-%C4%93-%C4%9D-%C4%A3-%C4%A5-%C4%AA-%C4%AB-%C4%B5-%C5%91-%C5%93-%C5%95-%C5%99-%C5%9D-%C5%A0-%C5%A1-%C5%AA-%C5%AB-%C5%AD-%C5%B1-%C5%B5-%C5%B7-%CF%80-%E1%B8%A7-%E1%BA%85-%E1%BA%8D-%E1%BA%91-%E1%BB%B3-%E2%80%82-%E2%80%83-%E2%80%93-%E2%80%94-%E2%80%98-%E2%80%99-%E2%80%A2-%E2%80%B2-%E2%80%B3-%E2%82%AC-%E2%84%A2-%E2%86%90-%E2%86%91-%E2%86%92-%E2%86%93-%E2%86%96-%E2%86%97-%E2%86%98-%E2%86%99-%E2%8C%98-%E2%8F%8F-%E2%98%91-%E2%9C%94"; //$NON-NLS-1$

	private static String[] fromEncode, toEncode;
	static { // GTK webkit URL encoding differs from Java File.toURI() encoding
		if ("gtk".equals(System.getProperty("osgi.ws"))) { //$NON-NLS-1$ //$NON-NLS-2$
			fromEncode = fromEncodeString.split("-"); //$NON-NLS-1$
			toEncode = toEncodeString.split("-"); //$NON-NLS-1$
		}
	}

	public static String encodeURL(String url) {
		for (int i = 0; i < fromEncode.length; i++) {
			url = url.replace(fromEncode[i], toEncode[i]);
		}
		return url;
	}

	@Override
	public void setURL(String url) {

		if (!this.browser.isDisposed()) {
			if (fromEncode != null) {
				url = encodeURL(url);
			}
			super.setURL(url);

			//			if ("webkit".equals(this.browser.getBrowserType())) {
			//				String u = URLUtils.encodeURL(url);
			//				Log.finer("setting encoded URL: "+u);
			//				super.setURL(u);
			//			} else {
			//				Log.finer("setting URL: "+url);
			//				super.setURL(url);
			//			}
			//			
			//			// fix when the webbrowser can't open the URL -> but encoding&security are not well managed
			//			URL url2;
			//			try {
			//				url2 = new URL(url);
			//				String content = IOUtils.getText(url2, "UTF-8");
			//				super.getBrowser().setText(content);
			//			}
			//			catch (Exception e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}

		}
	}

	/**
	 * Last page.
	 */
	public void lastPage() {
		// System.out.println(Messages.TxmBrowser_2+currentPage);
		if (currentEdition == null) return;

		if (currentPage != null && currentPage.getFile().equals(currentEdition.getLastPage().getFile())) {
			return;
		}
		currentPage = currentEdition.getLastPage();
		currentText = currentEdition.getText();
		reloadPage();
	}

	/**
	 * Previous page.
	 */
	public void previousPage() {

		if (currentEdition == null) return;

		// System.out.println(Messages.TxmBrowser_3+currentPage);
		if (currentPage != null) {
			Page previous = currentEdition.getPreviousPage(currentPage);

			if (previous == currentPage) {
				previousText(true);
			}
			else {
				currentPage = previous;
				reloadPage();
			}
			currentText = currentEdition.getText();
		}
		else {
			firstPage();
		}
		// page_label.setText(makePageLabel());
	}



	/**
	 * Next page.
	 */
	public void nextPage() {

		if (currentEdition == null) return;

		// System.out.println(Messages.TxmBrowser_4+currentPage);
		if (currentPage != null) {
			Page next = currentEdition.getNextPage(currentPage);
			if (next == currentPage) {
				nextText();
			}
			else {
				currentPage = next;
				reloadPage();
			}
			currentText = currentEdition.getText();
		}
		else {
			firstPage();
		}
		// page_label.setText(makePageLabel());
	}


	public void firstText() {

		// Text current = this.currentPage.getEdition().getText();
		Project project = currentText.getProject();
		String id;
		try {
			id = this.synopticEditionEditor.getCorpus().getCorpusTextIdsList()[0];
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return;
		}
		Text firstText = project.getText(id);

		if (firstText != null) {
			String editionName = currentEdition.getName();
			Edition tmp = firstText.getEdition(editionName);
			if (tmp == null) {
				System.out.println(EditionUIMessages.bind(EditionUIMessages.noEditionWithNameEqualsP0AvailableForTextEqualsP1, editionName, firstText.getName()));
				return;
			}
			currentEdition = tmp;
			currentPage = currentEdition.getFirstPage();
			currentText = firstText;
			if (currentPage == null) {
				StatusLine.setMessage("No previous text"); //$NON-NLS-1$
				return;
			}
			reloadPage();
		}
	}

	/**
	 * Next text.
	 */
	public void lastText() {

		// Text current = this.currentPage.getEdition().getText();
		Project project = currentText.getProject();
		String id;
		try {
			String[] ids = this.synopticEditionEditor.getCorpus().getCorpusTextIdsList();
			id = ids[ids.length - 1];
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return;
		}
		Text lastText = project.getText(id);

		if (lastText != null) {
			String editionName = currentEdition.getName();
			Edition tmp = lastText.getEdition(editionName);
			if (tmp == null) {
				System.out.println(EditionUIMessages.bind(EditionUIMessages.noEditionWithNameEqualsP0AvailableForTextEqualsP1, editionName, lastText.getName()));
				return;
			}

			currentEdition = tmp;
			currentPage = currentEdition.getFirstPage();
			currentText = lastText;
			if (currentPage == null) {
				StatusLine.setMessage("No text next"); //$NON-NLS-1$
				return;
			}

			reloadPage();
			// page_label.setText(makePageLabel());
		}
		// System.out.println("Next texts "+nextText);
	}

	/**
	 * Previous text.
	 * 
	 * @param fromNextText if true it means the user was reading the next text and then we show the last page of the previous text
	 */
	public void previousText(boolean fromNextText) {

		Project project = currentText.getProject();
		String id;
		try {
			String[] ids = this.synopticEditionEditor.getCorpus().getCorpusTextIdsList();
			String currentId = currentText.getName();
			int idx = 0;
			for (int i = 0; i < ids.length; i++) {
				if (currentId.equals(ids[i])) {
					idx = i;
					break;
				}
			}
			if (idx > 0) idx--;
			id = ids[idx];
		}
		catch (Exception e) {
			e.printStackTrace();
			return;
		}
		Text previousText = project.getText(id);

		if (previousText != null) {
			String editionName = currentEdition.getName();

			Edition tmp = previousText.getEdition(editionName);
			if (tmp == null) {
				System.out.println(EditionUIMessages.bind(EditionUIMessages.noEditionWithNameEqualsP0AvailableForTextEqualsP1, editionName, previousText.getName()));
				return;
			}
			currentEdition = tmp;
			currentText = previousText;
			if (fromNextText) {
				currentPage = currentEdition.getLastPage();
			}
			else {
				currentPage = currentEdition.getFirstPage();
			}

			if (currentPage == null) {
				StatusLine.setMessage("No previous text"); //$NON-NLS-1$
				return;
			}
			reloadPage();
		}
		// System.out.println("Previous texts "+previousText);
	}

	/**
	 * Next text.
	 */
	public Text getNextText() {

		// Text current = this.currentPage.getEdition().getText();
		Project project = currentText.getProject();
		String id;

		String[] ids;
		try {
			ids = this.synopticEditionEditor.getCorpus().getCorpusTextIdsList();

			String currentId = currentText.getName();
			int idx = 0;
			for (int i = 0; i < ids.length; i++) {
				if (currentId.equals(ids[i])) {
					idx = i;
					break;
				}
			}
			if (idx < ids.length - 1) idx++;
			id = ids[idx];
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return currentText;
		}


		return project.getText(id);
	}

	/**
	 * Next text.
	 */
	public void nextText() {

		Text nextText = getNextText();

		if (nextText != null) {
			String editionName = currentEdition.getName();
			Edition tmp = nextText.getEdition(editionName);
			if (tmp == null) {
				System.out.println(EditionUIMessages.bind(EditionUIMessages.noEditionWithNameEqualsP0AvailableForTextEqualsP1, editionName, nextText.getName()));
				return;
			}
			currentEdition = tmp;

			currentPage = currentEdition.getFirstPage();
			currentText = nextText;
			if (currentPage == null) {
				StatusLine.setMessage("No text next"); //$NON-NLS-1$
				return;
			}

			reloadPage();
			// page_label.setText(makePageLabel());
		}
		// System.out.println("Next texts "+nextText);
	}


	public void setText(Text text, boolean refresh) {

		// String editionName = currentEdition.getName();
		Edition tmp = text.getEdition(this.editionName);
		if (tmp == null) {
			System.out.println(EditionUIMessages.bind(EditionUIMessages.noEditionWithNameEqualsP0AvailableForTextEqualsP1, this.editionName, text.getName()));
			return;
		}

		currentEdition = tmp;

		currentText = text;

		if (refresh) {
			currentPage = currentEdition.getFirstPage();
			if (currentPage == null) {
				StatusLine.setMessage("No text next"); //$NON-NLS-1$
				return;
			}
			reloadPage();
		}
		// page_label.setText(makePageLabel());
	}

	/**
	 * Gets the current page.
	 *
	 * @return the current page
	 */
	public Page getCurrentPage() {

		return currentPage;
	}

	/**
	 * Gets the current text.
	 *
	 * @return the current page
	 */
	public Text getCurrentText() {

		return currentText;
	}

	/**
	 * Make page label.
	 *
	 * @return the string
	 */
	public String makePageLabel() {

		return currentPage.getName() + " / " + currentPage.getEdition().getNumPages(); //$NON-NLS-1$
	}

	/**
	 * Show page.
	 *
	 * @param page the page
	 */
	public void showPage(Page page) {

		if (currentEdition == null) return;
		// System.out.println("SHOW PAGE "+page);
		currentEdition = page.getEdition();
		currentText = currentEdition.getText();
		currentPage = page;
		reloadPage();
	}

	/**
	 * Reload the browser with the currentPage URL
	 */
	public void reloadPage() {

		this.setURL(currentPage.toURL());
	}

	// /**
	// * Sets the edition.
	// *
	// * @param edition the new edition
	// */
	// public void setEdition(Edition edition) {
	// this.currentEdition = edition;
	// }

	String EMPTYSELECTIONTEST = "if (typeof window.getSelection != \"undefined\") {" + // modern Web browsers //$NON-NLS-1$
			"var sel = window.getSelection();" + //$NON-NLS-1$
			"return sel.rangeCount == 0" + //$NON-NLS-1$
			"} else if (typeof document.selection != \"undefined\") {" + // for IE < 11 //$NON-NLS-1$
			"if (document.selection.type == \"Text\") {" + //$NON-NLS-1$
			"return document.selection.createRange().htmlText.length() == 0;" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"return false"; //$NON-NLS-1$

	String SCRIPT01 = "var html = \"\";" + //$NON-NLS-1$
			"if (typeof window.getSelection != \"undefined\") {" + // modern Web browsers //$NON-NLS-1$
			"var sel = window.getSelection();" + //$NON-NLS-1$
			"if (sel.rangeCount) {" + //$NON-NLS-1$
			"var container = document.createElement(\"div\");" + //$NON-NLS-1$
			"for (var i = 0, len = sel.rangeCount; i < len; ++i) {" + //$NON-NLS-1$
			"container.appendChild(sel.getRangeAt(i).cloneContents());" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"html = container.innerHTML;" + //$NON-NLS-1$
			"container = null;" + //$NON-NLS-1$
			//"container.parentNode.removeChild(container);" +
			"}" + //$NON-NLS-1$
			"} else if (typeof document.selection != \"undefined\") {" + // for IE < 11 //$NON-NLS-1$
			"if (document.selection.type == \"Text\") {" + //$NON-NLS-1$
			"html = document.selection.createRange().htmlText;" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"}" + //$NON-NLS-1$
			"return html"; //$NON-NLS-1$

	public String getTextSelectionDOM() {

		String t = ((String) evaluate(SCRIPT01));
		Log.finest("HTML text selection=" + t); //$NON-NLS-1$
		return t;
	}

	/**
	 * 
	 * @return array of 2 word id, if only one word is selected 2nd value is null
	 */
	public String[] getWordSelection() {

		// System.out.println("getWordSelection");
		String SCRIPT02_getspans = ""// functions //$NON-NLS-1$
				+ "var all = [];" //$NON-NLS-1$
				+ "\nvar sel = txmGetSelection();" //$NON-NLS-1$
				+ "\nif (sel.rangeCount == 0) {return all;}" //$NON-NLS-1$
				// +"\nalert('sel='+sel);"
				+ "\nvar range = txmGetRange(sel, 0);" //$NON-NLS-1$
				// +"\nalert('range='+range);"
				+ "\nvar frag = txmGetFragment(range);" //$NON-NLS-1$
				// +"\nalert('frag='+frag);"
				+ "\nvar children = txmGetChildren(frag);" //$NON-NLS-1$
				// +"\nalert('children='+children);"
				// +"\nalert('children.length='+children.length);"
				+ "\nif ((children.length == 0) || (children.length == 1 && children.item(0).nodeType == 3)) {" //$NON-NLS-1$
				+ "\n	var parent = txmGetParentElementRange(range);" //$NON-NLS-1$
				// +"\n if (parent instanceof HTMLHtmlElement) {alert('HTMLHtmlElement parent='+parent);return findIdsInString(parent, all);}"
				+ "\n	if (typeof parent.getAttribute == 'undefined') {return all}" //$NON-NLS-1$
				+ "\n	var id = parent.getAttribute('id');" //$NON-NLS-1$
				+ "\n	if (txmGetTagName(parent) == 'SPAN' && id != null && id.indexOf('w_') == 0) {" //$NON-NLS-1$
				+ "\n		all.push(id);" //$NON-NLS-1$
				+ "\n	} else {" // try with the parent's parent //$NON-NLS-1$
				+ "\n		var parent = txmGetParent(parent);" //$NON-NLS-1$
				// +"\n alert('text selection, parent='+txmGetTagName(parent))"
				+ "\n	if (typeof parent.getAttribute == 'undefined') {return all}" //$NON-NLS-1$
				+ "\n		var id = parent.getAttribute('id');" //$NON-NLS-1$
				+ "\n		if (txmGetTagName(parent) == 'SPAN' && id != null && id.indexOf('w_') == 0) {" //$NON-NLS-1$
				+ "\n			all.push(id);" //$NON-NLS-1$
				+ "\n		}" //$NON-NLS-1$
				+ "\n	}" //$NON-NLS-1$
				+ "\n} else {" //$NON-NLS-1$
				+ "\n	findSpans(children, all);" //$NON-NLS-1$
				+ "\n}" //$NON-NLS-1$
				// +"alert('result='+all)"
				+ "\nreturn all;"; //$NON-NLS-1$

		// System.out.println(functions);
		// System.out.println(SCRIPT02_getspans);

		Object ret = evaluate(SCRIPT02_getspans);
		if (ret instanceof Object[]) {
			Log.finer("selection word ids=" + Arrays.toString((Object[]) ret)); //$NON-NLS-1$
			Object[] array = (Object[]) ret;
			String[] sel = new String[2];
			if (array.length == 0) {
				return null;
			}
			else if (array.length == 1) {
				sel[0] = array[0].toString();
				return sel;
			}
			else {
				sel[0] = array[0].toString();
				sel[1] = array[array.length - 1].toString();
				return sel;
			}
		}
		else {
			Log.severe(EditionUIMessages.bind(EditionUIMessages.sCRIPTRETURNEDEqualsP0, ret));
			return null;
		}
	}

	public boolean isTextSelectionEmpty() {
		return browser.evaluate(EMPTYSELECTIONTEST).equals(true);
	}

	public String getTextSelection() {

		// System.out.println("DOM="+getTextSelectionDOM());
		String dom = getTextSelectionDOM();
		if (dom == null) return ""; //$NON-NLS-1$

		String rez = dom.replaceAll("<[^>]+>", ""); //$NON-NLS-1$ //$NON-NLS-2$
		// System.out.println("STR="+rez);
		return rez;
	}

	@Override
	public void addSelectionChangedListener(ISelectionChangedListener listener) {
	}

	@Override
	public ISelection getSelection() {

		return new SynopticTextSelection(this);
	}

	@Override
	public void removeSelectionChangedListener(
			ISelectionChangedListener listener) {
	}

	@Override
	public void setSelection(ISelection selection) {
	}

	static public class SynopticTextSelection implements ISelection {

		String str;

		public SynopticTextSelection(EditionPanel panel) {

			this.str = panel.getTextSelection();
		}

		@Override
		public boolean isEmpty() {

			return str != null && str.length() > 0;
		}

		public String getTextSelection() {

			return str;
		}
	}

	public Edition getEdition() {

		return currentEdition;
	}

	public MenuManager getMenuManager() {

		return menuManager;
	}

	public void initMenu() {

		// create a new menu
		menuManager = new MenuManager();

		Menu menu = menuManager.createContextMenu(this.getBrowser());

		menu.addMenuListener(new MenuListener() {

			@Override
			public void menuShown(MenuEvent e) {

				Menu menu = menuManager.getMenu();
				if (menu == null) return;
				new MenuItem(menu, SWT.SEPARATOR);

				MenuItem searchItem = new MenuItem(menu, SWT.NONE);
				searchItem.setText(EditionUIMessages.Search);
				searchItem.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						String text = EditionPanel.this.getTextSelection();
						synopticEditionEditor.getSearchEditionToolbar().openSearch(text);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

				String dom = EditionPanel.this.getTextSelectionDOM();
				if (dom != null && dom.length() > 0) {
					MenuItem copyItem = new MenuItem(menu, SWT.NONE);
					copyItem.setText(EditionUIMessages.copyTextSelection);
					copyItem.addSelectionListener(new SelectionListener() {

						@Override
						public void widgetSelected(SelectionEvent e) {

							String text = EditionPanel.this.getTextSelection();
							if (text != null && text.length() > 0) {
								IOClipboard.write(text);
							}
						}

						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
						}
					});

					Object currentHref = EditionPanel.this.getBrowser().evaluate("return document.currentHoverElement.getAttribute('href')");
					if (currentHref != null) {
						MenuItem copyLink = new MenuItem(menu, SWT.NONE);
						copyLink.setText("Copy link");
						copyLink.addSelectionListener(new SelectionListener() {

							@Override
							public void widgetSelected(SelectionEvent e) {

								String text = EditionPanel.this.getTextSelectionDOM();
								if (text != null && text.length() > 0) {

									if (currentHref != null) {
										IOClipboard.write(currentHref.toString());
									}
								}
							}

							@Override
							public void widgetDefaultSelected(SelectionEvent e) {
							}
						});
					}
				}


				MenuItem zoomInItem = new MenuItem(menu, SWT.NONE);
				zoomInItem.setText(EditionUIMessages.ZoomIn);
				zoomInItem.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						TXMBrowserEditor.zoomIn(browser);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

				MenuItem zoomOutItem = new MenuItem(menu, SWT.NONE);
				zoomOutItem.setText(EditionUIMessages.ZoomOut);
				zoomOutItem.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						TXMBrowserEditor.zoomOut(browser);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

				MenuItem zoomResetItem = new MenuItem(menu, SWT.NONE);
				zoomResetItem.setText(EditionUIMessages.InitialZoom);
				zoomResetItem.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						TXMBrowserEditor.zoomReset(browser);
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

				MenuItem reloadItem = new MenuItem(menu, SWT.NONE);
				reloadItem.setText(EditionUIMessages.reload);
				reloadItem.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						EditionPanel.this.getBrowser().refresh();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
				MenuItem backItem = new MenuItem(menu, SWT.NONE);
				backItem.setText(EditionUIMessages.back);
				backItem.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						EditionPanel.this.getBrowser().back();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
				MenuItem forwardItem = new MenuItem(menu, SWT.NONE);
				forwardItem.setText(EditionUIMessages.forward);
				forwardItem.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {

						EditionPanel.this.getBrowser().forward();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});

				MenuItem propertiesItem = new PagePropertiesMenu(getBrowser(), menu, SWT.NONE);
			}

			@Override
			public void menuHidden(MenuEvent e) {
				// TODO Auto-generated method stub
			}
		});
		// // restore old listeners
		// Menu origMenu = this.browser.getMenu();
		// if (origMenu != null) {
		// if (origMenu.getListeners(SWT.Hide) != null)
		// for (Listener listener : origMenu.getListeners(SWT.Hide)) menu.addListener(SWT.Hide, listener);
		// if (origMenu.getListeners(SWT.Show) != null)
		// for (Listener listener : origMenu.getListeners(SWT.Show)) menu.addListener(SWT.Show, listener);
		// for (MenuItem origItem : origMenu.getItems()) { // restore old menu items
		// MenuItem item = new MenuItem(menu, origItem.getStyle());
		// item.setText(origItem.getText());
		// item.setImage(origItem.getImage());
		// if (origItem.getListeners(SWT.Selection) != null && origItem.getListeners(SWT.Selection).length > 0)
		// item.addListener(SWT.Selection, origItem.getListeners(SWT.Selection)[0]);
		// }
		// }
		// replace the menu
		this.getBrowser().setMenu(menu);
	}

	public void initSelectionProvider() {

		selProvider = new ISelectionProvider() {

			@Override
			public void setSelection(ISelection selection) {
			}

			@Override
			public void removeSelectionChangedListener(ISelectionChangedListener listener) {
			}

			@Override
			public ISelection getSelection() {

				ITextSelection sel = new ITextSelection() {

					@Override
					public boolean isEmpty() {
						if (EditionPanel.this.isDisposed()) return true;
						boolean r = EditionPanel.this.getTextSelection().length() == 0; // EditionPanel.this.isTextSelectionEmpty();
						//System.out.println("EMPTY? "+r);
						return r;
					}

					@Override
					public String getText() {
						if (EditionPanel.this.isDisposed()) return null;
						return EditionPanel.this.getTextSelection();
					}

					@Override
					public int getStartLine() {

						return 0;
					}

					@Override
					public int getOffset() {

						return 0;
					}

					@Override
					public int getLength() {

						return 0;
					}

					@Override
					public int getEndLine() {

						return 0;
					}
				};
				return sel;
			}

			@Override
			public void addSelectionChangedListener(ISelectionChangedListener listener) {
			}
		};
	}



	public void goToPage(String text, String name) {

		if (text != null) {
			Text newText = this.synopticEditionEditor.getCorpus().getProject().getText(text);
			if (newText == null) {
				return;
			}
			if (!newText.equals(currentText)) {
				setText(newText, false);
			}
		}

		Page next = currentEdition.getPageForName(name);
		// System.out.println(Messages.TxmBrowser_4+currentPage);
		if (next != null && !next.equals(this.currentPage)) {
			currentPage = next;
			reloadPage();
		}
		else {
			System.out.println(EditionUIMessages.bind(EditionUIMessages.pageNotFoundP0, name));
		}
	}

	public void setFocusedWordID(String focusid) {

		this.focusedWordID = focusid;
	}

	public void setHighlightedArea(HashSet<int[]> positions) {

		Log.finer("Highlight area: not implemented"); //$NON-NLS-1$
	}

	public void expandSelectionTo(String[] ids) {

		// expand selection
		if (ids == null) return;
		if (ids.length == 0) return;
		// TODO: enable the feature for Windows computers
		// if (System.getProperty("os.name").toLowerCase().contains("windows")) return;

		// this.getBrowser().evaluate("alert('expand selection');");
		// System.out.println("EXPAND SELECTION TO "+Arrays.toString(ids));
		String id1 = ids[0];
		String id2 = ids[1];
		if (id2 == null) id2 = id1;
		String SCRIPT02_expand = "" //$NON-NLS-1$
				+ "var elt = document.getElementById(\"" + id1 + "\");\n" //$NON-NLS-1$ //$NON-NLS-2$
				+ "var elt2 = document.getElementById(\"" + id2 + "\");\n" //$NON-NLS-1$ //$NON-NLS-2$
				// + "alert(\"elt=\"+elt);\n"
				+ "var sel = txmGetSelection();\n" //$NON-NLS-1$
				// + "alert(\"sel=\"+sel.extentNode);\n"
				// + "alert(\"typeof sel=\"+Object.getOwnPropertyNames(sel));\n"
				// + "alert(\"get_type sel=\"+get_type(sel));\n"
				+ "var range = txmGetRange(sel, 0);\n" //$NON-NLS-1$
				// + "alert(\"range=\"+range);\n"
				// + "alert(\"typeof range=\"+Object.getOwnPropertyNames(range));\n"
				// + "alert(\"get_type range=\"+get_type(range));\n"
				+ "range.setStart(elt, 0);\n" //$NON-NLS-1$
				+ "range.setEndAfter(elt2, 0);\n" //$NON-NLS-1$
				+ "sel.removeAllRanges();\n" //$NON-NLS-1$
				+ "sel.addRange(range);\n" //$NON-NLS-1$
				+ "\n" //$NON-NLS-1$
				+ "return null;"; //$NON-NLS-1$

		// System.out.println("Eval...\n"+SCRIPT02_expand);
		evaluate(SCRIPT02_expand);
		return;
	}

	public void addFontWeightWordsById(String weight, List<String> wordids) {

		for (String wordid : wordids) {
			addFontWeightWordsById(weight, wordid);
		}
	}

	public void addFontWeightWordsById(String weight, String wordid) {

		this.fontWeightPerWordIDS.put(wordid, weight);
	}

	public void removeFontWeightWordsById(String wordid) {

		this.fontWeightPerWordIDS.remove(wordid);
	}

	public void removeFontWeightWordsById(List<String> wordids) {

		for (String wordid : wordids) {
			removeFontWeightWordsById(wordid);
		}
	}

	/**
	 * Clears all highlighted words.
	 */
	public void removeHighlightWords() {

		// FIXME: SJ: this code leads to a concurrency modification exception on Windows
		// Set<String> keys = highlightedColorPerWordIDS.keySet();
		// for (String key : keys) {
		// highlightedColorPerWordIDS.remove(key);
		// }

		// SJ: new version tests
		highlightedColorPerWordIDS.clear();



	}
}
