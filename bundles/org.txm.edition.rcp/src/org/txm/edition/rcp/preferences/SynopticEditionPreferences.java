package org.txm.edition.rcp.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SynopticEditionPreferences extends TXMPreferences {

	/**
	 * 
	 */
	public static final String FAST_HIGHLIGHT = "fast_highlight"; //$NON-NLS-1$

	/**
	 * One of the EModelService position constant: OVER = -1, ABOVE = 0, BELOW = 1, LEFT = 2, RIGHT = 3.
	 */
	public static final String BACKTOTEXT_POSITION = "backtotext_position"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {

		if (!TXMPreferences.instances.containsKey(SynopticEditionPreferences.class)) {
			new SynopticEditionPreferences();
		}
		return TXMPreferences.instances.get(SynopticEditionPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(FAST_HIGHLIGHT, true);
		preferences.putInt(BACKTOTEXT_POSITION, 0); // above position
	}
}
