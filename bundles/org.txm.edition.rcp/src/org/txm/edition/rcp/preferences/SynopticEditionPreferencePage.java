package org.txm.edition.rcp.preferences;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.txm.edition.rcp.messages.EditionUIMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;

/**
 * Edition preference page.
 *
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SynopticEditionPreferencePage extends TXMPreferencePage {

	private BooleanFieldEditor fast_highlight;

	private ComboFieldEditor backtotext_position;

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(SynopticEditionPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(EditionUIMessages.edition);
		this.setImageDescriptor(IImageKeys.getImageDescriptor(SynopticEditionPreferencePage.class, "icons/edition.png")); //$NON-NLS-1$
	}

	@Override
	protected void createFieldEditors() {
		fast_highlight = new BooleanFieldEditor(SynopticEditionPreferences.FAST_HIGHLIGHT, EditionUIMessages.enableFastWordHighlight, getFieldEditorParent());
		addField(fast_highlight);

		String[][] values = {
				{ EditionUIMessages.preference_split_position_Over, "-1" },  //$NON-NLS-1$
				{ EditionUIMessages.preference_split_position_Ratio, "-2" },  //$NON-NLS-1$
				{ EditionUIMessages.preference_split_position_Above, "" + EModelService.ABOVE },  //$NON-NLS-1$
				{ EditionUIMessages.preference_split_position_Below, "" + EModelService.BELOW },  //$NON-NLS-1$
				{ EditionUIMessages.preference_split_position_Left, "" + EModelService.LEFT_OF },  //$NON-NLS-1$
				{ EditionUIMessages.preference_split_position_Right, "" + EModelService.RIGHT_OF } }; //$NON-NLS-1$

		backtotext_position = new ComboFieldEditor(SynopticEditionPreferences.BACKTOTEXT_POSITION, EditionUIMessages.LinkedEditorOpeningPosition, values, getFieldEditorParent());
		backtotext_position.setToolTipText(EditionUIMessages.determinesThePositionOfTheNewWindow);
		addField(backtotext_position);

		Label note = new Label(getFieldEditorParent(), SWT.NONE);
		note.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		note.setText(EditionUIMessages.preference_split_position_Description);
	}

	public boolean performOk() {
		boolean b = super.performOk();
		String v = SynopticEditionPreferences.getInstance().getString(SynopticEditionPreferences.BACKTOTEXT_POSITION);
		return b;
	}
}
