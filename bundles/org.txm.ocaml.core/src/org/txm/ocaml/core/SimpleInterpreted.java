package org.txm.ocaml.core;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.ocamljava.runtime.support.scripting.OCamlContext;

public final class SimpleInterpreted {

	private static final String SCRIPT = "external get_binding : string -> 'a = \"script_get_binding\";;\n" +
			"let n = Int32.to_int (get_binding \"repetitions\");;\n" +
			"let s : string = get_binding \"message\";;\n" +
			"for i = 1 to n do print_endline s done;;\n";

	public static void main(final String[] args) throws Throwable {
		final ScriptEngineManager manager = new ScriptEngineManager();
		final ScriptEngine engine = manager.getEngineByName("OCaml");
		final ScriptContext ctxt = new OCamlContext();
		ctxt.getBindings(ScriptContext.ENGINE_SCOPE).put("repetitions", 3);
		ctxt.getBindings(ScriptContext.ENGINE_SCOPE).put("message", "hello!!!");
		engine.eval(SCRIPT, ctxt);
	}
}
