package org.txm.ocaml.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.script.ScriptContext;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.ocamljava.runtime.support.scripting.OCamlContext;
import org.txm.core.engines.ScriptEngine;
import org.txm.utils.io.IOUtils;

public class OcamlScriptEngine extends ScriptEngine {

	public OcamlScriptEngine() {
		super("ocaml", "ml");
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public IStatus executeScript(File script, HashMap<String, Object> env, List<String> stringArgs) {
		String cmd;
		try {
			cmd = IOUtils.getText(script, "UTF-8");
			return executeText(cmd, env);
		}
		catch (IOException e) {
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}
	}

	@Override
	public IStatus executeText(String cmd, HashMap<String, Object> env) {

		final ScriptEngineManager manager = new ScriptEngineManager();
		final javax.script.ScriptEngine engine = manager.getEngineByName("OCaml");
		final ScriptContext ctxt = new OCamlContext();
		ctxt.getBindings(ScriptContext.ENGINE_SCOPE).put("env", env);
		try {

			engine.eval(cmd, ctxt);
			return Status.OK_STATUS;
		}
		catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}
	}

	private static IStatus run(ArrayList<String> cmd, HashMap<String, Object> env) {
		return Status.OK_STATUS;
	}

	public static void main(String args[]) {
		ArrayList<String> cmd = new ArrayList<String>();
		run(cmd, null);
	}
}
