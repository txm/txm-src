package org.txm.ahc.core.functions;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.commons.lang3.StringUtils;
import org.txm.core.results.TXMResult;
import org.txm.utils.io.IOUtils;

/**
 * Base class for AHC data information result.
 *
 */
public abstract class AHCInfos extends TXMResult {

	protected LinkedHashMap<String, Object[]> infos;

	public AHCInfos(AHC parent) {

		this(null, parent);
	}

	public AHCInfos(String parametersNodePath) {

		this(parametersNodePath, null);
	}

	public AHCInfos(String parametersNodePath, AHC parent) {

		super(parametersNodePath, parent);
		this.setVisible(false);
	}

	@Override
	public boolean saveParameters() throws Exception {

		return true;
	}

	@Override
	public boolean loadParameters() throws Exception {

		return true;
	}

	@Override
	public void clean() {

	}

	@Override
	public AHC getParent() {
		return (AHC) super.getParent();
	}


	public abstract String getFirstColumnName();

	@Override
	public String getSimpleName() {
		return parent.getSimpleName();
	}

	@Override
	public boolean canCompute() throws Exception {

		return true;
	}

	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}

	@Override
	protected boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		PrintWriter writer = IOUtils.getWriter(outfile, encoding);
		
		ArrayList<String> titles2 = new ArrayList<String>();
		for (String t : getInfoNames()) {
			titles2.add(t.replace(colseparator, colseparator + colseparator));
		}
		writer.println(StringUtils.join(titles2, colseparator));
		
		for (String d : infos.keySet()) {
			Object[] line = infos.get(d);
			writer.println(d + colseparator + StringUtils.join(line, colseparator));
		}
		writer.close();
		return false;
	}

	public abstract String[] getInfoNames();

	@Override
	public String getResultType() {
		return "AHC infos"; //$NON-NLS-1$
	}

	public HashMap<String, Object[]> getInfos() {
		return infos;
	}

	public Object[] getInfos(String name) {
		if (name == null) return new Object[0];
		return infos.get(name);
	}

}
