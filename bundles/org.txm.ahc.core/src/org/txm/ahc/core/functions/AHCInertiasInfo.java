package org.txm.ahc.core.functions;

import java.util.LinkedHashMap;

import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.utils.TXMProgressMonitor;

/**
 * Stores inertia gains.
 * 
 * 
 * @author mdecorde
 *
 */
public class AHCInertiasInfo extends AHCInfos {

	public AHCInertiasInfo(AHC parent) {

		super(parent);
	}

	public AHCInertiasInfo(String parametersNodePath) {

		super(parametersNodePath);
	}

	public AHCInertiasInfo(String parametersNodePath, AHC parent) {

		super(parametersNodePath, parent);
	}

	@Override
	public String getDetails() {
		return AHCCoreMessages.bind(AHCCoreMessages.P0Informations, AHCCoreMessages.nodes);
	}

	@Override
	public String getName() {
		
		return AHCCoreMessages.nodes;
	}

	
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		if (infos == null) infos = new LinkedHashMap<>();
		infos.clear();

		double[] inertia = getParent().getInertiaGains();
		double sum = 0.0d;
		double max = 0d;
		double min = Double.MAX_VALUE;
		for (double d : inertia) {
			sum += d;
			if (max < d) max = d;
			if (min > d) min = d;
		}
		min = min/2d; // avoid starting from 0
		
		for (int i = 0; i < inertia.length; i++) {

			String s = Integer.toString(i + 1);
			if (!infos.containsKey(s)) {
				infos.put(s, new Object[2]);
			}
			infos.get(s)[0] = inertia[i]/sum * 100; // for display
			double rez = (inertia[i] - min) / (max - min); // relative position between min and max
			infos.get(s)[1] = rez; // for bar display
		}

		return true;
	}

	@Override
	public String getFirstColumnName() {
		return AHCCoreMessages.nodes;
	}

	/**
	 * Gets information names. One info per cluster: inertia.
	 */
	@Override
	public final String[] getInfoNames() {
		return new String[] { "Inertia", "Histogram" }; //$NON-NLS-1$
	}
}
