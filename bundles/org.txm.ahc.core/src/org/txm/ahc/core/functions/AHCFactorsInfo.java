package org.txm.ahc.core.functions;

import java.util.LinkedHashMap;

import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.utils.TXMProgressMonitor;

/**
 * Grouped variables informations (vtest score) per cluster, no info in a cell is null
 * 
 * 
 * @author mdecorde
 *
 */
public class AHCFactorsInfo extends AHCInfos {

	public AHCFactorsInfo(AHC parent) {

		super(parent);
	}

	public AHCFactorsInfo(String parametersNodePath) {

		super(parametersNodePath);
	}

	public AHCFactorsInfo(String parametersNodePath, AHC parent) {

		super(parametersNodePath, parent);
	}

	@Override
	public String getDetails() {
		return AHCCoreMessages.bind(AHCCoreMessages.P0Informations, AHCCoreMessages.factors);
	}

	@Override
	public String getName() {
		return AHCCoreMessages.factors;
	}

	
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		int n = getParent().getNumberOfClusters();

		if (infos == null) infos = new LinkedHashMap<>();
		infos.clear();

		for (int icluster = 1; icluster <= n; icluster++) {

			// v.test,Mean in category,Overall mean,sd in category,Overall sd
			double[] vtest1 = getParent().getClusterAxesInfo(icluster, "v.test"); //$NON-NLS-1$
			double[] vtest2 = getParent().getClusterAxesInfo(icluster, "Mean in category"); //$NON-NLS-1$
			double[] vtest3 = getParent().getClusterAxesInfo(icluster, "Overall mean"); //$NON-NLS-1$
			double[] vtest4 = getParent().getClusterAxesInfo(icluster, "sd in category"); //$NON-NLS-1$
			double[] vtest5 = getParent().getClusterAxesInfo(icluster, "Overall sd"); //$NON-NLS-1$

			int ivariable = 0;
			for (int v = 0; v < vtest1.length; v++) {
				String axe = "" + (v + 1);
				if (!infos.containsKey(axe)) {
					infos.put(axe, new Object[n * 5]);
				}
				int p = (icluster - 1) * 5;

				infos.get(axe)[p] = vtest1[ivariable];
				infos.get(axe)[p + 1] = vtest2[ivariable];
				infos.get(axe)[p + 2] = vtest3[ivariable];
				infos.get(axe)[p + 3] = vtest4[ivariable];
				infos.get(axe)[p + 4] = vtest5[ivariable];

				ivariable++;
			}
		}
		return true;
	}

	public String getFirstColumnName() {
		return AHCCoreMessages.factors;
	}

	/**
	 * one info per cluster: Intern %,glob %,Intern freq,Glob freq,p.value,v.test
	 */
	public final String[] getInfoNames() {

		// v.test,Mean in category,Overall mean,sd in category,Overall sd
		int n = getParent().getNumberOfClusters();
		String[] names = new String[n * 5];
		for (int i = 0; i < n; i++) {
			int p = i * 5;
			names[p] = "v.test " + (i + 1); //$NON-NLS-1$
			names[p + 1] = "Mean in category " + (i + 1); //$NON-NLS-1$
			names[p + 2] = "Overall mean " + (i + 1); //$NON-NLS-1$
			names[p + 3] = "sd in category " + (i + 1); //$NON-NLS-1$
			names[p + 4] = "Overall sd " + (i + 1); //$NON-NLS-1$
		}
		return names;
	}
}
