// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.ahc.core.functions;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPDouble;
import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.ahc.core.preferences.AHCPreferences;
import org.txm.ca.core.chartsengine.styling.CAStyler;
import org.txm.ca.core.functions.CA;
import org.txm.chartsengine.core.Theme;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Partitionable;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.StructuredPartition;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.PatternUtils;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * Agglomerative Hierarchical Clustering.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AHC extends ChartResult implements RResult, Partitionable, CAStyler {

	/**
	 * The R name prefix.
	 */
	protected static String prefixR = "FactoMineRAHC"; //$NON-NLS-1$

	/**
	 * Counter used in name suffix.
	 */
	protected static int ahcCounter = 1;


	/** The symbol. */
	String symbol = null;

	/** The CA symbol target. */
	String target;


	/**
	 * The coordinates of the clusters.
	 */
	protected double[][] clusterCoords = null;

	/**
	 * The numbers of the clusters.
	 */
	protected int[] clusterNumbers = null;

	/**
	 * The numbers of the clusters.
	 */
	protected String[] clusterNumbersNames = null;

	/**
	 * The names of the cluster rows.
	 */
	protected String[] treeLeavesLabels = null;

	/**
	 * The ordered names of the cluster rows.
	 */
	protected String[] treeLeavesOrdererLabels = null;

	/**
	 * The cluster tree heights.
	 */
	protected double[] treeMergesHeights = null;

	/**
	 * The cluster tree merges.
	 */
	protected double[][] treeMergesMatrix = null;

	/**
	 * The inertia gains.
	 */
	protected double[] inertiaGains = null;

	/**
	 * The metric (euclidean, manhattan).
	 */
	@Parameter(key = AHCPreferences.METRIC)
	protected String metric;

	/**
	 * The method (METHODS {@code <-} c("average", "single", "complete", "ward", "weighted", "flexible")).
	 */
	@Parameter(key = AHCPreferences.METHOD)
	protected String method;

	/**
	 * The number of clusters.
	 */
	@Parameter(key = AHCPreferences.N_CLUSTERS)
	protected int numberOfClusters;

	/**
	 * The number of clusters.
	 */
	@Parameter(key = AHCPreferences.CONSOLIDATION)
	protected Boolean pConsolidation;
	
	/**
	 * The number of clusters.
	 */
	@Parameter(key = AHCPreferences.N_MAX_CLUSTER_TO_PROCESS)
	protected int pMaximumColumnsNumbertoProcess;

	/**
	 * To compute the columns or the rows.
	 */
	@Parameter(key = AHCPreferences.COLUMNS_COMPUTING)
	protected Boolean columnsComputing;

	/**
	 * To display in 2D or 3D.
	 * TODO Tree/2D/3D
	 */
	//FIXME: SJ: should be done using ChartCreator
	@Parameter(key = AHCPreferences.RENDERING)
	protected String rendering;



	/**
	 * Instantiates a new AHC.
	 *
	 * @param parametersNodePath preference path
	 */
	public AHC(String parametersNodePath) {
		this(parametersNodePath, (LexicalTable) null);
	}

	/**
	 * Instantiates a new AHC.
	 *
	 * @param ca the ca
	 */
	public AHC(CA ca) {
		this(null, ca);
	}


	/**
	 * Instantiates a new AHC.
	 *
	 * @param ca the ca
	 */
	public AHC(String parametersNodePath, LexicalTable table) {
		super(parametersNodePath, table);
	}

	/**
	 * Instantiates a new AHC.
	 *
	 * @param ca the ca
	 */
	public AHC(LexicalTable table) {
		this(null, table);
	}


	/**
	 * Instantiates a new AHC.
	 *
	 * @param ca the ca
	 */
	public AHC(String parametersNodePath, CA ca) {
		super(parametersNodePath, ca);
	}

	@Override
	protected boolean __compute(TXMProgressMonitor monitor) {

		try {
			if (getParent() instanceof CA ca) {
				this.target = ca.getRSymbol();
			} else if (getParent() instanceof LexicalTable table) {
				this.target = table.getRSymbol();
			}

			// FIXME: SJ, 2024-11-14: useless?
			// reset the number of clusters to default if columns or rows computing has changed
			// if(this.hasParameterChanged(AHCPreferences.COLUMNS_COMPUTING)) {
			// this.numberOfClusters = AHCPreferences.getInstance().getInt(AHCPreferences.N_CLUSTERS);
			// }

			String columnsOrRows = "columns"; //$NON-NLS-1$
			if (!this.columnsComputing) {
				columnsOrRows = "rows"; //$NON-NLS-1$
			}

			if (this.symbol == null) {
				this.symbol = prefixR + (ahcCounter++);
			}
			else {
				try {
					RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(this.symbol);
				}
				catch (RWorkspaceException e) {
					Log.printStackTrace(e);
				}
			}

			// R will raise an error if kk >= number of columns
			int kk = pMaximumColumnsNumbertoProcess;
			
			if (getParent() instanceof CA ca) {
				if (kk >= ca.getColumnsCount()) {
					kk = Integer.MAX_VALUE; // deactivate
				}
			} else if (getParent() instanceof LexicalTable table) {
				if (kk >= table.getNColumns()) {
					kk = Integer.MAX_VALUE;  // deactivate
				}
			}
			
//			if (kk >= this.getParent().getColumnsCount()) {
//				kk = Integer.MAX_VALUE;
//			} else if (pConsolidation) {
//				Log.info("No AHC consolidation can be done if a k-means preprocess is done to limit the number of columns.");
//			}
			
			if (kk <= 1) { // avoid R error
				kk = Integer.MAX_VALUE;
			}
			
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("library(FactoMineR)"); //$NON-NLS-1$
			String t = this.target;
			if (getParent() instanceof LexicalTable) { // HCPC needs a data.frame of a factor result
				t = "data.frame(" + this.target + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				if ("columns".equals(columnsOrRows)) { //$NON-NLS-1$
					t = "data.frame(t(" + this.target + "))"; //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
			
			if (this.getParent() instanceof CA ca && this.numberOfClusters >= ca.getLexicalTable().getMinDimension()) {
				this.numberOfClusters = ca.getLexicalTable().getMinDimension() - 1; // ensure the number of cluster is < min LT dimensions
			}
			
			String s = this.symbol + " <- HCPC(" + t + //$NON-NLS-1$
					", cluster.CA=\"" + columnsOrRows + "\"" + //$NON-NLS-1$ //$NON-NLS-2$
					", nb.clust=" + this.numberOfClusters + //$NON-NLS-1$
					", metric=\"" + this.metric + "\"" + //$NON-NLS-1$ //$NON-NLS-2$
					", method=\"" + this.method + "\"" + //$NON-NLS-1$ //$NON-NLS-2$
					", consol=" + (pConsolidation ? "TRUE" : "FALSE") + //$NON-NLS-1$
					", kk=" + (kk == Integer.MAX_VALUE ? "Inf" : kk) + //$NON-NLS-1$
					", graph=FALSE)"; //$NON-NLS-1$
			rw.eval(s); //$NON-NLS-1$
			
//			if (kk != Integer.MAX_VALUE) { // restore tree labels ??
//				
//			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
			return false;
		}

		// Reset the cached data
		this.resetCache();
		return true;
	}


	/**
	 * 
	 * @param columns
	 * @param metric
	 * @param nCluster
	 * @param method
	 */
	public void setParameters(Boolean columns, String metric, Integer nCluster, String method) {
		if (columns != null) {
			this.columnsComputing = columns;
		}
		if (metric != null) {
			this.metric = metric;
		}
		if (nCluster != null) {
			this.numberOfClusters = nCluster;
		}
		if (method != null) {
			this.method = method;
		}
		this.setDirty(); //TODO check if this is still necessary
	}


	@Override
	public boolean loadParameters() {
		//FIXME: SJ, 2024-11-14: became useless?
		// if(this.ca == null) { // moved to ComputeAHC command
		// this.ca = (CA) this.getParent();
		// this.ca.setVisible(false); // hide the temporary CA
		// }
		return true;
	}

	@Override
	public boolean saveParameters() {
		// nothing to do
		return true;
	}

	@Override
	public boolean canCompute() {
				
		return Arrays.asList(AHC.getMethods()).indexOf(this.method) >= 0
				&& Arrays.asList(AHC.getMetrics()).indexOf(this.metric) >= 0
				&& this.numberOfClusters >= 2;
	}

	/**
	 * Resets the cached data so the next getter calls will request the values from R.
	 */
	public void resetCache() {
		this.clusterCoords = null;
		this.clusterNumbers = null;
		this.clusterNumbersNames = null;
		this.treeLeavesLabels = null;
		this.treeLeavesOrdererLabels = null;
		this.treeMergesHeights = null;
		this.treeMergesMatrix = null;
		this.inertiaGains = null;
	}

	@Override
	public String getRSymbol() {
		return symbol;
	}

	/**
	 * Gets the methods.
	 *
	 * @return the methods
	 */
	public static String[] getMethods() {
		String[] methods = { "average", "single", "complete", "ward", "weighted", "flexible" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		return methods;
	}

	/**
	 * Gets the metrics.
	 *
	 * @return the metrics
	 */
	public static String[] getMetrics() {
		String[] metrics = { "euclidean", "manhattan" }; //$NON-NLS-1$ //$NON-NLS-2$
		return metrics;
	}

	/**
	 *
	 * @param outfile
	 * @param encoding
	 * @return true if success
	 */
	// FIXME: SJ: to extract to future exporter extension
	@Override
	public boolean _toTxt(File outfile, String encoding, String colsep, String txtsep) {
		boolean ret = true;
		acquireSemaphore();
		RWorkspace rw;
		try {
			rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("sink(file=\"" + outfile.getAbsolutePath().replace("\\", "\\\\") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			rw.eval("print(\"Print " + symbol + "\")"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$

			String[][] vars = {
					{ "$data.clust", "dataset with the cluster of the individuals" }, //$NON-NLS-1$
					{ "$desc.var", "description of the clusters by the variables" }, //$NON-NLS-1$
					{ "$desc.axes", "description of the clusters by the dimensions" }, //$NON-NLS-1$
					{ "$desc.axes$quanti.var", "description of the cluster var. by the axes" }, //$NON-NLS-1$
					{ "$desc.axes$quanti", "description of the clusters by the axes" }, //$NON-NLS-1$
					{ "$desc.ind", "description of the clusters by the individuals" }, //$NON-NLS-1$
					{ "$desc.ind$para", "parangons of each clusters" }, //$NON-NLS-1$
					{ "$desc.ind$dist", "specific individuals" }, //$NON-NLS-1$
					{ "$call", "summary statistics" }, //$NON-NLS-1$
					{ "$call$t", "description of the tree" } //$NON-NLS-1$
			};
			for (String[] var : vars) {
				rw.eval("print(\"" + var[0] + " = " + var[1] + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
				rw.eval("print(" + symbol + var[0] + ")"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			//			rw.eval("print(" + symbol + "$call)"); //$NON-NLS-1$ //$NON-NLS-2$
			//			rw.eval("print(" + symbol + "$call$t$tree$merge)"); //$NON-NLS-1$ //$NON-NLS-2$
			//			rw.eval("print(" + symbol + "$call$t$tree$height)"); //$NON-NLS-1$ //$NON-NLS-2$
			//			rw.eval("print(" + symbol + "$call$t$res$var$coord)"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.eval("print(" + symbol + "$call$X$clust)"); //$NON-NLS-1$ //$NON-NLS-2$



			rw.eval("sink()"); //$NON-NLS-1$
		}
		catch (RWorkspaceException e) {
			Log.severe(AHCCoreMessages.bind(AHCCoreMessages.error_errorWhileExportingCAHToTXTP0, e.getMessage()));
			org.txm.utils.logger.Log.printStackTrace(e);
			ret = false;
		}
		finally {
			releaseSemaphore();
		}
		return ret;
	}


	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.txt" }; //$NON-NLS-1$
	}


	/**
	 * Gets the default method.
	 *
	 * @return the default method
	 */
	public static String getDefaultMethod() {
		return "ward"; //$NON-NLS-1$
	}

	/**
	 * Gets the default metric.
	 *
	 * @return the default metric
	 */
	public static String getDefaultMetric() {
		return "euclidean"; //$NON-NLS-1$
	}

	@Override
	public void clean() {
		try {
			RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(this.symbol);
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Gets the clusters coordinates.
	 * 
	 * @return the clusters coordinates
	 */
	public double[][] getClusterCoords() {
		if (this.clusterCoords == null) {
			try {

				// FIXME: SJ: check this code for the CAH 3D chart implementation
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();
				for (int i = 0; i < 2; i++) { // 2 dimensions
					REXP sv = rw.extractItemFromListByName(symbol, "data.clust[," + (i + 1) + "]"); //$NON-NLS-1$ //$NON-NLS-2$
					double[] coords = RWorkspace.toDouble(sv);
					for (int j = 0; j < coords.length; j++) {
						if (this.clusterCoords == null) {
							this.clusterCoords = new double[coords.length][2];
						}
						this.clusterCoords[j][i] = coords[j];
					}
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.clusterCoords;
	}


	/**
	 * Gets the clusters numbers.
	 * 
	 * @return clusters numbers
	 */
	public int[] getClusterNumbers() {
		if (this.clusterNumbers == null) {
			try {
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();
				// FIXME: SJ: not sorted version
				REXP sv = rw.extractItemFromListByName(symbol, "data$clust"); //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): data$clust = " + Arrays.toString(sv.asIntegers()));
				//				// FIXME: SJ: sorted version
				//				sv = rw.extractItemFromListByName(symbol, "data$clust[c(" + symbol + "$call$t$tree$order)]"); //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): data$clust ordered = " + Arrays.toString(sv.asIntegers()));
				//				// FIXME: SJ: other version
				//				sv = rw.extractItemFromListByName(symbol, "call$X$clust"); //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): call$X$clust = " + Arrays.toString(sv.asIntegers()));
				//				// FIXME: SJ: other version
				//				sv = rw.extractItemFromListByName(symbol, "call$X$clust[c(" + symbol + "$call$t$tree$order)]"); //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): call$X$clust ordered = " + Arrays.toString(sv.asIntegers()));
				//				
				//				
				this.clusterNumbers = sv.asIntegers();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.clusterNumbers;
	}

	/**
	 * Gets the clusters numbers.
	 * 
	 * @return clusters numbers
	 */
	public String[] getClusterNumbersNames() {
		if (this.clusterNumbersNames == null) {
			try {
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();
				// FIXME: SJ: not sorted version
				//REXP sv = rw.extractItemFromListByName(symbol, "data$clust"); //$NON-NLS-1$
				REXP sv = rw.eval("rownames(" + symbol + "$data)");  //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): data$clust = " + Arrays.toString(sv.asIntegers()));
				//				// FIXME: SJ: sorted version
				//				sv = rw.extractItemFromListByName(symbol, "data$clust[c(" + symbol + "$call$t$tree$order)]"); //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): data$clust ordered = " + Arrays.toString(sv.asIntegers()));
				//				// FIXME: SJ: other version
				//				sv = rw.extractItemFromListByName(symbol, "call$X$clust"); //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): call$X$clust = " + Arrays.toString(sv.asIntegers()));
				//				// FIXME: SJ: other version
				//				sv = rw.extractItemFromListByName(symbol, "call$X$clust[c(" + symbol + "$call$t$tree$order)]"); //$NON-NLS-1$
				//				System.err.println("AHC.getClusterNumbers(): call$X$clust ordered = " + Arrays.toString(sv.asIntegers()));
				//				
				//				
				this.clusterNumbersNames = sv.asStrings();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.clusterNumbersNames;
	}

	/**
	 * Gets the tree leaves labels.
	 * 
	 * @return the labels
	 */
	public String[] getTreeLeavesLabels() {
		if (this.treeLeavesLabels == null) {
			try {
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();

				// FIXME: SJ: old method, Save the row names in the R data frame
				// rw.safeEval(symbol + "$rownames <- rownames(" + symbol + "$data)");
				// REXP sv = rw.extractItemFromListByName(symbol, "rownames"); //$NON-NLS-1$


				REXP sv = rw.extractItemFromListByName(symbol, "call$t$tree$labels"); //$NON-NLS-1$


				this.treeLeavesLabels = sv.asStrings();

			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.treeLeavesLabels;
	}



	/**
	 * Gets the tree leaves labels ordered from left to right leaves.
	 * 
	 * @return the ordered labels
	 */
	public String[] getTreeLeavesOrderedLabels() {
		if (this.treeLeavesOrdererLabels == null) {
			try {
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();
				REXP sv = rw.extractItemFromListByName(symbol, "call$t$tree$labels[c(" + symbol + "$call$t$tree$order)]"); //$NON-NLS-1$ //$NON-NLS-2$
				this.treeLeavesOrdererLabels = sv.asStrings();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.treeLeavesOrdererLabels;
	}


	/**
	 * Gets the tree merges heights.
	 * 
	 * @return the heights of the merges
	 */
	public double[] getTreeMergesHeights() {
		if (this.treeMergesHeights == null) {
			try {
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();
				REXP sv = rw.extractItemFromListByName(symbol, "call$t$tree$height"); //$NON-NLS-1$
				this.treeMergesHeights = sv.asDoubles();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.treeMergesHeights;
	}


	/**
	 * Gets the tree merges/groups.
	 * The matrix is a table of pairs of integers that represent the merges/groups.
	 * Negative numbers represent a tree leaf and the number is the index of the item in the tree leaves unordered labels array.
	 * Positive numbers represent a merge/group number.
	 * eg. -4 1 pair means to link the leaf #4 to the merge/group #1.
	 * 
	 * @return clusters classes
	 */
	public double[][] getTreeMerges() {
		if (this.treeMergesMatrix == null) {
			try {
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();
				REXP sv = rw.extractItemFromListByName(symbol, "call$t$tree$merge"); //$NON-NLS-1$
				this.treeMergesMatrix = sv.asDoubleMatrix();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.treeMergesMatrix;
	}


	/**
	 * Gets the inertia gains.
	 * 
	 * @return the inertia gains
	 */
	// FIXME: SJ: for test purpose, this method is not used for the chart computing at this moment
	public double[] getInertiaGains() {
		if (this.inertiaGains == null) {
			try {
				RWorkspace rw = RWorkspace.getRWorkspaceInstance();
				REXP sv = rw.extractItemFromListByName(symbol, "call$t$inert.gain"); //$NON-NLS-1$
				// FIXME: tests
				// REXP sv = rw.extractItemFromListByName(symbol, "call$t$within"); //$NON-NLS-1$

				this.inertiaGains = sv.asDoubles();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		return this.inertiaGains;
	}



	/**
	 * Sets the metric to use for computing.
	 * 
	 * @param metric the metric to set
	 */
	public void setMetric(String metric) {
		this.metric = metric;
	}

	/**
	 * Sets the method to use for computing
	 * 
	 * @param method the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Sets the computing mode to columns or rows.
	 * 
	 * @param columnsComputing the computing mode
	 */
	public void setColumnsComputing(boolean columnsComputing) {
		this.columnsComputing = columnsComputing;
	}

	/**
	 * Sets the number of clusters to use for computing
	 * 
	 * @param nClusters the number of clusters
	 */
	public void setNumberOfClusters(int nClusters) {
		numberOfClusters = nClusters;
	}

	/**
	 * Checks if the computing mode is set as columns. If not, the computing mode is set as rows.
	 * 
	 * @return
	 */
	public boolean isColumnsComputing() {
		return columnsComputing;
	}

	/**
	 * Gets the maximum clusters available according to the current computing mode (columns or rows) and according to current source (<code>CA</code> or <code>LexicalTable</code>).
	 * The maximum numbers of available clusters is for now limited to 16 by this method due of a limitation of FactoMineR plot.HCPC().
	 * 
	 * @return the maximum clusters available according to the current computing mode
	 */
	public int getMaxClustersCount() {

		int maxClustersCount = 16;
		try {
			int n = -1;
			if (!this.columnsComputing) {
				n = this.getLexicalTable().getNRows() - 1; // -1 because HCPC can't make as much groups as rows
			}
			else {
				n = this.getLexicalTable().getNColumns() - 1; // -1 because HCPC can't make as much groups as cols
			}

			if (n >= 0 && n < maxClustersCount) {
				maxClustersCount = n;
			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}

		//FIXME: SJ: Debug
		// try {
		// RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		// REXP sv = rw.eval("min(length(" + symbol + "$data$clust) - 1, 16)"); //$NON-NLS-1$ //$NON-NLS-2$
		// maxClustersCount = sv.asInteger();
		// } catch(Exception e) {
		// System.out.println("Error: "+e.getLocalizedMessage());
		// Log.printStackTrace(e);
		// }

		// FIXME: SJ: FactoMineR plot.HCPC doesn't manage more than 16 clusters when tree.barplot=TRUE, therefore we can't plot a SVG with more than that, see ticket #836 then use the code below in
		// ChartsEngine to compute
		// the real maximum available clusters count
		// // CA
		// if(this.source instanceof CA) {
		// if(this.columns == true) {
		// maxClustersCount = ((CA)this.source).getColumnsCount() - 1;
		// }
		// else {
		// maxClustersCount = ((CA)this.source).getRowsCount() - 1;
		// }
		// }
		// // Lexical table
		// else if(this.source instanceof LexicalTable) {
		// if(this.columns == true) {
		// maxClustersCount = ((LexicalTable)this.source).getColumnsCount() - 1;
		// }
		// else {
		// maxClustersCount = ((LexicalTable)this.source).getRowsCount() - 1;
		// }
		// }

		return maxClustersCount;
	}



	/**
	 * Returns the number of clusters.
	 * 
	 * @return the number of clusters
	 */
	public int getNumberOfClusters() {
		return numberOfClusters;
	}

	/**
	 * Gets the parent lexical table.
	 * 
	 * @return the parent lexical table
	 */
	public LexicalTable getLexicalTable() {
		if (this.getParent() instanceof LexicalTable l) {
			return l;
		} else if (this.getParent() instanceof CA ca) {
			return ca.getLexicalTable();
		}
		
		return null;
	}


	@Override
	public String getSimpleName() {
		try {
			return this.getName();
		}
		catch (Exception e) {
			return this.getEmptyName();
		}
	}

	@Override
	public String getName() {
		return NLS.bind(AHCCoreMessages.P0classesOfP1, this.getNumberOfClusters(), this.columnsComputing ? AHCCoreMessages.columns : AHCCoreMessages.rows);
	}


	@Override
	public String getDetails() {
		return NLS.bind(this.getName() + AHCCoreMessages.AHC_computedWithTheP0MethodAndTheP1Metric, method, metric);
	}

	@Override
	public String getComputingStartMessage() {

		// from CA
		if (this.getParent().isVisible()) {
			return TXMCoreMessages.bind(AHCCoreMessages.info_ahcOfTheP0ca, this.getParent().getSimpleName());
		}
		// from lexical table
		else if (this.getLexicalTable().isVisible()) {
			return TXMCoreMessages.bind(AHCCoreMessages.info_ahcOfTheP0LexcialTable, this.getLexicalTable().getSimpleName());
		}
		else {

			// from partition
			if (this.getLexicalTable().getPartition() != null) {
				return TXMCoreMessages.bind(AHCCoreMessages.info_ahcOfTheP0PartitionCommaP1Property, this.getLexicalTable().getPartition().getSimpleName(), this.getLexicalTable().getProperty());
			}
			// from sub-corpus lexical table
			// FIXME: SJ: this special case is possible when creating Specificities then calling the command on the invisible Lexical Table
			// need to if we need a special log message here
			else {
				return ""; //$NON-NLS-1$
			}
		}
	}

	/**
	 * Gets the 2D rendering state.
	 * If false, the rendering will be done in 3D.
	 * 
	 * @return the rendering2D
	 */
	public boolean isRendering2D() {
		return this.rendering.equals("2D"); //$NON-NLS-1$
	}


	/**
	 * Sets the 2D rendering state.
	 * If set to false, the rendering will be done in 3D.
	 * 
	 * @param rendering2d the rendering2D to set
	 */
	public void setRendering2D(boolean rendering2d) {
		if (rendering2d) {
			this.rendering = "2D"; //$NON-NLS-1$
			//			this.setChartType("2D"); //$NON-NLS-1$
		}
		else {
			this.rendering = "3D"; //$NON-NLS-1$
			//		this.setChartType("3D");  //$NON-NLS-1$
		}
	}

	@Override
	public String getResultType() {
		return AHCCoreMessages.RESULT_TYPE;
	}

	/**
	 * Gets the parent lexical table unit property.
	 * 
	 * @return the parent lexical table unit property
	 */
	public Property getUnitProperty() {
		if (this.getLexicalTable() != null) {
			return this.getLexicalTable().getProperty();
		}
		return null;
	}

//	@Override
//	public CA getParent() {
//		return (CA) parent;
//	}

	@Override
	public String getNameForPartition() {

		return AHCCoreMessages.bind(AHCCoreMessages.clustersOfP0, this.getParent().getParent().getSimpleName());
	}

	@Override
	public String[] getNamesForPartition() {

		int n = this.getNumberOfClusters();
		String[] names = new String[n];
		for (int i = 0; i < n; i++) {
			names[i] = "C" + (i + 1); //$NON-NLS-1$
		}

		return names;
	}

	@Override
	public String[] getQueryStringsforPartition() {

		StructuredPartition partition = this.getFirstParent(StructuredPartition.class);
		if (partition == null) return null;

		StructuralUnitProperty sup = partition.getStructuralUnitProperty();
		if (sup == null) return null;

		List<List<String>> clusters = this.getClustersInd();
		if (clusters == null) {
			return null;
		}
		String[] queries = new String[clusters.size()];
		int i = 0;
		for (List<String> cluster : clusters) {
			queries[i++] = "<" + sup.getFullName() + "=\"" + StringUtils.join(cluster, "|") + "\"> [] expand to " + sup.getStructuralUnit().getName(); //$NON-NLS-1$
		}

		return queries;
	}

	/**
	 * 
	 * @return names per cluster
	 */
	public List<List<String>> getClustersInd() {

		//int[] RWorkspace.getRWorkspaceInstance().eval(this.getRSymbol()+"$call$X[\"clust\"]").asStrings();

		try {

			List<List<String>> rez = new ArrayList<>();
			int n = this.getNumberOfClusters();
			for (int i = 0; i < n; i++) {
				rez.add(new ArrayList<>());
			}

			String[] leafs = this.getClusterNumbersNames();
			int[] numbers = this.getClusterNumbers();
			for (int i = 0; i < leafs.length; i++) {
				rez.get(numbers[i] - 1).add(leafs[i]); // -1 because cluster numbering starts with 1
			}

			return rez;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public String[] getClusterVariables(int icluster) {
		// hcpc$desc.var$`2`[,"v.test"]
		try {
			return RWorkspace.getRWorkspaceInstance().eval("rownames(" + this.symbol + "$desc.var$`" + icluster + "`)").asStrings(); //$NON-NLS-1$
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new String[0];
		}
	}

	public double[] getClusterVariablesInfo(int icluster, String info) {
		// hcpc$desc.var$`2`[,"v.test"]
		try {
			REXP rez = RWorkspace.getRWorkspaceInstance().eval(this.symbol + "$desc.var$`" + icluster + "`[,\"" + info + "\"]"); //$NON-NLS-1$
			return rez.asDoubles();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new double[0];
		}
	}

	@Override
	public CQPCorpus getCorpus() {
		return this.getFirstParent(CQPCorpus.class);
	}

	@Override
	public HashMap<Pattern, HashMap<String, String>> getRowStyles() {

		HashMap<Pattern, HashMap<String, String>> styles = new HashMap<>();

		int clusters = getNumberOfClusters();
		Theme theme = new Theme();
		ArrayList<Color> colors = theme.getColorPaletteFor(clusters);

		HashMap<String, Integer> vars2Cluster = new HashMap<>();
		HashMap<String, Double> vars2Max = new HashMap<>();

		for (int iclust = 0; iclust < clusters; iclust++) {



			String[] vars = getClusterVariables(iclust + 1);
			double[] varsTest = getClusterVariablesInfo(iclust + 1, "v.test"); //$NON-NLS-1$

			for (int i = 0; i < vars.length; i++) {

				if (!vars2Cluster.containsKey(vars[i])) {
					vars2Cluster.put(vars[i], -1);
					vars2Max.put(vars[i], 2.0d); // BANALITY
				}

				if (vars2Max.get(vars[i]) < varsTest[i]) {
					vars2Cluster.put(vars[i], iclust);
					vars2Max.put(vars[i], varsTest[i]);
				}
			}
		}

		List<HashMap<String, String>> clusterToColor = new ArrayList<>();
		for (int iclust = 0; iclust < clusters; iclust++) {
			Color c = colors.get((iclust) % colors.size());
			String color = "" + c.getRed() + " " + c.getGreen() + " " + c.getBlue(); //$NON-NLS-1$

			HashMap<String, String> style = new HashMap<>();
			//			style.put("shape-color", color);
			style.put(StylingInstruction.LABEL_COLOR, color);

			clusterToColor.add(style);
		}
		HashMap<String, String> greyStyle = new HashMap<>();
		//		greyStyle.put("shape-color", "150 150 150");
		greyStyle.put(StylingInstruction.LABEL_COLOR, "150 150 150"); //$NON-NLS-1$

		for (String var : vars2Cluster.keySet()) {

			int iclust = vars2Cluster.get(var);
			HashMap<String, String> style = greyStyle;
			if (iclust >= 0) {
				style = clusterToColor.get(iclust);
			}

			styles.put(Pattern.compile(PatternUtils.quote(var)), style);
		}

		return styles;
	}

	@Override
	public HashMap<Pattern, HashMap<String, String>> getColStyles() {
		// set color styles : "shape-color" and "label-color"
		HashMap<Pattern, HashMap<String, String>> styles = new HashMap<>();

		List<List<String>> clusters = getClustersInd();
		Theme theme = new Theme();
		ArrayList<Color> colors = theme.getColorPaletteFor(clusters.size());
		int iclust = 0;
		for (List<String> cluster : clusters) {

			Color c = colors.get((iclust++) % colors.size());
			String color = "" + c.getRed() + " " + c.getGreen() + " " + c.getBlue(); //$NON-NLS-1$

			for (String point_label : cluster) {
				HashMap<String, String> style = new HashMap<>();
				styles.put(Pattern.compile(PatternUtils.quote(point_label)), style);

				//				style.put("shape-color", color);
				style.put(StylingInstruction.LABEL_COLOR, color);
			}
		}

		return styles;
	}

	public String[] getVariables(int icluster) {

		try {
			REXP rez = RWorkspace.getRWorkspaceInstance().eval("rownames(" + this.symbol + "$desc.var$`" + icluster + "`)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return rez.asStrings();
		}
		catch (Exception e) {
			e.printStackTrace();
			return new String[0];
		}
	}

	public String[] getIndividuals(int icluster) {

		try {
			REXP rez = RWorkspace.getRWorkspaceInstance().eval("names(" + this.symbol + "$desc.ind$para$`" + icluster + "`)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return rez.asStrings();
		}
		catch (Exception e) {
			e.printStackTrace();
			return new String[0];
		}
	}

	public double[] getClusterParangons(int icluster) {

		try { // hcpc$desc.ind$para$`1`
			REXP rez = RWorkspace.getRWorkspaceInstance().eval(this.symbol + "$desc.ind$para$`" + icluster + "`"); //$NON-NLS-1$ //$NON-NLS-2$
			if (rez.isList()) {
				return new double[] { ((REXPDouble) rez.asList().get(0)).asDouble() };
			}
			else {
				return rez.asDoubles();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return new double[0];
		}
	}

	public double[] getClusterDistances(int icluster) {

		try {
			REXP rez = RWorkspace.getRWorkspaceInstance().eval(this.symbol + "$desc.ind$dist$`" + icluster + "`"); //$NON-NLS-1$ //$NON-NLS-2$
			if (rez.isList()) {
				return new double[] { ((REXPDouble) rez.asList().get(0)).asDouble() };
			}
			else {
				return rez.asDoubles();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return new double[0];
		}
	}

	public double[] getClusterVariablesEta2Info() {

		try {
			REXP rez = RWorkspace.getRWorkspaceInstance().eval(this.symbol + "$desc.axes$quanti.var");  //$NON-NLS-1$
			if (rez.isList()) {
				return new double[] { ((REXPDouble) rez.asList().get(0)).asDouble() };
			}
			else {
				return rez.asDoubles();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return new double[0];
		}
	}

	public double[] getClusterAxesInfo(int icluster, String info) {

		// hcpc$desc.var$`2`[,"v.test"]
		try {
			REXP rez = RWorkspace.getRWorkspaceInstance().eval(this.symbol + "$desc.axes$quanti$`" + icluster + "`[,\"" + info + "\"]");  //$NON-NLS-1$  //$NON-NLS-2$  //$NON-NLS-3$
			return rez.asDoubles();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new double[0];
		}
	}

	public boolean getConsolidation() {
		return pConsolidation;
	}

}
