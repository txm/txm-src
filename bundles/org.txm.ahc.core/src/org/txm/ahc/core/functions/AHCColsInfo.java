package org.txm.ahc.core.functions;

import java.util.LinkedHashMap;

import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.utils.TXMProgressMonitor;

/**
 * Group variables informations (vtest score) per cluster, no info in a cell is null
 * 
 * 
 * @author mdecorde
 *
 */
public class AHCColsInfo extends AHCInfos {

	public AHCColsInfo(AHC parent) {

		super(parent);
	}

	public AHCColsInfo(String parametersNodePath) {

		super(parametersNodePath);
	}

	public AHCColsInfo(String parametersNodePath, AHC parent) {

		super(parametersNodePath, parent);
	}

	@Override
	public String getDetails() {
		return AHCCoreMessages.bind(AHCCoreMessages.P0Informations, AHCCoreMessages.columns);
	}

	
	@Override
	public String getName() {
		
		if (this.getParent().columnsComputing) {
			return AHCCoreMessages.rows;
		} else {
			return AHCCoreMessages.columns;
		}
	}

	
	public String getFirstColumnName() {
		return getName();
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		int n = getParent().getNumberOfClusters();

		if (infos == null) infos = new LinkedHashMap<>();
		infos.clear();

		for (int icluster = 1; icluster <= n; icluster++) {

			// Intern %,glob %,Intern freq,'Glob freq ',p.value,v.test
			double[] vtest1 = getParent().getClusterVariablesInfo(icluster, "Intern %"); //$NON-NLS-1$
			double[] vtest2 = getParent().getClusterVariablesInfo(icluster, "glob %"); //$NON-NLS-1$
			double[] vtest3 = getParent().getClusterVariablesInfo(icluster, "Intern freq"); //$NON-NLS-1$
			double[] vtest4 = getParent().getClusterVariablesInfo(icluster, "Glob freq "); //$NON-NLS-1$
			double[] vtest5 = getParent().getClusterVariablesInfo(icluster, "p.value"); //$NON-NLS-1$
			double[] vtest6 = getParent().getClusterVariablesInfo(icluster, "v.test"); //$NON-NLS-1$

			int ivariable = 0;
			for (String variable : getParent().getVariables(icluster)) {
				if (!infos.containsKey(variable)) {
					infos.put(variable, new Object[n * 6]);
				}
				int p = (icluster - 1) * 6;

				infos.get(variable)[p] = vtest1[ivariable];
				infos.get(variable)[p + 1] = vtest2[ivariable];
				infos.get(variable)[p + 2] = vtest3[ivariable];
				infos.get(variable)[p + 3] = vtest4[ivariable];
				infos.get(variable)[p + 4] = vtest5[ivariable];
				infos.get(variable)[p + 5] = vtest6[ivariable];

				ivariable++;
			}
		}
		return true;
	}

	/**
	 * one info per cluster: Intern %,glob %,Intern freq,Glob freq,p.value,v.test
	 */
	public final String[] getInfoNames() {

		int n = getParent().getNumberOfClusters();
		String[] names = new String[n * 6];
		for (int i = 0; i < n; i++) {
			int p = i * 6;
			names[p] = "Intern % " + (i + 1); //$NON-NLS-1$
			names[p + 1] = "glob % " + (i + 1); //$NON-NLS-1$
			names[p + 2] = "Intern freq " + (i + 1); //$NON-NLS-1$
			names[p + 3] = "Glob freq " + (i + 1); //$NON-NLS-1$
			names[p + 4] = "p.value " + (i + 1); //$NON-NLS-1$
			names[p + 5] = "v.test " + (i + 1); //$NON-NLS-1$
		}
		return names;
	}
}
