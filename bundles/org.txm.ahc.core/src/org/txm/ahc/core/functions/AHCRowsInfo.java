package org.txm.ahc.core.functions;

import java.util.LinkedHashMap;

import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.utils.TXMProgressMonitor;

/**
 * Group individuals informations (para + dist) for each cluster, no info in a cell is null
 * 
 * @author mdecorde
 *
 */
public class AHCRowsInfo extends AHCInfos {

	
	public AHCRowsInfo(AHC parent) {
		super(parent);
	}

	public AHCRowsInfo(String parametersNodePath) {
		super(parametersNodePath);
	}

	public AHCRowsInfo(String parametersNodePath, AHC parent) {
		super(parametersNodePath, parent);
	}

	@Override
	public String getDetails() {
		return AHCCoreMessages.bind(AHCCoreMessages.P0Informations, AHCCoreMessages.rows);
	}

	@Override
	public String getName() {
		
		if (this.getParent().columnsComputing) {
			return AHCCoreMessages.columns;
		} else {
			return AHCCoreMessages.rows;
		}
	}

	
	public String getFirstColumnName() {
		return getName();
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		int n = getParent().getNumberOfClusters();

		if (infos == null) infos = new LinkedHashMap<>();
		infos.clear();

		for (int icluster = 0; icluster < n; icluster++) {

			double[] para = getParent().getClusterParangons(icluster + 1);
			double[] dist = getParent().getClusterDistances(icluster + 1);
			String[] inds = getParent().getIndividuals(icluster + 1);

			int iIndividu = 0;
			for (String individual : inds) {
				if (!infos.containsKey(individual)) {
					infos.put(individual, new Object[2 * n]);
				}

				infos.get(individual)[(2 * icluster)] = para[iIndividu];
				infos.get(individual)[(2 * icluster) + 1] = dist[iIndividu];
				iIndividu++;
			}

		}
		return true;
	}

	/**
	 * 2 info per cluster: para and dist
	 */
	public final String[] getInfoNames() {

		int n = getParent().getNumberOfClusters();
		String[] names = new String[2 * n];
		for (int i = 0; i < n; i++) {
			names[(2 * i)] = "para" + (i + 1); //$NON-NLS-1$
			names[(2 * i) + 1] = "dist" + (i + 1); //$NON-NLS-1$
		}
		return names;
	}
}
