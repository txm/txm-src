package org.txm.ahc.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * AHC core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AHCCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.ahc.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;
	
	public static String AHC_computedWithTheP0MethodAndTheP1Metric;

	public static String P0classesOfP1;

	public static String columns;

	public static String rows;

	public static String ahc;

	public static String error_errorWhileExportingCAHToTXTP0;


	public static String clusterDendrogram;

	public static String Error;

	public static String info_ahcOfTheP0PartitionCommaP1Property;

	public static String info_ahcOfTheP0LexcialTable;

	public static String info_ahcOfTheP0ca;

	public static String error_canNotComputeAHCOnLexicalTableWithLessThan4Columns;

	public static String error_canNotComputeAHCWithLessThan4RowsInTheTable;

	public static String failToBuildTheAHCForSelectionP0DbldotP1;

	public static String inertia;

	public static String clustersOfP0;

	public static String factors;

	public static String clusters;
	
	public static String nodes;

	public static String clusterP0;
	
	
	public static String P0Informations;



	static {
		Utf8NLS.initializeMessages(BUNDLE_NAME, AHCCoreMessages.class);
	}
}
