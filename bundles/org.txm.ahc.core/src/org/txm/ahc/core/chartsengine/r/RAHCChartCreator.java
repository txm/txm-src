package org.txm.ahc.core.chartsengine.r;

import java.io.File;

import org.txm.ahc.core.functions.AHC;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.r.core.RChartCreator;

/**
 * R AHC chart creator.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class RAHCChartCreator extends RChartCreator<AHC> {

	@Override
	public File createChart(AHC ahc, File file) {

		this.chartsEngine.setColors(ChartsEngine.RENDERING_COLORS_MODE, ahc.getNumberOfClusters());

		String cmd = null;
		// 2D
		if (ahc.isRendering2D()) {
			cmd = "plot.HCPC(" + ahc.getRSymbol() + ", palette=palette(colors), new.plot=FALSE, choice=\"tree\")"; //$NON-NLS-1$ //$NON-NLS-2$

			// FIXME: SJ: for tests without drawing the inertia barplot. Without it, the max available clusters can exceed 16
			// cmd = "plot.HCPC(" + cah.getSymbol() + ", new.plot=FALSE, choice=\"tree\", tree.barplot=FALSE)"; //$NON-NLS-1$ //$NON-NLS-2$
			// FIXME: Tests for drawing only the inertia bar plot. We may need to create a chart creator for this plot
			// cmd= "plot.HCPC(" + cah.getSymbol() + ", new.plot=FALSE, choice=\"bar\")";
		}
		// 3D
		else {
			cmd = "plot.HCPC(" + ahc.getRSymbol() + ", palette=palette(colors), new.plot=FALSE)"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		return this.getChartsEngine().plot(file, cmd);
	}

	@Override
	public Class<AHC> getResultDataClass() {
		return AHC.class;
	}
}
