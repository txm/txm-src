package org.txm.ahc.core.chartsengine.jfreechart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.osgi.util.NLS;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYLineAnnotation;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.ahc.core.functions.AHC;
import org.txm.ahc.core.messages.AHCCoreMessages;
import org.txm.ahc.core.preferences.AHCPreferences;
import org.txm.ca.core.messages.CACoreMessages;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.chartsengine.jfreechart.core.themes.base.ExtendedNumberAxis;
import org.txm.chartsengine.jfreechart.core.themes.base.JFCTheme;
import org.txm.core.preferences.TXMPreferences;

/**
 * JFC AHC chart creator.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 * 
 */
public class JFCAHCChartCreator extends JFCChartCreator<AHC> {



	@Override
	public JFreeChart createChart(AHC ahc) {

	//	System.err.println("JFCAHCChartCreator.createChart() is not yet fully implemented."); //$NON-NLS-1$

		JFreeChart chart = null;

		String xAxisLabel = CACoreMessages.columns;
		if (!ahc.isColumnsComputing()) {
			xAxisLabel = CACoreMessages.rows;
		}
		
		// Create the data set
		XYSeriesCollection dataset = new XYSeriesCollection();


		// Create the chart
		chart = ChartFactory.createXYLineChart(
				AHCCoreMessages.clusterDendrogram, // chart title
				xAxisLabel, // x axis label
				AHCCoreMessages.inertia, // y axis label
				dataset, // data
				PlotOrientation.HORIZONTAL,
				true, // include legend
				true, // tooltips
				false // urls
		);

		// Custom renderer
		//XYLineAndShapeRenderer renderer = this.getChartsEngine().getJFCTheme().createXYLineAndShapeRenderer(ahc, chart, true, false);
		//FIXME: SJ: temporary disable the mouse over and selection. The above renderer is not ready to manage selection, mouse rollover and tooltips for the AHC chart. 
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true, false);
		
		chart.getXYPlot().setRenderer(renderer);
		
		
		
//		LogAxis lAxis = new LogAxis("internia");
//		chart.getXYPlot().setRangeAxis(lAxis);
		
		return chart;
	}

	@Override
	public void updateChart(AHC ahc) {

		JFreeChart chart = (JFreeChart) ahc.getChart();
		XYPlot plot = chart.getXYPlot(); 

		XYSeriesCollection dataset = (XYSeriesCollection) chart.getXYPlot().getDataset();
		dataset.removeAllSeries();

		XYItemRenderer renderer = chart.getXYPlot().getRenderer();

		plot.clearAnnotations();


		// freeze rendering while computing
		chart.setNotify(false);

		int[] clusterNumbers = ahc.getClusterNumbers();
		String[] clusterNumbersNames = ahc.getClusterNumbersNames();
		String[] treeLeavesLabels = ahc.getTreeLeavesLabels();
		String[] treeLeavesOrderedLabels = ahc.getTreeLeavesOrderedLabels();
		double[] treeMergesHeights = ahc.getTreeMergesHeights();
		double[][] treeMerges = ahc.getTreeMerges();
//		double[][] clusterCoords = ahc.getClusterCoords();

//		double[] yValuesByOrderedTreeLabels = new double[treeLeavesLabels.length];
//		double[] clusterNumbersValuesByOrderedRowNames = new double[treeLeavesLabels.length];

		// FIXME: SJ: Sort the cluster numbers in reverse order
		// Arrays.sort(clusterNumbers);
		// ArrayUtils.reverse(clusterNumbers);
		// System.out.println(Arrays.toString(clusterNumbers));

		// FIXME: SJ: Debug output
//		System.err.println("JFCAHCChartCreator.updateChart(): tree leaves labels: \t\t" + Arrays.toString(treeLeavesLabels));
//		System.err.println("JFCAHCChartCreator.updateChart(): tree leaves labels ordererd from left to right: \t" + Arrays.toString(treeLeavesOrderedLabels));
//		System.err.println("JFCAHCChartCreator.updateChart(): clusters numbers: \t\t" + Arrays.toString(clusterNumbers));
//		System.err.println("JFCAHCChartCreator.updateChart(): tree merges matrix: \t\t" + Arrays.toString(treeMerges));
//		System.err.println("JFCAHCChartCreator.updateChart(): tree merges heights: \t\t" + Arrays.toString(treeMergesHeights));
//		System.err.println("JFCAHCChartCreator.updateChart(): inertia gains: \t\t" + Arrays.toString(ahc.getInertiaGains()));
//		System.err.println("JFCAHCChartCreator.updateChart(): tree merges heights count: " + treeMergesHeights.length);



		// create "label => x positions" map
		Map<String, Integer> leavesXPositions = new HashMap<>();
		for (int i = 0; i < treeLeavesOrderedLabels.length; i++) {
			leavesXPositions.put(treeLeavesOrderedLabels[i], i);
			//FIXME: SJ: Debug
		//	System.err.println("JFCAHCChartCreator.updateChart(): x positions: " + treeLeavesOrderedLabels[i] + " = " + i);
		}

		// group number => group middle x coordinate
		Map<Integer, Double> groupMiddlesXPositions = new HashMap<>();


		//FIXME: SJ: R code to define a maximum height for the groups
		//height[i]/max(height))*3

//		ArrayList<Color> palette = this.chartsEngine.getTheme().getPaletteFor(ChartsEngine.RENDERING_COLORS_MODE, ahc.getNumberOfClusters());
		ArrayList<Color> palette = this.chartsEngine.getTheme().getPaletteFor(ahc.getRenderingColorsMode(), ahc.getNumberOfClusters());
		
		
		int nColorsAvailable = palette.size();

		double xMax = 0;
		
		// draw the tree
		for (int i = 0; i < treeMerges.length; i++) {

		//	System.err.println("JFCAHCChartCreator.updateChart(): tree merges: " + treeMerges[i][0] + " | " + treeMerges[i][1]);

			//FIXME: SJ: factorize the code below
			// Vertical lines and labels
			//	for (int j = 0; j < treeMerges[i].length; j++) {

			double x1 = 0;
			double x2 = 0;
			double yBase1 = 0;
			double yBase2 = 0;
			//				double y1 = 0;
			//				double y2 = 0;
			String name1 = "";
			String name2 = "";

			// left leaf
			if (treeMerges[i][0] < 0) {
				name1 = treeLeavesLabels[(int) Math.abs(treeMerges[i][0]) - 1];
				x1 = leavesXPositions.get(name1);
				yBase1 = 0;
			}
			// left merge middle
			else {
				x1 = groupMiddlesXPositions.get((int) Math.abs(treeMerges[i][0]) - 1);
				yBase1 = treeMergesHeights[(int) treeMerges[i][0] - 1];
			}
			// right leaf
			if (treeMerges[i][1] < 0) {
				name2 = treeLeavesLabels[(int) Math.abs(treeMerges[i][1]) - 1];
				x2 = leavesXPositions.get(name2);
				yBase2 = 0;
			}
			// right merge middle
			else {
				x2 = groupMiddlesXPositions.get((int) Math.abs(treeMerges[i][1]) - 1);
				yBase2 = treeMergesHeights[(int) treeMerges[i][1] - 1];
			}

			// store the group middle x coordinate
			groupMiddlesXPositions.put(i, (x1 + ((x2 - x1) / 2)));


			
			// left vertical line
			XYLineAnnotation line = new XYLineAnnotation(x1, yBase1, x1, treeMergesHeights[i]);
			plot.addAnnotation(line);

			// right vertical line
			line = new XYLineAnnotation(x2, yBase2, x2, treeMergesHeights[i]);
			plot.addAnnotation(line);

			// horizontal line
			line = new XYLineAnnotation(x1, treeMergesHeights[i], x2, treeMergesHeights[i]);
			plot.addAnnotation(line);

			
			// FIXME: SJ: test for drawing labels for leaf names with text annotations
			// draw a left leaf label
//			if(!name1.isEmpty()) {
//				XYTextAnnotation label = new XYTextAnnotation(name1, x1, -0.0002); // le problème ici c'est que la valeur décalage doit dépendre des coordonnées Y du jeu de données
//				label.setPaint(Color.RED);
//				label.setTextAnchor(TextAnchor.CENTER_RIGHT);
//				label.setRotationAnchor(TextAnchor.CENTER_RIGHT);
//				if (chart.getXYPlot().getOrientation() == PlotOrientation.VERTICAL) {
//					label.setRotationAngle(-1.5708);
//				}
//				chart.getXYPlot().addAnnotation(label);
//			}
//			// draw a right leaf label
//			if(!name2.isEmpty()) {
//				XYTextAnnotation label = new XYTextAnnotation(name2, x2, -0.0002); // le problème ici c'est que la valeur décalage doit dépendre des coordonnées Y du jeu de données
//				label.setPaint(Color.RED);
//				label.setTextAnchor(TextAnchor.CENTER_RIGHT);
//				label.setRotationAnchor(TextAnchor.CENTER_RIGHT);
//				if (chart.getXYPlot().getOrientation() == PlotOrientation.VERTICAL) {
//					label.setRotationAngle(-1.5708);
//				}
//				chart.getXYPlot().addAnnotation(label);
//			}


			if (xMax < x2) {
				xMax = x2;
			}
		}


		// FIXME: SJ: for cluster boxes we may use XYBoxAnnotation()
		//		XYBoxAnnotation cluster = new XYBoxAnnotation(x1, yBase1, x2, treeMergesHeights[i]);
		//		chart.getXYPlot().addAnnotation(cluster);

		// R code
		//y <- (res$call$t$tree$height[length(res$call$t$tree$height)-nb.clust+2]+res$call$t$tree$height[length(res$call$t$tree$height)-nb.clust+1])/2

//		System.err.println("JFCAHCChartCreator.updateChart(): nb clust length = " + clusterNumbers.length);
//		System.err.println("JFCAHCChartCreator.updateChart(): nb of clust = " + clusterNumbers.length);

		int nClusters = ahc.getNumberOfClusters();

		double ytop = (treeMergesHeights[treeMergesHeights.length - ahc.getNumberOfClusters() + 2 - 1] + treeMergesHeights[treeMergesHeights.length - nClusters + 1 - 1]) / 2;
		double ytop2 = ((treeMergesHeights[treeMergesHeights.length - ahc.getNumberOfClusters() + 2 - 1]) + 2 * (treeMergesHeights[treeMergesHeights.length - nClusters + 1 - 1])) / 3;
		
		double ybottom = 99;
		for (double y : treeMergesHeights) {
			if (ybottom > y) {
				ybottom = y;
			}
		}
		ybottom = -2 * ybottom; // bottom in the middle of the leaf branches

		// draw the cluster boxes
		for (int icluster = 0; icluster < ahc.getNumberOfClusters(); icluster++) {

			double xleft = 99999;
			double xright = 0;
			for (int member = 0; member < clusterNumbers.length; member++) {
				if (clusterNumbers[member] == icluster + 1) {
					String name1 = clusterNumbersNames[member]; // treeLeavesOrderedLabels is the the data$clust order
					double x1 = leavesXPositions.get(name1);
					if (x1 < xleft) xleft = x1;
					if (xright < x1) xright = x1;
				}
			}

			double b = 0.25; // 1 between each leaf 
			xleft -= b;
			xright += b;
			// ---

			//			XYLineAnnotation up = new XYLineAnnotation(xleft, ytop, xright, ytop, new BasicStroke(2), palette.get(icluster%nColorsAvailable));
			//			chart.getXYPlot().addAnnotation(up);
			//			// | <-
			//			XYLineAnnotation left = new XYLineAnnotation(xleft, ybottom, xleft, ytop, new BasicStroke(2), palette.get(icluster%nColorsAvailable));
			//			chart.getXYPlot().addAnnotation(left);
			//			// -> |
			//			XYLineAnnotation right = new XYLineAnnotation(xright, ybottom, xright, ytop, new BasicStroke(2), palette.get(icluster%nColorsAvailable));
			//			chart.getXYPlot().addAnnotation(right);
			//			// ---
			XYLineAnnotation down = new XYLineAnnotation(xleft, ybottom, xright, ybottom, new BasicStroke(2), palette.get(icluster % nColorsAvailable));
			plot.addAnnotation(down);

			Color paint = palette.get(icluster % nColorsAvailable);
			//renderer.setSeriesPaint(icluster, paint);

			XYSeries series = new XYSeries(NLS.bind(AHCCoreMessages.clusterP0, (icluster + 1)));
			series.add(xleft, ybottom);
			series.add(xleft, ytop2);
			series.add(xright, ytop2);
			series.add(xright, ybottom);
			dataset.addSeries(series);
			for (int member = 0; member < clusterNumbers.length; member++) {
				if (clusterNumbers[member] == icluster + 1) {

					String name1 = clusterNumbersNames[member]; // treeLeavesOrderedLabels is the the data$clust order
					double x1 = leavesXPositions.get(name1);
					//XYNoteAnnotation tAnnotation = new XYNoteAnnotation(name1, x1, ybottom / 2, 0.5d);
					XYTextAnnotation tAnnotation = new XYTextAnnotation(" " + name1 + " ", x1, ybottom / 2); //$NON-NLS-1$ //$NON-NLS-2$
					if (chart.getXYPlot().getOrientation() == PlotOrientation.VERTICAL) {
						tAnnotation.setRotationAngle(-1.5708);
					}
					tAnnotation.setPaint(paint);
					//tAnnotation.setOutlinePaint(paint);
					//tAnnotation.setOutlineVisible(true);
					//tAnnotation.setOutlineStroke(new BasicStroke(2));
//					Color b2 = paint.brighter(); 
					Color bgColor = new Color(paint.getRed(), paint.getGreen(), paint.getBlue(), 100);
					
					tAnnotation.setBackgroundPaint(bgColor);
					chart.getXYPlot().addAnnotation(tAnnotation);
					
					//FIXME: SJ: tests to use markers for labels
//					ValueMarker marker = new ValueMarker(x1);
//					marker.setPaint(Color.black);
//					marker.setLabel(name1);
//					marker.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
//					marker.setLabelTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
//					plot.addDomainMarker(marker);
				}
			}
		}
		
		float dashPhase = 0f;
        float dash[] = {5.0f,5.0f, 1f};
		BasicStroke b = new BasicStroke(
                 2f,
                 BasicStroke.CAP_ROUND,
                 BasicStroke.JOIN_MITER,
                 1.5f, //miter limit
                 dash,
                 dashPhase
                 );
		
		XYLineAnnotation cutLine = new XYLineAnnotation(-0.5, ytop, xMax + 0.5, ytop, b, Color.LIGHT_GRAY);
		plot.addAnnotation(cutLine);

		//		plot.setInsets(new RectangleInsets(10, -0.5, ybottom, xMax + 1));


		// Custom range axis for ticks drawing options
		if (plot.getRangeAxis() instanceof NumberAxis) {
			plot.setRangeAxis(new ExtendedNumberAxis((NumberAxis) plot.getRangeAxis(), false, true, 0, 9999999));
			//FIXME: SJ: test to hide range axis out of bound values
			//plot.setRangeAxis(new ExtendedNumberAxis((NumberAxis) plot.getRangeAxis(), false, true, 0, ahc.getInertiaGains()[0]));
		}
		//plot.setDomainAxis(new SymbolAxis(chart.getXYPlot().getDomainAxis().getLabel(), treeLeavesOrderedLabels));
		plot.getDomainAxis().setVisible(false);


		//FIXME: SJ: add a dummy Dataset to manage auto-bounds/reset zoom and pan
		XYSeriesCollection dataset2 = new XYSeriesCollection();
		dataset2.removeAllSeries();
		XYSeries series = new XYSeries("");
		series.add(0, ahc.getInertiaGains()[0]);
		//series.add(1, plot.getRangeAxis().getRange().getLength());
		dataset2.addSeries(series);
		plot.setDataset(1, dataset2);
		XYItemRenderer r = new XYLineAndShapeRenderer();
		r.setSeriesShape(0, new Area());
		r.setDefaultSeriesVisibleInLegend(false);
		plot.setRenderer(1, r);
				
		
		super.updateChart(ahc);

		
		// need to reset view and selection when changing some parameters
		if (ahc.hasParameterChanged(AHCPreferences.COLUMNS_COMPUTING)
				|| ahc.getParent().getParent().hasParameterChanged(TXMPreferences.UNIT_PROPERTY)) {
			ahc.setNeedsToResetView(true);
			ahc.setNeedsToClearItemsSelection(true);
		}

		
	}



	@Override
	public Class<AHC> getResultDataClass() {
		return AHC.class;
	}



}
