package org.txm.ahc.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AHCPreferences extends ChartsEnginePreferences {

	/**
	 * To display in 2D or 3D.
	 */
	public static final String RENDERING = "rendering"; //$NON-NLS-1$

	/**
	 * The method (METHODS {@code <-} c("average", "single", "complete", "ward", "weighted", "flexible")).
	 */
	public static final String METHOD = "method"; //$NON-NLS-1$

	/**
	 * The metric (euclidean, manhattan).
	 */
	public static final String METRIC = "metric"; //$NON-NLS-1$

	/**
	 * The number of clusters.
	 */
	public static final String N_CLUSTERS = "n_clusters"; //$NON-NLS-1$
	
	/**
	 * The number of clusters.
	 */
	public static final String N_MAX_CLUSTER_TO_PROCESS = "n_max_columns_to_process"; //$NON-NLS-1$

	/**
	 * The number of clusters.
	 */
	public static final String N_AXES = "n_axes"; //$NON-NLS-1$

	/**
	 * To compute the columns or the rows.
	 */
	public static final String COLUMNS_COMPUTING = "columns_computing"; //$NON-NLS-1$

	public static final String CONSOLIDATION = "consolidation"; //$NON-NLS-1$


	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(AHCPreferences.class)) {
			new AHCPreferences();
		}
		return TXMPreferences.instances.get(AHCPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(RENDERING, "2D"); //$NON-NLS-1$
		preferences.putInt(N_CLUSTERS, 3);
		preferences.putInt(N_MAX_CLUSTER_TO_PROCESS, 1000);
		preferences.putInt(N_AXES, 0);
		preferences.put(METHOD, "ward"); //$NON-NLS-1$
		preferences.put(METRIC, "euclidean"); //$NON-NLS-1$
		preferences.putBoolean(CONSOLIDATION, false);
		preferences.putBoolean(COLUMNS_COMPUTING, true);

		preferences.putBoolean(SHOW_GRID, false);

		// disable unavailable functionality
	//	TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), ChartsEnginePreferences.SHOW_LEGEND); //
		//getInstance().put(ChartsEnginePreferences.SHOW_LEGEND, true); // for reactivation in instance without reset all preferences
//		TXMPreferences.setEmpty(this.getPreferencesNodeQualifier(), RENDERING_COLORS_MODE);
//		getInstance().put(ChartsEnginePreferences.RENDERING_COLORS_MODE, ChartsEngine.RENDERING_COLORS_MODE); // for reactivation in instance without reset all preferences

	}
}
