package org.txm.specificities.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;



/**
 * Specificities core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SpecificitiesCoreMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.specificities.core.messages.messages"; //$NON-NLS-1$

	public static String RESULT_TYPE;
	public static String RESULT_TYPE_specificitiesSelection;
	
	public static String banalityColonP0;
	public static String cantCreateSpecificitiesChartFileChartsEngineColonP0;
	public static String error_errorWhileComputingSpecificitiesP0P0;
	public static String failedToGetTheSpecificitesP0;
	public static String info_specificitiesBarChartForTheP0LexcialTable;
	public static String info_specificitiesBarChartForTheP0PartitionCommaP1Property;
	public static String info_specificitiesBarChartForTheP0Subcorpus;
	public static String info_specificitiesOfTheP0Corpus;
	public static String info_specificitiesOfTheP0LexcialTable;
	public static String info_specificitiesOfTheP0PartitionCommaP1Property;
	public static String info_specificitiesOfTheP0PartitionIndex;
	public static String noSpecificitiesIndexArray;
	public static String numberOfRowsWantedP0AndFoundP1Mismatch;
	public static String P0specificityIndexComputed;
	public static String part;
	public static String referenceToNonExistingRow;
	public static String relativeFrequencyPercent;
	public static String score;
	public static String theLexicalTableCannotBeNull;
	public static String theSpecificityIndexArrayDoesNotProperlyRepresentAMatrix;
	public static String unit;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, SpecificitiesCoreMessages.class);
	}

}
