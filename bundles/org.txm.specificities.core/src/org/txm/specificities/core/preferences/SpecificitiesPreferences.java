package org.txm.specificities.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SpecificitiesPreferences extends ChartsEnginePreferences {

	/**
	 * Maximum score.
	 */
	public static final String MAX_SCORE = "max_score"; //$NON-NLS-1$

	public static final String MIN_SCORE = "min_score"; //$NON-NLS-1$

	/**
	 * Display format.
	 */
	public static final String FORMAT = "format"; //$NON-NLS-1$

	public static final String CONTRAST_SCRIPT = "contrast_mode"; //$NON-NLS-1$

	public static final String FILTER_SCRIPT = "filter_mode"; //$NON-NLS-1$

	// charts
	public static final String CHART_GROUP_BY_LINES = "chart_group_by_lines"; //$NON-NLS-1$

	public static final String CHART_DRAW_BARS = "chart_draw_bars"; //$NON-NLS-1$

	public static final String CHART_DRAW_LINES = "chart_draw_lines"; //$NON-NLS-1$

	public static final String CHART_BANALITY = "chart_banality"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {

		if (!TXMPreferences.instances.containsKey(SpecificitiesPreferences.class)) {
			new SpecificitiesPreferences();
		}
		return TXMPreferences.instances.get(SpecificitiesPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {

		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.put(FORMAT, "%,.1f"); //$NON-NLS-1$
		preferences.put(CONTRAST_SCRIPT, "specif");
		preferences.putInt(MAX_SCORE, 1000);

		preferences.putBoolean(CHART_GROUP_BY_LINES, false);
		preferences.putBoolean(CHART_DRAW_BARS, true);
		preferences.putBoolean(CHART_DRAW_LINES, false);
		preferences.putFloat(CHART_BANALITY, 2);
	}
}
