// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-09-19 10:31:00 +0200 (Mon, 19 Sep 2016) $
// $LastChangedRevision: 3298 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.specificities.core.statsengine.data;

import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.statsengine.core.data.ContingencyTable;
import org.txm.statsengine.core.data.Matrix;

// TODO: Auto-generated Javadoc
/**
 * A LexicalTable is a special kind of {@link ContingencyTable} extracted from a
 * corpora given a {@link Partition} (the columns) and a {@link Property} (the
 * rows).
 * 
 * Can be edited, rows can be deleted cols can be deleted
 * 
 * can be exported of imported from/to a file
 * 
 * @author sloiseau
 */
public interface ISpecificities extends Matrix {

}
