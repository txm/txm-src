// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.specificities.core.statsengine.r.function;

import org.rosuda.REngine.REXP;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.specificities.core.functions.Specificities;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.specificities.core.statsengine.data.ISpecificities;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.QuantitativeDataStructure;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.data.MatrixImpl;
import org.txm.statsengine.r.core.data.VectorImpl;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.logger.Log;

/**
 * Bridge between the statistical engine and the high-level specificities
 * function {@link Specificities}.
 * 
 * This class contains only one methods; it takes as input low-level object (a
 * {@link QuantitativeDataStructure}) and return a low level object (a
 * <int>double[][]</int> array of specificities index).
 * 
 * @author sloiseau
 * 
 */
public class SpecificitiesR extends MatrixImpl implements ISpecificities {

	protected static int specif_counter = 1;

	/** The DEFAULT symbol. */
	public static final String prefixR = "SpecifResult"; //$NON-NLS-1$

	protected double[][] specIndex;

	/**
	 * With an already computed specificities score matrix
	 * 
	 * @param symbol
	 * @throws RWorkspaceException
	 */
	public SpecificitiesR(String symbol) throws RWorkspaceException {
		super(symbol);
	}

	/**
	 * 
	 * @param lt
	 * @throws StatException
	 */
	public SpecificitiesR(ILexicalTable lt) throws StatException {
		super(prefixR + (specif_counter++));

		// System.out.println("Av specif ");
		specIndex = compute(symbol, lt, null, null);
	}

	// /**
	// * compute Specificites of subcorpus using the textometry package functions.
	// *
	// * @param lexicon the lexicon
	// * @param subLexicon the sub lexicon
	// * @return the double[][]
	// * @throws StatException the stat exception
	// * @throws REXPMismatchException
	// * @throws RserveException
	// */
	// public SpecificitiesR(Vector lexicon, Vector subLexicon) throws StatException, RserveException, REXPMismatchException {
	// super(prefixR+(specif_counter++));
	//
	// RWorkspace rw = RWorkspace.getRWorkspaceInstance();
	// rw.safeEval("library(textometry)"); //$NON-NLS-1$
	// REXP r = rw.callFunction("specificities.lexicon.new", new QuantitativeDataStructure[] { lexicon, subLexicon }, symbol); //$NON-NLS-1$
	// // double[] res = RWorkspace.toDouble(r);
	// //System.out.println(" build double matrix");
	// specIndex = r.asDoubleMatrix();
	// }

	/**
	 * compute Specificites of lexical table using the textometry package functions.
	 *
	 * @param table the table
	 * @param typeFocus the type focus
	 * @param partFocus the part focus
	 * @return the double[][]
	 * @throws StatException the stat exception
	 */
	protected static double[][] compute(String symbol, ILexicalTable table, String[] typeFocus, String[] partFocus) throws StatException {

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();

		VectorImpl row = null;
		if (typeFocus != null && typeFocus.length != 0) {
			row = new VectorImpl(typeFocus);
		}

		VectorImpl column = null;
		if (partFocus != null && partFocus.length != 0) {
			column = new VectorImpl(partFocus);
		}

		double[][] res = null;

		try {
			rw.safeEval("library(textometry)"); //$NON-NLS-1$

			if (table.getNRows() > 1 && table.getNColumns() > 1) {
				REXP r = rw.callFunction("specificities", new QuantitativeDataStructure[] { table, row, column }, symbol); //$NON-NLS-1$

				res = r.asDoubleMatrix();
				Log.finest(TXMCoreMessages.bind(SpecificitiesCoreMessages.P0specificityIndexComputed, (res.length * res[0].length)));
			}
			else {
				res = new double[table.getNRows()][table.getNColumns()];
			}
		}
		catch (Exception e) {
			throw new StatException(TXMCoreMessages.bind(SpecificitiesCoreMessages.failedToGetTheSpecificitesP0, e.getMessage()), e);
		}
		return res;
	}

	public double[][] getScores() {
		return specIndex;
	}
}
