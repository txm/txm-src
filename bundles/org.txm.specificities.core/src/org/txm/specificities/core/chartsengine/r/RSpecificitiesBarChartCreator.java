package org.txm.specificities.core.chartsengine.r;

import java.io.File;

import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.r.core.RChartCreator;
import org.txm.specificities.core.functions.SpecificitiesSelection;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.statsengine.core.data.Matrix;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.data.DoubleMatrix;
import org.txm.statsengine.r.core.data.VectorImpl;
import org.txm.utils.logger.Log;

/**
 * Creates a bar chart from the specified <code>SpecificitiesSelection</code> result.
 * 
 * @author sjacquot
 *
 */
public class RSpecificitiesBarChartCreator extends RChartCreator<SpecificitiesSelection> {



	@Override
	public File createChart(SpecificitiesSelection result, File file) {

		try {

			SpecificitiesSelection specificitiesSelection = result;

			// parameters
			boolean transpose = specificitiesSelection.isGroupingByLines();
			boolean drawBars = specificitiesSelection.isDrawingBars();
			boolean drawLines = specificitiesSelection.isDrawingLines();
			float banality = specificitiesSelection.getBanality();
			int renderingColorMode = result.getRenderingColorsMode();

			// boolean grayscale = (renderingColorMode == ChartsEngine.RENDERING_GRAYSCALE_MODE);



			// TODO: should filter max and min here
			// specIndex

			Matrix spec = new DoubleMatrix(specificitiesSelection.getSelectedSpecificitiesIndex());
			Vector types = new VectorImpl(specificitiesSelection.getSelectedTypeNames());
			Vector parts = new VectorImpl(specificitiesSelection.getSelectedPartNames());

			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("rownames(" + spec.getSymbol() + ") <- " + types.getSymbol() + ";"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			rw.eval("colnames(" + spec.getSymbol() + ") <- " + parts.getSymbol() + ";"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			// System.out.println(""+"barplot(" + spec.getSymbol() + ", beside=T, legend.text=rownames(" + spec.getSymbol() + "));"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

			String matrix = spec.getSymbol();
			if (transpose)
				matrix = "t(" + matrix + ")"; //$NON-NLS-1$ //$NON-NLS-2$


			String names = parts.getSymbol();
			if (transpose)
				names = types.getSymbol();

			String legend = types.getSymbol();
			if (transpose)
				legend = parts.getSymbol();


			// Title
			String title = ""; //$NON-NLS-1$
			if (result.getBooleanParameterValue(ChartsEnginePreferences.SHOW_TITLE)) {
				title = specificitiesSelection.getName();
			}


			// Colors
			int itemsCount = 0;
			// to put one color per part
			if (transpose) {
				itemsCount = specificitiesSelection.getSelectedPartNames().length;
			}
			// to put one color per word
			else {
				itemsCount = specificitiesSelection.getSelectedTypeNames().size();
			}

			String[] colors = new String[itemsCount];
			String[] allColors = this.getChartsEngine().getTheme().getPaletteAsHexFor(renderingColorMode, itemsCount);

			for (int i = 0; i < itemsCount; i++) {
				colors[i] = allColors[i % allColors.length];
			}
			this.getChartsEngine().setColors(colors);



			// String cmd = "op <- par(mar=c(5.5,5,5,5), xpd=TRUE);\n"; //$NON-NLS-1$
			String cmd = "op <- par(mar=c(5.5,5,5,5));\n"; //$NON-NLS-1$

			if (drawBars) {
				cmd += "mat <- " + matrix + ";\n" + //$NON-NLS-1$ //$NON-NLS-2$
						"draw <- barplot(mat, space=c(1,3), col=colors" + //$NON-NLS-1$
						", horiz=F, las=2, names.arg=" + names + ", main=\"" + title + //$NON-NLS-1$ //$NON-NLS-2$
						"\", beside=T, xpd=TRUE);\n" +  //$NON-NLS-1$
						// removed : legend.text="+legend + "
						// "rm(op);" + //$NON-NLS-1$
						"W2 <- 100000;\n" +//$NON-NLS-1$
						"lines(c(0,W2), c(-" + banality + ", -" + banality + "), lty=2);\n" + // seuil banalité inférieur //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"lines(c(0,W2), c(" + banality + ", " + banality + "), lty=2);\n" + // seuil banalité supérieur //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"lines(c(0,W2), c(0,0), lty=1);\n" + //$NON-NLS-1$
						"\n"; //$NON-NLS-1$
			}
			else {
				// compute the hist but dont show the bar
				cmd += "mat <- " + matrix + ";\n" + //$NON-NLS-1$ //$NON-NLS-2$
						"draw <- barplot(mat, space=c(1,3), col=\"white\", border=NA, horiz=F, las=2, names.arg=" + names + ", main=\"" + title + "\", beside=T);\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				// "rm(op);\n"; //$NON-NLS-1$
			}

			if (drawLines) {
				if (transpose) {
					cmd += "for(i in 1:length(draw[1,])){\n" + //$NON-NLS-1$
							"lines(draw[,i], mat[,i], col=colors[[i]]);\n" + //$NON-NLS-1$
							"}\n"; //$NON-NLS-1$
				}
				else {
					cmd += "for(i in 1:length(draw[,1])){\n" + //$NON-NLS-1$
							"lines(draw[i,], mat[i,], col=colors[[i]]);\n" + //$NON-NLS-1$
							"}\n"; //$NON-NLS-1$
				}
				if (!drawBars) {
					if (transpose) {
						cmd += "for(i in 1:length(draw[1,])){\n" + //$NON-NLS-1$
								"text(draw[,i], 0, label=" + legend + ", offset=0, srt=90);\n" + //$NON-NLS-1$ //$NON-NLS-2$
								"lines(c(draw[[1,i]],draw[[length(draw[,i]),i]]), c(0,0))" + //$NON-NLS-1$
								"}\n"; //$NON-NLS-1$
					}
					else {
						cmd += "lines( c(0,99999), c(0,0))\n"; //$NON-NLS-1$
					}
				}
			}


			// Grid
			if (result.getBooleanParameterValue(ChartsEnginePreferences.SHOW_GRID)) {
				cmd += this.getChartsEngine().getGridPlotCmd();
			}

			// Legend
			if (result.getBooleanParameterValue(ChartsEnginePreferences.SHOW_LEGEND)) {
				if (drawBars) {
					cmd += "legend(\"topright\", " + legend + ", inset = c(-0.2,0), col = colors, fill = colors, xpd=TRUE);\n"; //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					cmd += "legend(\"topright\", " + legend + ", inset = c(-0.2,0), col = colors, lty = 1, xpd=TRUE);\n"; //$NON-NLS-1$ //$NON-NLS-2$
				}
			}

			// Plot all
			this.getChartsEngine().plot(file, cmd);


		}
		catch (Exception e) {
			Log.severe(SpecificitiesCoreMessages.bind(SpecificitiesCoreMessages.cantCreateSpecificitiesChartFileChartsEngineColonP0, this.getChartsEngine().getName()) + e);
		}

		return file;
	}



	@Override
	public Class<SpecificitiesSelection> getResultDataClass() {
		return SpecificitiesSelection.class;
	}

}
