package org.txm.specificities.core.chartsengine.jfreechart;

import java.awt.Color;
import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.general.DatasetUtils;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.txm.chartsengine.jfreechart.core.JFCChartCreator;
import org.txm.chartsengine.jfreechart.core.themes.base.ExtendedNumberAxis;
import org.txm.chartsengine.jfreechart.core.themes.base.SymbolAxisBetweenTicks;
import org.txm.specificities.core.functions.SpecificitiesSelection;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;

/**
 * Creates a bar chart from the specified <code>SpecificitiesSelection</code> result.
 * 
 * @author sjacquot
 *
 */
public class JFCSpecificitiesBarChartCreator extends JFCChartCreator<SpecificitiesSelection> {

	@Override
	public JFreeChart createChart(SpecificitiesSelection result) {

		// parameters
		boolean drawBars = result.isDrawingBars();
		boolean drawLines = result.isDrawingLines();

		JFreeChart chart = null;


		// Create the data set
		XYSeriesCollection dataset = new XYSeriesCollection();

		if (drawBars) {
			// Create the bar chart
			chart = this.getChartsEngine().createXYBarChart(dataset, result.getName(), SpecificitiesCoreMessages.part, SpecificitiesCoreMessages.score, true, false);
		}
		else {
			// Create the line chart
			chart = this.getChartsEngine().createXYLineChart(result, dataset, result.getName(), SpecificitiesCoreMessages.part, SpecificitiesCoreMessages.score,
					true, false, true, true, true, false, false);
		}

		return chart;

	}

	@Override
	public void updateChart(SpecificitiesSelection result) {

		SpecificitiesSelection specificitiesSelection = result;
		JFreeChart chart = (JFreeChart) result.getChart();

		float banality = specificitiesSelection.getBanality();


		// freeze rendering while computing
		chart.setNotify(false);


		// removes all existing range marker
		chart.getXYPlot().clearRangeMarkers();

		// Add positive banality marker
		Marker marker = new ValueMarker(banality);
		marker.setPaint(Color.RED);
		marker.setLabel(SpecificitiesCoreMessages.bind(SpecificitiesCoreMessages.banalityColonP0, banality));
		marker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
		marker.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);
		chart.getXYPlot().addRangeMarker(marker);


		// Add negative banality marker
		marker = new ValueMarker(-banality);
		marker.setPaint(Color.RED);
		marker.setLabel(SpecificitiesCoreMessages.bind(SpecificitiesCoreMessages.banalityColonP0, -banality));
		marker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
		marker.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);
		chart.getXYPlot().addRangeMarker(marker);


		// Transpose or not (group by lines or not)
		if (result.hasParameterChanged(SpecificitiesPreferences.CHART_GROUP_BY_LINES)) {

			// Fill the data set
			XYSeriesCollection dataset = (XYSeriesCollection) chart.getXYPlot().getDataset();
			dataset.removeAllSeries();
			String[] xAxisSymbols;

			// Transpose the data set
			List<String> types = specificitiesSelection.getSelectedTypeNames();
			String[] parts = specificitiesSelection.getSelectedPartNames();
			if (specificitiesSelection.isGroupingByLines()) {
				xAxisSymbols = new String[types.size()];

				for (int i = 0; i < parts.length; i++) {
					XYSeries series = new XYSeries(parts[i]);
					dataset.addSeries(series);

					for (int j = 0; j < types.size(); j++) {
						series.add(j, specificitiesSelection.getSelectedSpecificitiesIndex()[j][i]);
						// Add X axis symbol
						xAxisSymbols[j] = types.get(j);
					}
				}

			}
			else {
				xAxisSymbols = new String[parts.length];

				for (int i = 0; i < types.size(); i++) {

					XYSeries series = new XYSeries(types.get(i));
					dataset.addSeries(series);

					for (int j = 0; j < parts.length; j++) {
						series.add(j, specificitiesSelection.getSelectedSpecificitiesIndex()[i][j]);
						// Add X axis symbol
						xAxisSymbols[j] = parts[j];
					}
				}
			}


			// Custom range axis for ticks drawing options
			double minimumValue = DatasetUtils.findMinimumRangeValue(chart.getXYPlot().getDataset()).doubleValue();
			double maximumValue = DatasetUtils.findMaximumRangeValue(chart.getXYPlot().getDataset()).doubleValue();
			// Adjust ticks values according to data set contains only positives values or only negatives values
			if (minimumValue > 0) {
				minimumValue = 0;
			}
			if (maximumValue < 0) {
				maximumValue = specificitiesSelection.getBanality();
			}
			// Custom range axis to cut the values to minimum and maximum values
			chart.getXYPlot().setRangeAxis(new ExtendedNumberAxis((NumberAxis) chart.getXYPlot().getRangeAxis(), true, true, minimumValue, maximumValue));

			chart.getXYPlot().setDomainAxis(new SymbolAxisBetweenTicks(SpecificitiesCoreMessages.part, xAxisSymbols));
		}

		super.updateChart(result);

	}


	@Override
	public Class<SpecificitiesSelection> getResultDataClass() {
		return SpecificitiesSelection.class;
	}



}
