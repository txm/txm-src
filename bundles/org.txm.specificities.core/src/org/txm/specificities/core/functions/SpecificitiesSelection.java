/**
 * 
 */
package org.txm.specificities.core.functions;

import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.Parameter;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;
import org.txm.statsengine.core.StatException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;

/**
 * A selection from a Specificities result dedicated to export or chart creation.
 * 
 * @author sjacquot
 * 
 */
public class SpecificitiesSelection extends ChartResult {

	/**
	 * The selected specificities indices to focus on.
	 */
	protected double[][] selectedSpecificitiesIndex;


	/**
	 * Banality threshold.
	 */
	@Parameter(key = SpecificitiesPreferences.CHART_BANALITY, type = Parameter.RENDERING)
	protected Float banality;

	/**
	 * To draw bars or not.
	 */
	@Parameter(key = SpecificitiesPreferences.CHART_DRAW_BARS, type = Parameter.RENDERING)
	protected Boolean drawBars;

	/**
	 * To draw lines or not.
	 */
	@Parameter(key = SpecificitiesPreferences.CHART_DRAW_LINES, type = Parameter.RENDERING)
	protected Boolean drawLines;

	/**
	 * Group by lines / transpose matrix.
	 */
	@Parameter(key = SpecificitiesPreferences.CHART_GROUP_BY_LINES, type = Parameter.RENDERING)
	protected Boolean groupByLines;

	/**
	 * The selected type names to focus on.
	 */
	@Parameter(key = SpecificitiesPreferences.VALUES, type = Parameter.RENDERING)
	protected List<String> selectedTypeNames;

	/**
	 * Creates an not computed specificities selection chart.
	 * 
	 * @param parent
	 */
	public SpecificitiesSelection(Specificities parent) {
		this(null, parent);
	}

	/**
	 * Creates an not computed specificities selection chart.
	 * 
	 * @param parametersNodePath
	 */
	public SpecificitiesSelection(String parametersNodePath) {
		this(parametersNodePath, null);
	}


	/**
	 * Creates an not computed specificities selection chart.
	 * 
	 * @param parametersNodePath
	 * @param parent
	 */
	public SpecificitiesSelection(String parametersNodePath, Specificities parent) {
		super(parametersNodePath, parent);

		this.domainGridLinesVisible = false;
	}

	/**
	 * @return true if selection is found in the specificities table
	 */
	@Override
	protected boolean __compute(TXMProgressMonitor monitor) throws Exception {
		Specificities specificities = this.getParent();
		double[][] tableLines = specificities.getSpecificitesIndices();
		String[] partNames = specificities.getColumnsNames();
		String[] rowNames = specificities.getRowNames();

		selectedSpecificitiesIndex = new double[selectedTypeNames.size()][partNames.length];

		// initialize with zeros in case a word is not found in the specif table
		double[] zeros = new double[partNames.length];
		for (int i = 0; i < selectedTypeNames.size(); i++) {
			selectedSpecificitiesIndex[i] = zeros;
		}

		// find words in the specif table
		int n = 0;
		for (int i = 0; i < rowNames.length; i++) {
			double[] line = tableLines[i];
			String rowName = rowNames[i];
			if (selectedTypeNames.contains(rowName)) {
				selectedSpecificitiesIndex[selectedTypeNames.indexOf(rowName)] = line;
				n++;
			}
		}

		return n > 0;
	}

	@Override
	public boolean canCompute() throws Exception {
		return this.parent != null;
	}

	@Override
	public void clean() {
		// nothing to do
	}

	/**
	 * @return the banality
	 */
	public float getBanality() {
		return banality;
	}

	@Override
	public String getDetails() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getName());

		if (banality != null) {
			sb.append("\n\t" + "banality: " + this.banality); //$NON-NLS-1$
		}
		if (drawBars != null) {
			sb.append("\n\t" + "draw bars: " + this.drawBars); //$NON-NLS-1$
		}
		if (drawLines != null) {
			sb.append("\n\t" + "draw lines: " + this.drawLines); //$NON-NLS-1$
		}
		if (groupByLines != null) {
			sb.append("\n\t" + "group lines: " + this.groupByLines); //$NON-NLS-1$
		}
		return sb.toString();
	}

	@Override
	public String getName() {
		String name = "[" + StringUtils.join(this.selectedTypeNames, ", ") + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (name.length() > 80) {
			name = name.substring(0, 79) + "..."; //$NON-NLS-1$
		}
		return name;
	}

	@Override
	public String getResultType() {
		return SpecificitiesCoreMessages.RESULT_TYPE_specificitiesSelection;
	}


	/**
	 * @return the selectedPartNames
	 */
	public String[] getSelectedPartNames() {
		try {
			return ((Specificities) this.parent).getColumnsNames();
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @return the selectedSpecificitiesIndex
	 */
	public double[][] getSelectedSpecificitiesIndex() {
		return selectedSpecificitiesIndex;
	}

	/**
	 * @return the selectedTypeNames
	 */
	public List<String> getSelectedTypeNames() {
		return selectedTypeNames;
	}

	@Override
	public String getSimpleName() {
		return this.getName();
	}


	@Override
	public String getComputingStartMessage() {
		// from lexical table
		if (this.parent.getParent().isVisible()) {
			return TXMCoreMessages.bind(SpecificitiesCoreMessages.info_specificitiesBarChartForTheP0LexcialTable, this.parent.getParent().getName());
		}
		else {

			// from partition
			if (((LexicalTable) this.parent.getParent()).getPartition() != null) {
				return TXMCoreMessages.bind(SpecificitiesCoreMessages.info_specificitiesBarChartForTheP0PartitionCommaP1Property, ((LexicalTable) this.parent.getParent()).getPartition()
						.getName(), ((LexicalTable) this.parent.getParent()).getProperty().asString());
			}
			// from subcorpus
			else {
				return TXMCoreMessages.bind(SpecificitiesCoreMessages.info_specificitiesBarChartForTheP0Subcorpus, this.parent.getParent().getParent().getName());
			}
		}
	}



	/**
	 * @return the drawBars
	 */
	public boolean isDrawingBars() {
		return drawBars;
	}

	/**
	 * @return the drawLines
	 */
	public boolean isDrawingLines() {
		return drawLines;
	}

	/**
	 * @return the groupByLines
	 */
	public boolean isGroupingByLines() {
		return groupByLines;
	}

	@Override
	public boolean loadParameters() {
		String str = this.getStringParameterValue(SpecificitiesPreferences.VALUES);
		if (!str.isEmpty()) {
			selectedTypeNames = Arrays.asList(str.split("\t")); //$NON-NLS-1$
		}
		return true;
	}

	@Override
	public boolean saveParameters() {
		if (selectedTypeNames != null) {
			this.saveParameter(SpecificitiesPreferences.VALUES, StringUtils.join(selectedTypeNames, "\t")); //$NON-NLS-1$
		}
		return true;
	}

	/**
	 * @param banality the banality to set
	 */
	public void setBanality(float banality) {
		this.banality = banality;
	}

	/**
	 * @param groupByLines the groupByLines to set
	 */
	public void setGroupByLines(boolean groupByLines) {
		this.groupByLines = groupByLines;
	}

	/**
	 * @param selectedTypeNames the selectedTypeNames to set
	 */
	public void setSelectedTypeNames(List<String> selectedTypeNames) {
		this.selectedTypeNames = selectedTypeNames;
	}

	@Override
	@Deprecated
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		PrintWriter writer = IOUtils.getWriter(outfile);

		Specificities specificities = this.getParent();
		String[] partNames = specificities.getColumnsNames();

		writer.println(txtseparator + "Lines" + txtseparator + colseparator + txtseparator + StringUtils.join(partNames, txtseparator + colseparator + txtseparator) + txtseparator); // all parts //$NON-NLS-1$

		for (int i = 0; i < selectedTypeNames.size(); i++) {

			Double[] values = new Double[selectedSpecificitiesIndex[i].length];
			for (int d = 0; d < selectedSpecificitiesIndex[i].length; d++) {
				values[d] = selectedSpecificitiesIndex[i][d];
			}

			writer.println(txtseparator + selectedTypeNames.get(i) + txtseparator +
					colseparator + txtseparator + StringUtils.join(values, txtseparator + colseparator + txtseparator) + txtseparator);
		}

		writer.close();
		return true;
	}


	@Override
	public Specificities getParent() {
		return (Specificities) this.parent;
	}


}
