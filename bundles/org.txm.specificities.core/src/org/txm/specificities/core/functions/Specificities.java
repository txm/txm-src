// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (Tue, 24 Jan 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.specificities.core.functions;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXP;
import org.txm.chartsengine.core.Theme;
import org.txm.chartsengine.core.styling.instructions.StylingInstruction;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.ResultStyler;
import org.txm.core.results.TXMResult;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.specificities.core.preferences.SpecificitiesPreferences;
import org.txm.specificities.core.statsengine.r.function.SpecificitiesR;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.core.utils.ArrayIndex;
import org.txm.statsengine.core.utils.CheckArray;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.PatternUtils;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * Compute specificities indexes using a {@link LexicalTable}
 * 
 * for alternative and test usages, the Specificities.computeIndex(...) method
 * can be used.
 * 
 * @author mdecorde
 * @author sloiseau
 * 
 */
public class Specificities extends TXMResult implements RResult, ResultStyler {

	public static int MAXSPECIF = 1000;

	/** The indices. */
	private double[][] indices;

	/** The rowindex. */
	private int[] rowindex = null;

	/** The colindex. */
	private int[] colindex = null;

	/** The frequencies. */
	private int[][] frequencies = null;

	/** The colnames. */
	private List<String> colnames = null;

	/** The rownames. */
	private List<String> rownames = null;

	/** The symbol. */
	private String symbol;

	/** The writer. */
	@Deprecated
	private BufferedWriter writer;

	/**
	 * The table. contains **all** the corpus data necessary to compute the
	 * Specificities
	 */
	private LexicalTable lexicalTable;

	/**
	 * Maximum score.
	 */
	@Parameter(key = SpecificitiesPreferences.MAX_SCORE)
	protected int pMaxScore;

	/**
	 * Minimum score.
	 */
	@Parameter(key = SpecificitiesPreferences.MIN_SCORE)
	protected int pMinScore;

	/**
	 * Minimum score.
	 */
	@Parameter(key = SpecificitiesPreferences.CONTRAST_SCRIPT)
	protected String pContrastScript;

	/**
	 * Minimum score.
	 */
	@Parameter(key = SpecificitiesPreferences.FILTER_SCRIPT)
	protected String pFilter;

	/**
	 * Creates a not computed Specificities.
	 * 
	 * @param parent
	 */
	public Specificities(LexicalTable parent) {
		super(parent);
	}

	/**
	 * Creates a not computed Specificities.
	 * 
	 * @param parametersNodePath
	 */
	public Specificities(String parametersNodePath) {
		this(parametersNodePath, null);
	}

	/**
	 * Creates a not computed Specificities.
	 *
	 * @param parent the table
	 */
	public Specificities(String parametersNodePath, LexicalTable parent) {
		super(parametersNodePath, parent);
	}

	@Override
	public boolean loadParameters() {
		this.lexicalTable = (LexicalTable) this.parent;
		return true;
	}

	@Override
	public boolean saveParameters() {
		// nothing to do
		return true;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		if (this.symbol != null && this.symbol.equals(symbol)) {
			try {
				RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(this.symbol);
			}
			catch (RWorkspaceException e) {
				Log.printStackTrace();
			}
		}

		this.frequencies = null;

		// delete the specificities selection chart children since they can not be valid anymore if the unit property has changed
		// FIXME: doesn't work
		//		if (this.lexicalTable.hasParameterChanged(TBXPreferences.UNIT_PROPERTY)) {
		//			this.deleteChildren(SpecificitiesSelection.class);
		//		}

		ILexicalTable data = this.lexicalTable.getData();
		double[][] specIndex = null;
		if (pContrastScript == null || "".equals(pContrastScript) || "specif".equals(pContrastScript)) { // switch between contrast modes //$NON-NLS-1$ //$NON-NLS-2$

			SpecificitiesR rSpecificities = new SpecificitiesR(data);
			//specIndex = rSpecificities.getScores();
			symbol = rSpecificities.getSymbol();
		}
		else if ("relative".equals(pContrastScript)) { //$NON-NLS-1$

			symbol = lexicalTable.getRSymbol() + "Rel"; //$NON-NLS-1$
			String script = symbol + " <- apply(" + this.lexicalTable.getRSymbol() + ", 2, function(x) x/sum(x)*100)"; //$NON-NLS-1$ //$NON-NLS-2$
			RWorkspace.getRWorkspaceInstance().voidEval(script);
		}
		else if (pContrastScript.contains("SYMBOL")) { //$NON-NLS-1$

			symbol = lexicalTable.getRSymbol() + "Rel"; //$NON-NLS-1$
			String script = symbol + " <- " + pContrastScript.replace("SYMBOL", this.lexicalTable.getRSymbol()); //$NON-NLS-1$ //$NON-NLS-2$
			RWorkspace.getRWorkspaceInstance().voidEval(script);//.asDoubleMatrix();
		}
		else if (pContrastScript.toUpperCase().endsWith(".R") && new File(pContrastScript).exists()) { //$NON-NLS-1$

			symbol = lexicalTable.getRSymbol() + "Script"; //$NON-NLS-1$
			String script = symbol + " <- " + IOUtils.getText(new File(pContrastScript)).replace("SYMBOL", this.lexicalTable.getRSymbol()); //$NON-NLS-1$ //$NON-NLS-2$
			RWorkspace.getRWorkspaceInstance().voidEval(script);//.asDoubleMatrix();
		}

		if (pFilter != null && pFilter.length() > 0) {

			int max = (int) SpecificitiesPreferences.getInstance().getFloat(SpecificitiesPreferences.CHART_BANALITY);
			System.out.println("BEFORE=" + RWorkspace.getRWorkspaceInstance().eval("length(rownames(" + symbol + "))").asInteger()); //$NON-NLS-1$ //$NON-NLS-2$
			if ("basic_max".equals(pFilter)) { //$NON-NLS-1$
				RWorkspace.getRWorkspaceInstance().voidEval(symbol + " <- " + symbol + "[apply(abs(" + symbol + "), 1, max) < " + max + ",]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			else if ("basic_sums".equals(pFilter)) { //$NON-NLS-1$
				RWorkspace.getRWorkspaceInstance().voidEval(symbol + " <- " + symbol + "[rowSums(abs(" + symbol + ")) < length(colnames(" + symbol + ")),]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			else if ("basic_means".equals(pFilter)) { //$NON-NLS-1$
				RWorkspace.getRWorkspaceInstance().voidEval(symbol + " <- " + symbol + "[rowMeans(abs(" + symbol + ")) < " + max + ",]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			else if ("top_max".equals(pFilter)) { //$NON-NLS-1$
				RWorkspace.getRWorkspaceInstance().voidEval(symbol + " <- " + symbol + "[apply(abs(" + symbol + "), 1, max) >= " + max + ",]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			else if ("top_sums".equals(pFilter)) { //$NON-NLS-1$
				RWorkspace.getRWorkspaceInstance().voidEval(symbol + " <- " + symbol + "[rowSums(abs(" + symbol + ")) >= length(colnames(" + symbol + ")),]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			else if ("top_means".equals(pFilter)) { //$NON-NLS-1$
				RWorkspace.getRWorkspaceInstance().voidEval(symbol + " <- " + symbol + "[rowMeans(abs(" + symbol + ")) >= " + max + ",]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			else if (pFilter.contains("SYMBOL")) { //$NON-NLS-1$
				RWorkspace.getRWorkspaceInstance().voidEval(symbol + " <- " + pFilter.replaceAll("SYMBOL", symbol)); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				Log.warning(NLS.bind("No filtering done with filter={0}.", pFilter));
			}
			System.out.println("BEFORE=" + RWorkspace.getRWorkspaceInstance().eval("length(rownames(" + symbol + "))").asInteger()); //$NON-NLS-1$ //$NON-NLS-2$

		}

		specIndex = RWorkspace.getRWorkspaceInstance().eval(symbol).asDoubleMatrix();
		init(symbol, specIndex);
		return true;
	}

	/**
	 * Instantiates a new specificities result.
	 *
	 * @param symbol the symbol
	 * @param specIndex the spec index (as double[row][column])
	 * @throws Exception the stat exception
	 */
	protected void init(String symbol, double[][] specIndex) throws Exception {

		this.symbol = symbol;

		this.indices = new double[0][0];
		this.colnames = new ArrayList<>();
		this.rownames = new ArrayList<>();

		if (this.lexicalTable == null) {
			return;//throw new IllegalArgumentException(SpecificitiesCoreMessages.theLexicalTableCannotBeNull);
		}
		// this.name = name;

		if (specIndex == null) {
			return;//throw new IllegalArgumentException(SpecificitiesCoreMessages.noSpecificitiesIndexArray);
		}
		boolean ok = CheckArray.checkMatrixRepresentation(specIndex);
		if (!ok) {
			return;//throw new IllegalArgumentException(SpecificitiesCoreMessages.theSpecificityIndexArrayDoesNotProperlyRepresentAMatrix);
		}

		this.indices = specIndex;
		this.colnames = Arrays.asList(getColumnsNames());
		this.rownames = Arrays.asList(getRowNames());

		// filter by max&min
		int MAX = MAXSPECIF;
		int MIN = -MAXSPECIF;

		if (pMaxScore > 0) {
			MAX = pMaxScore;
			MIN = -pMaxScore;
		}

		// fixing max and min score values
		for (int i = 0; i < indices.length; i++) {
			for (int j = 0; j < indices[i].length; j++) {
				if (indices[i][j] > MAX) {
					indices[i][j] = MAX;
				}
				else if (indices[i][j] < MIN) {
					indices[i][j] = MIN;
				}
			}
		}

		if (this.rownames != null && this.rownames.size() != 0) {
			if (this.rownames.size() != specIndex.length) {
				throw new IllegalArgumentException(
						SpecificitiesCoreMessages.bind(SpecificitiesCoreMessages.numberOfRowsWantedP0AndFoundP1Mismatch,
								this.rownames.size(), specIndex.length));
			}

			rowindex = ArrayIndex.getIndex(getRowNames(), this.rownames.toArray(new String[] {}));
			for (int i : rowindex) {
				if (i == -1) {
					throw new IllegalArgumentException(SpecificitiesCoreMessages.referenceToNonExistingRow);
				}
			}
		}
		// if (this.colnames != null && this.colnames.size() != 0) {
		// if (this.colnames.size() != specIndex[0].length) {
		// throw new IllegalArgumentException(
		// SpecificitiesCoreMessages.SpecificitesResult_7 + this.colnames.size()
		// + ", " + specIndex[0].length + ")."); //$NON-NLS-1$//$NON-NLS-2$
		// }
		// colindex = ArrayIndex.getIndex(
		// table.getColNames().asStringsArray(), partFocus
		// .toArray(new String[] {}));
		// for (int i : colindex) {
		// if (i == -1) {
		// throw new IllegalArgumentException(
		// SpecificitiesCoreMessages.SpecificitesResult_10);
		// }
		// }
		// }
	}

	/**
	 * Utility method to compute the specificity index of a word in a subcorpus.
	 * <br>
	 * Calls the R specificities function with a matrix built with the parameters
	 * 
	 * <pre>
	 * f	F-f
	 * t-f	T-t-F+f
	 * </pre>
	 * 
	 * @param f word frequency in subcorpus
	 * @param F word frequency in corpus
	 * @param t subcorpus size
	 * @param T corpus size
	 * @return the specificity index
	 * @throws Exception
	 */
	public static double computeIndice(int f, int F, int t, int T) throws Exception {
		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.voidEval("library(textometry)"); //$NON-NLS-1$
		REXP rez = rw.eval("specificities(matrix(c(" + f + ", " + (t - f) + ", " + (F - f) + ", " + (T - t - F + f) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				+ "), ncol=2))"); //$NON-NLS-1$
		if (rez != null) {
			return rez.asDoubles()[0];
		}
		return 0.0d;
	}

	/**
	 * Sets the unit property.
	 * 
	 * @param unitProperty
	 */
	public void setUnitProperty(WordProperty unitProperty) {
		this.getLexicalTable().setUnitProperty(unitProperty);
	}

	/**
	 * Gets the type focus.
	 *
	 * @return the type focus
	 */
	public List<String> getTypeFocus() {
		return rownames;
	}

	/**
	 * Gets the part focus.
	 *
	 * @return the part focus
	 */
	public List<String> getPartFocus() {
		return colnames;
	}

	/**
	 * Gets the number of columns of the lexical table.
	 *
	 * @return the nbr part
	 * @throws StatException the stat exception
	 */
	public int getColumnsCount() throws StatException {
		//		if (lexicalTable != null) {
		//			try {
		//				return lexicalTable.getNColumns();
		//			}
		//			catch (Exception e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//				return 0;
		//			}
		//		}
		//		else {
		return getColumnsNames().length;
		//		}
	}

	/**
	 * Gets the sum of all the columns frequencies.
	 *
	 * @return the corpus size
	 * @throws StatException the stat exception
	 */
	public int getFrequenciesSum() throws StatException {
		if (lexicalTable.getData() != null) {
			return lexicalTable.getData().getTotal();
		}
		else {
			return 0;
		}
	}

	/**
	 * Gets the sums of each columns frequencies.
	 *
	 * @return the sums of each columns frequencies
	 * @throws StatException the stat exception
	 * @throws CqiClientException
	 */
	public int[] getColumnsFrequenciesSums() throws StatException, CqiClientException {

		if (
		// this.lexicalTable != null &&
		this.lexicalTable.hasBeenComputedOnce()) {
			Vector colsSizes = lexicalTable.getColMarginsVector();
			if (colindex != null) {
				colsSizes = colsSizes.get(colindex);
			}
			return colsSizes.asIntArray();
		}
		else if (this.lexicalTable.getPartition() != null) {
			return this.lexicalTable.getPartition().getPartSizes();
		}
		else {
			return new int[] { 0, 0 };
		}
		// if (lexicalTable != null && this.subCorpus == null
		// ||
		// ) {
		// try {
		// Vector partSizes = lexicalTable.getColMarginsVector();
		// if (colindex != null) {
		// partSizes = partSizes.get(colindex);
		// }
		// return partSizes.asIntArray();
		// }
		// catch (Exception e) {
		// return ((Partition)this.lexicalTable.getParent()).getPartSizes();
		// }
		// }
		// else {
		// try {
		// return new int[] { subLexicon.nbrOfToken(), lexicon.nbrOfToken() -
		// subLexicon.nbrOfToken() };
		// }
		// // case of Subcorpus specificities (the lexicons has not yet been computed)
		// catch (Exception e) {
		// return new int[] {0, 0};
		// }
		// }
	}

	@Override
	public LexicalTable getParent() {
		return (LexicalTable) this.parent;
	}

	/**
	 * Gets the specificities index.
	 * 
	 * @return the specificities index
	 */
	public double[][] getSpecificitesIndices() {
		return indices;
	}

	/**
	 * Name of the type for which specificities are computed.
	 *
	 * @return the type names
	 * @throws StatException the stat exception
	 */
	public String[] getRowNames() throws Exception {
		//return lexicalTable.getRowNames().asStringsArray();
		try {
			return RWorkspace.getRWorkspaceInstance().eval("rownames(" + this.getRSymbol() + ")").asStrings(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (Exception e) {
			return new String[0];
		}
	}

	/**
	 * Gets the names of the lexical table columns.
	 *
	 * @return
	 * @throws StatException the stat exception
	 */
	public String[] getColumnsNames() throws StatException {
		//return lexicalTable.getColNames().asStringsArray();
		try {
			return RWorkspace.getRWorkspaceInstance().eval("colnames(" + this.getRSymbol() + ")").asStrings(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (Exception e) {
			return new String[0];
		}
	}

	/**
	 * Gets the frequencies of all columns.
	 *
	 * @return the frequencies
	 * @throws StatException the stat exception
	 */
	public int[][] getFrequencies() throws StatException {
		// System.out.println("GET FREQUENCIES");
		if (frequencies == null) {
			// System.out.println("no frequencies");
			// if (lexicalTable != null) {
			// System.out.println("FROM TABLE");

			//frequencies = RWorkspace.getRWorkspaceInstance().evalToInt2D(lexicalTable.getData().getSymbol());
			frequencies = RWorkspace.getRWorkspaceInstance().evalToInt2D(lexicalTable.getRSymbol() + "[rownames(" + this.getRSymbol() + "),]"); //$NON-NLS-1$ //$NON-NLS-2$

			// }
			// else {// if table == null : subcorpus specif
			// //System.out.println("FROM LEXICON");
			// frequencies = new int[lexicon.getFreq().length][2]; // build a frequency
			// table from lexicons
			// String[] corpusforms = lexicon.getForms(); // all the forms
			// String[] subcorpusforms = subLexicon.getForms(); // all the forms
			// int[] corpusfreq = lexicon.getFreq(); // all the freqs
			// int[] subcorpusfreq = subLexicon.getFreq(); // all the freqs in the subcorpus
			//
			// // System.out.println("len subforms: "+subcorpusforms.length);
			// // System.out.println("len subfreqs: "+subcorpusfreq.length);
			// //get all forms
			// for (int i = 0 ; i < corpusforms.length ; i++) {
			// int j = 0; // find the index of the form in the subcorpus arrays
			// for (j = 0; j < subcorpusforms.length; j++) {
			// if (subcorpusforms[j].equals(corpusforms[i]))
			// break;// found the good form
			// }
			// if(j < subcorpusforms.length)
			// frequencies[i][0] = subcorpusfreq[j];
			// frequencies[i][1] = corpusfreq[i] - frequencies[i][0];
			// }
			// }
		}
		return frequencies;
	}

	/**
	 * Gets the lexical table.
	 *
	 * @return the lexical table
	 */
	public LexicalTable getLexicalTable() {
		return lexicalTable;
	}

	/**
	 * The frequency in the whole corpus.
	 *
	 * @return the form frequencies
	 * @throws StatException the stat exception
	 */
	public int[] getFormFrequencies() throws Exception {
		// System.out.println("get freq by table");

		// if (rowindex != null) {
		// formFrequencies = formFrequencies.get(rowindex);
		// }

		return RWorkspace.getRWorkspaceInstance().eval("rowSums(" + lexicalTable.getRSymbol() + "[rownames(" + this.getRSymbol() + "),])").asIntegers(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		//Vector formFrequencies = lexicalTable.getRowMarginsVector();
		//return formFrequencies.asIntArray();
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {

		// NK: Declared as class attribute to perform a clean if the operation is
		// interrupted
		// OutputStreamWriter writer;
		try {
			this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), encoding));
		}
		catch (UnsupportedEncodingException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return false;
		}
		catch (FileNotFoundException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return false;
		}

		// int[] T = getPartSize();
		acquireSemaphore();
		try {
			getFrequencies();
			getFormFrequencies();
			String[] names = getColumnsNames();
			// if ("UTF-8".equals(encoding)) writer.write('\ufeff'); // UTF-8 BOM
			// write column header
			String txt = SpecificitiesCoreMessages.unit + colseparator + "F"; //$NON-NLS-1$
			for (String colname : names) {
				txt += colseparator + "f_" + colname + colseparator + "score_" + colname; //$NON-NLS-1$ //$NON-NLS-2$
			}
			writer.write(txt + "\n"); //$NON-NLS-1$
			String[] typeNames = getRowNames();
			// write data
			for (int ii = 0; ii < frequencies.length; ii++) {
				txt = txtseparator + typeNames[ii].replace(txtseparator, txtseparator + txtseparator)
						+ txtseparator;
				int somme = 0;
				String txtcols = ""; //$NON-NLS-1$
				for (int j = 0; j < frequencies[ii].length; j++) {
					somme += frequencies[ii][j];
					txtcols += (colseparator + frequencies[ii][j]);
					txtcols += (colseparator + indices[ii][j]);
				}
				writer.write(txt + colseparator + somme + txtcols + "\n"); //$NON-NLS-1$
				writer.flush();
			}
			writer.close();
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			releaseSemaphore();
			return false;
		}
		finally {
			releaseSemaphore();
		}

		return true;
	}

	/**
	 * release the R semaphore
	 */
	@Override
	public void resetComputingState() {
		super.resetComputingState();
		releaseSemaphore();
	}

	@Override
	public void clean() {
		try {
			if (symbol != null) {
				RWorkspace.getRWorkspaceInstance().removeVariableFromWorkspace(symbol);
			}
		}
		catch (RWorkspaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			if (this.writer != null) {
				this.writer.flush();
				this.writer.close();
			}
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	@Override
	public String getRSymbol() {
		return this.symbol;
	}

	// /**
	// * Gets the sorted part indexes.
	// *
	// * @return the sorted part indexes
	// */
	// public int[] getSortedPartIndexes() {
	//
	// try {
	// int[] indexes = new int[this.getNbrPart()];
	// String[] partsname = this.getPartShortNames().clone();
	// for (int i = 0; i < this.getNbrPart(); i++) {
	// indexes[i] = i;
	// }
	//
	// for (int i = 0; i < this.getNbrPart(); i++) {
	// int imin = i;
	// for (int j = i; j < this.getNbrPart(); j++) {
	// if (partsname[imin].compareTo(partsname[j]) > 0) {
	// imin = j;
	// }
	// }
	// String tmp = partsname[i];
	// partsname[i] = partsname[imin];
	// partsname[imin] = tmp;
	//
	// int tmp2 = indexes[i];
	// indexes[i] = indexes[imin];
	// indexes[imin] = tmp2;
	// }
	// return indexes;
	// } catch (StatException e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	// return new int[0];
	// }

	@Override
	public String getName() {
		// FIXME: SJ: to define
		return this.getParent().getParent().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
	}

	@Override
	public String getSimpleName() {
		// FIXME: SJ: to define
		// if (this.specificitiesResult.getLexicalTable()!= null &&
		// this.specificitiesResult.getLexicalTable().getPartition() != null) {
		// setPartName(this.specificitiesResult.getLexicalTable().getPartition().getName()+":
		// "+specificitiesResult.getName()); //$NON-NLS-1$
		// } else if (spinput.getSubcorpus() != null) {
		// setPartName(spinput.getSubcorpus().getName()+":
		// "+specificitiesResult.getName()); //$NON-NLS-1$
		// } else {
		// setPartName(specificitiesResult.getName());
		// }
		// FIXME: to define
		try {
			// if (this.lexicalTable.getProperty() != null) {
			if (pFilter != null && pFilter.length() > 0) {
				return this.lexicalTable.getProperty().asString() + " " + pFilter;
			}
			return this.lexicalTable.getProperty().asString();
			// }
			// else {
			// return this.lexicalTable.getParent().toString();
			// }
		}
		catch (Exception e) {
			return this.getEmptyName();
		}
	}

	@Override
	public String getDetails() {
		return NLS.bind(this.getName() + " (max score={0})", this.pMaxScore); //$NON-NLS-1$
	}

	@Override
	public String getComputingStartMessage() {
		// from lexical table
		if (this.parent.isVisible()) {
			return TXMCoreMessages.bind(SpecificitiesCoreMessages.info_specificitiesOfTheP0LexcialTable, this.getParent().getName());
		}
		else {
			// from partition index
			if (this.getParent().getParent() instanceof PartitionIndex) {
				return TXMCoreMessages.bind(SpecificitiesCoreMessages.info_specificitiesOfTheP0PartitionIndex, this.getParent().getParent().getName());
			}
			// from partition
			else if (this.getParent().getPartition() != null) {
				return TXMCoreMessages.bind(SpecificitiesCoreMessages.info_specificitiesOfTheP0PartitionCommaP1Property,
						this.getParent().getPartition().getName(), this.getParent().getProperty().asString());
			}
			// from sub-corpus
			else {
				return TXMCoreMessages.bind(SpecificitiesCoreMessages.info_specificitiesOfTheP0Corpus,
						this.getParent().getParent().getName());
			}
		}
	}

	@Override
	public boolean canCompute() {

		if (this.lexicalTable == null) {
			Log.severe("Specificities.canCompute(): can not compute without a lexical table.");
			return false;
		}

		// if (this.lexicalTable.getNColumns() < 2) {
		// Log.severe(SpecificitiesCoreMessages.ComputeError_NEED_AT_LEAST_2_PARTS);
		// return false;
		// }

		return this.lexicalTable.getProperty() != null;
	}

	/**
	 * @param maxScoreFilter the pMaxScore to set
	 */
	public void setMaxScoreFilter(int maxScoreFilter) {
		this.pMaxScore = maxScoreFilter;
	}

	/**
	 * @return the maxScore
	 */
	public int getMaxScore() {
		return pMaxScore;
	}

	/**
	 * @return the unitProperty
	 */
	public Property getUnitProperty() {
		return this.getLexicalTable().getProperty();
	}

	@Override
	public String getResultType() {
		return SpecificitiesCoreMessages.RESULT_TYPE;
	}

	@Override
	public HashMap<String, HashMap<Pattern, HashMap<String, String>>> getStyles() {

		HashMap<String, HashMap<Pattern, HashMap<String, String>>> allStyles = new HashMap<String, HashMap<Pattern, HashMap<String, String>>>();
		HashMap<Pattern, HashMap<String, String>> colsStyles = new HashMap<Pattern, HashMap<String, String>>();
		allStyles.put("cols", colsStyles); //$NON-NLS-1$

		HashMap<Pattern, HashMap<String, String>> rowsStyles = new HashMap<Pattern, HashMap<String, String>>();
		allStyles.put("rows", rowsStyles); //$NON-NLS-1$

		try {
			double[][] specifs = this.getSpecificitesIndices();
			String[] cols = this.getColumnsNames();
			String[] rows = this.getRowNames();

			int[] rowsBestCol = new int[rows.length];
			for (int i = 0; i < rows.length; i++) { // for each row, get its best score column name

				double maxscore = 0;
				int jmaxscore = -1;
				for (int j = 0; j < cols.length; j++) {
					if (maxscore < specifs[i][j] && specifs[i][j] > 2) {
						maxscore = specifs[i][j];
						jmaxscore = j;
					}
				}
				rowsBestCol[i] = jmaxscore; // ensure we have one row per column
				//System.out.println("R="+rows[i]+" -> "+jmaxscore);
			}

			Theme theme = new Theme();
			ArrayList<Color> colors = theme.getColorPaletteFor(cols.length);

			int icol = 0;
			for (String col : cols) {

				Color c = colors.get((icol) % colors.size());
				String color = "" + c.getRed() + " " + c.getGreen() + " " + c.getBlue(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

				HashMap<String, String> style1 = new HashMap<String, String>();
				//				style1.put("shape-color", color);
				style1.put(StylingInstruction.LABEL_COLOR, color);
				style1.put(StylingInstruction.LABEL_STYLE, "2"); // BOLD //$NON-NLS-1$

				HashMap<String, String> style = new HashMap<String, String>();

				//				style.put("shape-color", color);
				style.put(StylingInstruction.LABEL_COLOR, color);
				colsStyles.put(Pattern.compile(PatternUtils.quote(col)), style1);

				for (int i = 0; i < rowsBestCol.length; i++) { // for each row, get its best score column name
					if (rowsBestCol[i] == icol) {
						rowsStyles.put(Pattern.compile(PatternUtils.quote(rows[i])), style);
					}
				}

				icol++;
			}

			String color = "150 150 150"; //$NON-NLS-1$
			for (int i = 0; i < rowsBestCol.length; i++) { // for each row, get its best score column name
				if (rowsBestCol[i] == -1) {

					HashMap<String, String> style = new HashMap<String, String>();
					//					style.put("shape-color", color);
					style.put(StylingInstruction.LABEL_COLOR, color);

					rowsStyles.put(Pattern.compile(PatternUtils.quote(rows[i])), style);
				}
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return allStyles;
	}

	/**
	 * Use to sort an array of rows index with their score for a specific column
	 * 
	 * @author mdecorde
	 *
	 */
	public class SpecifComparator implements Comparator<Integer> {

		int icol = 0;

		double[][] specifs;

		public SpecifComparator(int icol, double[][] specifs) {
			this.icol = icol;
			this.specifs = specifs;
		}

		@Override
		public int compare(Integer o1, Integer o2) {

			return (int) (specifs[o1][icol] - specifs[o2][icol]);
		}
	}
}
