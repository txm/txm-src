package org.txm.functions.contrasts;

import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.utils.TXMProgressMonitor;

public class RelativeFrequency extends Contrast {

	private ILexicalTable itable;

	/**
	 * 
	 * @param table
	 */
	public RelativeFrequency(LexicalTable table) {
		super(table);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {
		itable = table.getData();
		colNames = itable.getColNames().asStringsArray();
		rowNames = itable.getRowNames().asStringsArray();
		frequencies = RWorkspace.getRWorkspaceInstance().evalToInt2D(itable.getSymbol());

		// compute
		String cmd = symbol + " <- 100*" + itable.getSymbol() + " / rowSums(" + itable.getSymbol() + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		indices = RWorkspace.getRWorkspaceInstance().evalToDouble2D(cmd);
		return true;
	}

	@Override
	public String getName() {
		return SpecificitiesCoreMessages.relativeFrequencyPercent;
	}

	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canCompute() {
		return itable != null;
	}
}
