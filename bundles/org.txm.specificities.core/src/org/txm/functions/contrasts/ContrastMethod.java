package org.txm.functions.contrasts;

public enum ContrastMethod {
	SPECIFICITIES, RELFREQ, ABSFREQ, CHI2
}
