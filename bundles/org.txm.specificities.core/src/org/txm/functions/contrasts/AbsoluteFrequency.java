package org.txm.functions.contrasts;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.utils.TXMProgressMonitor;

public class AbsoluteFrequency extends Contrast {

	private ILexicalTable itable;

	public AbsoluteFrequency(LexicalTable table) {
		super(table);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws StatException {
		itable = table.getData();
		colNames = itable.getColNames().asStringsArray();
		rowNames = itable.getRowNames().asStringsArray();
		frequencies = RWorkspace.getRWorkspaceInstance().evalToInt2D(itable.getSymbol());

		// compute
		String cmd = symbol + " <- " + itable.getSymbol(); //$NON-NLS-1$
		indices = RWorkspace.getRWorkspaceInstance().evalToDouble2D(cmd);
		return true;
	}

	@Override
	public String getName() {
		return TXMCoreMessages.common_absoluteFrequency;
	}

	@Override
	public String getSimpleName() {
		return TXMCoreMessages.common_absoluteFrequency;
	}

	@Override
	public String getDetails() {
		return TXMCoreMessages.common_absoluteFrequency;
	}

	@Override
	public void clean() {
		itable = null;
	}

	@Override
	public boolean canCompute() {
		return itable != null;
	}
}
