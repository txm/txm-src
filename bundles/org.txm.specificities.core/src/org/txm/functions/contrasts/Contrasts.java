package org.txm.functions.contrasts;

import java.util.ArrayList;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.lexicaltable.core.functions.LexicalTable;

public class Contrasts {

	LexicalTable table;

	ArrayList<Contrast> mesures = new ArrayList<Contrast>();

	ArrayList<ContrastMethod> methods;

	public Contrasts(LexicalTable table, ArrayList<ContrastMethod> methods) {
		this.table = table;
		this.methods = methods;
		for (ContrastMethod method : methods) {
			Contrast mesure = null;
			switch (method) {
				case SPECIFICITIES:
					mesure = new Specificites2(table);
					break;
				case RELFREQ:
					mesure = new RelativeFrequency(table);
					break;
				case ABSFREQ:
					mesure = new AbsoluteFrequency(table);
					break;
				case CHI2:
					mesure = new Chi2(table);
					break;
			}

			if (mesure != null)
				mesures.add(mesure);
		}
	}

	public boolean compute() throws Exception {
		for (Contrast m : mesures) {
			System.out.println(TXMCoreMessages.computingColon + m.getName());
			m.compute();
		}
		return true;
	}

	public ArrayList<Contrast> getMesures() {
		return mesures;
	}

	public ArrayList<ContrastMethod> getMethods() {
		return methods;
	}
}
