package org.txm.functions.contrasts;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import org.txm.chartsengine.r.core.themes.DefaultTheme;
import org.txm.core.results.TXMResult;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.specificities.core.messages.SpecificitiesCoreMessages;
import org.txm.stat.engine.r.RDevice;
import org.txm.stat.engine.r.RWorkspaceRenderer;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RWorkspace;
//import org.txm.progression.core.functions.Progression;

public abstract class Contrast extends TXMResult {

	protected static int no_contrast = 1;

	protected String symbol; // the R table that stores the freqs + indices

	protected LexicalTable table;

	protected double[][] indices;

	protected String[] rowNames, colNames;

	protected int[][] frequencies;

	//	public Contrast(Partition partition, Property property, int Fmin) throws Exception {
	//		super(partition);
	//		
	//		if (partition.getNPart() < 2) {
	//			throw new IllegalArgumentException(SpecificitiesCoreMessages.ComputeError_NEED_AT_LEAST_2_PARTS);
	//		}
	//		table = LexicalTableFactory.getLexicalTable(partition, property, Fmin);
	//		symbol = "Contrast_"+this.getClass().getSimpleName()+"_"+(no_contrast++);
	//	}

	public Contrast(LexicalTable table) {
		super(table);
		this.table = table;
		symbol = "Contrast_" + this.getClass().getSimpleName() + "_" + (no_contrast++); //$NON-NLS-1$ //$NON-NLS-2$
	}

	//	public Contrast(Corpus corpus, Subcorpus subcorpus, Property property) throws Exception, Exception {
	//		table = new LexicalTable(corpus, subcorpus);
	//		symbol = "Contrast_"+this.getClass().getSimpleName()+"_"+(no_contrast++);
	//	}


	@Override
	public abstract String getName();

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String s) {
		symbol = s;
	}

	public void reloadValues() throws StatException {

		colNames = table.getData().getColNames().asStringsArray();
		rowNames = table.getData().getRowNames().asStringsArray();
		frequencies = RWorkspace.getRWorkspaceInstance().evalToInt2D(table.getData().getSymbol());

		indices = RWorkspace.getRWorkspaceInstance().evalToDouble2D(symbol);
	}

	public double[][] getIndexes() {
		return indices;
	}

	public int[][] getFrequencies() {
		return frequencies;
	}

	public String[] getColNames() {
		return colNames;
	}

	public String[] getRowNames() {
		return rowNames;
	}

	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.csv" }; //$NON-NLS-1$
	}

	/**
	 * To txt.
	 *
	 * @param outfile the outfile
	 * @param encoding the encoding
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 * @return true, if successful
	 * @throws StatException the stat exception
	 * @throws IOException
	 */
	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws StatException, IOException {

		OutputStreamWriter writer;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(outfile), encoding);
		}
		catch (UnsupportedEncodingException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return false;
		}
		catch (FileNotFoundException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return false;
		}

		String[] rowNames = getRowNames();

		//write column header
		String txt = SpecificitiesCoreMessages.unit + colseparator + "F"; //$NON-NLS-1$
		for (String colname : getColNames()) {
			txt += colseparator + "f_" + colname + colseparator + "Score"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		writer.write(txt + "\n"); //$NON-NLS-1$

		//write data
		for (int ii = 0; ii < frequencies.length; ii++) {
			txt = txtseparator + rowNames[ii].replace(txtseparator, txtseparator + txtseparator) + txtseparator;
			int somme = 0;
			String txtcols = ""; //$NON-NLS-1$
			for (int j = 0; j < frequencies[ii].length; j++) {
				somme += frequencies[ii][j];
				txtcols += (colseparator + frequencies[ii][j]);
				txtcols += (colseparator + indices[ii][j]);
			}
			writer.write(txt + colseparator + somme + txtcols + "\n"); //$NON-NLS-1$
		}
		writer.flush();
		writer.close();
		return true;
	}

	/**
	 * To svg.
	 *
	 * @param file the file
	 * @param typeNames the type names
	 * @param partNames the part names
	 * @param specIndex the spec index
	 * @param transpose the transpose
	 * @param drawBarPlot the draw bar plot
	 * @param drawLines the draw lines
	 * @param device the device
	 * @throws StatException the stat exception
	 */
	// FIXME: should be manage by the charts engine
	@Deprecated
	public final void toSVG(File file, String[] typeNames,
			String[] partNames, double[][] specIndex, boolean transpose, boolean drawBarPlot, boolean drawLines, RDevice device, int BANALITE, boolean monochrome) throws StatException {

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		try {
			String matrix = getSymbol();
			if (transpose)
				matrix = "t(" + matrix + ")"; //$NON-NLS-1$ //$NON-NLS-2$

			String names = "colnames(" + getSymbol() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
			if (transpose)
				names = "rownames(" + getSymbol() + ")"; //$NON-NLS-1$ //$NON-NLS-2$

			String legend = "rownames(" + getSymbol() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
			if (transpose)
				legend = "colnames(" + getSymbol() + ")"; //$NON-NLS-1$ //$NON-NLS-2$

			String title = this.getName();

			String cmd = ""; //$NON-NLS-1$

			String[] colors;
			String[] allcolors = DefaultTheme.colors;
			if (transpose) { // one color per part
				colors = new String[partNames.length];
				for (int j = 0; j < partNames.length; j++) {
					if (j > allcolors.length)
						colors[j] = DefaultTheme.black;
					else
						colors[j] = allcolors[j];
				}
			}
			else { // one color per word
				colors = new String[typeNames.length];
				for (int j = 0; j < typeNames.length; j++)
					colors[j] = allcolors[j % allcolors.length];
			}

			RWorkspace.getRWorkspaceInstance().eval("rm(colors)"); //$NON-NLS-1$
			RWorkspace.getRWorkspaceInstance().addVectorToWorkspace("colors", colors); //$NON-NLS-1$

			String colorString;

			if (monochrome)
				colorString = ""; //$NON-NLS-1$
			else
				colorString = ", col=colors"; //$NON-NLS-1$

			if (drawBarPlot) {
				cmd = "mat <- " + matrix + ";\n" + //$NON-NLS-1$ //$NON-NLS-2$
						"op <- par(mar=c(20,5,5,5));\n" + //$NON-NLS-1$
						"draw <- barplot(mat, space=c(1,3)" + colorString + //$NON-NLS-1$
						", horiz=F, las=2, names.arg=" + names + ", main=\"" + title + //$NON-NLS-1$ //$NON-NLS-2$
						"\", beside=T, legend.text=" + legend + ");\n" +  //$NON-NLS-1$ //$NON-NLS-2$
						"rm(op);" + //$NON-NLS-1$
						"\n"; //$NON-NLS-1$ 
			}
			else {//compute the hist but don't show the bars
				cmd = "mat <- " + matrix + ";\n" + //$NON-NLS-1$ //$NON-NLS-2$
						"op <- par(mar=c(20,0,0,0));\n" + //$NON-NLS-1$
						"draw <- barplot(mat, space=c(1,3), col=\"white\", border=NA, horiz=F, las=2, names.arg=" + names + ", main=\"" + title + "\", beside=T);\n" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"rm(op);\n"; //$NON-NLS-1$ 
			}

			if (drawLines) {
				if (transpose) {
					cmd += "for(i in 1:length(draw[1,])){\n" + //$NON-NLS-1$
							"lines(draw[,i], mat[,i], col=colors[[i]]);\n" + //$NON-NLS-1$
							"}\n"; //$NON-NLS-1$
				}
				else {
					cmd += "for(i in 1:length(draw[,1])){\n" + //$NON-NLS-1$
							"lines(draw[i,], mat[i,], col=colors[[i]]);\n" + //$NON-NLS-1$
							"}\n"; //$NON-NLS-1$
				}
				if (!drawBarPlot) {
					if (transpose)
						cmd += "legend(\"topleft\", " + names + ", inset = .02, col = colors, lty = 1);\n"; //$NON-NLS-1$ //$NON-NLS-2$
					else
						cmd += "legend(\"topleft\", " + legend + ", inset = .02, col = colors, lty = 1);\n"; //$NON-NLS-1$ //$NON-NLS-2$


					if (transpose) {
						cmd += "for(i in 1:length(draw[1,])){\n" + //$NON-NLS-1$
								"text(draw[,i], 0, label=" + legend + ", offset=0, srt=90);\n" + //$NON-NLS-1$ //$NON-NLS-2$
								"lines(c(draw[[1,i]],draw[[length(draw[,i]),i]]), c(0,0))" + //$NON-NLS-1$
								"}\n"; //$NON-NLS-1$
					}
					else {
						cmd += "lines( c(0,99999), c(0,0))\n"; //$NON-NLS-1$
					}
				}
			}

			cmd += "lines(c(0,1000000), c(-" + BANALITE + ", -" + BANALITE + "), lty=2);\n" + // seuil banalité inférieur //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					"lines(c(0,1000000), c(" + BANALITE + ", " + BANALITE + "), lty=2);\n" + // seuil banalité supérieur //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					"lines(c(0,1000000), c(0,0), lty=1);\n";  //$NON-NLS-1$


			RWorkspaceRenderer.getInstance().plot(file, cmd, device);
		}
		catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new StatException(e);
		}
	}

	@Override
	public boolean saveParameters() {
		// TODO Auto-generated method stub
		System.err.println("Contrast.saveParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}


	@Override
	public boolean loadParameters() {
		// TODO Auto-generated method stub
		System.err.println("Contrast.loadParameters(): not yet implemented."); //$NON-NLS-1$
		return true;
	}

	@Override
	public String getResultType() {
		// TODO; later we must implement this methods in subclasses
		return this.getClass().getSimpleName();
	}

}
