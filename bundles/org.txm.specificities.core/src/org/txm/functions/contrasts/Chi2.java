package org.txm.functions.contrasts;

import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.utils.TXMProgressMonitor;

public class Chi2 extends Contrast {

	private ILexicalTable itable;

	public Chi2(LexicalTable table) {
		super(table);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws StatException {
		itable = table.getData();
		colNames = itable.getColNames().asStringsArray();
		rowNames = itable.getRowNames().asStringsArray();
		frequencies = RWorkspace.getRWorkspaceInstance().evalToInt2D(itable.getSymbol());

		// compute
		String cmd = "mat <- " + itable.getSymbol() + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				" mat2 <- rowSums(mat) %o% colSums(mat) / sum(mat)" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				symbol + " <- (mat - mat2) ^ 2 / mat2"; //$NON-NLS-1$
		indices = RWorkspace.getRWorkspaceInstance().evalToDouble2D(cmd);
		return true;
	}

	@Override
	public String getName() {
		return "Chi2"; //$NON-NLS-1$
	}

	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canCompute() {
		return itable != null;
	}

}
