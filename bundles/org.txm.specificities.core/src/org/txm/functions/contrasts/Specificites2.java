package org.txm.functions.contrasts;

import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.statsengine.data.ILexicalTable;
import org.txm.specificities.core.statsengine.r.function.SpecificitiesR;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.utils.TXMProgressMonitor;

public class Specificites2 extends Contrast {

	private ILexicalTable itable;

	/**
	 * 
	 * @param table
	 */
	public Specificites2(LexicalTable table) {
		super(table);
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws StatException {
		itable = table.getData();
		colNames = itable.getColNames().asStringsArray();
		rowNames = itable.getRowNames().asStringsArray();
		frequencies = RWorkspace.getRWorkspaceInstance().evalToInt2D(itable.getSymbol());

		// compute
		indices = new SpecificitiesR(itable).getScores();
		return true;
	}

	@Override
	public String getName() {
		return "Specif2"; //$NON-NLS-1$
	}

	@Override
	public String getSimpleName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canCompute() {
		return itable != null;
	}
}
