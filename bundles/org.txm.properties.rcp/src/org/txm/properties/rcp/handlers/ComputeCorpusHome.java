// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.properties.rcp.handlers;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.logger.Log;

/**
 * Calls the function Information on a MainCorpus or a SubCorpus.
 * Displays basic statistics and gives some properties values.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ComputeCorpusHome extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object selection = this.getCorporaViewSelectedObject(event);

		if (selection instanceof TXMResult result) {
			openDocumentation(result);
		}
		else {
			return super.logCanNotExecuteCommand(selection);
		}

		return null;
	}

	public static TXMBrowserEditor openDocumentation(TXMResult result) {
		Project p = result.getProject();
		String url = p.getDocumentationURL();
		if (url.isEmpty()) {
			File file = new File(result.getProject().getProjectDirectory(), "doc/index.html");
			Log.warning("This corpus has no documentation attached. You can create one at " + file);
			return null;
		}
		return OpenBrowser.openfile(url + "#" + result.getResultType(), p.getName());
	}
}
