// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.properties.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.core.results.TXMResult;
import org.txm.properties.core.functions.Properties;
import org.txm.properties.rcp.editors.PropertiesPagedEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;

/**
 * Calls the function Information on a MainCorpus or a SubCorpus.
 * Displays basic statistics and gives some properties values.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class ComputeProperties extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		Object selection = this.getCorporaViewSelectedObject(event);
		Properties information = null;

		// Creating from Corpus
		if (selection instanceof Properties) {
			information = (Properties) selection;

		}
		else if (selection instanceof TXMResult r) {

			information = r.getFirstChild(Properties.class);

			if (information == null) { // we want only one instance of Properties per TXMResult
				information = new Properties((TXMResult) selection);
			}

		}
		// Reopening from existing result
		else {
			return super.logCanNotExecuteCommand(selection);
		}

		if (!SWTEditorsUtils.isOpenEditor(new TXMResultEditorInput<TXMResult>(information), PropertiesPagedEditor.class.getName())) {
			information.setDirty(); // force update if there is editor opened
		}
		return TXMEditor.openEditor(information, PropertiesPagedEditor.class.getName());
	}
}
