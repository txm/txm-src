package org.txm.properties.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * CAP UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class PropertiesUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.properties.rcp.messages.messages"; //$NON-NLS-1$

	public static String apply;


	public static String descriptionColon;

	public static String displayedNameColon;

	public static String vMax;

	public static String theMaximumNumberOfWordPropertyValuesToShow;

	public static String corpusProperties;

	public static String Description;

	public static String Documentation;

	public static String Logs;

	public static String LogsSaved;

	public static String SaveTheLogsContent;

	public static String ShowTheStartPage;

	public static String maximumValuesToDisplay;
	public static String maximumNumberOfDifferentValuesToDisplayPerProperty;
	public static String maximumLinesLength;
	public static String maximumLinesLengthPerProperty;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, PropertiesUIMessages.class);
	}
}
