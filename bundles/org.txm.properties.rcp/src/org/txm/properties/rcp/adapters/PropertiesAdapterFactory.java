// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.properties.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.properties.core.functions.Properties;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;


/**
 * 
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class PropertiesAdapterFactory extends TXMResultAdapterFactory {

	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(PropertiesAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(PropertiesAdapterFactory.class).getSymbolicName() + "/icons/functions/properties.png"); //$NON-NLS-1$ //$NON-NLS-2$


	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof Properties) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			};
		}
		return null;
	}

}
