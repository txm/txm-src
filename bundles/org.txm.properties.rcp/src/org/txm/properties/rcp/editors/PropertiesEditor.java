package org.txm.properties.rcp.editors;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.txm.core.results.Parameter;
import org.txm.properties.core.functions.Properties;
import org.txm.properties.core.preferences.PropertiesPreferences;
import org.txm.properties.rcp.messages.PropertiesUIMessages;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.utils.logger.Log;

/**
 * Corpus, Subcorpus or Partition information editor.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 * @deprecated we use PropertiesPagedEditor now
 */
public class PropertiesEditor extends TXMEditor<Properties> {

	/**
	 * The browser to display the HTML file.
	 */
	private Browser browser;

	/**
	 * The maximum number of word property values to display.
	 */
	@Parameter(key = PropertiesPreferences.MAX_PROPERTIES_TO_DISPLAY)
	protected Spinner maxPropertiesToDisplay;

	@Parameter(key = PropertiesPreferences.MAX_LINELENGTH_TO_DISPLAY)
	protected Spinner maxLineLengthToDisplay;

	private Button nameSaveButton;

	private Text nameText;

	private StyledText descriptionText;

	private Button descriptionSaveButton;

	@Override
	public void _createPartControl() {

		// Extended parameters
		Composite extendedParametersArea = this.getExtendedParametersGroup();
		//		((GridLayout)extendedParametersArea.getLayout()).numColumns = 2;



		TXMParameterSpinner vMax = new TXMParameterSpinner(extendedParametersArea, this, PropertiesUIMessages.vMax);
		this.maxPropertiesToDisplay = vMax.getControl();
		this.maxPropertiesToDisplay.setToolTipText(PropertiesUIMessages.theMaximumNumberOfWordPropertyValuesToShow);
		this.maxPropertiesToDisplay.setMinimum(0);
		this.maxPropertiesToDisplay.setMaximum(10000);

		TXMParameterSpinner lineMax = new TXMParameterSpinner(extendedParametersArea, this, "Maximum line length");
		this.maxLineLengthToDisplay = lineMax.getControl();
		this.maxLineLengthToDisplay.setToolTipText("Maximum lin length");
		this.maxLineLengthToDisplay.setMinimum(100);
		this.maxLineLengthToDisplay.setMaximum(10000);


		//		// label
		//		Label vMaxLabel = new Label(extendedParametersArea, SWT.NONE);
		//		//vMaxLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		//		vMaxLabel.setText(PropertiesUIMessages.vMax);
		//
		//		// spinner
		//		this.maxPropertiesToDisplay = new Spinner(extendedParametersArea, SWT.BORDER);
		//		this.maxPropertiesToDisplay.setToolTipText(PropertiesUIMessages.theMaximumNumberOfWordPropertyValuesToShow);
		//		this.maxPropertiesToDisplay.setMinimum(0);
		//		this.maxPropertiesToDisplay.setMaximum(1000);
		//
		//		
		//		RowData data = new RowData();
		//		data.width = 100;
		//		this.maxPropertiesToDisplay.setLayoutData(data);
		//		
		//		this.maxPropertiesToDisplay.addSelectionListener(new ComputeSelectionListener(this));

		//		Group editComposite = this.getTopToolbar().installGroup(PropertiesUIMessages.corpusProperties, PropertiesUIMessages.corpusProperties, IImageKeys.PENCIL, null, false);
		//		editComposite.setLayout(GLComposite.createDefaultLayout(3));
		//
		//		Label nameLabel = new Label(editComposite, SWT.NONE);
		//		nameLabel.setText(PropertiesUIMessages.displayedNameColon);
		//
		//		nameText = new Text(editComposite, SWT.BORDER);
		//		nameText.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		//		nameSaveButton = new Button(editComposite, SWT.PUSH);
		//		nameSaveButton.setText(PropertiesUIMessages.apply);
		//		nameSaveButton.addSelectionListener(new SelectionListener() {
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				String newName = nameText.getText().trim();
		//				if (newName.length() > 0) {
		//					getResult().getCorpus().setUserName(newName);
		//					CorporaView.refreshObject(getResult().getCorpus());
		//				}
		//			}
		//
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//			}
		//		});
		//
		//		Label descLabel = new Label(editComposite, SWT.NONE);
		//		descLabel.setText(PropertiesUIMessages.descriptionColon);
		//
		//		descriptionText = new StyledText(editComposite, SWT.BORDER | SWT.V_SCROLL);
		//		descriptionText.setLayoutData(GridDataFactory.fillDefaults().hint(400, 100).minSize(400, 100).span(1, 2).create());
		//		descriptionSaveButton = new Button(editComposite, SWT.PUSH);
		//		descriptionSaveButton.setText(PropertiesUIMessages.apply);
		//		descriptionSaveButton.addSelectionListener(new SelectionListener() {
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				String newDescription = descriptionText.getText().trim();
		//				if (newDescription.length() > 0) {
		//					getResult().getCorpus().setDescription(newDescription);
		//					getResult().setDirty();
		//					compute(true);
		//				}
		//			}
		//
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {
		//			}
		//		});

		// browser
		GLComposite displayArea = this.getResultArea();

		this.browser = new Browser(displayArea, SWT.NONE);
		this.browser.addOpenWindowListener(new OpenWindowListener() {

			@Override
			public void open(WindowEvent event) {

				if (org.txm.utils.OSDetector.isFamilyUnix()) {
					event.browser = browser;
					event.required = false;
				}
			}
		});
		this.browser.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		browser.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						TXMBrowserEditor.zoomIn(browser);
					}
					else {
						TXMBrowserEditor.zoomOut(browser);
					}
				}
			}
		});
	}

	@Override
	public void updateEditorFromResult(boolean update) throws Exception {
		File file = this.getResult().getHTMLFile();
		if (file != null && file.exists()) {
			this.browser.setUrl(file.toURI().toURL().toString());
		}
		else {
			Log.severe("Information HTML file doesn't exist."); //$NON-NLS-1$
		}
		//		String n = getResult().getCorpus().getUserName();
		//		if (n == null)
		//			n = getResult().getCorpus().getName();
		//		nameText.setText(n);
		//
		//		String d = getResult().getCorpus().getDescription();
		//		if (d != null) {
		//			descriptionText.setText(getResult().getCorpus().getDescription());
		//		}
	}

	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}
}
