package org.txm.properties.rcp.editors;

import java.io.IOException;
import java.util.Date;

import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Project;
import org.txm.properties.rcp.Messages;
import org.txm.properties.rcp.messages.PropertiesUIMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

public class PropertiesGeneralTab extends PropertiesTab {

	private Text nameField;

	private Browser browser;

	private Text logsText;

	private Text descriptionText;

	private Text creationText;

	private Text updateText;

	public PropertiesGeneralTab(TabFolder folder, PropertiesPagedEditor editor2) {
		super(folder, editor2);
		tab.setText(Messages.PropertiesGeneralTab_0);

		parent.getLayout().numColumns = 4;
		parent.getLayout().makeColumnsEqualWidth = false;
		parent.getLayout().verticalSpacing = 10;
		parent.getLayout().horizontalSpacing = 10;
		parent.getLayout().marginTop = 10;
		parent.getLayout().marginLeft = 10;

		Label image = new Label(parent, SWT.NONE);
		// image.setImage(IImageKeys.getImage(""));
		IBaseLabelProvider lp = CorporaView.getInstance().getTreeViewer().getLabelProvider();
		if (lp instanceof DecoratingLabelProvider) {
			Image img = ((DecoratingLabelProvider) lp).getImage(editor.getResult().getParent());
			if (img != null) {
				image.setImage(img);
			}
			else {
				image.setImage(IImageKeys.getImage(IImageKeys.FILE));
			}
		}
		image.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, true, 1, 20));

		//		
		//		Button clipButton = new Button(parent, SWT.PUSH);
		//		clipButton.setLayoutData(new GridData(GridData.END, GridData.END, false, false));
		//		clipButton.setText(Messages.PropertiesGeneralTab_11);
		//		clipButton.setTooltipText(Messages.PropertiesGeneralTab_11);
		//		clipButton.addSelectionListener(new SelectionListener() {
		//
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				toClipboard();
		//			}
		//
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {}
		//		});

		// FIRST LINE
		Label l = new Label(parent, SWT.NONE);
		l.setText(Messages.PropertiesGeneralTab_1);

		nameField = new Text(parent, SWT.READ_ONLY);
		nameField.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));
		nameField.setText(editor.getResult().getParent().getUserName());
		nameField.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR && nameField.getText().length() > 0 && !nameField.getText().equals(editor.getResult().getParent().getUserName())) {
					editor.getResult().getParent().setUserName(nameField.getText());
					System.out.println(Messages.PropertiesGeneralTab_2 + nameField.getText());
					CorporaView.refreshObject(editor.getResult().getParent());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		new Label(parent, SWT.NONE);

		//		new Label(parent, SWT.NONE).setText(Messages.PropertiesGeneralTab_3);
		//		
		//		l = new Label(parent, SWT.NONE);
		//		l.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));
		//		l.setText(editor.getResult().getParent().getResultType());
		//
		//		// new Label(parent, SWT.NONE).setText("Size");
		//		// l = new Label(parent, SWT.NONE);
		//		// l.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));
		//		// l.setText(""+editor.getResult().getParent().getChildren().size());
		//

		// SECOND LINE : FULL SIMPLE PATH
		new Label(parent, SWT.NONE).setText(Messages.PropertiesGeneralTab_4);
		Text l2 = new Text(parent, SWT.READ_ONLY);
		l2.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));
		l2.setText("" + editor.getResult().getParent().getFullPathSimpleName(false));// .getParent().getParent().getUserName()); //$NON-NLS-1$
		l2.setToolTipText("copy and use this path in macros to retrieve this object: Toolbox.workspace.getResultByPath(path)");

		new Label(parent, SWT.NONE);

		// THIRD LINE : FOLDer

		new Label(parent, SWT.NONE).setText("Folder");
		Text l3 = new Text(parent, SWT.READ_ONLY);
		l3.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));
		l3.setText("" + editor.getResult().getProject().getProjectDirectory());// .getParent().getParent().getUserName()); //$NON-NLS-1$
		l3.setToolTipText("Path to the corpus folder which contains the technical files.");

		Button openProjectButton = new Button(parent, SWT.PUSH);
		openProjectButton.setText("Open...");
		openProjectButton.setToolTipText("Open the corpus binary directory in the system file navigator");
		openProjectButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Program.launch(editor.getResult().getProject().getProjectDirectory().toString());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// FOURTH LINE CREATION DATE
		new Label(parent, SWT.NONE).setText(Messages.PropertiesGeneralTab_6);
		creationText = new Text(parent, SWT.READ_ONLY);
		creationText.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));
		Date d = editor.getResult().getParent().getCreationDate();
		if (d != null) {
			creationText.setText("" + TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(d)); //$NON-NLS-1$
		}
		new Label(parent, SWT.NONE);

		// UPDATE DATE
		new Label(parent, SWT.NONE).setText(Messages.PropertiesGeneralTab_8);
		updateText = new Text(parent, SWT.READ_ONLY);
		updateText.setToolTipText("Corpus last update date");
		updateText.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false));
		d = editor.getResult().getParent().getLastComputingDate();
		if (d != null) {
			updateText.setText("" + TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(d)); //$NON-NLS-1$
		}
		else {
			updateText.setText(Messages.PropertiesGeneralTab_10);
		}

		new Label(parent, SWT.NONE);

		// DESCRIPTION LINE
		if (editor.getResult().getParent() instanceof CorpusBuild corpus) { // the parent of the Properties result is the result selected to show the properties
			l = new Label(parent, SWT.NONE);
			l.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
			l.setText(PropertiesUIMessages.Description);

			TabFolder descriptionTabFolderb = new TabFolder(parent, SWT.BOTTOM);
			descriptionTabFolderb.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));

			browser = new Browser(descriptionTabFolderb, SWT.H_SCROLL | SWT.V_SCROLL);
			this.browser.addOpenWindowListener(new OpenWindowListener() {

				@Override
				public void open(WindowEvent event) {

					if (org.txm.utils.OSDetector.isFamilyUnix()) {
						event.browser = browser;
						event.required = false;
					}
				}
			});

			browser.setText(editor.getResult().getProject().getDescription());

			browser.addMouseWheelListener(new MouseWheelListener() {

				@Override
				public void mouseScrolled(MouseEvent e) {

					if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
						if (e.count > 0) {
							TXMBrowserEditor.zoomIn(browser);
						}
						else {
							TXMBrowserEditor.zoomOut(browser);
						}
					}
				}
			});

			TabItem tabItem = new TabItem(descriptionTabFolderb, SWT.NULL);
			tabItem.setText("View");
			tabItem.setControl(browser);

			descriptionText = new Text(descriptionTabFolderb, SWT.H_SCROLL | SWT.V_SCROLL);
			descriptionText.setText(editor.getResult().getProject().getDescription());
			descriptionText.setToolTipText("This object description (editable)");

			TabItem tabItem2 = new TabItem(descriptionTabFolderb, SWT.NULL);
			tabItem2.setText("Edit");
			tabItem2.setControl(descriptionText);

			//		l = new Label(parent, SWT.NONE);
			//		l.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
			//		l.setText("");

			Button saveDescriptionButton = new Button(parent, SWT.PUSH);
			saveDescriptionButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
			saveDescriptionButton.setText("Save description");
			saveDescriptionButton.setToolTipText("Save the new description content");
			saveDescriptionButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					editor.getResult().getProject().setDescription(descriptionText.getText());
					browser.setText(editor.getResult().getProject().getDescription());
					Log.info("Description saved.");
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		if (editor.getResult().getParent() instanceof CorpusBuild corpus || editor.getResult().getParent() instanceof Project project) {

			// THIRD LINE

			Label lversion = new Label(parent, SWT.NONE);
			lversion.setText("TXM Version");
			lversion.setToolTipText("Version of TXM when this corpus has been imported from its sources or updated from its XML-TXM files.");

			Text tversion = new Text(parent, SWT.READ_ONLY);
			tversion.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false, 2, 1));
			tversion.setText(editor.getResult().getProject().getTXMVersion());// .getParent().getParent().getUserName()); //$NON-NLS-1$
			tversion.setToolTipText("Version of TXM when this corpus has been imported from its sources or updated from its XML-TXM files.");

			l = new Label(parent, SWT.NONE);
			l.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
			l.setText(PropertiesUIMessages.Logs);

			logsText = new Text(parent, SWT.H_SCROLL | SWT.V_SCROLL);
			logsText.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
			try {
				logsText.setText(editor.getResult().getProject().getLogs());
			}
			catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Button saveHistoryButton = new Button(parent, SWT.PUSH);
			saveHistoryButton.setLayoutData(new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false));
			saveHistoryButton.setText(PropertiesUIMessages.SaveTheLogsContent);
			saveHistoryButton.setText("Save the corpus history logs");
			saveHistoryButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					editor.getResult().getProject().writeandReplaceLogs(logsText.getText());
					Log.info(PropertiesUIMessages.LogsSaved);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}
	}

	public void toClipboard() {

		StringBuffer buffer = new StringBuffer();
		buffer.append(Messages.PropertiesGeneralTab_12 + editor.getResult().getParent().getUserName());
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append(Messages.PropertiesGeneralTab_14 + editor.getResult().getParent().getResultType());
		buffer.append("\n"); //$NON-NLS-1$
		// buffer.append("Size: "+editor.getResult().getParent().getChildren().size());
		// buffer.append("\n");
		buffer.append(Messages.PropertiesGeneralTab_16 + editor.getResult().getParent().getParent().getUserName());
		buffer.append("\n"); //$NON-NLS-1$
		Date d = editor.getResult().getParent().getCreationDate();
		if (d != null) {
			buffer.append(Messages.PropertiesGeneralTab_18 + TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(d));
		}

		buffer.append("\n"); //$NON-NLS-1$
		d = editor.getResult().getParent().getCreationDate();
		if (d != null) {
			buffer.append(Messages.PropertiesGeneralTab_20 + TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(d));
		}
		else {
			buffer.append(Messages.PropertiesGeneralTab_21);
		}

		org.txm.rcp.utils.IOClipboard.write(buffer.toString());
	}

	@Override
	public void updateResultFromEditor(boolean update) {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateEditorFromResult() {

		try {
			if (browser != null && !browser.isDisposed()) {
				browser.setText(editor.getResult().getProject().getDescription());
			}

			if (descriptionText != null && !descriptionText.isDisposed()) {
				descriptionText.setText(editor.getResult().getProject().getDescription());
			}

			if (logsText != null && !logsText.isDisposed()) {
				logsText.setText(editor.getResult().getProject().getLogs());
			}

			Date dCreation = editor.getResult().getParent().getCreationDate();
			if (dCreation != null) {
				creationText.setText("" + TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(dCreation)); //$NON-NLS-1$
			}

			Date dUpdate = null;
			if (editor.getResult().getParent() instanceof CorpusBuild corpusBuild) { // use the Project for presentation purpose
				dUpdate = editor.getResult().getParent().getParent().getLastComputingDate();
			} else {
				dUpdate = editor.getResult().getParent().getLastComputingDate();
			}
			
			if (dUpdate != null) {
				updateText.setText("" + TXMResult.PRETTY_LOCALIZED_TIME_FORMAT.format(dUpdate)); //$NON-NLS-1$
			}
			else {
				updateText.setText(Messages.PropertiesGeneralTab_10);
			}
			
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
