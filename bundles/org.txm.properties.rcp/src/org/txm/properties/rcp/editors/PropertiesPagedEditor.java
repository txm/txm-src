package org.txm.properties.rcp.editors;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.Parameter;
import org.txm.properties.core.functions.Properties;
import org.txm.properties.core.preferences.PropertiesPreferences;
import org.txm.properties.rcp.handlers.ComputeCorpusHome;
import org.txm.properties.rcp.messages.PropertiesUIMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.TXMParameterSpinner;

public class PropertiesPagedEditor extends TXMEditor<Properties> {

	private TabFolder tabFolder;

	protected ArrayList<PropertiesTab> tabs = new ArrayList<>();

	/**
	 * The maximum number of word property values to display.
	 */
	@Parameter(key = PropertiesPreferences.MAX_PROPERTIES_TO_DISPLAY)
	protected Spinner maxPropertiesToDisplay;

	@Parameter(key = PropertiesPreferences.MAX_LINELENGTH_TO_DISPLAY)
	protected Spinner maxLineLengthToDisplay;


	@Override
	public void _createPartControl() throws Exception {

		Composite mainParameters = this.getMainParametersComposite();
		if (result.getProject().getDocumentationURL().length() > 0) {
			Button openDocumentation = new Button(mainParameters, SWT.PUSH);
			openDocumentation.setImage(IImageKeys.getImage("platform:/plugin/org.txm.properties.rcp/icons/functions/home.png"));
			openDocumentation.setToolTipText("Documentation");
			openDocumentation.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					ComputeCorpusHome.openDocumentation(getResult());
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		Composite extendedParametersArea = this.getExtendedParametersGroup();
		TXMParameterSpinner fMax = new TXMParameterSpinner(extendedParametersArea, this, PropertiesUIMessages.vMax);
		this.maxPropertiesToDisplay = fMax.getControl();
		this.maxPropertiesToDisplay.setToolTipText(PropertiesUIMessages.theMaximumNumberOfWordPropertyValuesToShow);
		this.maxPropertiesToDisplay.setMinimum(0);
		this.maxPropertiesToDisplay.setMaximum(1000);

		TXMParameterSpinner lineMax = new TXMParameterSpinner(extendedParametersArea, this, "Maximum line length");
		this.maxLineLengthToDisplay = lineMax.getControl();
		this.maxLineLengthToDisplay.setToolTipText("Maximum lin length");
		this.maxLineLengthToDisplay.setMinimum(100);
		this.maxLineLengthToDisplay.setMaximum(10000);

		GLComposite parent = getResultArea();
		tabFolder = new TabFolder(parent, SWT.BORDER);
		tabFolder.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		PropertiesTXMResultTab resultTab = new PropertiesTXMResultTab(tabFolder, this);
		tabs.add(resultTab);

		tabs.add(new PropertiesGeneralTab(tabFolder, this));

		if (TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER)) {
			tabs.add(new PropertiesParametersTab(tabFolder, this));
		}

		tabFolder.setSelection(resultTab.getTab());
	}

	@Override
	public void updateResultFromEditor() {
		for (PropertiesTab tab : tabs) {
			tab.updateEditorFromResult();
		}
	}

	@Override
	public void updateEditorFromResult(boolean update) throws Exception {
		for (PropertiesTab tab : tabs) {
			tab.updateEditorFromResult();
		}
	}
}
