package org.txm.properties.rcp.editors;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.TabFolder;
import org.txm.properties.rcp.Messages;
import org.txm.rcp.views.cmdparameters.TXMResultParametersComposite;

/**
 * Display the Properties parent parameters stored in the preferences
 * 
 * @author mdecorde
 *
 */
public class PropertiesParametersTab extends PropertiesTab {

	private TXMResultParametersComposite parametersComposite;

	public PropertiesParametersTab(TabFolder folder, PropertiesPagedEditor editor) {
		super(folder, editor);
		tab.setText(Messages.PropertiesParametersTab_1);

		parametersComposite = new TXMResultParametersComposite(parent, SWT.NONE);
		parametersComposite.setLayoutData(GridDataFactory.fillDefaults().align(GridData.FILL, GridData.FILL).grab(true, true).create());
		parametersComposite.updateEditorParameters(editor.getResult().getParent());

		folder.layout();
	}

	@Override
	public void updateResultFromEditor(boolean update) {

	}

	@Override
	public void updateEditorFromResult() {
		parametersComposite.refresh();
	}
}
