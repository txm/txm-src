package org.txm.properties.rcp.editors;

import java.io.File;
import java.net.MalformedURLException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.txm.core.results.TXMResult;
import org.txm.properties.rcp.Messages;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.utils.logger.Log;

/**
 * Page displayed if Properties parent is a Corpus
 * 
 * @author mdecorde
 *
 */
public class PropertiesTXMResultTab extends PropertiesTab {

	TXMResult result;

	protected Browser browser;

	public PropertiesTXMResultTab(TabFolder folder, PropertiesPagedEditor editor) {
		super(folder, editor);

		this.result = editor.getResult().getParent();
		tab.setText(Messages.PropertiesParametersTab_0);

		buildContent(parent);
	}

	protected void buildContent(GLComposite parent) {
		this.browser = new Browser(parent, SWT.NONE);
		this.browser.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		browser.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						TXMBrowserEditor.zoomIn(browser);
					}
					else {
						TXMBrowserEditor.zoomOut(browser);
					}
				}
			}
		});
	}

	@Override
	public void updateResultFromEditor(boolean update) throws Exception {

	}

	@Override
	public void updateEditorFromResult() {
		File file = this.editor.getResult().getHTMLFile();
		if (file != null && file.exists()) {
			try {
				this.browser.setUrl(file.toURI().toURL().toString());
			}
			catch (MalformedURLException e) {
				Log.printStackTrace(e);
				;
			}
		}
		else {
			Log.severe("Information HTML file doesn't exist."); //$NON-NLS-1$
		}
	}

	public TabItem getTab() {
		return tab;
	}
}
