package org.txm.properties.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.txm.core.results.TXMResult;
import org.txm.objects.Project;
import org.txm.properties.rcp.messages.PropertiesUIMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.utils.logger.Log;

/**
 * Page displayed if Properties parent is a Corpus
 * 
 * @author mdecorde
 *
 */
public class DocumentationTab extends PropertiesTab {

	TXMResult result;

	protected Browser browser;

	public DocumentationTab(TabFolder folder, PropertiesPagedEditor editor) {
		super(folder, editor);

		this.result = editor.getResult().getParent();
		tab.setText(PropertiesUIMessages.Documentation);

		buildContent(parent);
	}

	protected void buildContent(GLComposite parent) {


		createToolbar(parent);
		this.browser = new Browser(parent, SWT.NONE);
		this.browser.addOpenWindowListener(new OpenWindowListener() {

			@Override
			public void open(WindowEvent event) {

				if (org.txm.utils.OSDetector.isFamilyUnix()) {
					event.browser = browser;
					event.required = false;
				}
			}
		});
		this.browser.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		browser.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						TXMBrowserEditor.zoomIn(browser);
					}
					else {
						TXMBrowserEditor.zoomOut(browser);
					}
				}
			}
		});
	}

	private ToolBar createToolbar(Composite parent) {
		ToolBar toolbar = new ToolBar(parent, SWT.FLAT);

		// create refresh, stop, ..., and print actions
		ToolItem back = new ToolItem(toolbar, SWT.NONE);
		back.setImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		back.setHotImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		back.setDisabledImage(IImageKeys.getImage(IImageKeys.CTRLREVERSE));
		// back.setToolTipText(Messages.actionWebBrowserBack);
		back.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> this.browser.back()));

		ToolItem forward = new ToolItem(toolbar, SWT.NONE);
		forward.setImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		forward.setHotImage(IImageKeys.getImage(IImageKeys.CTRLPLAY));
		// forward.setToolTipText(Messages.actionWebBrowserForward);
		forward.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> this.browser.forward()));

		ToolItem stop = new ToolItem(toolbar, SWT.NONE);
		stop.setImage(IImageKeys.getImage(IImageKeys.CTRLSTOP));
		stop.setHotImage(IImageKeys.getImage(IImageKeys.CTRLSTOP));
		stop.setDisabledImage(IImageKeys.getImage(IImageKeys.CTRLSTOP));
		// stop.setToolTipText(Messages.actionWebBrowserStop);
		stop.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> this.browser.stop()));

		ToolItem refresh = new ToolItem(toolbar, SWT.NONE);
		refresh.setImage(IImageKeys.getImage(IImageKeys.REFRESH));
		refresh.setHotImage(IImageKeys.getImage(IImageKeys.REFRESH));
		refresh.setDisabledImage(IImageKeys.getImage(IImageKeys.REFRESH));
		// refresh.setToolTipText(Messages.actionWebBrowserRefresh);
		refresh.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> this.browser.refresh()));

		ToolItem home = new ToolItem(toolbar, SWT.NONE);
		home.setImage(IImageKeys.getImage(IImageKeys.HOME));
		home.setHotImage(IImageKeys.getImage(IImageKeys.HOME));
		home.setDisabledImage(IImageKeys.getImage(IImageKeys.HOME));
		home.setToolTipText(PropertiesUIMessages.ShowTheStartPage);

		TXMResult r = this.editor.getResult().getParent();
		Project p = r.getFirstParent(Project.class);
		home.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> this.browser.setUrl(p.getDocumentationURL())));

		return toolbar;
	}

	@Override
	public void updateResultFromEditor(boolean update) throws Exception {

	}

	@Override
	public void updateEditorFromResult() {
		TXMResult r = this.editor.getResult().getParent();
		Project p = r.getFirstParent(Project.class);

		String documentationPath = p.getDocumentationURL();
		if (documentationPath.length() > 0) {
			this.browser.setUrl(documentationPath);
		}
		else {
			Log.severe("Information HTML file doesn't exist."); //$NON-NLS-1$
		}
	}

	public TabItem getTab() {
		return tab;
	}
}
