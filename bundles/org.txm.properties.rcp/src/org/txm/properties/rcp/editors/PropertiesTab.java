package org.txm.properties.rcp.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.txm.rcp.swt.GLComposite;

public abstract class PropertiesTab {

	protected TabFolder folder;

	protected TabItem tab;

	protected GLComposite parent;

	protected PropertiesPagedEditor editor;

	public PropertiesTab(TabFolder folder, PropertiesPagedEditor editor) {
		tab = new TabItem(folder, SWT.NONE);
		parent = new GLComposite(folder, SWT.NONE, "tab"); //$NON-NLS-1$
		tab.setControl(parent);

		this.folder = folder;
		this.editor = editor;
	}

	public abstract void updateResultFromEditor(boolean update) throws Exception;

	public abstract void updateEditorFromResult();
}
