package org.txm.properties.rcp.testers;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

/**
 * TRUE only if the corpora view first selected item is a corpus with a documentation 
 * 
 * @author mdecorde
 *
 */
public class HasDocumentation extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.properties.rcp.testers"; //$NON-NLS-1$

	public static final String PROPERTY_EXPERT_ENABLED = "HasDocumentation"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		
		if (CorporaView.getFirstSelectedObject() instanceof CQPCorpus c) {
			return c.getProject().getDocumentationURL().length() > 0;
		}
		
		return false;
	}
}
