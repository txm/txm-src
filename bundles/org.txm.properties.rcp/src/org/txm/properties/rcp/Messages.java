package org.txm.properties.rcp;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends TXMCoreMessages {

	private static final String BUNDLE_NAME = "org.txm.properties.rcp.messages"; //$NON-NLS-1$

	public static String PropertiesGeneralTab_0;

	public static String PropertiesGeneralTab_1;

	public static String PropertiesGeneralTab_10;

	public static String PropertiesGeneralTab_11;

	public static String PropertiesGeneralTab_12;

	public static String PropertiesGeneralTab_14;

	public static String PropertiesGeneralTab_16;

	public static String PropertiesGeneralTab_18;

	public static String PropertiesGeneralTab_2;

	public static String PropertiesGeneralTab_20;

	public static String PropertiesGeneralTab_21;

	public static String PropertiesGeneralTab_3;

	public static String PropertiesGeneralTab_4;

	public static String PropertiesGeneralTab_6;

	public static String PropertiesGeneralTab_8;

	public static String PropertiesParametersTab_0;

	public static String PropertiesParametersTab_1;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
