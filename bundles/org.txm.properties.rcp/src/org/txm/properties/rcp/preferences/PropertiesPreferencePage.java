// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.properties.rcp.preferences;

import org.eclipse.ui.IWorkbench;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.properties.core.preferences.PropertiesPreferences;
import org.txm.properties.rcp.messages.PropertiesUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;

/**
 * Information preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class PropertiesPreferencePage extends TXMPreferencePage {


	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(new PropertiesPreferences().getPreferencesNodeQualifier()));
		this.setTitle(TXMCoreMessages.common_description);
	}


	@Override
	public void createFieldEditors() {

		// The maximum number of word property values to display
		IntegerFieldEditor maxValue = new IntegerFieldEditor(PropertiesPreferences.MAX_PROPERTIES_TO_DISPLAY, PropertiesUIMessages.maximumValuesToDisplay, this.getFieldEditorParent());
		maxValue.setValidRange(1, 10000);
		maxValue.setToolTipText(PropertiesUIMessages.maximumNumberOfDifferentValuesToDisplayPerProperty);
		this.addField(maxValue);

		IntegerFieldEditor maxLineValue = new IntegerFieldEditor(PropertiesPreferences.MAX_LINELENGTH_TO_DISPLAY, PropertiesUIMessages.maximumLinesLength, this.getFieldEditorParent());
		maxLineValue.setToolTipText(PropertiesUIMessages.maximumLinesLengthPerProperty);
		maxLineValue.setValidRange(10, 10000);
		this.addField(maxLineValue);
	}
}
