package org.txm.perl.rcp.preferences;

import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.perl.core.PerlPreferences;
import org.txm.perl.rcp.messages.Messages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * Index preferences page.
 * 
 * @author mdecorde
 *
 */
public class PerlPreferencesPage extends TXMPreferencePage {

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(PerlPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle("Perl"); //$NON-NLS-1$
		this.setImageDescriptor(IImageKeys.getImageDescriptor("org.txm.perl.rcp", "icon/perl.png")); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	protected void createFieldEditors() {

		this.addField(new StringFieldEditor(PerlPreferences.EXECUTABLE_PATH, Messages.ExecutablePath, this.getFieldEditorParent()));

		this.addField(new StringFieldEditor(PerlPreferences.ADDITIONAL_PARAMETERS, Messages.ExecutableAdditionalParameters, this.getFieldEditorParent()));
	}
}
