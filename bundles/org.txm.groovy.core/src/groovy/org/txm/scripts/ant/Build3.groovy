package org.txm.scripts.ant;

import java.io.IOException;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

		String userdir = System.getProperty("user.home");
		String[] roots = [ userdir+"/TXM/scripts", userdir+"/TXM/scripts/test"];
		GroovyScriptEngine gse = new GroovyScriptEngine(roots, this.getClass().getClassLoader());
		//gse.loadScriptByName("test/B.groovy");
		//System.out.println("B: " +gse.getResourceConnection("test/B.groovy"));
		Binding binding = new Binding();
		binding.setVariable("input", "world");
		gse.run("test/A.groovy", binding);
		
	
