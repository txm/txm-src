package org.txm.scripts.ant;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
 
class Build2
{
	public void run() throws Exception
	{
		String userdir = System.getProperty("user.home");
		String[] roots = new String[] { userdir+"/scripts"};
		GroovyScriptEngine gse = new GroovyScriptEngine(roots, this.getClass().getClassLoader());
		gse.run("A.groovy", new Binding());
	}
	public static void main(String args[]) throws Exception
	{
		new Build2().run();
	}
}
