package org.txm.scripts.ant;

String userdir = System.getProperty("user.home");
File libdir = new File(userdir, "Bureau/exportRCP/linux.gtk.x86/TXM/plugins");
File srcdir = new File(userdir, "TXM/scripts/test")
File destdir = new File(userdir, "TXM/scripts/bin")
def ant = new AntBuilder().sequential {
	//webinf = libdir.getAbsolutePath()
	taskdef name: "groovyc", classname: "org.codehaus.groovy.ant.Groovyc"
	groovyc srcdir: srcdir.getAbsolutePath(), destdir: destdir.getAbsolutePath(), {
		classpath {
			fileset dir: libdir.getAbsolutePath(), {
		    	include name: "*.jar"
			}
			pathelement path: libdir.getAbsolutePath()+"/classes"
		}
		javac source: "1.5", target: "1.5", debug: "on"
	}
}
println "fin"