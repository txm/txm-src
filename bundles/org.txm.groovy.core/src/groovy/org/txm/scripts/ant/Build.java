// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-12-06 16:57:35 +0100 (ven. 06 déc. 2013) $
// $LastChangedRevision: 2583 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.ant;

import groovy.util.ResourceException;
import groovy.util.ScriptException;

import java.io.File;
import java.io.IOException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.selectors.FileSelector;
import org.codehaus.groovy.ant.Groovyc;

// TODO: Auto-generated Javadoc
/**
 *<taskdef name="groovyc"
         classname="org.codehaus.groovy.ant.Groovyc"
         classpathref=classpath/>
 **/
public class Build
{

	/**
	 * Run.
	 * @throws IOException 
	 * @throws ScriptException 
	 * @throws ResourceException 
	 */
	public void run() throws IOException, ResourceException, ScriptException
	{
		String userdir = System.getProperty("user.home");

		Groovyc groovyc = new Groovyc();

		groovyc.setCaseSensitive(true);
		groovyc.setListfiles(true);
		groovyc.setStacktrace(true);
		groovyc.setVerbose(true);

		System.out.println("Build project"); 
		Project project = new Project();
		project.setBaseDir(new File(userdir, "scripts"));
		project.setName("Scripts");
		project.setDescription("The TXM groovy scripts");
		groovyc.setProject(project);
		
		System.out.println("Set libs classpath"); 
		File libdir = new File(userdir, "Bureau/exportRCP/linux.gtk.x86/TXM/plugins");
		Path classpath = groovyc.createClasspath();
		FileSet fs = new FileSet();
		fs.setDir(libdir);
		fs.add(new FileSelector() {
			@Override
			public boolean isSelected(File arg0, String arg1, File arg2)
					throws BuildException {
					return true;
			}
		});
		classpath.addFileset(fs);
		classpath.setProject(project);	
		
		// src dir
		Path srcdir = groovyc.createSourcepath();
		srcdir.setLocation(new File(userdir, "scripts"));
		srcdir.setProject(project);
		
		groovyc.setEncoding("UTF-8");
		groovyc.setSrcdir(srcdir);
		groovyc.setDestdir(new File(userdir, "scripts/bin"));
		groovyc.setClasspath(classpath);

		System.out.println("srcdir "+groovyc.getSrcdir());
		System.out.println("destdir "+groovyc.getDestdir());
		System.out.println("classpath "+groovyc.getClasspath());
		System.out.println("encoding "+groovyc.getEncoding());

		groovyc.execute();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String args[])
	{
		try {
			new Build().run();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ResourceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
/*
class Build {
String srcdirectory ="~/TXM/scripts/src"

	AntBuilder ant = new AntBuilder().sequential {
	def webinf = "."

	taskdef name: "groovyc", classname: "org.codehaus.groovy.ant.Groovyc"
	groovyc srcdir: "src", destdir: "${webinf}", encoding: "UTF-8", {
		classpath {
			fileset dir: "${webinf}/plugins", {
		    	include name: "*.jar"
			}
			pathelement path: "${webinf}"
		}
		javac source: "1.5", target: "1.5", debug: "on"
	}
}


   def base_dir = "./"
   def src_dir = base_dir + "src"
   def lib_dir = base_dir + "lib"
   def build_dir = base_dir + "bin"
   def dist_dir = base_dir + "dist"
   def file_name = "mdecorde"

   def clean() {
      ant.delete(dir: "${build_dir}")
      ant.delete(dir: "${dist_dir}")
   }
   def build() {
      ant.mkdir(dir: "${build_dir}")
      ant.javac(destdir: "${build_dir}", srcdir: "${src_dir}", classpath: "${classpath}")
      ant.groovyc(destdir: "${build_dir}", srcdir: "${src_dir}", classpath: "${classpath}")              
   }
   def jar() {
      clean()
      build()
      ant.mkdir(dir: "${dist_dir}")
      ant.jar(destfile: "${dist_dir}/${file_name}.jar", basedir: "${build_dir}")
   }
   def static void main(args) {
      Build b = new Build()
      b.run(args)
   }
   void run(args) {
      if ( args.size() > 0 ) { 
         invokeMethod(args[0], null )
      }
      else {
        // build()
      } 
   }
}*/
