// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

import org.txm.index.core.functions.Lexicon;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.CorpusManager;
// TODO: Auto-generated Javadoc

/**
 * temporary class.
 *
 * @author mdecorde
 */
class TxmToSQL 
{
	
	/**
	 * Lexicon to sql.
	 *
	 * @param corpus the corpus
	 * @param lexproperties the lexproperties
	 * @return the string
	 */
	static String LexiconToSQL(Corpus corpus, Property lexproperties)
	{
		
		Lexicon lex = corpus.getLexicon(lexproperties);
		
		String[] forms = lex.getForms();
		
		HSQLFunctions.CreateSimpleTable(lex.getName(),"form","VARCHAR(50)",forms)
		return lex.getName();
	}
	
	/**
	 * Main.
	 *
	 * @param args the args
	 */
	static main(args) 
	{
		//lexicon example
		CorpusManager cm = CorpusManager.getCorpusManager();
		Corpus corpus = cm.getCorpus("RGAQCJ");
		Property p = corpus.getProperty("word");
		
		String table = TxmToSQL.LexiconToSQL(corpus,p);
		HSQLFunctions.printTable(table);
	}
}
