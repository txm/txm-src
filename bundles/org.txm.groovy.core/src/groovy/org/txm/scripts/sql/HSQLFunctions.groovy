// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-07-04 16:11:30 +0200 (lun. 04 juil. 2016) $
// $LastChangedRevision: 3257 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

import java.util.LinkedList;

import groovy.sql.Sql;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.logging.Level;

import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * set of Sql function for HSQL.
 *
 * @return the connection
 * @author mdecorde
 */
class HSQLFunctions {
	
	Connection c
	String path;
	
	public HSQLFunctions(String path) {
		this.path = path;
		
		String urlProperty = "jdbc:hsqldb:file:"+path+"/db;shutdown=true;hsqldb.write_delay=false;"
		if (Log.getLevel().intValue() < Level.INFO.intValue()) {
			urlProperty += "";
		} else {
			urlProperty += "hsqldb.applog=0;hsqldb.sqllog=0";
		}
		
		this.c = DriverManager.getConnection(urlProperty, "SA", "")
	}

	public Connection getConnection() {
		return c;
	}
	
	/**
	 * Creates the table.
	 *
	 * @param name the name
	 * @param argsname the argsname
	 * @param types the types
	 */
	public void CreateTable(String name)
	{
		Statement statement = c.createStatement();
		String query ="CREATE TABLE "+name+";";
		
		println("query : "+query);
		statement.executeUpdate(query);
		
		statement.close();
		c.commit();
	}
	
	/**
	 * Creates the table.
	 *
	 * @param name the name
	 * @param argsname the argsname
	 * @param types the types
	 */
	public void CreateTable(String name, String idColumn)
	{
		Statement statement = c.createStatement();
		String query = "CREATE TABLE \""+name+"\" (\""+idColumn+"\" INTEGER IDENTITY PRIMARY KEY);";
	
		println("query : "+query);
		statement.executeUpdate(query);
		
		statement.close();
		c.commit();
	}
	
	public boolean IsTableExisting(String name) {
		boolean e = false;
		
		DatabaseMetaData dbm = c.getMetaData();
		ResultSet tables = dbm.getTables(null, null, name, null);
		e = tables.next();
		tables.close()
		
		return e;		
	}
	
	
	/**
	 * Creates the table.
	 *
	 * @param name the name
	 * @param argsname the argsname
	 * @param types the types
	 */
	public void CreateTable(String name, def argsname, def types)
	{
		assert(argsname.size() == types.size());
		
		Statement statement = c.createStatement();
		String query ="CREATE TABLE "+name+" ( ";
		for (int i=0; i < types.size() ; i++) {
			query += argsname[i]+" "+types[i]
			if (i < types.size()-1)	{query += ",";}
		}
		query +=")";
		
		println("query : "+query);
		statement.executeUpdate(query);
		
		statement.close();
		c.commit();
	}
	
	
	public void DeleteTable(String name) {
		Statement statement = c.createStatement();
		statement.executeUpdate("DROP TABLE entries;");
		statement.close();
		c.commit();
	}

	/**
	 * Insert csv col.
	 *
	 * @param TableName the table name
	 * @param argsnames the argsnames
	 * @param types the types
	 * @param csvFile the csv file
	 * @param separator the separator
	 * @param encoding the encoding
	 */
	public void InsertCsvCol(String TableName, def argsnames, def types, File csvFile, String separator, String encoding)
	{
		Statement statement = c.createStatement();
		statement.setEscapeProcessing(true);
		Reader reader = new InputStreamReader(new FileInputStream(csvFile),encoding);
		reader.splitEachLine(separator) {fields ->
			//println("fields size "+fields.size())
			String query ="INSERT INTO "+TableName+" (";
			for(int i=0;i< argsname.size; i++)
			{
				query+= argsname[i];
				if(i < argsnames.size-1)	{query += ",";}
			}
			query +=") VALUES (";
			for(int i=0;i< fields.size(); i++)
			{
				if(fields[i].contains("'"))
					query+= "'"+fields[i].replace("'","''")+"'";
				else
					query+= "'"+fields[i]+"'";
				
				if(i < argsnames.size-1)	{query += ",";}
			}
			query +=")";
			
			//println("query : "+query);
			statement.executeUpdate(query);
		}
		statement.close();
		c.commit();
	}

	/**
	 * Import ordered csv table.
	 *
	 * @param TableName the table name
	 * @param argsname the argsname
	 * @param types the types
	 * @param csvFile the csv file
	 * @param separator the separator
	 * @param encoding the encoding
	 * @return the int
	 */
	public int ImportOrderedCSVTable(String TableName, List<String> argsname, List<String> types, File csvFile, String separator, String encoding)
	{
		argsname.add("n");
		types.add("INT");
		HSQLFunctions.CreateTable( TableName, argsname, types);
		argsname.remove("n");
		types.remove("INT");
		Statement statement = c.createStatement();
		statement.setEscapeProcessing(true);
		Reader reader = new InputStreamReader(new FileInputStream(csvFile),encoding);
		int n=0;
		reader.splitEachLine(separator) {fields ->
			n++;
			//println("fields size "+fields.size())
			String query ="INSERT INTO "+TableName+" (n,";
			for(int i=0;i< argsname.size(); i++)
			{
				query+= argsname[i];
				if(i < argsname.size()-1)	{query += ",";}
			}
			query +=") VALUES ('"+n+"',";
			for(int i=0;i< argsname.size(); i++)
			{
				if( i >= fields.size())
				{
					query+= "''";
				}
				else
				{
					if(fields[i].contains("'"))
						query+= "'"+fields[i].replace("'","''")+"'";
					else
						query+= "'"+fields[i]+"'";
				}
				if(i < argsname.size()-1)	{query += ",";}
			}
			query +=")";
			
			//println("query : "+query);
			statement.executeUpdate(query);
		}
		statement.close();
		c.commit();
		return n;
	}

	/**
	 * Import csv table.
	 *
	 * @param TableName the table name
	 * @param argsname the argsname
	 * @param types the types
	 * @param csvFile the csv file
	 * @param separator the separator
	 * @param encoding the encoding
	 */
	public void ImportCSVTable(String TableName, def argsname, def types, File csvFile, String separator, String encoding)
	{
		HSQLFunctions.CreateTable( TableName, argsname, types);
		
		Statement statement = c.createStatement();
		statement.setEscapeProcessing(true);
		Reader reader = new InputStreamReader(new FileInputStream(csvFile),encoding);
		reader.splitEachLine(separator) {fields ->
			//println("fields size "+fields.size())
			String query ="INSERT INTO "+TableName+" (";
			for(int i=0;i< argsname.size; i++)
			{
				query+= argsname[i];
				if(i < argsname.size-1)	{query += ",";}
			}
			query +=") VALUES (";
			for(int i=0;i< argsname.size; i++)
			{
				if( i >= fields.size())
				{
					query+= "''";
				}
				else
				{
					if(fields[i].contains("'"))
						query+= "'"+fields[i].replace("'","''")+"'";
					else
						query+= "'"+fields[i]+"'";
				}
				if(i < argsname.size-1)	{query += ",";}
			}
			query +=")";
			
			//println("query : "+query);
			statement.executeUpdate(query);
		}
		statement.close();
		c.commit();
	}

	// word column val column val2 column val3
	
	/**
	 * Import ref table.
	 *
	 * @param TableName the table name
	 * @param argsname the argsname
	 * @param types the types
	 * @param csvFile the csv file
	 * @param separator the separator
	 * @param encoding the encoding
	 */
	public void ImportRefTable(String TableName, def argsname, def types, File csvFile, String separator, String encoding)
	{
		HSQLFunctions.CreateTable( TableName, argsname, types);
		
		Statement statement = c.createStatement();
		statement.setEscapeProcessing(true);
		Reader reader = new InputStreamReader(new FileInputStream(csvFile),encoding);
		reader.splitEachLine(separator) 
				{fields ->
					String ref;
					if(fields[0].contains("'"))
						ref= "'"+fields[0].replace("'","''")+"'";
					else
						ref= "'"+fields[0]+"'";
					
					for(int j=1 ; j < fields.size() ; j +=argsname.size -1)
					{
						String query ="INSERT INTO "+TableName+" (";
						for(int i=0;i< argsname.size; i++)
						{
							query+= argsname[i];
							if(i < types.size-1)	{query += ",";}
						}
						query +=") VALUES (";
						query+= ref+",";
						for(int i=j;i < j+argsname.size -1; i++)
						{
							if(fields[i].contains("'"))
								query+= "'"+fields[i].replace("'","''")+"'";
							else
								query+= "'"+fields[i]+"'";
							
							if(i < j+argsname.size -2)	{query += ",";}
						}
						query +=")";
						
						//println("query : "+query);
						statement.executeUpdate(query);
					}
				}
		statement.close();
		c.commit();
	}

	/**
	 * Creates the simple table.
	 *
	 * @param TableName the table name
	 * @param argsname the argsname
	 * @param type the type
	 * @param values the values
	 */
	public void CreateSimpleTable(String TableName, String argsname , String type, def values)
	{
		def argsnames = [argsname];
		def types = [type]
		HSQLFunctions.CreateTable( TableName, argsnames, types);
		
		Statement statement = c.createStatement();
		for(int i=0; i < values.size; i++)
		{
			String query ="INSERT INTO "+TableName+" (";
			query+= argsname;
			query +=") VALUES ('";
			query+= values[i];
			query +="')";
			
			println("query : "+query);
			statement.executeUpdate(query);
		}
		statement.close();
		c.commit();
	}

	/**
	 * Test.
	 *
	 * @param table the table
	 * @param file the file
	 * @return the java.lang. object
	 */
	public test(String table, String file) {
		Statement statement = c.createStatement();

		statement.execute("SET TABLE " + table + " SOURCE \"" + file
				+ ";ignore_first=false;all_quoted=false\"");
		statement.close();
		c.commit();
	}

	// fonction temporaire, j'ai rien trouvé de mieux -_-
	
	/**
	 * Gets the column number.
	 *
	 * @param TableName the table name
	 * @return the column number
	 */
	public int getColumnNumber(String TableName)
	{
		Sql sql = Sql.newInstance(c)
		int i =0;
		try {
			sql.eachRow("select * from "+TableName) {
				for(i=0;;i++)
					it.getAt(i);
			}
		}
		catch(Exception e)
		{
			return i;
		}
		
		c.close();
		return 0;
	}

	/**
	 * Prints the table.
	 *
	 * @param TableName the table name
	 */
	public void printTable(String TableName)
	{
		Sql sql = Sql.newInstance(c)
		int colnum = getColumnNumber(TableName);
		sql.eachRow("select * from "+TableName) {
			for(int i=0;i< colnum; i++)
			{
				print(it.getAt(i));
				if(i < colnum -1)print("\t");
			}
			println();
		}
		
		c.commit();
	}

	/**
	 * Execute query.
	 *
	 * @param query the query
	 */
	public void executeQuery(String query) {
		Statement statement = c.createStatement();
		statement.setEscapeProcessing(true);
		println("query : " + query);
		statement.execute(query);
		statement.close();
		c.commit();
	}

	/**
	 * Gets the groovy sql.
	 *
	 * @return the groovy sql
	 */
	public Sql getGroovySql() {
		return Sql.newInstance(c)
	}

	/**
	 * To cvs file.
	 *
	 * @param TableName the table name
	 * @param outfile the outfile
	 */
	public void toCVSFile(String TableName, String outfile) {
		def sql = Sql.newInstance(c)
		
		Writer writer = new FileWriter(outfile);
		
		int colnum = getColumnNumber(TableName);
		sql.eachRow("select * from "+TableName) {
			for(int i=0;i< colnum; i++)
			{
				//println('get at '+i)
				writer.write(it.getAt(i));
				if(i < colnum -1)writer.write("\t");
			}
			writer.write("\n");
		}
		
		writer.close();
		c.commit();
	}

	// only for sql result with 3 columns
	
	/**
	 * To ref file.
	 *
	 * @param TableName the table name
	 * @param outfile the outfile
	 * @param groupcolumn the groupcolumn
	 * @param argname the argname
	 * @param encoding the encoding
	 * @return the int
	 */
	public int toRefFile(String TableName, String outfile, String groupcolumn,def argname, String encoding)
	{
		assert(argname.size() == 2);
		def sql = Sql.newInstance(c)
		LinkedList<String> linkset;
		File f = new File(outfile);
		Writer writer = new BufferedWriter(new OutputStreamWriter(new PrintStream(new FileOutputStream(f),true,encoding)));
		def content = new File(outfile).getText(encoding) 
		int colnum = getColumnNumber(TableName);
		String ref = "";
		String line ="";
		boolean firstline = true;
		HashMap<String,LinkedList<String>> reftable = new HashMap<String,LinkedList<String>>();
		HashMap<String,LinkedList<String>> lemmetable = new HashMap<String,LinkedList<String>>();
		
		String query = "SELECT "+groupcolumn;
		for (String s : argname)
			query +=","+s
		//System.out.println(query+" FROM "+TableName)
		sql.eachRow(query+" FROM "+TableName){
			String currentref = it.getAt(0);
			if (!reftable.containsKey(currentref)) {
				linkset = new LinkedHashSet<String>();
				reftable.put(currentref,linkset)
				linkset = new LinkedHashSet<String>();
				lemmetable.put(currentref,linkset)
			}
			
			linkset = reftable.get(currentref)
			if (!linkset.contains(it.getAt(1))) {
				linkset.add(""+it.getAt(1))
				linkset = lemmetable.get(currentref)
				linkset.add(""+it.getAt(2))
			}
		}
		//println("ref : "+reftable)
		for (String key : reftable.keySet()) {
			writer.write (key)
			for (int i = 0 ; i < reftable.get(key).size(); i++) {
				String s = (reftable.get(key)).get(i);
				String l = (lemmetable.get(key)).get(i);
				writer.write ("\t"+s+"\t"+l);
			}
			writer.write ("\n")
		}
		
		writer.close();
		c.commit();
		return reftable.size();
	}

	/**
	 * To ref hierarchical file.
	 *
	 * @param TableName the table name
	 * @param outfile the outfile
	 * @param groupcolumn the groupcolumn
	 * @param argname the argname
	 * @param encoding the encoding
	 * @return the int
	 */
	public int toRefHierarchicalFile(String TableName, String outfile, String groupcolumn,def argname, String encoding)
	{
		assert(argname.size() == 2);
		def sql = Sql.newInstance(c);
		LinkedList<String> linkset;
		File f = new File(outfile);
		Writer writer = new BufferedWriter(new OutputStreamWriter(new PrintStream(new FileOutputStream(f),true,encoding)));
		def content = new File(outfile).getText(encoding);
		int colnum = getColumnNumber(TableName);
		String ref = "";
		String line ="";
		boolean firstline = true;
		HashMap<String,LinkedList<String>> reftable = new HashMap<String,LinkedList<String>>();
		//HashMap<String,HashMap<String,Integer>> counttable = new HashMap<String,HashMap<String,Integer>>();
		HashMap<String,LinkedList<Integer>> counttable = new HashMap<String,LinkedList<Integer>>();
		HashMap<String,LinkedList<String>> lemmetable = new HashMap<String,LinkedList<String>>();
		
		String query = "SELECT "+groupcolumn;
		for(String s : argname)
			query +=","+s;
		//System.out.println(query+" FROM "+TableName)
		sql.eachRow(query+" FROM "+TableName){
			String currentref = it.getAt(0);
			if (!reftable.containsKey(currentref)) {
				linkset = new LinkedHashSet<String>();
				reftable.put(currentref,linkset);
				linkset = new LinkedHashSet<String>();
				lemmetable.put(currentref,linkset);
			}
			
			linkset = reftable.get(currentref);
			if (!linkset.contains(it.getAt(1))) {
				linkset.add(""+it.getAt(1));
				linkset = lemmetable.get(currentref);
				linkset.add(""+it.getAt(2));
			}
			
			
			//if(it.getAt(2) != "-")
			//{
			if(!counttable.containsKey(currentref))
				counttable.put(currentref, new HashMap<String,Integer>());
			
			HashMap<String,Integer> ht = counttable.get(currentref);
			if(!ht.containsKey(it.getAt(1)))
				ht.put(it.getAt(1),0);
			
			ht.put(it.getAt(1),ht.get(it.getAt(1)) +1)	
			
			//}
		}
		//println("countable : "+counttable)
		for (String key : counttable.keySet()) {
			HashMap<String,Integer> ht = counttable.get(key);
			LinkedList<String> indexorder = new LinkedList<Integer>();
			
			for(String cat : counttable.get(key).keySet())
			{
				if(indexorder.size() == 0)
				{
					indexorder.add(cat)
				}
				else
				{
					boolean inserted= false;
					for(int j = 0 ; j < indexorder.size();j++)
					{
						String comp = indexorder.get(j);
						if(ht.get(cat) > ht.get(comp))
						{
							indexorder.add(j, cat);
							inserted=true;
							break;
						}
					}
					if(!inserted)
						indexorder.addLast(cat);
				}
			}
			writer.write(key);
			for (String cat : indexorder) {
				
				int i=0;
				for(int r=0; r < reftable.get(key).size();r++)
					if((reftable.get(key)).get(r) == cat)
					{
						i=r;
						break;
					}
				String s = (reftable.get(key)).get(i);
				String l = (lemmetable.get(key)).get(i);
				writer.write ("\t"+s+"\t"+l);
			}
			writer.write ("\n")
		}
		
		writer.close();
		c.commit();
		return reftable.size();
	}

	/**
	 * Clear all.
	 */
	public void clearAll() {
		new File(path, "db.script").delete();
		new File(path, "db.properties").delete();
		new File(path, "db.log").delete();
		new File(path, "db.tmp").deleteDir();
	}
	
	/**
	 * Clear all.
	 */
	public void close() {
		c.close();
	}

	/**
	 * Main.
	 *
	 * @param args the args
	 */
	public static void main(String[] args)
	{
		HSQLFunctions hsql = new HSQLFunctions("/home/mdecorde/TEMP")
		//hsql.clearAll();
		
	//	hsql.executeQuery("SELECT col1 from test where col1 LIKE '@'' ESCAPE '@' ")
		
		def argsname = ["n","col1","col2"];
		def types = ["INT","VARCHAR(30)","VARCHAR(30)"];
		
		hsql.CreateTable( "test", argsname, types);
		hsql.CreateTable( "test2", argsname, types);
		hsql.executeQuery("INSERT INTO test VALUES (1,'poeut',1)");
		hsql.executeQuery("INSERT INTO test VALUES (2,'poeut',2)");
		hsql.executeQuery("INSERT INTO test VALUES (3,'poeut',3)");
		hsql.executeQuery("INSERT INTO test VALUES (4,'poeut',4)");
		hsql.executeQuery("INSERT INTO test VALUES (5,'poeut',5)");
		hsql.executeQuery("INSERT INTO test VALUES (5,'poeut',1)");
		
		hsql.executeQuery("INSERT INTO test2 VALUES (11,'poeut',52)");
		hsql.executeQuery("INSERT INTO test2 VALUES (22,'poeut',42)");
		hsql.executeQuery("INSERT INTO test2 VALUES (33,'poeut',32)");
		hsql.executeQuery("INSERT INTO test2 VALUES (44,'poeut',22)");
		hsql.executeQuery("INSERT INTO test2 VALUES (45,'poeut',22)");
		hsql.executeQuery("INSERT INTO test2 VALUES (55,'poeut',12)");
		
		hsql.printTable("test")
		hsql.printTable("test2")

		hsql.close();
		
		//
		//hsql.CreateTable( "test", argsname, types);
		//	hsql.ImportOrderedCSVTable( "test", argsname, types, new File("~/xml/rgaqcj/lexicon/lex_R"),"\t","UTF-8");
		//hsql.test("test","test.csv");
		//def forms=["hello","pouet","truc"]
		//hsql.CreateSimpleTable("test","form","VARCHAR(20)",forms)
		//hsql.printTable("test");
		//hsql.executeQuery("SELECT col1 from test where col1 LIKE '@'' ESCAPE '@' ");
		

	}
}
