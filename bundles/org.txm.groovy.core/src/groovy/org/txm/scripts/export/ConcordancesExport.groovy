/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-04-19 16:23:38 +0200 (mer. 19 avril 2017) $
// $LastChangedRevision: 3430 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.export

// import the packages containing the functions we are going to use
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.concordance.core.functions.Concordance
import org.txm.concordance.core.functions.Line
import org.txm.concordance.core.functions.comparators.LexicographicKeywordComparator
import org.txm.functions.concordances.*
import org.txm.functions.concordances.comparators.*
import org.txm.searchengine.cqp.ReferencePattern

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
def manualInit = false
if (!org.txm.Toolbox.isInitialized())
{
	manualInit = true
	org.txm.Toolbox.initialize(new File("~/TXM/install.prefs"))
}

// get the BFM corpus
def discours = CorpusManager.getCorpusManager().getCorpus("VOEUX")

// get some properties
def pos = discours.getProperty("frpos")
println "pos: "+pos
def id = discours.getProperty("id")
println "id: "+id
def word = discours.getProperty("word")
println "word: "+word
def text = discours.getStructuralUnit("text")
println "text: "+text
def text_id = text.getProperty("id")
println "text_id: "+text_id

// create a query. Edit the query here
def querys = javax.swing.JOptionPane.showInputDialog(null, "query ")
if (querys == null)
	return
def query = new Query(Query.fixQuery(querys))

println "Compute concordance "
def start = System.currentTimeMillis()

// define the references pattern for each concordance line
def referencePattern = new ReferencePattern()
referencePattern.addProperty(text_id)
referencePattern.addProperty(pos)
referencePattern.addProperty(id)

def sortReferencePattern = new ReferencePattern()
sortReferencePattern.addProperty(text_id)
sortReferencePattern.addProperty(pos)
sortReferencePattern.addProperty(id)

// compute the concordance with contexts of 15 words on each side of the keyword
def concordance = new Concordance(discours, query, word, [word, pos], referencePattern, sortReferencePattern, 10, 10)

// get a builtin sort function
def comparator = new LexicographicKeywordComparator()
comparator.initialize(discours)

// sort the concordance
//concordance.sort(comparator)

println "done "+(System.currentTimeMillis()-start)

def writer = new FileWriter("ExportConcordanceBFM3TT.csv")

println "write lines"
start = System.currentTimeMillis()

// define which occurrence properties will be displayed
concordance.setViewProperties([word])

for (int i = 0 ; i < concordance.getNLines() ; i += 1000)
{
	List<Line> lines
	if (i+1000 > concordance.getNLines())
		lines = concordance.getLines(i, concordance.getNLines()-1)
	else
		lines = concordance.getLines(i, i+1000)
		
	// println "lines: from "+i
	for (Line l : lines)
	{
		/*
		 A : référence (nom de texte + page)
		 B : contexte gauche (10 mots)
		 C : pivot (cf. la remarque sur "ce que")
		 D : pos du pivot
		 E : colonne vide pour les commentaires
		 F : contexte droit (10 mots)
		 G : identifiant de l'occurrence (pour injecter les corrections après)
		 */
		
		String ntext = l.getViewRef().getValue(text_id)
		String lcontext = l.leftContextToString()
		String pivot = l.keywordToString()
		String rien = ""
		String rcontext = l.rightContextToString()
		String ids = l.getViewRef().getValue(id)
		
		String poss = ""
		for(String s : l.getKeywordsViewProperties().get(pos))
		{
			poss += s+" "
		}
		
		//println l.getKeywordsViewProperties().get(pos)
		writer.write(ntext+"\t"+lcontext+"\t"+pivot+"\t"+poss+"\t"+rien+"\t"+rcontext+"\t"+ids+"\n")
	}
	writer.flush()
}
writer.close()

println "done "+(System.currentTimeMillis()-start)+" results"

println("Concordance "+query.getQueryString()+" saved in file "+new File("ExportConcordanceBFM3TT.csv").getAbsolutePath())

if(manualInit)
	org.txm.Toolbox.shutdown()

