package org.txm.scripts.export
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.functions.cooccurrences.*
import org.txm.lexicaltable.core.functions.LexicalTable


def corpus = CorpusManager.getCorpusManager().getCorpus("VOEUX") // mettre le nom du corpus en Majuscules ici
def partition = corpus.getPartition("VOEUX_text_id");

int NTable = 1;
for (Object o : partition.getResults()) {
	if (o instanceof LexicalTable) {
		File outfile = new File("lt${NTable}.txt")
		println "export LT of $partition in "+outfile.getAbsolutePath();
		NTable++;
		outfile.withWriter{writer->
		def lt = (LexicalTable)o;
		println "found lexical table: "+lt.getName()
		println "n col: "+lt.getNColumns()
		println "n line: "+lt.getNRows()
		def cols = lt.getColNames().asStringsArray();
		String strline = "unit\t";
		for (int i = 0 ; i < cols.length ; i++) {
			strline += cols[i]+"\t"
			
		}
		writer.println(strline.substring(0, strline.length()-1));
		
		def rows = lt.getRowNames().asStringsArray();
		for (int i = 0 ; i < rows.length ; i++) {
			strline = "";
			strline += rows[i]+"\t";
			def line = lt.getRow(i).asIntArray();
			for (int j = 0 ; j < line.length ; j++)
				strline += line[j]+"\t";
			writer.println(strline.substring(0, strline.length()-1));
		}
		
		writer.close();
		}
	}
}
