// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2011-09-27 16:44:37 +0200 (mar., 27 sept. 2011) $
// $LastChangedRevision: 2008 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.export

import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.concordance.core.functions.Concordance
import org.txm.concordance.core.functions.Line
import org.txm.functions.concordances.*
import org.txm.functions.concordances.comparators.*
import org.txm.searchengine.cqp.ReferencePattern

////// PARAMETERS //////

def output = "nomfichier"

def tgtCorpusName = "BLLATLAT"
def srcCorpusName = "BLLATFRO"

def tgtWords = "sum|ego" // use to highlight lat words
def srcWords = "je|j'" // use to highlight fro words

def tgtQuery = "<seg> []* \""+tgtWords+"\" []* </seg> :"+srcCorpusName+" \""+srcWords+"\"" // default lat query
def srcQuery = "<seg> []* \""+srcWords+"\" []* </seg> :"+tgtCorpusName+" \""+tgtWords+"\"" // default fro query

println tgtCorpusName+": "+tgtQuery
println srcCorpusName+": "+srcQuery

new File(output).withWriter("UTF-8"){writer->
//// LAT CONCORDANCE ////
def tgtCorpus = CorpusManager.getCorpusManager().getCorpus(tgtCorpusName)
def text_id = tgtCorpus.getStructuralUnit("text").getProperty("id")
def seg_id = tgtCorpus.getStructuralUnit("seg").getProperty("id")
def word = tgtCorpus.getProperty("word")
def referencePattern = new ReferencePattern()
//referencePattern.addProperty(text_id)
referencePattern.addProperty(seg_id)
def tgtConcordance = new Concordance(tgtCorpus, new Query(tgtQuery), word, [word], referencePattern, referencePattern, 0, 0)

//// LAT CONCORDANCE ////
def srcCorpus = CorpusManager.getCorpusManager().getCorpus(srcCorpusName)
text_id = srcCorpus.getStructuralUnit("text").getProperty("id")
seg_id = srcCorpus.getStructuralUnit("seg").getProperty("id")
word = srcCorpus.getProperty("word")
referencePattern = new ReferencePattern()
//referencePattern.addProperty(text_id)
referencePattern.addProperty(seg_id)
def srcConcordance = new Concordance(srcCorpus, new Query(srcQuery), word, [word], referencePattern, referencePattern, 0, 0)

//// MERGE CONCORDANCE RESULT ////
HashSet<String> allKeys = new HashSet<String>()
HashMap<String, Line> srcLines = [:]
HashMap<String, Line> tgtLines = [:]

println "nb tgt result: "+tgtConcordance.getNLines()
for(Line line : tgtConcordance.getLines(0, tgtConcordance.getNLines()))
{
	allKeys.add(line.getViewRef().toString())
	tgtLines.put(line.getViewRef().toString(), line)
}

println "nb src result: "+srcConcordance.getNLines()
for(Line line : srcConcordance.getLines(0, srcConcordance.getNLines()))
{
	allKeys.add(line.getViewRef().toString())
	srcLines.put(line.getViewRef().toString(), line)
}
println "src seg: "+srcLines.keySet()
println "tgt seg: "+tgtLines.keySet()
println "nb segments: "+allKeys.size()

writer.println "segment\tsource\ttarget"
for(Line line : srcConcordance.getLines(0, srcConcordance.getNLines()))
{
	String key = line.getViewRef().toString();
	def tgtLine = tgtLines.get(key)
	String srcString = line.keywordToString();
	String tgtString = "";
	if(tgtLine != null)	{
		tgtString = tgtLine.keywordToString();
		tgtLines.remove(key)
		}else
	{
		key = key+"*"
	}
	srcString = srcString.replaceAll("("+srcWords+")", "*\$1*");
	tgtString = tgtString.replaceAll("("+tgtWords+")", "*\$1*");
	writer.println key+"\t"+srcString+"\t"+tgtString
}

println "\n"
for(String key : tgtLines.keySet()) // process remaining tgt lines
{
	def srcline = srcLines.get(key)
	def tgtline = tgtLines.get(key)
	String srcString = "";
	String tgtString = tgtLines.keywordToString();
	if(srcline == null){
		srcString = srcline.keywordToString();
	}else
	{
		key = key+"*"
	}
	srcString = srcString.replaceAll("("+srcWords+")", "*\$1*");
	tgtString = tgtString.replaceAll("("+tgtWords+")", "*\$1*");
	writer.println key+"\t"+srcString+"\t"+tgtString
}
}

println "Done : written in "+new File(output).getAbsolutePath();