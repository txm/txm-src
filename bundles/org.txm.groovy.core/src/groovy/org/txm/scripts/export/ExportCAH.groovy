package org.txm.scripts.export

import org.txm.ahc.core.functions.AHC
import org.txm.rcp.views.*
import org.txm.stat.engine.r.*
// uncomment this
import org.eclipse.jface.viewers.*

// Select the CAH result in the CorporaView and run this script

// PARAMETER: the output file
def outfile = new File("/home/mdecorde/Bureau/CAH.txt")

// get selected object from Corpora view
def obj = null;
monitor.syncExec(new Runnable() {
	@Override
	public void run() {
		def corporaview = CorporaView.getInstance()
		def tree = corporaview.getTreeViewer()
		// uncomment these 2 lines
		def sel = tree.getSelection()
		obj = sel.getFirstElement()
		}
	});
println("Selected object: "+obj)

if (obj != null && obj instanceof AHC) {
	def rw = RWorkspace.getRWorkspaceInstance()
	rw.eval("sink(file=\"" + outfile.getAbsolutePath().replace("\\", "\\\\")+"\")")
	rw.eval("print("+((AHC)obj).getSymbol()+")")
	rw.eval("sink()")
	println("CAH saved in file: "+outfile)
}