package org.txm.scripts.export

// we import the packages containing the functions we are going to use
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.cooccurrence.core.functions.Cooccurrence
import org.txm.functions.cooccurrences.*

//////////// PARAMETERS ////////////
def corpus = CorpusManager.getCorpusManager().getCorpus("VOEUX")

def word = corpus.getProperty("word")
def pos = corpus.getProperty("frpos")
def viewprops = [word, pos]

def query = new Query(Query.fixQuery("je"))
def PROPSVALUETOKEEP = "V.+"
def PROPNUMBER = 1;

def limit = corpus.getStructuralUnit("s") // contexts by <s>
def includeXpivot = false; // used only if limit != null
//def limit = null // contexts by word 
def minleft = 1 // min = 1
def maxleft = 20 // min = 1
def minright = 1 // min = 1
def maxright = 20 // min = 1

def minf = 0
def mincof = 0
def minscore = 0

def buildLTWithOnlyCooccurrents = false

//////////// END OF PARAMETERS ////////////

println "computing cooc lines..."
long time = System.currentTimeMillis()
def cooccurrence = new Cooccurrence(corpus, query, viewprops, limit, maxleft, minleft, minleft, maxright, minf,mincof, minscore, includeXpivot, buildLTWithOnlyCooccurrents)
cooccurrence.process();
println("Compute time : "+(System.currentTimeMillis()-time)+" ms")

println "filtering lines..."
def lines = cooccurrence.getLines()
for(int i = 0 ; i < lines.size(); i++)
{
	def line = lines.get(i)
	if( !line.props.get(PROPNUMBER).matches(PROPSVALUETOKEEP))
	{
		lines.remove(i--)
	}	
}

println "writing result..."
def file = new File("cooc.txt")
cooccurrence.toTxt(file, "UTF-8")
println "printed cooccurrents in "+file.getAbsolutePath()