package org.txm.scripts.export
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.ca.core.functions.CA
import org.txm.functions.ca.*

String moncorpus = "VOEUX"
String mapartition = "texts"


def corpus = CorpusManager.getCorpusManager().getCorpus(moncorpus) // mettre le nom du corpus en Majuscules ici
def partition = corpus.getPartition(mapartition);

for(Object o : partition.getResults())
{
	if(o instanceof CA)
	{
		CA ca = ((CA)o);
		File colinfosfile = new File(ca.getName()+"_colinfosfile.txt");
		File rowinfosfile = new File(ca.getName()+"_rowinfosfile.txt");
	
		
		ca.exportColInfos(colinfosfile, "UTF-8", "\t", "")
		ca.exportRowInfos(rowinfosfile, "UTF-8", "\t", "")
		
		println CA.getName()+" exported in : "+rowinfosfile.getAbsolutePath()+" and "+colinfosfile.getAbsolutePath()
	}
}