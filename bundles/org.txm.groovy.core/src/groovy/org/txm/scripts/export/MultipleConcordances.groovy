/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2011-05-09 14:58:28 +0200 (lun., 09 mai 2011) $
// $LastChangedRevision: 1839 $
// $LastChangedBy: mdecorde $ 
//

package org.txm.scripts.export

// import the packages containing the functions we are going to use
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.concordance.core.functions.Concordance
import org.txm.concordance.core.functions.comparators.LexicographicLeftContextComparator
import org.txm.functions.concordances.*
import org.txm.functions.concordances.comparators.*
import org.txm.searchengine.cqp.ReferencePattern

//PARAMETRES
def corpusname = "VOEUX"
def requetes = ["je", "tu", "vous"]
// get the DISCOURS corpus
// TODO: Auto-generated Javadoc

/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
def discours = CorpusManager.getCorpusManager().getCorpus(corpusname)

// get some properties
def pos = discours.getProperty("pos")
def word = discours.getProperty("word")
def text = discours.getStructuralUnit("text")

//get structure text
def text_id = text.getProperty("id")
// and define the references pattern for each concordance line
def referencePattern = new ReferencePattern().addProperty(text_id)

// get a builtin sort function
def comparator = new LexicographicLeftContextComparator()
comparator.initialize(discours)

def noconc = 0
for(String querystring : requetes)
{
	println "Computing concordance of $querystring ..."
	// create a query. Here the concordance keyword will be the French word "Je" (the "I" pronoun in English)
	def query = new Query(Query.fixQuery(querystring))
	
	// compute the concordance with contexts of 15 words on each side of the keyword
	def concordance = new Concordance(discours, query, word, [word, pos], referencePattern, 15, 15)
		
	// sort the concordance
	//concordance.sort(comparator)
	
	// define which occurrence properties will be displayed
	concordance.setViewProperties([word])
	
	// write all the concordance in a text file
	def file = new File("conc"+(noconc++)+".txt")
	concordance.toTxt(file, "UTF-8")
	println("Concordance $querystring written in file: "+file.getAbsolutePath())
}