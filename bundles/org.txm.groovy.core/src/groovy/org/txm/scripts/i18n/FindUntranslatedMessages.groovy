package org.txm.scripts.scripts.i18n

import org.txm.scripts.scripts.i18n.ExternalizationFilesUpdater

def userdir = System.getProperty("user.home")
def workspace = new File(userdir, "workspace047")
def langs = ["fr", "ru"]

		scanner = new ExternalizationFilesUpdater();
		scanner.scanDirectory(new File(workspace, "org.txm.rcp"));
		for (def lang : langs) {
			scanner.createMissingFiles(lang);
		}
		scanner.processFoundDirectories();
		scanner.updateFiles();