package org.txm.scripts.scripts.i18n

import javax.swing.*;

/**
 * Scan a java file to find unexternalized Strings
 * create the Messages.java entries and the *.properties entries too
 * replace externalized Strings in the java file
 * 
 * uses a ReverseI18nDict
 * 
 * @author mdecorde
 */
class ExternalizeStringFile {

	File propFile;
	File messageFile

	ReverseI18nDict dict

	String prefix
	int iprefix = 0;
	
	boolean mustSaveChanges = false;

	public ExternalizeStringFile(File propFile, File messageFile) {
		dict = new ReverseI18nDict(propFile, messageFile);		
	}
	
	private ArrayList<String> getLineStrings(String l) {
		def strings = []
		boolean inS = false;
		int start = 0;
		int comment = 0;
		for(int i = 0 ; i < l.length() ; i++) {
			def c = l[i]
			if (inS) {
				comment = 0
				if (c == "\\") {
					i++;
				} else if (c == "\"") {
					inS = false;
					strings << l.substring(start, i+1);
				}
			} else {
				if (c == "\"") {
					inS = true;
					start = i;
					comment = 0
				} else if (c == "/") {
					comment++;
					if (comment == 2) break;
				} else {
					comment = 0
				}
			}
		}
		return strings;
	}
	
	BufferedReader reader;
	String line;
	String previousLine;
	boolean ignore = false;
	int nline = 1;

	def nlserrors = []
	def linesToWrite = []

	int nTotalChanges = 0;
	
	def strings;
	def nonnls;
	def toExternalize = []
	def toNLSize = []
	
	private int processSourceFile(File sourceFile, boolean processMode) {
		
		prefix = sourceFile.getName();
		prefix = prefix.substring(0, prefix.lastIndexOf("."))+"_" ;
		iprefix = 0;
		
		reader = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFile), "UTF-8"));
		line = reader.readLine();
		previousLine;
		ignore = false;
		nline = 1;

		nlserrors = []
		linesToWrite = []

		nTotalChanges = 0;
		
		while (line != null) {
			String l = line.trim()
			if (ignore) {
				if (l.startsWith("*/") || l.endsWith("*/")) {
					ignore = false
				} else if (l.startsWith("*")) {

				} else {
//					println "ERROR LINE $nline : not a comment ? "+l
//					return -1;
				}
			} else {
				if (l.startsWith("//")) {
					// ignore
				} else if (l.startsWith("@")) {
					// ignore
				} else if (l.startsWith("/*")) {
					ignore = true;
					if (l.endsWith("*/")) ignore=false
				} else {
					strings = getLineStrings(l);
					nonnls = getNONNLS(l);
					toExternalize = []
					toNLSize = []

					// fix NON-NLS
					if (nonnls.size() > 0 && nonnls.size() != strings.size()) {
						println "NLS error line $nline: "+l
						nlserrors << nline
					}

					if (nlserrors.size() == 0) {
						for (int i = 0 ; i < strings.size() ; i++) {
							if (nonnls.contains(i)) continue; // ignore

							if (strings[i] == "\"\"" || strings[i] == "\" \"" || strings[i] == "\"\\n\"") {
								toNLSize << i;
							} else {
								toExternalize << i
							}
						}

						if (processMode) {
							nTotalChanges += processLine();
						} else {
							nTotalChanges += (toExternalize.size() + toNLSize.size());
						}
					}
				}
			}

			linesToWrite << line
			previousLine = line;
			line = reader.readLine();
			nline++;
		}

		if (nlserrors.size() > 0) {
			println "NLS ERROR "+sourceFile.getName()+" lines $nlserrors"
			return -1;
		} else if (processMode && nTotalChanges > 0) {
			mustSaveChanges = true;
			File oldFile = new File(sourceFile.getParentFile(), sourceFile.getName()+".cpy");
			if (!sourceFile.renameTo(oldFile)) {
				println "Failed to backup old source file $sourceFile to $oldFile"
				return 0;
			} else {
				sourceFile.withWriter("UTF-8") { writer ->
					for(String l : linesToWrite) {
						writer.println l
					}
				}
			}
		}
		return nTotalChanges
	}
	
	private int processLine() {
		int nChanges = 0;
		int ichar = 0;
		for (int i = 0 ; i < strings.size() ; i++) {
			ichar = line.indexOf(strings[i], ichar);
			if (toExternalize.contains(i)) {
				println "Line $nline : $strings $nonnls $toNLSize $toExternalize"
				nChanges++;
				String value = strings[i].substring(1,strings[i].length() -1)
				if (dict.containsValue(value)) {
					String str = "Messages."+dict.getKey(value)
					println " automatic replace "+strings[i]+" with Messages."+dict.getKey(value);
					String p1 = line.substring(0, ichar);
					String p2 = line.substring(ichar+strings[i].length(), line.length())
					println " old: $line"
					line = p1+str+p2
					println " new: $line"
				} else {
					String key = getNextKey();
					String ok = JOptionPane.showInputDialog(null, "Key for value=$value\n\n$previousLine\n$line\n", key);
					if(ok == null) {
						println " add new NON-NLS "+(i+1)
						println " old: $line"
						line += " //\$NON-NLS-"+(i+1)+"\$"
						println " new: $line"
					} else {
						println " add new key: $key"
						dict.put(key, value);

						String p1 = line.substring(0, ichar);
						String p2 = line.substring(ichar+strings[i].length(), line.length())
						String str = "Messages."+key
						println " old: $line"
						line = p1+str+p2
						println " new: $line"
					}
				}
			} else if (toNLSize.contains(i)) {
				println "Line $nline : $strings $nonnls $toNLSize $toExternalize"
				nChanges++;
				println " add automatic new NON-NLS "+(i+1)
				println " old: $line"
				line += " //\$NON-NLS-"+(i+1)+"\$"
				println " new: $line"
			}
		}
		return nChanges;
	}

	private def getNONNLS(String l) {
		def idx = []
		def m = l =~ /\/\/.NON-NLS-[0-9]\$/
		m.each { idx << Integer.parseInt(it[11])-1 }
		return idx.sort()
	}

	private def getNextKey() {
		String tmp = prefix+(iprefix++);
		while (dict.containsKey(tmp)) {
			tmp = prefix+(iprefix++);
		}
		return tmp;
	}
	
	int getNbOfChanges(File sourceFile) {
		return processSourceFile(sourceFile, false);
	}
	
	int process(File sourceFile) {
		return processSourceFile(sourceFile, true);
	}
	
	public boolean saveChanges() {
		if (mustSaveChanges ) {
			mustSaveChanges = false;
			dict.saveChanges();
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		File sourceFile = new File("/home/mdecorde/workspace37/org.txm.core/src/java/org/txm/functions/cooccurrences/Cooccurrence.java");
		File propFile = new File("/home/mdecorde/workspace37/org.txm.core/src/java/org/txm/messages.properties")
		File messageFile = new File("/home/mdecorde/workspace37/org.txm.core/src/java/org/txm/Messages.java")

		ExternalizeStringFile externalizer = new ExternalizeStringFile(propFile, messageFile)
		println externalizer.getNbOfChanges(sourceFile);
		println externalizer.process(sourceFile);
		externalizer.saveChanges();
	}
}