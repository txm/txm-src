package org.txm.scripts.scripts.i18n

import org.txm.utils.LS;
import org.txm.utils.io.IOUtils;


def javaFileToPropertieFiles = [:]
def propertieFileToJavaFile = [:]

def langs = ["", "_fr", "_ru"] // lang pattern

public File getSRCDirectory(File projectDirectory) {
	def commons = ["src/main/java", "src/java", "src"]
	for (String root : commons) {
		if (new File(projectDirectory, root).exists()) {
			return new File(projectDirectory, root)
		}
	}
	return projectDirectory;
}

File workspace = new File(System.getProperty("user.home"), "workspace079");
def allProjectDirectories = LS.list(workspace, false, false)
def allProjectSRCDirectories = []
for (File projectDirectory : allProjectDirectories) {
	allProjectSRCDirectories << getSRCDirectory(projectDirectory)
}

// find Messages.java files
def allJavaFiles = []
for (File projectSRCDirectory : allProjectSRCDirectories) {
	allJavaFiles.addAll(LS.list(projectSRCDirectory, true, false))
}
allJavaFiles = allJavaFiles.findAll { it -> return it.getName().endsWith("Messages.java") }

// for each Java file find its properties files
for (File javaFile : allJavaFiles) {
	println javaFile
	def otherFiles = LS.list(javaFile.getParentFile(), false, false)
	otherFiles = otherFiles.findAll { it -> return it.getName().endsWith(".properties") && it.getName().startsWith("messages") }
	println otherFiles
	javaFileToPropertieFiles[javaFile] = otherFiles
	for (File o : otherFiles) {
		propertieFileToJavaFile[o] = javaFile
	}
}

// for each properties file, load its properties
def propertiesToMessageFile = [:]
def messagesFileToProperties = [:]

def messageToProperties = [:]
def messageKeyToProperties = [:]

for (def file : propertieFileToJavaFile.keySet()) {
	Properties newProperties = new Properties();
	newProperties.load(IOUtils.getReader(file, "UTF-8"));
	
	propertiesToMessageFile[file] = newProperties;
	messagesFileToProperties[newProperties] = file;
	
	for (String kmessage : newProperties.keySet()) {
		def message = newProperties[kmessage]
		if (!messageToProperties.containsKey(message)) messageToProperties[message] = []
		messageToProperties[message] << newProperties
		if (!messageKeyToProperties.containsKey(message)) messageKeyToProperties[message] = []
		messageKeyToProperties << newProperties
	}
}


//////////////////
for (def lang : langs) {
	def pattenr = /messages$lang\.properties/

	def allPropertiesFiles = []
	for (File projectSRCDirectory : allProjectSRCDirectories) {
		allPropertiesFiles.addAll(LS.list(projectSRCDirectory, true, false))
	}
	def propertiesFiles = []
	for (def file : allPropertiesFiles) {
		if (file.getName() ==~ pattenr) {
			propertiesFiles << file
		}
	}
	println "\n*** '$lang' properties files in $workspace***\n"

	def messages = [:]
	for (def file : propertiesFiles) {
		Properties newProperties = new Properties();
		newProperties.load(IOUtils.getReader(file, "UTF-8"));
		messages[file] = newProperties;
	}

	def allKeys = new HashSet();
	for (def file : propertiesFiles) {
		for (def keys : messages[file].keySet()) {
			allKeys.addAll(keys)
		}
	}
	allKeys = allKeys.sort()

	for (def key : allKeys) {
		def values = []
		for (def file : propertiesFiles) {
			def v = messages[file][key];
			if (v != null) {
				values << file.getAbsolutePath()
			}
		}
		if (values.size() > 1) {
			println "'$key': "+values.join("\t")
		}
	}
}