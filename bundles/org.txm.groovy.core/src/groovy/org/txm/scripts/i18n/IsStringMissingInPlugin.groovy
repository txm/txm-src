package org.txm.scripts.scripts.i18n

/**
 * Display the missing i18n keys of the plugin.xml file 
 * 
 * @author mdecorde
 *
 */
class IsStringMissingInPlugin {
	
	public static void main(String[] args) {
		String userhome = System.getProperty("user.home")
		File propertyFile = new File(userhome, "workspace047/org.txm.rcp/OSGI-INF/l10n/bundle_fr.properties");
		File copyFile = new File(userhome, "workspace047/org.txm.rcp/OSGI-INF/l10n/bundle_copy.properties");
		File pluginFile = new File(userhome, "workspace047/org.txm.rcp/plugin.xml");
		if (!(propertyFile.exists() && propertyFile.canRead() && propertyFile.canWrite() && propertyFile.isFile()))
		{
			println "error file : "+propertyFile
			return
		}

		println "Get properties of "+propertyFile
		Properties props = new Properties();
		propertyFile.withReader("iso-8859-1") { input->
			props.load(input);
			input.close()
		}

		
		String pluginXMLcontent = pluginFile.getText();
		def pluginKeys = [];
		def matcher = pluginXMLcontent =~ /"%([.A-Za-z0-9]+)+"/ // %key.of.the.field
		println "Comparing keys "+matcher.pattern() +" of "+pluginFile+" with those declared"
		matcher.each { // iterates of %key in the plugin.xml content
			def key = it[1]
			pluginKeys << key
			if (!props.containsKey(key)) {
				println "Prop file does not contains the key: $key"
				//println " adding default value."
				props.setProperty(key, "NA")
			}
		} 
		
		for (String key : props.keySet()) {
			if (!pluginKeys.contains(key)) {
				println "plugin.xml does not use the key: "+key
			}
		}
		
		propertyFile.withWriter("iso-8859-1") { output ->
			props.store(output, "")
		}
	}
}
