package org.txm.scripts.scripts.i18n

import org.txm.utils.BiHashMap

/**
 * Bihashmap 
 * Check any missing key in both of messages.properties and Message.java files
 * @author mdecorde
 *
 */
class ReverseI18nDict extends BiHashMap<String, String> {
	File propFile;
	File messageFile;
	
	def messageKeys = new HashSet<String>()
	
	public ReverseI18nDict(File propFile) {
		this(propFile, null)
	}
	
	public ReverseI18nDict(File propFile, File messageFile) {
		this.propFile = propFile
		this.messageFile = messageFile
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(propFile), "iso-8859-1"));
		String line = reader.readLine();
		while (line != null) {
			if (!line.startsWith("#")) {
				String[] split = line.split("=", 2);
				if (split.length == 2) {
					this.put(split[0], split[1]);
				}
			}
			line = reader.readLine();
		}
		
		if (messageFile != null) {
			BufferedReader reader2 = new BufferedReader(new InputStreamReader(new FileInputStream(messageFile), "iso-8859-1"));
			String line2 = reader2.readLine();
			while (line2 != null) {
				line2 = line2.trim();
				if (line2.startsWith("public static String")) {
					line2 = line2.substring(21, line2.length() -1);
					messageKeys << line2
				}
				line2 = reader2.readLine();
			}
		}
	}
	
	public def getMissingsMessageKeys() {
		def missing = []
		for (String pKey : this.getKeys())
			if (!messageKeys.contains(pKey))
				missing << pKey
				
		return missing
	}
	
	public getMissingPropKeys() {
		def missing = []
		def pKeys = this.getKeys()
		for (String mKey : messageKeys)
			if (!pKeys.contains(mKey))
				missing << mKey
				
		return missing
	}
	
	public void saveChanges() {

		//Write prop File
		File oldFile = new File(propFile.getParentFile(), propFile.getName()+".cpy");
		propFile.renameTo(oldFile);
		propFile.withWriter("iso-8859-1") { out ->
			for(String key : this.getKeys().sort()) {
				out.println(key+"="+this.get(key))
			}
		}
		
		// write message File
		if (messageFile == null) return;
		
		File oldFile2 = new File(messageFile.getParentFile(), messageFile.getName()+".cpy");
		messageFile.renameTo(oldFile2); // back up
		messageFile.withWriter("iso-8859-1") { out ->
			// write start
			out.println('''package org.txm;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	
	// The Constant BUNDLE_NAME. 
	private static final String BUNDLE_NAME = "org.txm.messages"; //$NON-NLS-1$
				''')
			
			//write keys
			for(String key : this.getKeys().sort())
				out.println("\tpublic static String $key;");
			
			// write end
			out.println('''
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
				''')
		}
	}
	
	public static void main(String[] args) {
		File propFile = new File("/home/mdecorde/workspace047/org.txm.core/src/java/org/txm/messages.properties")
		File messageFile = new File("/home/mdecorde/workspace047/org.txm.core/src/java/org/txm/Messages.java")
		ReverseI18nDict dict = new ReverseI18nDict(propFile, messageFile);
		println dict.getMissingsMessageKeys()
		println dict.getMissingPropKeys();
		println dict.toString();
		//dict.saveChanges();
	}
}
