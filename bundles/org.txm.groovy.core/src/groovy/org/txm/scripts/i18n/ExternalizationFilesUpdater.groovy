// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-07-25 16:24:13 +0200 (jeu. 25 juil. 2013) $
// $LastChangedRevision: 2490 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts.i18n;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

// TODO: Auto-generated Javadoc
/**
 * Tool to update the key of externalization files xxxx_fr.properties
 * adds _NA to missing keys
 * 
 * @author mdecorde
 * 
 */
public class ExternalizationFilesUpdater {

	/** The dirfiles. */
	LinkedList<File> dirfiles = new LinkedList<File>();
	
	/** The propertyfiles. */
	HashMap<String, File> propertyfiles = new HashMap<String, File>();
	
	/** The fileentries. */
	HashMap<File, List<String>> fileentries = new HashMap<String, List<String>>();
	
	/** The fileentriesvalues. */
	HashMap<File, HashMap<String, String>> fileentriesvalues = new HashMap<String, HashMap<String, String>>();

	/**
	 * Gets the entries.
	 *
	 * @param file the file
	 * @return the entries
	 */
	public List<String> getEntries(File file)
	{
		List<String> entries = [];
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file) , "iso-8859-1"));
		String line = reader.readLine();
		while (line != null) {
			String[] split = line.split("=", 2);
			if (split.length > 0) {
				String key = split[0];
				entries.add(key);
			}
			line = reader.readLine();
		}
		return entries;
	}

	/**
	 * Gets the values.
	 *
	 * @param file the file
	 * @return the values
	 */
	public HashMap<String, String> getValues(File file) {
		HashMap<String, String> values = new HashMap<String, String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(file), "iso-8859-1"));
		String line = reader.readLine();
		while (line != null) {
			String[] split = line.split("=");
			if (split.length > 0) {
				String key = split[0];
				String concat = "";
				for (int i = 1; i < split.length; i++)
					concat += split[i];
				values.put(key, concat);
			}
			line = reader.readLine();
		}
		return values;
	}

	/**
	 * Process found directories.
	 */
	public void processFoundDirectories()
	{
		for (String dirfile : dirfiles) {
			println "DIRECTORY : "+dirfile
			List<File> files = propertyfiles.get(dirfile)
			for (File f : files) {
				this.fileentries.put(f, getEntries(f));
				this.fileentriesvalues.put(f, getValues(f));
			}
			File reference = null;
			if (files.get(0).getName().startsWith("m")) {
				reference = new File(dirfile,"messages.properties");
			}
			else if (files.get(0).getName().startsWith("b"))
				reference = new File(dirfile,"bundle.properties");
			
			if (reference != null && reference.exists()) {
				for (File f : files) {
					if (f != reference) {
						List<String> refentries = this.fileentries.get(reference).clone();
						List<String> tmp1 = this.fileentries.get(f).clone();
						tmp1.removeAll(refentries);
						
						refentries = this.fileentries.get(reference).clone();
						List<String> tmp = this.fileentries.get(f);
						refentries.removeAll(tmp);
						for (String missing : refentries) {
							this.fileentriesvalues.get(f).put(missing,"N/A_"+this.fileentriesvalues.get(reference).get(missing)); // put entry's value
						}
						
						this.fileentries.put(f, this.fileentries.get(reference)); // update file entries
						
						if (tmp1.size() > 0 || refentries.size() > 0)
							println " "+f.getName()
						if (tmp1.size() > 0)
							println "  Removed keys : "+tmp1;
						if (refentries.size() > 0)
							println "  Added keys : "+refentries;
					}
				}
			}
		}
	}

	/**
	 * Update files.
	 */
	public void updateFiles()
	{
		for(String dirfile : dirfiles)
		{
			List<File> files = propertyfiles.get(dirfile);
			for(File f : files)
			{
				List<String> entries =  this.fileentries.get(f);
				HashMap<String, String> values = this.fileentriesvalues.get(f);
				
				Writer writer = new OutputStreamWriter(new FileOutputStream(f) , "iso-8859-1");
				for(String entry : entries)
				{
					writer.write(entry+"="+values.get(entry)+"\n");
				}
				writer.close()
			}
		}
	}

	/**
	 * Creates the missing files.
	 *
	 * @param suffix the suffix
	 */
	public void createMissingFiles(String suffix)
	{
		println "Looking for missing messages files "+ suffix;
		for(String dirfile : dirfiles)
		{
			//println "DIRECTORY : "+dirfile
			File reference = null;
			String lookingname = "";
			List<File> files = propertyfiles.get(dirfile)
			if(files.get(0).getName().startsWith("m"))
			{
				reference = new File(dirfile,"messages.properties");
				lookingname = "messages_"+suffix+".properties"
			}
			else if (files.get(0).getName().startsWith("b"))
			{
				reference = new File(dirfile,"bundle.properties");
				lookingname = "bundle_"+suffix+".properties"
			}
			
			boolean create = true;
			if(reference != null && reference.exists())
			{
				for(File f : files)
				{
					if(f.getName() == lookingname)
						create = false;
				}
			}
			if(create)
			{
				new File(dirfile,lookingname).createNewFile();
				println "Create file " +new File(dirfile,lookingname)
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String rez = "";
		for(String dirfile : dirfiles)
		{
			rez += dirfile+"\n";
			for(File f : propertyfiles.get(dirfile))
				rez += "  "+f.getName()+"\n"
		}
		return rez;
	}

	/**
	 * Scan directory.
	 *
	 * @param directory the directory
	 */
	public void scanDirectory(File directory)
	{
		if(!directory.exists())
		{
			println "directory '$directory' does not exists"
			return;
		}
		
		println "scan directory : "+directory.getAbsolutePath();
		LinkedList<File> files = new LinkedList<File>();
		files.add(directory);
		
		while(!files.isEmpty())
		{
			File current = files.removeFirst();
			if(current.isDirectory())
			{
				List<String> currentpfiles = [];
				for(File sfile : current.listFiles())
				{
					if(sfile.isDirectory())
						files.add(sfile);
					else if(sfile.getName().endsWith(".properties") && ( sfile.getName().startsWith("messages") || sfile.getName().startsWith("bundle")) )
						currentpfiles.add(sfile)
				}
				if(currentpfiles.size() > 0)
				{
					dirfiles.add(current.getAbsolutePath());
					propertyfiles.put(current.getAbsolutePath(), currentpfiles);
				}
			}
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String userdir = System.getProperty("user.home");

		println "\nRCP\n";
		
		ExternalizationFilesUpdater scanner = new ExternalizationFilesUpdater();
//		scanner.scanDirectory(new File(userdir, "workspace37/org.txm.rcp")); // find directories with a messages.properties file
//		scanner.createMissingFiles("fr"); // create messages_fr.properties files when a messages.properties is found
//		scanner.processFoundDirectories(); // find missing and obsolets keys
//		scanner.updateFiles(); // update messages files content
		
		println "\nTOOLBOX\n";
		
		scanner = new ExternalizationFilesUpdater();
		scanner.scanDirectory(new File(userdir, "workspace047/org.txm.rcp/src/main/java"));
		scanner.createMissingFiles("fr");
		scanner.createMissingFiles("ru");
		scanner.processFoundDirectories();
		scanner.updateFiles();
	}
}
