package org.txm.scripts.scripts.i18n

File propFile1 = new File("/home/mdecorde/workspace047/org.eclipse.equinox.p2.ui.nl_fr/org/eclipse/equinox/internal/p2/ui/messages_fr.properties")
File propFile2 = new File("/home/mdecorde/workspace047/org.txm.rcp.p2.ui/src/org/eclipse/equinox/internal/p2/ui/messages_fr.properties")

Properties props1 = new Properties()
props1.load(propFile1.newReader("iso-8859-1"))
Properties props2 = new Properties()
props2.load(propFile2.newReader("iso-8859-1"))

println "props1.size = "+props1.size()
println "props2.size = "+props2.size()

for( def key : props1.keySet()) {
	println "update: "+key
	props2.put(key, props1.get(key))
}

println "props2.size AFTER = "+props2.size()
props2.store(propFile2.newWriter("iso-8859-1"), "$propFile1 + $propFile2")