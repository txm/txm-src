// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.filters.Tokeniser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.txm.scripts.importer.graal.PersonalNamespaceContext;
import org.txm.objects.Project;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.xml.stream.*;

import java.net.URL;

import org.txm.tokenizer.TokenizerClasses;

public class OldSimpleTokenizerXml {
	
	/** The word_tags. */
	String word_tags;
	
	/** The intraword_tags. */
	String intraword_tags;
	
	/** The punct_strong. */
	def punct_strong;
	
	/** The punct_all. */
	def punct_all;
	
	/** The word_chars. */
	def word_chars;
	
	def fclitics = null; // default behavior don't manage clitics
	def pclitics = null; // default behavior don't manage clitics
	
	/** The ignorable_tags. */
	String ignorable_tags
	String outside_text_tags_keep_content
	String outside_text_tags
	String startTag = null
	
	/** The DEBUG. */
	public boolean DEBUG = false;
	
	/** The outfile. */
	File outfile;
	
	/** The infile. */
	File infile;
	
	String lang;
	
	/** The buffer. */
	StringBuffer buffer;
	
	/** The writer. */
	XMLStreamWriter writer;
	BufferedOutputStream output;
	
	/** The parser. */
	XMLStreamReader parser
	
	/** The localname. */
	String localname;
	
	/** The prefix. */
	String prefix;
	String filename;
	
	def regElision;
	def reg3pts;
	def regPunct;
	def regFClitics = null ;
	def regPClitics = null ;
	String whitespaces;
	
	public OldSimpleTokenizerXml(File infile, File outfile)
	{
		this(infile, outfile, "");
	}
	
	/**
	 * Instantiates a new simple tokenizer xml.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 */
	public OldSimpleTokenizerXml(File infile, File outfile, String lang)
	{
		this.lang = lang;
		TokenizerClasses tc = new TokenizerClasses(lang)
		if (lang != null)
			if (lang.startsWith("en")) {
				fclitics = tc.FClitic_en
			} else if (lang.startsWith("fr")) {
				fclitics = tc.FClitic_fr
				pclitics = tc.PClitic_fr
			} else if (lang.startsWith("gl")) {
				fclitics = tc.FClitic_gl
			} else if (lang.startsWith("it")) {
				pclitics = tc.PClitic_it
			}
		
		/** The word_tags. */
		word_tags = tc.word_tags;
		
		/** The intraword_tags. */
		intraword_tags = tc.intraword_tags;
		
		/** The punct_strong. */
		punct_strong = tc.punct_strong;
		
		/** The punct_all. */
		punct_all = tc.punct_all;
		
		/** The word_chars. */
		word_chars = tc.word_chars;
		
		this.outfile = outfile;
		this.infile = infile;
		this.filename = infile.getName();
		int index = filename.lastIndexOf(".");
		if (index > 0) filename = filename.substring(0, index);
		
		String strRegElision = TokenizerClasses.regElision;
		String strRegPunct = TokenizerClasses.regPunct;
		regElision = /\A(.*?)(\p{L}++$strRegElision)(.*)\Z/
		reg3pts = /\A(.*)(\.\.\.)(.*)\Z/
		regPunct = /\A(.*)($strRegPunct)(.*)\Z/
		if (fclitics != null)
			regFClitics = /(.+)($fclitics)(.*)/
		if (pclitics != null)
			regPClitics = /(.+)(pclitics)(.*)/
		whitespaces = TokenizerClasses.whitespaces;
	}
	
	/**
	 * Fill infos.
	 *
	 * @param event the event
	 * @return the java.lang. object
	 */
	public fillInfos(int event)
	{
		if (event == XMLStreamConstants.START_ELEMENT || event == XMLStreamConstants.END_ELEMENT)
		{
			localname = parser.getLocalName();
			prefix = parser.getPrefix();
		}
	}
	
	/**
	 * Donothing.
	 *
	 * @param event the event
	 * @param wordid the wordid
	 * @return the java.lang. object
	 */
	public donothing(int event, Integer wordid)
	{
		if (event == XMLStreamConstants.START_ELEMENT ) {
			String namespace = parser.getNamespaceURI();
			
			localname = parser.getLocalName();
			if (prefix != null && prefix.length() > 0)
				writer.writeStartElement(prefix+":"+localname);
			else
				//				if(namespace != null)
				//					writer.writeStartElement(namespace, localname);
				//				else
				writer.writeStartElement(localname);
			
			//			if(parser.getNamespaceCount() > 0)
			//				writer.writeDefaultNamespace(parser.getNamespaceURI(0))
			//			for(int i = 1 ; i < parser.getNamespaceCount() ; i++)
			//				writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
			
			String namespace_prefix;
			for (int i = 0 ; i<   parser.getNamespaceCount() ; i++) {
				namespace_prefix = parser.getNamespacePrefix(i);
				if ((namespace_prefix != null)&&   (namespace_prefix.length()>   0)) {
					writer.writeNamespace(namespace_prefix, parser.getNamespaceURI(i));
				} else {
					writer.writeDefaultNamespace(parser.getNamespaceURI(i));
				}
			}
			
			String attrprefix, attname;
			boolean hasId = false;
			boolean hasType = false
			boolean hasN = false
			for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
				attname = parser.getAttributeLocalName(i);
				attrprefix = parser.getAttributePrefix(i);
				if ("id".equals(attname)) hasId = true;
				if ("type".equals(attname)) hasType = true;
				if ("n".equals(attname)) hasN = true;
				
				if(attrprefix != null && attrprefix.length() > 0)
					writer.writeAttribute(attrprefix+":"+attname, parser.getAttributeValue(i))
				else
					writer.writeAttribute(attname, parser.getAttributeValue(i))
			}
			
			if (wordid != null && !hasId && localname == "w")
				writer.writeAttribute("id", "w_"+filename+"_"+wordcount);
			
			if (!hasType && localname == "w")
				writer.writeAttribute("type", "w");
			
			if (!hasN && localname == "w")
				writer.writeAttribute("n", ""+wordcount);
			
			writer.writeCharacters("\n");
		}
		else if(event == XMLStreamConstants.END_ELEMENT)
		{
			writer.writeEndElement();
			writer.writeCharacters("\n");
		}
		else if(event == XMLStreamConstants.CHARACTERS)
		{
			if (insideword) { // ensure there is not \t or \n in the word form value
				writer.writeCharacters(parser.getText().trim().replace("\n", " ").replace("\t", " "));
			} else {
				writer.writeCharacters(parser.getText());
			}
		}
	}
	
	/** The wordcount. */
	int wordcount = 0;
	
	/** The ignorecontent. */
	boolean ignorecontent = true;//tokenize a partir de <body>
	boolean insideword = false;
	/**
	 * Process.
	 *
	 * @return true, if successful
	 */
	public boolean process()
	{
		if (!infile.exists()) {
			println "$infile does not exists"
			return false;
		}
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		output = new BufferedOutputStream(new FileOutputStream(outfile))
		writer = factory.createXMLStreamWriter(output, "UTF-8")
		writer.setNamespaceContext(new PersonalNamespaceContext());
		
		def inputData = infile.toURI().toURL().openStream();
		def inputfactory = XMLInputFactory.newInstance();
		//inputfactory.setProperty("http://apache.org/xml/properties/input-buffer-size", new Integer(2048));
		//inputfactory.setExpandEntityReferences(false);
		parser = inputfactory.createXMLStreamReader(inputData, "UTF-8");
		//println "PARSER: "+parser.getClass()
		writer.writeStartDocument("UTF-8","1.0");
		writer.writeCharacters("\n");
		
		int previousEvent = 0;
		boolean startProcess = false;
		if (startTag == null) // if no startTag specified we process from the start
			startProcess = true;
		buffer = new StringBuffer();
		//println "process - start start tag: "+startTag+" startProcess: $startProcess"
		ignorecontent = !startProcess;
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				if (!startProcess) {
					if (event == XMLStreamConstants.START_ELEMENT) {
						if (parser.getLocalName().matches(startTag)) {
							
							startProcess = true
							ignorecontent = false;
						}
					}
					if (!startProcess) {
						donothing(event, null);
						continue;
					}
				}
				
				if (previousEvent == XMLStreamConstants.CHARACTERS && previousEvent != event) {
					processWord(); // tokenize
					buffer = new StringBuffer();
				}
				fillInfos(event);//get localname and prefix
				if (event == XMLStreamConstants.START_ELEMENT) {
					//println "Open: "+localname;
					
					if (localname.matches(word_tags)) // ignore the content of the word but keep counting
					{
						//println "Found pretagged word";
						wordcount++;
						donothing(event, wordcount);
						ignorecontent = true;
						insideword = true;
					} else if (outside_text_tags_keep_content != null && localname.matches(outside_text_tags_keep_content)) { // ignore the content of the tag ONLY
						//just ignore the tag
					} else if (ignorable_tags != null && localname.matches(ignorable_tags)) { // ignore the content of the tag ONLY
						donothing(event, null);
						ignorecontent = true;
					} else if (outside_text_tags != null && localname.matches(outside_text_tags)) { // ignore the tag and its content of the tag
						goToEndOfElement(localname);
					} else {
						donothing(event, null);
					}
				} else if(event == XMLStreamConstants.END_ELEMENT) {
					//println "Close: "+localname;
					if (localname.matches(word_tags)) {
						ignorecontent = false;
						insideword = false;
						writer.writeEndElement();
						writer.writeCharacters("\n");
					} else if (ignorable_tags != null && localname.matches(ignorable_tags)) { // ignore the content of the tag
						ignorecontent = false;
						donothing(event, null);
					} else if (outside_text_tags_keep_content != null && localname.matches(outside_text_tags_keep_content)) { // ignore the content of the tag ONLY
						//just ignore the tag
					} else {
						donothing(event, null);
					}
				} else if (event == XMLStreamConstants.CHARACTERS) {
					if (ignorecontent) {
						//println " ignore chars: "+parser.getText().trim();
						donothing(event, null);
					} else {
						//println " process chars: "+parser.getText().trim();
						buffer.append(parser.getText());
						if (buffer.length() >= 128 && buffer.charAt(buffer.length()-1) == " ") {
							processWord();
							buffer = new StringBuffer();
						}
					}
				} else if (event == XMLStreamConstants.COMMENT) {
					writer.writeComment(parser.getText())
				} else if (event == XMLStreamConstants.DTD) {
					//println "DTD!";
				} else {
					if (DEBUG) println "Warning in $infile: ignore XML event at location "+parser.getLocation()
				}
				previousEvent = event;
			}
			
			parser.close()
			writer.close();
			output.close();
			inputData.close();
		} catch (Exception e) {
			System.err.println("Error : "+infile);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void setOutSideElements(String regexp) {
		this.outside_text_tags = regexp;
	}
	
	public void setOutSideTextTags(String regexp) {
		this.outside_text_tags_keep_content = regexp;
	}
	
	protected void goToEndOfElement(String name) {
		//println "START ignoring tag and content of $name"
		def openedTags = []
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) {
				openedTags << parser.getLocalName()
				//println "append "+openedTags
			} else if (event == XMLStreamConstants.END_ELEMENT) {
				if (openedTags.size() == 0 && name == parser.getLocalName()) {
					//println "END ignoring tag and content of $name"
					return;
				}
				openedTags.pop()
				//println "pop $openedTags"
			}
		}
	}
	
	/**
	 * Process word.
	 */
	protected void processWord()
	{
		String text = buffer.toString();//parser.getText().trim().replace("\t", " ");
		if (DEBUG) println "-- chars: "+text+"--";
		text = text.replaceAll("\n", " ");
		text = text.replaceAll("\\p{C}", "");						// remove ctrl characters
		for (String s : text.split(whitespaces) )			// separate with unicode white spaces
		{
			if (DEBUG){println "process $s"}
			iterate(s);
		}
	}
	
	/**
	 * Iterate. a String, should be called when a word is found in a String
	 *
	 * @param s the s
	 * @return the java.lang. object
	 */
	protected iterate(String s)
	{
		while (s != null && s.length() > 0) {
			if (DEBUG){println "  > $s"}
			s = standardChecks(s);
		}
	}
	
	
	/**
	 * Standard checks.
	 *
	 * @param s the s
	 * @return the java.lang. object
	 */
	protected standardChecks(String s)
	{
		def m;
		if (fclitics != null && (m = s =~ regFClitics) ) {
			if (DEBUG) println "CLITIC found: $s ->"+ m[0][1]+" + "+m[0][2]+" + "+m[0][3]
			if (m[0][1] != null && m[0][1].length() > 0) iterate(m[0][1]);// process first part of the string
			
			wordcount++;
			writer.writeStartElement("w");
			writeWordAttributes();// id
			writer.writeAttribute("type", "w");
			writer.writeCharacters(m[0][2]);
			writer.writeEndElement();
			writer.writeCharacters("\n");
			
			return m[0][3];
		} else if ((m = s =~ regElision)) {
			if (DEBUG) println "Elision found: $s ->"+ m[0][1]+" + "+m[0][2]+" + "+m[0][3]
			iterate(m[0][1])
			
			int sep = s.indexOf("'");
			if (sep < 0)
				sep = s.indexOf("’");
			if (sep < 0)
				sep = s.indexOf("‘");
			
			wordcount++;
			writer.writeStartElement("w");
			writeWordAttributes();// id
			writer.writeAttribute("type", "w");
			writer.writeCharacters(m[0][2]);
			writer.writeEndElement();
			writer.writeCharacters("\n");
			
			return m[0][3];
		}
		else if ((m = s =~ reg3pts) )
		{
			if(DEBUG){println "REG '...' found: $s -> "+m[0][1]+" + "+m[0][2]+" + "+m[0][3]}
			iterate(m[0][1])
			
			wordcount++;
			writer.writeStartElement("w");
			writeWordAttributes();// id
			writer.writeAttribute("type","pon");
			writer.writeCharacters("...");
			writer.writeEndElement();
			writer.writeCharacters("\n");
			
			return m[0][3];
		}
		else if ((m = s =~ regPunct) )
			//else if((m = s =~ /\A(.*)($punct_all)(.*)\Z/) )
		{
			if(DEBUG){println "PUNCT found: $s ->"+m[0][1]+" + "+m[0][2]+" + "+m[0][3]}
			iterate(m[0][1]);
			
			wordcount++;
			writer.writeStartElement("w");
			writeWordAttributes();// id
			writer.writeAttribute("type","pon");
			writer.writeCharacters(m[0][2]);
			writer.writeEndElement();
			writer.writeCharacters("\n");
			
			return m[0][3];
		}
		else
		{
			if(DEBUG){println "Other found: "+s}
			wordcount++;
			writer.writeStartElement("w");
			writeWordAttributes();// id
			if (s.matches(/\p{P}/))
				writer.writeAttribute("type","pon");
			else
				writer.writeAttribute("type","w");
			writer.writeCharacters(s);
			writer.writeEndElement();
			writer.writeCharacters("\n");
			
			return "";
		}
	}
	
	/**
	 * Write word attributes.
	 *
	 * @return the java.lang. object
	 */
	protected writeWordAttributes()
	{
		writer.writeAttribute("id", "w_"+filename+"_"+wordcount);
		writer.writeAttribute("n",""+wordcount);
	}
	
	public void setStartTag(String tag)
	{
		this.startTag = tag;
	}
	
	/**
	 * Tokenize.
	 *
	 * @param str the str
	 * @return the list
	 */
	public List<String> tokenize(String str)
	{
		return str.tokenize()	// cut by whitespace
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File inputDir = new File("/home/mdecorde/xml/annotation/")
		File inputFile = new File(inputDir, "test.xml");
		for (String lang : ["fr", null]) {
			print "."
			File outputFile = new File(inputDir, "test-${lang}.xml")
			
			OldSimpleTokenizerXml tokenizer = new OldSimpleTokenizerXml(inputFile, outputFile, lang)
			tokenizer.DEBUG = true
			tokenizer.process();
		}
		
		println ""
		println "Done"
		
		//		String lang = "en"
		//		File inDir = new File("/home/mdecorde/xml/ny911/cycle1/Data_Modif_Pour_Import_XML_Cycle1/xml_files/1xml_par_participant")
		//		File outDir = new File("/home/mdecorde/xml/ny911/cycle1/Data_Modif_Pour_Import_XML_Cycle1/xml_files/1xml_par_participant-t")
		//		outDir.deleteDir()
		//		outDir.mkdir()
		//
		//		println "processing "+inDir.listFiles().size()+" files."
		//		for (def infile : inDir.listFiles()) {
		//			if (!infile.getName().endsWith(".xml")) continue;
		//
		//			print "."
		//			File outfile = new File(outDir, infile.getName())
		//			OldSimpleTokenizerXml tokenizer = new OldSimpleTokenizerXml(infile, outfile, lang)
		//			tokenizer.setDEBUG false
		//			tokenizer.process();
		//		}
		//		println ""
		//		println "Done"
		
	}
}
