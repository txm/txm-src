// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.filters.Tokeniser;

import java.text.DateFormat;
import java.util.Date;
import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;
import org.txm.importer.scripts.filters.*;

// TODO: Auto-generated Javadoc
/**
 * Used by the tokenizer. put one tag per line
 * 
 * @author mdecorde
 */

public class OneTagPerLine {

	/** The url. */
	private def url;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The output. */
	private def output;

	/** The solotags. */
	ArrayList<String> solotags;

	/**
	 * Instantiates a new one tag per line.
	 *
	 * @param url the url
	 * @param solotags the solotags
	 */
	public OneTagPerLine(URL url, ArrayList<String> solotags){
		try {
			this.url = url;
			this.solotags = solotags;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			factory.setProperty(XMLInputFactory.IS_VALIDATING,false)
			parser = factory.createXMLStreamReader(inputData);

		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			File f = outfile;
			output = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Read next.
	 *
	 * @param n the n
	 * @return the java.lang. object
	 */
	public def readNext(int n)
	{
		try
		{
			return parser.next()
		}catch(Exception e){
			if(n > 10)
				return parser.next();
			else
				readNext(n+1)
		}
	}
	boolean wordtag = false;
	/**
	 * Process.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	public boolean process(File outfile) {

		StringBuffer chars = new StringBuffer();
		if (createOutput(outfile)) {

			String lastopenlocalname= "";
			String localname = "";
			boolean word_part = false;
			output.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
			try {
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = readNext(0)) {

					String prefix = parser.getPrefix();
					if (prefix == null || prefix == "")
						prefix = "";
					else
						prefix +=":";

					switch (event) {
						case XMLStreamConstants.START_ELEMENT:
							if (chars.length() > 0) {
								output.write(chars.substring(0, chars.length()-1));
								chars = new StringBuffer();
							}
							localname = parser.getLocalName();
							lastopenlocalname = localname;
							word_part = false;
							for (int i= 0 ; i < parser.getAttributeCount() ;i++ )
								if (parser.getAttributeValue(i).contains("word_part"))
									word_part = true;

						/*if(word_part == true)
						 output.write("<"+prefix+localname);
						 else*/
						//						if(word_part)
						//							output.write("<"+prefix+localname);
						//						else
							if (wordtag)
								output.write("<"+prefix+localname);
							else
								output.write("\n<"+prefix+localname);
							for (int i= 0 ; i < parser.getAttributeCount() ;i++ ) {
								String ns = parser.getAttributePrefix(i);
								if (ns == null || ns == "")
									output.write(" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i).replace("&","&amp; ").replace("<","&lt;")+"\"" );
								else
									output.write(" "+ns+":"+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i).replace("&","&amp; ").replace("<","&lt;")+"\"" );

							}

							if (solotags.contains(localname))
								output.write("/>");
							else
								output.write(">");

						//						if(!word_part)
							if (localname == "w") {
								wordtag=true;
							} else if(!wordtag) {
								output.write("\n");
							}
							break;

						case XMLStreamConstants.END_ELEMENT:
							if (chars.length() > 0) {
								output.write(chars.substring(0, chars.length()-1));
								chars = new StringBuffer();
							}

							localname = parser.getLocalName();
							switch (localname) {
								default:
									if(!solotags.contains(localname))
									/*if(word_part) // see filter ReunitBrokenWords
							 output.write("</"+prefix+localname+">");
							 else*/
									if (localname == "w") {
										wordtag = false
										output.write("</"+prefix+localname+">\n");
									} else if (wordtag) {
										output.write("</"+prefix+localname+">");
									} else {
										output.write("\n</"+prefix+localname+">\n");
									}
							}
							break;

						case XMLStreamConstants.CHARACTERS:
							if (parser.getText().length() > 0) {
								int start=0;
								while (parser.getText().charAt(start) == "\n" || parser.getText().charAt(start) == " ")
									if (start < parser.getText().length()-1) {
										start++;
									} else {
										start++;
										break;
									}
								//replace & by &amp; ; < by &lt; ; insecable space by space
								chars.append(parser.getText().substring(start).replace("\t", " ").replace("&","&amp;").replace("<","&lt;").replace(" "," ").trim()+ " ");
							}
							break;
					}
				}
			if (output != null) output.close();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();

				Reader input = new InputStreamReader(new FileInputStream(outfile) , "UTF-8");
				File temp = new File(outfile.getParent(),"temp");
				output = new OutputStreamWriter(new FileOutputStream(temp) , "UTF-8");
				String line = input.readLine();
				while (line != null) {
					if (line.length() > 0)
						output.write(line+"\n");
					line = input.readLine();
				}
				input.close();
				output.close();

				if (!(outfile.delete() && temp.renameTo(outfile))) println "Warning can't rename file "+temp+" to "+outfile
			} catch (XMLStreamException ex) {
				input.close();
				output.close();
				System.out.println(ex);
				return false;
			} catch (IOException ex) {
				input.close();
				output.close();
				System.out.println("IOException while parsing " + inputData);
				return false;
			}
		}
		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/Hyperprince/";
		new File(rootDir + "/identity/").mkdir();

		ArrayList<String> milestones = new ArrayList<String>();// the tags who
		// you want them
		// to stay
		// milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");

		File srcfile = new File(rootDir, "Corpus-Hyperprince_2009-06-10.xml");
		File resultfile = new File(rootDir,
				"Corpus-Hyperprince_2009-06-10-T.xml");
		println("prepare tokenizer file : " + srcfile + " to : " + resultfile);

		def builder = new OneTagPerLine(srcfile.toURL(), milestones);
		builder.process(resultfile);

		return;
	}

}
