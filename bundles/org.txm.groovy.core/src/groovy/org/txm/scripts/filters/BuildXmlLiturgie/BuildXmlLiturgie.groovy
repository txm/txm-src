// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.BuildXmlLiturgie

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import groovy.xml.XmlSlurper
import org.txm.importer.scripts.filters.*;
// TODO: Auto-generated Javadoc

/**
 * The Class BuildXmlLiturgie.
 *
 * @author ayepdieu
 */

public class BuildXmlLiturgie extends Filter
{
	/*
	 * ville : msDescription>msIdentifier>settlement
	 * cote : msDescription>msIdentifier>idno
	 * institution : msDescription>msIdentifier>institution
	 * usage : msDescription>msItem>respStmt>name
	 * post : msDescription>history>origin>origDate , notBefore
	 * ante : msDescription>history>origin>origDate , notAfter
	 */
	
	/** The dir path name. */
	String dirPathName;;
	
	/** The file name. */
	String fileName;
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	public void SetUsedParam(Object args)
	{
		
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	public void filter()
	{
		
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	public boolean before()
	{
		this.readInput = false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	public void after() {
		println("start transformPage");

		def body = new XmlSlurper().parse(this.input);
		def nodes = body.text.body.listBibl.msDescription;
		def idNotice=1;
		def idp=1;

		def xmlBuilder = new groovy.xml.StreamingMarkupBuilder();
		xmlBuilder.encoding = this.encodage;

		def xml = xmlBuilder.bind{
			mkp.declareNamespace("":"http://www.tei-c.org/ns/1.0") 

				nodes.each {
					//msDescription
					def ville = it.msIdentifier.settlement.text();
					def institution = it.msIdentifier.institution.text();
					def cote = it.msIdentifier.idno.text();
					def usage = it.msContents.msItem.respStmt.name.text();
					def post = it.history.origin.origDate.'@notBefore';
					def ante = it.history.origin.origDate.'@notAfter';
					
					def msitems = it.msContents.msItem.msItem;
					//println msitems.size();
					notice(n:idNotice, ville:ville, institution:institution, cote:cote, usage:usage,post:post,ante:ante )
							{
								idp=1;
								msitems.each {
									def pp = (it.p);
									
									pp.each {
										it.attributes().put("n",idp) 
										mkp.yield(it)
										
										idp++
									}
								}
							}
					idNotice++;
				}
		}
		this.output.write(xml);
	}
}
