// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.ReunitBrokenTags;

//Pre-processing extra-word tags (1)
import org.txm.importer.scripts.filters.*;
import java.util.regex.*;

// TODO: Auto-generated Javadoc
/**
 * The Class ReunitBrokenTags.
 */
class ReunitBrokenTags extends Filter {
	
	/** The counter. */
	int counter;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {

	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		counter = 0;
		System.out.println("begin reunitbroken");
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after()
	{
		println "$counter Reunited tags \n";
	}

	/** The printcounter. */
	int printcounter = 0;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
		def m;
		def segment = (line =~ / /).replaceAll(" ");
		segment = (line =~ /&#160;|&nbsp;|&#xa0;/).replaceAll(" ");
		while(! (segment ==~ /\A\s*\Z/)) 
		{
			if (segment ==~ /^[^<>]*$/) {
				segment = (segment =~ /^\s+/).replaceFirst(" ");
				//if(printcounter < 15)
					//println(segment);
				printcounter++;
				output.write(segment+lineSeparator);
				segment = " ";
			}
			else if (segment ==~ /^\s*([^<]*<[^>]+)$/) {
				segment = (segment =~ /^\s+/).replaceFirst(" ");
				segment = (segment =~ /\s+$/).replaceFirst(" ");
				output.write(segment);
				segment = " ";
				counter++;
			}
			else if( (m = (segment =~ /^\s+([^<]*<[^>]+>)(.*)$/)) )	{
				segment = m[0][2];
				output.write(" "+m[0][1]);
			}
			else if( (m = (segment =~ /^([^< ]*<[^>]+>)(.*)$/)) ) {
				segment = m[0][2];
				output.write(m[0][1]);
			}
			else if( (m = (segment =~ /^\s*([^>]*)>(.*)$/)) ) {
				segment = m[0][2];
				output.write(" "+m[0][1]+">");
			}
			else {
				output.write(segment+lineSeparator);
				println "ERROR in tag restitution segment\n";
				segment = " ";
			}
		}
	}
}
