// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.filters.Tokeniser;

import org.txm.importer.scripts.filters.*;
import org.txm.tokenizer.TokenizerClasses;

// TODO: Auto-generated Javadoc
/**
 * The Class Tokeniser.
 */
class Tokeniser extends Filter {

	public Tokeniser(File text)
	{
		this.filename = text.getName();
		int index = filename.lastIndexOf(".");
		if(index > 0)
			filename = filename.substring(0, index);
			
		tc = new TokenizerClasses()
	}

	static public int DEBUG = 0;
	/** The charerrors. */
	HashSet<String> charerrors = new HashSet<String>();
	/*
	 * Elements processed by the tokenizer:
	 * 
	 * DEFS: - w|abbr|num (word_tags) - c|ex|caes|choice|corr|sic|reg
	 * (intraword_tags) - <[^>]+> (tag_all)
	 * 
	 * INPUT: OUTPUT: MODE: COMMENTS:
	 * 
	 * - &apos; ' BEGIN - <\/($word_tags)> </w>+NL word - --> -->+NL comment -
	 * <\/($head_name)> </($head_name)>+NL head - <\/note> </note>-->+NL note -
	 * <(head|p) [^>]*lang=['"]fr['"] ibid text Editor's title - <w(
	 * [^>]*)?>\s*)($tag_all\s*) ibid+NL if not </w> text - <note[^>]*>
	 * NL+<!--$note text Note found - <\/note> </note>-->+NL text - $tag_all
	 * ibid+NL text Line with tags only - <!-- NL+<!-- text XML comment found -
	 * --> -->+NL - <($word_tags)([^>]*)> <w type=\"$tag_name\"
	 * $tag_atts>$word</w>+NL text Tagged words, punctuation marks,
	 * abbreviations and numbers - $tag_all ibid+NL text Separated tags - \.[^
	 * .]+\. <w type=\"num\">$word</w>+NL text Numbers and abbreviations
	 * surrounded with dots - $punct_all|['‘’] <w type=\"pon\">$word</w>+NL text
	 * Punctuation marks - ($word_chars)*[\(\[]($word_cha <w >$word</w> text
	 * Words with brackets rs)+[\)\]]($word_chars)+|($wor
	 * d_chars)+[\(\[]($word_chars)+[ \)\]])(\s+|$punct_all) -
	 * (((<[^>]+>)*($word_chars)+(<[^ <w >$word</w> text Other word patterns
	 * without elision >]+>)*)+)(\s+|$punct_all) -
	 * (((<[^>]+>)*($word_chars)+(<[^ <w >$word</w> text Other word patterns
	 * with  >]+>)*)+)['’]
	 */

	/** The counter. */
	int counter = 1;

	/** The m. */
	def m;

	/** The segment. */
	def segment;

	/** The note. */
	def note;

	/** The tag. */
	def tag;

	/** The tag_name. */
	def tag_name;

	/** The tag_atts. */
	def tag_atts;

	/** The starttag. */
	def starttag;

	/** The endtag. */
	def endtag;

	/** The word. */
	def word;

	/** The linetype. */
	def linetype = "text"; // input processing state, can be : text, word, comment, head, note

	/** The head_name. */
	def head_name;

	/** The tag_all. */
	def tag_all = "<[^>]+>";
	
	TokenizerClasses tc;

	String filename;
	int wordcount = 1;
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args)
	{
		try {
			tc.word_tags = args.get("word_tags");
			tc.intraword_tags = args.get("intraword_tags");
			tc.punct_strong = args.get("punct_strong");
			tc.punct_all = args.get("punct_all");
			tc.word_chars = args.get("word_chars");
		} catch(Exception e) {
			System.err.println(e)
			System.err.println("tokenizer needs an \"args\" map with the following elements :\n word_tags, intraword_tags, punct_strong, " +
					"punct_all, word_chars")
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		// System.out.println("beginning of tokenizer processing");
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after() {
		if (charerrors.size() > 0) {
			System.out.print("Tokenizer unknown word chars in $filename: ");
			println charerrors;
			
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
		String word;
		// Write your code here, but don't forget to write to the output
		// Example : output.print("TheStringToWrite " + line);
		// The line variable contains the current line
		segment = line+" ";
		segment = segment.replace("&amp;", "&").replace("&apos;", "'").replace("&quot;","'");

		if(DEBUG-- > 0)
			println("Line: "+line);
		while (true) {
			if (DEBUG-- > 0)
				println " segment: "+segment+"\n linetype: "+linetype;

			if (linetype == "word") {
				if(	(m = segment =~ /\A\s*(.*?)<\/(${tc.word_tags})>(.*)\Z/) )
				{
					segment = m[0][3];
					linetype = "text";
					output.print( m[0][1]+"</w>"+lineSeparator);
					if (segment ==~ /\S/)
					{
						continue;
					}
				}
				else
				{
					segment = (segment =~ /\A\s*/).replaceAll("");
					segment = segment.trim();
					output.print(segment);
				}
			}
			else if (linetype == "comment")
			{
				if(	(m = segment =~ /\A\s*(.*?)-->(.*)\Z/))
				{
					linetype = "text";
					output.print(m[0][1]+"-->"+lineSeparator);
					//println(m[0][1]+"-->\n");
					segment = m[0][2];
					if(! (segment ==~ /^\s*$/))
					{
						continue;
					}
				}
				else
				{
					segment = (segment =~ /\A\s*/).replaceAll("");
					output.print(segment);
					//println(segment);
				}
			}
			else if (linetype == "head")
			{
				if(	(m = segment =~ /\A\s*(.*?)<\/($head_name)>\s*\Z/))
				{
					linetype = "text";
					output.print(m[0][1]+"</$head_name>"+lineSeparator);
					head_name = "";
				}
				else
				{
					output.print(segment);
					//print "Editor's title: $segment\n";
					//println("Editor's title: $segment\n");
				}
			}
			else if (linetype == "note")
			{
				if(	(m = segment =~ /\A\s*(.*?)<\/note>/))
				{
					linetype = "text";
					output.print(m[0][1]+"</note>-->"+lineSeparator);
					//println(m[0][1]+"</note>\n");
				}
				else
				{
					segment = (segment =~ /\A\s*/).replaceAll("");
					output.print(segment);
					//println(segment);
				}
			}
			else if (linetype == "text")
			{
				if(	(m = segment =~ /\A\s*<(head|p) [^>]*lang=['"]fr['"]/) )
				{
					if(DEBUG-- > 0)
						println "  tag head|p: m01"+head_name;
					head_name = m[0][1];
					output.print(segment);
					linetype = "head";
				}
				else if( (m = segment =~ /\A(\s*<w( [^>]*)?>\s*)($tag_all\s*)*\Z/))
				{
					String wordtag = m[0][1];
					String tail = m[0][3].trim();
					if(!wordtag.contains("id=\""))
						wordtag ="<w id=\"w_"+filename+"_"+wordcount+"\""+ wordtag.substring(2);
					wordcount++;
					if(DEBUG-- > 0)
						println "  tag w: m01"+head_name;
					segment = tail;
					if (segment == "</w>") {
						println("\nEmpty word tag found, adding [???] content!\nsegment: '$wordtag[???]$tail'");
						output.print(wordtag.trim()+"[???]"+tail.trim()+lineSeparator);
					} else if( !(segment ==~ /<\/w>/)) {
						linetype = "word";
						output.print(wordtag.trim()+tail.trim());
					}
					else
						output.print(wordtag.trim()+tail.trim()+lineSeparator);
				}
				else if( (m = segment =~ /\A\s*(<note[^>]*>)(.*)\Z/)){
					if(DEBUG-- > 0)
						println "  tag note: new seg="+m[0][2];
					segment = m[0][2];
					def note = m[0][1];
					//println("Note found: $note");
					output.print(lineSeparator+"<!--$note");
					if( (m = segment =~ /\A(.*?)<\/note>(.*)\Z/)){
						segment = m[0][2];
						//println(m[0][1]+"</note>\n");
						output.print(m[0][1]+"</note>-->"+lineSeparator);
					}
					else {
						linetype = "note";
						//println("$segment ");
						output.print("$segment ");
						segment = " ";
					}
				}
				else if (segment ==~ /\A\s*($tag_all\s*)+\Z/)  {
					//print "Line with tags only : $line\n";
					if(DEBUG-- > 0)
						println "  tag all: ";

					output.print(segment+lineSeparator);
				}
				else if( (m = segment =~ /\A\s*(.*?)\s*\Z/)){


					segment = m[0][1]+" ";
					while(! (segment ==~ /\A\s*\Z/) )
					{
						if(DEBUG-- > 0)
							println "  other: new seg="+segment;
						if( (m = segment =~ /\A\s*<!--(.*)\Z/))
						{
							segment = m[0][1]+" ";
							if(DEBUG-- > 0)
								println("   comment: new seg="+segment);
							output.print(lineSeparator+"<!--");
							if( (m = segment =~ /\A(.*?)-->(.*)\Z/)) {
								segment = m[0][2];
								//println(m[0][1]+"-->"+lineSeparator);
								output.print(m[0][1]+"-->"+lineSeparator);
							}
							else  {
								linetype = "comment";
								//println("$segment ");
								output.print("$segment ");
								segment = " ";
							}
						}
						else if( (m = segment =~ /\A\s*<(${tc.word_tags})([^>]*)>(.*)\Z/)) {
							if(DEBUG-- > 0)
								println("   tag word: 1w="+m[0][1]+" 2="+m[0][2]+" 3seg="+m[0][3]);
							segment = m[0][3];
							tag_name = m[0][1];
							tag_atts = m[0][2];
							//print "Tagged words, punctuation marks, abbreviations and numbers : <$tag_name$tag_atts>\n";
							output.print("<w");
							if(!tag_atts.contains("id=\""))
							{
								tag_atts = " id=\"w_"+filename+"_"+wordcount+"\" ";
							}
							wordcount++;
							if (tag_name ==~ /abbr|num/) {
								output.print(" type=\"$tag_name\"");
							}
							output.print("$tag_atts>");
							if( (m = segment =~ /\A(.*?)<\/($tag_name)>(.*)\Z/)) {
								word = m[0][1];
								segment = m[0][3];
								word = (word =~ /^\s*(.*)\s*$/).replaceAll("\$1");
								output.print("$word</w>"+lineSeparator);
							}
							else  {
								linetype = "word";
								output.print(segment);
								segment = " ";
							}
						}
						else if( (m = segment =~ /\A\s*($tag_all)\s+(.*)\Z/)) {
							if(DEBUG-- > 0)
								println "   Separated tags: 1w="+m[0][1]+" 2seg="+m[0][2];
							segment = m[0][2];
							tag = m[0][1];
							output.print("$tag"+lineSeparator);
						}
						else if( (m = segment =~ /\A\s*([.·][^ .·]+[.·])(.*)\Z/))
						{
							if(DEBUG-- > 0)
								println "   Numbers and abbreviations: 1w="+m[0][1]+" 2seg="+m[0][2];
							segment = m[0][2];
							word = m[0][1];
							word = (word =~ /^\s*(.*)\s*$/).replaceAll("\$1");
							output.print("<w id=\"w_"+filename+"_$wordcount\" type=\"num\">$word</w>"+lineSeparator);
							wordcount++;
							//push @numbers, $word;
							//print "Number found: <w type=\"num\">$word</w>\n";
						}
						else if( (m = segment =~ /\A\s*(${tc.punct_all}|[${tc.punct_quotes}])(.*)\Z/))
						{
							if(DEBUG-- > 0)
								println "   Punctuation marks: 1w="+m[0][1]+" 2seg="+m[0][2]
							segment = m[0][2];
							word = m[0][1];
							word = (word =~ /^\s*(.*)\s*$/).replaceAll("\$1");
							word = word.replace("<", "&lt;");
							output.print("<w id=\"w_"+filename+"_$wordcount\" type=\"pon\">"+word+"</w>"+lineSeparator);
							wordcount++;
						}
						else if( (m = segment =~ /\A\s*((${tc.word_chars})*[{\(\[](${tc.word_chars})+[\)\]}](${tc.word_chars})+|(${tc.word_chars})+[{\(\[](${tc.word_chars})+[\)\]}])(\s+|${tc.punct_all})(.*?)\Z/))						{
							if(DEBUG-- > 0)
								println "   Words with brackets: 1w="+m[0][1]+" 2="+m[0][2]+" 3="+m[0][3]+" 4="+m[0][4]+" 5="+m[0][5]+" 6="+m[0][6]+" 7seg="+m[0][7]+" 8seg="+m[0][8];
							segment = m[0][7]+m[0][8];//"$7$8";
							word = m[0][1];
							word = (word =~ /^\s*(.*)\s*$/).replaceAll("\$1");
							//push @w_brackets, $word;
							def word_clean = word;
							word_clean = (word_clean =~ /($tag_all)/).replaceAll("");
							if (word_clean ==~ /[^0-9A-Za-zœÀ-ÿ'’\(\)\[\]–\-]/)
							{	//System.err.println("Line $linecounter Error : " +word);
								charerrors.add(word);
							}
							output.print("<w id=\"w_"+filename+"_$wordcount\">"+word+"</w>"+lineSeparator);
							wordcount++;
						}
						else if( (m = segment =~ /\A\s*(((<[^>]+>)*(${tc.word_chars}+)(<[^>]+>)*)+)(\s+|${tc.punct_all})(.*?)\Z/))
						{
							segment = m[0][6]+m[0][7];//"$6$7";
							word = m[0][1];

							if(DEBUG-- > 0)
							{
								println "Other word without elision: 1w="+m[0][1]+" 2="+m[0][2]+" 3="+m[0][3]+" 4="+m[0][4]+" 5="+m[0][5]+" 6seg="+m[0][6]+" 7seg="+m[0][7];
							}
							def word_clean = word;
							word_clean = (word_clean =~ /($tag_all)/).replaceAll("");
							if (word_clean ==~ /[^0-9A-Za-zœÀ-ÿ'’()[\\]–\\-]/)
							{
								//System.err.println("Line $linecounter Error : " +word);
								charerrors.add(word);
							}
							String towrite = "<w id=\"w_"+filename+"_$wordcount\">"+word.toString()+"</w>"+lineSeparator;
							wordcount++;
							output.print(towrite);
						}
						else if( (m = segment =~ /\A\s*(((<[^>]+>)*(${tc.word_chars})+(<[^>]+>)*)+)[${tc.punct_quotes}](.*?)\Z/))
						{
							if(DEBUG-- > 0)
								println "   Other word patterns with elision: 1w="+m[0][1]+" 2="+m[0][2]+" 3="+m[0][3]+" 4="+m[0][4]+" 5="+m[0][5]+" 6seg="+m[0][6];
							segment = m[0][6];
							word = m[0][1]+"'";
							def word_clean = word;
							word_clean = (word_clean =~ /($tag_all)/).replaceAll("");
							if (word_clean ==~ /[^0-9A-Za-zœÀ-ÿ'’\(\)\[\]–\-]/)                    			{
								//System.err.println("Line $linecounter Error : " +word);
								charerrors.add(word);
							}
							output.print("<w id=\"w_"+filename+"_$wordcount\">"+word+"</w>"+lineSeparator);
							wordcount++;
						}
						else{
							System.err.println("   Unknown word pattern $segment");
							output.print("<note>Error! Unknown word pattern: $segment</note>"+lineSeparator);
							segment = " ";
						}
					}
				}
				else
				{
					println(" line (?) : $segment \n");
					System.exit(-1)
				}
			}
			else
			{
				output.print(segment);
			}
			break;
		}
		// End
	}
}
