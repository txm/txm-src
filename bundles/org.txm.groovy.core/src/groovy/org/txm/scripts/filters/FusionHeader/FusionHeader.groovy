// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.FusionHeader;

import java.io.File;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.txm.importer.scripts.filters.*;

// TODO: Auto-generated Javadoc
/**
 * The Class FusionHeader.
 */
class FusionHeader extends Filter {
	
	/** The xml header file. */
	String xmlHeaderFile = "";
	
	/** The first loop. */
	boolean firstLoop = true;
	
	/** The HEADER. */
	BufferedReader HEADER;
	
	/** The cut headerfilter. */
	Thread cutHeaderfilter;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {
		if (args.size() == 2) {
			xmlHeaderFile = args[0].toString();
			cutHeaderfilter = (Thread) args[1];
		} else {
			System.err
					.println("CutHeader needs 2 args\nString:xmlHEADERFilePath\nThread:cutHeaderFilter");
			System.exit(-1);
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after() {
		try {
			HEADER.close();
			HEADER= null;
		} catch (Exception e) {
			System.err
					.println("Can't close Header file, one cause might be that the header file is not correctly built");
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		if (xmlHeaderFile == "" || cutHeaderfilter == null) {
			System.err
					.println("CutHeader needs 2 args\nString:xmlHEADERFilePath\nThread:cutHeaderFilter");
		}
		// System.out.println("start FUSIONHEADER");

		return true;
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	protected void filter()
	{
		if (firstLoop) {
			//println("BEGIN COPYING HEADER");
		
			String templine;
			File inputFile = new File(xmlHeaderFile)
			if ( !inputFile.exists()) {
				System.err.println("ERROR : DOES NOT EXISTS "+xmlHeaderFile);
			}
			HEADER = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), encodage));
			while (!HEADER.ready()) {
				Thread.sleep(10);
				//system.out.println("waiting for cutHeader to finish");
			}
			//HEADER = new FileReader("/home/ayepdieu/xml/header (copie).xml");
			//System.out.println("ready ? "+HEADER.ready());
			while ((templine = this.HEADER.readLine()) != null) {
				//println(templine+lineSeparator);
				output.print(templine+lineSeparator);
			}
			//println("FINISH COPYING HEADER");
			firstLoop=false;
			HEADER.close();
		}
		output.print(line+lineSeparator);
	}
}
