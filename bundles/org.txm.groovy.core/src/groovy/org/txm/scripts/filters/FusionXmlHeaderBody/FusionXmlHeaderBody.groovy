// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.FusionXmlHeaderBody;

import org.txm.importer.scripts.filters.*;
import org.xml.sax.InputSource
import org.xml.sax.helpers.ParserAdapter
import org.xml.sax.helpers.*;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import java.io.Reader;
import groovy.xml.XmlUtil
import groovy.xml.XmlSlurper
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.helpers.DefaultHandler

// TODO: Auto-generated Javadoc
/**
 * The Class FusionXmlHeaderBody.
 */
class FusionXmlHeaderBody extends Filter
{
	
	/** The body file. */
	String bodyFile = "";
	
	/** The header file. */
	String headerFile = "";
	
	/** The Lastinput. */
	Reader Lastinput;
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args)
	{
		
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before()
	{
		if(bodyFile == "" || headerFile == "")
		{
			System.out.println("FusionXmlHeaderBody need 2 args : \nString:XmlBodyFile\nString:XmlHeaderFile");
		}
		System.out.println("start fusionxmlheaderbody "+this);
		Lastinput = this.input;
		this.input = new BufferedReader(new java.io.StringReader("") );
	}
	
	/**
	 * Sets the used param.
	 *
	 * @param args the args
	 */
	void SetUsedParam(Object[] args)
	{
		if(args.size() == 2)
		{
			bodyFile = args[0];//needed to get back its ID
			headerFile = args[1];
		}
		else
		{
			System.out.println("FusionXmlHeaderBody need 2 args : \nString:XmlBodyFile\nString:XmlHeaderFile");
		}
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after()
	{
		this.input = this.Lastinput;
		//System.out.println("input ready "+this.input.ready());
		def body = new XmlSlurper().parse(this.input);
		
		def headers = new XmlSlurper().parse(headerFile)
		
		def id = bodyFile.split('\\.')[0]
		def header = headers.discours.find{it.@file == id}
		
		this.output.withWriter{writer ->
			def xmlBuilder = new groovy.xml.StreamingMarkupBuilder();
			def xml = xmlBuilder.bind{
				mkp.xmlDeclaration();
				top (header <<body.children());	
			};
		
			writer <<XmlUtil.serialize(xml);
		}		
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
	}
	
}
