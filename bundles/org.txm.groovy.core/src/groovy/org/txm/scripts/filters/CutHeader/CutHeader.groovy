// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.CutHeader;

import java.util.regex.Matcher
import java.util.regex.Pattern

import org.txm.importer.scripts.filters.Filter;

// TODO: Auto-generated Javadoc
/**
 * The Class CutHeader.
 */
class CutHeader extends Filter
{
	
	/** The xml header file. */
	String xmlHeaderFile = ""; 
	
	/** The header. */
	boolean header = true;
	
	/** The HEADER. */
	OutputStreamWriter HEADER;
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {
		if (args.size() == 1) {
			xmlHeaderFile = args[0].toString();
		} else {
			System.err.println("CutHeader needs 1 args\nString:xmlHEADERFilePath");
			System.exit(-1);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after()
	{
		HEADER.close();
		//println("AFTER CUTHEADER");
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	protected void filter()
	{
		if(header) {
			//if(line ==~ /\s*<text>\s*/ )
			if (line.startsWith("<text ") || line.startsWith("<text>")) {
				header = false;
				HEADER.close();
				output.print(line+lineSeparator);
			} else {
				//print(line+lineSeparator);
				HEADER.print(line+lineSeparator);
			}
		} else {
			output.write(line+lineSeparator);	
		}
		//System.out.println( "!"+linecounter);
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		if (xmlHeaderFile == "") {
			println("need to define xmlHeaderFile before lauching this filter");
			return false;
		}
		File outputFile = new File(xmlHeaderFile)
		//HEADER = new PrintWriter(new BufferedWriter(new FileWriter(outputFile)));
		HEADER = new OutputStreamWriter(new FileOutputStream(outputFile) , this.encodageSortie);
		//println("BEFORE CUTHEADER");
		return true;
	}
}
