// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.ReunitBrokenWords;
//Pre-processing extra-word tags (2)
import org.txm.importer.scripts.filters.*;
import org.txm.tokenizer.*;

// TODO: Auto-generated Javadoc
/**
 * The Class ReunitBrokenWords.
 */
class ReunitBrokenWords extends Filter
{
	
	/** The tag_all. */
	def tag_all;
	
	/** The div_tags. */
	def div_tags;
	
	/** The q_tags. */
	def q_tags ;
	
	/** The extraword_tags. */
	def extraword_tags;
	
	/** The corr_tags_no_seg. */
	def corr_tags_no_seg;
	
	/** The corr_tags. */
	def corr_tags;
	
	/** The word_tags. */
	def word_tags;
	
	/** The intraword_tags. */
	def intraword_tags;
	
	/** The tag. */
	def tag;
	
	/** The tag_end. */
	def tag_end;
	
	/** The tag_name. */
	def tag_name;
	
	/** The word_part_tags. */
	List<String> word_part_tags = [];
	
	/** The unknowned. */
	Set<String> unknowned = new HashSet<String>();
	
	/** The level. */
	private String level = "extraword";
	
	public ReunitBrokenWords() {
		TokenizerClasses tc = new TokenizerClasses()
		/** The tag_all. */
		tag_all = tc.tag_all;
		
		/** The div_tags. */
		div_tags = tc.div_tags;
		
		/** The q_tags. */
		q_tags = tc.q_tags;
		
		/** The extraword_tags. */
		extraword_tags = tc.extraword_tags;
		
		/** The corr_tags_no_seg. */
		corr_tags_no_seg = tc.corr_tags_no_seg;
		
		/** The corr_tags. */
		corr_tags = tc.corr_tags;
		
		/** The word_tags. */
		word_tags = tc.word_tags;
		
		/** The intraword_tags. */
		intraword_tags = tc.intraword_tags;
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args)
	{
		try
		{
			div_tags = args.get("div_tags");
			q_tags = args.get("q_tags");
			extraword_tags = args.get("extraword_tags");
			corr_tags_no_seg = args.get("corr_tags_no_seg");
			corr_tags = args.get("corr_tags");
			word_tags = args.get("word_tags");
			intraword_tags = args.get("intraword_tags");
		}
		catch(Exception e)
		{
			System.err.println(e);
			System.err.println("PreProcess2 needs 1 Map with args  :\n div_tags, q_tags, extraword_tags, " +
					"corr_tags_no_seg, corr_tags, word_tags, intraword_tags")
		}
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before(){	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after()
	{ 
		if(unknowned.size() > 0)
			println "Unknown tags: "+unknowned;
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
		def m;
		def segment;
		boolean firstTest = true;

		while(!(line ==~ /\A\s*\Z/))// tant que la ligne contient des charracters autres que des blancs
		{
			if ((m = line =~ /^([^<]*)$/)) 	 // si la ligne NE commence PAS par une balise
			{
				if(firstTest)
				{
					//output.print(lineSeparator);
					firstTest=false;
				}
				output.print(line);//on ecrit la ligne
				line = " "
			}
			else if ((m = line =~ /\A\s*(<\/?($extraword_tags)( [^>]*|\/)?>)(.*)\Z/)) // balise externe 	
			{	
				tag = m[0][1]; // le nom du tag
				line = m[0][4];
				//println "balise ext : "+tag
				level = "extraword"; //di qu'on est ds un tag extern
				output.print("\n$tag \n");
			}
			else if ((m = line =~ /\A\s*(<($corr_tags)( [^>]*|\/)?>)(.*)\Z/)) //balise corr ouvrante		
			{   
				tag = m[0][1];
				String tag_end = m[0][3]; // ( [^>]*|\/)?>)
				line = m[0][4];
				//println "balise corr : "+tag+" end : "+tag_end
				if(tag_end != null && tag_end.find("word_part")) //on trouve un word_part	
				{
					//println "set level : intra :"+tag
					word_part_tags.add(m[0][2]);
					level = "intraword";
					output.print(tag);
				}
				else {
					level = "extraword";
					output.print("\n$tag\n");
				}
			} 
			else if ((m = line =~ /^\s*(<($word_tags)( [^>]*|\/)?>)(.*)$/)) 				
			{
				tag = m[0][1];
				line = m[0][4];
				//println "balise word : "+tag
				//output.print("\n$tag");
				output.write(line)
				level = "intraword";
			}
			else if ((m = line =~ /^\s*(<($intraword_tags)( [^>]*|\/)?>)(.*)$/))			
			{
				tag = m[0][1];
				line = m[0][4];
				//println "balise intra : "+tag
				output.print(tag);
				level = "intraword";
			}
			else if ((m = line =~ /^\s*(<\/($corr_tags|$word_tags|$intraword_tags)>)\s*(.*)$/))
			{
				tag = m[0][1];
				tag_name = m[0][2];
				line = m[0][3];
				//println "balise tous : "+tag+" level : "+level
				if (level ==~ /extraword/) 
				{//println "write extra"
					output.print("\n");
				}//else{println "dont write intra"}
				
				output.print(tag);
				if (tag_name ==~ /^($corr_tags|$word_tags)$/) 				
				{
					//println("> corrwordtags "+tag_name+" level "+level+" Word_part_tag "+word_part_tags)
					if (level ==~ /extraword/) 
					{
						output.print("\n");
					}
					//else{println "dont write intra corr"}
					if(word_part_tags.size() > 0)
					{
						//String word_part_tag = word_part_tags[-1];
						//if(tag_name == word_part_tag)
						//{
							word_part_tags.remove(word_part_tags.size()-1)
							if(word_part_tags.size() == 0 )
							{
								level = "extraword";
								//output.print("\n");
							}
						//}
					}
					else
						level = "extraword"
				}
			}
			else if ((m = line =~ /^\s*($tag_all)(.*)$/)) 				
			{
				//println "balise autres : "+tag
				tag = m[0][1];
				line = m[0][2];
				output.print("\n$tag\n");
				if(!unknowned.contains(tag))
				{
					unknowned.add(tag)
					//println "Warning: unknown tag $tag in line : "+line;
				}
				
			}
			else{			
				println "Error in "+ line;
				line = " ";
			}
			firstTest=false;
		}
		// End
	}
}
