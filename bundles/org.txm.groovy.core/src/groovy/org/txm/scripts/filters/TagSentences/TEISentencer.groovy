package org.txm.scripts.filters.TagSentences

import org.txm.importer.StaxIdentityParser;
import org.txm.importer.ValidateXml;

import java.net.URL;
import org.txm.scripts.filters.Tokeniser.TEITokenizer.STATES;

class TEISentencer extends StaxIdentityParser {

	boolean DEBUG = false;
	String filename;
	
	enum STATES { IGNORE, NOTE, WORK, W };
	enum SENT { IN, OUT };
	STATES state = STATES.IGNORE;
	SENT sentence = SENT.OUT;
	
	String STARTTAGS = "text"
	String NOTETAGS = "note"
	String WORDTAGS = "w"
	String DIVTAGS = "div"
	String QTAGS = "q"
	String EDTAGS = "supplied"
	def openTags = [];
	
	public TEISentencer(URL inputurl) {
		super(inputurl);

		this.filename = new File(inputurl.getFile()).getName();
		int index = filename.lastIndexOf(".");
		if (index > 0)
			filename = filename.substring(0, index);
	}

	protected void processStartElement() {

		if (state == STATES.IGNORE) {
			sentence = SENT.OUT;
			if (localname == STARTTAGS) {
				state = STATES.WORK;
			}
			super.processStartElement();
		} else {
			if (localname == NOTETAGS) {
				state = STATES.NOTE;
				super.processStartElement();
				goToEnd(localname)
			} else {
				switch(sentence) {
					case SENT.IN:
						processStartElementIN();
						break;
					case SENT.OUT:
						processStartElementOUT();
						break;
				}
			}
		}
	}
	
	@Override
	protected void processEndElement() {
		if (state == STATES.IGNORE) {
			super.processEndElement();
		} else {
			if (localname == STARTTAGS) {
				state = STATES.IGNORE;
				super.processEndElement();
			} else {
				switch(sentence) {
					case SENT.IN:
						processEndElementIN();
						break;
					case SENT.OUT:
						processEndElementOUT();
						break;
				}
			}
		}
	}
	
	protected void cutSentence() {
		for( int i = 0 ; i < openTags.size() ; i++) {
			writer.writeEndElement();
		}
		endSentence() // close sent
		super.processStartElement();
		startSentence()
		for( Tag tag : openTags) {
			tag.writeStart(writer);
		}
	}
	
	protected void cutSentenceAndClose() {
		for( int i = 0 ; i < openTags.size() ; i++) {
			writer.writeEndElement();
		}
		endSentence() // close sent
		super.processEndElement();
		startSentence()
		for( Tag tag : openTags) {
			tag.writeStart(writer);
		}
	}
	
	protected void closeRemainingSentence() {
		def tags = []
		while( openTags.size() > 0) {
			writer.writeEndElement();
			tags << openTags.pop()
		}
		writer.writeEndElement(); // close sent
		sentence = SENT.OUT;
	}
	
	protected void processStartElementIN() {
		if (localname.matches(DIVTAGS)) {
			if (openTags.size() == 0) {
				endSentence();
				super.processStartElement();
				println "WARNING: start DIVTAG in sentence at "+getLocation()
			} else {
				cutSentence();
				println "WARNING: DIVTAG opepened inside EDTAG at "+getLocation()
			}
		} else if (localname.matches(QTAGS)) {
			if (openTags.size() == 0) {
				endSentence();
				super.processStartElement();
			} else {
				cutSentence();
				println "WARNING: QTAG opepened inside EDTAG at "+getLocation()
			}
		} else if (localname.matches(EDTAGS)) {
			if (openTags.size() == 0) {
				if (hasMULTIS()) {
					endSentence();
				} else {
					if (openTags.size() > 0 && openTags[0].localname == localname) {
						println "STOP: openning same EDTAG $localname at "+getLocation()
						closeForError();
					}
					openTags << new Tag(parser)
				}
				super.processStartElement();
			} else {
				if (hasMULTIS()) {
					cutSentence();
					println "WARNING: EDTAG opened  inside EDTAG"
				} else {
					super.processStartElement();
					if (openTags.size() > 0 && openTags[0].localname == localname) {
						println "STOP: openning same EDTAG $localname at "+getLocation()
						closeForError();
					}
					openTags << new Tag(parser)
				}
			}
		} else if (localname.matches(WORDTAGS)) {
			boolean shouldClose = isWEndPunct()
			if (openTags.size() == 0) {
				super.processStartElement();
				goToEnd(localname);
				if (shouldClose) {
					endSentence();
				}
			} else {
				super.processStartElement();
				goToEnd(localname);
				if (shouldClose) {
					println "WARNING: Strong ponctuation found inside EDTAG at "+getLocation()
				}
			}
		} else { // OTHER TAGS
			if (openTags.size() == 0) {
				if (hasMULTIS()) {
					endSentence();
				} else {
					if (openTags.size() > 0 && openTags[0].localname == localname) {
						println "STOP: openning same EDTAG $localname at "+getLocation()
						closeForError();
					}
					openTags << new Tag(parser)
				}
				super.processStartElement();
			} else {
				if (hasMULTIS()) {
					cutSentence();
					println "WARNING: multi_s tag opens inside EDTAG"
				} else {
					super.processStartElement();
					if (openTags.size() > 0 && openTags[0].localname == localname) {
						println "STOP: openning same TAG $localname at "+getLocation()
						closeForError();
					}
					openTags << new Tag(parser);
				}
			}
		}
	}

	protected void processEndElementIN() {
		if (localname.matches(DIVTAGS)) {
			if (openTags.size() == 0) {
				endSentence();
				super.processEndElement();
			} else {
				cutSentenceAndClose();
				println "WARNING: DIVTAG closed  inside EDTAG"
			}
		} else if (localname.matches(QTAGS)) {
			if (openTags.size() == 0) {
				endSentence();
				super.processEndElement();
			} else {
				cutSentenceAndClose();
				println "WARNING: QTAG closed  inside EDTAG"
			}
		} else if (localname.matches(EDTAGS)) {
			if (openTags.size() == 0) { // it was a multi_s EDTAG
				endSentence();
				super.processEndElement();
			} else {
				if (openTags[-1].localname == localname) {
					super.processEndElement();
					openTags.pop();
				} else {
					println "STOP: closing EDTAG does not match last opened EDTAG "+localname+" != "+openTags[-1].localname+" at "+getLocation()
					closeForError()
				}
			}
		} else if (localname.matches(WORDTAGS)) {
			println "ERROR: processEndElementIN: </w> at "+getLocation();
		} else {
			if (openTags.size() == 0) {
				super.processEndElement();
			} else {
				if (openTags[-1].localname == localname) {
					super.processEndElement();
					openTags.pop();
				} else {
					println "STOP: closing tag does not match last opened EDTAG "+localname+" != "+openTags[-1].localname+" at "+getLocation()
					closeForError()
				}
			}
		}
	}

	protected void processStartElementOUT() {
		if (localname.matches(DIVTAGS)) {
			super.processStartElement();
		} else if (localname.matches(QTAGS)) {
			super.processStartElement();
		} else if (localname.matches(EDTAGS)) {
			if (hasMULTIS()) {
				super.processStartElement();
			} else {
				startSentence();
				super.processStartElement();
				if (openTags.size() > 0 && openTags[0].localname == localname) {
					println "STOP: openning same EDTAG name at "+getLocation()
					closeForError();
				}
				openTags << new Tag(parser);
			}
		} else if (localname.matches(WORDTAGS)) {
			if (isWEndPunct()) {
				startSentence();
				super.processStartElement();
				goToEnd(localname);
				endSentence();
				println "WARNING: empty sentence at "+getLocation();
			} else {
				startSentence();
				super.processStartElement();
				goToEnd(localname);
			}

		} else {

			if (hasMULTIS()) {
				super.processStartElement();
			} else {
				startSentence();
				super.processStartElement();
				if (openTags.size() > 0 && openTags[0].localname == localname) {
					println "STOP: openning same TAG $localname name at "+getLocation()
					closeForError();
				}
				openTags << new Tag(parser)
				println "WARNING: unclassified tag: "+localname+" at "+getLocation();
			}
		}
	}

	protected void processEndElementOUT() {
		if (localname.matches(DIVTAGS)) {
			super.processEndElement();
		} else if (localname.matches(QTAGS)) {
			super.processEndElement();
		} else if (localname.matches(EDTAGS)) {
			super.processEndElement();
		} else if (localname.matches(WORDTAGS)) {
			println "ERROR: processEndElementOUT: </w> at "+getLocation();
		} else {
			super.processEndElement();
		}
	}

	private static String STAG = "s"
	protected void startSentence() {
		sentence = SENT.IN;
		writer.writeStartElement(STAG);
	}

	protected void endSentence() {
		sentence = SENT.OUT;
		writer.writeEndElement();
	}

	private static String ENDOFSENTENCE = "PONfrt"
	protected boolean isWEndPunct() {
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			if (parser.getAttributeValue(i) == ENDOFSENTENCE) return true;
		}
		return false;
	}

	private static String MULTI_S = "multi_s"
	protected boolean hasMULTIS() {
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			if (parser.getAttributeValue(i) == MULTI_S) return true;
		}
		return false;
	}

	public static void main(String[] args) {
		File dir = new File ("/home/mdecorde/TXM/corpora/alceste/tokenized")
		File outdir = new File ("/home/mdecorde/TXM/corpora/alceste/tokenized/s")
		outdir.mkdir()
		for (File infile : dir.listFiles()) {
			long start = System.currentTimeMillis()
			println infile
			File outfile = new File(outdir, infile.getName())
	
			TEISentencer sentencer = new TEISentencer(infile.toURI().toURL());
			if (sentencer.process(outfile)) {
				println "DONE - OK: "+ValidateXml.test(outfile);
			} else {
				println "DONE - FAIL"
			}
			println "time: "+(System.currentTimeMillis()-start)/1000
		}
	}

	public class Tag {
		public String[] attnames, attvalues, attprefix;
		public String localname, prefix
		int count;
		ArrayList<String> contents = [""];
		public ArrayList<Tag> children = [];
		boolean inChild = false;

		public Tag(def parser) {
			prefix = parser.getPrefix()
			localname = parser.getLocalName();
			count = parser.getAttributeCount()
			attnames =  new String[count]
			attvalues = new String[count]
			attprefix = new String[count]
			for (int i = 0 ; i < count ; i++) {
				attnames[i] = parser.getAttributeLocalName(i)
				attprefix[i] = parser.getAttributePrefix(i).toString()
				attvalues[i] = parser.getAttributeValue(i).toString()
			}
		}

		public void appendContent(String content) {
			if (inChild) {
				children[-1].appendContent(content);
			} else {
				this.contents[-1] += content;
			}
		}

		public void endOfChild() {
			contents << "";
			inChild = false;
		}

		public void addChild(Tag child) {
			if (inChild) {
				children[-1].addChild(child);
			} else {
				children.add(child)
				inChild = true;
			}
		}

		public writeStart(def writer) {
			if (prefix != null && prefix.length() > 0)
				writer.writeStartElement(prefix+":"+localname)
			else
				writer.writeStartElement(localname)

			for (int i = 0 ; i < count ; i++) {
				if (attprefix[i] != null && attprefix[i].length() > 0) {
					writer.writeAttribute(attprefix[i]+":"+attnames[i], attvalues[i])
				} else {
					writer.writeAttribute(attnames[i], attvalues[i])
				}
			}

			//			for ( Tag child: children) {
			//				child.write(writer)
			//			}

			//			writer.writeEndElement();
		}

		public String getContent() {
			return contents.toString();
		}
	}
}
