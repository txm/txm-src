// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.OneOpenTagPerLine;

//Pre-processing extra-word tags (1)
import org.txm.importer.scripts.filters.Filter;
import org.txm.tokenizer.TokenizerClasses;

// TODO: Auto-generated Javadoc
/**
 * The Class OneOpenTagPerLine.
 */
class OneOpenTagPerLine extends Filter {
	
	/** The tag_all. */
	String tag_all = new TokenizerClasses().tag_all;
	
	/** The counterreg1. */
	int counterreg1 = 0;
	
	/** The counterreg2. */
	int counterreg2 = 0;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {

	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {

	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after()
	{
		println "reg1 : $counterreg1";
		println "reg2 : $counterreg2";
	}

	/** The segment. */
	def segment;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
		segment = line.trim()
		if(segment[0] == "<")
			output.write(lineSeparator+line);
		else
		output.write(" "+segment)
		/*if ( line ==~ /([^<])+>$/)
		{
			output.write(" "+line.trim())
		}
		else
		{
			output.write(lineSeparator+line);
		}*/
	}
}