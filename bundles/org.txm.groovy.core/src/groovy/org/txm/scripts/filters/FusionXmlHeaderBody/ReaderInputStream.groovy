// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.FusionXmlHeaderBody;

/*
 * Copyright 2004-2005 The Apache Software Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

// TODO: Auto-generated Javadoc
/**
 * Adapts a <code>Reader</code> as an <code>InputStream</code>. Adapted from
 * <CODE>StringInputStream</CODE>.
 */
public class ReaderInputStream extends InputStream {
	Reader input;

	/** The encoding. */
	String encoding = System.getProperty("file.encoding");

	/** The slack. */
	byte[] slack;

	/** The begin. */
	int begin;

	/**
	 * Construct a <CODE>ReaderInputStream</CODE> for the specified
	 * <CODE>Reader</CODE>.
	 * 
	 * @param reader
	 *            <CODE>Reader</CODE>. Must not be <code>null</code>.
	 */
	public ReaderInputStream(Reader reader) {
		input = reader;
	}

	/**
	 * Construct a <CODE>ReaderInputStream</CODE> for the specified
	 * <CODE>Reader</CODE>, with the specified encoding.
	 * 
	 * @param reader
	 *            non-null <CODE>Reader</CODE>.
	 * @param encoding
	 *            non-null <CODE>String</CODE> encoding.
	 */
	public ReaderInputStream(Reader reader, String encoding) {
		this(reader);
		if (encoding == null) {
			throw new IllegalArgumentException("encoding must not be null");
		} else {
			this.encoding = encoding;
		}
	}

	/**
	 * Reads from the <CODE>Reader</CODE>, returning the same value.
	 *
	 * @return the value of the next character in the <CODE>Reader</CODE>.
	 */
	public synchronized int read() throws IOException {
		if (input == null) {
			throw new IOException("Stream Closed");
		}

		byte result;
		if (slack != null && begin < slack.length) {
			result = slack[begin];
			if (++begin == slack.length) {
				slack = null;
			}
		} else {
			byte[] buf = new byte[1];
			if (read(buf, 0, 1) <= 0) {
				result = -1;
			}
			result = buf[0];
		}

		if (result < -1) {
			result += 256;
		}

		return result;
	}

	/**
	 * Reads from the <code>Reader</code> into a byte array.
	 *
	 * @param b the byte array to read into
	 * @param off the offset in the byte array
	 * @param len the length in the byte array to fill
	 * @return the actual number read into the byte array, -1 at the end of the
	 * stream
	 */
	public synchronized int read(byte[] b, int off, int len) throws IOException {
		if (input == null) {
			throw new IOException("Stream Closed");
		}

		while (slack == null) {
			char[] buf = new char[len]; // might read too much
			int n = input.read(buf);
			if (n == -1) {
				return -1;
			}
			if (n > 0) {
				slack = new String(buf, 0, n).getBytes(encoding);
				begin = 0;
			}
		}

		if (len > slack.length - begin) {
			len = slack.length - begin;
		}

		System.arraycopy(slack, begin, b, off, len);

		if ((begin += len) >= slack.length) {
			slack = null;
		}

		return len;
	}

	/**
	 * Marks the read limit of the StringReader.
	 * 
	 * @param limit
	 *            the maximum limit of bytes that can be read before the mark
	 *            position becomes invalid
	 */
	public synchronized void mark(final int limit) {
		try {
			input.mark(limit);
		} catch (IOException ioe) {
			throw new RuntimeException(ioe.getMessage());
		}
	}

	/**
	 * Available.
	 *
	 * @return the current number of bytes ready for reading
	 */
	public synchronized int available() throws IOException {
		if (input == null) {
			throw new IOException("Stream Closed");
		}
		if (slack != null) {
			return slack.length - begin;
		}
		if (input.ready()) {
			return 1;
		} else {
			return 0;
		}
	}

	/**
	 * Mark supported.
	 *
	 * @return false - mark is not supported
	 */
	public boolean markSupported() {
		return false; // would be imprecise
	}

	/**
	 * Resets the StringReader.
	 *
	 */
	public synchronized void reset() throws IOException {
		if (input == null) {
			throw new IOException("Stream Closed");
		}
		slack = null;
		input.reset();
	}

	/**
	 * Closes the Stringreader.
	 *
	 */
	public synchronized void close() throws IOException {
		if (input != null) {
			input.close();
			slack = null;
			input = null;
		}
	}
}
