// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.ProcessEnclitics;

import org.txm.importer.scripts.filters.*;
import java.util.regex.*;
import org.txm.tokenizer.TokenizerClasses;

// TODO: Auto-generated Javadoc
/**
 * The Class ProcessEnclitics.
 */
class ProcessEnclitics extends Filter {
	
	/** The counter. */
	int counter;
	
	/** The enclitics. */
	String enclitics = new TokenizerClasses().enclitics;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args)
	{
		try
		{
			enclitics = args.get("enclitics");

		}
		catch(Exception e)
		{
			System.err.println(e);
			System.err.println("Processenclitics needs 1 Map with arg  :\n enclitics")
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		counter = 0;
		System.out.println("begin enclitics");
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after()
	{
		print "$counter enclitics with dashes found\n";
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
		def m;
		def segment;
		// Write your code here, but don't forget to write in the output
		// ex : output.write("TheStringToWrite " + line );
		// in the var line is the current line
		if( (m = line =~ /\A\s*(<w[^>]*>)(.*)-($enclitics)<\/w>\Z/))
		{
			counter++;
			def word1_tag = (m[0][1]);
			def word1 = (m[0][2]);
			def word2 = (m[0][3]);
			output.write("$word1_tag$word1-</w>\n<w>$word2</w>"+lineSeparator);
		}
		else 
		{
			output.write(line+lineSeparator);
		}
		// End
	}
}
