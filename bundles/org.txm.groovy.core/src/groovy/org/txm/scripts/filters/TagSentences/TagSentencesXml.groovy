// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.filters.TagSentences;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.txm.importer.ValidateXml;
import org.txm.tokenizer.TokenizerClasses;
import org.txm.utils.AsciiUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.xml.stream.*;
import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * The Class TagSentencesXml.
 */
class TagSentencesXml {

	/** The url. */
	private def url;

	/** The input data. */
	private def inputData;

	/** The parser. */
	private XMLStreamReader parser;

	/** The factory. */
	XMLOutputFactory factory = XMLOutputFactory.newInstance();

	/** The output. */
	FileOutputStream output;

	/** The writer. */
	XMLStreamWriter writer;

	/** The IN. */
	boolean IN = false;
	boolean WORD = false;

	/** The pending. */
	boolean pending = false;

	/** The div_tags. */
	List<String> div_tags = ["front","body","div","head","trailer","p","ab","sp","speaker","list","item"];

	/** The flow_tags. */
	List<String> flow_tags = ["seg","foreign","hi","title","name","supplied","subst","add","damage", "unclear", "corr", "sic"];

	String startTag = "text";
	String rendMulti = "MULTI_S";
	boolean multi = false;
	boolean startProcess = false;
	String strongPunct = new TokenizerClasses().punct_strong;

	/** The localname. */
	String localname;
	boolean DEBUG = false;

	/**
	 * Instantiates a new tag sentences xml.
	 *
	 * @param discoursxml the discoursxml
	 */
	public TagSentencesXml(File discoursxml)
	{
		this.url = discoursxml.toURI().toURL();
		//corr_tags.addAll(corr_tags_no_seg);

		inputData = discoursxml.toURI().toURL().openStream();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile){
		try {
			output = new BufferedOutputStream(new FileOutputStream(outfile))
			writer = factory.createXMLStreamWriter(output, "UTF-8");//create a new file
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeCharacters("\n");
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/** The s_id. */
	int s_id = 1;

	/**
	 * Process.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	public boolean process(File outfile)
	{
		s_id = 1;
		if (!createOutput(outfile))
			return false;
		String localname = "";

		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				if (startProcess) {
					if (IN) {
						processIn(event);
					} else {
						processOut(event);
					}
				} else {
					writeEvent(event);
				}

				//detect start tag
				if (!startProcess)
					if (event == XMLStreamConstants.START_ELEMENT)
						if (parser.getLocalName() == startTag)
							startProcess = true;
			}
			
			writer.close()
			output.close()
			if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		} catch (Exception ex) {
			System.err.println(parser.getLocation().toString());
			ex.printStackTrace();
			
			writer.close()
			output.close()
			if (parser != null) parser.close();
		if (inputData != null) inputData.close();
			return false;
		}

		return true;
	}

	/**
	 * ignore content until the existing s is closed
	 * this sentence may contains others sentence
	 */
	public void processContentExistingSentence() // we're IN
	{
		ArrayList<String> openedtags = [];
		if(DEBUG) writer.writeComment("SENT+");
		assert(IN == true);
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					openedtags.add(localname); // stack the openened tag
					writeEvent(event);
					break;

				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					writeEvent(event);

					if (localname == "s") {
						//	println "end of word: "+parser.getLocation().getLineNumber();
						if(openedtags.size() == 0) // it's the end of the existing sentence
						{
							IN = false; // we're now OUT
							return; // we've finish to write the word
						}
					}
					openedtags.pop(); // pop the tag stack
					break;

				case XMLStreamConstants.CHARACTERS:
					writeEvent(event);
					break;
				case XMLStreamConstants.COMMENT:
					writer.writeComment(parser.getText());
					break;
			}
		}
		if(DEBUG) writer.writeComment("+SENT");
	}

	/**
	 * process the content of a word. start element must have been writen before calling this method
	 */
	public void processWordContent() // we're IN
	{
		if(DEBUG) writer.writeComment("WORD");
		assert(IN == true);
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					if(localname == "w")
						println "Error: found a word in a word: "+parser.getLocation();
					writeEvent(event);
					break;

				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();

					if (localname == "w" && pending) // we've found a strong punct in a word
					{
						IN = false;
						pending = false; // reset pending
						writeEvent(event);
						writer.writeEndElement();
					} else {
						writeEvent(event);
					}

					if (localname == "w") {
						//	println "end of word: "+parser.getLocation().getLineNumber();
						return; // we've finish to write the word
					}

					break;

				case XMLStreamConstants.CHARACTERS: // detect end of sentence
					String txt = parser.getText();
					if (txt.matches(strongPunct)) {
						pending = true;
					}
					writeEvent(event);
					break;
				case XMLStreamConstants.COMMENT:
					writer.writeComment(parser.getText());
					break;
			}
		}
	}

	/**
	 * Process out.
	 *
	 * @param event the event
	 */
	public void processOut(int event)
	{
		if (DEBUG) writer.writeComment("OUT");
		switch (event) {
			case XMLStreamConstants.START_ELEMENT:
				localname = parser.getLocalName();
				if (localname == "s") {
					IN = true;
					writeEvent(event);
					processContentExistingSentence();
				} else if (localname == "w") {
					IN = true;
					writer.writeStartElement("s");
					writer.writeAttribute("id", "s_"+s_id++)
					writeEvent(event);
					processWordContent();
				} else if (flow_tags.contains(localname)) {
					String rend = getRendAttribute();
					if (!rend.contains(rendMulti)) {
						IN = true;
						writer.writeStartElement("s");
						writer.writeAttribute("id", "s_"+s_id++)
					} else {
						multi = true;
					}
					writeEvent(event);
				} else if (div_tags.contains(localname)) // found a tag out of sentences, we do nothing
				{
					writeEvent(event);
				} else {
					writeEvent(event);
				}
				break;

			case XMLStreamConstants.END_ELEMENT:
				localname = parser.getLocalName();
				if (localname == "s") {
					System.out.println("Error: found a closing s but we are OUT");
				}
				writeEvent(event);
				break;

			case XMLStreamConstants.CHARACTERS:
				writeEvent(event);
				break;
			case XMLStreamConstants.COMMENT:
				writer.writeComment(parser.getText());
				break
		}
	}

	public String getRendAttribute()
	{
		for (int i = 0 ; i < parser.getAttributeCount() ; i++)
			if (parser.getAttributeLocalName(i) == "rend")
				return parser.getAttributeValue(i);
		return "";
	}

	/**
	 * Process xml in a created sentence.
	 *
	 * @param event the event
	 */
	public void processIn(int event)
	{
		if(DEBUG) writer.writeComment("IN")
		switch (event) {
			case XMLStreamConstants.START_ELEMENT:
				localname = parser.getLocalName();
				if (localname == "s") // found opening s, we close ours and stay IN
				{
					writer.writeEndElement(); // close our sentence
					writeEvent(event); // write the start element
					processContentExistingSentence(); // process its content
					//println "ERROR "+parser.getLocation();
				} else if (localname == "w") // can't close s from here, see END_ELEMENT
				{
					writeEvent(event);
					processWordContent();
				} else if (flow_tags.contains(localname)) // flow tag = we stay in the sentence
				{
					String rend = getRendAttribute();
					if (rend.contains(rendMulti)) // but this one must be seens as a div tag
					{
						writer.writeEndElement(); // close s
						IN = false; // get out of the sentence
						multi = true;
					}
					writeEvent(event);
				} else if (div_tags.contains(localname)) // div tag = we must get out the sentence
				{
					writer.writeEndElement(); // close s
					IN = false; // get out of the sentence
					writeEvent(event);
				} else {
					writeEvent(event);
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				localname = parser.getLocalName();
				if (localname == "s") // the sentence seems to already exists, we write the event and get OUT
				{
					IN = false;
					writeEvent(event);
				} else if (flow_tags.contains(localname)) // closing flow tag = we do nothing
				{
					if (multi)
						println "end of multi?"
					writeEvent(event);
				} else if (div_tags.contains(localname)) // closing div tag = we close the current sentence
				{
					//println "closing s because of "+localname+" "+parser.getLocation();
					writer.writeEndElement(); // close s
					IN = false;
					writeEvent(event);
				} else {
					writeEvent(event);
				}
				break;

			case XMLStreamConstants.CHARACTERS: // detect end of sentence
				writeEvent(event);
				break;
			case XMLStreamConstants.COMMENT:
				writer.writeComment(parser.getText());
				break
		}
	}

	/** The prefix. */
	String prefix;

	/**
	 * write the current event.
	 *
	 * @param event the stax event
	 */
	private void writeEvent(int event)
	{
		prefix = parser.getPrefix();
		if (event == XMLStreamConstants.START_ELEMENT )
		{
			localname = parser.getLocalName();
			if (prefix != null && prefix.length() > 0)
				writer.writeStartElement(prefix+":"+localname);
			else
				writer.writeStartElement(localname);
			String attrprefix;
			for (int i = 0 ; i < parser.getAttributeCount() ; i++)
			{
				attrprefix = parser.getAttributePrefix(i);
				if (attrprefix != null && attrprefix.length() > 0)
					writer.writeAttribute(attrprefix+":"+parser.getAttributeLocalName(i), parser.getAttributeValue(i))
				else
					writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i))
			}
			//writer.writeCharacters("\n");
		} else if (event == XMLStreamConstants.END_ELEMENT)
		{
			writer.writeEndElement();
			//writer.writeCharacters("\n");
		} else if (event == XMLStreamConstants.CHARACTERS)
		{
			writer.writeCharacters(parser.getText());
		} else if (event == XMLStreamConstants.COMMENT)
		{
			writer.writeComment(parser.getText());
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File ("/home/mdecorde/TXM/corpora/alceste/tokenized")
		File outdir = new File ("/home/mdecorde/TXM/corpora/alceste/tokenized/s")

		def files = dir.listFiles();//scan directory split

		println("Sentencing "+files.size()+" files")
		for (File infile : files) {
			print ".";
			File result = new File(outdir, infile.getName())
			TagSentencesXml sentencer = new TagSentencesXml(infile);
			if (!sentencer.process(result)) {
				println "error with: "+infile
				return;
			}

			//validation
			if (!ValidateXml.test(result)) {
				println "validation failed with: "+infile
				return;
			}
		}
		println "done"

	}
}
