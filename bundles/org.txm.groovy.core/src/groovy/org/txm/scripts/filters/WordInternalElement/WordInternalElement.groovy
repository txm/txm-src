// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.WordInternalElement;

import org.txm.importer.scripts.filters.*;
import java.util.regex.*;
import org.txm.tokenizer.TokenizerClasses;

// TODO: Auto-generated Javadoc
/**
 * The Class WordInternalElement.
 */
class WordInternalElement extends Filter {
	
	/** The counter. */
	int counter;
	
	/** The old. */
	def old;
	
	/** The corr_tags_no_seg. */
	String corr_tags_no_seg = new TokenizerClasses().corr_tags_no_seg;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args)
	{
		try
		{
			corr_tags_no_seg = args.get("corr_tags_no_seg");

		}
		catch(Exception e)
		{
			System.err.println(e);
			System.err.println("wordinternal needs 1 Map with arg  :\n corr_tags_no_seg")
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before()
	{
		counter = 0;
		println "begin wordinternal \n";
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after()
	{
		println "Deleted $counter wordinternalspaces \n";
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
		def m;
		def segment;
		// Write your code here, but don't forget to write in the output
		// ex : output.write("TheStringToWrite " + line );
		// in the variable "line" is the current line value
		old = line;
		line = (line =~ /(<(corr_tags_no_seg) [^>]*word_part[^>]*>)\s*([^<]*?)\s*(<\/\2>)/).replaceAll("<seg type=\"word_part\">\$1\$3\$4<\\/seg>");
		
		if( old != line)
			counter++;
		
		output.write(line+lineSeparator);
		// End
	}
}
