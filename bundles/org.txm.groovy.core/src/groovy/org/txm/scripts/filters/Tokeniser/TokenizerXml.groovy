// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.Tokeniser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.txm.objects.Project;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.xml.stream.*;
import java.net.URL;
import org.txm.tokenizer.TokenizerClasses;

// TODO: Auto-generated Javadoc
/**
 * The Class TokenizerXml.
 */
public class TokenizerXml {
	
	TokenizerClasses tc = new TokenizerClasses();
		
	/** The ignorable_tags. */
	String ignorable_tags = "note"
	
	/** The outfile. */
	File outfile;
	
	/** The infile. */
	File infile;
	
	/** The writer. */
	XMLStreamWriter writer;
	FileOutputStream output;
	
	/** The parser. */
	XMLStreamReader parser
	
	/** The localname. */
	String localname;
	
	/** The prefix. */
	String prefix;
	
	/**
	 * Instantiates a new tokenizer xml.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 */
	public TokenizerXml(File infile, File outfile)
	{
		this.outfile = outfile;
		this.infile = infile;
	}
	
	/**
	 * Fill infos.
	 *
	 * @param event the event
	 * @return the java.lang. object
	 */
	public fillInfos(int event)
	{
		if (event == XMLStreamConstants.START_ELEMENT || event == XMLStreamConstants.END_ELEMENT) {
			localname = parser.getLocalName();
			prefix = parser.getPrefix();
		}
	}
	
	/**
	 * Donothing.
	 *
	 * @param event the event
	 * @return the java.lang. object
	 */
	public donothing(int event)
	{
		if (event == XMLStreamConstants.START_ELEMENT ) {
			if (prefix != null && prefix.length() > 0)
				writer.writeStartElement(prefix+":"+localname);
			else
				writer.writeStartElement(localname);
			String attrprefix;
			for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
				attrprefix = parser.getAttributePrefix(i);
				if(attrprefix != null && attrprefix.length() > 0)
					writer.writeAttribute(attrprefix+":"+parser.getAttributeLocalName(i), parser.getAttributeValue(i))
				else
					writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i))
			}
		} else if(event == XMLStreamConstants.END_ELEMENT) {
			writer.writeEndElement();
			writer.writeCharacters("\n");
		} else if(event == XMLStreamConstants.CHARACTERS) {
			writer.writeCharacters(parser.getText());
		}
	}
	
	/**
	 * Process.
	 */
	public void process()
	{
		boolean ignorecontent = true;//tokenize a partir de <body>
		int wordcount = 0;
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		output = new FileOutputStream(outfile)
		writer = factory.createXMLStreamWriter(output, "UTF-8")
		
		def inputData = infile.toURI().toURL().openStream();
		def inputfactory = XMLInputFactory.newInstance();
		parser = inputfactory.createXMLStreamReader(inputData);
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			fillInfos(event);
			if (event == XMLStreamConstants.START_ELEMENT) {
				//println "Open: "+localname;
				if (localname == "text") {
					ignorecontent = false;
					donothing(event);
				}
				if (localname.matches(tc.word_tags)) {
					//println "Found pretagged word";
					donothing(event);
					wordcount++;
					if(parser.getAttributeValue(null, "id") == null)
						writer.writeAttribute("id", "w_"+wordcount);
					if(parser.getAttributeValue(null, "type") == null)
						writer.writeAttribute("type","w");
					ignorecontent = true;
				}
				else if(localname.matches(ignorable_tags))
				{
					//println "Found note";
					donothing(event);
					ignorecontent = true;
				}
				else
				{
					donothing(event);
				}
			}
			else if(event == XMLStreamConstants.END_ELEMENT) 
			{
				//println "Close: "+localname;
				if(localname == "w")
				{
					ignorecontent = false;
					writer.writeEndElement();
					writer.writeCharacters("\n");
				}
				else if(localname == "note")
				{
					ignorecontent = false;
					writer.writeEndElement();
					writer.writeCharacters("\n");
				}
				else
				{
					donothing(event);
				}
			}
			else if(event == XMLStreamConstants.CHARACTERS) 
			{
				if(ignorecontent)
				{	
					//println " ignore chars: "+parser.getText().trim();
					donothing(event);
				}
				else
				{
					//println " chars: "+parser.getText().trim();
					String text = parser.getText().trim().replace("\t"," ");
					text = text.replaceAll("\\p{C}", "");
					for(String s : tokenize(text))
					{
						if (s.matches(/\.[^ .]+\./)) {
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeAttribute("type","num");
							writer.writeCharacters(s);
							writer.writeEndElement();
							writer.writeCharacters("\n");
						} else if(s.matches(/${tc.punct_all}|['‘’]/))
										{
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeAttribute("type","pon");
							writer.writeCharacters(s);
							writer.writeEndElement();
							writer.writeCharacters("\n");
						}
						else if(s.matches(/[\[\(\{].*[\]\)\}]/))
						{
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeStartElement("supplied");
							writer.writeAttribute("rend","crochets");
							writer.writeCharacters(s);
							writer.writeEndElement();
							writer.writeEndElement();
							writer.writeCharacters("\n");
						}
						else if(s.contains("'") || s.contains("’"))
						{
							int sep = s.indexOf("'");
							if(sep < 0)
								sep = s.indexOf("’");

							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeCharacters(s.substring(0,sep+1));
							writer.writeEndElement();
							writer.writeCharacters("\n");
							
							if(s.substring(sep+1).length() > 0)
							{
								wordcount++;	
								writer.writeStartElement("w");
								writer.writeAttribute("id","w_"+wordcount);
								writer.writeCharacters(s.substring(sep+1));
								writer.writeEndElement();
								writer.writeCharacters("\n");
							}
						}
						else if(s.matches(/(${tc.punct_all})(.*)/))
						{
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeAttribute("type","pon");
							writer.writeCharacters(s.substring(0, 1));
							writer.writeEndElement();
							writer.writeCharacters("\n");
							
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeCharacters(s.substring(1));
							writer.writeEndElement();
							writer.writeCharacters("\n");
						}
						else if(s.matches(/(.*)(${tc.punct_all})/))
						{
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeCharacters(s.substring(0, s.length()-1));
							writer.writeEndElement();
							writer.writeCharacters("\n");
							
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeAttribute("type","pon");
							writer.writeCharacters(s.substring(s.length()-1, s.length()));
							writer.writeEndElement();
							writer.writeCharacters("\n");
						} else {
							wordcount++;	
							writer.writeStartElement("w");
							writer.writeAttribute("id","w_"+wordcount);
							writer.writeCharacters(s);
							writer.writeEndElement();
							writer.writeCharacters("\n");
						}
					}
				}
			}
			else if(event == XMLStreamConstants.COMMENT) 
			{
				writer.writeComment(parser.getText())
			}
			else if(event == XMLStreamConstants.DTD) 
			{
				//println "DTD!";
			}
			else
			{
				
			}
		}
		
		if (writer != null) writer.close();
			if (output != null) output.close();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
	}
	
	/**
	 * Tokenize.
	 *
	 * @param str the str
	 * @return the list
	 */
	public List<String> tokenize(String str)
	{
		return str.tokenize()//cut by whitespace
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File infile = new File("~/xml/quete","qgraal_cm.xml");
		File outfile = new File("~/xml/quete","qgraal_cm-out.xml");
		TokenizerXml tokenizer = new TokenizerXml(infile, outfile)
		tokenizer.process();
	}
}
