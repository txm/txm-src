// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.TagSentences;

import java.util.regex.*

import org.txm.importer.scripts.filters.*
import org.txm.tokenizer.*

import org.txm.scripts.filters.CutHeader.*
import org.txm.scripts.filters.FusionHeader.*

// TODO: Auto-generated Javadoc
/**
 * The Class TagSentences.
 */
class TagSentences extends Filter {
	
	/** The counter. */
	int counter;
	
	/** The m. */
	def m;
	
	/** The segment. */
	def segment;
	
	/** The linetype. */
	def linetype = "out";
	
	/** The open_corr_tags. */
	def open_corr_tags = new LinkedList<String>();
	
	/** The LAS topen_corr_tags. */
	def LASTopen_corr_tags = "";
	
	/** The open_div_tags. */
	def open_div_tags = new LinkedList<String>();
	
	/** The LAST open_div_tags. */
	def LASTopen_div_tags = "";
	
	/** The corr. */
	def corr = "no";
	
	/** The corr_name. */
	def corr_name;
	
	/** The corr_tag. */
	def corr_tag;
	
	/** The pending. */
	def pending = "no";
	
	/** The scounter. */
	def scounter = 0;
	
	/** The _before. */
	def _before;
	
	/** The _after. */
	def _after;
	
	/** The div_tags. */
	def div_tags;
	
	/** The q_tags. */
	def q_tags;
	
	/** The corr_tags_no_seg. */
	def corr_tags_no_seg;
	
	/** The corr_tags. */
	def corr_tags;
	
	def strongPunct;
	
	public TagSentences() {
		
		TokenizerClasses tc = new TokenizerClasses()
		/** The div_tags. */
		div_tags = tc.div_tags;
		
		/** The q_tags. */
		q_tags = tc.q_tags;
		
		/** The corr_tags_no_seg. */
		corr_tags_no_seg = tc.corr_tags_no_seg;
		
		/** The corr_tags. */
		corr_tags = tc.corr_tags;
		
		strongPunct = tc.punct_strong; // 
		
		reg_comment = /\A\s*<!--.*-->\s*\Z/
		reg_out_of_sentence = /^(.*)<s( [^>]*)?>(.*)$/
		reg_sentence_with_no_n_attribute = /<s( [^>]*)?>/
		reg_end_of_sentence = /^(.*)<\/s>(.*)$/
		reg_punct = /^(.*)<w type="pon"[^>]*>.*<\/w>(.*)$/
		reg_strong_punct = /^(.*)<w [^>]*>$strongPunct<\/w>(.*)/
		reg_word = /^(.*)<w .*<\/w>(.*)/
		reg_corr_tags_no_seg = /^\s*(<($corr_tags_no_seg)( [^>\/]*)?>)\s*$/
		reg_corr_tags_no_seg_alone = "<($corr_tags_no_seg)>"
		reg_corr_tags_no_seg2 = /^(.*)<\/($corr_tags_no_seg)>(.*)$/
		reg_block_tags = /^(.*)<\/($div_tags|$q_tags)>(.*)$/
		reg_block_tag_alone = "<($div_tags|$q_tags)>"
	}
	
	public TagSentences(TokenizerClasses tc) {
		
		/** The div_tags. */
		div_tags = tc.div_tags;
		
		/** The q_tags. */
		q_tags = tc.q_tags;
		
		/** The corr_tags_no_seg. */
		corr_tags_no_seg = tc.corr_tags_no_seg;
		
		/** The corr_tags. */
		corr_tags = tc.corr_tags;
		
		strongPunct = tc.punct_strong;
		
		reg_comment = /\A\s*<!--.*-->\s*\Z/
		reg_out_of_sentence = /^(.*)<s( [^>]*)?>(.*)$/
		reg_sentence_with_no_n_attribute = /<s( [^>]*)?>/
		reg_end_of_sentence = /^(.*)<\/s>(.*)$/
		reg_punct = /^(.*)<w type="pon"[^>]*>.*<\/w>(.*)$/
		reg_strong_punct = /^(.*)<w [^>]*>$strongPunct<\/w>(.*)/
		reg_word = /^(.*)<w .*<\/w>(.*)/
		reg_corr_tags_no_seg = /^\s*(<($corr_tags_no_seg)( [^>\/]*)?>)\s*$/
		reg_corr_tags_no_seg_alone = "<($corr_tags_no_seg)>"
		reg_corr_tags_no_seg2 = /^(.*)<\/($corr_tags_no_seg)>(.*)$/
		reg_block_tags = /^(.*)<\/($div_tags|$q_tags)>(.*)$/
		reg_block_tag_alone = "<($div_tags|$q_tags)>"
		
		//tc.dump()
		//println "reg_punct=$reg_punct reg_strong_punct=$reg_strong_punct"
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {
		try {
			div_tags = args.get("div_tags");
			q_tags = args.get("q_tags");
			corr_tags_no_seg = args.get("corr_tags_no_seg");
			corr_tags = args.get("corr_tags");
		}
		catch(Exception e) {
			System.err.println(e);
			System.err.println("TagginSentences needs 1 Map with args  :\n div_tags, q_tags, extraword_tags, " +
					"corr_tags_no_seg, corr_tags, word_tags, intraword_tags")
		}
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		// System.out.println("begin sentences");
	}
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after() {
		// System.out.println("end sentences");
	}
	
	def reg_empty_test = /\S/
	
	/**
	 * Test before after.
	 *
	 * @return true, if successful
	 */
	boolean testBeforeAfter() {
		if (_before ==~ reg_empty_test || _after ==~ reg_empty_test) {
			print "TagSentences : ERROR: $line";
			return false;
		}
		return true;
	}
	
	/**
	 * Affect13.
	 *
	 * @param m the m
	 */
	void affect13(def m) {
		_before = m[0][1];
		_after = m[0][3];
		testBeforeAfter();
	}
	
	/**
	 * Affect12.
	 *
	 * @param m the m
	 */
	void affect12(def m) {
		_before = m[0][1];
		_after = m[0][2];
		testBeforeAfter();
	}
	
	/** The MINPRINT. */
	int MINPRINT = 0
	
	/** The MAXPRINT. */
	int MAXPRINT = 0
	
	def reg_comment = /\A\s*<!--.*-->\s*\Z/
	def reg_out_of_sentence = /^(.*)<s( [^>]*)?>(.*)$/
	def reg_sentence_with_no_n_attribute = /<s( [^>]*)?>/
	def reg_end_of_sentence = /^(.*)<\/s>(.*)$/
	def reg_punct = /^(.*)<w type="pon"[^>]*>.*<\/w>(.*)$/
	def reg_strong_punct = /^(.*)<w [^>]*>$strongPunct<\/w>(.*)/
	def reg_word = /^(.*)<w .*<\/w>(.*)/
	def reg_corr_tags_no_seg = /^\s*(<($corr_tags_no_seg)( [^>\/]*)?>)\s*$/
	def reg_corr_tags_no_seg_alone = "<($corr_tags_no_seg)>"
	def reg_corr_tags_no_seg2 = /^(.*)<\/($corr_tags_no_seg)>(.*)$/
	def reg_block_tags = /^(.*)<\/($div_tags|$q_tags)>(.*)$/
	def reg_block_tag_alone = "<($div_tags|$q_tags)>"
	
	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter() {
		String tag;
		// Write your code here, but don't forget to write in the output
		// ex : output.print("TheStringToWrite " + line );
		// in the var line is the current line
		//line = line.trim()
		
		if (scounter > MINPRINT && scounter < MAXPRINT)
			println linetype+"LINE : "+line;
		
		if (line ==~ reg_comment) {		
			output.print(line);
		}
		else if (linetype == "out") { 
			//on est en dehors d'une phrase
			if ( (m = line =~ reg_out_of_sentence) ) { 
				//on trouve une balise de phrase => on met à jour le numéro, on est dans une phrase
				affect13(m);
				if (pending == "yes") {
					output.write("</s>\n")
					pending = "no";
				}
				
				scounter++;
				line = (line =~ reg_sentence_with_no_n_attribute).replaceAll("<s n=\"$scounter\">");
				output.print(line+"\n");	
				linetype = "in";
			}
			else if( (m = line =~ reg_end_of_sentence) ) { ////on trouve une balise de phrase fermante
				//println "found </s> "+line
				affect12(m);
				if (pending == "yes") {
					linetype = "out";
					pending = "no";
					//pending = "no";
					output.print(line);
				}
				else
				{
					//output.print(line);
					System.err.println("Found </s>, but pending = no and linetype= out : "+line+" ; scount $scounter");
				}
			}
			else if ( (	m = line =~ reg_punct)  && pending== "yes") { 
				// on trouve une ponctuation
				// et la balise de la phrase précédente n'est pas fermée => on ne fait rien
							//println "found w type pon"
				affect12(m);
				output.print(line);
			}
			
			else if( (m = line =~ reg_word) ) //on rencontre un mot...
			{ 	
				//println "found <w> open a sentence"
				affect12(m);
				if(pending == "yes")
				{
					output.write("</s>\n")
					pending = "no";
				}
				scounter++;
				output.print("<s n=\"$scounter\">\n"+line);
				linetype = "in";
			}
			else if( (m = line =~ reg_corr_tags_no_seg) 
			|| line.matches(reg_corr_tags_no_seg_alone))
			{ 
				if(pending == "yes")
				{
					output.write("</s>\n")
					pending = "no";
				}
				//System.out.println("OUT: found open corr "+line);
				//on trouve une balise de correction ouvrante
				def tag_name = m[0][2];
				def tag_end = m[0][3];
				
				if (tag_end != null && tag_end.contains("multi_s")) // traite le corr comme un div tag
				{ 
					open_div_tags.push(tag_name);
					LASTopen_div_tags = tag_name;
				} else  { 
					open_corr_tags.push(tag_name);

					scounter++;
					output.print("<s n=\"$scounter\">\n"); //on ouvre une <s>
					linetype = "in";
				}
				if (scounter > MINPRINT && scounter < MAXPRINT) {
					println "stacks $open_corr_tags ; $open_div_tags"
				}
				output.print(line);
			} else if( ( m = line =~ reg_corr_tags_no_seg2) ) 
			{ 
				//on trouve la balise fermante correspondante à la dernière correction ouverte	
						affect13(m);
				if (scounter > MINPRINT && scounter < MAXPRINT)
					println "closing corr tag "+line
				
				if (open_corr_tags.size() == 0 )//il n'y a plus de simple balise de corr
				{	
					open_div_tags.pop();
					if(open_div_tags.size() == 0)
					{
						if (pending == "yes") {
							output.print("</s>\n"+line);// comme une div tag
							pending = "no";
						} else {
							output.print(line);// comme une div tag
						}
					} else {
						output.print(line);// comme une div tag
					}
				} else {	
					open_corr_tags.pop();
					
					if (pending == "yes") {
						if (open_corr_tags.size() == 0) {
							output.print("</s>\n");
							output.print(line);
							linetype = "out";
							pending = "no";
						} else {
							output.print(line);
						}
					} else {
						output.print(line);
					}
				}
				
				if( scounter > MINPRINT && scounter < MAXPRINT) {
					println "stacks "+open_corr_tags+" ; "+open_div_tags
				}
			} else if( (	m = line =~ reg_block_tags) ||
						line.matches(reg_block_tag_alone)) { ////on trouve une balise de citation ou division fermante
				//println "found closing div|quote tag "+line
				//affect13(m);
				
				if (pending == "yes") {	//println "a sentence was closed"
					output.print("</s>\n$line");
					pending = "no";
				} else {
					//println "was NOT in sentence"
					output.print(line);
				}
			}
			else if( (m = line =~ /^(.*)<($div_tags|$q_tags)>(.*)$/) ||
			(m = line =~ /^(.*)<($div_tags|$q_tags)( [^>]*)?>(.*)$/) ||
			line.matches("<($div_tags|$q_tags)( [^>]*)?>")) { //balise div ouvrante
				
				//pending = "no";
				//push @s_errors, scounter;
				if (pending == "yes") {
					output.print("</s>\n");
					pending = "no";
				}
				
				output.print("$line");
			}	 else {	//println "ELSE de 'out'"
				output.print(line);
			}
		}
		else if (linetype == "in") //on est à l'intérieur d'une phrase
				{ 	//println "in sentence"
			
				if ( ( m = line =~ reg_strong_punct) ) { ////on trouve une ponctuation forte ==> on est à l'extérieur d'une phrase
						//println "found word .!? "+line
				affect12(m);
				
				if (open_corr_tags.size() == 0) // il n'y a pas de correction en cours
				{
					linetype = "out";
					pending ="yes"
					output.print(line);
				}
				else //on ne ferme pas desuite la phrase mais apres les corr
				{
					pending = "yes"
					output.print(line);
				}
			} else if( (m = line =~ reg_word) || (m = line =~ /^(.*)<w( [^>]*)?>.*<\/w>(.*)/) ) //on rencontre un mot...
			{ 	
				//println "found <w> open a sentence"
				affect12(m);
				output.print(line);
				
			}
			else if( (m = line =~ reg_corr_tags_no_seg) 
			|| line.matches(reg_corr_tags_no_seg_alone))
			{ 
				if(scounter > MINPRINT && scounter < MAXPRINT)
					System.out.println("open corr "+line);
				//on trouve une balise de correction ouvrante
				def tag_name = m[0][2];
				def tag_end = m[0][3];
				
				if (tag_end != null && tag_end.contains("multi_s")) // traite le corr comme un div tag
				{ 
					open_div_tags.push(tag_name);
					LASTopen_div_tags = tag_name;
					output.print("</s>\n");
					linetype="out"
				}
				else 
				{ 
					open_corr_tags.push(tag_name);
				}
				if (scounter > MINPRINT && scounter < MAXPRINT) {
					println "stacks "+open_corr_tags+" ; "+open_div_tags
				}
				output.print(line);
			}
			else if( ( m = line =~ reg_corr_tags_no_seg2) ) 
			{ 
				//on trouve la balise fermante correspondante à la dernière correction ouverte	
				if (scounter > MINPRINT && scounter < MAXPRINT)
					System.out.println("closing corr "+line);
				affect13(m);
				
				if (open_corr_tags.size() == 0 )//il n'y a plus de simple balise de corr
				{	
					if (open_div_tags.size() > 1) {
						output.print("</s>\n"+line);// comme une div tag
						linetype = "out";
						open_div_tags.pop();
					} else {
						println("error : found a non classified corr : "+line);
					}
				} else {	
					open_corr_tags.pop();
					output.print(line);
					if (pending == "yes" && open_corr_tags.size() == 0) // c'était le dernier corr
					{
						output.print("</s>\n");
						linetype = "out";
						pending = "no";
					}
				}
				if (scounter > MINPRINT && scounter < MAXPRINT) {
					println "stacks "+open_corr_tags+" ; "+open_div_tags
				}
			}
			else if( (m = line =~ reg_out_of_sentence) ) { ////on trouve une balise de phrase fermante
				//println "found </s> "+line
				affect12(m);
				
				linetype = "out";
				//pending = "no";
				output.print(line);
			}
			else if( (m = line =~ reg_block_tags) ||
			line.matches("</($div_tags|$q_tags)>")) { ////on trouve une balise de division ou de citation fermante ==> on ferme une </s>
				//println "found div or quote closing tag "+line
				//affect13(m);
				
				linetype = "out";
				//pending = "no";
				//push @s_errors, scounter;
				output.print("</s>\n$line");
			}
			else if( (m = line =~ /^(.*)<($div_tags|$q_tags)>(.*)$/) ||
			(m = line =~ /^(.*)<($div_tags|$q_tags)( [^>]*)?>(.*)$/) ||
			line.matches("<($div_tags|$q_tags)( [^>]*)?>")) { ////on trouve une balise de division ou de citation fermante ==> on ferme une </s>
				//println "found div or quote closing tag "+line
				//affect13(m);

				//push @s_errors, scounter;
				output.print("</s>\n")
				output.print("$line");
				output.print("<s n=\""+scounter+"\">\n"); //on ouvre une <s>
				
			}		
			else {
				if (scounter > MINPRINT && scounter < MAXPRINT)
					println "ELSE "+line
				
				output.print(line);
			}
		}
		else {
			print "Error in sentence tagging : $line";
			System.err.println("Error in sentence tagging : $line");
		}
		// End
	}
}