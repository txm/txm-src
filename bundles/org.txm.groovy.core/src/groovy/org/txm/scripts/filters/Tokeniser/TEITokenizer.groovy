package org.txm.scripts.filters.Tokeniser;

import java.net.URL;
import java.util.regex.Pattern;

import org.txm.importer.StaxIdentityParser;
import org.txm.tokenizer.TokenizerClasses;

public class TEITokenizer extends StaxIdentityParser {

	int DEBUG = 0;

	enum STATES { WORD, TEXT, IGNORE, NOTE };
	STATES state = STATES.IGNORE;
	String STARTTAG = ".*"
	String NOTETAG = ""

	static String PONTEST = "[.;:,!?]";
	static String TYPE = "type";
	static String ID = "id"
	static String PON = "pon"
	static String NUM = "num"
	static String ENDOFLINE = System.getProperty("line.separator");
	def tests;

	int firstElement = 1;
	String filename = "";
	int wordcount = 0;
	String previousElem;

	def word_tags;
	def intraword_tags;
	def punct_strong;
	def punct_all;
	def word_chars;
	def punct_quotes;

	StringBuffer buffer = new StringBuffer();

	public TEITokenizer(URL inputurl) {
		super(inputurl);

		this.filename = new File(inputurl.getFile()).getName();
		int index = filename.lastIndexOf(".");
		if (index > 0) {
			filename = filename.substring(0, index);
		}
		
		TokenizerClasses tc = new TokenizerClasses()
		tests = tc.tests;
			
		word_tags = tc.word_tags;
		intraword_tags = tc.intraword_tags;
		punct_strong = tc.punct_strong;
		punct_all = tc.punct_all;
		word_chars = tc.word_chars;
		punct_quotes = tc.punct_quotes;
	}

	protected void processBuffer() {
		if (buffer.size() > 0) {
			tokenize(buffer.toString());
			buffer = new StringBuffer();
		}
	}

	protected void processStartElement() {
		//println "OPEN "+localname+" = "+buffer.toString()
		if ((firstElement++) == 1) {
			if (localname == "TEI") {
				STARTTAG = "text"
				NOTETAG = "note"
			} else 
				STARTTAG = localname
		}
		processBuffer();
		switch (state) {
			case STATES.IGNORE:
				super.processStartElement();
				if (localname.matches(STARTTAG)) {
					//println "ENTER $STARTTAG"
					state = STATES.TEXT;
				}
				break;
			case STATES.TEXT:
				if (localname.matches(NOTETAG)) {
					super.processStartElement();
					//println "ENTER $NOTETAG"
					state = STATES.NOTE;
				} else if (localname.matches(word_tags)) {
					//println "ENTER $WORDTAG"
					state = STATES.WORD;
					processStartWordElement();
				} else {
					super.processStartElement();
					writer.writeCharacters(ENDOFLINE);
				}
				break;
			default: // note, comment
				super.processStartElement();
		}
		previousElem = localname;
	}

	protected void processEndElement() {
		//println "CLOSE "+localname+" = "+buffer.toString()
		processBuffer();
		switch (state) {
			case STATES.NOTE:
				super.processEndElement();
				if (localname.matches(NOTETAG)) {
					writer.writeCharacters(ENDOFLINE);
					//println "EXIT $NOTETAG"
					state = STATES.TEXT;
				}
				break;
			case STATES.WORD:
				super.processEndElement();
				writer.writeCharacters(ENDOFLINE);
				if (localname.matches(word_tags)) {
					//println "EXIT $WORDTAG"
					state = STATES.TEXT;
				}
				break;
			case STATES.TEXT:
				super.processEndElement();
				writer.writeCharacters(ENDOFLINE);
				if (localname.matches(STARTTAG)) {
					//println "EXIT $STARTTAG"
					state = STATES.IGNORE;
				}
				break;
			default: // note, comment
				super.processEndElement();
		}
		previousElem = localname;
	}

	protected void processStartWordElement() {
		super.processStartElement();
		wordcount++;
	}

	protected void tokenize(String text) {
		if (DEBUG > 0) {println "buffer="+text}
		text = text.replaceAll("\n", " ");
		text = text.replaceAll("\\p{C}", "");						// remove ctrl characters
		for (String s : text.split("[\\p{Z}\\p{C}]+") ) {
			s = s.trim();
			iterate(s);
		}
	}

	/**
	 * Iterate.
	 *
	 * @param s the s
	 * @return the java.lang. object
	 */
	protected iterate(String s)
	{
		while (s != null && s.length() > 0) {
			if (DEBUG > 0){println "  > $s"}
			s = standardChecks(s);
		}
	}

	/**
	 * Standard checks.
	 *
	 * @param s the s
	 * @return the java.lang. object
	 */
	protected standardChecks(String s)
	{
		if (DEBUG > 0){println "checks : "+s}
		def m;

		for (TTest test : tests) {
			if ((m = s =~ test.regex)) {
				if (DEBUG > 0){println "test : "+test.regex}
				if (test.before > 0)
					iterate(m[0][test.before])

				if (test.hit > 0) {
					DEBUG--
					wordcount++;
					writer.writeStartElement("w");
					writeWordAttributes();// id
					writer.writeAttribute("type",test.type);
					writer.writeCharacters(m[0][test.hit]);
					writer.writeEndElement();
					writer.writeCharacters("\n");
				}
				if( test.after > 0)
					return m[0][test.after];
			}
		}

		if (DEBUG > 0) {println "Other : "+s}
		//		def o = s.split("[\\p{Z}\\p{C}]+")
		//		for (String sub : o) {
		//			if (sub.length() == 0) continue
		wordcount++;
		writer.writeStartElement("w");
		writeWordAttributes();// id
		if (s.matches(/\p{P}/))
			writer.writeAttribute("type","pon");
		else
			writer.writeAttribute("type","w");
		writer.writeCharacters(s);
		writer.writeEndElement();
		writer.writeCharacters("\n");
		//		}
		return "";


	}

	/**
	 * Write word attributes.
	 *
	 * @return the java.lang. object
	 */
	protected writeWordAttributes()
	{
		writer.writeAttribute("id", "w_"+filename+"_"+wordcount);
		writer.writeAttribute("n",""+wordcount);
	}

	protected void processCharacters()
	{
		//println "TEXT: "+parser.getText()
		switch (state) {
			case STATES.TEXT:
				buffer.append(parser.getText())
			//tokenize();
				break
			default:
				super.processCharacters();
		}
	}

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		File dir = new File("/home/mdecorde/xml/txmrefman")
		File infile = new File(dir, "TEI/refman.xml");
		File outfile = new File(dir, "tokenization/refman-t3.xml");

		TEITokenizer tokenizer = new TEITokenizer(infile.toURI().toURL());

		if (tokenizer.process(outfile))
			println "Done - OK"
		else
			println "Done - FAIL"

		println "time: "+(System.currentTimeMillis()-start)/1000

		for (File f : new File("/home/mdecorde/xml/txmrefman/tokenization").listFiles()) {
			String text = f.getText("UTF-8")
			f.withWriter("UTF-8") { writer ->
				text = text.replaceAll(/ id="w_refman_.+" n="[^"]+"/, "")
				text = text.replaceAll("xml:id=", "id=")
				text = text.replaceAll("<w>", "<w type=\"w\">")
//				text = text.replaceAll("</?(p|body|item|text|table|list|ref|anchor|cell|row|head|div|term|hi|index|figure|graphic|lb|note).+", "")
//				text = text.replaceAll("\n[^<>\n]+\n", "\n")
//				text = text.replaceAll("\n[^<>\n]+\n", "\n")
//				text = text.replaceAll("\n[^<>\n]+\n", "\n")
//				text = text.replaceAll("\n++", "\n")
				writer.write(text)
			}
		}
	}
}
