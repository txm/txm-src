// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.OneTagPerLine;

//Pre-processing extra-word tags (1)
import org.txm.importer.scripts.filters.*;
import java.util.regex.*;
import org.txm.tokenizer.TokenizerClasses;

// TODO: Auto-generated Javadoc
/**
 * The Class OneTagPerLine.
 */
class OneTagPerLine extends Filter {
	
	/** The tag_all. */
	String tag_all = new TokenizerClasses().tag_all;
	
	/** The counterreg1. */
	int counterreg1 = 0;
	
	/** The counterreg2. */
	int counterreg2 = 0;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {

	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {

	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after() {

	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter()
	{
		def m;
		boolean firstTest = true;

		while(!(line ==~ /\A\s*\Z/))
		{
			if (( m = line =~ /^([^<]+)(.*)$/) )// trucs(<balise>
			{
				if(firstTest)
				{
					output.write(lineSeparator);
					firstTest=false;
				}
				output.write(m[0][1]);
				line = m[0][2];
			}
			else if ((m = line =~ /^($tag_all)(.*)$/)) {//
				firstTest=false;
				output.write("\n"+m[0][1]);
				line = m[0][2];
			}
			else
			{
				println "Error in "+ line;
				line = " ";
			}
		}
		// End
	}
}
