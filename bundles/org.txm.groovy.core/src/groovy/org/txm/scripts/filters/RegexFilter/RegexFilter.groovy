// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.RegexFilter;

import org.txm.importer.scripts.filters.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.txm.importer.scripts.filters.Filter;

// TODO: Auto-generated Javadoc
/** Filters out lines not matching a regexp. @author jmague */
public class RegexFilter extends Filter {
	private Pattern pattern;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {
		if (args.size() == 1) {
			pattern = Pattern.compile(args[0]);
		} else {
			System.out
					.println("Regexfilter needs 1 args :\nString:patternRegex");
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after() {
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		if (pattern == null) {
			System.out
					.println("Regex filter needs 1 args :\nString:patternRegex");
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter() {
		Matcher matcher = pattern.matcher(line);
		if (matcher.matches())
			output.write(line + lineSeparator);
	}
}
