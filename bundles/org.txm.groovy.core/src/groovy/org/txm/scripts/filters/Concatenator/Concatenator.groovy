// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.filters.Concatenator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import org.txm.importer.scripts.filters.*;

// TODO: Auto-generated Javadoc
/**
 * The Class Concatenator.
 */
class Concatenator extends Filter {
	
	/** The destination. */
	String destination;

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#before()
	 */
	boolean before() {
		if (destination == "") {
			println("You need to define destination file before launching this filter");
			return false;
		}
		def f = new File(destination);
		this.output = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");
		return true;
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#SetUsedParam(java.lang.Object)
	 */
	void SetUsedParam(Object args) {
		if (args.size() == 1) {
			destination = args[0];
		} else {
			System.out
					.println("Concatenator need 1 args : \nString:destinatione");
		}
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#after()
	 */
	void after() {
		System.out.println("Concatenator readed lines : " + linecounter);
		this.output.close();
	}

	/* (non-Javadoc)
	 * @see org.txm.importer.filters.Filter#filter()
	 */
	void filter() {
		output.write(line + lineSeparator);
		output.flush();
	}
}
