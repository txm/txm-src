/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.test

/**
 * How to use ProcessBuilder to wrap an external program
 * 
 * 
 * ======= Definition file =======
<?xml version="1.0" encoding="UTF-8"?>
<application name="Ls" version="0.0.0" desc="List directory">
	<progs>
		<prog exec="ls" version="0.0.0" desc="Ls">
			<args>
				<arg state="optional" type="none" name="l" desc="use a long listing format"/>
				<arg state="optional" type="none" name="a" desc="do not ignore entries starting with '.'"/>
				<arg state="optional" type="String" name="d" desc="list directory entries instead of contents"/>
			</args>
		</prog>
	</progs>
</application>
 * 
 */

/**
 * the file containing the declaration of the Ls program : ls-wrapper-definition.xml
 *
 */
def definitionFile = new File("~/workspace/org.txm.core/src/groovy/org/textometrie/scripts/ls-wrapper-definition.xml")

/**
 * The file which will contain the code : Ls.groovy
 */
def classFile = new File("~/Ls.groovy")

/**
 * the directory containing the binaries which will be called: /bin/
 */
def binariesDirectory = new File("/bin/")

/**
 * build wrapping code
 */
def aProgram = org.txm.utils.processbuilder.ProcessBuilder.wrapProgram(definitionFile, classFile, binariesDirectory)

/**
 * set call parameters
 */
aProgram.setl()
aProgram.seta()

/**
 * call the ls external program
 */
aProgram.ls()

