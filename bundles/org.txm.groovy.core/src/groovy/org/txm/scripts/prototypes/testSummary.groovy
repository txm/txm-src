package org.txm.scripts.test

import org.txm.Toolbox
import org.txm.searchengine.cqp.core.functions.summary.Summary
import org.txm.searchengine.cqp.corpus.*
import org.txm.functions.summary.*

def discours = CorpusManager.getCorpusManager().getCorpus("DISCOURS")
Summary summary = new Summary(discours);
if(summary.init())
	println "success";
else
	pritnln "fail";
