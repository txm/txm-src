package org.txm.scripts.test
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.cooccurrence.core.functions.Cooccurrence
import org.txm.cooccurrence.core.functions.comparators.OccComparator
import org.txm.functions.cooccurrences.*
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.stat.engine.r.data.*;
import org.txm.functions.cooccurrences.comparators.*;

/////////////////PARAMS/////////////////////

def corpusname = "DANS16FRANTEXT"
def PROP = "word"
def QUERY ="""
[type="expFound"]
"""

def minleft = 0
def maxleft = 10
def minright = 0
def maxright = 10

def minf = 1
def mincof = 1
def minscore = 0

File refFile = new File("/home/mdecorde/TEMP/lexiqueFrantext16.txt");
def file = new File("/home/mdecorde/TEMP/cooc_dans_ref.tsv")

///////////////////////////////////////////

minleft++
maxleft++
minright++
maxright++
if (!refFile.exists()) { println "ref file does not exists: $refFile"; return;}
QUERY = QUERY.trim();
MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus(corpusname)
Property property = corpus.getProperty(PROP)

// Build the reference corpus
println "building reference corpus."
def ref_lt = LexicalTableImpl.createLexicalTableImpl(refFile);

// Build the cooccurrences
def limit = null
def query = new Query(Query.fixQuery(QUERY))
print "building cooc"
def cooccurrence = new Cooccurrence(corpus, query, [property], limit, maxleft, minleft, minright, maxright, minf, mincof, minscore, false)
System.out.println("cooc: "+corpus+" "+query+" "+[property]+" "+null+" "+maxleft+" "+minleft+" "+minright+" "+maxright+" "+minf+" "+mincof+" "+minscore+" "+false);
print "."
if (refFile != null)
	cooccurrence.setReferenceCorpus(ref_lt.getSymbol()) // SET THE REFERENCE CORPUS
print "."
cooccurrence.stepQueryLimits();
print "."
cooccurrence.stepGetMatches();
print "."
cooccurrence.stepBuildSignatures();
print "."
cooccurrence.stepCount();
print "."
cooccurrence.stepBuildLexicalTable();
print "."
cooccurrence.stepGetScores();
print "."
cooccurrence.getLines();
cooccurrence.sort(new OccComparator())
println ""

println "exporting."
cooccurrence.toTxt(file, "UTF-8", "\t", "")
println "printed cooccurrents in "+file.getAbsolutePath()
