/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.test

import org.txm.Toolbox
import org.txm.ca.core.functions.CA
import org.txm.searchengine.cqp.clientExceptions.CqiClientException
import org.txm.searchengine.cqp.clientExceptions.InvalidCqpIdException
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.apache.commons.lang.time.StopWatch

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
def timer = new StopWatch()

		// on récupère le corpus VOEUX
		//EN: let's get the VOEUX corpus
		def voeux = CorpusManager.getCorpusManager().getCorpus("VOEUX")

timer.start()

		// on crée une partition sur les textes avec comme propriété discriminante l'id du texte
		//EN: let create a partition on texts with their ID's
		def text_su = voeux.getStructuralUnit("text")
		def text_id = text_su.getProperty("id")
		def textes = voeux.createPartition(text_su, text_id)
		
timer.stop()
println "Création de la partition : "+timer.toString()	
timer.reset()

		// choisir la propriété lexicale pour construire la table lexicale
		//EN: choose the lexical property to build the lexical table
		def prop = voeux.getProperty("lemme") 

timer.start()

		// on crée une table lexicale sans les hapax
		//EN: let create a lexical table without hapax legomenon
		def table = textes.getLexicalTable(prop, 2)
		
timer.stop()
println "Création de la table lexicale des lemmes : "+timer.toString()
timer.reset()		
	
timer.start()
		// on fait une afc
		//EN: compute the CA
		def ca = new CA(table)
		ca.compute();

timer.stop()
println "calcul de l'AFC 1 : "+timer.toString()		
timer.reset()

timer.start()

		// les valeurs propres
		//EN: singular values
		ca.getSingularValues()
		
timer.stop()
println "récupération des valeurs propres SV1 : "+timer.toString()	
timer.reset()	

timer.start()

		// les données de colonnes produites par le package CA de R
		//EN: column data produced by the CA R package
		ca.getColsCoords() 
		ca.getColsDist()
		ca.getColsInertia()
		ca.getColsMass()
		
timer.stop()
println "récupération de toutes les infos de colonnes1 : "+timer.toString()
timer.reset()

timer.start()

		// les données de lignes produites par le package CA de R
		//EN: row data produced by the CA R package
		ca.getRowsCoords()
		ca.getRowsDist()
		ca.getRowsInertia()
		ca.getRowsMass()
		
timer.stop()
println "récupération de toutes les infos de lignes1 : "+timer.toString()
timer.reset()

timer.start()

// on joue un peu avec la table EN: let's play with the table
table.cut(3000) 						// pas plus de 3000 valeurs, EN: not more than 3000 values
table.filter(table.getFreqs(), 2000, 5) // coupe a 2000 valeurs et Fmin=5, EN: cut to 2000 values and Fmin=5

timer.stop()
println "On filtre la table lexicale en fréquence et en nombre de lignes : "+timer.toString()
timer.reset()

timer.start()

// on fait une afc
//EN: we compute a CA
def ca2 = new CA(table)
ca2.compute();
timer.stop()
println "calcul de la deuxième AFC : "+timer.toString()		
timer.reset()

timer.start()

// les valeurs propres
//EN: singular values
ca2.getSingularValues()

timer.stop()
println "récupération des valeurs propres SV2 : "+timer.toString()	
timer.reset()	

timer.start()

// les données de colonnes produites par le package CA de R
//EN: column data produced by the CA R package
ca2.getColsCoords() 
ca2.getColsDist()
ca2.getColsInertia()
ca2.getColsMass()

timer.stop()
println "récupération de toutes les infos de colonnes2 : "+timer.toString()
timer.reset()

timer.start()

// les données de lignes produites par le package CA de R
//EN: row data produced by the CA R package
ca2.getRowsCoords()
ca2.getRowsDist()
ca2.getRowsInertia()
ca2.getRowsMass()

timer.stop()
println "récupération de toutes les infos de lignes2 : "+timer.toString()
timer.reset()