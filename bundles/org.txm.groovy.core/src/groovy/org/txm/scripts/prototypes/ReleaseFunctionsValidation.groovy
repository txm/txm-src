package org.txm.scripts.test
//import org.txm.*
//import org.txm.functions.*
//import org.txm.functions.ca.*
//import org.txm.functions.classification.*
//import org.txm.concordance.core.functions.*
//import org.txm.concordance.core.functions.comparators.*
//import org.txm.functions.cooccurrences.Cooccurrence
//import org.txm.searchengine.cqp.core.functions.information.*
//import org.txm.functions.index.*
//import org.txm.functions.referencer.Referencer
//import org.txm.functions.specificities.*
//import org.txm.searchengine.cqp.corpus.*
//import org.txm.searchengine.cqp.corpus.query.*
//import org.txm.stat.data.LexicalTable
//import org.txm.utils.DeleteDir
//
//File exporttestdir = new File(System.getProperty("user.home"), "TXM/testrelease");
//DeleteDir.deleteDirectory exporttestdir;
//exporttestdir.mkdir()
//println "Results are saved in dir: "+ exporttestdir
//
////diag corpus
//def corpora = CorpusManager.getCorpusManager().getCorpora()
//MainCorpus discours = CorpusManager.getCorpusManager().getCorpus("DISCOURS")
//println "CORPUS: "+discours
//
//// word properties
//def word_property = discours.getProperty("word")
//def pos_property = discours.getProperty("pos")
//def lemma_property = discours.getProperty("lemma")
//def func_property = discours.getProperty("func")
//println "props: $word_property, $pos_property, $lemma_property and $func_property"
//
//// structure properties
//StructuralUnit text_su = discours.getStructuralUnit("text")
//StructuralUnit s_su = discours.getStructuralUnit("s")
//println "structs: $text_su and $s_su"
//
//Property text_id_property = text_su.getProperty("id")
//Property text_type_property = text_su.getProperty("type")
//Property text_date_property = text_su.getProperty("date")
//Property text_loc_property = text_su.getProperty("loc")
//println "struct props: $text_id_property, $text_type_property, $text_date_property and $text_loc_property"
//ReferencePattern referencePattern = new ReferencePattern().addProperty(text_id_property)
//
//long time;
//
//// INFORMATIONS
//time = System.currentTimeMillis();
//Information diag = new Information(discours, 20)
//diag.stepGeneralInfos();
//diag.stepLexicalProperties();
//diag.stepStructuralUnits();
//diag.toHTML(new File(exporttestdir, "discours_diag"))
//println("Diag: "+(System.currentTimeMillis()-time)/1000)
//
//// LEXICON
//time = System.currentTimeMillis();
//discours.getLexicon(lemma_property).toTxt(new File(exporttestdir, "discours_lexpos"), "UTF-8", "\t", "");
//println("Lex: "+(System.currentTimeMillis()-time)/1000)
//
//// INDEX
//time = System.currentTimeMillis();
//Query query = new Query(Query.fixQuery("j.*"))
//IndexSample index = new IndexSample(discours, query, [lemma_property, func_property])
////index.sortLines(LineComparator.SortMode.FREQUNIT, true)
////index.filterLines(2, 3000)
//index.toTxt(new File(exporttestdir, "discours_indexlemmafuncj"), "UTF-8", "\t", "")
//println("Ind: "+(System.currentTimeMillis()-time)/1000)
//
//// REFERENCER
//time = System.currentTimeMillis();
//Referencer referencer = new Referencer(discours, query, lemma_property, [text_type_property], true);
//referencer.getQueryMatches()
//referencer.getQueryindexes()
//referencer.groupPositionsbyId()
//referencer.toTxt(new File(exporttestdir, "discours_referencer"), "UTF-8")
//println("Ref: "+(System.currentTimeMillis()-time)/1000)
//
//// CONCORDANCE
//time = System.currentTimeMillis();
//Concordance concordance = new Concordance(discours, query, word_property, [word_property, pos_property], referencePattern, referencePattern, 15, 15)
//LexicographicLeftContextComparator comparator = new LexicographicLeftContextComparator()
//comparator.initialize(discours)
//concordance.sort(comparator)
//concordance.setViewProperties([word_property])
//concordance.toTxt(new File(exporttestdir,"discours_concj"), Concordance.Format.CONCORDANCE)
//println("Conc: "+(System.currentTimeMillis()-time)/1000)
//
//// COOCCURRENCE WORD WINDOW
//time = System.currentTimeMillis();
//Cooccurrence cooc = new Cooccurrence(discours, query, [word_property], null, 21, 1, 1, 11, 2, 3, 1, false);
//cooc.process();
//cooc.toTxt(new File(exporttestdir, "discours_cooc_wordwindow"), "UTF-8")
//println("Cooc1: "+(System.currentTimeMillis()-time)/1000)
//
//// COOCCURRENCE SENTENCE WINDOW
//time = System.currentTimeMillis();
//Cooccurrence cooc2 = new Cooccurrence(discours, query, [word_property], s_su,2, 1, 1, 1, 2, 3,1, false);
//cooc2.process();
//cooc2.toTxt(new File(exporttestdir, "discours_cooc_swindow"), "UTF-8")
//println("Cooc2: "+(System.currentTimeMillis()-time)/1000)
//
//// SUBCORPORA
//time = System.currentTimeMillis();
//Corpus DGcorpus = discours.createSubcorpus(text_su, text_loc_property, "de Gaulle", "dgsubcorpus")
//println("sub: "+(System.currentTimeMillis()-time)/1000)
//
//// PARTITIONS
//time = System.currentTimeMillis();
//Partition discours_types = discours.createPartition(text_su, text_type_property)
//Partition discours_dates = discours.createPartition(text_su, text_date_property)
//println("partitions: "+(System.currentTimeMillis()-time)/1000)
//
//// LEXICAL TABLE
//time = System.currentTimeMillis();
//LexicalTable table = discours_types.getLexicalTable(word_property, 2);
//table.exportData(new File(exporttestdir, "discours_type_LT"), "\t", "");
//println("lex table: "+(System.currentTimeMillis()-time)/1000)
//
////// SPECIF PARTITION
////time = System.currentTimeMillis();
////SpecificitesResult specifresult = org.txm.functions.specificities.Specificites.specificites(discours_types, word_property, [], [], 1);
////specifresult.toTxt(new File(exporttestdir,"discours_speciftype"), "UTF-8", "\t", "")
////println("sepcif part: "+(System.currentTimeMillis()-time)/1000)
////
////// SPECIF SUBCORPUS
////time = System.currentTimeMillis();
////SpecificitesResult specifresult2 = org.txm.functions.specificities.Specificites.specificites(DGcorpus.getParent(), DGcorpus, word_property)
////specifresult2.toTxt(new File(exporttestdir,"discours_dgsub_specifloc"), "UTF-8", "\t", "")
////println("specif sub: "+(System.currentTimeMillis()-time)/1000)
////
////// SPECIF LEXICAL TABLE
////time = System.currentTimeMillis();
////SpecificitesResult specifresult3 = org.txm.functions.specificities.Specificites.specificites(table);
////specifresult3.toTxt(new File(exporttestdir,"discours_speciftype"), "UTF-8", "\t", "")
////println("specif LT: "+(System.currentTimeMillis()-time)/1000)
////
////// AFC PARTITION
////time = System.currentTimeMillis();
////CA ca = new CA(discours_dates, lemma_property)
////ca.toSVGFactorialMap(new File(exporttestdir,"discours_cadates"), true, true)
////ca.toSVGSingularValues(new File(exporttestdir,"discours_cadates_singularvalues"))
////println("CA part: "+(System.currentTimeMillis()-time)/1000)
////
////// AFC LEXICAL TABLE
////time = System.currentTimeMillis();
////CA ca2 = new CA(table);
////ca2.toSVGFactorialMap(new File(exporttestdir,"discours_cadates"), true, true)
////ca2.toSVGSingularValues(new File(exporttestdir,"discours_cadates_singularvalues"))
////println("CA LT: "+(System.currentTimeMillis()-time)/1000)
////
////// CAH
////time = System.currentTimeMillis();
////CAH cah = new CAH(ca, true, CAH.getDefaultMethod(), CAH.getDefaultMetric(), 4, false)
////cah.stepCompute()
////cah.toSVG(new File(exporttestdir, "discours_dates_cah"),__RDevice.SVG);
////println("CAH: "+(System.currentTimeMillis()-time)/1000)
