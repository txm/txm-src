package tests
import org.txm.Toolbox
import org.txm.rcp.views.*
import org.txm.rcp.views.corpora.*
import org.txm.searchengine.cqp.MemCqiServer
import org.txm.searchengine.cqp.corpus.*

// Debug
println "******************************** Starting ********************************************";


// Tests suite de requêtes directement sur le serveur et création manuelle du sous-corpus

// parameters
def CORPUS = "LIVRETOPERA10TEXTES"
//def SUBCORPUS = "NOMDUSOUSCORPUS"
// def SUBCORPUS = null // si partition sur corpus
//def SUBCORPUSQUERY = '"je" expand to s'
//def SUBCORPUSQUERY = null // si sous-corpus par nom


def discours = CorpusManager.getCorpusManager().getCorpus(CORPUS)
def subcorpusCqpId = 'S' + UUID.randomUUID().toString();

try {
	def server = (MemCqiServer)Toolbox.getCqiServer();
	
//	server.query('show corpora;');
//	server.query("info " + CORPUS + ";");
	
	server.query(CORPUS + ";");
	
	//client.query(SUBCORPUSQUERY);

	// Suppression de variable de résultats
//	server.query("discard A;");
//	server.query("discard B;");
//	server.query("discard C;");
	
//	server.query('C = /region[sp,a]::a.sp_who="IPHIS";');

	
// Test mono ligne : ne fonctionne pas en mode server
//	server.query('A = /region[sp]; B = /region[speaker]; C = difference B A;');
		
	
		
//	server.query('A = /region[sp];');
//	server.query('B = /region[speaker];');
//	server.query('C = difference A B;');

	// Tests options d'affichage
	server.query('show cd;');
	server.query('show +div1 +div2 +sp +lg +l +speaker;'); // Affichage des balises XML dans les listes de résultats
	//server.query('set ShowTagAttributes on;');
	
	// Tests
//	server.query('A = /region[sp,a]::a.sp_who="THALIE";');
//	server.query('B = /region[speaker];');
//	server.query(subcorpusCqpId + ' = diff A B;');

	
	//server.query(subcorpusCqpId + ' = B;');
	
	
	
//	server.query(subcorpusCqpId + ' = <sp>[(_.) != "speaker"]+</sp>;');
	//server.query(subcorpusCqpId + ' = <div1>[]*<sp>[]+</sp>[]*</div1>;');

	//server.query(subcorpusCqpId + ' = <div1>[!speaker]*</div1>;');
	
// server.query(subcorpusCqpId + ' = <lg>[_.div1_n=".*" & _.sp_who="THALIE"]+</lg>;');	
//	server.query(subcorpusCqpId + ' = /region[i];');
	

	// Tests de requête sur le sous-corpus créé
	//server.query('set MatchingStrategy shortest;'); // standard, shortest, longest, traditional
	//server.query(subcorpusCqpId + ' = [div1 & _.sp_who="THALIE" & !speaker];');
	//server.query(subcorpusCqpId + ' = [_.div1_name=".*" & !speaker];');
	//server.query(subcorpusCqpId + ' = [_.div1_name=".*" & !speaker] expand to lg;');
	server.query(subcorpusCqpId + ' = [div1 & !speaker] expand to lg;');
	
	
	//server.query('group ' + subcorpusCqpId + ' match word;');

	server.query(subcorpusCqpId + ';');
	
		
	//server.query(subcorpusCqpId + ' = [] !;');
	//server.query(subcorpusCqpId + ';');
	//server.query(subcorpusCqpId + ' = [] !;');
//	server.query(subcorpusCqpId + ' = [] expand to div1 & !speaker;');
	
	
	
	//server.query(subcorpusCqpId + ' = [div1 & !speaker];');
	//server.query(subcorpusCqpId + ' = [div1 & !speaker] expand to lg;');
	//server.query(subcorpusCqpId + ' = [div1 & !speaker] expand to lg;');
	
	//server.query(subcorpusCqpId + ' = [div1 & !speaker];');
	
	//server.query(subcorpusCqpId + ' = [div1 &_.sp_who="THALIE" & !speaker];');
	
	//server.query(subcorpusCqpId + ' = [div1 &_.sp_who="THALIE" & !speaker];');
	//server.query(subcorpusCqpId + ' = "fin" [];');
	//server.query(subcorpusCqpId + ' = [div1 & _.sp_who="THALIE" & !speaker];');
	
	// Affichage du sous-corpus
	//server.query("cat " + subcorpusCqpId + " 0 10;")
	server.query("cat " + subcorpusCqpId + ";")
	
	//server.cqpQuery(discours.getCqpId(), 'subcorpusid', SUBCORPUSQUERY);

		
	
	// Affichage des erreurs
	try {
		System.out.println("client.getLastCqiError() : "+server.getLastCqiError());
	} catch(Exception e) { System.out.println("Exception : "+e);}
	try {
		System.out.println("client.getLastCQPError() : "+server.getLastCQPError());
	} catch(Exception e) { System.out.println("Exception : "+e);}

	
	
	// Création manuelle du sous-corpus
//	Subcorpus subcorpus = new Subcorpus(subcorpusCqpId, "subcorpus" + subcorpusCqpId.toString().substring(0, 5), discours, new Query()); // FIXME : est-ce que cela pose un problème de passer une Query() vide ici ?
//	discours.subcorpora.add(subcorpus);
//	subcorpus.registerToParent();
	
	// Création manuelle d'une partition
//	subcorpus.createPartition(NAME, QUERIES, PARTNAMES)
	
	
}
 catch(Exception e) {
	  System.out.println("Exception : "+e);
  }



// Refresh de l'interface
monitor.syncExec(new Runnable() {
	public void run() {
		CorporaView.refresh();
		//CorporaView.expand(partition.getParent());
	}
});






//// parameters
//def CORPUS = "LIVRETOPERA10TEXTES"
////def SUBCORPUS = "NOMDUSOUSCORPUS"
//def SUBCORPUS = null // si partition sur corpus
////def SUBCORPUSQUERY = '"je" expand to s'
//def SUBCORPUSQUERY = null // si sous-corpus par nom
//
//
//
//// Liste des unités strcuturelles
////def HIERARCHY = ['text_id', 'div1_n', 'sp_who', 'lg'];
//def STRUCTURAL_UNITS = ['text', 'div1', 'sp'];
//def STRUCTURAL_UNITS_PROPERTIES = ['id', 'n', 'who'];
////def TARGET = 'lg';
//
//String NAME = '';




//def TEXT = "01_ACHILLE ET POLYXENE"
// def TEXT = "02_ZEPHIRE ET FLORE"
/*def QUERIES = [
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="1" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="2" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="3" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="4" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="5" & _.sp_who=".*"]+</lg>',
]
def PARTNAMES = [
"acte_1",
"acte_2",
"acte_3",
"acte_4",
"acte_5",
]*/


//def discours = CorpusManager.getCorpusManager().getCorpus(CORPUS)
//
//
//// Récupération des unités structurelles
//def i = 0;
//def supName;
//def QUERIES;
//def query;
//
//for (suName in STRUCTURAL_UNITS) {
//
//
//	// Définition auto du nom de partition
//	NAME += suName + '_';
//
//	//query = '';
//	
//	
//	println "Structural Unit Name: $suName"
//	
//	// Récupération des valeurs des propriétés d'unités structurelles
//	for (sup in discours.getStructuralUnitProperties(suName.asType(String)))	{
//		
//		supName = STRUCTURAL_UNITS_PROPERTIES[i];
//		
//		if(supName != '' && sup.getName() == supName)	{
//			
//			// Définition auto du nom de partition
//			NAME += supName + ".";
//			
//			println "Structural Unit Property: $supName";
//			
//			for (supValue in sup.getOrderedValues()) {
//			
//				//query += '<' + TARGET + '>' + supName;
//				
//				
//				print "$supValue "
//			}
//		}
//	}
//	i++;
//}
//
//// Suppression du dernier underscore ou point
//NAME = NAME.substring(0, NAME.length() - 1);
//// Debug
//println "NAME: " + NAME;








// start
/*if (QUERIES.size() == PARTNAMES.size()) {

	def partition;

	
	// Test unités structurelles
	for (t in discours.getStructuralUnits()) {
		println "Structural Unit: $t"
		
		// Test propriétés d'unités structurelles
		for (g in discours.getStructuralUnitProperties(t.asType(String))) {
			
			println "\tStructural Unit Property: $g"
			
			// Test valeurs des propriétés d'unités structurelles
			for (h in g.getOrderedValues()) {
				
				print "$h, "
				
			}
			
		}
		
	}
	
		
	// Test propriétés
	for (t in discours.getProperties()) {
		println "Property: $t"
	}
	
	
	if (SUBCORPUS == null) {
		println "partition build with $discours"
		partition = discours.createPartition(NAME, QUERIES, PARTNAMES)
	} else {
		def subcorpus
		if (SUBCORPUSQUERY == null) {
			println "partition build with subcorpus by name $SUBCORPUS"
			subcorpus = discours.getSubcorpusByName(SUBCORPUS)
			if (subcorpus == null) {
				println "Error: SUBCORPUS NOT FOUND with name : $SUBCORPUS"
			}
		} else {
			println "partition build with subcorpus with query $SUBCORPUSQUERY"
			subcorpus = discours.createSubcorpus(new Query(SUBCORPUSQUERY), SUBCORPUS)
		}
		partition = subcorpus.createPartition(NAME, QUERIES, PARTNAMES)
	}
	
	def list = Arrays.asList(partition.getPartSizes())
	println "Partition created $NAME: "+list+" parts"
	// println "Total size: "+list.sum()+" - is equal to (sub)corpus size : "+(list.sum() == partition.getCorpus().getSize())
    println "Total size: "+list.sum()+" - (sub)corpus size : "+(partition.getCorpus().getSize())
	
	monitor.syncExec(new Runnable() {
	public void run() {
		CorporaView.refresh();
		CorporaView.expand(partition.getParent());
	}
});
} else {
	println "QUERIES.size() != PARTNAMES.size() = "+QUERIES.size()+" != "+PARTNAMES.size()
}*/

