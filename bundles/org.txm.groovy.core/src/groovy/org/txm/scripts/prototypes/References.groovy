/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.test

//we import the packages containing the functions we are going to use
import org.txm.Toolbox
import org.txm.referencer.core.functions.Referencer;
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.functions.referencer.*
import org.txm.searchengine.cqp.ReferencePattern

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
Corpus discours = CorpusManager.getCorpusManager().getCorpus("DISCOURS")

// we get some properties
Property word = discours.getProperty("word")
StructuralUnit text = discours.getStructuralUnit("text")
StructuralUnitProperty text_id = text.getProperty("id")

// we create a query to find the positions
Query query = new Query(Query.fixQuery("j.*"))

// what the index must show
ReferencePattern referencePattern = new ReferencePattern().addProperty(text_id)

//define the referencer
// We want the references of the words who matche the query "je" in the discours corpus
long time = System.currentTimeMillis()
Referencer ref = new Referencer(discours, query, word, referencePattern.getProperties())
ref.compute()
println "time get positions : "+(System.currentTimeMillis()-time)

time = System.currentTimeMillis()
def lines =ref.getLines()
println "time get "+lines.size()+" lines : "+(System.currentTimeMillis()-time)

File file = new File("ref.txt")
ref.toTxt(file, "UTF-8")
println "writen in : "+file.getAbsolutePath()
