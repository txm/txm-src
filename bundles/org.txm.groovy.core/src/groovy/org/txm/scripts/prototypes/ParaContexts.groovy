package org.txm.scripts.test

import java.util.ArrayList;
import org.txm.para.functions.*
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*


ArrayList<String> queries = ["je", "je"]
String struct = "text_id"
ArrayList<Corpus> corpora = ["QGRAAL", "QGRAAL"]
ArrayList<String> props = ["word", "frlemma"]
ArrayList<String> refs = ["text_id", "text_titre"]
ArrayList<Integer> CGs = [30, 10]
ArrayList<Integer> CDs = [10, 1]
ArrayList<Boolean> participates = [true, true]

//ParallelContexts ctx = new ParallelContexts();

println "starts-ends: "+ctx.getKeywordsStartsEnds()
println "keywords: "+ctx.getKeywordsStrings()
println "seg keys; "+ctx.getSegKeys()