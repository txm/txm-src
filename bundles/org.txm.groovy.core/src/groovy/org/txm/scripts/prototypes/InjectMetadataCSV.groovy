package org.txm.scripts.test;
import org.txm.metadatas.*;

def txmfilesdir = new File(System.getProperty("user.home"), "xml/metadata")
def outdir =  new File(System.getProperty("user.home"), "xml/metadata/meta")
outdir.mkdir()
def allMetadataFile = Metadatas.findMetadataFile(outdir);
println "-- INJECT METADATA - from table file: "+allMetadataFile+" in directory: "+txmfilesdir
def metadatas = new Metadatas(allMetadataFile, "UTF-8", ",","\"", 1)
if(metadatas != null)
{
	println("Injecting metadata: "+metadatas.getHeadersList()+" in texts of directory "+txmfilesdir)
	for (File infile : txmfilesdir.listFiles()) {
		if(!infile.isFile() || infile.getName().endsWith(".csv.xml") || !infile.getName().endsWith(".xml"))
			continue;
		
		print "."
		File outfile = new File(outdir, infile.getName());
		String tag = "text";
		
		if(!metadatas.injectMetadatasInXmlTXM(infile, outfile))
		{
			outfile.delete();
			println "Error: Injection failed, stop import"
			return;
		}
	}
	println ""
}