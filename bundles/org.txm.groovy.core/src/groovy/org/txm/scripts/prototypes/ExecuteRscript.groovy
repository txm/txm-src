package org.txm.scripts.test;
/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
import org.txm.statsengine.r.core.RWorkspace
import org.txm.rcp.commands.OpenBrowser
// import org.txm.rcp.commands.*
// uncomment for test

// get a connection to R
// TODO: Auto-generated Javadoc

/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
def r = RWorkspace.getRWorkspaceInstance()

// start logging R output in the console
r.setLog(true)

// a sample plotting script
def script = new File(System.getProperty("user.home")+"/TXM/scripts/samples/R/plot100.R")
// we need this for Windows
def scriptpath = script.getAbsolutePath().replace("\\","\\\\")
// use a temporary file to transfert the graphic
def f = File.createTempFile("txm", ".svg", script.getParentFile())

// generate the graphic
r.plot(f, "source(\""+scriptpath+"\")")

// draw the graphic in a new window of TXM
monitor.syncExec(new Runnable() {
	@Override
	public void run() {	OpenBrowser.openfile(f.getAbsolutePath()) }
});


// stop logging R output
r.setLog(false)
