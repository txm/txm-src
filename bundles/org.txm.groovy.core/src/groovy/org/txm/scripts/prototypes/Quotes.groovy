package org.txm.scripts.test

import groovy.xml.XmlParser

println "Pre-balisage du discours direct"

List<String> containers = ["p","ab"]; // elements qui contiennent des <s>

File infile = new File(System.getProperty("user.home"), "TXM-SRC/quote-orig/perceval2.xml")
File outfile = new File(System.getProperty("user.home"), "TXM-SRC/quote-orig/perceval2-q.xml")

def doc = new XmlParser().parse (infile)
List<Node> nodesToInspect = doc.text.body.div
// nodesToInspect << doc.text.body.div // on en rajoute

/*************************/
new org.txm.importer.EncodeTEIQuotes(nodesToInspect, containers, "\"", "pon");

//copy the doc in "outfile" File  
String encoding = "UTF-8"
OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outfile) , encoding);
//writer.write("<?xml version=\"1.0\" encoding=\""+encoding+"\"?>");
//writer.write ("<?xml-stylesheet type=\"text/css\" href=\"tei-graal.css\"?>\n")
def pwriter = new PrintWriter(writer, true)
XmlNodePrinter xmlwriter = new XmlNodePrinter(pwriter)
xmlwriter.setPreserveWhitespace(false)
xmlwriter.print(doc)
pwriter.close()
writer.close()
xmlwriter = null
//println "write output file "+outfile

//update counts
if(outfile.exists())
{
	String txtid = org.txm.importer.WordCounter.findTextId(infile, "s"); // retrouve l'id du text qui a été concaténé aux id des S originels (ex : s19_12 >> 19)
	new org.txm.importer.WordCounter(outfile, "s", txtid);
	new org.txm.importer.WordCounter(outfile, "q", txtid);
}