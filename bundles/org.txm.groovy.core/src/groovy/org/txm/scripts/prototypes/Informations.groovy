/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.test


// imports des fonctions que l'on va utiliser
import org.txm.Toolbox
import org.txm.properties.core.functions.Properties
import org.txm.searchengine.cqp.corpus.*
import org.txm.objects.*

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
def manualInit = false
if (!org.txm.Toolbox.isInitialized())
{
	manualInit = true
	org.txm.Toolbox.initialize(new File(System.getProperty("user.home")+"/TXM/install.prefs"))
}

def corpus = CorpusManager.getCorpusManager().getCorpus("QUETE")

try
{
	def d = new Properties(corpus, 20)
	d.process();
	println d.htmlDump()
} catch(Exception e){	e.printStackTrace() }

if(manualInit)
	org.txm.Toolbox.shutdown()

