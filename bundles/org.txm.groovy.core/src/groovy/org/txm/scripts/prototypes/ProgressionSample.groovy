package org.txm.scripts.test;
import java.util.List;

import org.txm.progression.core.functions.Progression
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.functions.progression.*
import org.txm.stat.engine.r.*;

def discours = CorpusManager.getCorpusManager().getCorpus("DISCOURS");

def q1 = new Query(Query.fixQuery("je|j'"))
def q2 = new Query(Query.fixQuery("tu"))
def q3 = new Query(Query.fixQuery("il"))
def q4 = new Query(Query.fixQuery("elle"))
def q5 = new Query(Query.fixQuery("on"))
def q6 = new Query(Query.fixQuery("nous"))
def q7 = new Query(Query.fixQuery("vous"))
def q8 = new Query(Query.fixQuery("ils"))
def q9 = new Query(Query.fixQuery("elles"))
def q10 = new Query(Query.fixQuery("eux"))
def q11 = new Query(Query.fixQuery("ceux"))
def q12 = new Query(Query.fixQuery("autre|autres"))
def q13 = new Query(Query.fixQuery("monde"))
def q14 = new Query(Query.fixQuery("France"))
def q15 = new Query(Query.fixQuery("Algérie"))
def q16 = new Query(Query.fixQuery("Europe"))

//Progression(Corpus corpus, List<Query> queries,
//			StructuralUnit structure, StructuralUnitProperty property, String propertyregex,
//			boolean doCumulative, boolean monochrome, boolean monostyle, 
//			int linewidth, boolean repeat, float bandemultiplier) {
		
int linewidth = 2;
float bandewidth = 1.0f
Progression progression5 = new Progression(discours, [q1, q2, q3, q4, q5],
	null, null,	"", 
	true, false, false, 
	linewidth, false, bandewidth);
Progression progression10 = new Progression(discours, [q1, q2, q3, q4, q5, q6, q7, q8, q9, q10],
	null, null,	"", 
	true, false, false, 
	linewidth, false, bandewidth);
Progression progression16 = new Progression(discours, [q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16],
	null, null,	"", 
	true, false, false, 
	linewidth, false, bandewidth);

File outputdir = new File("/home/mdecorde/Bureau")
File svgFile5 = new File(outputdir, "progression5");
File svgFile10 = new File(outputdir, "progression10");
File svgFile16 = new File(outputdir, "progression16");

progression5.toSvg(svgFile5, RDevice.SVG);
progression10.toSvg(svgFile10, RDevice.SVG);
progression16.toSvg(svgFile16, RDevice.SVG);

monitor.syncExec(new Runnable() {
	@Override
	public void run() {	
		OpenBrowser.openfile(svgFile5.getAbsolutePath())
OpenBrowser.openfile(svgFile10.getAbsolutePath())
OpenBrowser.openfile(svgFile16.getAbsolutePath())
		}
});

