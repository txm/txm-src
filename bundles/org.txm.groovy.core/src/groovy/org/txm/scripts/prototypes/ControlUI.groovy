package org.txm.scripts.test

//import org.txm.rcp.views.*

// Bindings
println "The current job monitor: "+monitor
println "The current editor: "+editor
println "The current selection: "+selection

// Interact with SWT Objects
monitor.syncExec(new Runnable() {
	public void run() {
		//CorporaView.reload()
		println "corpora view refreshed !"
	}
})

// Move the progress bar and cancel if necessary
println "waiting..."
Thread.sleep(1000)
monitor.worked(20)
if (monitor.isCanceled()) {monitor.done(); return}

println "waiting..."
Thread.sleep(1000)
monitor.worked(20)
if (monitor.isCanceled()) {monitor.done(); return}

println "waiting..."
Thread.sleep(1000)
monitor.worked(20)
if (monitor.isCanceled()) {monitor.done(); return}

println "waiting..."
Thread.sleep(1000)
monitor.worked(20)
if (monitor.isCanceled()) {monitor.done(); return}

println "waiting..."
Thread.sleep(1000)
monitor.worked(20)
if (monitor.isCanceled()) {monitor.done(); return}

println "Done."