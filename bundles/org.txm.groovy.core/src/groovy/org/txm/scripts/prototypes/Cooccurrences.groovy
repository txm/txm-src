/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.test

// we import the packages containing the functions we are going to use
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.cooccurrence.core.functions.Cooccurrence
import org.txm.functions.cooccurrences.*

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
def discours = CorpusManager.getCorpusManager().getCorpus("DISCOURS")

def word = discours.getProperty("word")
def pos = discours.getProperty("pos")

def limit = discours.getStructuralUnit("s")
def minleft = 1
def maxleft = 2
def minright = 1
def maxright = 2

def minf = 0
def mincof = 0
def minscore = 0

def query = new Query(Query.fixQuery("je"))

//long time = System.currentTimeMillis()

def cooccurrence = new Cooccurrence(discours, query, [word], limit, minleft, maxleft, minright, maxright, minf, mincof, minscore)
cooccurrence.process();
//cooccurrence.compute()

//println("Compute time : "+(System.currentTimeMillis()-time)+" ms")


def file = new File("cooc.txt")
cooccurrence.toTxt(file)

println "printed cooccurrents in "+file.getAbsolutePath()

