/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.test

import org.rosuda.REngine.REXP;
import org.txm.Toolbox;
import org.txm.statsengine.r.core.RWorkspace;

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
RWorkspace rw = RWorkspace.getRWorkspaceInstance();//get the RWorkspace instance
rw.setLog(true);//start R log

//execute a simple line of code
rw.eval("print(2+2)")
println "-"

//execute a file
/*
 * script.R:
a <- 2+2
print(a)
b <- 4+4
print(b)
c <- 6+6
print(c)
 */
File scriptR = new File("script.R")// get a File
String filepath = scriptR.getAbsolutePath().replace("\\","\\\\")//We need to to it for Windows OS
rw.eval("source(\""+filepath+"\")")
println "-"

//execute a file line per line
Reader reader = new FileReader(scriptR);// create a file reader
String line = reader.readLine();// read next line
while(line != null)//continue until the end of the file
{
	rw.eval(line)//evaluate line in R
	line = reader.readLine();//read next line
}
println "-"

println "Last log line : "+rw.getLastLogLine();// get the last log
rw.setLog(false);//stop R log