package org.txm.scripts.test;
import org.txm.functions.mesures.*;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*

def corpus1 = CorpusManager.getCorpusManager().getCorpus("MESURES")

Mesures mesures = new Mesures([corpus1]);
// Taille en mot des textes
mesures.add(new Magnitude('<text> [] expand to text', "word", false, Synthese.SUM));
// Qurtiles des discours direct
mesures.add(new Magnitude('<q> [_.q_type="DD"] expand to q', "word", true, Synthese.QUARTILE));
// Quartiles des paragraphes (ici div1, j'ai pas de paragraphes -_-)
mesures.add(new Magnitude('<div1> [] expand to div1', "word", true, Synthese.QUARTILE));
// moyenne de la taille en mot des paragraphes
//mesures.add(new Magnitude('<p> [] expand to p', "word", true, Synthese.MOYENNE));

// proportion des ! et ? dans le discours direct
mesures.add(new Proportion('[_.q_type="DD"]', "word", true, Synthese.COUNTMATCHES, '[word=".*[.!?].*"]', ['[word=".*\\?*"]', '[word=".*!.*"]']));
// proportion de discours direct avec tiret ou guillemet
mesures.add(new Proportion(null, "word", true, Synthese.COUNTMATCHES, '[_.q_type="DD"] expand to q', ['[_.q_type="DD" & _.q_rend="tiret"] expand to q', '[_.q_type="DD" & _.q_rend="guillemet"] expand to q']));
// Proportion de discours direct avec incise de dire / verbe introducteur
mesures.add(new Proportion(null, "word", true, Synthese.COUNTMATCHES, '[_.q_type="DD"] expand to q', ['[_.q_type="DD" & _.seg_ana="incise_di"] expand to q', '[_.seg_ana="int_DD"] </seg> []{0,10} <q> [_.q_type="DD"]']));
//mesures.add(new Proportion('"j.*"%c expand to text', "word", true, Synthese.SUM, '"j.*" expand to text', ['"j.....*"%c expand to text', '"j........*"%c expand to text']));

// Taux de présence linéaire de ponctuation forte dans le DD
mesures.add(new PresenceRate('[_.q_type="DD"] expand to q', "word", true, Synthese.SUM, '[word=".*[;!?].*"]'));
// Taux de présence linéaire de DD
mesures.add(new PresenceRate(null, "word", true, Synthese.SUM, '[_.q_type="DD"]'));
// Taux de présence Linéaire de PR 
mesures.add(new PresenceRate(null, "word", true, Synthese.SUM, '<pr> [] expand to pr'));

// Diversité (en lemmes) des verbes introducteurs
mesures.add(new Diversity(null, "word", true, Synthese.SUM, '[_.seg_ana="int.*" & frpos="V.*"]', "frlemma"));
//mesures.add(new Diversity('[_.q_type="DD"] expand to q', "word", true, Synthese.SUM, '"j.*"', "frpos"));

mesures.prettyPrint();