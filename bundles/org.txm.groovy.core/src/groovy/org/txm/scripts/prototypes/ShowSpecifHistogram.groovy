/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2011-05-09 14:58:28 +0200 (lun., 09 mai 2011) $
// $LastChangedRevision: 1839 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.test

// imports des fonctions que l'on va utiliser
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*

def corpusname = "DISCOURS"
def struct = "text"
def structprop = "date"

MainCorpus discours = CorpusManager.getCorpusManager().getCorpus(corpusname)
println "Building partition..."
Partition discours_dates = discours.createPartition( discours.getStructuralUnit(struct), discours.getStructuralUnit(struct).getProperty(structprop))
	
// on récupère ses propriétés
Property pos = discours.getProperty("pos")
Property lemme = discours.getProperty("lemma")
Property word = discours.getProperty("word")

def Fmin = 2
def Vmax = 10

println "Building lexical table..."
def table = discours_dates.getLexicalTable(lemme, Fmin)
//for(int i = 0 ; i table.get)
table.cut(Vmax)

println "Computing specificities..." 
def	result = Specificites.specificites(table)

File outfile = new File("specif.txt")
result.toTxt(outfile, "UTF-8")
println "Saved in file "+outfile.getAbsolutePath()