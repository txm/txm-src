package org.txm.scripts.test;

File currentFile = editor.getEditedFile()
String text = currentFile.getText("UTF-8")
currentFile.withWriter("UTF-8") { writer ->
	writer.write(text.replaceAll("e", "E"));
}
