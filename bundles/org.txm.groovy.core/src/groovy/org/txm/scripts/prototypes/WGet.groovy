package org.txm.scripts.test

URL url = new URL("http://docs.codehaus.org/display/GROOVY/Simple+file+download+from+URL");
File outFile = new File("test.out")

static def download(def file, def url) {
		url.withInputStream() {is->
			 file.withOutputStream() {os->
				 def bs = new BufferedOutputStream( os )
				 bs << is
			 }
		 }
}

// if proxy , uncomment this and set up properties
//System.properties.putAll( ["http.proxyHost":"proxy-host",
//							 "http.proxyPort":"proxy-port",
//							 "http.proxyUserName":"user-name",
//							 "http.proxyPassword":"proxy-passwd"] )

WGet.download(outFile, url);
println outFile.getText()