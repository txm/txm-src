package org.txm.scripts.test


//import the packages containing the functions we are going to use
import org.txm.Toolbox
import org.txm.searchengine.cqp.core.functions.selection.Selection
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.functions.selection.*

MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus("DISCOURS")

Selection sel = new Selection(corpus);
sel.fullDump()