/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-08-30 09:45:56 +0200 (mar. 30 août 2016) $
// $LastChangedRevision: 3283 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.test

// imports des fonctions que l'on va utiliser
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.functions.index.*
// TODO: Auto-generated Javadoc

/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
boolean manualInit = false
if(!org.txm.Toolbox.isInitialized())
{
	manualInit = true
	org.txm.Toolbox.initialize(new File(System.getProperty("user.home")+"/TXM/install.prefs"))
}
	// on récupère le corpus DISCOURS
	MainCorpus discours = CorpusManager.getCorpusManager().getCorpus("DISCOURS")
	Partition discours_dates = discours.createPartition( discours.getStructuralUnit("text"), discours.getStructuralUnit("text").getProperty("date"))
	println discours
	
	// on récupère ses propriétés
	Property pos = discours.getProperty("pos")
	Property word = discours.getProperty("word")
	
	// on créé une Query, ici "[]"
	Query query = new Query(Query.fixQuery("[word=\"j.*\"]"))
	
	List<Property> props = [pos,word]
	
	// on exécute la commande
	def time = System.currentTimeMillis()
	println(""+discours+"\t"+query+"\t"+props)
	IndexSample index = new IndexSample(discours_dates, query, props )
	println("execution time : "+(System.currentTimeMillis()-time)+" ms")
	
	//sort
	//index.sortLines(LineComparator.SortMode.FREQUNIT)
	
	//get some infos
	println("V : "+index.getV())
	println("T : "+index.getT())
	
	//filter
	index.filterLines(2,3000)
	println("V after: "+index.getV())
	println("T after: "+index.getT())
	
	//on écrit tout dans un fichier
	File file = new File("voc.txt")
	index.toTxt(file)
	println("index file : "+ file.getAbsolutePath())

	
if(manualInit)
	org.txm.Toolbox.shutdown()