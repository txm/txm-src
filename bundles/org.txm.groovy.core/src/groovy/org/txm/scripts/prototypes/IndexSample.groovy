/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-08-30 09:45:56 +0200 (mar. 30 août 2016) $
// $LastChangedRevision: 3283 $
// $LastChangedBy: mdecorde $
//

package org.txm.scripts.test

// imports des fonctions que l'on va utiliser
import org.txm.Toolbox
import org.txm.index.core.functions.Index
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*

// on récupère le corpus DISCOURS
MainCorpus discours = CorpusManager.getCorpusManager().getCorpus("BFM")
println discours

// on récupère ses propriétés
Property pos = discours.getProperty("ttpos")
Property word = discours.getProperty("word")

// on créé une Query, ici "[]"
Query query = new Query(Query.fixQuery("[word=\"je\"]"))

List<Property> props = [pos,word]

// on exécute la commande
def time = System.currentTimeMillis()
println(""+discours+"\t"+query+"\t"+props)
Index index = new Index(discours, query, props )
println("execution time : "+(System.currentTimeMillis()-time)+" ms")

//sort
//index.sortLines(LineComparator.SortMode.FREQUNIT)

//get some infos
println("V : "+index.getV())
println("T : "+index.getT())

//filter
index.filterLines(2,3000)
println("V after: "+index.getV())
println("T after: "+index.getT())

//on écrit tout dans un fichier
File file = new File("voc.txt")
index.toTxt(file, "'", "\t", "UTF-8")
println("index file : "+ file.getAbsolutePath())