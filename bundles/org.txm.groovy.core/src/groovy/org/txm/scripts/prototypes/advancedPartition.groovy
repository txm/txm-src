package tests

import org.txm.rcp.views.*
import org.txm.searchengine.cqp.corpus.*


/*
import java.awt.Dimension
import java.util.UUID;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.txm.Toolbox
import org.txm.searchengine.cqp.MemCqiClient
import org.txm.searchengine.cqp.MemCqiServer
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.ReferencePattern
import org.txm.rcp.views.CorporaView
import org.txm.searchengine.cqp.corpus.query.Query
*/


/**
 * Create partition with advanced CQL queries and autoname the parts.
 * Can create partitions by defining a multi-level structural units hierarchy or by defining several properties values for one structural unit.
 * Can define some structural units to remove from the resulting parts subcorpus.
 *
 */

// TODO : Log
println "******************************** Starting ********************************************";


// *************************** User parameters ************************************************************


// Test 1 : partition en excluant des sections
def CORPUS_NAME = "LIVRETOPERA2"								// The name of the corpus to partition

def SUBCORPUS_NAME = null										// The name of the subcorpus to partition
//def SUBCORPUS_NAME = "TEST"										// The name of the subcorpus to partition if needed
// FIXME: subcorpus management is bugged, when trying to create a CA on a Partition created on a subcorpus, it doesn't work because of the getLexicon() method which throws a CqiCqpErrorErrorGeneral exception

PartitionQueriesGenerator.STRUCTURAL_UNITS = ['sp']; 					// Applying the partition on these structural units
PartitionQueriesGenerator.STRUCTURAL_UNITS_PROPERTIES = ['who'];		// Applying the partition on these properties of structural units defined above

PartitionQueriesGenerator.STRUCTURAL_UNITS_TO_IGNORE = ['speaker'];	// These structural units will be removed from the partition. NOTE : If doing that you need to define an EXPAND_TARGET
																		// if you want some subcorpus parts that will manage sequential positions queries

PartitionQueriesGenerator.EXPAND_TARGET = 'lg'; 						// Expand the results to this structural unit parent target. If some structural units to ignore are defined
																		// and the expand target is upper on the hierarchy than them, the structural units to ignore WON'T be ignored

//PartitionQueriesGenerator.PARTITION_NAME = 'gugu';					// The partition name. If empty or not defined, the partition will be autonamed regarding of the structural units and properties




// Test 2 : partition sur arborescence (multi-niveau sur 2 niveaux)
//def CORPUS_NAME = "LIVRETOPERA10TEXTES"
//PartitionQueriesGenerator.STRUCTURAL_UNITS = ['text', 'div1'];
//PartitionQueriesGenerator.STRUCTURAL_UNITS_PROPERTIES = ['id', 'name'];


// Test 3 : partition sur arborescence (multi-niveau sur 3 niveaux)
//def CORPUS_NAME = "LIVRETOPERA10TEXTES"
//PartitionQueriesGenerator.STRUCTURAL_UNITS = ['text', 'div1', 'div2'];
//PartitionQueriesGenerator.STRUCTURAL_UNITS_PROPERTIES = ['id', 'name', 'name'];


// Test 4 : partition sur arborescence (multi-niveau sur 4 niveaux)
// Ajouter le 'n' permet par exemple ici de trier les parties par ordre de scène car sinon le tri est problématique, ex : "SCENE II" passe avant "SCENE PREMIERE"
//def CORPUS_NAME = "LIVRETOPERA2"
//PartitionQueriesGenerator.STRUCTURAL_UNITS = ['text', 'div1', 'div2', 'div2'];
//PartitionQueriesGenerator.STRUCTURAL_UNITS_PROPERTIES = ['id', 'name', 'n', 'name'];


// Test 5 : partitions croisées (sur plusieurs propriétés d'une même structure)
//def CORPUS_NAME = "DISCOURS"
//PartitionQueriesGenerator.STRUCTURAL_UNITS = ['text', 'text'];
//PartitionQueriesGenerator.STRUCTURAL_UNITS_PROPERTIES = ['loc', 'type'];



// Tests
//def CORPUS_NAME = "LIVRETOPERA10TEXTES"
//PartitionQueriesGenerator.STRUCTURAL_UNITS = ['div1', 'sp'];
//PartitionQueriesGenerator.STRUCTURAL_UNITS_PROPERTIES = ['name', 'who'];

// Tests
//def CORPUS_NAME = "LIVRETOPERA10TEXTES"
//PartitionQueriesGenerator.STRUCTURAL_UNITS = ['div1'];
//PartitionQueriesGenerator.STRUCTURAL_UNITS_PROPERTIES = ['n'];
//PartitionQueriesGenerator.PART_NAMES_PREFIX = 'act_';




// *************************** Debug parameters ************************************************************


PartitionQueriesGenerator.DEBUG = 0; 	// If DEBUG != 0 then partition is not created,
										// script only outputs the created queries and part names strings in console



// *************************** End of parameters ************************************************************







// Running
def partition = PartitionQueriesGenerator.createPartition(CORPUS_NAME, SUBCORPUS_NAME);


// Refreshing the RCP component
if(partition != null)	{
	monitor.syncExec(new Runnable() {
			public void run() {
				CorporaView.refresh();
				CorporaView.expand(partition.getParent());
			}
		});
}





/**
 * Create a list of queries and part names regarding the structural units, structural units properties, structural units to ignore user defined lists and expand target value specified.
 * @author s
 *
 */
public class PartitionQueriesGenerator	{

	public static int DEBUG = 0;								// si DEBUG != 0, alors les requêtes sont affichées mais la partition n'est pas créée

	public static String PARTITION_NAME = '';
	public static String[] STRUCTURAL_UNITS = [];				// Liste des unités structurelles sur lesquelles effectuer la partition, ex: ['text', 'div1']
	public static String[] STRUCTURAL_UNITS_PROPERTIES = [];	// Propriétés des unités structurelles sur lesquelles effectuer la partition, ex : ['id', 'name']
	public static String[] STRUCTURAL_UNITS_TO_IGNORE = [];		// Structure à ignorer, ex. CQL : !speaker
	public static String PART_NAMES_PREFIX = '';
	public static String EXPAND_TARGET = null;					// Expand to target, englobe les empans jusqu'à la balise parente spécifiée. NOTE : Le expand entre en conflit avec les sections à ignorer.
																// Si la target est à un niveau supérieur aux balises à ignorer, il les remet dans liste de résultats CWB et elles ne sont donc pas ignorées



	public static ArrayList<String> queries = new ArrayList<String>();
	public static ArrayList<String> partNames = new ArrayList<String>();


	/**
	 * Init the generator and process.
	 * @param corpusName
	 */
	public static Partition createPartition(String corpusName, String subcorpusName)	{

		if(STRUCTURAL_UNITS.size() > 0 && STRUCTURAL_UNITS.size() == STRUCTURAL_UNITS_PROPERTIES.size())	{

			// TODO : Log
			println '**************************************************************************************************************'
			println 'Creating the queries on corpus "' + corpusName + "'" ;

			Corpus corpus = CorpusManager.getCorpusManager().getCorpus(corpusName);


			// Subcorpora
			if(subcorpusName != null)	{
				corpus = corpus.getSubcorpusByName(subcorpusName);
			}



			// Recursing through the corpus and subcorpus
			process(corpus, 0, '', '');

			// Finalizing the queries
			finalizeQueries();


			// TODO : Debug
			// Displaying the partition name
			println '';
			println 'PARTITION_NAME: ' + PartitionQueriesGenerator.PARTITION_NAME;

			// Displaying the queries
			println 'Queries (count = ' + PartitionQueriesGenerator.queries.size() + '):';
			for(query in PartitionQueriesGenerator.queries)	{
				println query;
			}
			// Displaying the part names
			println 'Partnames (count = ' + PartitionQueriesGenerator.partNames.size() + '):';
			for(partName in PartitionQueriesGenerator.partNames)	{
				print partName + ' / ';
			}


			// TODO : Log
			println 'Queries created.';

			// Creating the partition
			if(DEBUG == 0 && queries.size() == partNames.size()) {
				return corpus.createPartition(PARTITION_NAME, queries, partNames);
			}

		}
		else	{
			// TODO : Log
			println 'Structural units count or structural units properties count error.';
			return null
		}

	}




	/**
	 * Recurse through structural units and structural units properties of corpus and create the queries and the part names.
	 * @param corpus the corpus or subcorpus
	 * @param index the index for recursion
	 * @param tmpQuery the temporary query for creating subcorpus part
	 * @param tmpPartName the temporary part name of the subcorpus part
	 */
	protected static void process(Corpus corpus, int index, String tmpQuery, String tmpPartName)	{


		// End of array
		if(index >= STRUCTURAL_UNITS.size())	{

			queries.add(tmpQuery);
			partNames.add(PART_NAMES_PREFIX + tmpPartName);

			return;
		}

		StructuralUnit su = corpus.getStructuralUnit(STRUCTURAL_UNITS[index]);
		StructuralUnitProperty sup = su.getProperty(STRUCTURAL_UNITS_PROPERTIES[index]);

		// TODO : Log
		println ''
		if(index == 0)	{
			println 'Processing Structural Unit Property "' + sup.getFullName() + '" on mother corpus "' + corpus.getID() + '"';
		}
		else	{
			println 'Processing Structural Unit Property "' + sup.getFullName() + '" on subcorpus part "' + tmpPartName + '"';
		}
		println ''


		// Creating the queries parts for each structural units properties values
		//for (supValue in sup.getOrderedValues()) { // TODO : signaler bug Matthieu, on ne devrait pas être obligé de repasser le sous-corpus à la méthode car sup a déjà été créée depuis le sous-corpus ? getValues() bugge aussi
		for (supValue in sup.getOrderedValues(corpus)) {

			// TODO : Log
			println ''
			println 'Value "' + supValue + '"';
			println ''


			// Getting the subcorpus linked to the structural unit property value
			Subcorpus tmpSubcorpus = corpus.createSubcorpusWithQueryString(su, sup, supValue, "tmp" + UUID.randomUUID());

			// Partition conditions and part name separators
			String and = '';
			String underscore = '';
			if(tmpQuery != '')	{
				underscore = '_';
				and = ' & ';
			}


			process(tmpSubcorpus, index + 1, (tmpQuery + and + '_.' + sup.getFullName() + '="' + supValue + '"'), tmpPartName + underscore + supValue);

			// Deleting the temporary subcorpus
			// TODO : bug : cette méthode ne supprime pas le corpus sans doute car il faut que le sous-corpus ne contienne pas d'autres sous-corpus ? le delete() en revanche fonctionne.
//			corpus.dropSubcorpus(tmpSubcorpus);
			tmpSubcorpus.delete();

		}
	}


	/**
	 * 	Autoname the partition.
	 * @param partitionName
	 */
	protected static void autoNamePartition(String partitionName)	{

		// Structural units names and properties
		for(int i = 0; i < STRUCTURAL_UNITS.size(); i++)	{
			partitionName +=  STRUCTURAL_UNITS[i] + '_' + STRUCTURAL_UNITS_PROPERTIES[i] + '.';
		}

		// Structural units to ignore
		for(int i = 0; i < STRUCTURAL_UNITS_TO_IGNORE.size(); i++)	{
			partitionName +=  'NOT_' + STRUCTURAL_UNITS_TO_IGNORE[i] + '.';
		}


		// Removing last point in name
		PARTITION_NAME = partitionName.substring(0, partitionName.length() - 1);
	}


	/**
	 * Finalize the queries.
	 */
	protected static void finalizeQueries()	{

		String expandTo = '';
		// Expanding to user defined target
		if(EXPAND_TARGET != null && EXPAND_TARGET != '')	{
			expandTo = ' expand to ' + EXPAND_TARGET;
		}
		// Expanding to last child structural unit in user defined hierarchy
		else if(STRUCTURAL_UNITS_TO_IGNORE.size() == 0)	{
			expandTo = ' expand to ' + STRUCTURAL_UNITS[STRUCTURAL_UNITS.size() - 1];
		}

		// Autonaming the partition
		if(PARTITION_NAME == '')	{
			autoNamePartition(PARTITION_NAME);
			// Finalizing partition name
			PARTITION_NAME += expandTo.replace(' expand to', '.EXPAND TO').replace(' ', '_');
		}


		// Finalizing queries
		for(int j = 0; j < queries.size(); j++)	{

			String queryEnd = '';

			// Removing some sections
			for(sectionToIgnore in STRUCTURAL_UNITS_TO_IGNORE)	{
				queryEnd += ' & !' + sectionToIgnore;
			}

			queryEnd += ']' + expandTo;

			queries.set(j, '[' +  queries.get(j) + queryEnd);
		}
	}

}






/*
// Test dialogue de confirmation avant création de la partition
JFrame frame = new JFrame('test');
//frame.setMinimumSize(new Dimension(400, 400));
//frame.setVisible(true);
//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
int n = JOptionPane.showConfirmDialog(
	frame, "Would you like green eggs and ham?",
	"An Inane Question",
	JOptionPane.YES_NO_OPTION);
if (n == JOptionPane.YES_OPTION) {
frame.setTitle("Ewww!");
} else if (n == JOptionPane.NO_OPTION) {
frame.setTitle("Me neither!");
} else {
frame.setTitle("Come on -- tell me!");
}*/





//// Récupération des unités structurelles et création de la liste des requêtes CQL
//int i = 0;
//ArrayList<String> queries = new ArrayList<String>();
//ArrayList<String> partNames = new ArrayList<String>();
//
//
//int currentQueriesCount;
//if(STRUCTURAL_UNITS.size() > 0 && STRUCTURAL_UNITS.size() == STRUCTURAL_UNITS_PROPERTIES.size())	{
//
//	// Parcours des unités strcuturelles à traiter
//	for (suName in STRUCTURAL_UNITS) {
//
//		// Définition auto du nom de partition
//		PARTITION_NAME += suName + '_';
//
//
//		StructuralUnit su = corpus.getStructuralUnit(suName);
//
//
//		// TODO : Debug
//		println ''
//		println 'Structural Unit Name: ' + su.getName();
//
//
//		// Récupération des propriétés d'unités structurelles
//		for (StructuralUnitProperty sup in su.getProperties())	{
//
//
//			if(STRUCTURAL_UNITS_PROPERTIES[i] != '' && sup.getName() == STRUCTURAL_UNITS_PROPERTIES[i])	{
//
//				// Définition auto du nom de partition
//				PARTITION_NAME += STRUCTURAL_UNITS_PROPERTIES[i] + ".";
//
//				// TODO : Debug
//				println "Structural Unit Property: " + sup.getName() + " ";
//
//
//				// Récupération des valeurs des propriétés d'unités structurelles
//
//				//for (supValue in sup.getOrderedValues()) {
//				for (supValue in sup.getOrderedValues()) {
//
//					// Récupération du sous-corpus lié à la valeur de propriété de structure
//					Subcorpus tmpSubcorpus = corpus.createSubcorpusWithQueryString(su, sup, supValue, "tmp");
//
//					String query = suName + '_' + STRUCTURAL_UNITS_PROPERTIES[i] + '="' + supValue + '"';
//
//
//					queries.add(query);
//					partNames.add(supValue);
//
//					// TODO : Debug
//					print supValue + " ";
//
//					// Suppression du sous-corpus temporaire
//					corpus.dropSubcorpus(tmpSubcorpus);
//
//				}
//
//			}
//		}
//
//
////		// Ajout des niveaux inférieurs
////		for(int i = 1; i < STRUCTURAL_UNITS.size(); i++)	{
////
////			for(int j = 0; j < queries.size(); j++)	{
////				queries.set(j, queries.get(j) + ' & _.' + STRUCTURAL_UNITS[i] + '_' + STRUCTURAL_UNITS_PROPERTIES[i] + '=".*"');
////
////				// Noms de partitions
////				partNames.set(j, partNames.get(j) + '_' + STRUCTURAL_UNITS[i] + '_'  + STRUCTURAL_UNITS_PROPERTIES[i]);
////			}
////		}
//
//
//
//
//
//
//		i++;
//	//	}
//	//	else	{
//	//
//	//	}
//
//	}
//
//
//
//
//
//	// Clôture des requêtes
//	for(int j = 0; j < queries.size(); j++)	{
//
//		String queryEnd = '';
//
//		// Suppression de sections
//		for(sectionToIgnore in STRUCTURAL_UNITS_TO_IGNORE)	{
//			queryEnd += ' & !' + sectionToIgnore;
//		}
//
//		queryEnd += ']';
//
//		// Expand to target
//		if(EXPAND_TARGET != null && EXPAND_TARGET != '')	{
//			queryEnd += ' expand to ' + EXPAND_TARGET;
//		}
//
//		queries.set(j, '[_.' +  queries.get(j) + queryEnd);
//	}
//}
















////for (suName in STRUCTURAL_UNITS) {
//if(STRUCTURAL_UNITS.size() > 0 && STRUCTURAL_UNITS.size() == STRUCTURAL_UNITS_PROPERTIES.size())	{
//
//	suName = STRUCTURAL_UNITS[0]
//	//if(STRUCTURAL_UNITS.size() > 1)	{
//
//	// Définition auto du nom de partition
//	PARTITION_NAME += suName + '_';
//
//
//	// TODO : Debug
//	println ''
//	println "Structural Unit Name: $suName"
//
//	// Récupération des valeurs des propriétés d'unités structurelles
//	for (sup in discours.getStructuralUnitProperties(suName.asType(String)))	{
//
//		supName = STRUCTURAL_UNITS_PROPERTIES[0];
//
//		if(supName != '' && sup.getName() == supName)	{
//
//			// Définition auto du nom de partition
//			PARTITION_NAME += supName + ".";
//
//			// TODO : Debug
//			println "Structural Unit Property: " + supName + " ";
//
//			for (supValue in sup.getOrderedValues()) {
//
//				String query = '[_.' + suName + '_' + supName + '="' + supValue + '"';
//
//				// Suppression de sections
//				for(sectionToIgnore in STRUCTURAL_UNITS_TO_IGNORE)	{
//					query += ' & !' + sectionToIgnore;
//				}
//
//
//
//				queries.add(query);
//				partNames.add(supValue);
//
//				// TODO : Debug
//				print supValue + " ";
//			}
//		}
//	}
//
//
//	// Ajout des niveaux inférieurs
//	for(int i = 1; i < STRUCTURAL_UNITS.size(); i++)	{
//
//		for(int j = 0; j < queries.size(); j++)	{
//			queries.set(j, queries.get(j) + ' & _.' + STRUCTURAL_UNITS[i] + '_' + STRUCTURAL_UNITS_PROPERTIES[i] + '=".*"');
//
//			// Noms de partitions
//			partNames.set(j, partNames.get(j) + '_' + STRUCTURAL_UNITS[i] + '_'  + STRUCTURAL_UNITS_PROPERTIES[i]);
//		}
//	}
//
//
//	// Clôture des requêtes
//	for(int j = 0; j < queries.size(); j++)	{
//
//		String queryEnd = ']';
//
//		// Expand to target
//		if(EXPAND_TARGET != null && EXPAND_TARGET != '')	{
//			queryEnd += ' expand to ' + EXPAND_TARGET;
//		}
//
//		queries.set(j, queries.get(j) + queryEnd);
//	}
//
//
//
//	//i++;
////	}
////	else	{
////
////	}
//
////}
//}



// Fonctionnelle pour 1 niveau de hiérarchie
//// Récupération des unités structurelles et création de la liste des requêtes CQL
//int i = 0;
//String supName;
//ArrayList queries = new ArrayList();
//ArrayList partNames = new ArrayList();
//
//for (suName in STRUCTURAL_UNITS) {
//
//
//	// Définition auto du nom de partition
//	NAME += suName + '_';
//
//
//
//	println "Structural Unit Name: $suName"
//
//	// Récupération des valeurs des propriétés d'unités structurelles
//	for (sup in discours.getStructuralUnitProperties(suName.asType(String)))	{
//
//		supName = STRUCTURAL_UNITS_PROPERTIES[i];
//
//		if(supName != '' && sup.getName() == supName)	{
//
//			// Définition auto du nom de partition
//			NAME += supName + ".";
//
//			// TODO : Debug
//			println "Structural Unit Property: " + supName + " ";
//
//			for (supValue in sup.getOrderedValues()) {
//
//				String query = '[_.' + suName + '_' + supName + '="' + supValue + '"';
//
//				// Suppression de sections
//				for(sectionToIgnore in STRUCTURAL_UNITS_TO_IGNORE)	{
//					query += ' & !' + sectionToIgnore;
//				}
//
//				 query += ']';
//
//				// Expand to target
//				if(EXPAND_TARGET != null && EXPAND_TARGET != '')	{
//					query += ' expand to ' + EXPAND_TARGET;
//				}
//
//
//				queries.add(query);
//				partNames.add(supValue);
//
//				// TODO : Debug
//				print supValue + " ";
//			}
//		}
//	}
//	i++;
//}




//// start
//if (queries.size() == partnames.size()) {
////	def discours = CorpusManager.getCorpusManager().getCorpus(CORPUS)
//	def partition;
//	if (SUBCORPUS == null) {
//		println "partition build with $discours"
//		partition = discours.createPartition(NAME, queries.as, PARTNAMES)
//	} else {
//		def subcorpus
//		if (SUBCORPUSQUERY == null) {
//			println "partition build with subcorpus by name $SUBCORPUS"
//			subcorpus = discours.getSubcorpusByName(SUBCORPUS)
//			if (subcorpus == null) {
//				println "Error: SUBCORPUS NOT FOUND with name : $SUBCORPUS"
//			}
//		} else {
//			println "partition build with subcorpus with query $SUBCORPUSQUERY"
//			subcorpus = discours.createSubcorpus(new Query(SUBCORPUSQUERY), SUBCORPUS)
//		}
//		partition = subcorpus.createPartition(NAME, QUERIES, PARTNAMES)
//	}
//
//	def list = Arrays.asList(partition.getPartSizes())
//	println "Partition created $NAME: "+list+" parts"
//	// println "Total size: "+list.sum()+" - is equal to (sub)corpus size : "+(list.sum() == partition.getCorpus().getSize())
//    println "Total size: "+list.sum()+" - (sub)corpus size : "+(partition.getCorpus().getSize())
//
//	monitor.syncExec(new Runnable() {
//	public void run() {
//		CorporaView.refresh();
//		CorporaView.expand(partition.getParent());
//	}
//});
//} else {
//	println "QUERIES.size() != PARTNAMES.size() = "+QUERIES.size()+" != "+PARTNAMES.size()
//}




// parameters
//def CORPUS_NAME = "LIVRETOPERA10TEXTES"
////def SUBCORPUS = "NOMDUSOUSCORPUS"
//def SUBCORPUS = null // si partition sur corpus
////def SUBCORPUSQUERY = '"je" expand to s'
//def SUBCORPUSQUERY = null // si sous-corpus par nom






//def TEXT = "01_ACHILLE ET POLYXENE"
// def TEXT = "02_ZEPHIRE ET FLORE"
/*def QUERIES = [
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="1" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="2" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="3" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="4" & _.sp_who=".*"]+</lg>',
'<lg>[_.text_id="' + TEXT + '" & _.div1_n="5" & _.sp_who=".*"]+</lg>',
]
def PARTNAMES = [
"acte_1",
"acte_2",
"acte_3",
"acte_4",
"acte_5",
]*/
