package org.txm.scripts.test

import org.txm.functions.mesures.*;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
//import org.txm.rcp.commands.*;

def corpus1 = CorpusManager.getCorpusManager().getCorpus("ALCESTE")
def corpus2 = CorpusManager.getCorpusManager().getCorpus("DISCOURS")
def corpus3 = CorpusManager.getCorpusManager().getCorpus("RENOUEE")
Mesures mesures = new Mesures([corpus1, corpus2, corpus3]);
mesures.add(new Magnitude('"je"%c expand to text', "word", false, Synthese.COUNTMATCHES));
mesures.add(new Magnitude('"je"%c expand to text', "word", false, Synthese.SUM));
mesures.add(new Magnitude('"je"%c expand to text', "word", true, Synthese.MEAN));
mesures.add(new Magnitude('"je"%c expand to text', "word", true, Synthese.QUARTILE));
mesures.add(new Proportion('"j.*"%c expand to text', "word", true, Synthese.COUNTMATCHES, '"j.*" expand to text', ['"j.....*"%c expand to text', '"j........*"%c expand to text']));
mesures.add(new Proportion('"j.*"%c expand to text', "word", true, Synthese.SUM, '"j.*" expand to text', ['"j.....*"%c expand to text', '"j........*"%c expand to text']));
mesures.add(new PresenceRate('"j.*" expand to text', "word", true, Synthese.SUM, '"je"'));
mesures.add(new PresenceRate('"j.*" expand to text', "word", true, Synthese.MIN, '"je"'));

File csvFile = new File("mesures.txt");
mesures.export(csvFile, "UTF-8", "\t", "");

monitor.syncExec(new Runnable() {
	public void run() {
	//	EditFile.openfile(csvFile);
	}
})

mesures.prettyPrint();