package org.txm.scripts.test

import org.txm.stat.engine.r.*
import org.txm.stat.engine.r.data.*
import org.txm.statsengine.r.core.data.MatrixImpl

def r = RWorkspace.getRWorkspaceInstance()
r.setLog(true)

int[][] data = [[1,2,3,4,0], [78,64,18,3,0], [2,84,7,168,0], [5,7,6,0,0], [12,4,88,1668,0], [0,0,0,0,1]]
String[] lineNames = ["L1", "L2", "L3", "L4", "L5", "L6"]
String[] colNames = ["C1", "C2", "C3", "C4", "C5"]

MatrixImpl matrix = new MatrixImpl(data, lineNames, colNames)
matrix.print()

//println "remove rows 1"
//matrix.removeRows([1], true)
//matrix.print()

//println "remove cols 4"
//matrix.removeCols([4], true)
//matrix.print()

//println "merge rows 0 1"
//int[] rows = [0,1]
//matrix.mergeRows(rows);
//matrix.print()

//println "merge cols 0 1"
//int[] cols = [0,1]
//matrix.mergeCols(cols);
//matrix.print()

//println "sortByFreqs"
//matrix.sortByFreqs();
//matrix.print()

//println "filter fmin=100 nlines=4"
//matrix.filter(5,100);
//matrix.print()

println "save data in test.txt"
matrix.exportData(new File("test.txt"), ";", "\"")
println "do something: delete col 4"
matrix.removeCols([4], true)
matrix.print()

println "reload data from test.txt"
matrix.importData(new File("test.txt"))
matrix.print()

r.setLog(false)