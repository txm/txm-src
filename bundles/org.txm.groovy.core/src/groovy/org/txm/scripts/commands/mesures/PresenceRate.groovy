package org.txm.scripts.functions.mesures;
import java.util.Arrays;

public class PresenceRate extends Proportion {
	public PresenceRate(String unit, def syntheses, String all, String cas) {
		super(all, unit, syntheses, all, Arrays.asList(cas));
//remplace momentanément la ligne suivante, pour pouvoir tourner sur une liste de corpus
//		super(null, unit, syntheses, all, Arrays.asList(cas));
	}
}
