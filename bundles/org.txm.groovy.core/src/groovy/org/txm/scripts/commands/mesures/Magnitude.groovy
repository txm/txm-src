package org.txm.scripts.functions.mesures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.Query;

/**
 * 
 * Magnitude synonymes
extent	Magnitude, importance, extension, superficie, longueur
magnitude	Magnitude, grandeur, magnitude
amplitude	amplitude, Magnitude, largeur
fullness	plénitude, Magnitude, abondance
width	largeur, Magnitude
fulness	plénitude, Magnitude, abondance
richness	richesse, abondance, Magnitude, luxe, fertilité
ampleness	Magnitude
thoroughness	minutie, Magnitude
roundness	rondeur, circularité, Magnitude, franchise

 * @author mdecorde
 *
 */
public class Magnitude extends Mesure {

	private static final long serialVersionUID = 3317066047569716511L;

	private static String TMP = "TMP";
	
	public Magnitude(String domaine, String unit, Synthese[] syntheses) {
		this.domaine = domaine;
		this.unit= unit;
		this.syntheses = syntheses;

		//if (synthese.equals(Synthese.NONE)) this.synthese = Synthese.SUM; // NO synthese will sum the matches size
	}

	@Override
	public boolean compute(ArrayList<Corpus> corpora) {
		this.corpora = corpora;

		//		switch (synthese) {
		//		case COUNTMATCHES:
		//			return computeCounts();
		//		case MAX:
		//		case MIN:
		//		case MEAN:
		//		case QUARTILE1:
		//		case QUARTILE3:
		//		case MEDIAN:
		//		case NONE:
		//		case SUM:
		return computeSynthese();
		//		default:
		//			System.out.println("UNSUPPORTED SYNTHESIS: "+synthese);
		//			return false;
		//		}
	}

	//	private boolean computeCounts() {
	//		Integer[] rez = new Integer[corpora.size()];
	//		for (int i = 0 ; i < corpora.size() ; i++) {
	//			Corpus corpus = corpora.get(i);
	//			try {
	//				rez[i] = computeCount(corpus);
	//			} catch (CqiClientException e) {
	//				e.printStackTrace();
	//				return false;
	//			}
	//		}
	//		this.add(new MesureResult(this, rez, "Magnitude synt="+synthese+" unit="+unit+" dom="+domaine));
	//		return true;
	//	}

	//	private int computeCount(Corpus corpus) throws CqiClientException {
	//		QueryResult result = corpus.query(new Query(domaine), TMP);
	//		return result.getNMatch();
	//	}
	HashMap<Synthese, Object[]> rezPerSynthese = new HashMap<Synthese, Object[]>();
	private boolean computeSynthese() {

		for (Synthese synt : syntheses) {
			rezPerSynthese.put(synt, new Object[corpora.size()]);
		}

		for (int i = 0 ; i < corpora.size() ; i++) {
			Corpus corpus = corpora.get(i);
			try {
				computeSynthese(i, corpus);
			} catch (CqiClientException e) {
				e.printStackTrace();
				return false;
			}
		}
		for ( Synthese synt : syntheses) {
			this.add(new MesureResult(this, rezPerSynthese.get(synt), "Magnitude synt="+synt+" unit="+unit+" dom="+domaine));
		}
		return true;
	}

	private void computeSynthese(int icorpus, Corpus corpus) throws CqiClientException {
        QueryResult result = corpus.query(new Query(domaine), TMP, false);		
		List<Match> matches = result.getMatches();
		Integer[] sizes = new Integer[matches.size()];
		for (int i = 0 ; i < matches.size() ; i++) {
			Match m  = matches.get(i);
			sizes[i] = m.getEnd() - m.getStart() + 1;
		}
		
		builder = new SyntheseBuilder(sizes);
		for (Synthese synt : syntheses) {
			Object[] rez = rezPerSynthese.get(synt);
			if (synt.equals(Synthese.NONE)) {
				rez[icorpus] = builder.doSynthese(Synthese.SUM);
			} else {
				rez[icorpus] = builder.doSynthese(synt);
			}
		}
	}
}
