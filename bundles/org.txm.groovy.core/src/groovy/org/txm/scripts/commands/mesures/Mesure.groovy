package org.txm.scripts.functions.mesures;

import java.util.ArrayList;

import org.txm.searchengine.cqp.corpus.Corpus;

public abstract class Mesure extends ArrayList<MesureResult> {
	
	private static final long serialVersionUID = 4804073482551008502L;
	
	protected String domaine;
	protected String unit;
	protected Synthese[] syntheses;
	protected SyntheseBuilder builder;

	protected ArrayList<Corpus> corpora;
	
	public abstract boolean compute(ArrayList<Corpus> subcorpora);
}
