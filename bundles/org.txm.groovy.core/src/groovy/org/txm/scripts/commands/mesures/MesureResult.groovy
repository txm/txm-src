package org.txm.scripts.functions.mesures;

public class MesureResult {
	public Mesure from;
	public Object[] data;
	public String name;
	
	public MesureResult(Mesure from, Object[] data, String name) {
		this.from = from;
		this.data = data;
		this.name = name;
	}
}