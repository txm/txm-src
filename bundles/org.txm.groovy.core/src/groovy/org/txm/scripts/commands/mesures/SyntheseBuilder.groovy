package org.txm.scripts.functions.mesures;

import java.util.Arrays;

import org.apache.tools.ant.types.resources.selectors.InstanceOf;

public class SyntheseBuilder {
	private Integer[] isrc;
	private Integer[] isorted;
	private Float[] fsrc;
	private Float[] fsorted;

	public SyntheseBuilder(Integer[] rez) {
		assert(rez.length > 0);
		isrc = rez;
		isorted = new Integer[rez.length];
		System.arraycopy(rez, 0, isorted, 0, rez.length);
		Arrays.sort(isorted);
	}

	public SyntheseBuilder(Float[] rez) {
		assert(rez.length > 0);
		fsrc = rez;
		fsorted = new Float[rez.length];
		System.arraycopy(rez, 0, fsorted, 0, rez.length);
		Arrays.sort(fsorted);
	}

	public Number doSynthese(Synthese synt) {
		if (isorted != null) {
			return doSynthese(synt, isrc);
		} else {
			return doSynthese(synt, fsrc);
		}
	}

	private Integer[] getSorted(Integer[] rez) {
		return isorted;
	}

	private Float[] getSorted(Float[] rez) {
		return fsorted;
	}

	private Number[] getSorted(Number[] rez) {
		if (isorted != null) return isorted;
		return fsorted;
	}

	public Number doSynthese(Synthese synt, Number[] rez) {
		if (rez instanceof Integer[])
			return doISynthese(synt, rez);
		else if  (rez instanceof Float[])
			return doFSynthese(synt, rez);

		println "Error: doSynthese unsuported array type: "+rez.getClass();
		return -1;
	}

	public Number doISynthese(Synthese synt, Integer[] rez) {
		switch (synt) {
			case Synthese.MIN: return min(rez);
			case Synthese.MAX: return max(rez);
			case Synthese.MEAN: return mean(rez);
			case Synthese.QUARTILE1: return quartile1(rez);
			case Synthese.MEDIAN: return median(rez);
			case Synthese.QUARTILE3: return quartile3(rez);
			case Synthese.SUM: return sum(rez);
			case Synthese.COUNTMATCHES: return rez.length;
			case Synthese.FIRST:
				if (rez.length > 0) return rez[0];
				else return -1;
			case Synthese.LAST:
				if (rez.length > 0) return rez[(int)rez.length -1];
				else return -1;
			case Synthese.MIDDLE:
				if (rez.length > 0) return rez[(int)rez.length/2];
				else return -1;
		}
		return null;
	}

	public Number doFSynthese(Synthese synt, Float[] rez) {
		switch (synt) {
			case Synthese.MIN: return min(rez);
			case Synthese.MAX: return max(rez);
			case Synthese.MEAN: return mean(rez);
			case Synthese.QUARTILE1: return quartile1(rez);
			case Synthese.MEDIAN: return median(rez);
			case Synthese.QUARTILE3: return quartile3(rez);
			case Synthese.SUM: return sum(rez);
			case Synthese.COUNTMATCHES: return rez.length;
			case Synthese.FIRST:
				if (rez.length > 0) return rez[0];
				else return -1;
			case Synthese.LAST:
				if (rez.length > 0) return rez[(int)rez.length -1];
				else return -1;
			case Synthese.MIDDLE:
				if (rez.length > 0) return rez[(int)rez.length/2];
				else return -1;
		}
		return null;
	}

	public Number min(Number[] obj) {
		obj = getSorted(obj);
		return obj[0];
	}

	public Number max(Number[] obj) {
		obj = getSorted(obj);
		return obj[obj.length - 1];
	}

	public Number sum(Float[] obj) {
		Float sum = 0.0f;
		for (int i = 0 ; i < obj.length ; i++)
			sum += obj[i];
		return sum;
	}

	public Number sum(Integer[] obj) {
		Integer sum = 0;
		for (int i = 0 ; i < obj.length ; i++)
			sum += obj[i];
		return sum;
	}

	public Number mean(Integer[] obj) {
		Integer i = (Integer) sum(obj);
		return (float) i / (float)obj.length;
	}

	public Number mean(Float[] obj) {
		Float f = (Float) sum(obj);
		return (float) f / obj.length;
	}

	public Number quartile1(Number[] obj) {
		obj = getSorted(obj);
		int len = obj.length;
		return obj[(int)len/4];
	}

	public Number quartile3(Number[] obj) {
		obj = getSorted(obj);
		int len = obj.length;
		return obj[(int)len/4 + len/2];
	}

	public Number median(Number[] obj) {
		obj = getSorted(obj);
		int len = obj.length;
		return obj[(int)len/2];
	}
}
