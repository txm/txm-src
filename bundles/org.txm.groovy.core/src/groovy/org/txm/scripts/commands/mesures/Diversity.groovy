package org.txm.scripts.functions.mesures;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.txm.Toolbox;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.Query;
import org.txm.searchengine.cqp.serverException.CqiServerError;

public class Diversity extends Mesure {

	private static final long serialVersionUID = -375293299211271669L;
	
	public static List<Synthese> SUPORTED = Arrays.asList(Synthese.SUM);
	private String query;
	private String property;

	public Diversity(String domaine, String query, String property) {
		this.domaine = domaine;
		this.query = query;
		this.property = property;
	}

	@Override
	public boolean compute(ArrayList<Corpus> corpora) {
		this.corpora = corpora;
		Integer[] rez = new Integer[corpora.size()];
		for (int icorpus = 0 ; icorpus < corpora.size() ; icorpus++) {
			try {
				Corpus corpus = corpora.get(icorpus);
				rez[icorpus] = computeForCorpus(corpus);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}	
		}
		this.add(new MesureResult(this, rez, "Diversite: "+query+" "+property));
		return true;
	}
	
	private Integer computeForCorpus(Corpus corpus) throws CqiClientException, IOException, CqiServerError {
		if (domaine != null) corpus = corpus.createSubcorpus(new Query(domaine), "DOMAINE");
		
		Property p = corpus.getProperty(property);
		QueryResult result = corpus.query(new Query(query), "TMP", false);
		int[] starts = result.getStarts();
		int[] ids = CQPSearchEngine.getCqiClient().cpos2Id(p.getQualifiedName(), starts);
		HashSet<Integer> set = new HashSet<Integer>();
		for (int i : ids) set.add(i); // no double
		return set.size();
	}
}
