package org.txm.scripts.functions.mesures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.Query;

public class Proportion extends Mesure {

	private static final long serialVersionUID = -3160461437451974147L;

	public static List<Synthese> SUPORTED = Arrays.asList(Synthese.COUNTMATCHES, 
			Synthese.SUM);

	protected String all;
	protected List<String> cas;

	public Proportion(String domaine, String unit, def syntheses, String all, List<String> cas) {
		this.domaine = domaine;
		this.unit= unit;
		this.syntheses = syntheses;

		this.all = all;
		this.cas = cas;
	}

	@Override
	public boolean compute(ArrayList<Corpus> arg0) {
		this.corpora = arg0;
		//		switch (synthese) {
		//		case COUNTMATCHES:
		//			return computeMatches();
		//		case SUM:
		//			return computeSum();
		//		default:
		//			System.out.println("UNSUPPORTED SYNTHESIS: "+synthese);
		//			return false;
		//		}
		return computeSynthese();
	}

	HashMap<Synthese, ArrayList<Object[]>> rezsPerSynt = new HashMap<Synthese, ArrayList<Object[]>>();
	private boolean computeSynthese() {

		// initialize list of results 
		for (Synthese synt : syntheses) {
			ArrayList<Object[]> rezs = new ArrayList<Object[]>();
			for (String c : cas) {
				rezs.add(new Number[corpora.size()]);
			}
			rezsPerSynt.put(synt, rezs);
		}

		for (int icorpus = 0 ; icorpus < corpora.size() ; icorpus++) {
			computeForCorpus(icorpus, corpora.get(icorpus)); // one cell per cas
			//			Object[] r = compute(icorpus, corpora.get(icorpus)); // one cell per cas
			//			for( int i = 0 ; i < cas.size() ; i++)
			//				rezs.get(i)[icorpus] = r[i];
		}

		// register results
		for (Synthese synt : syntheses) {
			ArrayList<Object[]> rezs = rezsPerSynt.get(synt);
			for (int i = 0 ; i < cas.size() ; i++) {
				Object[] rez = rezs.get(i);
				this.add(new MesureResult(this, rez, "Proportion synt="+synt+" domaine="+domaine+" cas="+cas.get(i)+" all="+all+")"));
			}
		}
		return true;
	}

	private Integer[] countMatchesInMatches(List<Match> domaine, List<Match> cas) {
		//		System.out.println("count dom.size "+domaine.size()+" cas.size "+cas.size());
		//		System.out.println(domaine);
		//		System.out.println(cas);

		Integer[] rez = new Integer[domaine.size()];
		int iCas = 0;
		int iDom = 0;
		rez[0] = 0;

		int firsterror = 0;
		while(iCas < cas.size() & iDom < domaine.size()) {
			Match mDom = domaine.get(iDom);
			Match mCas = cas.get(iCas);

			if (mDom.getStart() <= mCas.getStart() & mCas.getEnd() <= mDom.getEnd()) {
				rez[iDom] = rez[iDom] + 1; 
				iCas++;
			} else if ( mCas.getStart() < mDom.getStart()) {
				iCas++;
			} else if ( mDom.getEnd() < mCas.getEnd()) {
				iDom++;
				rez[iDom] = 0;
			} else {
				if (firsterror++ == 0)
					System.out.println("ERROR MATCH COUNT: dom="+domaine+" cas="+cas);
				System.out.println("ERROR idom="+iDom+" icas="+iCas);
			}
		}
		//		for(int i = 0 ; i < rez.length ; i++) {
		//			Match m = domaine.get(i);
		//			while (currentMatchCas != null) {
		//				if (m.contains(currentMatchCas)) {
		//					rez[i] = rez[i]+1;
		//				} else {
		//					break; // next domaine match
		//				}
		//				// next cas match
		//				if (iCas == cas.size()) break;
		//				else currentMatchCas = cas.get(iCas++);
		//			}
		//		}

		// complete counts
		for (int i = 0 ; i < rez.length ; i++)
			if (rez[i] == null) rez[i] = 0;

		return rez;
	}

	private void computeForCorpus(int icorpus, Corpus corpus) {
		HashMap<Synthese, Number> allSyntheses = new HashMap<Synthese, Number>();
		HashMap<Synthese, Number[]> casSyntheses = new HashMap<Synthese, Number[]>();
		for (Synthese synt : syntheses) {
			casSyntheses.put(synt, new Number[cas.size()]);
		}
		//Object[] rez = new Object[cas.size()];
		try {
			Corpus domaineSub = corpus;
			if (domaine != null && domaine.length() > 0)
				domaineSub = corpus.createSubcorpus(new Query(domaine), "domaine");
			else 
				domaine = corpus.getName();
			//System.out.println("domaineSub.name "+ domaineSub.getName());
			//System.out.println("domaine "+ domaine);
			List<Match> domaineMatches = domaineSub.getMatches();
			//System.out.println("domaineMatches.size"+ domaineMatches.size());
			// first process the all case

			QueryResult allSub = domaineSub.query(new Query(all), "all", false);
			Integer[] allCounts = countMatchesInMatches(domaineMatches, allSub.getMatches());
			//System.out.println("allCounts.length "+allCounts.length+" count : "+Arrays.toString(allCounts));
			builder = new SyntheseBuilder(allCounts);
			for( Synthese synt : syntheses) {
				Synthese s = synt;
				if (s.equals(Synthese.NONE)) s = Synthese.SUM;
				allSyntheses.put(synt, builder.doSynthese(s));
			}

			// then process each cas
			for (int iCas = 0 ; iCas < cas.size() ; iCas++) {
				String c = cas.get(iCas);
				QueryResult result = domaineSub.query(new Query(c), "TMP", false);
				Integer[] counts = countMatchesInMatches(domaineMatches, result.getMatches());

				Float[] rapports = new Float[counts.length];
				for (int i = 0 ; i < rapports.length ; i++) {
					if (allCounts[i] == 0) rapports[i] = 0.0f;
					else rapports[i] = (float)counts[i] / (float)allCounts[i];
				}
				
				for (Synthese synt : syntheses) {
					
					if (synt.equals(Synthese.NONE)) {
						int allTotal = 0;
						for( int i : allCounts) allTotal += i;
						int casTotal = 0;
						for( int i : counts) casTotal += i;
								
						ArrayList<Object[]> rez = rezsPerSynt.get(synt);
						rez.get(iCas)[icorpus] = (float)casTotal / (float)allTotal;
					} else {
						
						builder = new SyntheseBuilder(rapports);

						Synthese s = synt;
						if (s.equals(Synthese.NONE)) s = Synthese.SUM;

						ArrayList<Object[]> rez = rezsPerSynt.get(synt);
						rez.get(iCas)[icorpus] = builder.doSynthese(s);
					}
				}
			}

			if (!domaineSub.equals(corpus)) { // cleaning
				domaineSub.delete();
			}
		} catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
