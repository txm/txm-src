package org.txm.scripts.functions.mesures;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.txm.searchengine.cqp.corpus.Corpus;
import org.txm.searchengine.cqp.corpus.Partition;

/**
 * Container of mesures
 * 
 * @author mdecorde
 *
 */
public class Mesures extends ArrayList<Mesure>{

	private static final long serialVersionUID = 324346022230541761L;

	ArrayList<Corpus> data = new ArrayList<Corpus>();

	public Mesures(Corpus corpus) {
		data = new ArrayList<Corpus>();
		data.add(corpus);
	}

	public Mesures(ArrayList<Corpus> corpora) {
		data = corpora;
		println "hello TXM"
	}

	public Mesures(Partition partition) {
		data.addAll(partition.getParts());
	}

	public boolean add(Mesure mesure) {
		if (!mesure.compute(data)) {
			return false;
		}

		return super.add(mesure);
	}

	public boolean export(Writer writer, String encoding, String colseparator, String txtseparator) throws IOException {
		String line = "Mesures";
		for (Corpus c : data) {
			line += colseparator+txtseparator+c.getName().replaceAll(txtseparator,txtseparator+txtseparator)+txtseparator;
		}
		writer.write(line+"\n");

		for (Mesure m : this) {
			for (MesureResult r : m) {
				line = r.name;
				for (int i = 0 ; i < r.data.length ; i++)
					line += colseparator+r.data[i];
				writer.write(line+"\n");
			}
		}

		return true;
	}

	public boolean export(File outfile, String encoding, String colseparator, String txtseparator) throws IOException {
		Writer writer = new OutputStreamWriter(
				new FileOutputStream(outfile), encoding); //$NON-NLS-1$
		boolean rez = export(writer, encoding, colseparator, txtseparator);
		writer.close();
		return rez;
	}

	DecimalFormat floatFormatter = new DecimalFormat("###,###.##");
	DecimalFormat intFormatter = new DecimalFormat("###,###,###");
	public void prettyPrint() {	
		ArrayList<String[]> lines = new ArrayList<String[]>();
		int[] maxx = new int[data.size()+1];
		int[] maxxDecimales = new int[data.size()+1];


		String[] line = new String[data.size() + 1];
		line[0] = "Mesures";
		maxx[0] = line[0].length();
		for (int i = 1 ; i <= data.size() ; i++) {
			line[i] = data.get(i-1).getName();
			maxx[i] = line[i].length();
		}
		lines.add(line);

		// fill lines
		for (Mesure m : this) {
			for(MesureResult r : m) {
				line = new String[data.size()+1];
				line[0] = r.name;
				if (maxx[0] < r.name.length())
					maxx[0] = r.name.length();

				int i = 1;
				for (Object o : r.data) {
					String str = ""+o;
					if (o instanceof Integer) str = intFormatter.format(o);
					else if (o instanceof Float) str = floatFormatter.format(o);

					line[i] = str;

//					if (maxx[i] < str.length())
//						maxx[i] = str.length();

					int idxDecimal = str.indexOf(",");
					if (idxDecimal > 0) {
						idxDecimal = str.length() - idxDecimal;
						if (maxxDecimales[i] < idxDecimal)
							maxxDecimales[i] = idxDecimal;
					}
					i++;
				}
				lines.add(line);
			}
		}

//		System.out.println("maxx: "+Arrays.toString(maxx));
//		System.out.println("maxxDecimales: "+Arrays.toString(maxxDecimales));

		// fix cells right lengths (decimals)
		boolean notfirst = false;
		for (String[] l : lines) {
			for (int i = 1 ; i < data.size() +1; i++) {
				String str = l[i];
				int nbDecimalToAdd = 0;

				if (str.indexOf(",") < 0) {
					nbDecimalToAdd = maxxDecimales[i];
				} else {
					nbDecimalToAdd = maxxDecimales[i] - (str.length() - str.indexOf(","));
				}

				if (notfirst)
				for (int j = 0 ; j < nbDecimalToAdd ; j++)
					str += " ";

				if (maxx[i] < str.length())
					maxx[i] = str.length();
				
				l[i] = str;
			}
			notfirst = true;
		}
		
		// fix cells left lengths (alignment)
		for (String[] l : lines) {
			for (int i = 0 ; i < data.size() +1; i++) {
				while (l[i].length() < maxx[i]) {
					if (i == 0)	l[i] = ""+l[i]+" ";
					else l[i] = " "+l[i];
				}
			}
		}

		// print cells
		for (String[] l : lines) {
			for (int i = 0 ; i < data.size() +1; i++) {
				if (i == 0) System.out.print(l[i]);
				else  System.out.print(" "+l[i]);
			}
			System.out.println("");
		}
	}
}
