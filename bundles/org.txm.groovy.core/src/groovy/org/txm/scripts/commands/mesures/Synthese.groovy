package org.txm.scripts.functions.mesures;

public enum Synthese {
	
	NONE,
	COUNTMATCHES,
	SUM,
	MEAN,
	MIN,
	QUARTILE1,
	MEDIAN,
	QUARTILE3,
	MAX,
	FIRST,
	LAST,
	MIDDLE,
}