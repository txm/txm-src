// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.scripts.tigersearch;

import org.txm.sw.ReplaceXmlDomNode;

import java.io.File;
import java.util.ArrayList;
import groovy.xml.XmlParser

// TODO: Auto-generated Javadoc
/**
 * The Class SubCorpusToSimpleCorpus.
 */
class SubCorpusToSimpleCorpus
{
	
	/** The srcdir. */
	File srcdir;
	
	/** The outdir. */
	File outdir;
	
	/** The files. */
	ArrayList<File> files = new ArrayList<File>();
	
	/** The master. */
	File master;
	
	/**
	 * Instantiates a new sub corpus to simple corpus.
	 *
	 * @param srcdir the srcdir
	 * @param outdir the outdir
	 */
	public SubCorpusToSimpleCorpus(File srcdir, File outdir)
	{
		this.srcdir = srcdir;
		this.outdir = outdir;
	}
	
	/**
	 * Scan.
	 *
	 * @return true, if successful
	 */
	public boolean scan()
	{
		if(!srcdir.exists())
		{
			System.out.println("Cant find srcdir: "+srcdir);			
		}
		
		outdir.mkdir();
		if(!outdir.exists())
		{
			System.out.println("Cant find outdir: "+srcdir);			
		}	
		
		master = new File(srcdir, "master.xml");
		def root = new XmlParser().parse(master);
		for(def subcorpus : root.body.subcorpus)
		{
			String filename = subcorpus.@external
			filename = filename.substring(5);
			File f = new File(srcdir, filename);
			if(f.exists())
				files.add(f);
			else
				println("Missing file : "+f);
		}
		return true;
	}
	
	/**
	 * Fusion.
	 *
	 * @return true, if successful
	 */
	public boolean fusion()
	{
		for(File f : files)
		{
			println "replace "+f.getName();
			String filename = f.getName();
			File target = master;
			
			String xpathsrc = "//subcorpus"
				
			String xpathtarget = "//subcorpus[@external=\"file:"+filename+"\"]"
			
			new ReplaceXmlDomNode(f, target, xpathsrc, xpathtarget)
		}
	}
	
	/**
	 * Gets the files.
	 *
	 * @return the files
	 */
	public ArrayList<File> getFiles()
	{
		return files;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File srcdir = new File("/home/mdecorde/xml/beroul_bp")
		File outdir = new File("/home/mdecorde/xml/beroul_bp/merged") // not used
		def fusioner = new SubCorpusToSimpleCorpus(srcdir, outdir);
		if(fusioner.scan())
		{
			println "subcorpus files : "+fusioner.getFiles()
			fusioner.fusion();
		}
	}
}