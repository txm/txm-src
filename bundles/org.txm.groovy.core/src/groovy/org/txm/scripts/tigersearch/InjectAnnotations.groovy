// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts.tigersearch;

import java.text.DateFormat;
import java.util.Date;
import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;
import org.txm.importer.scripts.filters.*;

// TODO: Auto-generated Javadoc
/**
 * The Class InjectAnnotations.
 *
 * @author mdecorde
 * 
 * inject annotations into ONE file
 */

public class InjectAnnotations {
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;

	/** The reader. */
	private Reader reader;

	/** The output. */
	private def output;

	/** The solotags. */
	ArrayList<String> solotags;
	
	/** The lespos. */
	HashSet<String> lespos = new HashSet<String>();

	/**
	 * Instantiates a new inject annotations.
	 *
	 * @param url the url
	 * @param annotations the annotations
	 * @param solotags the solotags
	 */
	public InjectAnnotations(URL url, File annotations,
			ArrayList<String> solotags) {
		try {
			this.url = url;
			this.solotags = solotags;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);

			reader = new FileReader(annotations);

		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			File f = outfile;
			output = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the next annotation.
	 *
	 * @return the next annotation
	 */
	private String getNextAnnotation() {
		String line = reader.readLine();
		while (line.startsWith("<"))
			line = reader.readLine();
		lespos.add(line.split("\t")[1]);
		return line = line.split("\t")[1];
	}

	/**
	 * Process.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	public boolean process(File outfile) {
		if (createOutput(outfile)) {

			String lastopenlocalname = "";
			String localname = "";
			try {
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser
						.next()) {

					switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();
						String prefix = parser.getPrefix();
						if (prefix == null || prefix == "")
							prefix = "";
						else
							prefix += ":";

						lastopenlocalname = localname;
						output.write("\n<" + prefix + localname);

						for (int i = 0; i < parser.getAttributeCount(); i++)
							output.write(" " + parser.getAttributeLocalName(i)
									+ "=\"" + parser.getAttributeValue(i)
									+ "\"");

						// get annotation
						if (localname.equals("t"))
							output.write(" pos=\"" + getNextAnnotation()
											+ "\"");

						if (solotags.contains(localname))
							output.write("/>");
						else
							output.write(">");
						break;

					case XMLStreamConstants.END_ELEMENT:

						localname = parser.getLocalName();
						String prefix = parser.getPrefix();
						if (prefix == null || prefix == "")
							prefix = "";
						else
							prefix += ":";

						switch (localname) {

						default:
							if (!solotags.contains(localname))
								if (lastopenlocalname.equals(localname))
									output.write("</" + prefix + localname
											+ ">");
								else
									output.write("\n</" + prefix + localname
											+ ">");
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						output.write(parser.getText().trim());
						break;
					}
				}
				output.close();
				
			} catch (XMLStreamException ex) {
				System.out.println(ex);
			} catch (IOException ex) {
				System.out.println("IOException while parsing " + inputData);
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
	}

	/**
	 * Gets the feature.
	 *
	 * @param f the f
	 * @return the feature
	 */
	public void getFeature(File f)
	{
		Writer writer = new OutputStreamWriter(new FileOutputStream(f) , "UTF-8");
		writer.write("<feature name=\"pos\" domain=\"T\">\n")
		for(String pos : lespos)
			writer.write("<value name=\""+pos+"\"></value>\n");
		writer.write("</feature>\n")
		writer.close();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/beroul/";
		new File(rootDir + "/identity/").mkdir();

		ArrayList<String> milestones = new ArrayList<String>();// the tags who
		// you want them
		// to stay
		// milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		milestones.add("catRef");

		File srcfile = new File(rootDir, "beroul.xml");
		File annotationsfiles = new File(rootDir, "result.tt");
		File resultfile = new File(rootDir, "beroul-result.xml");
		println("identity file : " + srcfile + " to : " + resultfile);

		def builder = new InjectAnnotations(srcfile.toURL(), annotationsfiles,
				milestones);
		builder.process(resultfile);

		return;
	}

}
