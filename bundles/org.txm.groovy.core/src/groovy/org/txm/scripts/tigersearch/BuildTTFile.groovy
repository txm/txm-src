// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts.tigersearch;

import java.text.DateFormat;
import java.util.Date;
import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;
import org.txm.importer.scripts.filters.*;

// TODO: Auto-generated Javadoc
/**
 * The Class BuildTTFile.
 *
 * @author mdecorde
 * 
 * build the TT source for tigerSearch
 */

public class BuildTTFile {
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;

	/** The output. */
	private def output;

	/** The solotags. */
	ArrayList<String> solotags;

	/**
	 * Instantiates a new builds the tt file.
	 *
	 * @param url the url
	 * @param solotags the solotags
	 */
	public BuildTTFile(URL url, ArrayList<String> solotags) {
		try {
			this.url = url;
			this.solotags = solotags;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);

		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			File f = outfile;
			output = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Process.
	 *
	 * @param outfile the outfile
	 * @param targetbalise the targetbalise
	 * @return true, if successful
	 */
	public boolean process(File outfile, String targetbalise) {
		if (createOutput(outfile)) {

			String lastopenlocalname = "";
			String localname = "";
			try {
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser
						.next()) {

					switch (event) {

					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();
						switch (localname) {
						case targetbalise:
							String word = parser
									.getAttributeValue(null, "word");
							output.write(word + "\n");
							break;
						case "s":
							output.write("<s>\n");
							break;
						}

					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						switch (localname) {

						case targetbalise:

							break;
						case "s":
							output.write("</s>\n");
							break;
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						// output.write(parser.getText().trim());
						break;
					}
				}
				output.close();
			} catch (XMLStreamException ex) {
				System.out.println(ex);
			} catch (IOException ex) {
				System.out.println("IOException while parsing " + inputData);
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/beroul/";
		// new File(rootDir+"/identity/").mkdir();

		ArrayList<String> milestones = new ArrayList<String>();// the tags who
		// you want them
		// to stay
		// milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		milestones.add("catRef");

		File srcfile = new File(rootDir, "beroul.xml");
		File resultfile = new File(rootDir, "beroul.tt");
		println("identity file : " + srcfile + " to : " + resultfile);

		def builder = new BuildTTFile(srcfile.toURL(), milestones);
		builder.process(resultfile, "t");

		return;
	}

}
