/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts.tigersearch;

import org.txm.utils.treetagger.TreeTagger;
import org.txm.importer.scripts.xmltxm.*;

// TODO: Auto-generated Javadoc
/**
 * script to insert annotation into beroul file.
 *
 * @return the java.lang. object
 */

String home = System.getProperty("user.home")
File rootDir = new File(home, "xml/fullberoul/")

ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
milestones.add("tagUsage");
milestones.add("pb");
milestones.add("lb");
milestones.add("catRef");

//transform xml tiger >> TTsrc
File srcfile = new File(rootDir,"beroul.xml");
File resultfile = new File(rootDir,"beroul.tt");
println("xml>>TT from : "+srcfile+" to : "+resultfile );

def builder = new BuildTTFile(srcfile.toURL(), milestones);
builder.process(resultfile, "t");

//tag TT
String infile = resultfile;
String modelfile = home+"/treetagger/models/fro.par";
String outfile = rootDir.getAbsolutePath()+"/result.tt";

println("proj "+modelfile+ " on " +resultfile +" >> "+outfile);

TreeTagger tt = new TreeTagger(home+"/treetagger/bin/");
tt.settoken();
tt.setquiet();
tt.setsgml();
tt.seteostag("<s>");
tt.treetagger( modelfile, infile, outfile)

//inject new TTattributes
//File srcfile = new File(rootDir,"beroul.xml");
File annotationsfiles = new File(rootDir,"result.tt");
File lastresultfile = new File(rootDir,"beroul-result.xml");
println("insert TT annotations : "+srcfile+" to : "+resultfile );

builder = new InjectAnnotations(srcfile.toURL(),annotationsfiles, milestones);
builder.process(lastresultfile);

builder.getFeature(new File(rootDir,"feature.xml"));
/*
//TAG with TnT
//need to replace <s> by nothing and </s> by \n
String encoding = "UTF-8"
for(String text : texts)
{
	//patch src files
	File f = new File(textsDir,text+".t");
	File temp = new File("tempFileCVScleaner")
	println("patch texts files "+f+": rmv <s> and replace </s>");
	Reader reader = new InputStreamReader(new FileInputStream(f),encoding);
	Writer writer = new FileWriter(temp);
	reader.eachLine 
			{
				if(it.trim().startsWith("</s"))
					writer.write("\n")
				else if(it.trim().startsWith("<s"))
					writer.write("")
				else
					writer.write(it+"\n")
			}
	reader.close();
	writer.close();
	if (!(f.delete() && temp.renameTo(f))) println "Warning can't rename file "+temp+" to "+f
}*/
