package org.txm.scripts.tal

import org.txm.importer.StaxIdentityParser;

/**
 * Experiment of Tokenizer that targets XPath results text
 * 
 * @author mdecorde
 *
 */
class XMLStaxTokenizerAdapter extends StaxIdentityParser {
	def xpaths;
	boolean debug = false;
	boolean doTokenize = false
	String str; // string to tokenize
	def tokenizeResults = []
	def nsegments = 0
	
	public XMLStaxTokenizerAdapter(File infile, def xpaths, def tokenizeResults) {
		super(infile.toURI().toURL());
		this.xpaths = xpaths
		this.tokenizeResults = tokenizeResults
	}
	
	protected void processStartElement() {
		String localname = parser.getLocalName();
		if (debug) println "start element $currentXPath"
		if (doTokenize) {
			if (debug) println "Found text element $currentXPath"
			tokenizeCurrentText();
		} else {
			if (xpaths.contains(currentXPath.toString())) {
				doTokenize = true;
				str = "";
			}
		}
		super.processStartElement()
	}
	
	protected void processEndElement() {
		if (xpaths.contains(currentXPath.toString())) {
			tokenizeCurrentText()
			doTokenize = false;
			if (debug) println "Found text element: stop tokenizing"
		}
		
		super.processEndElement();
	}
	
	protected void processCharacters() {
		if (doTokenize) {
			str += parser.getText();
		} else {
			super.processCharacters();
		}
	}
	
	protected tokenizeCurrentText() {
		if (str.length() == 0) return;
		def tokens = tokenizeResults[nsegments]
		for (def token : tokens) {
			writer.writeStartElement("w");
			writer.writeCharacters(token);
			writer.writeEndElement();
			writer.writeCharacters(" ");
		}

		nsegments++;
	}
		
	/**
	 * Best tokenizer ever.
	 * Extends this function
	 * 
	 * @param str
	 * @return
	 */
	protected List<CharSequence> tokenize(String str) {
		return str.tokenize();
	}
	
	public static void main(String[] args) {
		File inputXMLFile = new File("/home/mdecorde/xml/comere/cmrclavardage/cmr-getalp_org-actu-tei-v1.xml");
		File outputXMLFile = new File("/home/mdecorde/xml/comere/cmrclavardage/tmp.xml");
		new XMLStaxTokenizerAdapter(inputXMLFile, ["/TEI/text/body/div/post/p/"]).process(outputXMLFile)
	}
}
