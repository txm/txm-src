package org.txm.scripts.tal

class Tagger {
	File executablePath
	File modelPath
	String lang
	
	public Tagger(File executablePath, File modelPath, String lang) {
		this.executablePath = executablePath
		this.modelPath = modelPath
		this.lang = lang
	}
}
