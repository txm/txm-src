package org.txm.scripts.scripts;

import java.util.ArrayList;
import org.txm.utils.treetagger.TreeTagger;
import org.txm.utils.*;
import org.txm.scripts.importer.*;
import org.txm.scripts.importer.RGAQCJ.*;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.text.DecimalFormat;
/**
 * test TT wrapper
 * add NLP annotation
 * build result matrix
 * 
 */
rootDir = "C:/Documents and Settings/alavrent/Mes documents/Projets/Morpho MultiDim/bfm3_2011";
new File(rootDir+"/models/").mkdir();
new File(rootDir+"/lexicon/").mkdir();
//new File(rootDir+"/proj/").mkdir();
new File(rootDir+"/ttlearn/").mkdir();
//new File(rootDir+"/ttsrc/").mkdir();
//new File(rootDir+"/pos/").mkdir();
//new File(rootDir+"/injection/").mkdir();
//new File(rootDir+"/anainline/").mkdir();
//new File(rootDir+"/xml-txm/").mkdir();


//train
def ttlearnfiles = ["bfm3_2011-learn.tt"];
def modelfiles = ["bfm3_2011.par"];

def lexfiles = ["bfm3_2011-lex-fus.txt"];
def openclassfiles = ["cattex2009_open_class.txt"];
//def lexiconfile = rootDir+"/lexicon/"+"lexfra-rgaqcj.txt";


for(int i=0; i < ttlearnfiles.size();i++)
{
	String infile = rootDir+"/ttlearn/"+ttlearnfiles[i];
	String outfile = rootDir+"/models/"+modelfiles[i];
	String lexiconfile = rootDir+"/lexicon/"+lexfiles[i];
	String openclassfile = rootDir+"/lexicon/"+openclassfiles[i];
	TreeTagger tt = new TreeTagger("C:/Program Files/treetagger/bin/");
	println("learn "+ttlearnfiles[i] +" >> "+modelfiles[i]);
	tt.settoken();
	tt.setquiet();
	//tt.setlemma();
	tt.setsgml();
	tt.setst("PONfrt");
	tt.setproto();
	tt.setutf8();
	tt.traintreetagger( lexiconfile, openclassfile, infile, outfile)
}

//