package org.txm.scripts.tal.stanford
import edu.stanford.nlp.ling.*
import edu.stanford.nlp.process.*
import edu.stanford.nlp.tagger.maxent.*;

// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

@Field @Option(name="inputDir", usage="input directory", widget="Folder", required=true, def="/home/mdecorde/xml/testarabe/out-s")
File inputDir
@Field @Option(name="outputDir", usage="output directory", widget="Folder", required=true, def="/home/mdecorde/xml/testarabe/out-t")
File outputDir

@Field @Option(name="model", usage="'.tagger' model file", widget="File", required=false, def="taggers/arabic-train.tagger")
File model
@Field @Option(name="extension", usage="Regexp de l'extension des fichiers à modifier", widget="String", required=true, def='\\.txt')
String extension = "\\.txt"

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;
def modelPath = model.getAbsolutePath()

outputDir.mkdir()
if (!outputDir.exists()) {
	println("Could not create $outputDir")
	return;
}

inputDir.eachFileMatch(~/.*$extension/) { inputFile ->
	String name = inputFile.getName()
	int idx = name.lastIndexOf(".")
	if (idx > 0) name = name.substring(0, idx)
	
	new File(outputDir, name+".xml").withWriter("UTF-8") { writer ->
		
		writer.println('<?xml version="1.0" encoding="UTF-8"?>')
		writer.println('<text>')
		//TODO: uncomment me
	//	def tagger = new MaxentTagger(modelPath);
	//	def ptbTokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(),	"untokenizable=noneKeep");
	
			//TODO: uncomment me
//		def documentPreprocessor = new DocumentPreprocessor(inputFile.newReader());
		documentPreprocessor.setTokenizerFactory(ptbTokenizerFactory);
		int n = 0
		
		for (def sentence : documentPreprocessor) {
			writer.println("<s n=\"$n\">")
			n++;
			def tSentence = tagger.tagSentence(sentence);
			//println(Sentence.listToString(tSentence, false));
			for (def taggedWord : tSentence)
			{
				def form = w.word();
				def pos = w.tag();
				writer.println("<w pos=\"$pos\">$form</w>")
			}
			writer.println('</s>')
		}
		writer.println('</text>')
	}
}