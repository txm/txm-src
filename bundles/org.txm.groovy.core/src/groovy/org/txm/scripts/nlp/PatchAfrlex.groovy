// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

// TODO: Auto-generated Javadoc
/**
 * Patch.
 *
 * @param correspondanceFile the correspondance file
 * @param afrlex the afrlex
 * @param afrlexfixed the afrlexfixed
 * @param encoding the encoding
 * @deprecated
 * @author mdecorde patch a cvs file replace POS name with the
 * correspondanceFile
 */
class PatchAfrlex {
	public static void patch(File correspondanceFile,File afrlex,File afrlexfixed, String encoding )
	{
		HashMap<String,String> correspondances = new HashMap<String,String>();
		
		def content = correspondanceFile.getText(encoding) 
		
		String separator= "\t"
		content.splitEachLine(separator) {fields ->
			correspondances.put( fields[0], fields[1]);
		}
		println(correspondances);
		
		
		Writer writer = new FileWriter(afrlexfixed);
		
		content = afrlex.getText(encoding) 
		separator= "\t"
		content.splitEachLine(separator) {fields ->
			writer.write(fields[0]);
			for(int i = 1 ; i< fields.size(); i = i+2)
			{
				writer.write("\t"+ correspondances.get(fields[i])+"\t"+fields[i+1]); 				
			}
			writer.write("\n")
		}
		writer.close();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File afrlex = new File('afrlex.txt');
		File afrlexfixed = new File('afrlex-patched.txt');
		File correspondanceFile = new File('TTnca_2_CTX9.txt');
		String encoding ="ISO-8859-1"
		
		PatchAfrlex.patch( correspondanceFile, afrlex, afrlexfixed, encoding );
	}
}
