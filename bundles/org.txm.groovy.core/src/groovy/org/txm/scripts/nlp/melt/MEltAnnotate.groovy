// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2013-11-08 13:38:06 +0100 (Fri, 08 Nov 2013) $
// $LastChangedRevision: 2569 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.tal.melt

import java.io.BufferedReader;
import java.io.File
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat
import java.util.ArrayList;
import java.util.Date
import java.util.Locale;

import org.txm.Toolbox
import org.txm.scripts.importer.*
import org.txm.importer.cwb.*
import org.txm.objects.*
import org.txm.importer.xmltxm.Annotate;
import org.txm.tal.SlashFile2TTRez;
import org.txm.utils.LangDetector;
import org.txm.utils.treetagger.TreeTagger

// TODO: Auto-generated Javadoc
/**
 * Annotate and replace the TEI-TXM files of the folder $rootDirFile/txm with TreeTagger.
 * creates $rootDirFile/interp and $rootDirFile/treetagger
 *
 */
class MEltAnnotate extends Annotate {

	public void initTTOutfileInfos(File rootDirFile, File modelfile)
	{
		reportFile = new File(rootDirFile,"NLPToolsParameters.xml");

		respPerson = System.getProperty("user.name");
		respId = "txm";
		respDesc = "NLP annotation tool";
		respDate = DateFormat.getDateInstance(DateFormat.SHORT, Locale.UK).format(new Date());
		respWhen = DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date());

		appIdent = "MElt";
		appVersion = "2.0";

		distributor = "";
		publiStmt = """""";
		sourceStmt = """""";

		types = ["frpos"];
		typesTITLE = ["frpos"];

		typesDesc = ["pos tagset built from MElt"]
		typesTAGSET = ["",""]
		typesWEB = ["",""]

		idform ="w";
	}

	/**
	 * Apply tt.
	 *
	 * @param ttsrcfile the ttsrcfile
	 * @param ttoutfile the ttoutfile
	 * @param modelfile the modelfile
	 * @return true, if successful
	 */
	public boolean applyTT(File ttsrcfile, File ttoutfile, File modelfile) {
		return applyMElt(ttsrcfile, ttoutfile)
	}

	public boolean applyMElt(File ttsrcfile, File ttoutfile) {
		// Call Melt
		def args = [];
		args << "MElt"
		args
		ProcessBuilder pb = new ProcessBuilder(args);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		OutputStream os = process.getOutputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		ttsrcfile.eachLine ("UTF-8") { line ->
			println "> $line"
			os.println line
		}
		
		ttoutfile.withWriter("UTF-8") { writer ->
			String line;
			println "getting result"
			while ((line = br.readLine()) != null) {
				println "< $line"
				writer.println(line);
			}
			println "end of result"
			int e = 0;
			try {
				e = process.waitFor();
			} catch (Exception err) {	}

			if (e != 0) {
				System.err .println("Process exited abnormally with code " + e + " at " + DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date())); //$NON-NLS-1$ //$NON-NLS-2$

				for (int c = 0; c < args.size(); c++)
					System.out.print("" + args.get(c) + " "); //$NON-NLS-1$ //$NON-NLS-2$
				System.out.println();
			}
		}

		// Reformat MElt output
		File tmpFile = new File(ttoutfile.getParentFile(),ttoutfile.getName()+".tmp")
		new SlashFile2TTRez(ttoutfile, tmpFile, "UTF-8")
	}
	
	public boolean run(File binDir, File txmDir) {
		return run(binDir, txmDir, "fr.par")
	}

	public static void main(def args) {
		Toolbox.setParam(Toolbox.INSTALL_DIR,new File("C:/Program Files/TXM"));
		Toolbox.setParam(Toolbox.METADATA_ENCODING, "UTF-8");
		Toolbox.setParam(Toolbox.METADATA_COLSEPARATOR, ",");
		Toolbox.setParam(Toolbox.METADATA_TXTSEPARATOR, "\"");
		Toolbox.setParam(Toolbox.USER_TXM_HOME, new File("/home/mdecorde/TXM"));
		
		println "-- ANNOTATE - Running MElt tool"

		File binDir = new File("/home/mdecorde/xml/melt-out")
		File txmDir = new File("/home/mdecorde/xml/melt-in")
		new MEltAnnotate().run(binDir, txmDir);
	}
}