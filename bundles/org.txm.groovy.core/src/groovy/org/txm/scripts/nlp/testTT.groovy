/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

import java.util.ArrayList;
import org.txm.utils.treetagger.TreeTagger;
import org.txm.utils.*;
import org.txm.scripts.importer.*;
import org.txm.scripts.importer.RGAQCJ.*;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.text.DecimalFormat;
// TODO: Auto-generated Javadoc

/**
 * test TT wrapper
 * add NLP annotation
 * build result matrix.
 *
 * @return the java.lang. object
 */
String rootDir = "~/xml/rgaqcj/";
new File(rootDir+"/models/").mkdir();
new File(rootDir+"/lexicon/").mkdir();
new File(rootDir+"/proj/").mkdir();
new File(rootDir+"/ttlearn/").mkdir();
new File(rootDir+"/ttsrc/").mkdir();
new File(rootDir+"/pos/").mkdir();
new File(rootDir+"/injection/").mkdir();
new File(rootDir+"/anainline/").mkdir();
new File(rootDir+"/xml-txm/").mkdir();

//create srcana.xml from src.xml
def srcfiles = ["roland.xml","artu.xml","qjm.xml","commyn1.xml","jehpar.xml"];
def anafiles = ["roland-ana.xml","artu-ana.xml","qjm-ana.xml","commyn1-ana.xml","jehpar-ana.xml"];
def xslfiles = ["bfm2txm-w.xsl","bfm2txm-w.xsl","bfm2txm-w.xsl","bfm2txm-w.xsl","bfm2txm-w.xsl"];
//bfmgraal2txm-w.xsl
for(int i=0 ; i < xslfiles.size() ; i++)
{
	String xslfile = rootDir+"xsl/"+xslfiles[i];
	String infile = rootDir+"src/"+srcfiles[i];
	String outfile = rootDir+"anainline/"+anafiles[i];
	println("create xml-txm from "+srcfiles[i]);
	ApplyXsl a = new ApplyXsl(xslfile);
	a.process(infile,outfile);
}

//Import to CWB
BuildXmlRGAQCJ.process( anafiles,  rootDir)
println("build cqp RGAQCJ");
println("encode + makeall");

//create TT TRAIN files
def ttlearnfiles = ["roland-learn.tt","artu-learn.tt","qjm-learn.tt","commyn1-learn.tt","jehpar-learn.tt","rgaqcj-learn.tt"];
String initiales = "RAQCJZ"

//ProcessBuilderBuilder.build(new File("src/groovy/org/txm/scripts/cwb-decode-wrapper-definition.xml"), new File("src/groovy/org/textometrie/scripts/CwbDecode.groovy"));
GroovyClassLoader gcl = new GroovyClassLoader();
gcl.addClasspath(".");

String registryPath = rootDir+"/registry";
String cwbdecodeexecDir = "~/Bureau/trunkCWB/cwb-3.0/utils/"

Class clazz = gcl.parseClass(new File("src/groovy/org/txm/scripts/CwbDecode.groovy"));
def aScript = clazz.newInstance(cwbdecodeexecDir);

aScript.setC()
def pAttributesTrain = ["word","cat"];
def pAttributesLex = ["word","cat","LEMMA"];
def pAttributesTag = ["word"];
def sAttributes = ["s"];
aScript.setP(pAttributesTag)
aScript.setS(sAttributes)
aScript.setr(registryPath)
aScript.debug(true);
int[] nbmots = [0,35306,134170,173526,198929,228149]

for(int i=0 ; i < initiales.length() ; i++)
{
	println("creation TTsrc "+ttlearnfiles[i]);
	FileOutputStream fos = new FileOutputStream(rootDir+"/ttsrc/"+ttlearnfiles[i]);
	PrintStream ps = new PrintStream(fos);
	def out = System.out;
	if(!(initiales[i]+"").matches("Z"))
	{
		if(i > 0)
			aScript.sets(nbmots[i]+1)
		else
			aScript.sets(nbmots[i])
		aScript.sete(nbmots[i+1])
	}
	else
	{
		aScript.unsets();
		aScript.unsete();
	}
	System.setOut(ps);
	
	if(System.getProperty("os.name").contains("Windows"))
		aScript.cwbdecodeexe("RGAQCJ")
	else
		aScript.cwbdecode("RGAQCJ")
	
	System.setOut(out);
}


aScript.unsetS()
aScript.setP(pAttributesTrain)
for(int i=0 ; i < initiales.length() ; i++)
{
	println("creation TTlearn "+ttlearnfiles[i]);
	FileOutputStream fos = new FileOutputStream(rootDir+"/ttlearn/"+ttlearnfiles[i]);
	PrintStream ps = new PrintStream(fos);
	def out = System.out;
	if(!(initiales[i]+"").matches("Z"))
	{
		if(i > 0)
			aScript.sets(nbmots[i]+1)
		else
			aScript.sets(nbmots[i])
		aScript.sete(nbmots[i+1])
	}
	else
	{
		aScript.unsets();
		aScript.unsete();
	}
	System.setOut(ps);
	
	if(System.getProperty("os.name").contains("Windows"))
		aScript.cwbdecodeexe("RGAQCJ")
	else
		aScript.cwbdecode("RGAQCJ")
	
	System.setOut(out);
}
aScript.setP(pAttributesLex)
HSQLFunctions.clearAll();
HashMap<String,Integer> nbFormLexique = new HashMap<String,Integer>()
HashMap<String,Integer> nbOccurencesTxt = new HashMap<String,Integer>()
for(int i=0 ; i < initiales.length() ; i++)
{
	println("creation lex brut lex_"+initiales[i]);
	FileOutputStream fos = new FileOutputStream(rootDir+"lexicon/lex_"+initiales[i]);
	PrintStream ps = new PrintStream(fos);
	def out = System.out;
	if(!(initiales[i]+"").matches("Z"))
	{
		if(i > 0)
			aScript.sets(nbmots[i]+1)
		else
			aScript.sets(nbmots[i])
		aScript.sete(nbmots[i+1])
	}
	else
	{
		aScript.unsets();
		aScript.unsete();
	}
	System.setOut(ps);
	
	if(System.getProperty("os.name").contains("Windows"))
		aScript.cwbdecodeexe("RGAQCJ")
	else
		aScript.cwbdecode("RGAQCJ")
	
	System.setOut(out);
	def argsname = ["form","cat","lemme"];
	def types = ["VARCHAR(30)","VARCHAR(30)","VARCHAR(30)"];
	File lexfile = new File(rootDir+"lexicon/lex_"+initiales[i]);
	int nbMot = HSQLFunctions.ImportOrderedCSVTable("lexbrut_"+initiales[i], argsname,types,lexfile,"\t","UTF-8");
	nbOccurencesTxt.put("lexbrut_"+initiales[i],nbMot);
	//HSQLFunctions.ImportCSVTable("lex_"+initiales[i], argsname,types,lexfile,"\t","ISO-8859-1");
	
	argsname = ["cat","lemme"];
	int nbForm = HSQLFunctions.toRefHierarchicalFile( "lexbrut_"+initiales[i], lexfile.getAbsolutePath(),"form",argsname,"UTF-8");
	nbFormLexique.put("lex_"+initiales[i],nbForm);
}
aScript.unsets();
aScript.unsete();
println("creation lex brut RGAQCJ");
FileOutputStream fos = new FileOutputStream(rootDir+"/lexicon/rgaqcj-brut");
PrintStream ps = new PrintStream(fos);
def out = System.out;
System.setOut(ps);

if(System.getProperty("os.name").contains("Windows"))
	aScript.cwbdecodeexe("RGAQCJ")
else
	aScript.cwbdecode("RGAQCJ")

System.setOut(out);

println("create lexiq from csv");

//train
//def ttlearnfiles = ["roland-learn.tt","qgraal_cm-learn.tt","artu-learn.tt","qjm-learn.tt","commyn1-learn.tt","jehpar-learn.tt","rgaqcj-learn.tt"];
def modelfiles = ["roland.par","artu.par","qjm.par","commyn1.par","jehpar.par","rgaqcj.par"];

def lexfiles = ["lex_R","lex_A","lex_Q","lex_C","lex_J","lex_Z"];
def openclassfiles = ["open_class_R.txt","open_class_A.txt","open_class_Q.txt","open_class_C.txt","open_class_J.txt","open_class_Z.txt"];
//def lexiconfile = rootDir+"/lexicon/"+"lexfra-rgaqcj.txt";


for(int i=0; i < ttlearnfiles.size();i++)
{
	String infile = rootDir+"/ttlearn/"+ttlearnfiles[i];
	String outfile = rootDir+"/models/"+modelfiles[i];
	String lexiconfile = rootDir+"/lexicon/"+lexfiles[i];
	String openclassfile = rootDir+"/lexicon/"+openclassfiles[i];
	TreeTagger tt = new TreeTagger("~/Bureau/treetagger/bin/");
	println("learn "+ttlearnfiles[i] +" >> "+modelfiles[i]);
	tt.settoken();
	tt.setquiet();
	//tt.setlemma();
	tt.setsgml();
	tt.setst("PONfrt");
	tt.setproto();
	tt.setutf8();
	tt.traintreetagger( lexiconfile, openclassfile, infile, outfile)
}

//projection
def ttsrcfiles = ["roland-learn.tt","artu-learn.tt","qjm-learn.tt","commyn1-learn.tt","jehpar-learn.tt","rgaqcj-learn.tt"];
def ttrezfiles = [];
for(int i=0; i < ttsrcfiles.size();i++)
{
	for(int j=0; j < modelfiles.size();j++)
	{
		String infile = rootDir+"/ttsrc/"+ttsrcfiles[i];
		String modelfile =  rootDir+"/models/"+modelfiles[j];
		String outfile = ttsrcfiles[i].substring(0,3)+"_"+modelfiles[j].substring(0,3)+".tt";
		ttrezfiles.add(outfile);
		println("proj "+modelfiles[j]+ " on " +ttsrcfiles[i] +" >> "+outfile);
		
		TreeTagger tt = new TreeTagger("~/Bureau/treetagger/bin/");
		tt.settoken();
		tt.setquiet();
		tt.setsgml();
		tt.seteostag("<s>");
		tt.treetagger( modelfile, infile, rootDir+"proj/"+outfile)
	}
}



/*
 //create ana-pos.xml files
 def posfiles = ["roland-pos","artu-pos","qjm-pos","commyn1-pos","jehpar-pos","rgaqcj-pos"];
 String encoding ="ISO-8859-1";
 String person ="Serge Heiden"; 
 String software = "tree-tagger cmd line with options ..."; 
 String cmdLine = "tree-tagger -token ...";
 String title ="POS annotation";
 def types = ["POS","lemma"];
 String idform ="fro";
 for(int i=0; i < ttsrcfiles.size();i++)
 {
 for(int j = 0; j< ttsrcfiles.size();j++)
 {
 int c = j +i*ttsrcfiles.size(); // id ttrez
 File ttfile = new File(rootDir+"/tt/",ttrezfiles[c]);
 File posfile = new File(rootDir+"/pos/",posfiles[i]+"-"+modelfiles[j].substring(0,3)+".xml");
 println("TTrez > ana-pos : "+ttfile+" to : "+posfile );
 c++;
 def transfo = new TTrezToXml();
 transfo.setAnnotationTypes( types, idform);
 transfo.setResp( person, software, cmdLine);
 transfo.process( ttfile, posfile, encoding );
 }
 }	
 //injection annotations
 String respPerson ="sheiden"
 String respId = "";
 String respDate = DateFormat.getDateInstance(DateFormat.FULL, Locale.UK).format(new Date());
 String respCDATA = "";
 for(int i=0; i < anafiles.size();i++)
 {
 for(int j = 0; j < 6 ; j++)
 {
 File srcfile = new File(rootDir+"/anainline/",anafiles[i]);
 File posfile = new File(rootDir+"/pos/",posfiles[i]+"-"+modelfiles[j].substring(0,3)+".xml");
 println("annote file : "+srcfile+" with : "+posfile );
 def builder = new AnnotationInjection(srcfile.toURL(), posfile.toURL(), solotags);
 builder.setResp(respId,respPerson, respDate, respCDATA);
 builder.transfomFile(rootDir+"/injection/",anafiles[i]);
 }
 }	
 */
//remove <s> from ttrezfiles
String encoding = "UTF-8"
for(int i=0; i < ttrezfiles.size();i++)
{
	String csvfile = rootDir+"proj/"+ttrezfiles[i];
	File f = new File(csvfile);
	File temp = new File("tempFileCVScleaner")
	println("fix <s> "+f);
	Reader reader = new InputStreamReader(new FileInputStream(csvfile),encoding);
	Writer writer = new FileWriter(temp);
	reader.eachLine 
			{
				if(!it.startsWith("<s") && !it.startsWith("</s"))
					writer.write(it+"\n")
			}
	reader.close();
	writer.close();
	if (!(f.delete() && temp.renameTo(f))) println "Warning can't rename file "+temp+" to "+f
}

//Build process infos
//import table into hsql
for(int i=0; i < ttrezfiles.size();i++)
{
	String csvfile = rootDir+"proj/"+ttrezfiles[i];
	def argsname = ["form","cat"];
	def types = ["VARCHAR(30)","VARCHAR(30)"];
	HSQLFunctions.ImportOrderedCSVTable(ttrezfiles[i].replace(".",""), argsname,types,new File(csvfile),"\t","UTF-8");
	println("create Table "+ttrezfiles[i].replace(".",""));
}

//calc richesses lexicales
LinkedHashMap<String,ArrayList<String>> richesses = new LinkedHashMap<String,ArrayList<String>>();
richesses.put("Text", ["TTrola","TTartu","TTqjm","TTcomm","TTjehpar","TTrgaqcj"]);

LinkedHashMap<String,ArrayList<String>> richessesCat = new LinkedHashMap<String,ArrayList<String>>();
richessesCat.put("Text", ["TTrola","TTartu","TTqjm","TTcomm","TTjehpar","TTrgaqcj"]);

LinkedHashMap<String,ArrayList<String>> richessesocc = new LinkedHashMap<String,ArrayList<String>>();
richesses.put("Text", ["TTrola","TTartu","TTqjm","TTcomm","TTjehpar","TTrgaqcj"]);

LinkedHashMap<String,ArrayList<String>> richessesoccCat = new LinkedHashMap<String,ArrayList<String>>();
richessesCat.put("Text", ["TTrola","TTartu","TTqjm","TTcomm","TTjehpar","TTrgaqcj"]);
for(int i=0;i < initiales.size();i++)
{
	richesses.put(""+initiales[i], new ArrayList<String>());
	richessesCat.put(""+initiales[i], new ArrayList<String>());
	richessesocc.put(""+initiales[i], new ArrayList<String>());
	richessesoccCat.put(""+initiales[i], new ArrayList<String>());
}
for(int i=0; i < initiales.size();i++)
{
	for(int j=0; j < initiales.size();j++)
	{
		String query = "SELECT count(*) FROM ((SELECT DISTINCT form FROM lexbrut_"+initiales[i]+" ) MINUS (SELECT DISTINCT form FROM lexbrut_"+initiales[j]+"))"
		println(query);
		String query2 = "SELECT count(*) FROM ((SELECT DISTINCT cat FROM lexbrut_"+initiales[i]+" ) MINUS (SELECT DISTINCT cat FROM lexbrut_"+initiales[j]+"))"
		println(query2);
		String query3 = "SELECT count(form) FROM lexbrut_"+initiales[i]+" WHERE form NOT IN (SELECT form FROM lexbrut_"+initiales[j]+")"
		println(query3);
		String query4 = "SELECT count(cat) FROM lexbrut_"+initiales[i]+" WHERE cat NOT IN (SELECT cat FROM lexbrut_"+initiales[j]+")"
		println(query4);
		HSQLFunctions.getGroovySql().eachRow(query) {
			def rich = it.getAt(0);
			println("rich form :"+ rich);
			richesses.get(""+initiales[i]).add( ""+rich ) ;	
		}
		
		HSQLFunctions.getGroovySql().eachRow(query2) {
			def rich = it.getAt(0);
			println("rich cat :"+ rich);
			richessesCat.get(""+initiales[i]).add( ""+rich ) ;	
		}
		
		
		HSQLFunctions.getGroovySql().eachRow(query3) {
			def rich = it.getAt(0);
			println("richocc form :"+ rich);
			richessesocc.get(""+initiales[i]).add( ""+rich ) ;	
		}
		
		HSQLFunctions.getGroovySql().eachRow(query4) {
			def rich = it.getAt(0);
			println("richocc cat :"+ rich);
			richessesoccCat.get(""+initiales[i]).add( ""+rich ) ;	
		}
	}
}

LinkedHashMap<String,ArrayList<String>> matrix = new LinkedHashMap<String,ArrayList<String>>();

matrix.put("Text", ["T    ","Vf    ","Vc    ","TTrola","TTartu","TTqjm","TTcomm","TTjehpar","TTrgaqcj"]);
for(int i=0;i < initiales.size();i++)
{
	matrix.put(""+initiales[i], new ArrayList<String>());
}

ArrayList<Integer> countOccForm = new ArrayList<Integer>();
ArrayList<Integer> countOccCat = new ArrayList<Integer>();
ArrayList<Integer> countLexForm = new ArrayList<Integer>();
ArrayList<Integer> countLexCat = new ArrayList<Integer>();

for(int i=0;i < initiales.size() ; i++ )
{
	String query1 = "SELECT count(*) FROM (SELECT form from lexbrut_"+initiales[i]+")";
	String query2 = "SELECT count(*) FROM (SELECT cat from lexbrut_"+initiales[i]+")";
	String query3 = "SELECT count(*) FROM (SELECT DISTINCT form from lexbrut_"+initiales[i]+")";
	String query4 = "SELECT count(*) FROM (SELECT DISTINCT cat from lexbrut_"+initiales[i]+")";
	
	HSQLFunctions.getGroovySql().eachRow(query1) {
		def rich = it.getAt(0);
		println("count occ form :"+ rich);
		countOccForm.add( ""+rich ) ;	
	}
	HSQLFunctions.getGroovySql().eachRow(query2) {
		def rich = it.getAt(0);
		println("coutn occ cat :"+ rich);
		countOccCat.add( ""+rich ) ;	
	}
	HSQLFunctions.getGroovySql().eachRow(query3) {
		def rich = it.getAt(0);
		println("count lex form :"+ rich);
		countLexForm.add( ""+rich ) ;	
	}
	HSQLFunctions.getGroovySql().eachRow(query4) {
		def rich = it.getAt(0);
		println("count lex cat :"+ rich);
		countLexCat.add( ""+rich ) ;	
	}
	println("col1 et 2 : "+nbOccurencesTxt.get("lexbrut_"+initiales[i])+" "+nbFormLexique.get("lex_"+initiales[i]))
	matrix.get(""+initiales[i]).add( ""+nbOccurencesTxt.get("lexbrut_"+initiales[i]) ) ;
	matrix.get(""+initiales[i]).add( ""+nbFormLexique.get("lex_"+initiales[i]) ) ;
	matrix.get(""+initiales[i]).add( "0" ) ;
}
for(int i=0; i < ttrezfiles.size();i++)
{
	//get occurences ordonnées de la gold
		//get occurences ordonnées de la projection
		//compare
	
	int id = i/ttsrcfiles.size();
	
	println("comp "+ttrezfiles[i]+" VS lexbrut_"+initiales[id]);
	String query = "SELECT count(*) FROM ((SELECT n,form,cat FROM lexbrut_"+initiales[id]+" ) MINUS (SELECT n,form,cat FROM "+ttrezfiles[i].replace(".","")+"))"
	//HSQLFunctions.executeQuery(query);
	//println(query)
	int ttotal=0;
	int total ;
	HSQLFunctions.getGroovySql().eachRow(query) {
		def dif = it.getAt(0);
		if((""+initiales[id]) != "Z")
		{
			total = nbmots[id+1] - nbmots[id];
			ttotal += total;
		}
		else
			total= ttotal;
		
		total = nbOccurencesTxt.get("lexbrut_"+initiales[id])
		println("dif "+dif+"/ tot "+total+" = "+((float)dif/(float)total));
		Float perf = ((float)dif/(float)total)*100f;			
		matrix.get(""+initiales[id]).add( ""+perf ) ;
	}
	
	/*	HSQLFunctions.getGroovySql().eachRow("SELECT count(*) from "+table+" WHERE form NOT IN (SELECT form FROM "+ttrezfiles[i].replace(".","")+")") {
	 def dif = it.getAt(0);
	 int total = nbmots[i+1] - nbmots[i];
	 richesses.put(""+initiales[i],""+(dif/total)) ;
	 }*/
}
println("Matrice d'erreur : ");
for(String k : matrix.keySet())
{
	print(k)
	for(String f : matrix.get(k))
		print("\t"+f)
	println()
}
println("richesse lexiques en Form : ");
for(String k : richesses.keySet())
{
	print(k)
	for(String f : richesses.get(k))
		print("\t"+f)
	println()
}
println("richesse lexiques en Cat : ");
for(String k : richessesCat.keySet())
{
	print(k)
	for(String f : richessesCat.get(k))
		print("\t"+f)
	println()
}

println("richesse occurance en Form : ");
for(String k : richessesocc.keySet())
{
	print(k)
	for(String f : richessesocc.get(k))
		print("\t"+f)
	println()
}
println("richesse occurance en Cat : ");
for(String k : richessesoccCat.keySet())
{
	print(k)
	for(String f : richessesoccCat.get(k))
		print("\t"+f)
	println()
}

println("RichBrutes	roland	artu	qjm	comm	jehpar	rgaqcj");
print("Focc")
for(int i=0;i < countOccForm.size() ; i++)
	print("\t"+countOccForm[i]); 
print("\nCocc")
for(int i=0;i < countOccCat.size() ; i++)
	print("\t"+countOccCat[i]); 
print("\nFvoc")
for(int i=0;i < countLexForm.size() ; i++)
	print("\t"+countLexForm[i]); 
print("\nCvoc")
for(int i=0;i < countLexCat.size() ; i++)
	print("\t"+countLexCat[i]); 
println()

