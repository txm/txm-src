
package org.txm.scripts.tal.melt

import java.io.File;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.txm.tal.Tagger
import org.txm.tal.SlashFile2TTRez
import org.txm.utils.StreamHog;

class SxPipe {

	Process process = null;
	InputStream meltis
	OutputStream meltos
	boolean debug = false

	BufferedReader reader
	StringBuffer buffer = new StringBuffer();

	public SxPipe() {
	}

	public boolean start() {
		// Call Melt
		def args = [];
		args << "MElt"
		args << "-t"
		args << "-C"
		ProcessBuilder pb = new ProcessBuilder(args);
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
			return false
		}
		meltis = process.getInputStream();
		meltos = process.getOutputStream();

		new StreamHog(process.getErrorStream(), debug) // read Error Stream
		//new StreamHog(process.getInputStream(), true) // read Error Stream
		reader = new BufferedReader(new InputStreamReader(meltis));
		return true;
	}

	public boolean processText(String text) {
		if (debug) println "Send text: $text"
		process << text
		process << "\n"
		meltos.flush()
		return true;
	}

	public def readStream() {
		if (debug) println "reading lines..."

		try {
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer << line
				buffer << "\n"
			}
		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}

		return "end"
	}

	public boolean stop() {
		meltos.close()
		readStream()

		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {	}

		if (e != 0) {
			System.err .println("Process exited abnormally with code " + e )
		}
	}

	public def getResult() {
		return buffer.toString()
		//return SlashFile2TTRez.processString(buffer.toString())
	}

	public void printResult() {
		def segs = getResult().split("\n")// \\{\\\$\\\$\\\$\\\$\\} \\\$\\\$\\\$\\\$

		int i = 0;
		for (def seg : segs) {
			def tokens = []
			for(def str :  seg.replaceAll("\\{", "\n{").tokenize("\n")) {
				if (str.startsWith("{"))
					println str.substring(1, str.indexOf("}")).replace("_ACC_O", "{").replace("_ACC_F", "}")
			}
		}
	}

	public def getResultTokens() {
		def segs = getResult().split("\n")
		def tokens = []

		int i = 0, idx = -1;
		for (def seg : segs) {
			for(def str :  seg.replaceAll("\\{", "\n{").tokenize("\n")) {
				if (str.startsWith("{")) {
					idx = str.indexOf("}")
					try{
						tokens << [str.substring(1, idx).replace("_ACC_O", "{").replace("_ACC_F", "}"),
							str.substring(idx+1)]
					} catch(Exception e) {e.printStackTrace();println "STRING IS : '$str'"}
				}
			}

		}
		return tokens
	}

	public def getResultSegments() {
		def segs = getResult().split("\n")
		def segsFinal = []

		int i = 0, idx = -1;
		for (def seg : segs) {
			def tokens = []
			for(def str :  seg.replaceAll("\\{", "\n{").tokenize("\n")) {
				if (str.startsWith("{")) {
					idx = str.indexOf("}")
					try{
						tokens << [str.substring(1, idx).replace("_ACC_O", "{").replace("_ACC_F", "}"),
							str.substring(idx+1)]
					} catch(Exception e) {e.printStackTrace();println "STRING IS : '$str'"}
				}
			}
			segsFinal << tokens
		}
		return segsFinal
	}


	public static void main(String[] args) {
		SxPipe melt = new SxPipe()
		melt.start()
		melt.processText("""Il y a {du} pain \$\$\$\$ c'était bon. Y a-t-il assez ?
Un autre bout, de texte : fin.""")
		melt.stop()

		melt.printResult()



		//		println "\nMELT DONE\n"
		//		for (def r : melt.getResult()) {
		//			for (def rr : r)
		//				print rr+"\t"
		//			println ""
		//		}
	}
}
