package org.txm.scripts.tal

class SlashFile2TTRez {
	public static boolean debug = true;

	public SlashFile2TTRez(File inputFile, File outputFile, String encoding) {
		outputFile.withWriter("UTF-8") { writer ->
			int nline = 1;
			inputFile.eachLine(encoding) { line ->
				def rez = processString(line)
				for (def l : rez) {
					//if (debug) println "result: $l"
					writer.println(l[0] + "\t" + l[1])
				}
				nline++;
			}
		}
	}

	def static empty = [null,null]
	public static def processString(String line) {
		def rez = [];
		if (debug) println "process line $line"
		if (line.length() == 0) {
			return []
		} else {
			for (String str : line.tokenize()) {
				if (debug) println " process str $str"
				int idx = str.lastIndexOf("/")
				if (idx > 0) {
					rez << [str.substring(0, idx), str.substring(idx+1)]
				} else {
					if (debug) println "ERROR line $line"
				}
			}
		}
		return rez
	}

	public static void main(def args) {
		File inputFile = new File("/home/mdecorde/Taggers/melt-2.0b2/txtbrut-o.txt");
		File outputFile = new File("/home/mdecorde/Taggers/melt-2.0b2/txtbrut-tt.txt");
		new SlashFile2TTRez(inputFile, outputFile, "UTF-8")
	}
}
