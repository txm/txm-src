// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

// TODO: Auto-generated Javadoc
/** The person. @deprecated @author mdecorde tranform Ttrez into a stand-off file */
class TTrezToXml {
	String person = ""; // used for respStmt/resp/name(person)
	
	/** The software. */
	String software = ""; // used for respStmt/resp/name(software)
	
	/** The cmd line. */
	String cmdLine = "";// used for respStmt/resp/name(software)/p
	
	/** The title. */
	String title = "";// used for <title>
	
	/** The types. */
	def types;// used for linkGrp type
	
	/** The idform. */
	String idform = "";
	
	/** The comments. */
	String comments = "...";// used for linkGrp type comments

	/** The writer. */
	Writer writer;

	/**
	 * Process.
	 *
	 * @param TTrez the t trez
	 * @param xmlform the xmlform
	 * @param encoding the encoding
	 */
	public void process(File TTrez,File xmlform, String encoding )
	{
		writer = new FileWriter(xmlform);
		writeHead();	
		writeBody(TTrez,encoding)
		writeTail();
		writer.close();
	}

	/**
	 * Write head.
	 */
	private void writeHead() {
		writer.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
		writer.write("<!DOCTYPE TEI SYSTEM \"tei_bfm_v1.dtd\">\n");
		writer.write("<TEI xmlns:txm=\"http://textometrie.ens-lyon.fr/1.0\">\n");
		writer.write("<teiHeader xml:lang=\"eng\">\n");
		writer.write("<fileDesc>\n");
		writer.write("<titleStmt>\n");
		for (int i = 0; i < types.size(); i++)
			writer.write("<title>" + types[i] + "</title>\n");
		writer.write("<respStmt>\n");
		writer.write("<resp>produced by</resp>\n");
		writer.write("<name type=\"person\">" + person + "</name>\n");
		writer.write("<name type=\"software\">" + software + "</name>\n");
		writer.write("<p>" + cmdLine + "</p>\n");
		writer.write("</respStmt>\n");
		writer.write("</titleStmt>\n");
		writer.write("<publicationStmt>\n");
		writer
				.write("<distributor>BFM project - http://bfm.ens-lsh.fr</distributor>\n");
		writer.write("<availability>\n");
		writer.write("<p>(c) 2010 Projet BFM - CNRS/ENS-LSH.\n");
		writer.write("<hi>Conditions d'utilisation</hi> : \n");
		writer
				.write("Sous licence <ref target=\"http://creativecommons.org/licenses/by-sa/2.0/fr/\">Creative Commons</ref>.\n");
		writer.write("</p>\n");
		writer.write("</availability>\n");
		writer.write("</publicationStmt>\n");
		writer.write("<sourceDesc>\n");
		writer
				.write("<p>born digital : TXM project - http://textometrie.org</p>\n");
		writer.write("</sourceDesc>\n");
		writer.write("</fileDesc>\n");
		writer.write("</teiHeader>\n");
		writer.write("<text xml:lang=\"fr\" type=\"standoff\">\n");
		writer.write("<body>\n");
		writer.write("<div>\n");
	}

	/**
	 * Write body.
	 *
	 * @param TTrez the t trez
	 * @param encoding the encoding
	 * @return the java.lang. object
	 */
	private writeBody(File TTrez, String encoding)
	{
		
		String targets= "w_"+idform+"_";
		
		def content = TTrez.getText(encoding) 
		def separator= "\t"
		
		for(int i=0 ; i< types.size() ; i++)
		{
			int id=1;
			writer.write("<linkGrp type=\""+types[i]+"\">\n");
			content.splitEachLine(separator) {fields ->
				if(! ((String)fields[0]).substring( 0,1).matches("<"))
				{
					writer.write("<link targets=\"#"+targets+id+" #"+ fields[i+1]+"\"/>\n");
					id++;
				}
			}
			writer.write("</linkGrp>\n");
		}
	}

	/**
	 * Write tail.
	 */
	private void writeTail() {

		writer.write("</div>\n");
		writer.write(" </body>\n");
		writer.write("</text>\n");
		writer.write("</TEI>\n");
	}

	/**
	 * Sets the annotation types.
	 *
	 * @param types the types
	 * @param idform the idform
	 */
	public void setAnnotationTypes(def types, String idform) {
		this.types = types;
		this.idform = idform;
	}

	/**
	 * Sets the resp.
	 *
	 * @param person the person
	 * @param software the software
	 * @param cmdLine the cmd line
	 */
	public void setResp(String person, String software, String cmdLine) {
		this.person = person;
		this.software = software;
		this.cmdLine = cmdLine;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String rootDir = "~/xml/rgaqcj/";
		new File(rootDir+"/pos/").mkdir();
		
		
		def ttfiles = ["roland-tt.txt","artu-tt.txt","qjm-tt.txt","commyn1-tt.txt","jehpar-tt.txt"];
		def posfiles = ["roland-pos.xml","artu-pos.xml","qjm-pos.xml","commyn1-pos.xml","jehpar-pos.xml"];
		
		String encoding ="ISO-8859-1";
		String person ="Serge Heiden"; 
		String software = "tree-tagger cmd line with options ..."; 
		String cmdLine = "tree-tagger -token ...";
		String title ="POS annotation";
		def types = ["POS","lemma"];
		String idform ="fro";
		
		for(int i=0; i < ttfiles.size();i++)
		{
			File ttfile = new File(rootDir+"/tt/",ttfiles[i]);
			File posfile = new File(rootDir+"/pos/",posfiles[i]);
			println("Process file : "+ttfile+" to : "+posfile );
			
			def transfo = new TTrezToXml();
			transfo.setAnnotationTypes( types, idform);
			transfo.setResp( person, software, cmdLine);
			transfo.process( ttfile, posfile, encoding );
		}	
	}
}
