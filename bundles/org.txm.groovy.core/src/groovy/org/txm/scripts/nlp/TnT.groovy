// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

// Trigrams'n'Tags : part-of-speach tagger
// TODO: Auto-generated Javadoc

/**
 * The Class TnT.
 */
class TnT {
	
	/** The binpath. */
	String binpath = "";

	/**
	 * Instantiates a new tn t.
	 *
	 * @param binpath the binpath
	 */
	public TnT(String binpath) {
		this.binpath = binpath;
	}

	/** The version. */
	String version = "0.0.0";
	
	/** The desc. */
	String desc = "Trigrams'n'Tags : part-of-speach tagger";
	
	/** The debug. */
	boolean debug = false;

	/**
	 * Debug.
	 *
	 * @param b the b
	 */
	public void debug(boolean b) {
		debug = b;
	};

	// use suffix trie with max. suffix length = len; default = 10
	
	/** The isa. */
	private Boolean isa = false;
	
	/** The a. */
	private int a;

	/**
	 * Sets the a.
	 *
	 * @param arg the new a
	 */
	public void seta(int arg) {
		this.isa = true;
		this.a = arg;
	}

	/**
	 * Unseta.
	 */
	public void unseta() {
		this.isa = false;
	}

	// use fil as backup lexicon
	
	/** The isb. */
	private Boolean isb = false;
	
	/** The b. */
	private File b;

	/**
	 * Sets the b.
	 *
	 * @param arg the new b
	 */
	public void setb(File arg) {
		this.isb = true;
		this.b = arg;
	}

	/**
	 * Unsetb.
	 */
	public void unsetb() {
		this.isb = false;
	}

	// backup mode : 0=main only, 1=mix, 2=backup only (default: 1)
	
	/** The is b. */
	private Boolean isB = false;
	
	/** The B. */
	private int B;

	/**
	 * Sets the b.
	 *
	 * @param arg the new b
	 */
	public void setB(int arg) {
		this.isB = true;
		this.B = arg;
	}

	/**
	 * Unset b.
	 */
	public void unsetB() {
		this.isB = false;
	}

	// sparse data mode
	
	/** The isd. */
	private Boolean isd = false;
	
	/** The d. */
	private String d;

	/**
	 * Sets the d.
	 *
	 * @param arg the new d
	 */
	public void setd(String arg) {
		this.isd = true;
		this.d = arg;
	}

	/**
	 * Unsetd.
	 */
	public void unsetd() {
		this.isd = false;
	}

	// copy HTML tags to output (without tagging)
	
	/** The is h. */
	private Boolean isH = false;

	/**
	 * Sets the h.
	 */
	public void setH() {
		this.isH = true;
	}

	/**
	 * Unset h.
	 */
	public void unsetH() {
		this.isH = false;
	}

	// mark unknown words in output with an asterisk (*)
	
	/** The ism. */
	private Boolean ism = false;

	/**
	 * Setm.
	 */
	public void setm() {
		this.ism = true;
	}

	/**
	 * Unsetm.
	 */
	public void unsetm() {
		this.ism = false;
	}

	// use num-grams, num = 1, 2, 3, default = 3
	
	/** The isn. */
	private Boolean isn = false;
	
	/** The n. */
	private int n;

	/**
	 * Sets the n.
	 *
	 * @param arg the new n
	 */
	public void setn(int arg) {
		this.isn = true;
		this.n = arg;
	}

	/**
	 * Unsetn.
	 */
	public void unsetn() {
		this.isn = false;
	}

	// unknown word count, default = 3
	
	/** The isu. */
	private Boolean isu = false;
	
	/** The u. */
	private int u;

	/**
	 * Sets the u.
	 *
	 * @param arg the new u
	 */
	public void setu(int arg) {
		this.isu = true;
		this.u = arg;
	}

	/**
	 * Unsetu.
	 */
	public void unsetu() {
		this.isu = false;
	}

	// set verbosity level num: (default = 3)
	
	/** The isv. */
	private Boolean isv = false;
	
	/** The v. */
	private int v;

	/**
	 * Sets the v.
	 *
	 * @param arg the new v
	 */
	public void setv(int arg) {
		this.isv = true;
		this.v = arg;
	}

	/**
	 * Unsetv.
	 */
	public void unsetv() {
		this.isv = false;
	}

	// output tag if prob is in beam num, default =0 (means infinity)
	
	/** The isz. */
	private Boolean isz = false;
	
	/** The z. */
	private int z;

	/**
	 * Sets the z.
	 *
	 * @param arg the new z
	 */
	public void setz(int arg) {
		this.isz = true;
		this.z = arg;
	}

	/**
	 * Unsetz.
	 */
	public void unsetz() {
		this.isz = false;
	}

	// cut off path if prob is not in beam num (default 1000)
	
	/** The is z. */
	private Boolean isZ = false;
	
	/** The Z. */
	private int Z;

	/**
	 * Sets the z.
	 *
	 * @param arg the new z
	 */
	public void setZ(int arg) {
		this.isZ = true;
		this.Z = arg;
	}

	/**
	 * Unset z.
	 */
	public void unsetZ() {
		this.isZ = false;
	}

	// tagging. the language model is loaded from model.lex and model.123. if
	// model.map exists, it is used for output mapping compressed files are
	// recognized by suffix .gz, .bz2, or .Z
	
	/**
	 * Tnt.
	 *
	 * @param model the model
	 * @param corpus the corpus
	 */
	public void tnt(String model, File corpus) throws IOException
	// arg : model name, will be searched in current dir or in the path
	// specified in the env var TNT_MODELS
	// arg : corpus file ?
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "tnt");
		if (isa) {
			args.add("-a");
			args.add("" + a);
		}
		if (isb) {
			args.add("-b");
			args.add("" + b);
		}
		if (isB) {
			args.add("-B");
			args.add("" + B);
		}
		if (isd) {
			args.add("-d");
			args.add("" + d);
		}
		if (isH)
			args.add("-H");
		if (ism)
			args.add("-m");
		if (isn) {
			args.add("-n");
			args.add("" + n);
		}
		if (isu) {
			args.add("-u");
			args.add("" + u);
		}
		if (isv) {
			args.add("-v");
			args.add("" + v);
		}
		if (isz) {
			args.add("-z");
			args.add("" + z);
		}
		if (isZ) {
			args.add("-Z");
			args.add("" + Z);
		}
		args.add("" + model);
		args.add("" + corpus);

		ProcessBuilder pb = new ProcessBuilder(args);
		// pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code "
					+ e
					+ " at "
					+ DateFormat.getDateInstance(DateFormat.FULL, Locale.UK)
							.format(new Date()));

			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " ");
			System.out.println();
		}
	}

	// print accuracy vs. frequency, max freq = max
	
	/** The isf. */
	private Boolean isf = false;
	
	/** The f. */
	private int f;

	/**
	 * Sets the f.
	 *
	 * @param arg the new f
	 */
	public void setf(int arg) {
		this.isf = true;
		this.f = arg;
	}

	/**
	 * Unsetf.
	 */
	public void unsetf() {
		this.isf = false;
	}

	// ignore upper/lower case of tokens
	
	/** The isi. */
	private Boolean isi = false;

	/**
	 * Seti.
	 */
	public void seti() {
		this.isi = true;
	}

	/**
	 * Unseti.
	 */
	public void unseti() {
		this.isi = false;
	}

	// lexicon to account for known/unknown words
	
	/** The isl. */
	private Boolean isl = false;
	
	/** The l. */
	private File l;

	/**
	 * Sets the l.
	 *
	 * @param arg the new l
	 */
	public void setl(File arg) {
		this.isl = true;
		this.l = arg;
	}

	/**
	 * Unsetl.
	 */
	public void unsetl() {
		this.isl = false;
	}

	// counting differences
	
	/**
	 * Tntdiff.
	 *
	 * @param originalfile the originalfile
	 * @param resultfile the resultfile
	 */
	public void tntdiff(File originalfile, File resultfile) throws IOException
	// arg : model file
	// arg : corpus file ?
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "tnt-diff");
		if (isa)
			args.add("-a");
		if (isf) {
			args.add("-f");
			args.add("" + f);
		}
		if (isi)
			args.add("-i");
		if (isl) {
			args.add("-l");
			args.add("" + l);
		}
		if (ism) {
			args.add("-m");
			args.add("" + m);
		}
		args.add("" + originalfile);
		args.add("" + resultfile);

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code "
					+ e
					+ " at "
					+ DateFormat.getDateInstance(DateFormat.FULL, Locale.UK)
							.format(new Date()));

			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " ");
			System.out.println();
		}
	}

	// encode capitalization in tag
	
	/** The isc. */
	private Boolean isc = false;

	/**
	 * Setc.
	 */
	public void setc() {
		this.isc = true;
	}

	/**
	 * Unsetc.
	 */
	public void unsetc() {
		this.isc = false;
	}

	// base name for output files, default=basename of corpus
	
	/** The iso. */
	private Boolean iso = false;
	
	/** The o. */
	private String o;

	/**
	 * Sets the o.
	 *
	 * @param arg the new o
	 */
	public void seto(String arg) {
		this.iso = true;
		this.o = arg;
	}

	/**
	 * Unseto.
	 */
	public void unseto() {
		this.iso = false;
	}

	// parameters generation
	
	/**
	 * Tntpara.
	 *
	 * @param corpus the corpus
	 */
	public void tntpara(File corpus) throws IOException
	// arg : corpus file ?
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "tnt-para");
		if (isc)
			args.add("-c");
		if (isH)
			args.add("-H");
		if (isi)
			args.add("-i");
		if (isl)
			args.add("-l");
		if (isn)
			args.add("-n");
		if (iso) {
			args.add("-o");
			args.add("" + o);
		}
		if (isv)
			args.add("-v");
		args.add("" + corpus);

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code "
					+ e
					+ " at "
					+ DateFormat.getDateInstance(DateFormat.FULL, Locale.UK)
							.format(new Date()));

			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " ");
			System.out.println();
		}
	}

	// count tags
	
	/** The ist. */
	private Boolean ist = false;

	/**
	 * Sett.
	 */
	public void sett() {
		this.ist = true;
	}

	/**
	 * Unsett.
	 */
	public void unsett() {
		this.ist = false;
	}

	// count words tokens
	
	/** The isw. */
	private Boolean isw = false;

	/**
	 * Setw.
	 */
	public void setw() {
		this.isw = true;
	}

	/**
	 * Unsetw.
	 */
	public void unsetw() {
		this.isw = false;
	}

	// counting tokens and types
	
	/**
	 * Tntwc.
	 *
	 * @param corpus the corpus
	 */
	public void tntwc(File corpus) throws IOException
	// arg : corpus file ?
	{
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "tnt-wc");
		if (isH)
			args.add("-H");
		if (isi)
			args.add("-i");
		if (isl)
			args.add("-l");
		if (ist)
			args.add("-t");
		if (isw)
			args.add("-w");
		args.add("" + corpus);

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code "
					+ e
					+ " at "
					+ DateFormat.getDateInstance(DateFormat.FULL, Locale.UK)
							.format(new Date()));

			for (int c = 0; c < args.size(); c++)
				System.out.print("" + args.get(c) + " ");
			System.out.println();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		TnT tt = new TnT("");
	}
}