package org.txm.scripts.tal

import java.net.URL;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.graal.PersonalNamespaceContext;
import org.txm.tal.melt.SxPipe


/**
 * Test of tokenizer that tokenize parts of a XML file using XPath resolved with the Stax parser
 * 
 * @author mdecorde
 *
 */
class XMLStaxTokenizerAdapter2 {
	def xpaths;
	boolean debug = false;
	boolean doTokenize = false
	def segments = []
	String str;

	protected URL inputurl;
	protected def inputData;
	protected XMLInputFactory factory;
	protected XMLStreamReader parser;

	public static String TXMNS = "http://textometrie.org/1.0";
	public static String TEINS = "http://www.tei-c.org/ns/1.0";
	protected static PersonalNamespaceContext Nscontext = new PersonalNamespaceContext();

	String currentXPath = "/"
	String localname;

	public XMLStaxTokenizerAdapter2(File infile, def xpaths) {
		this.inputurl = infile.toURI().toURL();
		inputData = inputurl.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		this.xpaths = xpaths
	}

	public boolean process() {
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();
						currentXPath += localname+"/"
						processStartElement();
						break;
					case XMLStreamConstants.CHARACTERS:
						processCharacters();
						break;
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						processEndElement();
						currentXPath = currentXPath.substring(0, currentXPath.length() - localname.length() -1)
						break;

				}
			}
		} catch(Exception e) {
			println("Error while parsing file "+inputurl);
			println("Location "+parser.getLocation());
			org.txm.utils.logger.Log.printStackTrace(e);
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}

		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		return true;
	}

	protected void processStartElement() {
		String localname = parser.getLocalName();
		if (debug) println "start element $currentXPath"
		if (doTokenize) {
			if (debug) println "Found text element $currentXPath"
			tokenizeCurrentText();
		} else {
			if (xpaths.contains(currentXPath)) {
				doTokenize = true;
				str = "";
			}
		}
	}

	protected void processEndElement() {
		if (xpaths.contains(currentXPath)) {
			tokenizeCurrentText()
			doTokenize = false;
			if (debug) println "Found text element: stop tokenizing"
		}
	}

	protected void processCharacters() {
		if (doTokenize) {
			str += parser.getText();
		}
	}

	int nsegments = 0;
	protected tokenizeCurrentText() {
		if (str.length() == 0) return;

		segments << str
		nsegments++;
		str = "" // continue tokenizing
	}

	/**
	 * Best tokenizer ever.
	 * Extends this function
	 * 
	 * @param str
	 * @return
	 */
	protected List<CharSequence> tokenize(String str) {
		return str.tokenize();
	}

	public static void main(String[] args) {
		File inputXMLFile = new File("/home/mdecorde/xml/comere/cmrclavardage/cmr-getalp_org-actu-tei-v1.xml");
		def firstPass = new XMLStaxTokenizerAdapter2(inputXMLFile, ["/TEI/text/body/div/post/p/"]);

		println "FIRST PASS"
		firstPass.process()
		println firstPass.getSegments().size()

		println "TOKENIZING..."
		StringBuffer buffer = new StringBuffer();
		for(def segment : firstPass.getSegments()) {
			buffer << segment.replaceAll("\n", " ") + "\n"
		}
		new File("/home/mdecorde/Bureau/test1.txt").withWriter {writer -> writer.println(buffer)}

		SxPipe sxpipe = new SxPipe()
		sxpipe.start()
		sxpipe.processText(buffer.toString())
		sxpipe.stop()

		new File("/home/mdecorde/Bureau/test.txt").withWriter {writer -> writer.println(sxpipe.getResult())}
		def tokenizedSegments = sxpipe.getResultSegments()
		println "tokenizedSegments size "+tokenizedSegments.size()
		//		for(def segment : firstPass.getSegments()) {
		//			tokenizedSegments << segment.tokenize()
		//		}

		//		println "SECOND PASS"
		//		def secondPass = new XMLStaxTokenizerAdapter(inputXMLFile, ["/TEI/text/body/div/post/p/"], tokenizedSegments);
		//		secondPass.process(new File("/home/mdecorde/xml/comere/cmrclavardage/tmp.xml"))
	}
}
