
package org.txm.scripts.tal.melt

import java.io.File;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.txm.tal.Tagger
import org.txm.tal.SlashFile2TTRez
import org.txm.utils.StreamHog;

class MElt extends Tagger {
	
	Process process = null;
	InputStream meltis
	OutputStream meltos
	boolean debug = false
	
	BufferedReader reader
	StringBuffer buffer = new StringBuffer();
	
	public MElt(File executablePath, File modelPath, String lang) {
		super(executablePath, modelPath, lang);
	}
	
	public boolean start() {
		// Call Melt
		def args = [];
		args << "MElt"
		//args << "-L"
		ProcessBuilder pb = new ProcessBuilder(args);
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
			return false
		}
		meltis = process.getInputStream();
		meltos = process.getOutputStream();
		
		new StreamHog(process.getErrorStream(), debug) // read Error Stream
		//new StreamHog(process.getInputStream(), true) // read Error Stream
		reader = new BufferedReader(new InputStreamReader(meltis));
		return true;
	}
	
	public boolean processText(String text) {
		if (debug) println "Send text: $text"
		process << text
		process << "\n"
		meltos.flush()
		return true;
	}
	
	public def readStream() {
		if (debug) println "reading lines..."
		
		try {
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer << line
				buffer << "\n"
			}
		} catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		
		return "end"
	}
	
	public boolean stop() {
		meltos.close()
		readStream()
		
		int e = 0;
		try {
			e = process.waitFor();
		} catch (Exception err) {	}

		if (e != 0) {
			System.err .println("Process exited abnormally with code " + e )
		}
	}
	
	public def getResult() {
		return SlashFile2TTRez.processString(buffer.toString())
	}
	
	
	public static void main(String[] args) {
		MElt melt = new MElt(new File(""), new File(""), "fr")
		melt.start()
		melt.processText("""Il va.
Un autre bout, de texte""")
		melt.stop()

		println "\nMELT DONE\n"
		for (def r : melt.getResult()) {
			for (def rr : r)
				print rr+"\t"
			println ""
		}
	}
}
