package org.txm.scripts.tal

import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.XPathResult;
import org.w3c.dom.*;

class XMLDomTokenizerAdapter {
	String xpath;
	boolean debug = true;
	def nodes = []
	
	File infile, outfile;
	public XMLDomTokenizerAdapter(File infile, def xpath) {
		this.infile = infile
		this.xpath = xpath
	}
	
	public boolean process(File outfile) {
		this.outfile = outfile
		
		XPathResult rez = new XPathResult(infile)
		def nodes = rez.getNodes(xpath)
		println nodes.size()
//		for (Node node : rez.getNodes(xpath)) {
//			println "Found $node"
//			//nodes << [node, ""]
//		}
	}
	
	
	public static void main(String[] args) {
		File inputXMLFile = new File("/home/mdecorde/xml/comere/cmrclavardage/cmr-getalp_org-actu-tei-v1.xml");
		File outputXMLFile = new File("/home/mdecorde/xml/comere/cmrclavardage/tmp.xml");
		def processor = new XMLDomTokenizerAdapter(inputXMLFile, "/tei:TEI/tei:text/tei:body/tei:div/tei:post") //tei:div/tei:post/tei:p
		processor.process(outputXMLFile)
		//println processor.getNodes()
	}
}
