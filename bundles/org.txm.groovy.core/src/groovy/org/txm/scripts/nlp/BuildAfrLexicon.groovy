// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

import groovy.lang.GroovyClassLoader

import java.io.File

import org.txm.utils.ProcessBuilderBuilder
// TODO: Auto-generated Javadoc

/**
 * build an afr lexicon in a SQL table.
 *
 * @author mdecorde
 */
class BuildAfrLexicon 
{
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	static void main(String[] args)
	{
		HSQLFunctions.clearAll();
		
		//get lexicon from TXM RGAQCJ
		ProcessBuilderBuilder.build(new File("src/groovy/org/txm/scripts/cwb-decode-wrapper-definition.xml"), new File("src/groovy/org/textometrie/scripts/CwbDecode.groovy"));

		GroovyClassLoader gcl = new GroovyClassLoader();
		gcl.addClasspath(".");

		String registryPath = "~/Bureau/trunkCWB/corpora/registry";
		//String registryPath = "C:/Documents and Settings/sheiden/.txm/registry"
		 String cwbdecodeexecDir = "~/Bureau/trunkCWB/cwb-3.0/utils/"
		//String cwbdecodeexecDir = "C:/Projets/Textom�trie/Logiciel/Toolbox/0.4.5/toolbox/src/main/C/cwb-3.0/utils/"

		Class clazz = gcl.parseClass(new File("src/groovy/org/txm/scripts/CwbDecode.groovy"));
		def aScript = clazz.newInstance(cwbdecodeexecDir);

		aScript.setC()
		def pAttributes = ["word","CATTEX2009","LEMMA"];
		def sAttributes = ["s"];
		aScript.setP(pAttributes)
		//aScript.setS(sAttributes)
		aScript.setr(registryPath)
		
String rootdir = "~/xml/rgaqcj/lexicon";
File lexRGAQCJFile = new File(rootdir, "lexRGAQCJ.txt");
FileOutputStream fos = new FileOutputStream(lexRGAQCJFile);
PrintStream ps = new PrintStream(fos);
def out = System.out;
System.setOut(ps);

		if(System.getProperty("os.name").contains("Windows"))
			aScript.cwbdecodeexe("RGAQCJ")
		else
			aScript.cwbdecode("RGAQCJ")
System.setOut(out);	
println("fin")
		//Load lexRGAQCJ.txt >> lexRGAQCJ(form,cat,val)
		String[] argsname = ["form","cat","val"];
		String[] types = ["VARCHAR(30)","VARCHAR(30)","VARCHAR(100)"];

		HSQLFunctions.ImportRefTable("lexRGAQCJ", argsname,types,lexRGAQCJFile,"\t","ISO-8859-1");
		
		//load afrlex.txt >> afrlex(form,cat,val)
		File afrlexFile = new File(rootdir, "afrlex.txt");
		HSQLFunctions.ImportRefTable("afrlex", argsname,types,afrlexFile,"\t","ISO-8859-1")

		//Load TTcna_2_CTX9.txt >> TTcna_2_CTX9(catcna,catctx9)
		File correspFile = new File(rootdir, "TTnca_2_CTX9.txt");
		argsname = ["catcna","catctx9"];
		types = ["VARCHAR(20)","VARCHAR(20)"];
		HSQLFunctions.ImportCSVTable("TTnca_2_CTX9", argsname,types,correspFile,"\t","ISO-8859-1")

		//Union(lexRGAQCJ, afrlex) >> lextotal(form,cat,val)
		//HSQLFunctions.CreateTable( "lextotal", argsname, types);
		String query = 	"INSERT INTO afrlex " +
						"SELECT * from lexRGAQCJ "+
						"WHERE " +
						"form NOT IN (SELECT form FROM afrlex) " +
						"AND cat NOT IN (SELECT cat FROM afrlex) ";  
		HSQLFunctions.executeQuery( query );

		
		//check for missing cat correspondence
		String query2 = "SELECT cat from afrlex WHERE cat NOT IN (SELECT catcna FROM TTnca_2_CTX9);"
		int numberOfCatNotSpecif = 0
		def addedCat = [];
		HSQLFunctions.getGroovySql().eachRow(query2) {
			if(!addedCat.contains(it.getAt(0)))
			{
				addedCat.add(it.getAt(0));
				println("!! No correspondance for "+ it.getAt(0));
				HSQLFunctions.executeQuery("INSERT INTO TTnca_2_CTX9 VALUES ('"+it.getAt(0)+"','"+it.getAt(0)+"ERROR')");
				numberOfCatNotSpecif++;
			}
		}
		
		//Substitution des cat >> lexRGAQCJ(form,cat,val)
		String query3 = "UPDATE afrlex " +
				"SET cat = (SELECT catctx9 FROM TTnca_2_CTX9 WHERE cat=catcna)";				
		HSQLFunctions.executeQuery( query3 );
		
		HSQLFunctions.printTable "afrlex";
	
		//export lexRGAQCJ >> lexRGAQCJ.txt
		File rezlexRGAQCJFile = new File(rootdir, "lexfra-rgaqcj.txt");
		HSQLFunctions.toRefFile( "afrlex", rezlexRGAQCJFile.getAbsolutePath(),"form");
		
	}
}
