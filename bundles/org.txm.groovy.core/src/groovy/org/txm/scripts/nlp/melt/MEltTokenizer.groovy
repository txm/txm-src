package org.txm.scripts.tal.melt

import org.txm.tal.XMLStaxTokenizerAdapter;

class MEltTokenizer extends XMLStaxTokenizerAdapter {
	
	public MEltTokenizer(File infile, def xpaths) {
		super(infile, xpaths);
	}
	
	protected List<CharSequence> tokenize(String str) {
		def ret = str.tokenize()
		return ret;
	}
	
	public static void main(String[] args) {
		File inputXMLFile = new File("/home/mdecorde/xml/xmltest/txt1.xml");
		File outputXMLFile = new File("/home/mdecorde/xml/xmltest/txt1-o.xml");
		
		new MEltTokenizer(inputXMLFile, ["/root/subtruc/submachin/"]).process(outputXMLFile)
	}
}
