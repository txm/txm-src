// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2015-12-17 12:11:39 +0100 (jeu. 17 déc. 2015) $
// $LastChangedRevision: 3087 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * Read a list of word from a txt file and inject them in w elements
 *
 * @author mdecorde
 */
class ParseAndInject {
	
	/** The writer. */
	XMLStreamWriter writer;
	def output;
	
	/** The lang. */
	String lang;
	
	/** The type. */
	String type;
	
	/** The brutreader. */
	BufferedReader brutreader;
	
	/** The line. */
	String line;
	
	/** The current. */
	int current;
	
	/** The split. */
	String[] split;
	
	/** The token. */
	String token;
	
	/** The is ex. */
	boolean isEx;
	
	/** The is w. */
	boolean isW;
	
	/**
	 * start the processing.
	 *
	 * @param brutfile the brutfile
	 * @param xmlfile the xmlfile
	 * @param outfile the outfile
	 */
	public void run(File brutfile, File xmlfile,  File outfile) throws Exception
	{
		String localname;
		
		// create XML writer
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		output = new FileOutputStream(outfile);
		writer = factory.createXMLStreamWriter(output, "UTF-8");
		
		// create XML reader
		InputStream inputData = xmlfile.toURI().toURL().openStream();
		XMLInputFactory inputfactory = XMLInputFactory.newInstance();
		XMLStreamReader parser = inputfactory.createXMLStreamReader(inputData);
		
		//create Exbrut whitespace tokenizer
		brutreader = new BufferedReader(new FileReader(brutfile));
		
		writer.writeStartDocument("UTF-8","1.0");
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
				
					if (localname == "w") {
						isW=true;
					}
					writer.writeStartElement(localname); // create text tag in each xml file
					for (int i= 0 ; i < parser.getAttributeCount() ; i++ ) {
						writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
					}
					break;
				
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					if (localname == "w") {
						isW=false;
					}
					writer.writeEndElement();
				
					break;
				
				case XMLStreamConstants.CHARACTERS:
					if (isW) {
						token = this.getNextToken();
						//System.err.println("next token "+token);
						if (token == null) {
							System.err.println("error getting next word");
							return;
						}
						String text = parser.getText();
						int dist = Distance.LD(token, text);// check integrity
						int max = 0;
						if (token.contains("(("))
						 max += 2
						if (token.contains("))"))
							max += 2
						if (dist > max) {
							writer.writeComment("Warning !! Distance($token, $text) > 4")
						}
						
						processToken();
						//writer.writeCharacters(token);
					} else {
						writer.writeCharacters(parser.getText());
					}
					break;
			}
		}
		
		writer.close();
		output.close();
		parser.close();
		inputData.close();
	}
	
	/**
	 * Process token.
	 */
	public void processToken()
	{
		//System.err.println("process $token");
		if (token.length() == 0)
			return;
		int idxopen = token.indexOf("((")
		int idxclose = token.indexOf("))")
		if (isEx) {
			if (idxopen != -1)
			if (idxopen < idxclose) {
				System.err.println("Error: isEx $isEx token: $token : $idxopen < $idxclose");
				return;
			}
			if (idxclose == -1) {
				writer.writeStartElement("ex");
				writer.writeCharacters(token);
				writer.writeEndElement();
				return;
			}
			
			String ok = token.substring(0, idxclose);
			
			writer.writeStartElement("ex");
			writer.writeCharacters(ok);
			writer.writeEndElement();
			isEx = !isEx;
			
			token = token.substring(idxclose+2);
			processToken();
		} else {
			if (idxclose != -1)
			if (idxclose < idxopen) {
				System.err.println("Error: isEx $isEx token: $token : $idxclose < $idxopen");
				return;
			}
			if (idxopen == -1) {
				//System.out.println("write "+token);
				writer.writeCharacters(token);
				return;
			}
			
			String ok = token.substring(0, idxopen);
			writer.writeCharacters(ok);
			isEx = !isEx;
			token = token.substring(idxopen+2);
			processToken();
		}		
	}
	
	/**
	 * Gets the next token.
	 *
	 * @return the next token
	 */
	public String getNextToken() throws IOException
	{
		if (split == null) {
			line = brutreader.readLine();
			if (line == null)
				return null;
			
			split = line.split(" ");
			if (split == null || split.length == 1) {
				split = null;
				return line;
			} else {
				current = 0;
				return split[current++];
			}
		} else {
			if (current < split.length) {
				return split[current++];
			} else {
				split = null;
				return getNextToken();
			}
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File xmlfile = new File(System.getProperty("user.home"),"xml/injectEx/test.xml");
		File brutfile = new File(System.getProperty("user.home"),"xml/injectEx/brut.txt");
		File outfile = new File(System.getProperty("user.home"),"xml/injectEx/test-out.xml");
		try {
			new ParseAndInject().run(brutfile, xmlfile, outfile);
			System.out.println("done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}




