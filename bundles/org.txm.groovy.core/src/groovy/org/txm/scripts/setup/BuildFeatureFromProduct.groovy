package org.txm.scripts.setup;

import groovy.xml.*

String home = System.getProperty("user.home")
File rcpSrc = new File(home, "workspace047/org.txm.rcp")
File rcpFeatureSrc = new File(home, "workspace047/org.txm.rcp.feature")
File productFile = new File(rcpSrc, "rcp.product")
File featureFile = new File(rcpFeatureSrc, "feature.xml")
//File featureFileBackUp = new File(rcpFeatureSrc, "feature_back.xml")


def slurper = new XmlParser();
def product = slurper.parse(productFile.toURI().toString())

def plugins = product.plugins.plugin
def toSend = []
for (def plugin : plugins) { // set @os @ws @arch and @nl
	boolean remove = false;
	String id = plugin.@id
	if (id.contains(".linux")) plugin.@os = "linux"
	else if (id.contains(".win32")) plugin.@os = "win32"
	else if (id.contains(".macosx")) plugin.@os = "macosx"
	else if (id.contains(".aix")) remove = true
	else if (id.contains(".hpux")) remove = true
	else if (id.contains(".qnx")) remove = true
	else if (id.contains(".solaris")) remove = true
	
	if (id.contains(".wpf")) plugin.@ws = "wpf"
	else if (id.contains(".win32")) plugin.@ws = "win32"
	else if (id.contains(".photon")) remove = true
	else if (id.contains(".motif")) remove = true
	else if (id.contains(".gtk")) plugin.@ws = "gtk"
	else if (id.contains(".cocoa")) plugin.@ws = "cocoa"
	else if (id.contains(".carbon")) plugin.@ws = "carbon"
	else if (id.contains(".s390")) remove = true
	else if (id.contains(".s390x")) remove = true
	
	if (id.contains(".x86_64")) plugin.@arch = "x86_64"
	else if (id.contains(".x86")) plugin.@arch = "x86"
	else if (id.contains(".ia64_32")) remove = true
	else if (id.contains(".ia64")) remove = true
	else if (id.contains(".spark")) remove = true
	else if (id.contains(".ppc")) remove = true
	else if (id.contains(".PA_RISC")) remove = true
	
//	def m = id =~ /.+\.nl_(..)/
//	if (m.size() > 0) {
//		if (m[0].size() > 1) {
//			plugin.@nl = m[0][1]
//		}
//	}
	
	plugin."@download-size"="0"
	plugin."@install-size"="0"
	plugin.@version="0.0.0"
	plugin.@unpack="false"
	
	if (!remove) {
		toSend << plugin
	}
}

println "FEATURE"

def feature = slurper.parse(featureFile.toURI().toString())

for (def plugin : toSend) {
	feature.append(plugin)
}
//println XmlUtil.serialize(feature)
featureFile.write(XmlUtil.serialize(feature))
