package org.txm.scripts.setup

File svnLog = new File("/home/mdecorde/SVN/ALLTXMSVN/txm/log.xml");
File outRev = new File("/home/mdecorde/TEMP/stats_rev.tsv")
File outPeriode = new File("/home/mdecorde/TEMP/stats_periode.tsv")
def periodes = [1624, 1771, 1823, 2095, 2299]
def SVNROOT = new File("/home/mdecorde/SVN/ALLTXMSVN/txm")
def composants = ["/trunk/CWB/", "/trunk/doc/", "/trunk/groovy-all/", "/trunk/RCP/", "/trunk/textometrieR/", "/trunk/Toolbox/", "/trunk/WEB/", ""]

def logs = new XmlParser().parse(svnLog);
def revisions = new LinkedHashMap<String, Object>();
def allPackages = new HashSet<String>();
def allFiles = new HashSet<String>();

println "Counting revisions..."
for (def logEntry : logs.logentry) {
	// SVN log.xml infos
	def commitAuthor = logEntry.author.text();
	def commitDate = logEntry.date.text();
	def nUpdate = 0;
	def nAdd = 0
	def nDelete = 0;
	def newPackages = new HashSet<String>()
	def newFiles = new HashSet<String>()

	for (def path : logEntry.paths.path) {
		String file = path.text();
		switch(path.'@action') {
			case "M":
				nUpdate++
				break;
			case "A":
				nAdd++
				if (file.matches(".+\\.[A-Za-z]{1,10}")) {
					newFiles << file;
				} else {
					newPackages << file;
				}
				break;
			case "D":
				nDelete++;
				break;
			default:
				break;
		}
	}

	// SVN file explore
	def nLines = 0;
	def authors = [commitAuthor]
	// count lines
	// get @author

	revisions[Integer.parseInt(logEntry.'@revision')] = [commitDate, authors, nUpdate, nAdd, nDelete, newPackages.size(), newPackages, newFiles.size(), newFiles]
}

def periodesStats = new LinkedHashMap<String, Object>()
int currentPeriode = 0
def nCommit = 0
def nUpdate = 0
def nAdd = 0
def nDelete = 0
def authors = new HashSet<String>()
def newPackages = new HashSet<String>()
def newFiles = new HashSet<String>()

println "Counting periodes..."
def revKeys = revisions.keySet().sort()
def previousRev = revKeys[0]
for( def rev : revKeys) {
	if (rev >= periodes[currentPeriode]) {
		//println "end of periode "+periodes[currentPeriode]+ " at rev "+rev
		periodesStats[periodes[currentPeriode]] = [revisions[previousRev][0], authors, nCommit, nUpdate, nAdd, nDelete, newPackages.size(), newFiles.size(), newPackages]

		currentPeriode++
		authors = new HashSet<String>()
		nCommit = 0;
		nUpdate = 0;
		nAdd = 0
		nDelete = 0;
		newPackages = new HashSet<String>()
		newFiles = new HashSet<String>()
	}
	def infos = revisions[rev]
	nCommit++
	authors.addAll(infos[1])
	nUpdate += infos[2]
	nAdd += infos[3]
	nDelete += infos[4]
	newPackages.addAll(infos[6])
	newFiles.addAll(infos[8])
	
	previousRev = rev
}
//periodesStats[2299] = [nCommit, authors, nUpdate, nAdd, nDelete, newPackages.size(), newPackages, newFiles.size(), newFiles]


println "Counting composants of periodes..."
for(def p : periodesStats.keySet()) {
	def infos = periodesStats[p]
	//println infos.size()
	newPackages = infos[8]
	def newPackagesComp = [:]

	for (String path : newPackages) {
	for (int i = 0 ; i < composants.size() ; i++) {
		def comp = composants[i]
		
			if (path.startsWith(comp)) {
				if (newPackagesComp[comp] == null) newPackagesComp[comp] = new HashSet<String>();
				newPackagesComp[comp] << path.substring(comp.length())
				break;
			}
		}
	}
	for (int i = 0 ; i < composants.size() ; i++) {
		def comp = composants[i]
		if (newPackagesComp[comp] != null)
			newPackagesComp[comp] = newPackagesComp[comp] .size()
	}
	infos[8] = newPackagesComp;
}

println "revisions stat size: "+revisions.size()
println "periodes stat size: "+periodesStats.size()

println "writing outRev"
outRev.withWriter("UTF-8") { writer ->
	writer.println "rev\tdate\tauthors\tnUpdate\tnAdd\tnDelete\tnNewPackages\tnewPackages\tnNewFiles\tnewFiles"
	for( def rev : revisions.keySet()) {
		writer.println rev+"\t"+revisions[rev].join ("\t")
	}
}
//[revisions[previousRev][0], authors, nCommit, nUpdate, nAdd, nDelete, newPackages.size(), newFiles.size(), newPackages]
println "writing outPeriode"
outPeriode.withWriter("UTF-8") { writer ->
	writer.println "periode\tdate\tauthors\tnCommit\tnUpdate\tnAdd\tnDelete\tnNewPackages\tnNewFiles\tnewPackagesPerComp"
	for(def p : periodesStats.keySet()) {
		writer.println p+"\t"+periodesStats[p].join ("\t")
		writer.flush()
	}
}
