package org.txm.scripts.scripts

import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlSlurper
import groovy.xml.XmlParser

def importxml = new File("/home/mdecorde/TEMP/import.xml")
def usersdir = new File("/home/mdecorde/TEMP/users")
def usersdir2 = new File("/home/mdecorde/TEMP/users2")
usersdir2.deleteDir()
usersdir2.mkdir()

// 1- get subcorpus & partitions per user
def entities = [:];
corpusInfo = [:];
def root = new XmlSlurper().parse(importxml)

def corpusNode = null
for (def tmp : root.corpora.corpus) corpusNode = tmp
def prefix = "/"+corpusNode.@name.toString()

for (def preBuild : corpusNode.preBuild) {
	for (def child : preBuild.children()) {
		def tmp_entities = getEntities(prefix, child)
		def user = "";
		for (def entity : tmp_entities) {
			String name = entity;
			user = entity.substring(prefix.length()+1, entity.indexOf(":"))
			//println user
			break;
		}

		if (!entities.keySet().contains(user)) entities[user] = new HashSet<String>();
		for (def entity : tmp_entities) {
			entities[user] << entity
		}
	}
}
println entities
println corpusInfo

// 2- fix user_xxxx.xml
for (def file : usersdir.listFiles()) {
	if (!file.getName().endsWith(".xml")) continue;

	def rootuser = new XmlParser().parse(file);
	String login = rootuser.@login
	if (entities.keySet().contains(login)) {
		def user_entities = entities[login]

		def existingEntities = new HashSet();
		for (def entityNode : rootuser.permissions.entity) {
			existingEntities << entityNode.@path.toString()
		}
		println "FIXING $file with $user_entities existing: $existingEntities"
		for (def newentity : user_entities) {
			if (!existingEntities.contains(newentity)) {
				def permissionNode = null
				for( def tmp : rootuser.permissions) permissionNode = tmp
				// new
				println " append $newentity "
				def newnode = new Node(null, "entity");
				newnode.@path = newentity
				addPermissions(newnode, corpusInfo[newentity])
				permissionNode.children().add(newnode)
			}
		}

		new File(usersdir2, file.getName()).withWriter("UTF-8") { writer ->
			new XmlNodePrinter(new PrintWriter(writer)).print(rootuser)
		}
	}
}




def addPermissions(def node, def type) {
	
	def subcorpusPerm = ["EditionPermission", "SpecificitiesPermission","IndexPermission","DictionnaryPermission",
		"TextSelectionPermission", "CreateSubcorpusPermission", "DeletePermission", "DimensionsPermission",
		"CreatePartitionPermission","DisplayPermission"]
	
	def partitionPerm = ["SpecificitiesPermission","IndexPermission","DictionnaryPermission",
		"AFCPermission", "DeletePermission", "DimensionsPermission","DisplayPermission"]
	
	def children = node.children();

	if (type == "partition") {
		for( def perm : partitionPerm) {
			def newnode = new Node(null, "permission");
			newnode.@anti = "false"
			newnode.@value = perm
			children.add(newnode);
		}
	} else if (type == "subcorpus") {
		def concnode = new Node(null, "permission");
		concnode.@anti = "false"
		concnode.@value = "ConcordancePermission"
		concnode.@leftContext = 9999
		concnode.@rightContext = 9999
		children.add(concnode);
		for( def perm : subcorpusPerm) {
			def newnode = new Node(null, "permission");
			newnode.@anti = "false"
			newnode.@value = perm
			children.add(newnode);
		}
	}
}

def getEntities(String prefix, def node) {
	def list = []

	String name = node.@name
	String tmp = prefix +"/"+name
	list << tmp

	corpusInfo[tmp] = node.name()

	for (def child : node.children()) {
		if (child.name().toString() == "partition" | child.name().toString() == "subcorpus") {
			list.addAll(getEntities(tmp,child))
		}
	}
	return list
}
