package org.txm.scripts.setup

import org.txm.utils.io.FileCopy;
import groovy.xml.*


String home = System.getProperty("user.home")
File rcpSrc = new File(home, "workspace047/org.txm.rcp.feature")
File productFile = new File(rcpSrc, "feature.xml")

//File dir = new File("/home/mdecorde/ECLIPSE/babel/babel-kepler/features/org.eclipse.babel.nls_eclipse_ru_4.3.0.v20131123020001")
File dir = new File(home, "workspace047/babel/eclipse/features/org.eclipse.babel.nls_eclipse_fr_4.7.0.v20190126095834")

File origFeatureFile = new File(dir, "feature_orig.xml") // will be created if not, and parsed
File featureFile = new File(dir, "feature.xml")
File featureResultFile = new File(dir, "result_feature.xml")

if (!origFeatureFile.exists()) { // backup feature file if not already done
	FileCopy.copy(featureFile, origFeatureFile)
}

def slurper = new XmlParser();

// get used plugin IDs
def product = slurper.parse(productFile.toURI().toString())
def usedPlugins = new HashSet<String>()

for (def plugin : product.requires.import) {
	usedPlugins.add(plugin.@plugin)
}

println "Number of plugins declared in product: "+usedPlugins.size()

// Fix feature : remove unused plugins and fix @os @ws @arch and @nl attributes
def feature = slurper.parse(origFeatureFile.toURI().toString())
def plugins = feature.plugin
def toSend = []
for (def plugin : plugins) { // set @os @ws @arch and @nl
	boolean remove = false;
	String id = plugin.@id
	
	String hostid = id.substring(0, id.lastIndexOf(".nl_")) // remove the nl_xx.yyyyyyy part
	println "$id >> $hostid"
	
	// set os
	if (id.contains(".linux")) plugin.@os = "linux"
	else if (id.contains(".win32")) plugin.@os = "win32"
	else if (id.contains(".macosx")) plugin.@os = "macosx"
	else if (id.contains(".aix")) remove = true
	else if (id.contains(".hpux")) remove = true
	else if (id.contains(".qnx")) remove = true
	else if (id.contains(".solaris")) remove = true
	
	// set ws
	if (id.contains(".wpf")) plugin.@ws = "wpf"
	else if (id.contains(".win32")) plugin.@ws = "win32"
	else if (id.contains(".photon")) remove = true
	else if (id.contains(".motif")) remove = true
	else if (id.contains(".gtk")) plugin.@ws = "gtk"
	else if (id.contains(".cocoa")) plugin.@ws = "cocoa"
	else if (id.contains(".carbon")) plugin.@ws = "carbon"
	
	if (id.contains(".s390")) remove = true
	else if (id.contains(".s390x")) remove = true
	
	// set arch
	if (id.contains(".x86_64")) plugin.@arch = "x86_64"
	else if (id.contains(".x86")) plugin.@arch = "x86"
	else if (id.contains(".ia64_32")) remove = true
	else if (id.contains(".ia64")) remove = true
	else if (id.contains(".spark")) remove = true
	else if (id.contains(".ppc")) remove = true
	else if (id.contains(".PA_RISC")) remove = true
	
	// set nl
//	def m = id =~ /.+\.nl_(..)/
//	if (m.size() > 0) {
//		if (m[0].size() > 1) {
//			plugin.@nl = m[0][1]
//		}
//	}
	
	if (remove) {
		plugin.parent().remove(plugin)
	} else if (!usedPlugins.contains(hostid)) {
		plugin.parent().remove(plugin)
	}
}

println "WRITE RESULT FEATURE"
//println XmlUtil.serialize(feature)
featureResultFile.write(XmlUtil.serialize(feature))
