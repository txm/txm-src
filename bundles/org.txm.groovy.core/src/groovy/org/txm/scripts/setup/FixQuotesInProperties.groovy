package org.txm.scripts.setup

File propDirFile = new File("/home/mdecorde/workspace441/org.txm.core/src/java/org/txm")

for (File propFile : propDirFile.listFiles()) {
	if (!propFile.getName().endsWith(".properties")) continue;

	println "FILE: $propFile"
	String text = propFile.getText("iso-8859-1")

	propFile.withWriter("iso-8859-1") { writer->
		for (String line : text.split("\n")) {
			if (line.contains("{0}") || line.contains("{1}") || line.contains("{2}") || line.contains("{3}")) {
				writer.println line.replaceAll("''", "'").replaceAll("''", "'").replaceAll("'", "''")
			} else {
				writer.println line
			}
		}
	}
}