package org.txm.scripts.setup

import org.txm.importer.ApplyXsl2;
import groovy.xml.XmlParser

// build log.xml
// cd /home/mdecorde/workspace37/org.txm.core
// svn log --xml --verbose > /home/mdecorde/workspace37/org.txm.core/log.xml
String home = System.getProperty("user.home")
File rootDir = new File(home, "SVN/TXMSVN")
File xsl = new File(home, "xsl/svn2cl.xsl")
File rawLogs = new File(home, "TEMP/portal_hist.txt")
File log = new File(home, "TEMP/log.txt")
File tbx = new File(home, "SVN/TXMSVN")

println "** FILES:"
println home
println rootDir
println xsl
println rawLogs
println log
println tbx

String dateLimit = "2014-01"
String revLimit = "1"

String cmd = """svn log --xml --verbose $tbx"""
println "\n** SVN LOG: "+cmd
def proc = cmd.execute()
rawLogs.withWriter("UTF-8") { writer -> writer.write(proc.in.getText()) }
proc.waitFor()
if (proc.exitValue() != 0) {
	println "SVN LOG error code: "+proc.exitValue()
	println "Error: "+proc.err.getText()
	return;
} 

println "** Filter XML : by date=$dateLimit and rev=$revLimit"
def slurper = new XmlParser();
def root = slurper.parse(rawLogs.toURI().toString())
log.withWriter("UTF-8") { writer ->
	for (def entry : root.logentry) {
		String rev = entry.@revision
		if (rev.compareTo(revLimit) < 0) continue
		def msgs = entry.msg
		
		for (def date : entry.date) {
			String sdate = date.text()
			if (sdate.compareTo(dateLimit) < 0) {
				return
			}
			writer.print("* "+sdate.substring(0, 10) + " - " + (sdate.substring(11, 19)))+ " REV "+rev
		}
		
		boolean first = true;
		for (def msg : msgs) {
			String smsg = msg.text().trim()
			if (smsg.length() == 0) continue;
			if (first) { first = false; smsg = "\n"+smsg; }
			smsg = smsg.replace("\nRCP", "\n** RCP")
			smsg = smsg.replace("\nTBX", "\n** TBX")
			smsg = smsg.replace("\nPLUGIN", "\n** PLUGIN")
			smsg = smsg.replace("\nWEB", "\n** WEB")
			smsg = smsg.replace("\nSETUP", "\n** SETUP")
			smsg = smsg.replace("\nDOC", "\n** DOC")
			smsg = smsg.replace("\nTRA", "\n** TRA")
			smsg = smsg.replace("\nCQP", "\n** CQP")
			smsg = smsg.replace("\nCWB", "\n** CQP")
			smsg = smsg.replace("\nINSTALL", "\n** INSTALL")
			smsg = smsg.replace("\nR textometry", "\n** R textometry")
			smsg = smsg.replace(" :", ":") // hahaha
			smsg = smsg.replace("textoemtry", "textometry") // hohoho
			smsg = smsg.replace("# ", "*** ")
			smsg = smsg.replace("#([0-9]+)", "[https://forge.cbp.ens-lyon.fr/redmine/issues/${1} #{1}]")
			writer.print smsg
		}
		writer.println ""
	}
}

println "RESULT SAVED IN : "+log.toURI()
