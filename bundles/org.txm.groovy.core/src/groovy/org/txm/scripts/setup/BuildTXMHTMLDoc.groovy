package org.txm.scripts.setup

import java.io.File;

import org.txm.macro.txt.SearchReplaceInDirectoryMacro;
import org.txm.utils.io.FileCopy;

String home = System.getProperty("user.home")
String svnpath = "SVN/TXMSVN/trunk/doc/user manuals"

File svnDir = new File(home, svnpath)
File svnImagesDir = new File(svnDir, "ImagesTXM")
//File odtFile = new File(home, "SVN/TXMSVN/trunk/doc/user manuals/Manuel de TXM 0.7 FR.odt")
File htmlDir = new File(svnDir, "txm-manual-fr.html")

//if (!htmlDir.listFiles()) {
//	println "Error: you must empty $htmlDir before. Aborting."
//	return;
//}

if (!svnDir.exists()) {
	println "Error: Cannot find SVN files $svnDir. Aborting"
	return
}

//// Update SVN // marche pas :(
//String cmd = "svn update \"$svnDir\""
//println "SVN update: "+cmd
//def proc = cmd.execute()
//proc.in.getText()
//proc.waitFor()
//if (proc.exitValue() != 0) {
//	println "SVN update error code: "+proc.exitValue()
//	println "Error: "+proc.err.getText()
//	return;
//} 

// Copy images
//File htmlImagesDir = new File(htmlDir, svnImagesDir.getName())
//FileCopy.copyFiles(svnImagesDir, htmlImagesDir)
//if (!htmlImagesDir.exists()) {
//	println "Failed to copy $htmlImagesDir"
//	return
//}

// Fix HTML files
// fix images source directory - remove SVN absolute path
svnpath = svnpath.replaceAll(" ", "%20")
def changes = [
	[/ src="\.\.\/\.\.\/$svnpath\//, ' src="'],
	[/"ImagesTXM/,	'"../ImagesTXM'],
	[/<style media="all" type="text\/css">/, '<link rel="stylesheet" type="text/css" href="style.css"/>\n<style media="all" type="text/css">']
]

for (def change : changes) {
	println change
	new SearchReplaceInDirectoryMacro().process(htmlDir, "\\.xhtml", "UTF-8",
		change[0], 
		change[1], false)
}
