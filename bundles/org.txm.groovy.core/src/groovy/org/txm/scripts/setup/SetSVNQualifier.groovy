import java.util.jar.Manifest
import groovy.xml.XmlSlurper
import org.txm.setup.SVNStats

File workspace = new File(System.getProperty("user.home"), "workspace047")

for (File dir : workspace.listFiles()) {
	if (!dir.isDirectory()) continue
	
	File manifestFile = new File(dir, "META-INF/MANIFEST.MF")
	if (!manifestFile.exists()) continue;
	
	Manifest manifest = new Manifest();
	def inputStream = manifestFile.toURI().toURL().openStream()
	manifest.read(inputStream)
	inputStream.close();
	
	def id = manifest.getMainAttributes().getValue("Bundle-SymbolicName")
	def version = manifest.getMainAttributes().getValue("Bundle-Version")
	
	String xml = "svn info --xml".execute().getText()
	String revision = new XmlSlurper().parseText(xml).info.entry["@revision"]
	println "id=$id"
	println "version=$version"
	println "revision=$revision"
	
	
//	if (id.startsWith("org.txm")) {
//		println "$id ..."
//		if (version.endsWith(".qualifier")) {
//			println "	fix .qualifier"
//			version = version.replaceAll(".qualifier", revision)
//		} else if (version.matches("^.+\.r[0-9]+\$")) {
//			version.replaceAll(("^.+\.(r[0-9]+)\$", revision)
//		} else {
//			println "	ERROR with version format: $version"
//		}
//	}
}
