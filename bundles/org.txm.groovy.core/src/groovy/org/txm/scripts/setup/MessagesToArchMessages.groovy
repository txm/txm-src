package org.txm.scripts.setup
rootDir = new File("/home/mdecorde/paquets")
linuxMessagesDir = new File(rootDir, "shared/debian/DEBIAN")
windowsMessagesDir = new File(rootDir, "archs/win")
macMessagesDir = new File(rootDir, "shared/mac")

windowsMessages = ";START OF LANG MESSAGES\n"
win_langs = ["en": "ENGLISH" , "fr":"FRENCH"]

needed_keys = [
	"InfosTitle", "InfosText", "WelcomeTitle", "WelcomeText",
	"InstallRLibrariesTitle", "InstallRLibrariesText", "FinishTitle",
	"FinishTextWindows", "FinishTextLinux", "FinishTextMac",
	"ErrorArch32", "ErrorArch64", "UninstallWindows", "InstallTreeTagger"]

if (!(writeLangFiles("en") && writeLangFiles("fr"))) {
	println "Stoping process"
	return;
} 

windowsMessages += ";END OF LANG MESSAGES\n"

File nsis32 = new File(windowsMessagesDir, "32bit/txm.nsi")
File nsis64 = new File(windowsMessagesDir, "64bit/txm.nsi")

if (!(patchNSIS(nsis32) && patchNSIS(nsis64))) {
	println "Error while patching nsis files"
}

def patchNSIS(File nsis) {
	String text = nsis.getText("Windows-1252")
	boolean startReplacing = false;
	nsis.withWriter("Windows-1252") { writer ->
		for (String line : text.split("\n")) {
			if (";START OF LANG MESSAGES" == line.trim()) {
				startReplacing = true
				line = windowsMessages
				writer.println line
			}
			if (";END OF LANG MESSAGES" == line.trim())  {
				startReplacing = false
				continue
			}

			if (!startReplacing) {
				writer.println line
			}
		}
	}
	return true;
}
def checkProps(def props) {
	def missing = []
	for (String prop : needed_keys)
		if (!props.containsKey(prop))
			missing << prop
	return missing
}

def writeLangFiles(String lang) {
	println "Writing lang files for $lang"
	File propFile = new File(rootDir, "SetupMessages_${lang}.properties")

	Properties props = new Properties()
	InputStreamReader input = new InputStreamReader(new FileInputStream(propFile) , "UTF-8")
	props.load(input)
	input.close()

	def missingProps = checkProps(props)
	if (missingProps.size() > 0) {
		println "Error missings keys $lang: "+missingProps
		return false;
	}

	String ext = ""
	if (lang.length() > 0) ext = "."+lang

	// LINUX
	File template = new File(linuxMessagesDir, "templates"+ext)
	template.withWriter("UTF-8") { writer ->
		writer.println(
"""Template: TXM/showinfos
Type: note
Description:"""+props.get("InfosTitle"))
		writer.println(
				" .\n  "+props.get("InfosText").replaceAll("\n\n", '\n .\n').replaceAll("\n([^ ])", '\n  $1').replaceAll("\n", '\n '))
		writer.println()
		writer.println(
"""Template: TXM/showwelcome
Type: note
Description:"""+props.get("WelcomeTitle"))
		writer.println(
				" .\n  "+props.get("WelcomeText").replaceAll("\n\n", '\n .\n').replaceAll("\n([^ ])", '\n  $1').replaceAll("\n", '\n '))
		writer.println()
		writer.println(
"""Template: TXM/askr
Type: boolean
Default: true
Description:"""+props.get("InstallRLibrariesTitle"))
		writer.println(
				" .\n  "+props.get("InstallRLibrariesText").replaceAll("\n\n", '\n .\n').replaceAll("\n([^ ])", '\n  $1').replaceAll("\n", '\n '))
		writer.println()
		writer.println(
"""Template: TXM/finish
Type: note
Description:"""+props.get("FinishTitle"))
	writer.println(
		" .\n  "+props.get("FinishTextLinux").replaceAll("\n\n", '\n .\n').replaceAll("\n([^ ])", '\n  $1').replaceAll("\n", '\n '))
	writer.println(
		" .\n  "+props.get("InstallTreeTagger").replaceAll("\n\n", '\n .\n').replaceAll("\n([^ ])", '\n  $1').replaceAll("\n", '\n '))
	}

	//WINDOWS NSIS
	if (lang.length() > 0) ext = "_"+lang

	StringWriter swriter = new StringWriter();
	swriter.println(";Lang Strings "+win_langs[lang])
	swriter.println('LangString InfosTitleMessage ${LANG_'+win_langs[lang]+'} "'+props.get("InfosTitle").replaceAll("\"", "'")+'"')
	swriter.println('LangString InfosTextMessage ${LANG_'+win_langs[lang]+'} "'+props.get("InfosText").replaceAll("\n", '\\$\\\\n').replaceAll("\"", "'")+'"')
	swriter.println('LangString WelcomeTitleMessage ${LANG_'+win_langs[lang]+'} "'+props.get("WelcomeTitle").replaceAll("\"", "'")+'"')
	swriter.println('LangString WelcomeTextMessage ${LANG_'+win_langs[lang]+'} "'+props.get("WelcomeText").replaceAll("\n", '\\$\\\\n').replaceAll("\"", "'")+'"')
	// no R libs question
	swriter.println('LangString FinishTitleMessage ${LANG_'+win_langs[lang]+'} "'+props.get("FinishTitle".replaceAll("\"", "'"))+'"')
	swriter.println('LangString FinishTextMessage ${LANG_'+win_langs[lang]+'} "'+(props.get("FinishTextWindows") + '$\\n' + props.get("InstallTreeTagger")).replaceAll("\n", '\\$\\\\n').replaceAll("\"", "'")+'"')
	// Windows specific messages
	swriter.println('LangString ErrorArch32Message ${LANG_'+win_langs[lang]+'} "'+props.get("ErrorArch32").replaceAll("\"", "'")+'"')
	swriter.println('LangString ErrorArch64Message ${LANG_'+win_langs[lang]+'} "'+props.get("ErrorArch64").replaceAll("\"", "'")+'"')
	swriter.println('LangString UninstallWindowsMessage ${LANG_'+win_langs[lang]+'} "'+props.get("UninstallWindows").replaceAll("\n", '\\$\\\\n').replaceAll("\"", "'")+'"')
	swriter.close();

	windowsMessages += "\n"+swriter.toString()+"\n"

	//MAC 3 FILES : Intro, ReadMe and Finish
	File intromac = new File(macMessagesDir, "intro${ext}.txt")
	intromac.withWriter("x-MacRoman") { writer ->
		writer.println("")
		writer.println(props.get("InfosText"))
	}
	File readmemac = new File(macMessagesDir, "readme${ext}.txt")
	readmemac.withWriter("x-MacRoman") { writer ->
		writer.println("")
		writer.println(props.get("WelcomeText"))
	}
	File rlibsmac = new File(macMessagesDir, "rlibs${ext}.txt")
	rlibsmac.withWriter("x-MacRoman") { writer ->
		writer.println(props.get("InstallRLibrariesTitle"))
		writer.println(props.get("InstallRLibrariesText"))
	}
	File finishmac = new File(macMessagesDir, "finish${ext}.txt")
	finishmac.withWriter("x-MacRoman") { writer ->
		writer.println("")
		writer.println(props.get("FinishTextMac"))
		writer.println("")
		writer.println(props.get("InstallTreeTagger"))
	}
	
	return true;
}




