package org.txm.scripts.setup;

import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.imageio.ImageIO;
import org.txm.scripts.importer.XPathResult;
import groovy.xml.XmlSlurper

// Update splash screen using the RCP project version set in the product file
// The BMP image must be 32bit encoded

int img_width = 316;

File rcpDir = new File(System.getProperty("user.home"), "workspace047/org.txm.rcp")
File product = new File(rcpDir, "rcpapplication.product");
File imgFile = new File(rcpDir, "splash_sansversion.bmp");
File imgFile2 = new File(rcpDir,"splash.bmp");

String modifier = ""

//String version =  org.txm.rcp.Activator.getDefault().getBundle().getVersion().toString(); // "0.7.7.201412160925";
println "Reading Version from $product"
String version = new XmlSlurper().parse(product)['@version']
println "RAW version: $version"
//def date = " ("+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+")"
def date = ""
version = "TXM "+version.replaceAll(".qualifier", date);

if (modifier.length() > 0 ) version = version + " - "+modifier

println "VERSION: '$version'"

BufferedImage img = null;
img = ImageIO.read(imgFile);
Graphics g = img.getGraphics();
g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

File imgMascot = new File(System.getProperty("user.home"),"Images/splashs/"+version+".png");
println imgMascot
if (imgMascot.exists()) {
	println "write mascot"
	g.drawImage(ImageIO.read(imgMascot), 0, 0, img_width, 360, null, null);
}

Font font = new Font("Arial", Font.BOLD, 20);
g.setColor(new Color(109,139,0)); //
g.setFont(font);
FontMetrics fontMetrics = g.getFontMetrics();
int width = fontMetrics.stringWidth(version);
g.drawString(version, img_width - width - 20, 300);

ImageIO.write(img, "BMP", imgFile2);

println "Done: "+imgFile2.getAbsoluteFile()