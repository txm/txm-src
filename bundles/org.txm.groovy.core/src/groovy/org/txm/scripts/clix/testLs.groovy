/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts.clix;
import java.io.File;
import java.util.Properties;

import org.txm.importer.scripts.filters.Filter;
import org.txm.utils.ProcessBuilderBuilder;
import groovy.lang.GroovyClassLoader;

import org.txm.Toolbox
// TODO: Auto-generated Javadoc

/**
 * test Ls wrapper.
 *
 * @return the java.lang. object
 */
ProcessBuilderBuilder.build(new File("src/groovy/org/txm/scripts/ls-wrapper-definition.xml"), new File("src/groovy/org/textometrie/scripts/Ls.groovy"));

GroovyClassLoader gcl = new GroovyClassLoader();

def aScript

try
{
	gcl.addClasspath(".");
	Class clazz = gcl.parseClass(new File("src/groovy/org/txm/scripts/Ls.groovy"));
	aScript = clazz.newInstance("C:/Program Files/UnxUtils/usr/local/wbin/");
	
}
catch(Exception e)
{	System.err.println(e);
	return null;
}

aScript.setl()
aScript.lsexe()

