// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-09-05 09:27:13 +0200 (jeu. 05 sept. 2013) $
// $LastChangedRevision: 2529 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts;

import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
// Shell
/**
 * The Class Sh.
 */
public class Sh {
	
	/** The binpath. */
	String binpath = "";

	/** The version. */
	String version = "0.0.0";
	
	/** The desc. */
	String desc = "Shell";

	/**
	 * Instantiates a new sh.
	 *
	 * @param binpath the binpath
	 */
	public Sh(String binpath) {
		// System.out.println("set binpath "+binpath);
		this.binpath = binpath;
	}

	// execute command line in argument
	/** The isc. */
	private Boolean isc = false;
	
	/** The c. */
	private String c;

	/**
	 * Sets the isc.
	 *
	 * @param arg the new isc
	 */
	public void setIsc(String arg) {
		this.isc = true;
		this.c = arg;
	}

	/**
	 * Unset isc.
	 */
	public void unsetIsc() {
		this.isc = false;
	}

	// Sh
	/**
	 * Shexe.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void shexe() throws IOException {
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "sh.exe");
		if (isc)
			args.add("-c " + c);
		System.out.println(args);
		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (e != 0) {
			System.err.println("Process exited abnormally with code "
					+ e
					+ " at "
					+ DateFormat.getDateInstance(DateFormat.FULL, Locale.UK)
							.format(new Date()));
		}
	}

	// Sh
	/**
	 * Sh.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void sh() throws IOException {
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "sh");
		if (isc) {
			args.add("-c");
			args.add(c);
		}
		// System.out.println(args);
		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			Log.severe(Log.toString(e));
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			Log.fine(line);
		}
		int e = 0;
		try {
			e = process.waitFor();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (e != 0) {
			Log.fine("Process exited abnormally with code "
					+ e
					+ " at "
					+ DateFormat.getDateInstance(DateFormat.FULL, Locale.UK)
							.format(new Date()));
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// Sh tt = new Sh("");
		// tt.setIsc("kill `ps aux | grep cqpserver | awk '/-P 4877/ {print \\$2}'`");//
		// try {
		// tt.sh();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}
}