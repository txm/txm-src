// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.scripts.clix;

import java.util.Date;
import java.text.DateFormat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

// List directory
// TODO: Auto-generated Javadoc

/**
 * The Class Ls.
 */
class Ls {
	
	/** The binpath. */
	String binpath = "";

	/**
	 * Instantiates a new ls.
	 *
	 * @param binpath the binpath
	 */
	public Ls(String binpath) {
		this.binpath = binpath;
	}

	/** The version. */
	String version = "0.0.0";
	
	/** The desc. */
	String desc = "List directory";

	// use a long listing format
	
	/** The isl. */
	private Boolean isl = false;

	/**
	 * Sets the isl.
	 */
	public void setIsl() {
		this.isl = true;
	}

	/**
	 * Unset isl.
	 */
	public void unsetIsl() {
		this.isl = false;
	}

	// Ls
	
	/**
	 * Lsexe.
	 */
	public void lsexe() throws IOException {
		ArrayList<String> args = new ArrayList<String>();
		args.add(binpath + "ls.exe");
		if (isl)
			args.add("-l");

		ProcessBuilder pb = new ProcessBuilder(args);
		pb.redirectErrorStream(true);
		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			System.err.println(e);
		}
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		int e = process.waitFor();
		if (e != 0) {
			System.err.println("Process exited abnormally with code "
					+ e
					+ " at "
					+ DateFormat.getDateInstance(DateFormat.LONG).format(
							new Date()));
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Ls tt = new Ls("");
	}
}