// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2012-04-24 14:30:47 +0200 (mar., 24 avr. 2012) $
// $LastChangedRevision: 2174 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.doc;

import org.txm.scripts.importer.*;
import javax.xml.stream.XMLStreamReader;
import org.txm.importer.ApplyXsl2;
import org.txm.importer.ValidateXml;
import org.txm.scripts.importer.xml.importer;
import org.txm.scripts.importer.xml.compiler;
import org.txm.scripts.importer.xml.pager;
import org.txm.objects.*;
import org.txm.utils.*;
import org.txm.utils.io.*;
import org.txm.*;
import org.txm.core.preferences.TBXPreferences
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;
import org.txm.metadatas.*;
import javax.xml.stream.*;
import org.w3c.dom.Element;
import org.txm.utils.xml.DomUtils;
import org.txm.scripts.importer.RemoveTag;
import org.txm.scripts.doc.*;

String userDir = System.getProperty("user.home");

def MONITOR;
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName();
String basename = corpusname
String rootDir = project.getSrcdir();
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL();
def xslParams = project.getXsltParameters();
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
String page_element = project.getEditionDefinition("default").getPageElement()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

File txmDir = new File(binDir, "txm/$corpusname");
txmDir.deleteDir();
txmDir.mkdirs();

String textSortAttribute = null; // a property in metadata.csv file
String paginationElement = "pb";
boolean normalizeMetadata = false;

File allMetadataFile = Metadatas.findMetadataFile(srcDir);

def filesToProcess = FileUtils.listFiles(srcDir)
if (filesToProcess == null) {
	println "No file to process"
	return;
}

def srcfiles = [];
for (File f : filesToProcess) {
	String name = f.getName().toLowerCase();
	if (name.endsWith(".properties")) continue;
	if (name.equals("import.xml")) continue;
	if (name.endsWith(".csv")) continue;
	if (f.isDirectory()) continue;
	if (f.isHidden()) continue;
	if (name.endsWith(".doc") || name.endsWith(".docx") || name.endsWith(".odt") || name.endsWith(".rtf")) {
		srcfiles.add(f)
	}
}

if (srcfiles.size() == null) {
	println "No file to process"
	return;
}

// convert to TEI
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "CREATING TEI FILES")
println "-- CONVERTER - Converting source files"
File xsldir = new File(Toolbox.getTxmHomePath(), "xsl")
if (!DocumentToTei.processFiles(srcfiles, txmDir, xsldir)) {
	println "Abort import"
	return;
}

// move data folders and build css file
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "RETRIEVE STYLES")
println "Retrieving data folders and style files"
filesToProcess = FileUtils.listFiles(txmDir)
File docfiles = new File(binDir, "docfiles");
docfiles.mkdir()

for (File infile : filesToProcess) {
	if (infile.isDirectory()) {
		File unzipDir = new File(docfiles, infile.getName())
		infile.renameTo(unzipDir)
		
		//println "zipdir "+unzipDir
		StylesToCSS converter = new StylesToCSS(unzipDir);
		if (!converter.process(new File(unzipDir, "style.css"))) {
			println "WARNING: Failed to build css file of $unzipDir"
		}
		// and get the soft page breaks and styles parents
		def parentStyles = converter.parentStyles
		def beforebreaks = converter.beforebreaks
		def afterbreaks = converter.afterbreaks
		
		//println "BEFORES: "+beforebreaks
		//println "AFTERS: "+afterbreaks
		//println "PARENTS: "+parentStyles
		
		// se servir de ça pour insérer <pb/> et remplacer styles automatiques
		File xmlFile = new File(txmDir, unzipDir.getName().substring(6))
		//println "PATCH : $xmlFile"
		new FixBreaksAndAutomaticStyles().run(xmlFile, beforebreaks, afterbreaks, parentStyles)
	}
}

// set new Root
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FIXING TEI")
println "Setting new XML root element"
filesToProcess = FileUtils.listFiles(txmDir)
for (File infile : filesToProcess) {
	if (infile.isHidden() || infile.isDirectory() || infile.getName().startsWith(".")) continue;
	print "."
//	if (!SetNewXmlDocumentRoot.process(infile, "//text")) {
//		println "Failed to reset XML root $infile"
//		return
//	}
}
println ""

// filtering
println "Filtering XML files with xpaths: [//term]"
def xpaths = ["//term"]

if (xpaths != null) {
	println "Filtering XML files with xpaths: $xpaths"
	filesToProcess = FileUtils.listFiles(txmDir)
	for (File infile : filesToProcess) {
		if (infile.isHidden() || infile.isDirectory() || infile.getName().startsWith(".")) continue;
		print "."
		if (!RemoveTag.xpath(infile, xpaths)) {
			println "Failed to filter $infile"
			return
		}
	}
	println ""
}

//get metadata values from CSV
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "READ METADATA")
Metadatas metadatas; // text metadata
//println "Trying to read metadata from: "+allMetadataFile
if (allMetadataFile != null && allMetadataFile.exists()) {
	File copy = new File(binDir, allMetadataFile.getName())
	if (!FileCopy.copy(allMetadataFile, copy)) {
		println "Error: could not create a copy of the $allMetadataFile file "+allMetadataFile.getAbsoluteFile();
		return;
	}
	metadatas = new Metadatas(copy, Toolbox.getMetadataEncoding(), 
		Toolbox.getMetadataColumnSeparator(), 
		Toolbox.getMetadataTextSeparator(), 1)
} else {
	println "no $allMetadataFile metadata file found."
}

filesToProcess = FileUtils.listFiles(txmDir)
for (File infile : filesToProcess) {
	if (infile.isHidden()) infile.delete();
	if (infile.getName().startsWith(".")) infile.delete();
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(5, "IMPORTER")
println "-- IMPORTER - Reading source files"
def imp = new importer();
imp.doValidation(true) // change this to not validate xml
imp.doTokenize(true) // change this, to not tokenize xml
if (!imp.run(srcDir, binDir, txmDir, basename, "", lang, project)) {
	println "import process stopped";
	return;
}

if (metadatas != null) {
	if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
	if (MONITOR != null) MONITOR.worked(20, "INJECTING METADATA")
	println "-- INJECTING METADATA - from csv file: "+allMetadataFile
	
	println("Injecting metadata: "+metadatas.getHeadersList()+" in texts of directory "+new File(binDir,"txm"))
	filesToProcess = FileUtils.listFiles(txmDir)
	for (File infile : filesToProcess) {
		if (infile.isHidden() || infile.isDirectory() || infile.getName().startsWith(".")) continue;
		print "."

		File outfile = File.createTempFile("temp", ".xml", infile.getParentFile());
		if (!metadatas.injectMetadatasInXml(infile, outfile, "text", null)) {
			outfile.delete();
		} else {
			if (!(infile.delete() && outfile.renameTo(infile))) println "Warning can't rename file "+outfile+" to "+infile
			if (!infile.exists()) {
				println "Error: could not replace $infile by $outfile"
				return false;
			}
		}
	}
	println ""
}

filesToProcess = FileUtils.listFiles(txmDir)
if (filesToProcess == null || filesToProcess.length == 0) {
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "ANNOTATE")

boolean annotationSuccess = false;
if (annotate) {
	println "-- ANNOTATE - Running NLP tools"
	String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
	def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
	if (engine.processDirectory(txmDir, binDir, ["lang":model])) {
		annotationSuccess = true;
		if (project.getCleanAfterBuild()) {
			new File(binDir, "treetagger").deleteDir()
			new File(binDir, "ptreetagger").deleteDir()
			new File(binDir, "annotations").deleteDir()
		}
	}
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(25, "COMPILING")
println "-- COMPILING - Building Search Engine indexes"
def c = new compiler();
if(debug) c.setDebug();
//c.setCwbPath("~/TXM/cwb/bin");
c.setOptions(textSortAttribute, normalizeMetadata);
c.setAnnotationSuccess(annotationSuccess)
c.setLang(lang);
if (!c.run(project, binDir, txmDir, corpusname, null, srcfiles, metadatas)) {
	println "import process stopped";
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }

new File(binDir,"HTML/$corpusname").deleteDir();
new File(binDir,"HTML/$corpusname").mkdirs();
if (build_edition) {

	println "-- EDITION - Building edition"
	if (MONITOR != null) MONITOR.worked(25, "EDITION")

	File outdir = new File(binDir,"/HTML/$corpusname/default/");
	outdir.mkdirs();
	
	filesToProcess = FileUtils.listFiles(txmDir)
	def second = 0

	println "Paginating "+filesToProcess.length+" texts"
	ConsoleProgressBar cpb = new ConsoleProgressBar(filesToProcess.length);
	for (File txmFile : filesToProcess) {
		if (txmFile.isHidden() || txmFile.isDirectory() || txmFile.getName().startsWith(".")) continue;
		cpb.tick()

		String txtname = txmFile.getName();
		int i = txtname.lastIndexOf(".");
		if(i > 0) txtname = txtname.substring(0, i);

		List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);

		Text t = new Text(project);
		t.setName(txtname);
		t.setSourceFile(txmFile)
		t.setTXMFile(txmFile)
		
		def ed = new pager(txmFile, outdir, txtname, NoSpaceBefore, NoSpaceAfter, basename, project)
		Edition edition = new Edition(t);
		edition.setName("default");
		edition.setIndex(outdir.getAbsolutePath());
		for (i = 0 ; i < ed.getPageFiles().size();) {
			File f = ed.getPageFiles().get(i);
			String wordid = "w_0";
			if (i < ed.getIdx().size()) {
				wordid = ed.getIdx().get(i);
			}
			edition.addPage(""+(++i), wordid);
		}
	}
	cpb.done()

	for (File unzipDir : FileUtils.listFiles(docfiles)) {
		File css = new File(unzipDir, "style.css")
		String textname = unzipDir.getName();
		textname = textname.substring(6, textname.lastIndexOf(".")) // remove "files-" and ".xml"
		File newcss = new File(outdir, textname+".css")
		css.renameTo(newcss)
	}
	File doc = new File(Toolbox.getTxmHomePath(), "css/doc.css")
	doc_copy = new File(outdir, "doc.css")
	FileCopy.copy(doc, doc_copy);


	// copy images : images externaly linked must be added manually in the html/CORPUS/default directory
	println "Copying internal images..."
	for (File txmFile : filesToProcess) {
		if (txmFile.isHidden() || txmFile.isDirectory() || txmFile.getName().startsWith(".")) continue;
		print "."
		File picturesDirectory = new File(binDir, "docfiles/files-"+txmFile.getName()+"/Pictures")
		println picturesDirectory
		if (picturesDirectory.exists()) {
			File picturesDirectory_copy = new File(outdir, "Pictures")
			FileCopy.copyFiles(picturesDirectory, picturesDirectory_copy)
			if (!picturesDirectory_copy.exists()) {
				println "Failed to copy $picturesDirectory to $picturesDirectory_copy"
			}
		}
	}
	println ""
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FINALIZING")

readyToLoad = project.save();