package org.txm.scripts.importer.cqp

import org.txm.importer.cwb.BuildCwbEncodeArgs;
import java.io.File;
import java.util.ArrayList;

import org.txm.objects.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.utils.FileUtils
import org.txm.utils.i18n.*;
import org.txm.metadatas.*;
import org.txm.utils.io.FileCopy;
import org.w3c.dom.Element
import org.txm.utils.xml.DomUtils;

String userDir = System.getProperty("user.home");

def MONITOR;
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName();
String basename = corpusname
String rootDir = project.getSrcdir();
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL();
def xslParams = project.getXsltParameters();
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
String page_element = project.getEditionDefinition("default").getPageElement()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

// FAKE: will contains no XML-TXM files
File txmDir = new File(binDir, "txm/$corpusname");
txmDir.deleteDir();
txmDir.mkdirs();

// Will contains the edition files
File htmlDir = new File(binDir, "HTML/$corpusname");
htmlDir.deleteDir();
htmlDir.mkdirs();

// get the cqp file and the registry file
File cqpFile;
File registryFile;
println "Looking for files to process..."
for(File f : FileUtils.listFiles(srcDir)) {
	if (f.isHidden()) continue;
	if (f.isDirectory()) continue;
	if (f.getName().endsWith(".cqp")) {
		if (cqpFile == null) {
			cqpFile = f;
			println "Using CQP file : $cqpFile"
		}
	} else if (f.getName().endsWith(".xml")) {

	} else if (!f.getName().contains(".")){ // its the registry file
		if (registryFile == null) {
			registryFile = f;
			println "Using registry file : $f"
		}
	}
}

if (cqpFile == null) {
	println "No CQP file in source directory: "+srcDir;
	return false;
}

// get pAttributes an run cwb-encode + cwb-makeall
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(25, "COMPILING")
println "-- COMPILING - Building Search Engine indexes"
def c = new compiler(cqpFile, registryFile)
c.setDebug(debug)
if (!c.run(project, binDir, corpusname)) {
	println "Import stopped"
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }

new File(binDir,"HTML/$corpusname").deleteDir();
new File(binDir,"HTML/$corpusname").mkdirs();
if (build_edition) {
	
	println "-- EDITION - Building edition"
	if (MONITOR != null) MONITOR.worked(20, "EDITION - Building edition")

	File outdir = new File(binDir,"/HTML/$corpusname/default/");
	outdir.mkdirs();

	print "."

	List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
	List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);

	File binCqpFile = new File(binDir,"cqp/"+corpusname+".cqp");
	def ed = new pager(binCqpFile, outdir, NoSpaceBefore, NoSpaceAfter, wordsPerPage, basename, registryFile !=null, c.pAttributesList);
	def allIndexes = ed.getIdx();
	def allPages = ed.getPageFiles();
	for (String txtname : ed.getTextNames()) {
		def txtIndexes = allIndexes[txtname];
		def txtPages = allPages[txtname];

		Text t = new Text(project);
		t.setName(txtname);
		t.setSourceFile(cqpFile)
		t.setTXMFile(cqpFile)
		
		Edition edition = new Edition(t);
		edition.setName("default");
		edition.setIndex(outdir.getAbsolutePath());
		for (i = 0 ; i < txtPages.size();) {
			File f = txtPages.get(i);
			String wordid = txtIndexes.get(i);
			edition.addPage(""+(++i), wordid);
		}
	}
}
println ""

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FINALIZING")

readyToLoad = project.save();