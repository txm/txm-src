package org.txm.scripts.importer.xtz

import java.io.File;

import org.txm.*
import org.txm.importer.SAttributesListener
import org.txm.importer.cwb.*
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.DeleteDir
import org.txm.utils.logger.Log
import org.txm.core.preferences.TXMPreferences
import org.txm.core.preferences.TBXPreferences
import org.txm.libs.cqp.CQPLibPreferences

import javax.xml.stream.*
import org.txm.importer.xtz.*
import org.txm.objects.*
import org.txm.searchengine.cqp.corpus.*

class XTZCompiler extends Compiler {
	
	SAttributesListener sattrsListener; // store scanned structures
	
	String regPath;
	String corpusname;
	String wtag;
	
	boolean doNormalizeAttributeValues = false;
	boolean doNormalizeAnaValues = true;
	
	public XTZCompiler(ImportModule module) {
		super(module);
		
		corpusname = module.getProject().getName();
		regPath = module.getBinaryDirectory().getAbsolutePath() + "/registry/"+corpusname.toLowerCase()
		
		wtag = module.getProject().getTokenizerWordElement();
		
		doNormalizeAttributeValues = "true".equals(module.getProject().getPreferencesScope().getNode("import").get(TBXPreferences.NORMALISEATTRIBUTEVALUES, "false"))
		doNormalizeAnaValues = "true".equals(module.getProject().getPreferencesScope().getNode("import").get(TBXPreferences.NORMALISEANAVALUES, "false"))
	}
	
	/**
	 * the Text list
	 */
	def texts;
	/**
	 * the Text to process (dirty or newer than the cqp files) list
	 */
	def textsToProcess;
	def initialTypesValues;
	@Override
	public void _process() {
		
		if (orderedTextIDs == null) orderedTextIDs = module.getProject().getTextsID() ;
		
		Project project = module.getProject();
		
		texts = orderedTextIDs.collect() { id -> module.getProject().getText(id) }
		textsToProcess = getTextsToProcess(texts)
		
		// get all structures
		sattrsListener = new SAttributesListener() // will store the structure and properties declaration
		sattrsListener.W = wtag
		
		File registryFile = new File(regPath)
		
		initialTypesValues = new HashSet<String>()
		if (registryFile.exists()) {
			ReadRegistryFile rrf = new ReadRegistryFile(registryFile);
			rrf.read()
			initialTypesValues.addAll(rrf.getPAttributes())
			initialTypesValues.remove("id")
			initialTypesValues.remove("word")
		}
		
		CorpusBuild corpus = project.getCorpusBuild(project.getName(), MainCorpus.class);
		if (corpus != null) {
			if (project.getDoUpdate()) {
				corpus.clean(); // remove old files
			} else {
				corpus.delete(); // remove old files and TXMResult children
			}
		} else {
			corpus = new MainCorpus(project);
			corpus.setID(project.getName());
			corpus.setName(project.getName());
		}
		corpus.setDescription("Built with the XTZ import module");
		
		if (!doScanStep()) return;
		if (!doCQPStep()) return;
		if (!doCWBEncodeStep()) return;
		if (!doCWBMakeAllStep()) return;
		
		if (module.getProject().getCleanAfterBuild()
				&& !module.getProject().getDoUpdate()) { // for optimization purpose, don't clean the CQP files
			new File(module.getBinaryDirectory(), "cqp").deleteDir()
		}
		
		isSuccessFul = true;
	}
	
	/**
	 * Scan all XML-TXM files to find out structures and word properties
	 */
	public boolean doScanStep() {
		
		println "-- Scanning structures&properties to create for "+texts.size()+" texts..."
		
		
		
		//		def initialTypesValues = new HashSet<String>()
		//		initialTypesValues.addAll(sattrsListener.getAnatypes())
		
		// get all word properties
		ConsoleProgressBar cpb = new ConsoleProgressBar(texts.size())
		for (Text t : texts) {
			try {
				cpb.tick();
				sattrsListener.scanFile(t.getXMLTXMFile()); // results saved in 'listener' data
				//				println "LISTENER RESULT with ${xmlFile.getName()}: "+listener
				//				println " prof: "+listener.getStructs()
				//				println " prof: "+listener.getProfs()
				//				println " path: "+listener.structPath
				
				
				
			} catch (Exception e) {
				println "Error while processing $t text XML-TXM file : "+t.getSource()+". Error: "+e
				e.printStackTrace();
				return false;
			}
		}
		cpb.done();
		if (sattrsListener.getStructs().containsKey("lb") // structure detected
			&& sattrsListener.getStructs().get("lb").contains("n") // AND property detected
			&& !sattrsListener.getAnatypes().contains("lbn") // AND there is no lbn word property
			&& "true".equals(module.getProject().getTextualPlan("lbn")) ) { // AND option is activated
			sattrsListener.getAnatypes().add("lbn") // ACTIVATE lb_n projection in lbn
			println "Note: @n attributes detected in <lb> milestones. Their values will be added to the word properties."
		}
		
		if (initialTypesValues.size() == sattrsListener.getAnatypes().size()
				&& initialTypesValues.containsAll(sattrsListener.getAnatypes())) { // the word properties changed all CQP files must be recreated
			// no new property
		} else {
			if (module.isUpdatingCorpus()) {
				//println "All CQP files will be updated."
			}
			textsToProcess.clear()
			textsToProcess.addAll(texts)
		}
		
		return true;
	}
	
	def getTextsToProcess(def texts) {
		
		def textsToProcess = texts.findAll() { text ->
			File xmlFile = text.getXMLTXMFile()
			String textname = text.getName()
			
			File cqpFile = new File(cqpDirectory, textname + ".cqp")
			cqpFiles << cqpFile // insert cqp files to concat later
			// skip step if cqpFile exists AND is more recent than the XML-TXM File
			boolean mustBuild = false;
			if (!cqpFile.exists() || xmlFile.lastModified() >= cqpFile.lastModified()) {
				return true
			}
			
			if (!text.isDirty() && !mustBuild) {
				Log.finer("skipping .cqp step of $text");
				return false
			}
			
			return true
		}
		
		return textsToProcess
	}
	
	def cqpFiles = [] // ordered cqp files to concat before calling cwb-encode
	int cqpFilesUpdated = 0;
	LinkedHashMap<String, LinkedHashMap<String, String>> projectionsFromValues = new LinkedHashMap<String, LinkedHashMap<String, String>>(); // values of properties to inject
	LinkedHashMap<String, LinkedHashMap<String, ArrayList<ArrayList>>> projectionsToDo = new LinkedHashMap<String, LinkedHashMap<String, ArrayList<ArrayList>>>(); // list of projections to do
	public boolean doCQPStep() {
		
		cqpDirectory.mkdir(); // if not created
		
		println "-- Building CQP files ${textsToProcess.size()}/${texts.size()}..."
		
		// Building projections datas to use for each step
		String projectionsParameterValue = module.project.getTextualPlan("Projections").trim()
		projectionsParameterValue = projectionsParameterValue.replace("\n", "\t")
		def projectionsParameter = projectionsParameterValue.split("\t");
		if (projectionsParameterValue.length() > 0 && projectionsParameterValue.contains("->")) {
			for (def projection : projectionsParameter) {
				if (!projection.contains("->")) continue;
				
				String[] fromTo = projection.split("->", 2)
				String from = fromTo[0].trim()
				String to = fromTo[1].trim()
				if (projection.contains("->") && from.contains("_") && to.contains("_")) {
					String toStructure = to.substring(0, to.indexOf("_"))
					String toStructureProperty = to.substring(to.indexOf("_") + 1)
					String fromStructure = from.substring(0, from.indexOf("_"))
					String fromStructureProperty = from.substring(from.indexOf("_") + 1)
					
					if (!projectionsToDo.containsKey(toStructure)) {
						projectionsToDo[toStructure] = new LinkedHashMap<String, ArrayList<ArrayList>>();
					}
					if (!projectionsToDo[toStructure].containsKey(fromStructure)) {
						projectionsToDo[toStructure][fromStructure] = new ArrayList<ArrayList>();
					}
					projectionsToDo[toStructure][fromStructure].add([toStructureProperty, fromStructureProperty])
					
					if (!projectionsFromValues.containsKey(fromStructure)) projectionsFromValues[fromStructure] = new LinkedHashMap<String, String>();
					projectionsFromValues[fromStructure][fromStructureProperty] = "";
				}
			}
		}
		// registering the new structure properties
		for (String struct : projectionsToDo.keySet()) {
			for (String struct2 : projectionsToDo[struct].keySet()) {
				for (def couple : projectionsToDo[struct][struct2]) {
					sattrsListener.getStructs()[struct].add(couple[0])
				}
			}
		}
		
		ConsoleProgressBar cpb = new ConsoleProgressBar(textsToProcess.size())
		cqpFilesUpdated = 0;
		//for (Text text : textsToProcess) {
		textsToProcess.stream().forEach() { text -> // should be ok only file process is done here
			cpb.tick();
			
			File xmlFile = text.getXMLTXMFile()
			String textname = text.getName()
			
			File cqpFile = new File(cqpDirectory, textname + ".cqp")
			
			cqpFilesUpdated++
			
			XTZCompilerStep step = new XTZCompilerStep(this, xmlFile, cqpFile, textname, corpusname, "default", sattrsListener.getAnatypes(), wtag)
			step.setNormalizeAnaValues(doNormalizeAnaValues)
			step.setNormalizeAttributeValues(doNormalizeAttributeValues)
			if (!step.process()) {
				reason = "Fail to process $xmlFile."
				return false;
			}
		}
		println ""
		return true;
	}
	
	public boolean doCWBEncodeStep() {
		println "-- Running cwb-encode..."
		
		// clean directories
		DeleteDir.deleteDirectory(outputDirectory);
		outputDirectory.mkdirs();
		dataDirectory.mkdirs();
		
		DeleteDir.deleteDirectory(registryDirectory);
		registryDirectory.mkdirs();
		
		CwbEncode cwbEn = new CwbEncode()
		cwbEn.setDebug(debug)
		
		List<String> pargs = ["id"]
		for (String ana : sattrsListener.getAnatypes()) {
			if (ana == "word") continue; // no need to be added, cwb will declared it automatically
			if (ana == "id") continue; // no need to be added, we did it already
			if (ana == "join") ana = "joined"
			pargs.add(ana)
		}
		
		String[] pAttrs = pargs
		
		def structs = sattrsListener.getStructs()
		def structsProf = sattrsListener.getProfs()
		
		if (debug) {
			println structs
			println structsProf
		}
		
		List<String> sargs = new ArrayList<String>()
		def tmpTextAttrs = []
		for (String name : structs.keySet()) {
			if (name == "txmcorpus") continue;
			
			if (name == "text") {
				for (String value : structs.get(name)) // append the attributes
					tmpTextAttrs << value // added after
				continue;
			}
			
			String concat = name+":"+structsProf.get(name); // append the depth
			for (String attributeName : structs.get(name)) { // append the attributes
				concat += "+"+attributeName.toLowerCase();
			}
			
			if (structs.get(name).size() == 0) {
				concat += "+n";
			} else {
				if (!structs.get(name).contains("n")) {
					concat += "+n"
				}
			}
			
			if ((name == "p" || name == "body" || name == "back" || name == "front")
					&& !concat.contains("+n+") && !concat.endsWith("+n")) {
				concat += "+n"
			}
			sargs.add(concat)
		}
		
		String textSAttributes = "text:0+id+base+project";
		for (String name : tmpTextAttrs) {
			if (!("id".equals(name) || "base".equals(name) || "project".equals(name))) {
				textSAttributes += "+"+name.toLowerCase()
			}
		}
		
		sargs.add(textSAttributes)
		sargs.add("txmcorpus:0+lang")
		
		sargs.sort()
		
		String[] sAttributes = sargs
		String[] pAttributes = pAttrs
		println " Word properties: "+pAttributes.join(', ')
		println " Structures: "+sargs.join(', ')
		File allcqpFile = new File(cqpDirectory, "all.cqp");
		allcqpFile.delete()
		try {
			if (!CwbEncode.concat(cqpFiles, allcqpFile)) {
				println "Fail to write the master cqp file: "+allcqpFile
				return false;
			}
			
			new File(regPath).delete()// ensure the registry file is deleted
			
			if (!cwbEn.run(outputDirectory.getAbsolutePath() + "/$corpusname",
					allcqpFile.getAbsolutePath(), regPath, pAttributes, sAttributes, false)) {
				println "** cwb-encode did not ends well. Please activate a finer log level to see more details."
				return false;
			}
			
			allcqpFile.delete(); // clean
		} catch (Exception e) {
			println "Error while running cwb-encode: "+e
			e.printStackTrace()
			allcqpFile.delete(); // clean
			return false;
		}
		println ""
		return true;
	}
	
	public boolean doCWBMakeAllStep() {
		println "-- Running cwb-makeall..."
		try {
			CwbMakeAll cwbMa = new CwbMakeAll();
			cwbMa.setDebug(debug);
			
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			if (!cwbMa.run(corpusname, new File(regPath).getParent())) {
				println "** cwb-makeall did not ends well. Activate finer logs to see details."
				return false;
			}
			
			// remove milestones from CWB registry and data files
			FixMilestoneDeclarations fm = new FixMilestoneDeclarations(
					new File(regPath), new File(outputDirectory.getAbsolutePath(), corpusname));
			if (!fm.process()) {
				println "Fail to verify&fix milestone declarations"
				return false
			}
		} catch (Exception e) {
			println "Error while running cwb-makeall: "+e
			return false;
		}
		return true;
	}
}
