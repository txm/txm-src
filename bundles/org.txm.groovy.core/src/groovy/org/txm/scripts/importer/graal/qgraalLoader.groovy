// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (jeu. 29 août 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.scripts.importer.graal

//
//import org.txm.scripts.importer.graal.BuildGraalGSP;
//import org.txm.scripts.importer.graal.ImportInCWB;
//
//import org.txm.importer.cwb.CwbEncode
//import org.txm.importer.cwb.CwbMakeAll
//import org.txm.importer.cwb.PatchCwbRegistry;
//import org.txm.objects.*;
//import org.txm.utils.*
import org.txm.utils.io.*;
//String userDir = System.getProperty("user.home");
//
//String rootDir;
//try{rootDir = rootDirBinding;}//srcDir the selected directoy in TXM RCP via the Binding 
//catch(Exception)
//{	println "DEV MODE";//exception means we debug
//	if(!org.txm.Toolbox.isInitialized())
//	{
//		rootDir = userDir+"/xml/qgraal/";
//		org.txm.Toolbox.workspace = new Workspace(new File(userDir,"TXM/corpora/default.xml"));
//		org.txm.Toolbox.setParam(org.txm.Toolbox.INSTALL_DIR,new File(userDir,"TXM"));
//	}
//}
//
//String pathToCwbBinaries = org.txm.Toolbox.getParam(org.txm.Toolbox.INSTALL_DIR)+"/cwb/bin/";//chemin vers executable cqp de l'installation
//
//	//2- create GSP & import to CQP
//	println "EDITION"
//	def edition = new BuildGraalGSP(rootDir,//rootdir
//	pathToCwbBinaries// cwb utils dir
//	,"qgraal_cm_2009-07-d",	"qgraal_frmod_2009-07-w",//infile1 , infile2
//	"qgraalcm","qgraalfrmod"); //outfile1 & outfile2
//	edition.start();
//	
//	println "COMPILATION"
//	def compiler = new ImportInCWB(rootDir,//rootdir
//	pathToCwbBinaries// cwb utils dir
//	,"qgraal_cm_2009-07-d",	"qgraal_frmod_2009-07-w",//infile1 , infile2
//	"qgraalcm","qgraalfrmod"); //outfile1 & outfile2
//	compiler.start();
//
//	//THIS IS AN OLD SCRIPT WE HAVE TO MOVE SOMES FILES
//	new File(rootDir,"txm").deleteDir()
//	new File(rootDir,"txm").mkdir()
//	new File(rootDir,"HTML").deleteDir()
//	new File(rootDir,"HTML").mkdir()
//	//txm : copy src
//	FileCopy.copy(new File(rootDir+"/src","qgraal_cm_2009-07-d.xml"),new File(rootDir+"/txm","qgraal.xml"))
//	
//	//HTML : get from/cour
//	for(File f : new File(rootDir+"/imported/form/cour").listFiles())
//	{
//		String name = f.getName();
//		String pageid= name.substring(15,name.length()-4);
//		String newfilename = "qgraal_"+pageid+".html";
//		println newfilename
//		FileCopy.copy(f,new File(rootDir+"/HTML/multi",newfilename));
//	}
//	
//	//data
//	new File(rootDir,"data/qgraalcm").renameTo(new File(rootDir,"dataCM"));
//	new File(rootDir,"data").deleteDir();
//	new File(rootDir,"dataCM").renameTo(new File(rootDir,"data"));
//	
//	//registry
//	File registryfiletopatch = new File(rootDir+"/registry",qgraalcm);
//	File cwbindex = new File(rootDir,"data");
//	try {
//		PatchCwbRegistry.patch(registryfiletopatch, cwbindex);
//	} catch (IOException e) {org.txm.utils.logger.Log.printStackTrace(e);}
//	
//	// copy reg
//	try {
//		FileCopy.copy(registryfiletopatch, new File(registrypath,"qgraalcm"));
//	} catch (IOException e) {org.txm.utils.logger.Log.printStackTrace(e);}
//	
//	
//	Workspace w = org.txm.Toolbox.workspace;
//	Project p = w.getProject("default")
//	p.removeBase("qgraal")
//	Base b = p.addBase("qgraal");
//	b.addDirectory(new File(rootDir,"txm"));
//
//	