package org.txm.scripts.importer.transcriber
/**
 * Converts a file from Transcriber XML to STR subtitle file
 * 
 * @author mdecorde
 *
 */


import java.util.ArrayList

import javax.xml.parsers.*
import javax.xml.stream.*

import java.net.URL

class TRS2SRT {

	private def url
	private def inputData
	private def factory
	private XMLStreamReader parser
	def writer

	def colors = ["white", "red", "blue", "green", "yellow", "grey", "violet", "pink", "orange", "brown"];
	def icolor = 0;

	File trsFile;
	boolean debug = false

	public TRS2SRT(File trsFile) {
		inputData = trsFile.toURI().toURL().openStream()
		factory = XMLInputFactory.newInstance()
		parser = factory.createXMLStreamReader(inputData)

		this.trsFile = trsFile
	}

	/**
	 * Transform the trsFile to STR format
	 * 
	 * @param srtFile: result file
	 */
	public void process(File srtFile) {
		writer = srtFile.newWriter("UTF-8")
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					processStartElement()
					break;
				case XMLStreamConstants.END_ELEMENT:
					processEndElement()
					break;
				case XMLStreamConstants.CHARACTERS:
					processText()
					break;
			}
		}
		writer.close()
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
	}

	def speakers = [:]
	def currentSpeakers = []
	def currentSpeaker = "";
	def currentSpeach = ""
	def currentStartTime = ""
	def currentEndTime = ""
	def inTurn = false
	def nTurn = 1
	def syncCounter = 0;
	def whoCounter = 0;

	protected void processStartElement() {
		String localname = parser.getLocalName()
		switch (localname) {
			case "Speaker":
				def info = [:]
				speakers[parser.getAttributeValue(null, "id")] = info
				info["name"] = parser.getAttributeValue(null, "name")
				info["color"] = colors[icolor]
				info["count"] = 0;
				icolor++;
				break;
			case "Episode":
				if (debug) println "speakers $speakers"
				break;
			case "Turn":
				if (debug) println "Turn!"
				currentSpeakers = parser.getAttributeValue(null, "speaker").split(" ")
				currentSpeaker = currentSpeakers[0]
				currentSpeach = ""
				String s = parser.getAttributeValue(null, "startTime")
				String e = parser.getAttributeValue(null, "endTime")
				currentStartTime = Float.parseFloat(s)
				if (e != null) currentEndTime = Float.parseFloat(e)
				else currentEndTime = currentStartTime+3.0; // temporary fix
				inTurn = true
				syncCounter = 0;
				whoCounter = 0;
				break;
			case "Event":
			if (debug) println "Event!"
				currentSpeach += " ["+parser.getAttributeValue(null, "desc")+"] "
				break;
			case "Sync": // cut a Turn, the Turn@endTime must be replace with Sync@time
				if (debug) println "Sync!"
				syncCounter++; 
				if (syncCounter > 1) { // ignore first 'Sync', there is no speech to write
					def end = currentEndTime
					currentEndTime = Float.parseFloat(parser.getAttributeValue(null, "time"))
					writeSRTTurn() // write previous Turn, so currentEndTime is the Sync@time
					currentEndTime = end; // restore Turn@endTime
				}
				break;
			case "Who":
				if (debug) println "Who!"
				whoCounter++;
				if (whoCounter > 1) { // ignore first 'Who', there is no speach to write
					int n = Integer.parseInt(parser.getAttributeValue(null, "nb")) - 1
					if (currentSpeakers.size() <= n) {
						println "'Who@nb' Error at "+parser.getLocation()
						break;
					}
					
					writeSRTTurn() // write previous speach		
					// switch current speaker
					currentSpeaker = currentSpeakers[Integer.parseInt(parser.getAttributeValue(null, "nb")) - 1]
				}
				break;
		}
	}

	protected void processEndElement() {
		String localname = parser.getLocalName()
		switch (localname) {
			case "Speaker":
				break;
			case "Turn":
				inTurn = false
				writeSRTTurn()
				break;
			case "Sync":
				break;
			case "Who":
				break;
		}
	}

	protected writeSRTTurn() {
		currentSpeach = currentSpeach.trim()

		if (currentSpeach.length() == 0) return; // nothing to write

		//println "Writing Turn of '$currentSpeaker': "+speakers[currentSpeaker]
		def color = speakers[currentSpeaker]["color"]

		if (speakers[currentSpeaker]["count"] < 2) {
			speakers[currentSpeaker]["count"] = speakers[currentSpeaker]["count"] + 1;
			currentSpeach = speakers[currentSpeaker]["name"]+"\t"+currentSpeach
		}

		currentSpeach = "<font color=\"$color\">$currentSpeach</font>".replaceAll("\n", " ")

		def s = formatTime(currentStartTime)
		def e = formatTime(currentEndTime)
		currentStartTime = currentEndTime
		writer.println """
$nTurn
$s --> $e
$currentSpeach"""
		nTurn++
		currentSpeach = "" // reset speach
	}

	protected void processText() {
		String txt = parser.getText();
		if (inTurn) {
			currentSpeach += txt
		}
	}

	private String formatTime(float time) {
		String rez = " ";

		int ms = (time - (int)time) * 1000

		float h = time / 3600;
		time = time%3600;

		float min = (time%3600) / 60;
		int sec = (int)time%60

		if (min < 10)
			rez = ""+(int)h+":0"+(int)min;//+":"+time%60;
		else
			rez = ""+(int)h+":"+(int)min;//+":"+time%60;

		if (sec >= 10)
			rez += ":"+sec;
		else
			rez += ":0"+sec;

		rez += "."+ms
		return rez;
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			println "Usage:"
			println "java -jar TRS2SRT.jar file1 file2 file3"
			println "\nResult files are saved in the save directory as fileN files."
		}
	//	args = ["/home/mdecorde/CCC/RECETTE/TRS2SRT/victor/pazinterior_ok.trs"]
		for (String path : args) {
			println "Processing path=$path ..."
			
			File trsFile = new File(path)
			String name = trsFile.getName()
			name = name.substring(0, name.lastIndexOf("."))
			File srtFile = new File(trsFile.getParentFile(), name+".srt")
			TRS2SRT t = new TRS2SRT(trsFile)
			t.process(srtFile)
		}
	}
}
