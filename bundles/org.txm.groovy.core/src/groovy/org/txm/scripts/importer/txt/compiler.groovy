// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-04-11 15:30:35 +0200 (mar. 11 avril 2017) $
// $LastChangedRevision: 3426 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.txt

import org.txm.Toolbox;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.cwb.PatchCwbRegistry;
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.objects.*
import org.txm.utils.*
import org.txm.utils.treetagger.TreeTagger;
import org.txm.searchengine.cqp.corpus.*
import javax.xml.stream.*;

import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class compiler.
 */
class compiler {
	
	/** The debug. */
	boolean debug = false;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	/** The url. */
	private def url;
	
	/** The anahash. */
	private HashMap<String, String> anahash = new HashMap<String,String>() ;
	
	/** The text. */
	String text="";
	
	/** The base. */
	String base="";
	
	/** The lang. */
	private String lang ="fr";
	
	/**
	 * initialize.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 */
	
	public compiler(URL url,String text,String base, String projectName)
	{
		this.text = text
		this.base = base;
		this.url = url;
	}
	
	/**
	 * Sets the debug.
	 *
	 * @return the java.lang. object
	 */
	public setDebug()
	{
		debug =true;
	}
	
	/** The annotation success. */
	boolean annotationSuccess = false;
	
	/**
	 * Sets the annotation success.
	 *
	 * @param val the new annotation success
	 */
	public void setAnnotationSuccess(boolean val)
	{
		this.annotationSuccess = val;
	}
	
	/** The metadata s attributes. */
	String metadataSAttributes = "";
	
	/**
	 * Sets the metadata attributes.
	 *
	 * @param attrsdecl the new metadata attributes
	 */
	public void setMetadataAttributes(String attrsdecl)
	{
		metadataSAttributes = attrsdecl;
	}
	
	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}
	
	/**
	 * Creates the output.
	 *
	 * @param f the f
	 * @return true, if successful
	 */
	private boolean createOutput(File f) {
		try {
			output = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(f,f.exists())) , "UTF-8");
			return true;
		} catch (Exception e) { 
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(Project project) {
		
		File binDir = project.getProjectDirectory()
		String corpusname = project.getName();
		File txmDir = new File(binDir, "txm/"+corpusname)
		
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executable rights are not well set.")
			return false;
		}
		
		CorpusBuild corpus = project.getCorpusBuild(project.getName(), MainCorpus.class);
		if (corpus != null) {
			if (project.getDoUpdate()) {
				corpus.clean(); // remove old files
			} else {
				corpus.delete(); // remove old files and TXMResult children
			}
		} else {
			corpus = new MainCorpus(project);
			corpus.setID(project.getName());
			corpus.setName(project.getName());
		}
		corpus.setDescription("Built with the TXT+CSV import module");
		
		File cqpFile = new File(binDir,"cqp/"+corpusname+".cqp");
		cqpFile.delete()
		new File(binDir,"cqp").mkdirs()
		new File(binDir,"data").mkdirs()
		new File(binDir,"registry").mkdirs()
		
		String textid="";
		int counttext =0;
		
		//start corpus
		if (createOutput(cqpFile)) {
			output.write("<txmcorpus lang=\""+lang+"\">\n");
			output.close();
		}
		
		//1- Transform into CQP file
		XMLTXM2CQP cqpbuilder = null;
		cqpFile.delete()
		ArrayList<File> files = FileUtils.listFiles(txmDir);
		Collections.sort(files);
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
		for (File txmfile : files) {
			cpb.tick()
			cqpbuilder = new XMLTXM2CQP(txmfile.toURI().toURL());
			String txtname = txmfile.getName().substring(0,txmfile.getName().length()-4);
			cqpbuilder.setTextInfo(txtname, this.base, project.getName());

			cqpbuilder.setBalisesToKeep(["text","s"]);
			cqpbuilder.setSendToPAttributes(["lb":["n"]]);
			cqpbuilder.setLang(lang);
			if (!cqpbuilder.transformFile(cqpFile)) {
				println("Failed to compile "+txmfile)
			}
		}
		cpb.done()

		
		//end corpus
		if (createOutput(cqpFile)) {
			output.write("</txmcorpus>\n");
			output.close();
		}
		
		//2- Import into CWB
		def outDir = binDir.getAbsolutePath()+"/";
		
		CwbEncode cwbEn = new CwbEncode();
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbEn.setDebug(debug);
		cwbMa.setDebug(debug);
		
		if (cqpbuilder == null) {
			return false;
		}
		
		List<String> pAttributesList = cqpbuilder.getpAttributs(); // use last text s and p attributes
		List<String> sAttributesList = cqpbuilder.getsAttributs();
		println "pAttrs : "+pAttributesList
		println "sAttrs : "+sAttributesList
		String[] pAttributes = pAttributesList.toArray(new String[pAttributesList.size()])
		String[] sAttributes = sAttributesList.toArray(new String[sAttributesList.size()])
		
		try {
			String regPath = outDir + "/registry/"+corpusname.toLowerCase();
			cwbEn.run(outDir + "/data/${corpusname}", outDir + "/cqp/"+corpusname+".cqp", regPath, pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(corpusname, outDir + "/registry");
		} catch (Exception ex) {System.out.println("CWB error: "+ex); return false;}
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "cqp").deleteDir()
		}
		
		return true;
	}
}
