// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-11-08 13:38:06 +0100 (ven. 08 nov. 2013) $
// $LastChangedRevision: 2569 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.perrault

import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.scripts.xmltxm.BuildTTSrc;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;
import org.txm.Toolbox;

// TODO: Auto-generated Javadoc
/**
 * The Class annotate.
 */
class annotate {
	
	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 */
	public static void run(File rootDirFile)
	{
		String rootDir = rootDirFile.getAbsolutePath()+"/";

		//cleaning
		new File(rootDir,"annotations").deleteDir();
		new File(rootDir,"annotations").mkdir();
		new File(rootDir,"treetagger").deleteDir();
		new File(rootDir,"treetagger").mkdir();
		
		ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		
		List<File> files = new File(rootDir,"txm").listFiles()	
		//BUILD TT FILE READY TO BE TAGGED
		for(File f : files)
		{
			File srcfile = f;
			File resultfile = new File(rootDir+"treetagger/",f.getName()+".tt");
			new BuildTTSrc(srcfile.toURL()).process(resultfile)
		}
		
		//APPLY TREETAGGER
		files = new File(rootDir,"treetagger").listFiles()	
		for(File f : files)
		{
			File modelfile = new File(Toolbox.getPreference(Toolbox.TREETAGGER_MODELS_PATH),"/fr.par");
			if(!modelfile.exists())
			{
				println "Skipping ANNOTATE: Incorrect modelfile path: "+modelfile;
				return;
			}
			File infile = f
			File outfile = new File(f.getParent(),f.getName()+"-out.tt");
			println("3- APPLY TT on : "+infile+" with : "+modelfile +" >>  "+outfile);
			
			if(!new File(Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/").exists())
			{
				println("Path to TreeTagger is wrong "+Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/")
				return;
			}
			TreeTagger tt = new TreeTagger(Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/");
			tt.settoken();
			tt.setlemma();
			tt.setquiet();
			tt.setsgml();
			tt.setnounknown();
			tt.seteostag("<s>");
			tt.treetagger( modelfile.getAbsolutePath(), infile.getAbsolutePath(), outfile.getAbsolutePath())
			infile.delete();
		}
		
		//BUILD STAND-OFF FILES
		//contains txm:application/txm:commandLine
		File reportFile = new File(rootDir,"NLPToolsParameters.xml");
		
		String respPerson = System.getProperty("user.name");
		String respId = "txm";
		String respDesc = "NLP annotation tool";
		String respDate = "";
		String respWhen = ""
		
		String appIdent = "TreeTagger";
		String appVersion = "3.2";
		
		String distributor = "";
		String publiStmt = """""";
		String sourceStmt = """""";
		
		def types = ["pos","lemme"];
		def typesTITLE = ["",""];
		def typesDesc = ["",""];
		def typesTAGSET = ["",""];
		def typesWEB = ["",""];
		String idform ="w_c_";
		
		files = new File(rootDir,"treetagger").listFiles()	
		for(File f : files)
		{
			String target = f.getAbsolutePath();
			File ttfile = f
			File posfile = new File(rootDir+"annotations/",f.getName()+"-STOFF.xml");
			
			def encoding ="UTF-8";
			def transfo = new CSV2W_ANA();
			println("build w-interp "+ttfile.getName()+ ">>"+posfile.getName())
			transfo.setAnnotationTypes( types, typesDesc, typesTAGSET, typesWEB, idform);
			transfo.setResp(respId, respDesc,respDate, respPerson, respWhen);
			transfo.setApp(appIdent, appVersion);
			transfo.setTarget(target, reportFile);
			transfo.setInfos(distributor,  publiStmt, sourceStmt);
			transfo.process( ttfile, posfile, encoding );
		}
		
		files = new File(rootDir,"annotations").listFiles();
		List<File> txmfiles = new File(rootDir,"txm").listFiles();
		files.sort();
		txmfiles.sort();
		for(int i = 0 ; i< files.size();i++)
		{
			File srcfile = txmfiles.get(i);
			
			File pos1file = files.get(i);
			File temp = new File(rootDir,"temp"); 

			println("5- inject annotation in file : "+srcfile+" with : "+pos1file );
			
			def builder = new org.txm.scripts.teitxm.AnnotationInjection(srcfile.toURL(), pos1file.toURL(), milestones);
			builder.transfomFile(temp.getParent(),temp.getName());
			
			if (!(srcfile.delete() && temp.renameTo(srcfile))) println "Warning can't rename file "+temp+" to "+srcfile
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File rootDir = new File("~/xml/perrault/");
		new annotate().run(rootDir);
	}
}
