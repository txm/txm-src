// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-06-26 16:53:47 +0200 (lun. 26 juin 2017) $
// $LastChangedRevision: 3451 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.alceste;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.txm.scripts.importer.CleanFile;
import org.txm.utils.AsciiUtils;
import org.txm.utils.CharsetDetector;
import org.txm.utils.i18n.DetectBOM;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.stream.*;

import java.net.URL;


// TODO: Auto-generated Javadoc
/**
 * Create a file per text of an alceste file.
 */
class Alceste2Xml {

	/** The textno. */
	String textno = "0001";

	/** The textcount. */
	int textcount = 0;
	int nbOfStarNumbers;
	static def TEXTNUMBERREGEX = /^[0-9]{4,6} .+/
	static def TEXTSTARREGEX = /^[*]{4,6} .+/
	/**
	 * run the script.
	 *
	 * @param infile the infile
	 * @param outdir the outdir
	 * @param encoding the encoding
	 */
	public boolean run(File infile, File outdir, String encoding) {
		if (encoding == "??") {
			encoding = new CharsetDetector(infile).getEncoding();
			println "Guessing encoding of $infile : $encoding"
		}
		int noline = 0;
		int p_n = 1;
		int lb_n = 1;
		def duplicatedAttributes = new HashSet<String>();
		try {
			outdir.mkdir();
			boolean star = guessStar(infile, encoding);

			String textname = infile.getName().substring(0,infile.getName().length()-4);// creates file with prefix the name of the alceste file

			FileInputStream input = new FileInputStream(infile);
			Reader reader = new InputStreamReader(input , encoding);

			input.skip(new DetectBOM(infile).getBOMSize())
			
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			BufferedOutputStream output;
			XMLStreamWriter writer;

			String line = reader.readLine();
			while (line != null) {
				line = CleanFile.clean(line); // remove ctrl and surrogate chars
				
				if (((line ==~ TEXTNUMBERREGEX) || line ==~ TEXTSTARREGEX) && line.contains("*")) { // we found a text declaration with star attributes
					if (writer != null) { // end previous text if any
						writer.writeEndElement(); // p
						writer.writeEndElement(); // text
						writer.close();
						output.close()
					}
					
					lb_n = 1; // reset line counter
					p_n = 1; // reset paragraph counter
					int idx = line.indexOf(" "); // star metadata starts after the first ' '
					
					if (!star) {
						textno = line.substring(0, idx)
						textcount++;
					} else {
						this.buildNextId(); // update textno
					}

					String filename = textno;
					def attributes = new LinkedHashMap<String, String>();
					filename = filename.trim();
					output = new BufferedOutputStream(new FileOutputStream(new File(outdir, filename+".xml")))
					writer = factory.createXMLStreamWriter(output, "UTF-8"); //create a new file
					writer.writeStartDocument("UTF-8", "1.0")
					writer.writeStartElement("text");
					
					attributes["id"] = filename // writen at the end
					// get text metadata
					line = line.substring(idx + 1); // remove the text marker
					
					String[] splited = line.split("\\*");
					for (String s1 : splited) { // for each text star metadata
						s1 = s1.trim();
						if (s1.length() == 0) continue;

						String[] splited2 = s1.split("_", 2);
						if (splited2.length == 1) { // attribute without value
							
							if (s1.endsWith("0")) {
								String attrname = s1.substring(0, s1.length()-1); // remove "0"
								String attrnamebase = attrname = AsciiUtils.buildId(attrname);
								int c = 2;
								while (attributes.containsKey(attrname)) {
									attrname = attrnamebase+(c++)
								}
								if (c > 2) duplicatedAttributes.add(attrname);
								attributes.put(attrname, "N/A")
							} else {
								println "Error: attribute: "+s1+ " with line "+line
							}
						} else if (splited2.length == 2) {
							
							String attrname = splited2[0].toLowerCase()
							String attrnamebase = attrname = AsciiUtils.buildId(attrname);
								int c = 2;
								while (attributes.containsKey(attrname)) {
									attrname = attrnamebase+(c++)
								}
								if (c > 2) duplicatedAttributes.add(attrname);
							
							String attrvalue = splited2[1]
								
							attributes.put(attrname, attrvalue)
						} else {
						
							print("wrong size: name_value : "+splited2.length+" : ");
							for (String s : splited ) print s+""
							println ""
						}
					}
					
					for (def attname: attributes.keySet()) { // finally write the XML attributes
						writer.writeAttribute(attname, attributes[attname]);
					}
					writer.writeCharacters("\n");
					writer.writeStartElement("p"); // we create a tag <p>
					writer.writeAttribute("n", ""+(p_n++));// then we add attributes to the tag

				} else if (line.startsWith("-*")) {
					println "ignored locution : "+line;
				} else {
					if (writer != null) {
						if (line.length() > 0) {
							writer.writeEmptyElement("lb");
							writer.writeAttribute("n", ""+(lb_n++))
							writer.writeCharacters("\n");
						} else if (line.trim().length() == 0) {
							writer.writeEndElement(); // p
							writer.writeStartElement("p");
							writer.writeAttribute("n", ""+(p_n++))
							writer.writeCharacters("\n");
						}
						
						writer.writeCharacters(line);
					}
				}
				line = reader.readLine();
				noline++;
			}
			// the end
			if (writer != null) { // end last text
				writer.writeEndElement(); // p
				writer.writeEndElement(); // text

				reader.close();
				writer.close();
				output.close();
			}
		} catch(Exception e) {
			println "Error line : "+noline;
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		
		if (duplicatedAttributes.size() > 0){
			println "Some attributes were duplicated: "+duplicatedAttributes;
		}
		
		if (textcount == 0) {
			println "Error: no text found in "+infile;
			return false;
		}
		
		return true;
	}

	/**
	 * Builds the next id.
	 *
	 * @return the string
	 */
	private String buildNextId() {
		textcount++;
		textno = ""+textcount;
		while (textno.length() < nbOfStarNumbers) {
			textno="0"+textno;
		}
		return textno;
	}

	/**
	 * allow to guess if the file use **** or 000x format.
	 *
	 * @param infile the infile
	 * @param encoding the file encoding
	 * @return true, if the text marker is "****"
	 */
	private boolean guessStar(File infile, String encoding)
	{
		Reader input = new InputStreamReader(new FileInputStream(infile) , encoding);
		String line = input.readLine();
		while (line != null) {
			if (line ==~ /^[0-9]{4,6} .*/) { // 0000
				int idx = line.indexOf(" ");
				nbOfStarNumbers = idx;
				//println "NB NUMBER:" + nbOfStarNumbers
				input.close();
				return false;
			} else if (line ==~ /^[*]{4,6} .*/) { 
				input.close();
				int idx = line.indexOf(" ");
				nbOfStarNumbers = idx;
				//println "NB STAR:" + nbOfStarNumbers
				return true;
			}
			line = input.readLine();
		}
		input.close();
	}

	/**
	 * Main.
	 *
	 * @param args the args
	 */
	static main(args)
	{
		//File infile = new File(System.getProperty("user.home")+"/xml/nature/Nature.txt");
		File infile = new File(System.getProperty("user.home")+"/xml/TESTS2/alceste/Voeux.txt");
		//File infile = new File("~/xml/geo/src/rivières.txt");
		File outdir = new File(infile.getParentFile(), "out");
		outdir.mkdir()
		new Alceste2Xml().run(infile, outdir, "cp1252");
		println "done: $infile -> $outdir"
	}
}

