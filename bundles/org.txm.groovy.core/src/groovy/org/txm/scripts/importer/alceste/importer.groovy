// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-06-26 16:53:47 +0200 (lun. 26 juin 2017) $
// $LastChangedRevision: 3451 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.alceste


//import org.txm.scripts.filters.TabulatedToXml.*;
import org.txm.scripts.importer.*;
import org.txm.tokenizer.TokenizerClasses
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.FileUtils
import org.txm.utils.io.FileCopy;
import org.txm.importer.scripts.xmltxm.*;

import javax.xml.stream.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URL;

import org.txm.core.engines.*
import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.FusionHeader.*;
import org.txm.scripts.filters.TagSentences.*;
import org.txm.tokenizer.TokenizerClasses
import org.txm.Toolbox;

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {

	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param encoding the encoding
	 * @param basename the basename
	 * @return true, if successful
	 */
	public static boolean run(File rootDirFile, File binDir, File txmDir, String encoding, String basename, String lang, def project)
	{
		
		File srcfile = null;;
		
		for (File f : FileUtils.listFiles(rootDirFile, ".*\\.txt")) { // use the first txt file available
			srcfile = f
			break;
		}
		if (srcfile == null) {
			println "Error: no '*.txt' file to process in "+rootDirFile;
			return false;
		}
		
		File splitDir = new File(binDir, "split");
		File tokenizeDir = new File(binDir, "tokenized");
		File stokenizeDir = new File(binDir,"stokenized")
		stokenizeDir.deleteDir();
		stokenizeDir.mkdir();
		tokenizeDir.deleteDir();
		tokenizeDir.mkdir();
		splitDir.deleteDir();
		splitDir.mkdir();

		// Build xml files from the alceste file
		Alceste2Xml transformer = new Alceste2Xml()
		if (!transformer.run(srcfile, splitDir, encoding)) {
			return false;
		}

		// Tokenize xml files
		List<File> srcfiles = FileUtils.listFiles(splitDir);
		println (srcfiles.size()+ " texts found in "+srcfile)

		println("Tokenizing files ("+srcfiles.size()+")")
		ConsoleProgressBar cpb = new ConsoleProgressBar(srcfiles.size())
		for (File f : srcfiles) {
			cpb.tick()
			File resultfile = new File(tokenizeDir, f.getName());
			try {
				
				def tokenizer = new SimpleTokenizerXml(f, resultfile, TokenizerClasses.newTokenizerClasses(project.getPreferencesScope(), lang));
				if (project.getAnnotate()) { // an annotation will be done, does the annotation engine needs another tokenizer ?
					String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
					def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
					def stringTokenizer = engine.getStringTokenizer(lang)
					if (stringTokenizer != null) {
						tokenizer.setStringTokenizer(stringTokenizer)
					}
				}
				tokenizer.setStartTag("text")
				if (!tokenizer.process()) {
					println "Failed to tokenize: "+f;
					resultfile.delete();
				}
			}
			catch(Exception e){org.txm.utils.logger.Log.printStackTrace(e); println "Failed to tokenize: "+f; return false;}
		}
		cpb.done()
		
		List<File> stokenfiles = FileUtils.listFiles(tokenizeDir);	
		println("Tagging sentences of "+stokenfiles.size()+" files")
		cpb = new ConsoleProgressBar(stokenfiles.size())
		for (File f : stokenfiles) {
			cpb.tick()
			Sequence S = new Sequence();
			Filter F1 = new CutHeader();
			Filter F7 = new TagSentences();
			Filter F11 = new FusionHeader();
			S.add(F1);
			S.add(F7);
			S.add(F11);
			File infile = f;
			File xmlfile = new File(stokenizeDir, f.getName());
			File headerfile = new File(f.getParentFile(), f.getName()+"header.xml");

			S.SetInFileAndOutFile(infile.getPath(), xmlfile.getPath());
			S.setEncodages("UTF-8","UTF-8");
			Object[] arguments1 = [headerfile.getAbsolutePath()];
			F1.SetUsedParam(arguments1);
			Object[] arguments2 = [headerfile.getAbsolutePath(),F1];
			F11.SetUsedParam(arguments2);
			if (!S.proceed()) {
				println "Failed to tag file: "+f
			}
			S.clean();
			headerfile.delete();//remove the prepared file to clean
		}
		cpb.done()

		//TRANSFORM INTO XML-TEI-TXM
		List<File> tokenfiles = FileUtils.listFiles(stokenizeDir)
		println("Building xml-tei-txm ("+tokenfiles.size()+" files)")
		cpb = new ConsoleProgressBar(tokenfiles.size())
		for (File f : tokenfiles) {
			cpb.tick()
			File file = f;
			File txmfile = new File(txmDir, f.getName());

			def correspType = new HashMap<String, String>()
			def correspRef = new HashMap<String, String>()
			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>();
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
			//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();
			//lance le traitement
			def builder = new Xml2Ana(file);
			builder.setConvertAllAtrtibutes true;
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			if(!builder.process(txmfile))
			{
				println "Failed to build xml-txm of file: "+file;
				txmfile.delete();
			}
		}
		cpb.done()
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "tokenized").deleteDir()
			new File(binDir, "stokenized").deleteDir()
			new File(binDir, "src").deleteDir()
			new File(binDir, "split").deleteDir()
		}
		return true;
	}
}
