// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.tmx;

import org.txm.importer.cwb.BuildAlignOut;
import org.txm.importer.cwb.CwbAlign;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.cwb.PatchCwbRegistry;
import org.txm.importer.SAttributesListener
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.FileUtils;
import org.txm.utils.logger.Log;
import org.txm.utils.treetagger.TreeTagger;
import org.txm.objects.*;
import org.txm.searchengine.cqp.corpus.*;
import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The Class compiler builds the aligned CQP corpus indexes
 */
class compiler {
	/** The debug. */
	private boolean debug= false;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The dir. */
	private def dir;

	/** The output. */
	private def output;

	/** The url. */
	private def url;

	/** The text. */
	String text="";

	/** The base. */
	String base="";

	/** The project. */
	String projectName="";

	/** The anahash. */
	static boolean firstWord = true;
	private def anaTypes = [];
	public def getAnaTypes() { return anaTypes; }
	private HashMap<String,String> anahash = new HashMap<String,String>() ;

	private static SAttributesListener sattrsListener;
	private static HashMap<String,ArrayList<String>> structs;
	private static HashMap<String, Integer> structsProf;
	def headerAttrs; // only for TMX

	/** The tuprops. */
	List<String> tuprops;

	/** The corporanames. */
	List<String> corporanames = new ArrayList<String>();

	/** The align structure. */
	String alignStructure;

	/** The align attribute. */
	String alignAttribute;

	/** The do align. */
	boolean doAlign = false;
	public static HashMap<String, Integer> segs_id = [:]; // static so the id are uniques
	public static int seg_id = 0;

	/**
	 * initialize.
	 *
	 */
	public compiler(){}

	/**
	 * Instantiates a new compiler.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 * @param tuprops the tuprops
	 */
	public compiler(URL url, String text, String base, String project, List<String> tuprops)
	{
		this.text = text
		this.base = base;
		this.projectName = project;
		this.tuprops = tuprops;
		try {
			this.url = url;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);

			if (sattrsListener == null)
				sattrsListener = new SAttributesListener(parser);
			else
				sattrsListener.start(parser)
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.err.println("IOException while parsing ");
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			File f = new File(dirPathName, fileName)
			//We don't want to concatenate the cqp files
			output = new OutputStreamWriter(new FileOutputStream(f, f.exists()) , "UTF-8");
			return true;
		} catch (Exception e) {
			System.err.println(e);

			return false;
		}
	}

	/**
	 * Go to text.
	 */
	private void GoToText()
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			if(event == XMLStreamConstants.END_ELEMENT)
				if(parser.getLocalName().equals("teiHeader"))
					return;
		}
	}

	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(String dirPathName, String fileName)
	{
		createOutput(dirPathName, fileName);

		GoToText();
		firstWord = true;
		String headvalue = ""
		String vAna = "";
		String vForm = "";
		String wordid= "";
		String vHead = "";

		boolean captureword = false;
		boolean flagForm = false;
		boolean flagAna = false;

		String anaType;
		String tuRef;
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
			{
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						sattrsListener.startElement(parser.getLocalName());
						switch (parser.getLocalName()) {
							case "header": // capture header attributes
								headerAttrs = [:];
								for ( int i = 0 ; i < parser.getAttributeCount() ; i++) {
									headerAttrs[parser.getAttributeLocalName(i)] =  parser.getAttributeValue(i);
								}
								break;
							case "text":
								output.write("<text id=\""+text+"\" base=\""+base+"\"" +
								" project=\""+projectName+"\"");
								def textAttrs = [];
								for ( int i = 0 ; i < parser.getAttributeCount() ; i++)
								{
									String attrname = parser.getAttributeLocalName(i);
									String attrvalue = parser.getAttributeValue(i);
									if(attrname != "id")
										output.write(" "+attrname+"=\""+attrvalue+"\"")
									textAttrs << attrname
								}
								if (headerAttrs != null)
									for( String key : headerAttrs.keySet()) {
										if (!textAttrs.contains(key))
											output.write(" "+key+"=\""+headerAttrs[key]+"\"")
									}
								output.write(">\n");

								break;

							case "tu":
								output.write("<tu");
								for( String attrname : tuprops)
								{
									String attrvalue = parser.getAttributeValue(null, attrname);
									attrname = attrname.toLowerCase();
									if(attrvalue == null)
										output.write(" "+attrname+"=\"N/A\"")
									else
										output.write(" "+attrname+"=\""+attrvalue+"\"")
								}
								tuRef = text;
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									output.write(" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i)+"\"")
									if (parser.getAttributeLocalName(i) == "ref")
										tuRef = parser.getAttributeValue(i);
								}
								output.write(">\n");
								break;
							case "seg":
								output.write("<seg")
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									output.write(" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i)+"\"")
								}
								output.write(">\n")
								break;

							case "w":
								for(int i = 0 ; i < parser.getAttributeCount(); i++)
									if(parser.getAttributeLocalName(i).equals("id"))
								{
									wordid = parser.getAttributeValue(i);
								}
								break;
							case "form":
								flagForm = true;
								vForm = "";
								anahash.clear();
								break;

							case "ana":
								flagAna = true;
								anaType = parser.getAttributeValue(null, "type")
								if (anaType.length() > 0) anaType = anaType.substring(1);
							//println "anatype $anaType"
								anahash.put(anaType, "");
								if (firstWord) {
									anaTypes << anaType;
								}
								break;
						}
						break;

					case XMLStreamConstants.END_ELEMENT:
						sattrsListener.endElement(parser.getLocalName());
						switch (parser.getLocalName())
						{
							case "text":
								output.write("</text>\n");
								break;

							case "tu":
								output.write("</tu>\n")
								break;
							case "seg":
								output.write("</seg>\n")
								break;

							case "w":
								firstWord = false;
								output.write( vForm.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
										+"\t"+wordid+"\t"+tuRef);
								for(String type : anaTypes) {
									output.write("\t"+anahash.get(type));
								}
								output.write("\n")

								vForm = "";
								break;

							case "form":
								flagForm = false;
								break;

							case "ana":
								anahash.put(anaType, vAna);
								vAna = "";
								flagAna = false;
								break;
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						if(flagForm)
							vForm += parser.getText().trim();
						if (flagAna) {
							vAna += parser.getText().trim()
						}
						break;
				}
			}
			output.close();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
		}
		catch (Exception ex) {
			System.out.println("Exception while parsing " + inputData);
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}

		return true;
	}

	/**
	 * Sets the alignment.
	 *
	 * @param structure the structure
	 * @param attribute the attribute
	 */
	public void setAlignment(String structure, String attribute)
	{
		doAlign = true;
		this.alignStructure = structure;
		this.alignAttribute = attribute;
	}

	/**
	 * Gets the tu props.
	 *
	 * @param files the files
	 * @return the tu props
	 */
	public List<String> getTuProps(List<File> files)
	{
		Set<String> tuprops = new HashSet<String>();
		for (File f : files) {
			def inputData = f.toURI().toURL().openStream();
			def factory = XMLInputFactory.newInstance();
			def parser = factory.createXMLStreamReader(inputData);
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
			{
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						switch (parser.getLocalName()) {
							case "tu":
							for ( int i = 0 ; i < parser.getAttributeCount() ; i++) {
								tuprops.add(parser.getAttributeLocalName(i));
							}
							break;
						}
				}
			}
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
		}
		return new ArrayList<String>(tuprops);
	}

	public static HashMap<String, String> langs;
	public void setLangs(HashMap<String, String> langs)
	{
		this.langs = langs;
	}

	public static HashMap<Integer, ArrayList<String>> langGroups;
	public void setLangGroups(HashMap<Integer, ArrayList<String>> groups)
	{
		this.langGroups = groups;
	}

	public static HashMap<Integer, ArrayList<String>> corpusIDS;
	public void setCorpusIDS(HashMap<Integer, ArrayList<String>> corpusIDS)
	{
		this.corpusIDS = corpusIDS;
	}

	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param basename the basename
	 * @param textAttributes the text attributes
	 * @return true, if successful
	 */
	public boolean run(Project project, File binDir, File txmDir, String basename)
	{
		sattrsListener = null; // reset SAttribute Listener for each new import
		String rootDir = binDir.getAbsolutePath();
		seg_id=1;

		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}

		new File(binDir,"cqp").mkdirs()
		new File(binDir,"data").mkdirs()
		new File(binDir,"registry").mkdirs()

		String textid="";
		int counttext =0;
		List<File> files = FileUtils.listFiles(txmDir)

		//0- get all tu props
		//List<String> tuprops = getTuProps(files);

		if (corpusIDS == null || corpusIDS.size() == 0) {
			corpusIDS = [:];
			for (int group : langGroups.keySet()) {
				String lang = langs.get(langGroups.get(group)[0]);
				if (lang ==null) {
					println "ERROR: no lang defined for group $group . Aborting."
				}
				corpusIDS[group] = lang+group
			}
		}
		println "Using corpus ID: $corpusIDS"

		String cqpName;
		for (int group : langGroups.keySet()) {
			String lang = langs.get(langGroups.get(group)[0]);
			segs_id.put(group, 0);
			cqpName = basename+"_"+corpusIDS.get(group);
			createOutput(rootDir+"/cqp", "${cqpName}.cqp");
			output.write("<txmcorpus id=\"${cqpName}\" lang=\""+lang.toLowerCase()+"\">\n")
			output.close();

			//create txmDirs
			new File(rootDir, "txm/"+cqpName.toUpperCase()).mkdir();
		}

		//1- Transform into CQP file and build a corpus per file
		Collections.sort(files);
		def anaTypesPerCqp = [:];
		def builder = null;
		for (int group : langGroups.keySet()) {
			//String lang = langs.get(langGroups.get(group)[0]);
			cqpName = basename+"_"+corpusIDS.get(group);

			CorpusBuild corpus = project.getCorpusBuild(cqpName, MainCorpus.class);
			if (corpus != null) {
				if (project.getDoUpdate()) {
					corpus.clean(); // remove old files
				} else {
					corpus.delete(); // remove old files and TXMResult children
				}
			} else {
				corpus = new MainCorpus(project);
				corpus.setID(cqpName);
				corpus.setName(cqpName);
			}
			corpus.setDescription("Built with the TMX import module");

			File cqpFile = new File(binDir,"cqp/"+cqpName+".cqp");

			def filenames = langGroups.get(group);
			filenames.sort()
			//println("Process group no $group of files "+filenames)
			for (String filename : filenames) {
				File f = new File(txmDir, filename);
				print "."
				if (!f.exists()) {
					println("COMPILER: file "+f+ " does not exists -> stop")
					return false;
				}

				filename = filename.substring(0, filename.length()-4);
				counttext++;

				String txtname = FileUtils.stripExtension(f);
				seg_id = segs_id.get(group);
				builder = new compiler(f.toURI().toURL(), txtname, basename, "default", tuprops);
				builder.transfomFileCqp(rootDir+"/cqp",cqpName+".cqp");
				anaTypesPerCqp[cqpName] = builder.getAnaTypes();
				segs_id.put(group, seg_id);

				// move xml-txm file
				File txmCorpusDir = new File(rootDir, "txm/"+cqpName.toUpperCase())
				f.renameTo(new File(txmCorpusDir, f.getName()));
			}
		}
		println ""

		for (int group : langGroups.keySet()) {
			//String lang = langs.get(langGroups.get(group)[0]);
			createOutput(rootDir+"/cqp", basename+"_"+corpusIDS.get(group)+".cqp");
			output.write("</txmcorpus>")
			output.close();
		}

		if(builder == null) {
			System.out.println("No TXM files to process Stop import");
			return false;
		}
		//2- Import into CWB
		def outDir = rootDir;
		def outDirTxm = rootDir;

		def cqpFiles = [:]
		for (File cqpfile : FileUtils.listFiles(new File(rootDir, "cqp"))) {
			String corpusname = cqpfile.getName();

			corpusname = corpusname.substring(0, corpusname.length()-4);
			def corpusAnaTypes = anaTypesPerCqp[corpusname];

			CwbEncode cwbEn = new CwbEncode();
			cwbEn.setDebug(debug);
			CwbMakeAll cwbMa = new CwbMakeAll();
			cwbMa.setDebug(debug);

			def pAttrs = ["id", "ref"];
			for(String type : corpusAnaTypes) {
				int i = 2;
				while (pAttrs.contains(type))
					type = type+(i++);
				pAttrs.add(type);
			}

			structs = sattrsListener.getStructs();
			structsProf = sattrsListener.getProfs();
			// add structures+properties found in sources
			List<String> sargs = new ArrayList<String>();

			for (String name : structs.keySet()) {
				if (name == "header") continue;
				//if ( name == "text") continue; // added after
				//if ( name == "q") continue; // added after
				//if ( name == "foreign") continue; // added after
				String concat = name+":"+structsProf.get(name); // append the depth
				for (String value : structs.get(name)) // append the attributes
					concat += "+"+value;
				if ((name == "text") &&
				!(concat.endsWith("+id") || concat.contains("+id+")))
					concat += "+id"
				if ((name == "text") &&
				!(concat.endsWith("+base") || concat.contains("+base+")))
					concat += "+base"
				if ((name == "text") &&
				!(concat.endsWith("+project") || concat.contains("+project+")))
					concat += "+project"
				sargs.add(concat);
			}

			sargs.add("txmcorpus:0+id+lang")
			sargs.sort();

			String[] sAttributes = sargs;
			String[] pAttributes = pAttrs;
			println "P-attributes: "+pAttrs
			println "S-attributes: "+sargs

			try {
				cqpFiles[corpusname.toLowerCase()] = outDir + "/cqp/"+cqpfile.getName();
				String regPath = outDirTxm + "/registry/"+corpusname.toLowerCase()
				cwbEn.run(outDirTxm + "/data/"+corpusname.toUpperCase()+"/", outDir + "/cqp/"+cqpfile.getName(), regPath, pAttributes, sAttributes);
				if (!new File(regPath).exists()) {
					println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
					return false;
				}
				cwbMa.run(corpusname.toUpperCase(), outDirTxm + "/registry");
			} catch (Exception ex) {System.out.println(ex); return false;}
		}

		// 3- do alignement
		if (doAlign) {
			File registryDirectory = new File(rootDir,"registry");
			def registryFiles = FileUtils.listFiles(registryDirectory)
			for (File corpusName : registryFiles) {
				for (File targetName : registryFiles) {
					if (!corpusName.equals(targetName)) {
						try {
							PatchCwbRegistry.patchAlignment(new File(registryDirectory, corpusName.getName()), targetName.getName());

							File cqpFile1 = new File(cqpFiles[corpusName.getName()]);
							File cqpFile2 = new File(cqpFiles[targetName.getName()]);
							File alignOutFile = new File(outDir, "align.out");

							BuildAlignOut bao = new BuildAlignOut(cqpFile1, cqpFile2);
							if (!bao.process(alignOutFile, "seg", "id")) {
								println "Error while creating alignement file of $corpusName. Aborting."
								return false;
							}

							CwbAlign tt = new CwbAlign();
							// ./cwb-align-encode -D -r ~/TXM/corpora/tmxtest/registry/ -v out.align
							tt.setD();
							tt.setv();
							tt.setr(new File(rootDir,"registry").getAbsolutePath());
							tt.cwbalignencode(alignOutFile.getAbsolutePath());
						} catch (IOException e) {
							Log.severe("Error while calling TreeTagger: "+e.getLocalizedMessage());
							return false;
						}
					}
				}
			}
		}
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "cqp").deleteDir()
		}

		return true;
	}

	/**
	 * Gets the corpora names.
	 *
	 * @return the corpora names
	 */
	public List<String> getCorporaNames()
	{
		return corporanames;
	}

	/**
	 * Sets the debug.
	 */
	public void setDebug()
	{
		this.debug = true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/geo");
		def c = new compiler();
		c.setDebug();
		c.setCwbPath("~/TXM/cwb/bin");
		c.run(dir,"geo");
	}
}
