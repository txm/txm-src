package org.txm.scripts.importer.factiva

import org.txm.utils.FileUtils

/**
 * 
 * This is a Groovy port of the converter made by Pierre Ratinaud
 * #Author: Pierre Ratinaud
 * #Copyright (c) 2012 Pierre Ratinaud
 * #Lisense: GNU/GPL
 * 
 * @author mdecorde
 *
 */
class FactivaMail2Alceste {

	def txtdir;
	def fileout;
	def encodage_in = 'UTF-8'
	def encodage_out = 'UTF-8'

	def metadata = ["BY":"Author",
		"ART":"CaptionsDescriptionGraphics",
		"CLM":"Column",
		"CT":"Contact",
		"CY":"Copyright",
		"CX":"Correction",
		"CR":"Credit",
		"ED":"Edition",
		"HD":"Headline",
		"LA":"Language",
		"PG":"Page",
		"PD":"PublicationDate",
		"ET":"PublicationTime",
		"RBBCM":"ReutersCodes",
		"SE":"Section",
		"SC":"SourceCode",
		"GC":"SourceGroupCode",
		"NGC":"SourceGroupName",
		"SN":"SourceName",
		"TD":"TextFollowingLeadParagraphs",
		"VOL":"Volume",
		"WC":"WordCount"]

	def starts = ["LP":"LeadParagraph"]

	def ends = ["RF":"Reference",
		"AN":"AccessionNumber",
		"PUB":"PublisherName",
		"RE":"RegionCodeDescriptor",
		"IPC":"InformationProviderCodes",
		"IN":"IndustryCodeDescriptor",
		"CO":"CompanyCodeName",
		"IPD":"InformationProviderDescriptors",
		"NS":"SubjectCodeDescriptor",
		"DJIC":"DowJonesCodes",
		"DJID":"DowJonesDescriptors"]
	
	def ucis = []
	def writeTextOK = false
	def nextLineIsAnEndMetadataValue = false
	def endsKey

	FactivaMail2Alceste(File txtdir, File fileout, String encodage_in, String encodage_out) {
		this.txtdir = txtdir;
		this.fileout = fileout;
		this.encodage_in = encodage_in;
		this.encodage_out = encodage_out;
	}

	def toAttribute(String str) {
		return str.replaceAll(' ','')
//		.replaceAll('\'','').replaceAll(":", "").replaceAll("\\|", "").replaceAll("_","")
//			 .replaceAll('´','').replaceAll('’','').replaceAll('-','').toLowerCase().replaceAll("/", "").replaceAll("\\.", "")
//			 .replaceAll(" \"", "")
	}
	
	def writeText(String str) {
		if (writeTextOK) ucis[-1][1] += str+"\n";
	}
	
	/**
	 * parser de texte pour factiva
	 * @return liste d'uci (= list header + corp)
	 */
	def parsetxt(txt) {
		def lines = txt.split("\n") // met le texte dans une liste de lignes
		def keepline = false
		
		for (String line : lines)  { //pour chaque ligne du texte...
			if (line.length() == 0) continue;
			
			if (nextLineIsAnEndMetadataValue) {
				//println "DATA: "+line
				ucis[-1][0] += " *${endsKey}_"+toAttribute(line)
				nextLineIsAnEndMetadataValue = false;
			} else if (line.startsWith('---------------------------------------------------------------')) {// si la ligne commence avec...
				ucis << ['****',''] // nouveau texte
			} else {
				int idxSpace = line.indexOf(" ")
				if (0 <= idxSpace && idxSpace < 5) { // maybe a key
					String head = line.substring(0, idxSpace)
					String tail = line.substring(idxSpace+1)
					String tail2;
					int idxTab = tail.indexOf("\t");

					if (metadata.containsKey(head)) {
						if (idxTab == 0 && tail.length() > 0) {
							ucis[-1][0] += " *${head}_"+toAttribute(tail.substring(1));
						} else if (tail.length() > 0){
							writeText(line)
						}
					} else if (ends.containsKey(head)) {
						if (idxTab == 0) nextLineIsAnEndMetadataValue = true;
						endsKey = head;
						writeTextOK = false; // stop writing text
					} else if (starts.containsKey(head)) {
						writeTextOK = true; // start writing text
					} else {
						writeText(line)
					}
				} else {
					writeText(line)
				}
			}
		}
		return ucis
	}

	def doparse() {
		def files = FileUtils.listFiles(txtdir) // liste des fichiers dans txtdir
		fileout.withWriter("UTF-8") { writer ->
			for (File f : files) { // pour chaque fichier en entree...
				if (f.isHidden() || !f.canRead() || !f.isFile() || f.getName() == "import.xml") {
					continue;
				}
				println "parse file: $f"
				for (def uci : parsetxt(f.getText("UTF-8"))) { // on récupère les lignes
					writer.println uci[0] // header
					writer.println uci[1] // corp
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		File  txtdir = new File("/home/mdecorde/xml/factivatxt/") // repertoire des textes
		File fileout = new File("/home/mdecorde/xml/factivatxt/montagne-out.txt")
		String encodage_in = "UTF-8"
		String encodage_out = "UTF-8"
		def converter = new FactivaMail2Alceste(txtdir, fileout, encodage_in, encodage_out);

		println 'Done: '+converter.doparse()
	}
}
