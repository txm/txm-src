// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-12-01 17:48:31 +0100 (jeu. 01 déc. 2016) $
// $LastChangedRevision: 3351 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.tmx



//import org.txm.scripts.filters.TabulatedToXml.*;
import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.io.FileCopy;
//import org.txm.utils.treetagger.TreeTagger;
import javax.xml.stream.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URL;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.FusionHeader.*;
import org.txm.scripts.filters.TagSentences.*;
import org.txm.utils.FileUtils
import org.txm.objects.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.tokenizer.TokenizerClasses
// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {
	static Tmx2XmlFiles converter;
	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param base the base
	 */
	public static void run(File srcDir, File binDir, File txmDir, HashMap<String, String> langs, HashMap<String, String> originalTexts, def project)
	{
		String filename = binDir.getName();
		
		new File(binDir, "ptokenized").deleteDir();
		new File(binDir, "ptokenized").mkdir();
		new File(binDir, "tokenized").deleteDir();
		new File(binDir, "tokenized").mkdir();
		new File(binDir, "split").deleteDir();
		new File(binDir, "split").mkdir();

		//build xml files
		if (FileUtils.listFiles(srcDir).length == 0)
			return;
		converter = new Tmx2XmlFiles(langs, originalTexts) // fill langs and originalTexts dictionaries
		converter.run(srcDir, new File(binDir, "split"));
			
		List<File> srcfiles = FileUtils.listFiles(new File(binDir, "split"))
		println("Tokenizing "+srcfiles.size()+" files")
		for (File f : srcfiles) {
			
			String lang = langs.get(f.getName());
						
			print "."
			File outfile = new File(binDir,"tokenized/"+f.getName());
			SimpleTokenizerXml tokenizer = new SimpleTokenizerXml(f, outfile, TokenizerClasses.newTokenizerClasses(project.getPreferencesScope(), lang))
			tokenizer.setOutSideTextTagsAndKeepContent("note");
			if (!tokenizer.process()) {
				println("Failed to tokenize $f")
				outfile.delete()
			}
		}
		println ""
		
		// copy css file if any
		File cssSrc = new File(srcDir, "style.css");
		File cssBin = new File(binDir, "style.css");
		if (cssSrc.exists()) {
			FileCopy.copy(cssSrc, cssBin)
		}
		
		//TRANSFORM INTO XML-TEI-TXM
		List<File> tokenfiles = FileUtils.listFiles(new File(binDir,"tokenized"))
		println("Building xml-tei-txm ("+tokenfiles.size()+" files)")
		for (File f : tokenfiles) {
			print "."
			String txmfile = f.getName();
			
			def correspType = new HashMap<String, String>()
			def correspRef = new HashMap<String, String>()
			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String, HashMap<String,String>>();	
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
			//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String, String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String, HashMap<String, String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String, String[]>();
			//lance le traitement
			String wordprefix = "w_";
			def builder = new Xml2Ana(f);
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			//builder.setAddTEIHeader();
			if (!builder.process(new File(txmDir, txmfile))) {
				println("Failed to process file "+f)
				new File(srcDir, txmfile).delete()
			}
		}
		println ""
		
		if (project.getCleanAfterBuild()) {
			new File(project.getProjectDirectory(), "tokenized").deleteDir()
			new File(project.getProjectDirectory(), "ptokenized").deleteDir()
			new File(project.getProjectDirectory(), "stokenized").deleteDir()
			new File(project.getProjectDirectory(), "src").deleteDir()
			new File(project.getProjectDirectory(), "split").deleteDir()
		}
	}
	
	public def getCorpusIDS() {
		converter.getCorpusIDS()
	}
}
