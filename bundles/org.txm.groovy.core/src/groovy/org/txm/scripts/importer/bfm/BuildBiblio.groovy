// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.importer.bfm;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.txm.utils.*;

import javax.xml.stream.*;
import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * The Class BuildBiblio.
 */
class BuildBiblio {
	
	/**
	 * Process.
	 *
	 * @param csvfile the csvfile
	 * @param separator the separator
	 * @param encoding the encoding
	 * @param outputfile the outputfile
	 * @param buildIndex the build index
	 * @return true, if successful
	 */
	public static boolean process(File csvfile, String separator, String encoding, File outputfile, boolean buildIndex) {
		if(!(csvfile.exists() && csvfile.canRead()))
		{
			println "csvfile does not exist"
			return false;
		}
		
		if(!outputfile.getParentFile().exists())
		{
			println "outpurfile's parent does not exist"
			return false;
		}
		
		CsvReader csvreader = new CsvReader(csvfile.getAbsolutePath(), separator.charAt(0), Charset.forName(encoding))
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		FileOutputStream output = new FileOutputStream(outputfile)
		XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8");
		
		if(csvreader == null)
		{
			println "failed to build csvreader"
			return false;
		}
		
		writer.writeStartDocument("UTF-8","1.0");
		writer.writeStartElement("html");
		//header
		writer.writeStartElement("head");
		//writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
		writer.writeStartElement("meta");
		writer.writeAttribute("http-equiv","Content-Type");
		writer.writeAttribute("content","text/html");
		writer.writeAttribute("charset","UTF-8");
		writer.writeEndElement();//meta
		writer.writeStartElement("title");
		writer.writeCharacters("Fiches biblio BFM")
		writer.writeEndElement();//title
		writer.writeEndElement();//head
		
		//get titles
		csvreader.readHeaders();
		def titles = [];
		for(String title : csvreader.getHeaders())
		{
			titles << title
		}
		
		writer.writeStartElement("body");
		
		if(buildIndex)
		{
		writer.writeStartElement("ul");
		while (csvreader.readRecord()){
			
			writer.writeStartElement("li");
			writer.writeStartElement("a");
			writer.writeAttribute("href", "#"+csvreader.get("id"))
			writer.writeCharacters(csvreader.get("id"))
			writer.writeEndElement();//a
			writer.writeEndElement();//li			
		}
		writer.writeEndElement();//ul
		}
		
		println "Metadata declared: "+titles
		csvreader = new CsvReader(csvfile.getAbsolutePath(), separator.charAt(0), Charset.forName(encoding))
		csvreader.readHeaders();
		while (csvreader.readRecord()){
			writer.writeStartElement("div");
			writer.writeAttribute("class", "fiche");
			
			writer.writeStartElement("a")
			writer.writeAttribute("name", csvreader.get("id"));
			writer.writeEndElement();//a
			
			writer.writeStartElement("h1");
			String auteur = csvreader.get("Auteur")
			String titre = csvreader.get("Titre")
			if(auteur != null && auteur != "anonyme")
				writer.writeCharacters(auteur+", ")
			writer.writeCharacters(titre);
			writer.writeEndElement();//h1
			
			writer.writeStartElement("ul");
			for (int i = 0 ; i < titles.size() ; i++) {
				writer.writeStartElement("li");
				writer.writeStartElement("b");
				writer.writeCharacters(titles.get(i)+" : ")
				writer.writeEndElement();
				
				writer.writeCharacters(csvreader.get(titles.get(i)))
				writer.writeEndElement();//li
			}
			
			writer.writeEndElement();//ul
			writer.writeEndElement();//div
		}
		
		writer.writeEndElement();//body
		writer.close();
		output.close()
		return true;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File csvfile = new File("C:\\Documents and Settings\\alavrent\\Mes documents\\bfm - local\\Bases bibliographiques (copie)\\Fiches_biblio_TXM.txt")
		File outputfile = new File("C:\\Documents and Settings\\alavrent\\Mes documents\\bfm - local\\Bases bibliographiques (copie)\\Fiches_biblio_TXM.html")
		String separator = "\t"
		String encoding = "cp1252"
		
		if(!BuildBiblio.process(csvfile, separator, encoding, outputfile, false))
			println("Error.")
		else
			println "Success";
	}
}
