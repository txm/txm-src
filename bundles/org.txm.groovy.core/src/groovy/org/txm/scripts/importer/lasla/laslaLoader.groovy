/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.lasla;

import java.io.File;
import org.txm.scripts.importer.lasla.importer;
import org.txm.scripts.importer.xml.compiler;
import org.txm.scripts.importer.xml.pager_old;
import org.txm.objects.*;
import org.txm.utils.*
import org.txm.utils.io.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.utils.i18n.*;

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
String userDir = System.getProperty("user.home");
String rootDir;
String lang;
String encoding;
String model;
String basename;
try{rootDir = rootDirBinding;lang=langBinding;encoding=encodingBinding;model=modelBinding;basename=basenameBinding;}
catch(Exception)
{	println "DEV MODE";//exception means we debug
	if(!org.txm.Toolbox.isInitialized())
	{
		rootDir = userDir+"/xml/lasla/";
		basename = "latin";
		lang="fr";
		encoding= "ISO-8859-1";
		model="rgaqcj";//not used
		Toolbox.workspace = new Workspace(new File(userDir,"TXM/corpora/default.xml"));
		Toolbox.setParam(Toolbox.INSTALL_DIR,"/usr/lib/TXM");
		Toolbox.setParam(Toolbox.METADATA_ENCODING, "UTF-8");
		Toolbox.setParam(Toolbox.METADATA_COLSEPARATOR, ",");
		Toolbox.setParam(Toolbox.METADATA_TXTSEPARATOR, "\"");
		Toolbox.setParam(Toolbox.USER_TXM_HOME, new File(System.getProperty("user.home"),"TXM"));
	}
}

//String basename = new File(rootDir).getName().toLowerCase()
println "-- IMPORTER - Reading source files"
def imp = new importer();
if(!imp.run( new File(rootDir), encoding, basename))
{
	println "import process stopped";
	return;
}

File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+basename);
File txmfiles = new File(binDir,"txm");
files = txmfiles.listFiles()

println "-- COMPILING - Building Search Engine indexes"

def files = new File(binDir, "src").listFiles()
File onesrcfile = null;
if(files.size() > 0)
	onesrcfile = files[0];
if(onesrcfile == null)
{
	println "No XML file (extension '.xml') has been found in directory "+binDir
	return;
}

def c = new compiler();
c.setDebug();
//c.setCwbPath("~/TXM/cwb/bin");
c.setAnnotationSuccess(false)
c.setLang(lang);
if(!c.run(binDir, basename, null, files, null))
{
	println "import process stopped";
	return;
}

//move registry file to cwb registry dir
File registryfile = new File(binDir,"registry/"+basename);
if(registryfile.exists())
	FileCopy.copy(registryfile,new File(Toolbox.getTxmHomePath(), "registry/"+basename))

Workspace w = org.txm.Toolbox.workspace;
Project p = w.getProject("default")
p.removeBase(basename)
Base b = p.addBase(basename);
b.addDirectory(new File(binDir, "txm"));
b.setAttribute("lang", lang)
b.propagateAttribute("lang")

println "-- EDITION - Building edition"
new File(binDir,"HTML").deleteDir();
new File(binDir,"HTML").mkdir();
new File(binDir,"HTML/default").mkdir();
List<File> filelist = new File(binDir,"txm").listFiles();
def second = 0

println "Paginating text: "
for(String textname : b.getTextsID())
{
	Text text = b .getText(textname);
	File srcfile = text.getSource();
	File resultfile = new File(binDir, "HTML/"+srcfile.getName().substring(0,srcfile.getName().length()-4)+".html");
	List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
		
		if (second) { print(", ") }
		if (second > 0 && (second % 5) == 0) println ""
		print(srcfile.getName());
		second++
	
	def ed = new pager_old(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter,1000,basename, "br");
	
	Edition editionweb = text.addEdition("default","html",resultfile);
//	println("pages "+ed.getPageFiles())
//	println("idx "+ed.getIdx())
	for(int i = 0 ; i < ed.getPageFiles().size();i++)
	{
		File f = ed.getPageFiles().get(i);
		String idx = ed.getIdx().get(i);
		editionweb.addPage(f,idx);
	}
	
//	Edition editionbp = text.addEdition("onepage","html",resultfile);
//	editionbp.addPage(resultfile,ed.getIdx().get(0));
}


w.save()
println "done"

