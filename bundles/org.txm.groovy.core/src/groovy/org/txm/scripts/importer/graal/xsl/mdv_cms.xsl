<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">

	<!-- Eléments communs à toutes les présentations -->

	<xsl:output method="html" encoding="UTF-8" version="4.0"
		indent="yes" />

	<xsl:strip-space elements="*" />

	<xsl:variable name="text_id">
		<xsl:value-of
			select="//tei:teiHeader//tei:title[@type='reference']" />
	</xsl:variable>

	<xsl:template match="/">
		<html>
			<head>
			</head>
			<body>
				<xsl:apply-templates select="//tei:text" />
				<xsl:apply-templates select="//tei:note"
					mode="final" />
			</body>
		</html>
	</xsl:template>


	<xsl:template match="//tei:body/tei:div">
		<br />
		<span style="font-family:Times;color:indigo">
			[
			<i>
				<xsl:value-of select="@type" />
			</i>
			&#xa0;
			<xsl:value-of select="@n" />
			]
		</span>
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="//tei:body/tei:div/tei:div">
		<br />
		<span style="font-family:Times;color:indigo">
			[
			<i>
				<xsl:value-of select="@type" />
			</i>
			&#xa0;
			<xsl:value-of select="@n" />
			]
		</span>
		<br />
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="tei:head">
		<span style="font-family:Arial; color:darkred">
			<xsl:apply-templates />
		</span>
		<br />
	</xsl:template>

	<xsl:template match="//tei:p">
		<p>
			<xsl:if test="@n">
				[ §
				<xsl:value-of select="@n" />
				]&#xa0;
			</xsl:if>
			<xsl:apply-templates />
		</p>
	</xsl:template>

	<xsl:template match="tei:pb">
		<br />
		<br />
		<span style="font-family:Times" id="{$text_id}-{@n}">
			[folio
			<xsl:value-of select="@n" />
			]
			<a href="{$text_id}-compare.html#{$text_id}-{@n}">[autres facettes]</a>
			<xsl:if test="@facs">
				<xsl:text> </xsl:text>
				<a href="../{@facs}" target="_blank">[Image]</a>
			</xsl:if>
		</span>
		<xsl:if test="not(following-sibling::*[1][self::cb])">
			<br />
		</xsl:if>
	</xsl:template>

	<xsl:template match="//tei:cb">
		<br />
		<xsl:if test="not(preceding-sibling::*[1][self::pb])">
			<br />
		</xsl:if>
		<span style="font-family:Times">
			[colonne
			<xsl:value-of select="@n" />
			]
		</span>
		<br />
	</xsl:template>

	<xsl:template match="tei:note">
		<xsl:variable name="note_num">
			<xsl:choose>
				<xsl:when test="@n">
					<xsl:value-of select="@n" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="count(preceding::tei:note) + 1" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<a style="font-size:75%;color:blue;font-family:Times" title="{.}"
			id="noteref_{$note_num}" href="#note_{$note_num}">
			<sup>
				[
				<xsl:value-of select="count(preceding::tei:note) + 1" />
				]
			</sup>
		</a>
		<xsl:if test="following::*[1][self::tei:w]">
			<xsl:text> </xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template match="tei:note" mode="final">
		<xsl:variable name="note_num">
			<xsl:choose>
				<xsl:when test="@n">
					<xsl:value-of select="@n" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="position()" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<span id="note_{$note_num}">
			<a href="#noteref_{$note_num}">
				[
				<xsl:value-of select="$note_num" />
				]
			</a>
			(fol.
			<xsl:value-of select="preceding::tei:pb[1]/@n" />
			, col.
			<xsl:value-of select="preceding::tei:cb[1]/@n" />
			, l.
			<xsl:value-of select="preceding::tei:lb[1]/@n" />
			)
			<xsl:apply-templates />
		</span>
		<br />
	</xsl:template>

	<xsl:template match="tei:q">
		<span style="color:darkblue">
			<xsl:apply-templates />
		</span>
	</xsl:template>


	<xsl:template match="tei:gap">
		<xsl:text> [...] </xsl:text>
	</xsl:template>


	<!-- <xsl:template match="tei:q"> <span style="background-color:khaki"> 
		<xsl:apply-templates/> </span> </xsl:template> -->

	<xsl:template match="tei:gap">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="//tei:lb">
		<xsl:choose>
			<xsl:when test="position()=1 and not(ancestor::tei:seg)" />
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//tei:w">
		<span class="word" id="{@xml:id}" title="{@type}">
			<xsl:if
				test="starts-with(.,':') or starts-with(.,'!') or starts-with(.,'?') or starts-with(.,';')">
				&#xa0;
			</xsl:if>
			<xsl:value-of select="." />
			<xsl:choose>
				<xsl:when
					test="following::*[1][starts-with(., ',') or starts-with(., '.') or starts-with(., ']') or starts-with(., ')') or starts-with(.,':') or starts-with(.,'!') or starts-with(.,'?') or starts-with(.,';')]" />
				<xsl:when test=".[string-length(.) = 0]" />
				<xsl:when test="@bfm:aggl='elision'">
					'
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>

	<xsl:template match="tei:sic">
		<span style="text-decoration:line-through;color:red">
			<xsl:apply-templates mode="norm" />
		</span>
	</xsl:template>

	<xsl:template match="tei:del">
	</xsl:template>


	<xsl:template match="//tei:hi[@rend='sup']">
		<sup>
			<xsl:apply-templates />
		</sup>
	</xsl:template>

	<!-- fin éléments communs -->


</xsl:stylesheet>
