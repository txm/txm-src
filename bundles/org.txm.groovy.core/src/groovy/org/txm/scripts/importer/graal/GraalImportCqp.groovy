// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.graal

import javax.xml.stream.*;
import java.net.URL;
import java.lang.Boolean
import java.lang.System

// TODO: Auto-generated Javadoc
/**
 * The Class GraalImportCqp.
 *
 * @author ayepdieu
 * parse graal xml file to build the cqp the next step is to call importInCWB script
 */
public class GraalImportCqp{
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private def parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	
	/**
	 * initialize.
	 *
	 * @param url the url to the xmlfile to transform
	 */
	public GraalImportCqp(URL url){
		try {
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
			//System.out.println "ouverture du document en entree reussi";
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * create the output file writer.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			dir = new File(dirPathName)
			output = new OutputStreamWriter(new FileOutputStream(new File(dir, fileName)) , "UTF-8")
			//System.out.println "ouverture du document en sortie reussi";
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/**
	 * process !!!.
	 *
	 * @param dirPathName the output directory
	 * @param fileName the outfile name
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(String dirPathName, String fileName){
		if(findBody() && createOutput(dirPathName, fileName)){
			String idColumn;
			int idParagraph = 0;
			int idSentence = 0;
			int idLine = 0;
			int q_id =0;
			String idLinesuiv;
			String idWord;
			boolean flagVersion = false;
			boolean flagWord = false;
			boolean flagNorm = false;
			boolean flagDipl = false;
			boolean flagFacs = false;
			//boolean flagSupplied = false;
			int levelSupplied = 0;
			int levelq = 0;
			String typeWord = "";
			String vWordDipl = "";
			String vWordFacs = "";
			String vWordNorm = "";
			try {
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
					switch (event) {
						case XMLStreamConstants.START_ELEMENT:
							switch (parser.getLocalName()) {
								case "milestone":
									if (parser.getAttributeValue(0) == "column") {
										idColumn = parser.getAttributeValue(1);
										idColumn = idColumn.substring(4);
									}
								break;
								case "p":
									idParagraph = parser.getAttributeValue(0).toInteger();
									output.write "<p n=" + idParagraph +">\n";
									//if(idParagraph < 13 && idParagraph > 4){
										flagVersion = true;
									/*}else{
										flagVersion = false;
									}*/
								break;
								case "s":
//									String no = parser.getAttributeValue(0);
//									if(no == null)
//										idSentence = -1;
//									else
//										idSentence = no.toInteger();
								idSentence++;
									output.write "<s n=" + idSentence + " id=\"" + parser.getAttributeValue(1) + "\">\n";
								break;
								case "q":
									output.write "<q n=\""+q_id+"\">\n";
									q_id++;
									//new
									levelq = levelq + 1;
								break;
								
								case "supplied":
								levelSupplied++;
								break;								
								
								case "lb":
									idLinesuiv = parser.getAttributeValue(0);
									if(!("facs" == idLinesuiv)){
										idLine = idLinesuiv.toInteger();
									}
								break;
								case "w":
									typeWord = parser.getAttributeValue(0);
									if(flagVersion  && null != parser.getAttributeValue(2)){
										idWord = parser.getAttributeValue(2);
									}else{
										idWord = parser.getAttributeValue(1);
									}
									flagWord = true;
									flagNorm = true;
								break;
								case "norm":
									flagNorm = true;
								break;
								case "dipl":
									flagDipl = true;
								break;
								case "facs":
									flagFacs = true;
								break;
							}
						break;
						
						case XMLStreamConstants.END_ELEMENT:
							switch (parser.getLocalName()) {
								case "p":
									output.write "</p>\n";
								break;
								case "s":
									output.write "</s>\n";
								break;
								case "q":
									output.write "</q>\n";
									levelq = levelq - 1;
								break;
								case "supplied":
									levelSupplied = levelSupplied - 1;
								break;
								case "w":
									output.write vWordNorm + "\t" + typeWord + "\t" + levelq + "\t" + levelSupplied.toString().substring(0,1) + "\t" + idColumn + "\t" + idLine + "\t" + idWord + "\t" + vWordDipl + "\t" + vWordFacs + "\n";
									vWordNorm = "";
									vWordDipl = "";
									vWordFacs = "";
									flagNorm = false;
									flagDipl = false;
									flagFacs = false;
									flagWord = false; 
								break;
								case "norm":
									flagNorm = false;
								break;
								case "dipl":
									flagDipl = false;
								break;
								case "facs":
									flagFacs = false;
								break;
							}
						break;
						
						case XMLStreamConstants.CHARACTERS:
							if(flagWord){
								vWordNorm.trim();
								if(flagNorm){
									vWordNorm += parser.getText().trim();
								}else if(flagDipl){
									vWordDipl += parser.getText().trim();
								}else if(flagFacs){
									vWordFacs += parser.getText().trim();
								}
							}
						break;
					}
				}
				output.close();
				parser.close();
			}
			catch (Exception ex) {
				System.out.println("Xml Location: "+parser.getLocation());
				System.out.println(ex);
			}
			
		}
	}
	
	/**
	 * a simplier version of transform file to process a non-facette xml.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFileCqpBrut(String dirPathName, String fileName){
		if(findBody() && createOutput(dirPathName, fileName)){
			String idColumn = "";
			int idParagraph = 0;
			int idSentence = 0;
			String idWord;
			boolean flagWord = false;
			boolean flagCont = true;
			boolean flagStart = true;
			String typeWord = "";
			String vWord = "";
			try {
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT && flagCont; event = parser.next()) {
					switch (event) {
						case XMLStreamConstants.START_ELEMENT:
							//System.out.println parser.getLocalName();
							switch (parser.getLocalName()) {
								case "milestone":
									if (parser.getAttributeValue(0) == "p") {
										idParagraph = parser.getAttributeValue(1).toInteger();
										if(flagStart){
											output.write "<p n=" + idParagraph + ">\n";
										}else{
											output.write "</p>\n<p n=" + idParagraph + ">\n";
										}
									}else if(parser.getAttributeValue(0) == "column"){
										idColumn = parser.getAttributeValue(1);
										idColumn = idColumn.substring(4);
									}else if (parser.getAttributeValue(0) != "s"){
										idColumn = "";
									}
								break;
								case "s":
									idSentence = parser.getAttributeValue(0).toInteger();
									output.write "<s n=" + idSentence + ">\n";
								break;
								case "w":
									if(null != parser.getAttributeValue(1)){
										typeWord = parser.getAttributeValue(0);
										idWord = parser.getAttributeValue(1);
									}else{
										idWord = parser.getAttributeValue(0);
										typeWord = "";
									}
									flagWord = true;
								break;
								case "div":
									if (parser.getAttributeValue(0) == "note") {
										output.write "</s>\n</p>";
										flagCont = false;
									}
								break;
							}
						break;
						
						case XMLStreamConstants.END_ELEMENT:
							switch (parser.getLocalName()) {
								case "w":
									output.write vWord + "\t" + typeWord + "\t" + idWord + "\t" + idColumn + "\n";
									flagWord = false; 
								break;
							}
						break;
						case "s":
							output.write "</s>\n";
						break;						
						case XMLStreamConstants.CHARACTERS:
							if(flagWord){
								vWord = parser.getText().trim();
							}
						break;
					}
				}
				output.close();
				parser.close();
			}
			catch (XMLStreamException ex) {
				System.out.println(ex);
			}
			catch (IOException ex) {
				System.out.println("IOException while parsing " + input);
			}
		}
	}
	
	/**
	 * test if the current element if the body.
	 *
	 * @param name the name
	 * @return true, if is body
	 */
	private static boolean isBody(String name) {
		if (name.equals("body")) return true;
	}
	
	/**
	 * bring the parser to the body element.
	 *
	 * @return true, if successful
	 */
	private boolean findBody(){
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()){
			if(event == XMLStreamConstants.START_ELEMENT){
				if (isBody(parser.getLocalName())){
					return true;
				}
			}
		}
		return false;
	} 
	
	/**
	 * no args needed.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
	 	def inDir = "/home/txm/src";
	 	def outDir = "/home/txm/";
	 	def inFile = "qgraal_cm_2009-07-d.xml";
		System.out.println "file://"+ inDir + "/" + inFile;

		if (args.length == 0) {
			GraalImportCqp traitTxt = new GraalImportCqp(new URL("file://"+ inDir + "/" + inFile));
			traitTxt.transfomFileCqp("/home/ayepdieu/srcQuete/result", "graal.cqp");
		}else if(args.length == 3){
			GraalImportCqp traitTxt = new GraalImportCqp(new URL(args[0]));
			traitTxt.transfomFileCqp(args[1], args[2]);
		}else{
			System.err.println("Usage: java XHTMLOutliner url" );
			return;
		}
		return
	}
	
}
