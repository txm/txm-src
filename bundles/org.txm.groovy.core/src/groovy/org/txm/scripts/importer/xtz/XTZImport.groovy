package org.txm.scripts.importer.xtz;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.utils.xml.DomUtils;
import org.txm.core.preferences.TBXPreferences;
import org.txm.metadatas.Metadatas
import org.txm.utils.io.FileCopy;
import org.txm.*
import org.txm.objects.*
import org.w3c.dom.Element
import org.txm.importer.xtz.*

public class XTZImport extends ImportModule {

	public XTZImport(File importParametersFile) {
		super(importParametersFile);
	}

	public XTZImport(Project p) {
		super(p);
	}

	@Override
	public void init(Project p) {
		super.init(p);
		
		importer = new XTZImporter(this)
		compiler = new XTZCompiler(this)
		annotater = new TTAnnotater(this);
		pager = new XTZPager(this)
	}
	
	@Override
	protected ArrayList<String> getTXMFilesOrder() {
		//System.out.println("XTZ FILES ORDER");
		if (importer == null) {
			//println "no importer step, using default text order"
			return super.getTXMFilesOrder();
		}
		Metadatas metadata = importer.getMetadata();
		if (metadata == null) { // if metadata was not built, try building it
			File allMetadataFile = Metadatas.findMetadataFile(binaryDirectory);
			if (allMetadataFile != null && allMetadataFile.exists()) {
				metadata = new Metadatas(allMetadataFile,
						Toolbox.getPreference(TBXPreferences.METADATA_ENCODING),
						Toolbox.getPreference(TBXPreferences.METADATA_COLSEPARATOR),
						Toolbox.getPreference(TBXPreferences.METADATA_TXTSEPARATOR), 1)
			}
		}
		
		if (metadata == null) {
			//println "no metadata, using default text order"
			return super.getTXMFilesOrder();
		}
		
		File txmDirectory = new File(binaryDirectory, "txm/"+corpusName);
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(txmDirectory.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isFile() && !file.isHidden() && file.getName().endsWith(".xml");
			}
		})));
	
		// fill sort data map
		final HashMap<String, String> textorder = new HashMap<String, String>();
		for (String t : metadata.keySet()) {
			def ti = metadata.get(t)
			for (org.txm.metadatas.Entry e : ti) {
				if ("text-order".equals(e.getId()) || "textorder".equals(e.getId())) {
					textorder[t] = e.getValue()
				}
			}
		}
		
		// TODO Tri des textes par l'ordre alphanumérique de la métadonnée  « text-order »
		// TODO Tri des textes par ordre alphanumérique des noms de fichiers
		if (textorder.size() > 0) {
			println "Sorting texts by alphanumeric order of the metadata \"text-order\": "+textorder
		} else {
			println "Sorting texts by alphanumeric order of file names."
		}
		def texts = project.getTextsID();
		Collections.sort(texts, new Comparator<String>() {
			public int compare(String f1, String f2) {
				String o1 = textorder[f1];
				String o2 = textorder[f2];
				if (o1 == null && o2 == null) {
					return f1.compareTo(f2);
				} else if (o1 == null) {
					return 1
				} else if (o2 == null) {
					return -1
				} else {
					int c = o1.compareTo(o2);
					if (c == 0) return f1.compareTo(f2);
					else return c;
				}
			}
		});
		//println files
		return texts;
	}
	
	public void start() throws InterruptedException {
		super.start();

		if (isSuccessful) {
			
			//declare a local KR
			//TODO find out how the annotation plugin may hook the import steps
//			List<String> krnames = importParameters.getKnowledgeRepositoryNames();
//			if (krnames.size() == 0) {
//				importParameters.createKnowledgeRepositoryElement("DEFAULT"); // set a default KR shared by all XTZ corpus
//			} else if (krnames.size() == 1 && krnames.get(0).equals("DEFAULT")) {
//				// nothing to do
//			} else {
//				println("Corpus is using custom Knowledge repositories: "+importParameters.getKnowledgeRepositoryNames());
//			}
			
			//copy sub directories
			if (isUpdatingCorpus()) {

			} else {
				def dirToCopy = ["xsl", "css", "dtd", "doc"]
				println "--- Copying subdirectories $dirToCopy"
				for (String dir : dirToCopy) {
					File origDirectory = new File(this.sourceDirectory, dir)
					if (origDirectory.exists()) {
						print "."
						File copyDirectory = new File(this.binaryDirectory, dir)
						copyDirectory.deleteDir(); // clean before copying
						FileCopy.copyFiles(origDirectory, copyDirectory)
					}
				}
				println ""
			}
		}
	}
}
