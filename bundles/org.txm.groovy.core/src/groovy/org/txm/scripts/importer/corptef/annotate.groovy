// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-11-08 13:38:06 +0100 (ven. 08 nov. 2013) $
// $LastChangedRevision: 2569 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.corptef;

import org.txm.importer.cwb.*
import org.txm.importer.scripts.xmltxm.*;
import org.txm.scripts.importer.*;
import org.txm.utils.treetagger.TreeTagger;
import org.txm.Toolbox;

// TODO: Auto-generated Javadoc
/**
 * The Class annotate.
 */
class annotate {
	
	/**
	 * Run.
	 *
	 * @param dir the dir
	 */
	public void run(File dir)
	{
		String rootDir = dir.getAbsolutePath()+"/";//"~/xml/discours/src";
		String txmDir = dir.getAbsolutePath()+"/txm/"

		//cleaning
		new File(rootDir,"annotations").deleteDir();
		new File(rootDir,"annotations").mkdir();
		new File(rootDir,"treetagger").deleteDir();
		new File(rootDir,"treetagger").mkdir();
		println txmDir
		List<File> listfiles = new File(txmDir).listFiles();
		for(File teifile : listfiles)
		{
			println("annotate "+teifile)
			File modelfile = new File(Toolbox.getPreference(Toolbox.TREETAGGER_MODELS_PATH),"/rgaqcj.par");
			if(!modelfile.exists())
			{
				println "Skipping ANNOTATE: Incorrect modelfile path: "+modelfile;
				return;
			}
			File annotfile = new File(rootDir+"annotations",teifile.getName()+"-STDOFF.xml");
			File ttsrcfile = new File(rootDir+"treetagger",teifile.getName()+"-src.tt");
			File ttrezfile = new File(rootDir+"treetagger",teifile.getName()+"-out.tt");
			
			//prepare file to be tagged
			def builder = new BuildTTSrc(teifile.toURL());
			builder.process(ttsrcfile);
			
			//Apply TT
			if(!new File(Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/").exists() || Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH).length() == 0 )
			{
				println("Path to TreeTagger is wrong: "+Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/")
				return;
			}
			TreeTagger tt = new TreeTagger(Toolbox.getPreference(Toolbox.TREETAGGER_INSTALL_PATH)+"/bin/");
			tt.settoken();
			tt.setlemma();
			tt.setquiet();
			tt.setnounknown();
			tt.setsgml();
			tt.seteostag("<s>");
			tt.treetagger( modelfile.getAbsolutePath(), ttsrcfile.getAbsolutePath(), ttrezfile.getAbsolutePath())
			
			//create stand-off annotation file
			//targeted file for annotations
			String target = teifile.getName()
			
			
			//contains txm:application/txm:commandLine
			File reportFile = new File(rootDir,"NLPToolsParameters.xml");
			
			String respPerson = System.getProperty("user.name");
			String respId = "txm";
			String respDesc = "NLP annotation tool";
			String respDate = "Tue Mar  11 1:02:55 Paris, Madrid 2010";
			String respWhen = ""
			
			String appIdent = "TreeTagger";
			String appVersion = "3.2";
			
			String distributor = "";
			String publiStmt = """""";
			String sourceStmt = """""";
			
			def types = ["ttpos","ttlemma"];
			def typesTITLE = ["ttpos","ttlemma"];
			def typesDesc = ["fr pos","fr lemma"]
			def typesTAGSET = ["",""]
			def typesWEB = ["",""]
			String idform ="w_";
			String encoding ="UTF-8";
			
			def transfo = new CSV2W_ANA();
			transfo.setAnnotationTypes( types, typesDesc, typesTAGSET, typesWEB, idform);
			transfo.setResp(respId, respDesc,respDate, respPerson, respWhen);
			transfo.setApp(appIdent, appVersion);
			transfo.setTarget(target, reportFile);
			transfo.setInfos(distributor,  publiStmt, sourceStmt);
			transfo.process( ttrezfile, annotfile, encoding );
			
			//merge into the tei file
			builder = new AnnotationInjection(teifile.toURL(), annotfile.toURL(), new ArrayList<String>());
			builder.transfomFile(rootDir,"temp");
			
			if (!(teifile.delete() && new File(rootDir,"temp").renameTo(teifile))) println "Warning can't rename file "+new File(rootDir,"temp")+" to "+teifile
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("C:/Documents and Settings/alavrent/TXM/corpora/corptef/")
		new annotate().run(dir);
	}
}
