// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2012-06-01 17:47:31 +0200 (ven., 01 juin 2012) $
// $LastChangedRevision: 2185 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.tmx;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

// TODO: Auto-generated Javadoc
/** Build GEO corpus simple edition from a xml-tei. @author mdecorde */
class pager {
	List<String> NoSpaceBefore;

	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;

	/** The pagecount. */
	int pagecount = 0;

	/** The wordmax. */
	int wordmax = 0;

	/** The basename. */
	String basename = "";
	String txtname = "";
	File outdir;

	/** The wordid. */
	String wordid;

	/** The first word. */
	boolean firstWord = true;

	/** The wordvalue. */
	String wordvalue;

	/** The interpvalue. */
	String interpvalue;

	/** The lastword. */
	String lastword = " ";

	/** The wordtype. */
	String wordtype;

	/** The flagform. */
	boolean flagform = false;

	/** The flaginterp. */
	boolean flaginterp = false;

	/** The url. */
	private def url;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The writer. */
	OutputStreamWriter writer;

	/** The multiwriter. */
	OutputStreamWriter multiwriter = null;

	/** The infile. */
	File infile;

	/** The outfile. */
	File outfile;

	/** The pages. */
	ArrayList<File> pages = new ArrayList<File>();

	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>();
	String editionPage;
	ArrayList<Integer> splitTUs; // contains the tu ids used to split pages
	boolean shouldSplit = false;
	boolean useSplitTUs = false;
	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 * @param basename the basename
	 */
	pager(File infile, File outdir, String txtname, List<String> NoSpaceBefore,
	List<String> NoSpaceAfter, int max, String basename, String editionPage, ArrayList<Integer> splitTUs) {
		this.editionPage = editionPage;
		this.basename = basename;
		this.txtname = txtname;
		this.outdir = outdir;
		this.wordmax = max;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();
		this.infile = infile;
		this.splitTUs = splitTUs;
		//println "spliting pages with : "+splitTUs
		useSplitTUs = splitTUs.size() > 0;
		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		process();
	}

	private void closeMultiWriter()
	{
		if (multiwriter != null) {
			if (firstWord) { // there was no words
				this.idxstart.add("w_0")
				multiwriter.write("<span id=\"w_0\"/>");
			}
			multiwriter.write("</div>\n")
			multiwriter.write("</body>");
			multiwriter.write("</html>");
			multiwriter.close();
		}
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput()
	{
		wordcount = 0;
		shouldSplit = false;
		try {
			closeMultiWriter();
			File outfile = new File(outdir, txtname+"_"+(++pagecount)+".html")
			pages.add(outfile);
			firstWord = true; // waiting for next word

			multiwriter = new OutputStreamWriter(new FileOutputStream(outfile) , "UTF-8");

			multiwriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			multiwriter.write("<!DOCTYPE html>\n")
			multiwriter.write("<html>\n");
			multiwriter.write("<head>\n");
			multiwriter.write("<title>"+basename.toUpperCase()+" $txtname Edition - Page "+pagecount+"</title>\n");
			multiwriter.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"tmx.css\"/>\n");
			multiwriter.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>\n");
			multiwriter.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n");
			multiwriter.write("</head>\n");
			multiwriter.write("<body>\n");
			multiwriter.write("<div class=\"tmx\">\n")

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput() {
		try {
			return createNextOutput();
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	/**
	 * Process.
	 */
	void process() {

		String vNote = ""
		String cssNote = "note"
		boolean flagNote = false;
		
		String tuCSS = "tu"
		
		int tuCounter = 0;
		String localname = "";
		createNextOutput();
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "text":
							boolean grey = "true"
							String title = txtname;
							String subtitle = "";
							String author = "";
							String translator = "";
							String table = "";
							table += ("<table>\n");
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								String name = parser.getAttributeLocalName(i);
								grey = !grey;
								if (grey) table += ("<tr  style=\"background-color:lightgrey;\">");
								else table += ("<tr>");
								
								table += ("<td>"+parser.getAttributeLocalName(i)+": </td>");
								table += ("<td>"+parser.getAttributeValue(i)+"</td>");
								table += ("</tr>\n");
								
								if (name == "title") title = parser.getAttributeValue(i);
								else if (name == "subtitle") subtitle = parser.getAttributeValue(i);
								else if (name == "author") author = parser.getAttributeValue(i);
								else if (name == "translator") translator = parser.getAttributeValue(i);
							}
							multiwriter.write("<div class=\"header\">\n")
							multiwriter.write("<span class=\"authorStyle\"> "+author+" </span>")
							multiwriter.write("<span class=\"titleStyle\"> "+title+" </span>")
							multiwriter.write("<span class=\"subtitleStyle\"> "+subtitle+" </span>")
							if (translator.size() > 0) {
								multiwriter.write("<span class=\"translatorStyle\"> "+translator+" </span>")
							}
							multiwriter.write("</div>\n")
//							multiwriter.write(table)
//							multiwriter.write("</table><br/>\n");
							break;
						case "head":
							multiwriter.write("<h2>\n")
							break;
						case "graphic":
							String url = parser.getAttributeValue(null, "url")
						//println "URL: "+url
							if (url != null) multiwriter.write("<div><img src=\"$url\"/></div>");
							break;
						case "lg":
						case "p":
						case "q":
							String rend = parser.getAttributeValue(null, "rend")
							if (rend == null) rend = "normal"
							multiwriter.write("<p class=\"$rend\">\n")
							break;
						//case "pb":
						case editionPage:
							createNextOutput();
							wordcount=0;
							if (parser.getAttributeValue(null,"n") != null) {
								multiwriter.write("<p style=\"color:red\" align=\"center\">- "+parser.getAttributeValue(null,"n")+" -</p>\n")
							}
							break;
						case "lb":
						case "br":
							multiwriter.write("<br/>\n")
							break;
						case "tu":
							tuCounter++;
							if (useSplitTUs) {
								if (splitTUs.contains(tuCounter))
									createNextOutput();
							} else if (shouldSplit) {
								createNextOutput();
								this.splitTUs << tuCounter
							}
							String str = "";
							String title;
							tuCSS = "tu"
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								str += " "+parser.getAttributeLocalName(i)+"="+parser.getAttributeValue(i)
								if (parser.getAttributeLocalName(i).equals("cssClass")) {
									tuCSS = parser.getAttributeValue(i)
								} else if (parser.getAttributeLocalName(i).equals("title")) {
									title = parser.getAttributeValue(i)
								}
							}
							if (title != null) str = title;
							multiwriter.write("<h5 class=\"${tuCSS}Title\">$str</h5>\n")
							break;
						case "note":
						cssNote = "note"
						for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
							if (parser.getAttributeLocalName(i) == "cssClass") {
								cssNote = parser.getAttributeValue(i)
								break
							}
						}
							vNote = "";
							flagNote = true;
							break;
						case "seg":
						String css = "seg"
							if (tuCSS != "tu") css = tuCSS;
							multiwriter.write("<p class=\"${css}\">\n");
							break;
						case "w":
							wordid = parser.getAttributeValue(null,"id");

							wordcount++;
							if (wordcount >= wordmax) {
								//createNextOutput();
								shouldSplit = true;
								//println "should split: "+wordcount
							}
							
							if (firstWord) {
								firstWord = false;
								this.idxstart.add(wordid);
							}
							
							break;

						case "ana":
							flaginterp=true;
							interpvalue+=" "+parser.getAttributeValue(null,"type").substring(1)+":"
							break;

						case "form":
							wordvalue="";
							interpvalue ="";
							flagform=true;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "head":
							multiwriter.write("</h2>\n")
							break;
						case "lg":
						case "p":
						case "q":
							multiwriter.write("</p>\n")
							break;

						case "seg":
							multiwriter.write("</p>\n");
							break;
						case "note":
							flagNote = false;
							multiwriter.write("<div class=\"${cssNote}\">${vNote}</div>")
							break;

						case "form":
							flagform = false
							break;
						case "ana":
							flaginterp = false
							break;
						case "w":

							int l = lastword.length();
							String endOfLastWord = "";
							if (l > 0)
								endOfLastWord = lastword.subSequence(l-1, l);

							if (interpvalue != null)
								interpvalue = interpvalue.replace("&", "&amp;").replace("<", "&lt;").replace("\"","&quot;");

							if (NoSpaceBefore.contains(wordvalue) ||
							NoSpaceAfter.contains(lastword) ||
							wordvalue.startsWith("-") ||
							NoSpaceAfter.contains(endOfLastWord)) {
								multiwriter.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
							} else {
								multiwriter.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
							}
						//writer.write(wordvalue.replace("&", "&amp;").replace("\"","&quot;").replace("<", "&lt;")+"</span>");
							multiwriter.write(wordvalue.replace("&", "&amp;").replace("\"","&quot;").replace("<", "&lt;")+"</span>");
							lastword=wordvalue;
							break;
					}
					break;

				case XMLStreamConstants.CHARACTERS:
					if (flagform)
						if (parser.getText().length() > 0)
							wordvalue+=(parser.getText());
					if (flaginterp)
						if (parser.getText().length() > 0)
							interpvalue+=(parser.getText());
					if (flagNote)
						if (parser.getText().length() > 0)
							vNote+=(parser.getText());
					break;
			}
		}
		//writer.write("</body>");
		//writer.write("</html>");
		//writer.close();
		closeMultiWriter();
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
	}
}
