// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-05-02 11:55:17 +0200 (mar. 02 mai 2017) $
// $LastChangedRevision: 3436 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.xml;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.*
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;
import org.txm.utils.io.FileCopy
import org.txm.objects.Project
import org.txm.utils.BundleUtils;

/** 
 * Build a simple edition from a xml-tei. 
 *
 * @author mdecorde 
 */
class pager {

	List<String> NoSpaceBefore;

	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;

	/** The pagecount. */
	int pagecount = 0;

	/** The wordmax. */
	int wordmax = 0;

	/** The basename. */
	String basename = "";
	String txtname = "";
	File outdir;

	/** The wordid. */
	String wordid;

	/** The first word. */
	boolean firstWord = true;
	
	boolean paginate = true;
	boolean enableCollapsibles = false;

	/** The wordvalue. */
	String wordvalue = "";

		/** The lastword. */
	String lastword = " ";

	/** The wordtype. */
	String wordtype;

	/** The flagform. */
	boolean flagform = false;

	/** The flaginterp. */
	boolean flaginterp = false;

	/** The url. */
	private def url;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The writer. */
	OutputStreamWriter writer;

	/** The pagedWriter. */
	StaxStackWriter pagedWriter = null;

	/** The infile. */
	File infile;

	/** The outfile. */
	File outfile;

	/** The pages. */
	ArrayList<File> pages = new ArrayList<File>();

	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>();
	String paginationElement;

	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 * @param basename the basename
	 */
	pager(File infile, File outdir, String txtname, List<String> NoSpaceBefore,
	List<String> NoSpaceAfter, String basename, Project project) {
		this.paginationElement = project.getEditionDefinition("default").getPageElement()
		this.paginate = project.getEditionDefinition("default").getPaginateEdition()
		this.wordmax = project.getEditionDefinition("default").getWordsPerPage();
		this.enableCollapsibles = project.getEditionDefinition("default").getEnableCollapsibleMetadata();
		this.basename = basename;
		this.txtname = txtname;
		this.outdir = outdir;
		
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();
		this.infile = infile;

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);

		process();
		
		// copy txm.css file near editions
		try {
			File txmDir = infile.getParentFile().getParentFile();
			if (txmDir.exists() && txmDir.getName().equals("txm")) {
				File binDir = txmDir.getParentFile();
				File corporaDir = binDir.getParentFile();
				if (binDir.exists() && corporaDir.getName().equals("corpora")) {
					File htmlDir = new File(binDir, "HTML/"+basename.toUpperCase()+"/default")
					File txmhomeDir = corporaDir.getParentFile()
					File txmcss = new File(txmhomeDir, "css/txm.css")
					if (htmlDir.exists() && txmcss.exists()) {
						FileCopy.copy(txmcss, new File(htmlDir, txmcss.getName()))
					}
					BundleUtils.copyFiles("org.txm.core", "res", "org/txm", "js", htmlDir);
				}
			}
		} catch(Exception e) {
			println "Failed to copy TXM default CSS: "+e.getLocalizedMessage();
		}
	}

	private def closeMultiWriter()
	{
		if (pagedWriter != null) {
			def tags = pagedWriter.getTagStack().clone();

			if (firstWord) { // there was no words found
				pagedWriter.writeCharacters("");
				this.idxstart.add("w_0")
				pagedWriter.write("<span id=\"w_0\"></span>");
			}
			pagedWriter.writeEndElements();
			pagedWriter.close();
			return tags;
		} else {
			return [];
		}
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput() {
		wordcount = 0;
		try {
			def tags = closeMultiWriter();
			for (int i = 0 ; i < tags.size() ; i++) { // remove tags before the div@class=txmeditionpage and the div@class=txmeditionpage
				String tag = tags[i]
				if (tag.toString() == "[div, [[class, txmeditionpage]]]") {
					tags.remove(i--)
					break;
				}
				tags.remove(i--)
			}
			File outfile = new File(outdir, txtname+"_"+(++pagecount)+".html")
			pages.add(outfile);
			firstWord = true; // waiting for next word

			pagedWriter = new StaxStackWriter(outfile, "UTF-8");

			//pagedWriter.writeStartDocument()
			pagedWriter.writeDTD("<!DOCTYPE html>")
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeStartElement("html");
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeEmptyElement("meta", ["http-equiv":"Content-Type", "content":"text/html","charset":"UTF-8"]);
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeEmptyElement("link", ["rel":"stylesheet", "type":"text/css","href":"txm.css"]);
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeEmptyElement("link", ["rel":"stylesheet", "type":"text/css","href":"${basename}.css"]);
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeStartElement("head");
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeStartElement("title")
			pagedWriter.writeCharacters(basename.toUpperCase()+" Edition - Page "+pagecount)
			pagedWriter.writeEndElement(); // </title>
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeStartElement("script", ["src":"js/collapsible.js"]);
			pagedWriter.writeEndElement(); // </script>
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeEndElement() // </head>
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeStartElement("body") //<body>
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeStartElement("div") //<div class="txmeditionpage">
			pagedWriter.writeAttribute("class", "txmeditionpage")
			
			pagedWriter.writeStartElements(tags);
			return true;
		} catch (Exception e) {
			System.out.println("Error: "+e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput() {
		try {
			return createNextOutput();
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	/**
	 * Process.
	 */
	void process() {
		
		def figurerend = null;
		def unordered = false
		
		boolean flagNote = false;
		String noteContent = "";

		String localname = "";
		createNextOutput();
		def anaValues = [:]
		def anaType = ""
		def anaResp = ""
		def anaValue =  new StringBuilder()
		
		int event = parser.next()
		
		// go to the text element before writing anything
		for (; event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT && parser.getLocalName() == "text") {
				break; // continue parsing the content
			}
		}
		
		if (event == XMLStreamConstants.END_DOCUMENT) {
			println "Error: no 'text' element found. No edition built for $txtname"
			closeMultiWriter();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return;
		}
				
		for (; event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					
					if (localname == paginationElement) {
						if (paginate) createNextOutput();
						
						pagedWriter.write("\n");
						if (parser.getAttributeValue(null,"n") != null) {
							pagedWriter.writeElement("p", ["style":"color:red", "align":"center"], "- "+parser.getAttributeValue(null,"n")+" -")
						}
					}
					
					switch (localname) {
						case "text":
							pagedWriter.write("\n");
							if (parser.getAttributeValue(null,"id") != null) {
								pagedWriter.writeElement("h3", parser.getAttributeValue(null,"id"))
							}
							
							if (enableCollapsibles && parser.getAttributeCount() > 2) {
								pagedWriter.writeStartElement("button");
								pagedWriter.writeAttribute("class", "collapsible");
								pagedWriter.writeAttribute("onclick", "onCollapsibleClicked(this)");
								pagedWriter.writeCharacters("➕");
								pagedWriter.writeEndElement()
								pagedWriter.writeCharacters("\n")
							}
							pagedWriter.writeStartElement("table");
							if (enableCollapsibles && parser.getAttributeCount() > 2) {
								pagedWriter.writeAttribute("class", "transcription-table collapsiblecontent")
								pagedWriter.writeAttribute("style", "display:none;")
							} else {
								pagedWriter.writeAttribute("class", "transcription-table");
							}
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								pagedWriter.writeStartElement("tr");
								pagedWriter.writeElement("td", parser.getAttributeLocalName(i));
								pagedWriter.writeElement("td", parser.getAttributeValue(i).toString());
								pagedWriter.writeEndElement();
							}
							
							pagedWriter.writeEndElement() // table
							pagedWriter.writeEmptyElement("br")
							pagedWriter.writeCharacters("");
							break;
						case "note":
							flagNote = true;
							noteContent = ""
							break;
						case "a":
							pagedWriter.writeStartElement("a")
							pagedWriter.writeAttribute("href", parser.getAttributeValue(null,"href"));
							String target = parser.getAttributeValue(null,"target");
							if (target != null && !target.isEmpty()) pagedWriter.writeAttribute("target", target);
							break;
						case "head":
							pagedWriter.write("\n");
							pagedWriter.writeStartElement("h2")
							break;
						case "figure":
							figurerend = parser.getAttributeValue(null, "rend")
							break;
						case "graphic":
							pagedWriter.write("\n");
							String url = parser.getAttributeValue(null, "url")
							if (url != null) {
								pagedWriter.writeStartElement("div")
								pagedWriter.writeEmptyElement("img", ["class":figurerend, "src":url])
								pagedWriter.writeEndElement() // div
							}
							figurerend = null
							break;
						case "table":
							pagedWriter.writeStartElement("table")
							pagedWriter.writeAttribute("class", parser.getAttributeValue(null, "rend"))
							break;
						case "row":
							pagedWriter.writeStartElement("tr")
							break;
						case "cell":
							pagedWriter.writeStartElement("td")
							break;
						case "list":
							String type = parser.getAttributeValue(null,"type");
							
							if ("unordered" == type) {
								unordered = true;
								pagedWriter.writeStartElement("ul")
							} else {
								unordered = false
								pagedWriter.writeStartElement("ol")
							}
							break
						case "item":
							pagedWriter.writeStartElement("li")
							break;
						case "lg":
						case "p":
						case "q":
							pagedWriter.write("\n");
							String rend = parser.getAttributeValue(null, "rend")
							if (rend == null) rend = "normal";
							pagedWriter.writeStartElement("p",["class":rend])
							break;
						//case "pb":
						
						case "lb":
						case "br":
							pagedWriter.writeEmptyElement("br")
							break;
						case "w":
						
							wordid = parser.getAttributeValue(null,"id");
							anaValues.clear()
							wordcount++;
							if (paginate && wordcount >= wordmax) {
								createNextOutput();
							}
							
							if (firstWord) {
								firstWord = false;
								this.idxstart.add(wordid);
							}
							
							break;
						case "ana":
							flaginterp=true;
							anaType = parser.getAttributeValue(null, "type").substring(1)
							anaResp = parser.getAttributeValue(null, "resp").substring(1)
							anaValue.setLength(0)
							break;
						case "form":
							wordvalue="";
							flagform=true;
							break;
						//						default:
						//							pagedWriter.writeStartElement(localname)
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "head":
							pagedWriter.writeEndElement() // </h2>
							pagedWriter.writeCharacters("\n")
							break;
						case "note":
							flagNote = false;
							if (noteContent.length() > 0) {
								pagedWriter.writeStartElement("span", ["style":"color:red;", "title":noteContent]);
								pagedWriter.writeCharacters("[*]");
								pagedWriter.writeEndElement() // </span>
							}
							break;
						case "lg":
						case "p":
						case "q":
							pagedWriter.writeEndElement() // </p>
							pagedWriter.writeCharacters("\n")
							break;
						case "a":
							pagedWriter.writeEndElement() // </a>
							break;
						case "table":
							pagedWriter.writeEndElement(); // table
							pagedWriter.writeCharacters("\t")
							break;
						case "row":
							pagedWriter.writeEndElement(); // tr
							pagedWriter.writeCharacters("\t")
							break;
						case "cell":
							pagedWriter.writeEndElement(); // td
							break;
						case "list":
							pagedWriter.writeEndElement(); // ul or ol
							pagedWriter.writeCharacters("\t")
							break
						case "item":
							pagedWriter.writeEndElement(); // li
							pagedWriter.writeCharacters("\t")
							break;
						case "form":
							flagform = false
							break;
						case "ana":
							flaginterp = false
							if (anaValues[anaType] == null || "src".equals(anaResp)) {
								anaValues[anaType] = anaValue.toString().trim()
							}
							break;
						case "w":
							int l = lastword.length();
							String endOfLastWord = "";
							if (l > 0)
								endOfLastWord = lastword.subSequence(l-1, l);

							anaValues.put("id", wordid)
							String interpvalue = "- "+anaValues.entrySet().join("\n- ")
							
							if (NoSpaceBefore.contains(wordvalue) ||
							NoSpaceAfter.contains(lastword) ||
							wordvalue.startsWith("-") ||
							NoSpaceAfter.contains(endOfLastWord)) {
								pagedWriter.writeStartElement("span", ["title":interpvalue, "id":wordid]);
							} else {
								pagedWriter.writeCharacters(" ");
								pagedWriter.writeStartElement("span", ["title":interpvalue, "id":wordid]);
							}

							pagedWriter.writeCharacters(wordvalue);
							pagedWriter.writeEndElement();
							pagedWriter.writeComment("\n")
							lastword=wordvalue;
							wordvalue = ""; // reset form when a word is written
							break;
						//						default:
						//							pagedWriter.writeEndElement()
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					if (flagform && parser.getText().length() > 0) {
						wordvalue+=(parser.getText());
						if (flagNote == parser.getText().length() > 0)
							noteContent += parser.getText().replace("\n", " ");
					} else	if (flaginterp && parser.getText().length() > 0) {
						anaValue.append(parser.getText());
					} else if (flagNote == parser.getText().length() > 0) {
						noteContent += parser.getText().replace("\n", " ");
					}
					break;
			}
		}
		closeMultiWriter();
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
	}
}