package org.txm.scripts.importer.transcriber
import java.nio.charset.Charset
import org.txm.utils.CsvReader

/// START PARAMETERS ///
File dir = new File("C:/") // output and input directory
File csvfile = new File(dir, "test.csv") // 3 cols: int enq ext
String spkSeparator = "\\|" // regular expression
char colSeparator = '\t' // csv column separator
String csvencoding = "ISO-8859-1" // csv encoding

//transcriptions header
String date = "111128" // date of transcription edition
String scribes= "studentN" // scribe name
String encoding = "ISO-8859-1" // transcription encoding

/// END PARAMETERS ///
String canvas="""<?xml version="1.0" encoding="@ENCODING"?>
<!DOCTYPE Trans SYSTEM "trans-14.dtd">
<Trans scribe="@SCRIBE" audio_filename="@AUDIOFILE" version="1" version_date="@DATE">
<Speakers>
@SPEAKERS</Speakers>
<Episode>
<Section type="report" startTime="0" endTime="2.0">
<Turn startTime="0" endTime="1.0" speaker="@FIRSTSPK">
<Sync time="0"/>

</Turn>
</Section></Episode></Trans>
""";
String spkCanvas="<Speaker id=\"@ID\" name=\"@ID\" check=\"no\" dialect=\"native\" accent=\"\" scope=\"local\"/>"

//load csv
CsvReader csvreader = new CsvReader(csvfile.getAbsolutePath(), colSeparator, Charset.forName(csvencoding))

//check header
csvreader.readHeaders();
def titles = [];
for(String title : csvreader.getHeaders())
	titles << title
assert(titles.contains("int"))
assert(titles.contains("enq"))
assert(titles.contains("ext"))

// one transcription per csv line
while (csvreader.readRecord()){

	String ints = csvreader.get("int")
	String enqs = csvreader.get("enq")
	String exts = csvreader.get("ext")
	
	def intsS = ints.split(spkSeparator)
	def enqsS = enqs.split(spkSeparator)
	def extsS = exts.split(spkSeparator)
	assert(intsS.size() > 0)
	assert(enqsS.size() > 0)
	
	// build speaker declarations
	String speakers = ""
	for(String s : intsS)
		if(s.length() > 0)
			speakers += spkCanvas.replace("@ID", s)+"\n"
	for(String s : enqsS)
		if(s.length() > 0)
			speakers += spkCanvas.replace("@ID", s)+"\n"
	for(String s : extsS)
		if(s.length() > 0)
			speakers += spkCanvas.replace("@ID", s)+"\n"

	// some infos
	String transfile = intsS[0]
	String firstSpeaker = enqsS[0]
			
	// fill infos
	String content = canvas
	content = content.replace("@ENCODING", encoding)
	content = content.replace("@SCRIBE", scribes)
	content = content.replace("@AUDIOFILE", dir.getAbsolutePath()+transfile+".mp3")
	content = content.replace("@DATE", date)
	content = content.replace("@SPEAKERS", speakers)
	content = content.replace("@FIRSTSPK", firstSpeaker)
	
	//write transcription
	File outfile = new File(dir, transfile+".trs")
	println "create: "+outfile
	outfile.withWriter(encoding) { out ->
      out.println content
    }
}