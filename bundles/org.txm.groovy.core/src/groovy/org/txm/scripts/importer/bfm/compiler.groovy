// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.bfm

import org.txm.Toolbox;
import org.txm.importer.SAttributesListener
import org.txm.importer.cwb.*
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.FileUtils
import org.txm.utils.treetagger.TreeTagger;
import org.txm.objects.*
import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.txm.searchengine.cqp.corpus.*

/**
 * Produce CQP files from the TEI-TXM files. <br/>
 * - Read texts metadata with XPath queries <br/>
 * - Add the following word properties : sic, abbr, orig, lb and pb <br/>
 * - Keep &lt;front>, &lt;body> and &lt;back> for each text <br/>
 * - Text enclosed in &lt;q> is tokenized <br/>
 * 
 * @author mdecorde
 *
 */
class compiler {
	/** The debug. */
	private boolean debug= false;

	/** The annotate_status. */
	private boolean annotate_status=true;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The dir. */
	private def dir;

	/** The output. */
	private def output;

	/** The url. */
	private def url;

	/** The anahash. */
	static boolean firstWord = true;
	static private def anaTypes = [];
	private HashMap<String,String> anahash = new HashMap<String,String>() ;

	private static SAttributesListener sattrsListener;
	private static HashMap<String,ArrayList<String>> structs;
	private static HashMap<String, Integer> structsProf;

	/** The text. */
	private String text="";

	/** The base. */
	private String base="";

	/** The lang. */
	private String lang ="fr";

	/**
	 * contains the metadata xpath organize per name
	 */
	Properties metadataXPath;

	/**
	 * initialize.
	 *
	 */
	public compiler(){
		firstWord = true;
		anaTypes = [];
	}

	/**
	 * initialize the compiler.
	 *
	 * @param url the file to process
	 * @param text the Texte's name
	 * @param base the base's name
	 * @param project the Project's name
	 */
	public compiler(URL url,String text,String base, String projectName, Properties metadataXPath)
	{
		this.metadataXPath = metadataXPath;
		this.text = text
		this.base = base;
		try {
			this.url = url;
			inputData = url.openStream();

			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
			if (sattrsListener == null)
				sattrsListener = new SAttributesListener(parser);
			else
				sattrsListener.start(parser)
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	ArrayList<File> orderedFiles;
	public ArrayList<File> getOrderedTxmFiles() {
		return orderedFiles;
	}

	/**
	 * Sets the lang.
	 *
	 * @param lang the new lang
	 */
	public void setLang(String lang)
	{
		this.lang = lang;
	}

	/**
	 * Sets the annotation done.
	 *
	 * @param done the new annotation done
	 */
	public void setAnnotationDone(boolean done)
	{
		this.annotate_status = done;
	}

	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(File f) {
		try {
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8");
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
		return true;
	}

	/**
	 * Go to text.
	 */
	private void GoToText()
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.END_ELEMENT)
				if (parser.getLocalName().equals("teiHeader"))
					return;
		}
	}

	/**
	 * Increment.
	 *
	 * @param parser the parser
	 * @param value the value
	 * @return the java.lang. object
	 */
	private def increment(XMLStreamReader parser, int value)
	{
		String n=null;
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			//System.out.println("attr name "+parser.getAttributeLocalName(i));
			if (parser.getAttributeLocalName(i) == "n") {
				n = parser.getAttributeValue(i);
				break;
			}
		}
		//System.out.println("inc n "+n);
		if (n != null)
			try {
				value = Integer.parseInt(n);
				return value;
			}
			catch (Exception e) {return value+1;}

		value = value+1;
		return value;
	}

	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean transfomFileCqp(Project project, File cqpFile)
	{
		try {
			if (!createOutput(cqpFile)) return false;

			String headvalue = ""
			String vAna = "";
			String vForm = "";
			String wordid = "";
			String vHead = "";

			Integer p_id = 0;
			Integer s_id = 0;
			Integer q_id = 0;
			int sp_id = 0;
			Integer body_id = 0;
			Integer front_id = 0;
			Integer back_id = 0;
			Integer lb_id = 0;
			Integer pb_id = 0;
			Integer ab_id = 0;
			int foreign_id = 0;
			int name_id = 0;

			boolean captureword = false;

			String vExpan = "";
			String vCorr = "";
			String vReg = "";
			String vOrig = "";
			String vSic = "";
			String vAbbr = "";
			String givenpos = "";
			String pb_n = "";
			String foreign_lang = "";
			String nameType = "";
			String anaType;
			//String abType = "";
			
			boolean foundtei=false, foundtext=false;

			boolean flaglg = false;
			int levelq = 0;
			//boolean flagq = false;
			boolean flaghead = false;
			//Added:
			boolean flagSp = false;
			boolean flagAuthor = false;
			boolean flagDate = false;
			boolean flagWord = false;
			boolean flagForm = false;
			boolean flagAna = false;

			boolean flagchoice = false;
			boolean flagcorr = false;
			boolean flagsic = false;
			boolean flagreg = false;
			boolean flagexpan = false;
			boolean flagorig = false;
			boolean flagabbr = false;
			boolean flagfw = false;
			//boolean flagSupplied = false;
			int levelSupplied = 0;
			//boolean flagSurplus = false;
			boolean flagForeign = false;
			//boolean flagName = false;

			this.GoToText();
			int missingId= 0
			boolean USEVERSE = false; // switch default reference to verse references
			String titreId; // the title to use in the reference
			
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:

						String localname = parser.getLocalName();
						if (foundtext) sattrsListener.startElement(localname);
						
						switch (localname) {
							case "TEI":
								foundtei = true;
								break;
							case "text":
								foundtext = true;
								sattrsListener.startElement(localname);
								output.write("<text id=\""+text+"\"")

								for (int i = 0; i < parser.getAttributeCount() ; i++) {
									String name = parser.getAttributeLocalName(i);
									if ("id" == name || "base" == name || "project" == name) continue;
									output.write(" "+name+"=\""+parser.getAttributeValue(i)+"\"");
									
									if (name == "forme") {
										USEVERSE = (parser.getAttributeValue(i).contains("vers"))
									} else if (name == "sigle") {
										titreId = parser.getAttributeValue(i)
									}
								}
								
								output.write(" base=\""+base+"\" project=\""+project.getName()+"\">\n");
								captureword=true;
								break;

							case "div":
							//output.write("<div type=\""+parser.getAttributeValue(null,"type")+"\">\n");
								String divType = "NA";
								String divSubtype = "NA";
								String divN = "NA";
								String divId ="NA";
								for(int i = 0 ; i < parser.getAttributeCount(); i++) {
									if(parser.getAttributeLocalName(i) == "type") {
										divType = parser.getAttributeValue(i);
									} else if(parser.getAttributeLocalName(i) == "subtype") {
										divSubtype = parser.getAttributeValue(i);
									} else if(parser.getAttributeLocalName(i) == "n") {
										divN = parser.getAttributeValue(i);
									} else if(parser.getAttributeLocalName(i) == "id") {
										divId = parser.getAttributeValue(i);
										break;
									}
								}
								output.write("<div type=\""+divType+"\" subtype=\""+divSubtype+"\" n=\""+divN+"\" id=\""+divId+"\">\n");
								break;
							case "p":
								p_id = increment(parser, p_id);
								output.write("<p n=\""+p_id+"\">\n");
								break;
							case "ab":
								ab_id = increment(parser, ab_id)
								output.write("<ab n=\""+ab_id+"\" type=\""+parser.getAttributeValue(null,"type")+"\" subtype=\""+parser.getAttributeValue(null,"subtype")+"\" rend=\""+parser.getAttributeValue(null,"rend")+"\">\n");
								break;
							case "q":
								q_id = increment(parser, q_id)
								output.write("<q n=\""+q_id+"\">\n");
							//flagq=true;
								levelq = levelq + 1;
								break;
							case "sp":
								sp_id = increment(parser, sp_id)
								output.write("<sp n=\""+sp_id+"\">\n");
								flagSp = true;
								break;
							case "front":
								front_id = increment(parser, front_id)
								output.write("<front n=\""+front_id+"\">\n");
								break;
							case "body":
								body_id= increment(parser, body_id)
								output.write("<body n=\""+body_id+"\">\n");
								break;
							case "back":
								back_id = increment(parser, back_id)
								output.write("<back n=\""+back_id+"\">\n");
								break;
							case "lb":
								lb_id = increment(parser, lb_id)
								break;
							case "pb":
								pb_id = increment(parser, pb_id)
								for (int i = 0 ; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeLocalName(i) == "n") {
										pb_n = parser.getAttributeValue(i);
									}
								}
								break;
							case "s":
								s_id = increment(parser, s_id)
								output.write("<s n=\""+s_id+"\">\n");
								break;
							case "choice":
								flagchoice = true;
								break;
							case "corr":
								flagcorr = true;
								vCorr= "";
								break;
							case "reg":
								flagreg = true;
								vReg= "";
								break;
							case "expan":
								flagexpan = true;
								vExpan= "";
								break;
							case "orig":
								flagreg = true;
								vOrig= "";
								break;
							case "sic":
								flagsic = true;
								vSic= "";
								break;
							case "abbr":
								flagreg = true;
								vAbbr= "";
								break;
							case "foreign":
								flagForeign = true;
								for (int i = 0 ; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeLocalName(i) == "lang") {
										lang = parser.getAttributeValue(i);
										break;
									}
								}

								output.write("<foreign n=\""+(foreign_id++)+"\" lang=\""+lang+"\">\n");
							//vForeign = "";
								break;

							case "name":
							//flagName = true;
								for(int i = 0 ; i < parser.getAttributeCount(); i++)
									if(parser.getAttributeLocalName(i) == "type")
								{
									nameType = parser.getAttributeValue(i);
									break;
								}

								output.write("<name n=\""+(name_id++)+"\" type=\""+nameType+"\">\n");
								break;
							case "supplied":
							//flagSupplied = true;
								levelSupplied = levelSupplied + 1;
								break;

							case "surplus":
								flagfw = true;
								break;

							case "del":
								flagfw = true;
								break;

							case "w":
								givenpos = "";
								wordid = "w_"+text+"_m"+missingId++
								for (int i = 0 ; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeLocalName(i) == "id") {
										wordid = parser.getAttributeValue(i);
									} else if (parser.getAttributeLocalName(i) == "type") {
										givenpos = parser.getAttributeValue(i);
									}
								}
								if (wordid.startsWith("w")) {
									if (!wordid.startsWith("w_"))
										wordid = "w_"+wordid.substring(1)
								} else {
									wordid = "w_"+wordid;
								}

								if (givenpos == null || givenpos == "")
									givenpos = "NA";
								vForm = "";
								anahash.clear(); // remove previous word ana values
								flagWord = true;
								break;
							case "form":
								flagForm = true;
								vForm = "";
								break;

							case "ana":
								flagAna = true;
								anaType = parser.getAttributeValue(null, "type")
								anahash.put(anaType, "");
								if (firstWord) {
									anaTypes << anaType;
								}
								break;
						}
						break;

					case XMLStreamConstants.END_ELEMENT:
						String localname = parser.getLocalName();
						if (foundtext) sattrsListener.endElement(localname);

						switch (localname) {
							case "div":
								output.write("</div>\n");
								break;
							case "text":
								output.write("</text>\n");
								captureword=false;
								break;
							case "p":
								output.write("</p>\n");
								break;
							case "s":
								output.write("</s>\n");
								break;
							case "ab":
								output.write("</ab>\n");
								break;
							case "q":
								output.write("</q>\n");
							//flagq= false;
								levelq = levelq - 1;
								break;
							case "sp":
								output.write("</sp>\n");
								flagSp = false;
								break;
							case "front":
								output.write("</front>\n");
								break;
							case "body":
								output.write("</body>\n");
								break;
							case "back":
								output.write("</back>\n");
								break;

							//							case "fw":
							//							flagfw = false;
							//							break;

							case "choice":
								if(vOrig == "")
									vOrig="NA";
								if(vSic == "")
									vSic="NA";
								if(vAbbr == "")
									vAbbr="NA";

								String ref;
								if(USEVERSE)
									ref = titreId+", p."+pb_n+", v."+lb_id;
								else
									ref = titreId+", p."+pb_n;

								if (flagfw) {
									// on est hors texte
								} else {
									String vFormToWrite = vForm;
									if (vCorr != "") {
										vFormToWrite = vCorr;
									} else if(vReg != "") {
										vFormToWrite = vReg;
									} else if(vExpan != "") {
										vFormToWrite = vExpan
									}
									firstWord = false;
									output.write( vFormToWrite +"\t"+wordid+"\t"+levelq.toString().substring(0,1)+"\t"+flagSp.toString().substring(0,1)+"\t"+pb_n+"\t"+lb_id+"\t"+vOrig+
											"\t"+vSic+"\t"+vAbbr+"\t"+ref+"\t"+givenpos+"\t"+levelSupplied.toString().substring(0,1)+"\t"+lang+"\t"+nameType);
									for(String type : anaTypes) {
										output.write("\t"+anahash.get(type));
									}
									output.write("\n")
								}
								flagchoice = false;
								vCorr= "";
								vSic= "";
								break;
							case "corr":
								flagcorr = false;

								break;
							case "reg":
								flagreg = false;
								vReg = "";
								break;
							case "expan":
								flagexpan = false;
								vExpan= "";
								break;
							case "orig":
								flagreg = false;
								vOrig= "";
								break;
							case "sic":
								flagsic = false;

								break;
							case "abbr":
								flagreg = false;
								vAbbr= "";
								break;

							case "foreign":
								flagForeign = false;
								lang = "";
								output.write("</foreign>\n");
								break;

							case "name":
							//flagName = false;
								nameType = "";
								output.write("</name>\n");
								break;

							case "supplied":
							//flagSupplied = false;
								levelSupplied = levelSupplied - 1;
								break;

							case "surplus":
								flagfw = false;
								break;

							case "del":
								flagfw = false;
								break;

							case "w":
								if (captureword) {
									if (flagchoice) {

									} else if(flagfw) {

									} else {
										if (vOrig == "")
											vOrig="NA";
										if(vSic == "")
											vSic="NA";
										if(vAbbr == "")
											vAbbr="NA";
										if (nameType == "")
											nameType = "NA";
										if(lang == "")
											lang="fr"

										String ref;
										if(USEVERSE)
											ref = titreId+", p."+pb_n+", v."+lb_id;
										else
											ref = titreId+", p."+pb_n;

										firstWord = false;
										output.write(vForm.replaceAll("&", "&amp;").replaceAll("<", "&lt;") +"\t"+wordid+"\t"+levelq.toString().substring(0,1)+"\t"+flagSp.toString().substring(0,1)+"\t"+pb_n+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\t"+ref+"\t"+givenpos+"\t"+levelSupplied.toString().substring(0,1)+"\t"+lang+"\t"+nameType);
										for(String type : anaTypes) {
											output.write("\t"+anahash.get(type));
										}
										output.write("\n")
									}

									flagWord = false;
								}
								break;

							case "form":
								flagForm = false;
								break;

							case "ana":
								flagAna = false;
								anahash.put(anaType, vAna);
								vAna = "";
								break;
						}
						break; // end elem

					case XMLStreamConstants.CHARACTERS:
						if (flagAna) {
							vAna += parser.getText().trim()
						}

						if (flagForm) {
							vForm += parser.getText().trim();
							if (flagchoice) {
								if (flagsic) {
									vSic += parser.getText().trim();
								}
								if (flagorig) {
									vOrig += parser.getText().trim();
								}
								if (flagabbr) {
									vAbbr += parser.getText().trim();
								}
								if (flagcorr) {
									vCorr += parser.getText().trim();
								}
							}
						}
				}
			}
			//output.write("</txmcorpus>");
			output.close();
			parser.close();
			inputData.close();
		} catch (Exception ex) {
			System.out.println("Exception while parsing " + inputData);
			ex.printStackTrace();
			if (output != null) output.close();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}

		return true;
	}


	/**
	 * Run.
	 *
	 * @param rootDirFile contains the TEI-TXM files
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(Project project, File binDir, File txmDir, String corpusname, Properties metadataXPath)
	{
		sattrsListener = null; // reset SAttribute Listener for each new import
		this.metadataXPath = metadataXPath;

		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		
		CorpusBuild corpus = project.getCorpusBuild(project.getName(), MainCorpus.class);
		if (corpus != null) {
			if (project.getDoUpdate()) {
				corpus.clean(); // remove old files
			} else {
				corpus.delete(); // remove old files and TXMResult children
			}
		} else {
			corpus = new MainCorpus(project);
			corpus.setID(project.getName());
			corpus.setName(project.getName());
		}
		corpus.setDescription("Built with the BFM import module");
		
		File cqpFile = new File(binDir,"cqp/"+corpusname+".cqp");
		cqpFile.delete()
		new File(binDir,"cqp").mkdirs()
		new File(binDir,"data").mkdirs()
		new File(binDir,"registry").mkdirs()

		String textid = "";
		int counttext = 0;
		List<File> files = FileUtils.listFiles(txmDir);
		
		// get text siecles to be able to sort with it
		HashMap<File,Integer[]> filesiecle = new HashMap<File, Integer[]>()
		for (File f : files) {
			Integer[] date = new Integer[3];
			date[0] = date[1] = date[2] = 0;
			String xpath = "//TEI/teiHeader/profileDesc/creation/date[@type='compo']/@when"
			if (metadataXPath.containsKey("datecompo"))
				xpath = metadataXPath.get("datecompo")
			String datecompo = XPathResult.getXpathResponse(f, xpath);
			if (datecompo != null) {
				//println f.getName()+" > "+datecompo
				String[] split = datecompo.split("-"); // yyyy-mm-dd
				if (split.length == 3) {
					date[0] = Integer.parseInt(split[0]);
					date[1] = Integer.parseInt(split[1]);
					date[2] = Integer.parseInt(split[2]);
				}
				else if (split.length == 1) { // yyyy
					date[0] = Integer.parseInt(split[0]);
					date[1] = 1;
					date[2] = 1;
				}				
			}
			filesiecle.put(f, date);
		}
		//println "date compos: "+filesiecle
		Collections.sort(files); // Alpha order
		Collections.sort(files, new Comparator<File>() { // Date order
					@Override
					public int compare(File o1, File o2) {
						Integer[] date1 = filesiecle.get(o1);
						Integer[] date2 = filesiecle.get(o2);
						if (date1[0] < date2[0]) {
							return -1;
						} else if(date1[0] > date2[0]) {
							return 1;
						}

						if (date1[1] < date2[1]) {
							return -1;
						} else if(date1[1] > date2[1]) {
							return 1;
						}

						if (date1[2] < date2[2]) {
							return -1;
						} else if(date1[2] > date2[2]) {
							return 1;
						}

						return 0;
					}
				});
			
		this.orderedFiles = files;
		println("process "+files.size()+" files ")
		//println("files: $files")
		//write txmcorpus
		if (!createOutput(cqpFile)) {
			println "Error: could not write cqp file"
			return false;
		} else {
			output.write("<txmcorpus lang=\""+lang+"\">\n");
			output.close();
		}
		
		//1- Transform into CQP file
		for (File f : files) {
			counttext++;
			if (!f.exists()) {
				println("file "+f+ " does not exists")
			} else {
				print "."
				String txtname = f.getName().substring(0, f.getName().length()-4);
				def builder = new compiler(f.toURI().toURL(), txtname, corpusname, "default", metadataXPath);
				builder.setLang(lang)
				if (!builder.transfomFileCqp(project, cqpFile)) {
					println "Failed to compile "+f
				}
				builder.setAnnotationDone(this.annotate_status);
			}
		}

		//close txmcorpus
		if (!createOutput(cqpFile)) {
			println "Error: could not write cqp file"
			return false;
		} else {
			output.write("</txmcorpus>\n");
			output.close();
		}
		println ""
		
		//2- Import into CWB
		def outDir = binDir.getAbsolutePath();;
		CwbEncode cwbEn = new CwbEncode();
		cwbEn.setDebug(debug);
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbMa.setDebug(debug);
		
		def pAttrs = ["id","q","sp","pb","lb","orig","sic","abbr","ref","pos","supplied","lang","nametype"];
		for(String type : anaTypes)
			pAttrs.add(type.substring(1)); // remove #

		structs = sattrsListener.getStructs();
		structsProf = sattrsListener.getProfs();
		if (debug) {
			println structs
			println structsProf
		}
		// add structures+properties found in sources
		List<String> sargs = new ArrayList<String>();
		for (String name : structs.keySet()) {
			if ( name == "text") continue; // added after
			//if ( name == "q") continue; // added after
			//if ( name == "foreign") continue; // added after
			String concat = name+":"+structsProf.get(name); // append the depth
			for (String value : structs.get(name)) // append the attributes
				concat += "+"+value;
			if ((name == "p" || name == "body" || name == "back" || name == "front") &&
			!(concat.endsWith("+n") || concat.contains("+n+")))
				concat += "+n"
			sargs.add(concat);
		}

		String textSAttributes = "text:0+id+base+project";
		if (metadataXPath != null) {
			for (String meta : metadataXPath.keySet()) // text property declarations from metadata.csv
				textSAttributes+="+"+meta;
		}
		if (!metadataXPath.keySet().contains("sigle"))
			textSAttributes+="+sigle";
			
		sargs.add(textSAttributes)
		sargs.add("txmcorpus:0+lang")
		//sargs.add("q:0+n+lang")
		for (int c = 0 ; c < sargs.size() ; c++) {
			String sarg = sargs.get(c);
			if (sarg.startsWith("q:")) {
				if (! sarg.contains("+n")) sarg +="+n"
				if (! sarg.contains("+lang")) sarg +="+lang"

				sargs.set(c, sarg);
			} else if(sarg.startsWith("foreign:")) {
				if (! sarg.contains("+n")) sarg +="+n"
				if (! sarg.contains("+lang")) sarg +="+lang"
				sargs.set(c, sarg);
			} else if(sarg.startsWith("ab:") || sarg.startsWith("sp:")) {
				if (! sarg.contains("+n")) sarg +="+n"
				if (! sarg.contains("+subtype")) sarg +="+subtype"
				if (! sarg.contains("+rend")) sarg +="+rend"
				sargs.set(c, sarg);
			}  else if(sarg.startsWith("div:")) {
				if (! sarg.contains("+n")) sarg +="+n"
				if (! sarg.contains("+id")) sarg +="+id"
				if (! sarg.contains("+type")) sarg +="+type"
				if (! sarg.contains("+subtype")) sarg +="+subtype"
				sargs.set(c, sarg);
			} else if(sarg.startsWith("name:")) {
				if (! sarg.contains("+n")) sarg +="+n"
				if (! sarg.contains("+type")) sarg +="+type"
				sargs.set(c, sarg);
			}
		}
		sargs.sort();
		
		String[] sAttributes = sargs;
		String[] pAttributes = pAttrs;
		println "P-attributes: "+pAttributes
		println "S-attributes: "+sargs

		try {
			String regPath = outDir + "/registry/"+corpusname.toLowerCase(); // CQP wants lower case registry files
			cwbEn.run(outDir + "/data/${corpusname}", outDir + "/cqp/"+corpusname+".cqp", regPath,pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(corpusname, outDir + "/registry");
		} catch (Exception ex) {System.out.println(ex);return false;}

		if (project.getCleanAfterBuild()) {
			new File(binDir, "cqp").deleteDir()
		}
		
		return true;
	}

	/**
	 * show cwb utils messages.
	 */
	public void setDebug()
	{
		this.debug = true;
	}

	/**
	 * test purpose.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/bfm");
		def c = new compiler();
		c.setDebug();
		c.setCwbPath("~/TXM/cwb/bin");
		c.run(dir);
	}
}
