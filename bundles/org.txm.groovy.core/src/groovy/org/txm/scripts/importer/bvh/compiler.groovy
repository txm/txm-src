// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.bvh


import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.BuildTTSrc;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;

import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Produce CQP files from TEI-TXM files. <br/>
 * Add the following word properties : "frpos","frlemme","afrpos","afrlemme","id","page","line","orig","sic","abbr". <br/>
 * 
 * @author mdecorde
 */
class compiler 
{
	
	/** The debug. */
	private boolean debug= false;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	/** The url. */
	private def url;
	
	/** The anahash. */
	private HashMap<String,String> anahash =new HashMap<String,String>() ;
	
	/** The text. */
	String text="";
	
	/** The base. */
	String base="";
	
	/** The project. */
	String project="";
	
	/** The lang. */
	private String lang ="fr";
	
	/**
	 * Instantiates a new compiler.
	 */
	public compiler(){}
	
	/**
	 * initialize the compiler.
	 *
	 * @param url the fiel to read
	 * @param text the Texte's name
	 * @param base the Base's name
	 * @param project the Project's name
	 */
	public compiler(URL url,String text,String base, String project)
	{
		this.text = text
		this.base = base;
		this.project = project;
		try {
			this.url = url;
			inputData = url.openStream();
			
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}
	
	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8");
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/**
	 * Go to text.
	 */
	private void GoToText()
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
		{
			if(event == XMLStreamConstants.END_ELEMENT)
				if(parser.getLocalName().equals("teiHeader"))
					return;
		}
	}
	
	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean transfomFileCqp(String dirPathName, String fileName)
	{
		createOutput(dirPathName, fileName);
		
		String headvalue=""
		String vAna = "";
		String vForm = "";
		String wordid= "";
		String vHead = "";
		
		String vExpan = "";
		String vCorr = "";
		String vReg = "";
		String vOrig = "";
		String vSic = "";
		String vAbbr = "";
		
		String divtype = "";
		
		int p_id = 0;
		int q_id = 0;
		int lg_id = 0;
		int l_id = 0;
		int pb_id = 0;
		int lb_id = 0;
		int said_id = 0;
		int foreign_id = 0;
		int sp_id = 0;
		int speaker_id = 0;
		int stage_id = 0;
		
		boolean captureword = false;
		boolean fwcapture = false;
		boolean choicecapture = false;
		boolean divcapture = false;
		
		boolean flagchoice = false;
		boolean flagcorr = false;
		boolean flagsic = false;
		boolean flagreg = false;
		boolean flagexpan = false;
		boolean flagorig = false;
		boolean flagabbr = false;
		boolean flagfw = false;
		
		boolean flaglg = false;
		boolean flaghead = false;
		boolean flagAuthor = false;
		boolean flagDate = false;
		boolean flagForm = false;
		boolean flagAna = false;
		boolean
		
		this.GoToText()
		output.write("<txmcorpus lang=\""+lang+"\">\n"); 
		try 
		{
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
			{
				switch (event) 
				{
					case XMLStreamConstants.START_ELEMENT:
						switch (parser.getLocalName()) 
						{				
							case "text":
							captureword=true;
							output.write("<text id=\""+text+"\" type=\""+parser.getAttributeValue(null,"type")+"\" base=\""+base+"\" project=\""+project+"\">\n");
							break;
							
							case "div":
								divtype = parser.getAttributeValue(null,"type")
							switch(divtype)
							{
								case "toa":
								case "toc":
								break;
								case null:
								break;
								default:
								output.write("<div id=\""+parser.getAttributeValue(null,"n")+"\" type=\""+parser.getAttributeValue(null,"type")+"\">\n");		
							}
							
							break;
							
							case "p":
							output.write("<p id=\""+(p_id++)+"\">\n");
							break;
							
							case "lg":
							output.write("<lg id=\""+(lg_id++)+"\">\n");
							break;
							
							case "l":
							output.write("<l id=\""+(l_id++)+"\">\n");
							break;
							
							case "q":
							output.write("<q id=\""+(q_id++)+"\">\n");
							break;
							
							case "said":
							output.write("<said id=\""+(said_id++)+"\">\n");
							break;
							
							case "foreign":
							output.write("<foreign id=\""+(foreign_id++)+"\">\n");
							break;
							
							case "stage":
							output.write("<stage id=\""+(stage_id++)+"\">\n");
							break;
							
							case "sp":
							output.write("<sp id=\""+(sp_id++)+"\">\n");
							break;
							
							case "speaker":
							output.write("<speaker id=\""+(speaker_id++)+"\">\n");
							break;
							
							case "fw":
							flagfw = true;
							break;
							case "choice":
							flagchoice = true;
							break;
							case "corr":
							flagcorr = true;
							vCorr= "";
							break;
							case "reg":
							flagreg = true;
							vReg= "";
							break;
							case "expan":
							flagexpan = true;
							vExpan= "";
							break;
							case "orig":
							flagreg = true;
							vOrig= "";
							break;
							case "sic":
							flagsic = true;
							vSic= "";
							break;
							case "abbr":
							flagreg = true;
							vAbbr= "";
							break;
							
							case "w":
							for(int i = 0 ; i < parser.getAttributeCount(); i++)
								if(parser.getAttributeLocalName(i).equals("id"))
								{	
									wordid = parser.getAttributeValue(i);
								}
							break;
							case "form":
							flagForm = true;
							vForm = "";
							vAna ="";
							break;
							
							case "ana":
							flagAna = true;
							break;
						}
						break;
					
					case XMLStreamConstants.END_ELEMENT:
						switch (parser.getLocalName()) 
						{	
							case "body":
							captureword=false;
							break;
							
							case "text":
							output.write("</text>\n");
							break;
							
							case "div":
							switch(divtype)
							{
								case "toa":
								case "toc":
								break;
								default:
									output.write("</div>\n");		
							}
							
							break;
							
							case "p":
							output.write("</p>\n");
							break;
							
							case "q":
							output.write("</q>\n");
							break;
							
							case "said":
							output.write("</said>\n");
							break;
							
							case "foreign":
							output.write("</foreign>\n");
							break;
							
							case "lg":
							output.write("</lg>\n");
							break;
							
							case "pb":
							pb_id++;
							break;
							
							case "lb":
								lb_id++;
								break;
							
							case "l":
							output.write("</l>\n");
							break;
							
							case "stage":
							output.write("</stage>\n");
							break;
							
							case "sp":
							output.write("</sp>\n");
							break;
							
							case "speaker":
							output.write("</speaker>\n");
							break;
							
							
							case "fw":
							flagfw = false;
							break;
							case "choice":
							flagchoice = false;
							break;
							case "corr":
							flagcorr = false;
							vCorr= "";
							break;
							case "reg":
							flagreg = false;
							vReg= "";
							break;
							case "expan":
							flagexpan = false;
							vExpan= "";
							break;
							case "orig":
							flagreg = false;
							vOrig= "";
							break;
							case "sic":
							flagsic = false;
							vSic= "";
							break;
							case "abbr":
							flagreg = false;
							vAbbr= "";
							break;
							
							case "w":
							if(vAna != null)
								if(captureword)
								{
									if(flagchoice)
									{
										if(flagcorr)
										{
											output.write( vCorr +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
										}
										else if(flagreg)
										{
											output.write( vReg +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
										}
										else if(flagsic)
										{
											
										}
										else if(flagexpan)
										{
											output.write( vExpan +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
										}	
									}
									else if(flagfw)
									{
										
									}
									else
									{
										output.write( vForm +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
									}
									
								}
							vAna = "";
							vForm = "";
							break;
							
							case "form":
							flagForm = false;
							break;
							
							case "ana":
							vAna += "\t";
							flagAna = false;
							break;
						}
						break;
					
					case XMLStreamConstants.CHARACTERS:
						if(flagForm)
						{	
							vForm += parser.getText().trim();
						}
						if(flagAna)
						{
							vAna += parser.getText().trim();
						}
						if(flagsic)
						{
							vSic += parser.getText().trim();
						}
						if(flagorig)
						{
							vOrig += parser.getText().trim();
						}
						if(flagabbr)
						{
							vAbbr += parser.getText().trim();
						}
						break;
				}
			}
			output.write("</txmcorpus>"); 
			output.close();
			parser.close();
			inputData.close();
		}
		catch (XMLStreamException ex) {
			System.out.println(ex);
		}
		catch (IOException ex) {
			System.out.println("IOException while parsing " + inputData);
		}
		
		return true;
	}
	
	
	/**
	 * start processing.
	 *
	 * @param rootDirFile the directory containing the TEI-TXM files
	 * @return true, if successful
	 */
	public boolean run(File rootDirFile) 
	{
		String rootDir =rootDirFile.getAbsolutePath();
		
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		
		if(!new File(rootDir).exists())
		{
			println ("binary directory does not exists: "+rootDir)
			return false;
		}
		new File(rootDir+"/cqp/","bvh.cqp").delete();//cleaning&preparing
		new File(rootDir,"/cqp/").deleteDir();
		new File(rootDir,"/cqp/").mkdir();
		new File(rootDir,"/data/").deleteDir();
		new File(rootDir,"/data/").mkdir();
		new File(rootDir,"registry/").mkdir();
		
		String textid="";
		int counttext =0;
		List<File> files = new File(rootDirFile,"txm").listFiles();
		Collections.sort(files);
		//1- Transform into CQP file
		for(File f : files)
		{
			counttext++;
			if(!f.exists())
			{
				println("file "+f+ " does not exists")	
			}
			else
			{	
				println("process file "+f)
				String txtname = f.getName().substring(0,f.getName().length()-4);
				def builder = new compiler(f.toURL(),txtname, "bvh", "default");
				builder.setLang lang
				builder.transfomFileCqp(rootDir+"/cqp","bvh.cqp");
			}
		}
		
		//2- Import into CWB
		def outDir =rootDir;
		def outDirTxm = rootDir;
		
		CwbEncode cwbEn = new CwbEncode();
		cwbEn.setDebug(debug);
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbMa.setDebug(debug);
		String[] pAttributes = ["frpos","frlemme","afrpos","afrlemme","id","page","line","orig","sic","abbr"];
		String[] sAttributes = ["txmcorpus:0+lang", "text:2+id+type+base+project","div:2+id+type","p:0+id","q:0+id","said:0+id","foreign:0+id","lg:0+id","l:0+id","sp:0+id","speaker:0+id","stage:0+id"];
		try {
			String regPath = outDirTxm + "/registry/"+"bvh"
			cwbEn.run(outDirTxm + "/data", outDir + "/cqp/"+"bvh.cqp", regPath, pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run("BVH", outDirTxm + "/registry");
			
		} catch (Exception ex) {System.out.println(ex); return false}
		
		System.out.println("Done.") 
		
		return true;
	}
	
	/**
	 * show cwb-encode and cwb-makeall messages.
	 */
	public void setDebug()
	{
		this.debug = true;
	}
	
	/**
	 * test purpose.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/bvh");
		def c = new compiler();
		c.setDebug();
		c.setCwbPath("~/TXM/cwb/bin");
		c.run(dir);
	}
}
