// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.importer.transcriber

import org.txm.scripts.filters.Tokeniser.SimpleTokenizerXml;

/**
 * The Class TranscriberTokenizer : manages Transcriber notations
 */
class TranscriberTokenizer extends SimpleTokenizerXml{
	
	/** The troncature. */
	boolean troncature = false;
	
	/** The audio. */
	String audio ="F";
	
	/** The notation. */
	String notation = "";
	
	/** paroles rapportees */
	boolean rapp = false;
	
	/** The event. */
	String event="";
	String txtname;
	
	/**
	 * Instantiates a new transcriber tokenizer.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 */
	public TranscriberTokenizer(File infile, File outfile)
	{
		super(infile, outfile, null)
		txtname = infile.getName();
		int idx = txtname.lastIndexOf(".")
		if (idx > 0) txtname = txtname.substring(0, idx);
	}
	
	/**
	 * Instantiates a new transcriber tokenizer.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 */
	public TranscriberTokenizer(File infile, File outfile, String lang) {
		super(infile, outfile, lang)
		txtname = infile.getName();
		int idx = txtname.lastIndexOf(".")
		if (idx > 0) txtname = txtname.substring(0, idx);
	}
	
	/* (non-Javadoc)
	 * @see filters.Tokeniser.SimpleTokenizerXml#processWord()
	 */
	protected void processWord()
	{
		//println " chars: "+parser.getText().trim();
		String text = buffer.toString().trim().replace("\t", " ");
		text = text.replaceAll("\\p{C}", "");
		String previousChar = "";
		for (String s : text.split("[\\p{Z}\\p{C}]+") )	{
			//init transcriber attributes
			if (troncature)
				audio = "absent";
			else
				audio = "present"
			notation = s;
			event = "";
			
			//TODO does not work (eg ' "word" '). This step should be done after the tokenizer step is done
			//			if (s.startsWith("\"") && s.endsWith("\"")) {
			//				// not rapp1 or rapp2
			//			} else if (s.startsWith("\"")) {
			//				rapp = true;
			//				event += "#rapp1";
			//			} else if(s.endsWith("\"")) {
			//				rapp = false;
			//				event += "#rapp2";
			//			}
			
			//test events
			if (s.startsWith("^^") && s.length() > 2) {
				event += "#orth";
				s = s.substring(2);
			}
			if (s.startsWith("*") && s.length() > 1) {
				event += "#corr";
				s = s.substring(1);
			}
			
			if (event.length() > 0)
				event = event.substring(1);//remove '|'
			
			// test audio
			if ((s.contains("(") || s.contains(")")) && s.length() > 2) // contains ( or )
			{
				if (s.contains("(") ^ s.contains(")")) // ert(ert XOR ert)ert
				{
					audio = "partiel"
					
					if (s.contains("(")) // ert(ert
					{
						troncature = true;
						if (s.startsWith("(")) // (ertert
						{
							audio = "absent";
						}
					} else // ert)ert
					{
						troncature = false;
						if (s.endsWith(")")) // ertert)
						{
							audio = "absent";
						}
					}
				}
				else if (s.contains("(") && s.contains(")")) // ert(ert)ert OR ert)ert(ert
				{
					audio = "partiel";
					if (s.startsWith("(") && s.endsWith(")")) // (ertert)
					{
						audio = "absent";
						troncature = false;
					} else if (s.indexOf("(") < s.indexOf(")")) // ert(ert)ert
					{
						troncature = false;
					} else // ert)ert(ert
					{
						troncature = true;
					}
				}
				s = s.replace("(","");
				s = s.replace(")","");
			}
			
			iterate(s);
		}
	}
	
	/**
	 * Iterate over the String to tokenize it and write words using the 'audio', 'event' and "notation' properties.
	 *
	 * @param s the string to tokenize
	 * @return the java.lang. object
	 */
	protected iterate(String s)
	{
		def sentences = stringTokenizer.processText(s);
		for (def words : sentences) {
			for (def word : words) {
				wordcount++;
				writer.writeStartElement(word_element_to_create);
				writeWordAttributes(false);// id
				writer.writeCharacters(word);
				writer.writeEndElement();
				writer.writeCharacters("\n");
			}
			if (stringTokenizer.doSentences())  {
				writer.writeProcessingInstruction("txm", "</s>")
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see filters.Tokeniser.SimpleTokenizerXml#writeWordAttributes()
	 */
	protected writeWordAttributes(boolean fromParser) {
		super.writeWordAttributes(fromParser)
		
		if (!retokenizedWordProperties.containsKey("audio")) writer.writeAttribute("audio", audio);
		if (!retokenizedWordProperties.containsKey("event")) writer.writeAttribute("event", event);
		if (!retokenizedWordProperties.containsKey("notation")) writer.writeAttribute("notation", notation);
	}
}
