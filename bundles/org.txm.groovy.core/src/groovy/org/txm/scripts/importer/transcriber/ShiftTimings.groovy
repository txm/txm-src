// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS  FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2011-11-01 16:12:36 +0100 (mar., 01 nov. 2011) $
// $LastChangedRevision: 2049 $
// $LastChangedBy: sheiden $
//

package org.txm.scripts.importer.transcriber
import java.text.DecimalFormat;
import groovy.xml.XmlParser
// parameters

String userdir = System.getProperty("user.home")
File infile = new File(userdir, "xml/minitranscriber/int01.trs")
File outfile = new File(userdir, "xml/minitranscriber/int01-shifted.trs")
float shift = -0.4;
formater = DecimalFormat.getInstance(Locale.ENGLISH)

def shiftTime(element, timeProperties, float shift) // end < start
{
	//println "shift "+element.attributes()+" "+timeProperties
	for (def p : timeProperties) {
		
		def timeS = element.attributes()[p]
		def time =  Float.parseFloat(timeS)
		
		if (time == 0.0f) {
			continue;
		}
		
		def newTime = time + shift
		if (newTime < 0) newTime = 0.0f;
		
		element.attributes()[p] = formater.format(newTime);
	}
	
	return true
}

public def shiftTRS(File infile, File outfile, float shift) {
	def timeResolution = 0.001
	URL u = infile.toURI().toURL()
	InputStream ins = u.openStream()

	// Open input file
	def slurper = new XmlParser();
	slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
	def trs = slurper.parse(infile.toURI().toString())

	// Then fix all <Sync>s of Turns
	for (def section : trs.Episode.Section) {
		
		if (!shiftTime(section, ["startTime", "endTime"], shift)) {
			println "Shift Section error: "+section.attributes()
			return false;
		}
		
		section.Turn.each{ turn ->
			
			if (!shiftTime(turn, ["startTime", "endTime"], shift)) {
				println "Shift Turn error: "+turn.attributes()
				return false;
			}
			
			turn.Sync.each(){ sync ->
				if (!shiftTime(sync, ["time"], shift)) {
					println "Shift Sync error: "+sync.attributes()
					return false;
				}
			}
		}
	}

	String xml = "";
	println ""+xml
	outfile.withWriter("UTF-8"){ writer ->
		writer.write('<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE Trans SYSTEM "trans-14.dtd">\n')
		new groovy.xml.XmlNodePrinter(new PrintWriter(writer)).print(trs) }
}

/// MAIN ///
shiftTRS(infile, outfile, shift);