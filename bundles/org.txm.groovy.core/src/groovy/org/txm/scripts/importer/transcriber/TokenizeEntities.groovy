package org.txm.scripts.importer.transcriber

import org.txm.importer.StaxIdentityParser


/**
 * Wrap  "entities" span event in "w" tags 
 * 
 * @author mdecorde
 *
 */
class TokenizeEntities extends StaxIdentityParser{
	int wordcount = 1;
	boolean writeStartWord = false;
	boolean writeEndWord = false;
	public TokenizeEntities(URL url) {
		super(url);
	}
	
	public void processStartElement() {
		// close previous word
		if (localname == "Event" && parser.getAttributeValue(null, "type") == "entities") {
			if (parser.getAttributeValue(null, "extent") == "end") { // end of word
				writer.writeEndElement();
				writeEndWord = false;
			}
		}
		
		super.processStartElement();
		
		// start new word if necessary
		if (localname == "Event" && parser.getAttributeValue(null, "type") == "entities") {
			if (parser.getAttributeValue(null, "extent") == "begin") { // start of word
				writeStartWord = true;
			}
		}
	}
	
	public void processEndElement() {

		super.processEndElement();
		
		if (writeStartWord) {
			writer.writeStartElement("w");
			//writer.writeAttribute("id", "w_entity_"+(wordcount++));
			writeStartWord = false;
		} 
	}
	
	public static void main(String[] args) {
		File dir = new File ("C:\\Documents and Settings\\mdecorde\\xml\\minileman2011")
		File infile = new File(dir, "int05.trs")
		TokenizeEntities tokenizer = new TokenizeEntities(infile.toURI().toURL());
		println tokenizer.process(new File(dir, "out.trs")) 
	}
}
