// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.fleurs;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

// TODO: Auto-generated Javadoc
/** Build Discours corpus simple edition from a xml-tei. @author mdecorde */
class BuildEdition {
	int wordcount;
	
	/** The No space before. */
	List<String> NoSpaceBefore;
	
	/** The No space after. */
	List<String> NoSpaceAfter;
	
	/** The wordid. */
	String wordid;
	
	/** The wordvalue. */
	String wordvalue;
	
	/** The interpvalue. */
	String interpvalue;
	
	/** The flaginterp. */
	boolean flaginterp = false;
	
	/** The lastword. */
	String lastword = " ";
	
	/** The wordtype. */
	String wordtype;
	
	/** The flagform. */
	boolean flagform = false;
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The writer. */
	def writer;

	/**
	 * Instantiates a new builds the edition.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 */
	BuildEdition(File infile, File outfile, List<String> NoSpaceBefore,
			List<String> NoSpaceAfter) {
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		createOutput(outfile);
		process();
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			writer = new OutputStreamWriter(new FileOutputStream(outfile),
					"UTF-8");

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Process.
	 */
	void process()
	{
		String localname = "";
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			
			
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{
						case "teiHeader":
						if(parser.getAttributeValue(null,"type").equals("text"))
						{
							writer.write("<h1 class=\"text\">Discours</h1>")
						}
						else if(parser.getAttributeValue(null,"type").equals("corpus"))
						{
							writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
							writer.write("<html>");
							writer.write("<head>");
							writer.write("<title>Discours edition</title>");
							writer.write("</head>");
							writer.write("<body>");
						}
						break;
						
						case "text":
							writer.write("<ul>")
							writer.write("<li>type : "+parser.getAttributeValue(null,"type")+"</li>")
							writer.write("<li>locuteur : "+parser.getAttributeValue(null,"loc")+"</li>")
							writer.write("<li>date : "+parser.getAttributeValue(null,"date")+"</li>")
							writer.write("</ul>")
						
						break;
						case "p":
							writer.write("<p>");
						break;
						
						case "w":
						wordid=parser.getAttributeValue(null,"id");
						wordcount++;
						break;
						
						case "ana":
						flaginterp=true;
						interpvalue+=" "+parser.getAttributeValue(null,"type")+":"
						break;
						
						case "form":
						wordvalue="";
						interpvalue ="";
						flagform=true;
					}
				break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{
						case "teiHeader":

						break;
						
						case "p":
							writer.write("</p>");
						break;
						
						case "form":
						flagform = false
						
						break;
						
						case "ana":
						flaginterp = false
						break;
						
						case "w":
						int l = lastword.length();
						interpvalue = interpvalue.replace("\"","&quot;");
						if(NoSpaceBefore.contains(wordvalue))
						writer.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
						else if(NoSpaceAfter.contains(lastword.substring(l-1, l)))
						writer.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
						else if(wordvalue.startsWith("-") && wordvalue.length() > 0)
						writer.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
						else
						writer.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
						writer.write(wordvalue);
						writer.write("</span>");
						lastword=wordvalue;
						break;
					}
				break;
				
				case XMLStreamConstants.CHARACTERS:
					if(flagform)
						if(parser.getText().length() > 0)
							wordvalue+=(parser.getText());
					if(flaginterp)
						if(parser.getText().length() > 0)
							interpvalue+=(parser.getText());
				break;
			}
		}	
		writer.write("</body>");
		writer.write("</html>");
		inputData.close();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String rootDir = "~/.txm/cwb/data/discours/src/";
		new File(rootDir+"/identity/").mkdir();
		
		File srcfile = new File(rootDir,"discours-p5.xml");
		File resultfile = new File(rootDir,"discours-p5.html");
		List<String> NoSpaceBefore = [",",".",")","]","}"];
		List<String> NoSpaceAfter = ["'","(","[","{"];
		println("build discours xml-tei file : "+srcfile+" to : "+resultfile );
		
		def builder = new BuildEdition(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter);
		
		return;
	}
}
