// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2015-06-03 15:04:53 +0200 (mer. 03 juin 2015) $
// $LastChangedRevision: 2984 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.hyperbase

//import org.txm.scripts.filters.TabulatedToXml.*;
import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.scripts.xmltxm.*;
//import org.txm.utils.treetagger.TreeTagger;
import javax.xml.stream.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URL;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.FusionHeader.*;
import org.txm.scripts.filters.TagSentences.*;

import org.txm.*;
import org.txm.core.engines.*;
import org.txm.utils.*;
import org.txm.tokenizer.TokenizerClasses
/**
 * The Class importer.
 */
class importer {
	
	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param encoding the encoding
	 * @param basename the basename
	 * @return true, if successful
	 */
	public static boolean run(File srcDir, File binDir, File txmDir, String encoding, String basename, String lang, def project)
	{
		String rootDir = srcDir.getAbsolutePath()+"/"
		//cleaning
		
		new File(binDir,"ptokenized").deleteDir();
		new File(binDir,"ptokenized").mkdir();
		new File(binDir,"tokenized").deleteDir();
		new File(binDir,"tokenized").mkdir();
		new File(binDir,"split").deleteDir();
		new File(binDir,"split").mkdir();
		
		ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		

		File corpusfile = new File(srcDir, srcDir.getName());
		if (!corpusfile.exists()) {
			for (File f : FileUtils.listFiles(srcDir)) {
				corpusfile = f;
				break;
			}
		}
		
		//build xml files
		if (!corpusfile.exists()) {
			println "No corpus file ($corpusfile) in: "+srcDir;
			return false;
		}
		
		System.out.println("Corpus file: "+corpusfile);
		if (!new Hyperbase2Xml().run(corpusfile, new File(binDir, "split"), encoding)) {
			println("Failed to split the corpus file into text: "+corpusfile);
			return false;
		}
		
		//TOKENIZE ALL FILES
		List<File> srcfiles = FileUtils.listFiles(new File(binDir, "split"))
		println("Tokenizing "+srcfiles.size()+" files");
		for (File pfile : srcfiles) {
			print "."
			String filename = pfile.getName().substring(0, pfile.getName().length()-4)
			File tfile = new File(binDir, "tokenized/"+filename+".xml");
			
			SimpleTokenizerXml tokenizer = new SimpleTokenizerXml(pfile, tfile, TokenizerClasses.newTokenizerClasses(project.getPreferencesScope(), lang));
			if (project.getAnnotate()) { // an annotation will be done, does the annotation engine needs another tokenizer ?
				String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
				def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
				def stringTokenizer = engine.getStringTokenizer(lang)
				if (stringTokenizer != null) {
					tokenizer.setStringTokenizer(stringTokenizer)
				}
			}
			if (!tokenizer.process()) {
				println("Failed to tokenize file: "+pfile);
				tfile.delete();
			}
		}
		println ""
		
		//Tag sentences
		List<File> stokenfiles = FileUtils.listFiles(new File(binDir, "tokenized"))
		new File(binDir,"stokenized").mkdir();
		println("Tag sentences of "+stokenfiles.size()+" files")
		for (File f : stokenfiles) {
			print "."
			try {
				Sequence S = new Sequence();
				Filter F1 = new CutHeader();
				Filter F7 = new TagSentences();
				Filter F11 = new FusionHeader();
				S.add(F1);
				S.add(F7);
				S.add(F11);
				File infile = new File(binDir, "tokenized/"+f.getName());
				File xmlfile = new File(binDir, "stokenized/"+f.getName());
				File headerfile = new File(binDir, "tokenized/"+f.getName()+"header.xml");
				
				S.SetInFileAndOutFile(infile.getPath(), xmlfile.getPath());
				S.setEncodages("UTF-8","UTF-8");
				Object[] arguments1 = [headerfile.getAbsolutePath()];
				F1.SetUsedParam(arguments1);
				Object[] arguments2 = [headerfile.getAbsolutePath(),F1];
				F11.SetUsedParam(arguments2);
				S.proceed();
				S.clean();
				headerfile.delete();//remove the prepared file to clean
			} catch (Exception e) {println "Failed to sentence file "+f;println e;}
		}
		println ""
		
		//TRANSFORM INTO XML-TEI-TXM
		List<File> tokenfiles = FileUtils.listFiles(new File(binDir, "stokenized"))
		println("Building xml-txm ("+tokenfiles.size()+" files)")
		for (File f : tokenfiles) {
			print "."
			File file = f;
			String txmfile = f.getName();
			
			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()
			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>();
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
			//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();
			//lance le traitement
			def builder = new Xml2Ana(file);
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			//builder.setAddTEIHeader();
			if (!builder.process(new File(txmDir,txmfile))) {
				println("Failed to process "+f);
				new File(txmDir, txmfile).delete();
			}
		}
		println ""
		return true;
	}
}
