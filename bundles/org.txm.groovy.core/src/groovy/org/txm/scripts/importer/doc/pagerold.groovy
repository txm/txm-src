// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2012-04-24 14:30:47 +0200 (mar., 24 avr. 2012) $
// $LastChangedRevision: 2174 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.doc;

import org.txm.scripts.importer.*
import org.xml.sax.Attributes
import org.txm.importer.scripts.filters.*
import java.io.File
import java.io.IOException
import java.util.ArrayList
import javax.xml.parsers.SAXParserFactory
import javax.xml.parsers.ParserConfigurationException
import javax.xml.parsers.SAXParser
import javax.xml.stream.*
import java.net.URL
import org.xml.sax.InputSource
import org.xml.sax.helpers.DefaultHandler
import org.txm.objects.Project

/** 
 * Build edition from xml-tei-odt
 * 
 * @author mdecorde
 * 
 */
class pager {
	
	List<String> NoSpaceBefore;

	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;

	/** The pagecount. */
	int pagecount = 0;

	/** The wordmax. */
	int wordmax = 0;

	/** The basename. */
	String basename = "";
	String txtname = "";
	File outdir;

	/** The wordid. */
	String wordid;

	/** The first word. */
	boolean firstWord = true
	
	boolean paginate = true

	/** The wordvalue. */
	String wordvalue;

	/** The interpvalue. */
	String interpvalue;

	/** The lastword. */
	String lastword = " "

	/** The wordtype. */
	String wordtype;

	/** The flagform. */
	boolean flagform = false

	/** The flaginterp. */
	boolean flaginterp = false

	/** The url. */
	private def url

	/** The input data. */
	private def inputData

	/** The factory. */
	private def factory

	/** The parser. */
	private XMLStreamReader parser

	/** The writer. */
	OutputStreamWriter writer

	/** The multiwriter. */
	OutputStreamWriter multiwriter = null

	/** The pagedWriter. */
	StaxStackWriter pagedWriter = null

	/** The infile. */
	File infile

	/** The outfile. */
	File outfile

	/** The pages. */
	ArrayList<File> pages = new ArrayList<File>()

	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>()
	String editionPage

	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 * @param basename the basename
	 */
	pager(File infile, File outdir, String txtname, List<String> NoSpaceBefore,
	List<String> NoSpaceAfter, String basename, String editionPage, Project project) {
		this.editionPage = editionPage;
		this.basename = basename;
		this.txtname = txtname;
		this.outdir = outdir;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();
		this.infile = infile;
		this.wordmax = project.getEditionDefinition("default").getWordsPerPage();
		this.paginate = project.getEditionDefinition("default").getPaginateEdition()

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		process();
	}

	private def closeMultiWriter() {
		if (pagedWriter != null) {
			def tags = pagedWriter.getTagStack().clone();
			if (firstWord) { // there was no words
				this.idxstart.add("w_0")
				pagedWriter.write("<span id=\"w_0\"/>");
			}

			pagedWriter.writeEndElements();
			pagedWriter.close();
			return tags;

			//			pagedWriter.write("</body>");
			//			pagedWriter.write("</html>");
			//			pagedWriter.close();
		} else {
			return [];
		}
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput() {
		wordcount = 0;
		try {
			def tags = closeMultiWriter()
			for (int i = 0 ; i < tags.size() ; i++) {
				String tag = tags[i]
				if ("body" != tag) {
					tags.remove(i--)
				} else {
					tags.remove(i--) // remove "body"
					break; // remove elements until "body tag
				}
			}
			File outfile = new File(outdir, txtname+"_"+(++pagecount)+".html")
			pages.add(outfile)
			firstWord = true // waiting for next word

			pagedWriter = new StaxStackWriter(outfile , "UTF-8");
			pagedWriter.writeStartDocument("UTF-8", "1.0")
			pagedWriter.writeStartElement("html")
			pagedWriter.writeEmptyElement("meta", ["http-equiv":"Content-Type", "content":"text/html","charset":"UTF-8"])
			pagedWriter.writeStartElement("head")
			pagedWriter.writeEmptyElement("link", ["rel":"stylesheet", "href":txtname+".css"])
			pagedWriter.writeEmptyElement("link", ["rel":"stylesheet", "href":"doc.css"])
			pagedWriter.writeStartElement("title")
			pagedWriter.writeCharacters(basename.toUpperCase()+" Edition - Page "+pagecount)
			pagedWriter.writeEndElement() // </title>
			pagedWriter.writeEndElement() // </head>
			pagedWriter.writeStartElement("body") //<body>
			pagedWriter.writeStartElements(tags)
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput() {
		try {
			return createNextOutput();
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	void gotoText() {
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.END_ELEMENT:
					if(parser.getLocalName() == "teiHeader")
						return;
			}
		}
	}
	/**
	 * Process.
	 */
	void process() {
		try {
			String localname = "";
			String figurerend = ""
			boolean unordered = false;
			createNextOutput();

			gotoText();
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();

						if (localname == editionPage) {
							if (paginate) createNextOutput();
							
							wordcount=0;
							if (parser.getAttributeValue(null,"n") != null) {
								pagedWriter.writeStartElement("p", ["style":"color:red", "align":"center"])
								pagedWriter.writeCharacters("- "+parser.getAttributeValue(null,"n")+" -")
								pagedWriter.writeEndElement() // p
							}
						}

						switch (localname) {
							case "textunit":
								if (parser.getAttributeValue(null,"id") != null) {
									pagedWriter.writeStartElement("h3")
									pagedWriter.writeCharacters(parser.getAttributeValue(null,"id"))
									pagedWriter.writeEndElement() // h3
								}
								pagedWriter.writeStartElement("table")
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									pagedWriter.writeStartElement("tr")
									pagedWriter.writeStartElement("td")
									pagedWriter.writeCharacters(parser.getAttributeLocalName(i))
									pagedWriter.writeEndElement() // td
									pagedWriter.writeStartElement("td")
									pagedWriter.writeCharacters(parser.getAttributeValue(i))
									pagedWriter.writeEndElement() // td
									pagedWriter.writeEndElement() // tr
								}
								pagedWriter.writeEndElement() // table
								pagedWriter.writeEmptyElement("br")
								break;
							case "head":
								String rend = parser.getAttributeValue(null, "rend")
								if (rend == null) rend = "normal"
								pagedWriter.writeStartElement("h2", ["class":rend])
								break;
							case "figure":
								figurerend = parser.getAttributeValue(null, "rend")
								break;
							case "graphic":
								String url = parser.getAttributeValue(null, "url")
								if (url.startsWith("..//")) url = url.substring(4);
								else if (url.startsWith("../")) url = url.substring(3);

								if (url != null) {
									pagedWriter.writeStartElement("div")
									pagedWriter.writeEmptyElement("img", ["class":figurerend, "src":url])
									pagedWriter.writeEndElement() // div
								}
								figurerend = "";
								break;
							case "lg":
							case "p":
							case "q":
								String rend = parser.getAttributeValue(null, "rend")
								if (rend == null) rend = "normal"
								pagedWriter.writeStartElement("p", ["class":rend])
								break;
							//case "pb":
							case "table":
								pagedWriter.writeStartElement("table")
								pagedWriter.writeAttribute("class", parser.getAttributeValue(null, "rend"))
								break;
							case "row":
								pagedWriter.writeStartElement("tr")
								break;
							case "cell":
								pagedWriter.writeStartElement("td")
								break;
							case "lb":
							case "br":
								pagedWriter.writeEmptyElement("br")
								break;
							case "list":
								String type = parser.getAttributeValue(null,"type");
								if ("unordered" == type) {
									unordered = true;
									pagedWriter.writeStartElement("ul")
								} else {
									unordered = false
									pagedWriter.writeStartElement("ol")
								}
								break
							case "item":
								pagedWriter.writeStartElement("li")
								break;
							case "hi":
								pagedWriter.writeStartElement("b")
								break;
							case "emph":
								pagedWriter.writeStartElement("i")
								break;
							case "w":
								wordid = parser.getAttributeValue(null,"id");

								if (firstWord) {
									firstWord = false;
									this.idxstart.add(wordid);
								}
								wordcount++;

								break;
							case "ana":
								flaginterp=true;
								interpvalue+=" "+parser.getAttributeValue(null,"type").substring(1)+":"
								break;

							case "form":
								wordvalue="";
								interpvalue ="";
								flagform=true;
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						switch(localname)
						{
							case "head":
								pagedWriter.writeEndElement(); // h2
								pagedWriter.writeCharacters("\t")
								break;
							case "lg":
							case "p":
							case "q":
								pagedWriter.writeEndElement(); // p
								pagedWriter.writeCharacters("\t")
								if (paginate && wordcount >= wordmax) {
									createNextOutput();
								}
								break;
								
							case "list":
								pagedWriter.writeEndElement(); // ul or ol
								pagedWriter.writeCharacters("\t")
								break
							case "item":
								pagedWriter.writeEndElement(); // li
								pagedWriter.writeCharacters("\t")
								break;
							case "hi":
								pagedWriter.writeEndElement(); // b
								break;
							case "emph":
								pagedWriter.writeEndElement(); // i
								break;
							case "table":
								pagedWriter.writeEndElement(); // table
								pagedWriter.writeCharacters("\t")
								break;
							case "row":
								pagedWriter.writeEndElement(); // tr
								pagedWriter.writeCharacters("\t")
								break;
							case "cell":
								pagedWriter.writeEndElement(); // td
								break;
							case "form":
								flagform = false
								break;
							case "ana":
								flaginterp = false
								break;
							case "w":
								int l = lastword.length();
								String endOfLastWord = "";
								if (l > 0)
									endOfLastWord = lastword.subSequence(l-1, l);

								if (interpvalue != null)
									interpvalue = interpvalue;

								if (NoSpaceBefore.contains(wordvalue) ||
								NoSpaceAfter.contains(lastword) ||
								wordvalue.startsWith("-") ||
								NoSpaceAfter.contains(endOfLastWord))
								{
									// multiwriter.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
								} else {
									// multiwriter.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
									pagedWriter.writeCharacters(" ");
								}
								pagedWriter.writeStartElement("span", ["title":interpvalue, "id":wordid]);
								pagedWriter.writeCharacters(wordvalue);
								pagedWriter.writeEndElement() // span
								lastword=wordvalue;
								break;
						}
						break;
					case XMLStreamConstants.CHARACTERS:
						if (flagform)
							if (parser.getText().length() > 0)
								wordvalue+=(parser.getText());
						if (flaginterp)
							if (parser.getText().length() > 0)
								interpvalue+=(parser.getText());
						break;
				}
			}
			//writer.write("</body>");
			//writer.write("</html>");
			//writer.close();
			closeMultiWriter();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
		} catch(Exception e) { 
			println "Error while processing ${infile} at "+parser.getLocation().getLineNumber()+": "+e
			org.txm.utils.logger.Log.printStackTrace(e); 
			pagedWriter.close();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
		 }
	}
}
