

// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2012-01-05 14:27:34 +0100 (jeu., 05 janv. 2012) $
// $LastChangedRevision: 2096 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.cqp

import org.txm.Toolbox;
import org.txm.importer.cwb.*
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.objects.*;
import org.txm.searchengine.cqp.corpus.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.FileUtils
import org.txm.utils.io.FileCopy;
import org.txm.utils.treetagger.TreeTagger;
import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * The Class compiler.
 */
class compiler {
	/** The debug. */
	boolean debug = false;

	/** The dir. */
	private def dir;

	File srcCQPFile, srcRegistryFile;

	public def pAttributesList = [];
	public def sAttributesList = [];

	public compiler(File cqpFile, File registryFile) {
		this.srcCQPFile = cqpFile;
		this.srcRegistryFile = registryFile
	}

	/**
	 * Sets the debug.
	 *
	 * @return the java.lang. object
	 */
	public setDebug()
	{
		debug =true;
	}

	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(Project project, File binDir, String corpusname)
	{
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		CorpusBuild corpus = project.getCorpusBuild(project.getName(), MainCorpus.class);
		if (corpus != null) {
			if (project.getDoUpdate()) {
				corpus.clean(); // remove old files
			} else {
				corpus.delete(); // remove old files and TXMResult children
			}
		} else {
			corpus = new MainCorpus(project);
			corpus.setID(project.getName());
			corpus.setName(project.getName());
		}
		corpus.setDescription("Built with the CQP import module");
		
		File cqpFile = new File(binDir,"cqp/"+corpusname+".cqp");
cqpFile.delete()
		new File(binDir,"cqp").mkdirs()
		new File(binDir,"data").mkdirs()
		new File(binDir,"registry").mkdirs()

		FileCopy.copy(srcCQPFile, cqpFile);

		//2- Import into CWB
		def outDir = binDir.getAbsolutePath()+"/";

		CwbEncode cwbEn = new CwbEncode();
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbEn.setDebug(debug);
		cwbMa.setDebug(debug);

		String[] pAttributes;
		String[] sAttributes;

		if (srcRegistryFile == null) {
			println "WARNING: No registry file found in source directory"
			println "We'll search for positional attributes and structural attributes in the CQP file"

			// s attributes
			BuildCwbEncodeArgsFromCQP argsgetter = new BuildCwbEncodeArgsFromCQP(); // XML stream
			argsgetter.process(cqpFile); //$NON-NLS-1$
			sAttributesList = argsgetter.getSAttributes();

			// p attributes
			int nbAttr = -1;
			File tmp = File.createTempFile("txm", ".cqp", cqpFile.getParentFile());
			int wcounter = 1;
			println "Adding the 'id' property to the CQP file and getting word properties number."
			tmp.withWriter("UTF-8") { writer ->
				cqpFile.eachLine("UTF-8") { line ->
					if (!line.startsWith("<")) {
						if (nbAttr == -1) nbAttr = line.split("\t").size();
						writer.println(line+"\tw_"+(wcounter++))
					} else {
						writer.println(line)
					}
					writer.flush();
				}
			}
			cqpFile.delete()
			tmp.renameTo(cqpFile)

			System.out.println("Found "+(nbAttr-1)+" word properties, $nbAttr with the 'id'");
			for (int i = 1; i < nbAttr ; i++) {
				pAttributesList << "p$i";
			}
			pAttributesList << "id"
		} else {
			ReadRegistryFile reader = new ReadRegistryFile(srcRegistryFile);
			pAttributesList = reader.getPAttributes();
			sAttributesList = reader.getSAttributes();
			pAttributesList.remove(0) // remove word

			if (!pAttributesList.contains("id")) {
				System.out.println("Error: The registry file does not declare the 'id' word property");
				return false;
			}
		}

		pAttributes = pAttributesList; // cast to array
		sAttributes = sAttributesList; // cast to array

		println "pAttrs : "+Arrays.toString(pAttributes)
		println "sAttrs : "+Arrays.toString(sAttributes)

		try {
			cwbEn.setDebug(debug);
			cwbMa.setDebug(debug);
			String regPath =outDir + "/registry/"+corpusname.toLowerCase()
			cwbEn.run(outDir + "/data/$corpusname", 
					cqpFile.getAbsolutePath(),
					regPath, pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(corpusname, outDir + "/registry");
			return true;
		} catch (Exception ex) {System.out.println(ex); return false;}
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "cqp").deleteDir()
		}
		
		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/perrault/txm/");
		List<File> files = FileUtils.listFiles(dir);
		new compiler().run(files);
	}
}
