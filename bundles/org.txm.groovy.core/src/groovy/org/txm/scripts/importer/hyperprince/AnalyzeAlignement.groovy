// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2015-12-17 12:11:39 +0100 (jeu. 17 déc. 2015) $
// $LastChangedRevision: 3087 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.hyperprince

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.stream.*;

// TODO: Auto-generated Javadoc
/**
 * The Class AnalyzeAlignement.
 */
class AnalyzeAlignement {
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/**
	 * Instantiates a new analyze alignement.
	 *
	 * @param file the file
	 */
	public AnalyzeAlignement(File file){
		try {
			this.url = file.toURI().toURL();
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			factory.setProperty(XMLInputFactory.IS_VALIDATING,false)
			parser = factory.createXMLStreamReader(inputData);
			
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * Process.
	 *
	 * @return the list
	 */
	public List <Set <String>> process(){
		List <Set <String>> list = new ArrayList <Set <String>>();
		
		String localname = "";
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			
			String prefix = parser.getPrefix();
			if(prefix == null || prefix == "")
				prefix = "";
			else
				prefix +=":";
			
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
				
				// <link targets="#i1_Pr-Seg0 #f1_Pr-Seg0 #f3_Pr-Seg0 #f5_Pr-Seg0 #f4_Pr-Seg0 "/>
					if(localname == "link"){
						for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
						{
							String att = parser.getAttributeLocalName(i)
							if(att == "target"){
								List<String> vals = parser.getAttributeValue(i).split(" ");
								HashSet<String> newVals = new HashSet<String>();
								//vals*.trim(); dans le cas du #
								vals.each{ it->
									newVals << it.substring(1);
								}
								list.add(newVals);
							}
						}
					}
			}
		}
		parser.close()
		return list;
	}
	
	/**
	 * Adds the alignement.
	 *
	 * @param file the file
	 * @param alignIds the align ids
	 * @return the file
	 */
	public File addAlignement(File file, HashSet<String> alignIds){
		try {
			this.url = file.toURI().toURL();
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			factory.setProperty(XMLInputFactory.IS_VALIDATING,false)
			parser = factory.createXMLStreamReader(inputData);
			
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
		
		/*
		 File outfile = new File(outdir,textname+language+".xml");
		 Writer output = new OutputStreamWriter(new FileOutputStream(outfile) , "UTF-8");
		 def writer = factory.createXMLStreamWriter(output, "UTF-8")*/
		String textname = file.getName().substring(0,file.getName().length()-4)+"_";
		
		// create XML writer
		File tempFile = new File(file.getParent(),textname+"temp.xml");
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		FileOutputStream output = new FileOutputStream(tempFile)
		def writer = factory.createXMLStreamWriter(output, "UTF-8")
		
		
		String localname = "";
		writer.writeStartDocument("UTF-8","1.0");
		writer.writeStartElement ("TEI");
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			
			String prefix = parser.getPrefix();
			if(prefix == null || prefix == "")
				prefix = "";
			else
				prefix +=":";
			
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					if(prefix == null){
						writer.writeStartElement(prefix, localname);
					}else {
						writer.writeStartElement(localname);
					}
					for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
					{
						writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i))
						if("id" == parser.getAttributeLocalName(i)){
							if((parser.getAttributeValue(i)).startsWith("w")){
							}else {
								String idAlign = searchForAlignId(parser.getAttributeValue(i), alignIds);
								writer.writeAttribute("pid", idAlign);
								if(idAlign == "-1"){
									System.out.println(parser.getAttributeValue(i)+" - ALIGN ID is ? "+idAlign);
								}
							}
						}
					}
				
					break;
				case XMLStreamConstants.END_ELEMENT:
					writer.writeEndElement();
				
					break;
				case XMLStreamConstants.CHARACTERS:
					writer.writeCharacters(parser.getText());
					break;
			}
		}
		writer.writeEndElement(); // close TEI
		parser.close();
		writer.close();
		output.close()
		inputData.close();
		return copyfile(tempFile.getAbsolutePath(), file.getAbsolutePath());

	}
	
	/**
	 * Copyfile.
	 *
	 * @param srFile the sr file
	 * @param dtFile the dt file
	 * @return the file
	 */
	private static File copyfile(String srFile, String dtFile){
		File f2 ;
		try{
			File f1 = new File(srFile);
			f2 = new File(dtFile);
			InputStream inStr = new FileInputStream(f1);
			
			//For Append the file.
			//OutputStream out = new FileOutputStream(f2,true);
			
			//For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);
			
			byte[] buf = new byte[1024];
			int len;
			while ((len = inStr.read(buf)) > 0){
				out.write(buf, 0, len);
			}
			inStr.close();
			out.close();
			System.out.println("File copied.");
		}
		catch(FileNotFoundException ex){
			System.out.println(ex.getMessage() + " in the specified directory.");
			return null;
		}
		catch(IOException e){
			System.out.println(e.getMessage());      
		}
		return f2;
	}
	
	/**
	 * Search for align id.
	 *
	 * @param id the id
	 * @param alignIds the align ids
	 * @return the string
	 */
	public String searchForAlignId(String id, HashSet<String> alignIds){
		int i = 0;
		for(Set<String> set: alignIds){
			i++;
			if(set.contains(id)){
				return ""+i;
			}
		}
		return "-1";
	}
	
	
}