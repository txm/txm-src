package org.txm.scripts.importer.xtz;

import java.io.File;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.*;

import org.txm.scripts.importer.StaxStackWriter;
import org.eclipse.ui.part.PageSwitcher
import org.txm.core.preferences.TBXPreferences
import org.txm.importer.xtz.*

public class XTZDefaultPagerStep {

	List<String> NoSpaceBefore;

	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;

	/** The pagecount. */
	int pagecount = 0;

	/** The wordmax. */
	int wordmax = 0;

	/** The basename. */
	String basename = "";
	String txtname = "";
	File outdir;

	/** The wordid. */
	String wordid;

	/** The first word. */
	boolean firstWord = true;

	boolean enableCollapsibles = false;

	/** The wordvalue. */
	String wordvalue = "";

	/** The interpvalue. */
	String interpvalue = "";

	/** The previouswordValue. */
	String previouswordValue = " ";

	/** The wordtype. */
	String wordtype;

	/** The flagform. */
	boolean flagform = false;

	/** The flaginterp. */
	boolean flaginterp = false;

	/** The url. */
	private def url;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The writer. */
	OutputStreamWriter writer;

	/** The pagedWriter. */
	StaxStackWriter pagedWriter = null;

	/** The infile. */
	File infile;

	/** The outfile. */
	File outfile;

	/** The pages. */
	//TODO enhance this to store the page name/id as well
	ArrayList<File> pages = new ArrayList<File>();

	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>();
	String paginationElement;
	boolean paginate;
	def cssList;
	def wordTag = "w";
	def noteElements = new HashSet<String>();
	def outOfTextElements = new HashSet<String>();
	XTZPager pager;

	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 * @param basename the basename
	 */
	public XTZDefaultPagerStep(XTZPager pager, File infile, String txtname, List<String> NoSpaceBefore, List<String> NoSpaceAfter, def cssList) {
		this.pager = pager;
		this.paginationElement = pager.page_element;
		this.paginate = pager.paginate
		this.cssList = cssList;
		this.basename = pager.corpusname;
		this.txtname = txtname;
		this.outdir = pager.outputDirectory;
		this.wordmax = pager.wordsPerPage;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();
		this.infile = infile;
		this.wordTag= pager.wordTag;
		outdir.mkdirs()
		this.enableCollapsibles = pager.getImportModule().getProject().getEditionDefinition("default").getEnableCollapsibleMetadata();

		inputData = new BufferedInputStream(url.openStream());
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);

		String notesListString = pager.getImportModule().getProject().getTextualPlan("Note")
		if (notesListString != null) for (def s : notesListString.split(",")) noteElements << s;

		String elems = pager.getImportModule().getProject().getTextualPlan("OutSideTextTagsAndKeepContent")
		if (elems != null) for (def s : elems.split(",")) outOfTextElements << s;

		//process();
	}

	public String getAttributeValue(def parser, String ns, String name) {
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			if (name == parser.getAttributeLocalName(i)) {
				return parser.getAttributeValue(i).toString()
			}
		}
		return null;
	}

	private def closeMultiWriter() {
		if (pagedWriter != null) {
			def tags = []
			tags.addAll(pagedWriter.getTagStack())

			//println "CLOSING... STACK="+pagedWriter.getTagStack()
			// def stack = Thread.currentThread().getStackTrace();
			// int m = Math.min(15, stack.size()-1)
			// for (def s : stack[1..m]) println s
			// println "FILE ="+outfile
			if (firstWord) { // there was no words
				pagedWriter.writeCharacters("")
				this.idxstart.add("w_0")
				pagedWriter.write("<span id=\"w_0\"/>")
			}

			// write notes before closing all tags
			if (notes.size() > 0) {
				pagedWriter.writeStartElement("hr", ["id":"notes", "width":"20%", "align":"left"])
				pagedWriter.writeEndElement() // </hr>
				//pagedWriter.writeStartElement("ol");
				int i = 1;
				for (String note : notes) {
					note = note.trim()
					//pagedWriter.writeStartElement("li");
					pagedWriter.writeStartElement("a", ["href":"#noteref_"+i, "name":"note_"+i])
					pagedWriter.writeStartElement("sup")
					pagedWriter.writeCharacters(""+i)
					pagedWriter.writeEndElement() // </sub>
					pagedWriter.writeEndElement() // </a>

					if (note.startsWith("<") && note.endsWith(">")) {
						pagedWriter.write(note)
					} else {
						pagedWriter.writeCharacters(note)
					}
					pagedWriter.writeEmptyElement("br")
					i++;
				}
				notes.clear()
			}

			pagedWriter.writeEndElements();

			pagedWriter.close();

			//println "STACK TO REWRITE: $tags"
			int removedDiv = 0;
			for (int i = 0 ; i < tags.size() ; i++) {
				def tag = tags.remove(0)
				i--

				if (tag[0] == "div" ) {
					removedDiv++
					if (removedDiv == 2) break; // remove elements until first "div" tag
				}
			}
			// println "STACK TO REWRITE2: $tags"
			return tags;
		} else {
			return [];
		}
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput() {
		wordcount = 0;
		try {
			def tags = closeMultiWriter()

			outfile = new File(outdir, txtname+"_"+(++pagecount)+".html")
			pages.add(outfile)
			firstWord = true; // waiting for next word

			pagedWriter = new StaxStackWriter(outfile, "UTF-8")

			//pagedWriter.writeStartDocument()
			pagedWriter.writeDTD("<!DOCTYPE html>")
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeStartElement("html")

			pagedWriter.writeCharacters("\n\t")
			pagedWriter.writeEmptyElement("meta", ["http-equiv":"Content-Type", "content":"text/html","charset":"UTF-8"])


			for (String css : cssList) {
				pagedWriter.writeCharacters("\t\n")
				pagedWriter.writeEmptyElement("link", ["rel":"stylesheet", "type":"text/css","href":"$css"])
			}

			pagedWriter.writeCharacters("\t\n")
			pagedWriter.writeStartElement("head")
			pagedWriter.writeCharacters("\t\t\n")
			pagedWriter.writeStartElement("title")
			pagedWriter.writeCharacters(basename.toUpperCase()+" Edition - Page "+pagecount)
			pagedWriter.writeEndElement(); // </title>
			pagedWriter.writeCharacters("\t\t\n")
			pagedWriter.writeStartElement("script", ["src":"js/collapsible.js"]);
			pagedWriter.writeEndElement(); // </script>
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeEndElement() // </head>
			pagedWriter.writeCharacters("\t\n")
			pagedWriter.writeStartElement("body") //<body>
			pagedWriter.writeCharacters("\t\t\n")
			pagedWriter.writeStartElement("div", ["class": pager.getImportModule().getProject().getName()]) //<div> of the corpus
			pagedWriter.writeCharacters("\t\t\n")
			pagedWriter.writeStartElement("div", ["class": "txmeditionpage"]) //<div>
			pagedWriter.writeCharacters("\n")

			//			println "NEW HTML: "+outfile
			//			println "TAGS: "+tags
			pagedWriter.writeStartElements(tags)
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage())
			e.printStackTrace()
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput() {
		try {
			return createNextOutput()
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage())
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	/**
	 * Go to text.
	 */
	private void goToText() {

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.END_ELEMENT) {
				if (parser.getLocalName().matches("teiHeader")) {
					return;
				}
			}
		}
	}

	def notes = []
	def currentOutOfTextElements = [] // stack of element with out of text to edit opened element
	def writeOutOfTextToEditText = false
	/**
	 * Process.
	 */
	public boolean process() {

		try {
			def anaValues = [:]
			def anaType = ""
			def anaResp = ""
			def anaValue = new StringBuilder()

			boolean flagNote = false
			boolean flagW = false

			boolean allTags = true
			boolean ignoreUnmanagedTags = true
			// unmanagedElementsPolicyCombo.setItems("ignore", "keep as is", "rename to span");
			String unmanagedElementsPolicy = pager.project.getTextualPlan("UnmanagedElementsPolicy", "rename to span")
			if (unmanagedElementsPolicy.equals("rename to span")) {
				allTags = true;
			} else if (unmanagedElementsPolicy.equals("ignore")) {
				allTags = false
				ignoreUnmanagedTags = true
			}  else if (unmanagedElementsPolicy.equals("keep as it")) {
				allTags = false
				ignoreUnmanagedTags = false
			}
			String noteContent = ""
			String noteType = null
			String rend = null
			String style = null
			String type = null
			String subtype = null
			String popupReason = null

			boolean spaceAlreadyInserted = false;
			goToText();

			String localname = ""
			if (!createNextOutput()) {
				return false;
			}

			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				rend = "";
				style = "";
				type = "";
				subtype = ""
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName()
						if (outOfTextElements.contains(localname)) {
							currentOutOfTextElements << localname
							writeOutOfTextToEditText = true;
						} else if (currentOutOfTextElements.size() > 0) {
							currentOutOfTextElements << localname
						}

						if (localname == paginationElement) {
							if (paginate) {
								createNextOutput()
							}

							wordcount = 0;
							pagedWriter.write("\n")
							if (getAttributeValue(parser, null,"n") != null) {
								pagedWriter.writeElement("p", ["class":"txmeditionpb", "align":"center"], getAttributeValue(parser, null,"n"))
							}
						}

						rend = getAttributeValue(parser, null, "rend")
						if (rend == "") rend = null
						style = getAttributeValue(parser, null, "style")
						if (style == "") style = null
						type = getAttributeValue(parser, null, "type")
						if (type == "") type = null
						subtype = getAttributeValue(parser, null, "subtype")
						if (subtype == "") type = null

						switch (localname) {
							case "text":
								LinkedHashMap attributes = new LinkedHashMap();
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									attributes[parser.getAttributeLocalName(i)] = parser.getAttributeValue(i).toString()
								}

								pagedWriter.write("\n")
								pagedWriter.writeStartElement("p")
								pagedWriter.writeAttribute("class", rend)
								if (attributes.containsKey("id")) {
									pagedWriter.writeElement("h3", attributes["id"])
								}

								if (enableCollapsibles && parser.getAttributeCount() > 2) {
									pagedWriter.writeStartElement("button");
									pagedWriter.writeAttribute("class", "collapsible");
									pagedWriter.writeAttribute("onclick", "onCollapsibleClicked(this)");
									pagedWriter.writeCharacters("➕");
									pagedWriter.writeEndElement()
									pagedWriter.writeCharacters("\n")
								}
								pagedWriter.writeStartElement("table");
								if (enableCollapsibles && parser.getAttributeCount() > 2) {
									pagedWriter.writeAttribute("class", "collapsiblecontent")
									pagedWriter.writeAttribute("style", "display:none;")
								} else {
									pagedWriter.writeAttribute("class", "metadata");
								}

								for (String k : attributes.keySet()) {
									if (k == "id") continue;
									if (k == "rend") continue;

									pagedWriter.writeStartElement("tr")
									pagedWriter.writeAttribute("class", "metadata-line")
									pagedWriter.writeElement("td", ["class": "metadata-cell"], k)
									pagedWriter.writeElement("td", ["class": "metadata-cell"], attributes[k])
									pagedWriter.writeEndElement() //tr
								}
								pagedWriter.writeEndElement() // table

								pagedWriter.writeEndElement() // p
								pagedWriter.writeCharacters("\n")
								spaceAlreadyInserted = true;
								break;
							case "u":
								pagedWriter.writeStartElement("span", ["class":"sync", "type":type, "subtype":subtype, "style":style])
								if (parser.getAttributeValue(null,"time") != null) {
									pagedWriter.writeCharacters(parser.getAttributeValue(null,"time"))

									writeMediaAccess(parser.getAttributeValue(null,"time"), corpus, txtname)
								}
								spaceAlreadyInserted = false;
								break;
							case "q":
								pagedWriter.writeStartElement("q", ["class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							case "p":
							//case "lg":
								pagedWriter.write("\n")
								if ("p".equals(type)) type = null;
								pagedWriter.writeStartElement("p", ["class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							case "ab":
							case "l":
							//case "lg":
								pagedWriter.write("\n")
								pagedWriter.writeStartElement("p", ["class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							// reg/orig corr/sic supplied/surplus add/del abbr/expan
							case "corr": // TODO decomment for
							case "sic":
							case "add":
							case "del":
							case "reg":
							case "orig":
							case "supplied":
							case "surplus":
							case "abbr":
							case "expan":
								if (rend == null) rend = "";
								
								if (!flagW) {
//									println "EDIT: "+localname + " at "+parser.getLocation().getLineNumber();
									if (!spaceAlreadyInserted) {
										pagedWriter.writeCharacters(" ");
										spaceAlreadyInserted = true
									}
									
									pagedWriter.writeStartElement("span", ["class":(localname+" "+rend).trim(), "type":type, "subtype":subtype, "style":style, "part":getAttributeValue(parser, null, "part")])
									popupReason = getAttributeValue(parser, null, "reason")
									if (popupReason == null || popupReason == "") {
										popupReason = getAttributeValue(parser, null, "cause")
									}
									spaceAlreadyInserted = true;
								} else {
//									println "IGNORED Edit: "+localname + " at "+parser.getLocation().getLineNumber();
								}
//								else if (flagform) {
//									if (localname == "supplied" && "I" == getAttributeValue(parser, null, "part")) {
//										wordvalue += "["
//									} else if (localname == "supplied" && "F" == getAttributeValue(parser, null, "part")) {
//										wordvalue += "]"
//									} else if (localname == "surplus" && "I" == getAttributeValue(parser, null, "part")) {
//										wordvalue += "("
//									} else if (localname == "surplus" && "F" == getAttributeValue(parser, null, "part")) {
//										wordvalue += ")"
//									} 
//								}

								break;
							case "sp":
								pagedWriter.writeStartElement("p", ["class":rend, "type":type, "subtype":subtype, "style":style])

								if (parser.getAttributeValue(null,"who") != null) {
									pagedWriter.writeStartElement("span", ["class":"spk"])
									pagedWriter.writeCharacters(parser.getAttributeValue(null,"who")+": ")
									pagedWriter.writeEndElement() // span@class=spk
								}
								spaceAlreadyInserted = false;

								break;
							case "cb":
								pagedWriter.write("\n")
								pagedWriter.writeElement("span", ["class":"txmeditioncb", "align":"center", "type":type, "subtype":subtype, "style":style], getAttributeValue(parser, null,"n")) // element ignored in the END_ELEMENT event
								spaceAlreadyInserted = false;
								break;
							case "lb":
								pagedWriter.writeEmptyElement("br", ["class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							case "body":
							case "front":
							case "back":
							case "div":
							case "div1":
							case "div2":
							case "div3":
							case "div4":
							case "div5":
								pagedWriter.writeStartElement("div", ["class":rend, "type":type, "subtype":subtype, "style":style])
								pagedWriter.write("\n");
								spaceAlreadyInserted = true;
								break;
							case "head":
								pagedWriter.write("\n")
								pagedWriter.writeStartElement("h2", ["class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							case "graphic":
								pagedWriter.write("\n");
								String url = getAttributeValue(parser, null, "url")
								if (url != null) {
									// TEI <graphic rend="left-image" url="image.png"/> -> <center class="left-image"><img href="image.png"/></center> + <moncorpus>.css avec rule ".left-image"
									pagedWriter.writeStartElement("center", ["class":rend, "type":type, "subtype":subtype, "style":style]) // css -> .<rend> { ... } styles OR
									pagedWriter.writeEmptyElement("img", ["src":url, "align":"middle"])
									pagedWriter.writeEndElement() // center
								}
								spaceAlreadyInserted = false;
								break;
							case "list":
								if ("unordered" == rend || "bulleted" == rend) {
									pagedWriter.writeStartElement("ul", ["class":rend, "type":type, "subtype":subtype, "style":style])
								} else if ("ordered" == rend || "numbered" == rend) {
									pagedWriter.writeStartElement("ol", ["class":rend, "type":type, "subtype":subtype, "style":style])
								} else {
									pagedWriter.writeStartElement("ul", ["class":rend, "type":type, "subtype":subtype, "style":style])
								}
								spaceAlreadyInserted = false;
								break
							case "item":
								pagedWriter.writeStartElement("li", ["class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							case "hi":
							case "emph":
								if ("i".equals(rend) || "italic".equals(rend)) {
									pagedWriter.writeStartElement("i", ["class":rend, "type":type, "subtype":subtype, "style":style])
								} else if ("b".equals(rend) || "bold".equals(rend)) {
									pagedWriter.writeStartElement("b", ["class":rend, "type":type, "subtype":subtype, "style":style])
								} else {
									if ("emph".equals(localname)) {
										pagedWriter.writeStartElement("i", ["class":rend, "type":type, "subtype":subtype, "style":style])
									} else { // hi
										pagedWriter.writeStartElement("b", ["class":rend, "type":type, "subtype":subtype, "style":style])
									}
								}
								spaceAlreadyInserted = false;
								break;
							case "table":
								pagedWriter.writeStartElement("table", ["class":rend, "type":type, "subtype":subtype, "style":style])
								pagedWriter.write("\n");
								spaceAlreadyInserted = true;
								break;
							case "row":
								pagedWriter.writeStartElement("tr", ["class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							case "cell":
								pagedWriter.writeStartElement("td", ["class":rend, "type":type, "subtype":subtype, "style":style])
								String rows = getAttributeValue(parser, null, "rows")
								if (rows != null && rows.length() > 0) {
									pagedWriter.writeAttribute("rowspan", rows)
								}
								String cols = getAttributeValue(parser, null, "cols")
								if (cols != null && cols.length() > 0) {
									pagedWriter.writeAttribute("colspan", cols)
								}
								spaceAlreadyInserted = false;
								break;
							case "ref":
								pagedWriter.writeStartElement("a", ["href":getAttributeValue(parser, null, "target"), "class":rend, "type":type, "subtype":subtype, "style":style])
								spaceAlreadyInserted = false;
								break;
							case "form":
								wordvalue=""
								flagform=true
								break;
							case "ana":
								flaginterp=true;
								anaType = type.substring(1)
								anaResp = getAttributeValue(parser, null, "resp").substring(1)
								anaValue.setLength(0)
								break;
							case wordTag:
								wordid = getAttributeValue(parser, null,"id");
								anaValues.clear()
								wordcount++;
								if (paginate && wordcount >= wordmax) {
									createNextOutput();
								}

								if (firstWord) {
									firstWord = false;
									this.idxstart.add(wordid);
								}

								if (flagNote) {
									noteContent += "<w id=\""+wordid+"\">";
								}
								flagW = true
								break;
							default:
								if (noteElements.contains(localname)) {
									flagNote = true;
									noteContent = ""
									noteType = type
								} else if (allTags && !flagW && localname != paginationElement) {
									pagedWriter.writeStartElement("span", ["class":localname, "type":type, "subtype":subtype, "style":style])
								} else if (ignoreUnmanagedTags && !flagW && localname != paginationElement) {
									// do nothing
								} else {
									LinkedHashMap attributes = new LinkedHashMap();
									for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
										attributes[parser.getAttributeLocalName(i)] = parser.getAttributeValue(i).toString()
									}
									pagedWriter.writeStartElement(localname, attributes)
								}
								spaceAlreadyInserted = false;
								break;
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();

						if (currentOutOfTextElements.size() > 0) currentOutOfTextElements.pop()

						writeOutOfTextToEditText = currentOutOfTextElements.size() > 0

						if (localname == paginationElement) {
							break; // element already processed in the START_ELEMENT event
						}

						switch (localname) {
							case "text":
								break;
							case "u":
								pagedWriter.writeEmptyElement("br")
								spaceAlreadyInserted = true;
								break;
							case "p":
							case "q":
							case "ab":
							case "l":
							case "sp":
							//case "lg":
								pagedWriter.writeEndElement() // </p>
								pagedWriter.write("\n")
								spaceAlreadyInserted = true;
								break;

							// reg/orig corr/sic supplied/surplus add/del abbr/expan
							case "corr":
							case "sic":
							case "add":
							case "del":
							case "reg":
							case "orig":
							case "supplied":
							case "surplus":
							case "abbr":
							case "expan":

								if (!flagW) { // don't insert space in the word
									pagedWriter.writeEndElement() // </p>
									insertPopup(localname, popupReason);
									pagedWriter.write("\n")
									spaceAlreadyInserted = true
								} else {
									spaceAlreadyInserted = false;
								}
								break;
							case "lb":
							case "cb":
								break;
							case "body":
							case "front":
							case "back":
							case "div":
							case "div1":
							case "div2":
							case "div3":
							case "div4":
							case "div5":
								pagedWriter.writeEndElement() // </div>
								pagedWriter.write("\n");
								spaceAlreadyInserted = true;
								break;
							case "head":
								pagedWriter.writeEndElement() // </h2>
								pagedWriter.write("\n")
								spaceAlreadyInserted = true;
								break;
							case "graphic":
								break;
							case "list":
								pagedWriter.writeEndElement() // ul or ol
								pagedWriter.write("\n")
								spaceAlreadyInserted = true;
								break
							case "item":
								pagedWriter.writeEndElement() // li
								pagedWriter.write("\n")
								spaceAlreadyInserted = true;
								break;
							case "hi":
							case "emph":
								pagedWriter.writeEndElement() // b
								spaceAlreadyInserted = false;
								break;
							case "table":
								pagedWriter.writeEndElement() // table
								pagedWriter.write("\n");
								spaceAlreadyInserted = true;
								break;
							case "row":
								pagedWriter.writeEndElement() // tr
								spaceAlreadyInserted = false;
								break;
							case "cell":
								pagedWriter.writeEndElement() // td
								spaceAlreadyInserted = false;
								break;
							case "ref":
								pagedWriter.writeEndElement() // </a>
								spaceAlreadyInserted = false;
								break;
							case "form":
								flagform = false
								break;
							case "ana":
								flaginterp = false
								if (anaValues[anaType] == null || "src".equals(anaResp)) {
									anaValues[anaType] = anaValue.toString().trim()
								}
								break;
							case wordTag:
								int l = previouswordValue.length();
								String endOfLastWord = ""
								if (l > 0) {
									endOfLastWord = previouswordValue.subSequence(l-1, l)
								}

								String interpvalue = "";
								def tooltipProperties = pager.project.getEditionDefinition("default").get(TBXPreferences.EDITION_DEFINITION_TOOLTIP_PROPERTIES, "*");
								if (tooltipProperties.equals("*")) {
									tooltipProperties = anaValues.keySet().join(",")
								}
							//									int lmax = 4;
							//									tooltipProperties.split(",").each() { if (it.length() > lmax) lmax = it.length()}
								for (String p : tooltipProperties.split(",")) {
									//interpvalue += String.format("- %-${lmax}s: %s\n", p, anaValues.get(p))//"- "+p+": "+anaValues.get(p)+"\n"
									interpvalue += "- $p: ${anaValues.get(p)}\n"
								}
								interpvalue += "- "+wordid

								if (!flagNote) { // don't write words of the note elements
									if (NoSpaceBefore.contains(wordvalue)
										 || NoSpaceAfter.contains(previouswordValue)
										 || wordvalue.startsWith("-")
										 || NoSpaceAfter.contains(endOfLastWord)
										 || anaValues["join"] == "left"
										 || anaValues["join"] == "both"
										 || spaceAlreadyInserted) {
										pagedWriter.writeStartElement("span", ["title":interpvalue, "id":wordid])
									} else {
										pagedWriter.writeCharacters("\n")
										pagedWriter.writeStartElement("span", ["title":interpvalue, "id":wordid])
									}

									pagedWriter.writeCharacters(wordvalue)
									pagedWriter.writeEndElement()
									
									if (anaValues["join"] == "right" || anaValues["join"] == "both") {
										spaceAlreadyInserted = true; // should make skip next space
									} else {
										spaceAlreadyInserted = false;
									}
								} else {
									noteContent += "</w>";
								}
							//pagedWriter.writeComment("\n")
								previouswordValue=wordvalue;
								wordvalue="" // reset
								wordid = ""
								flagW = false
								break;
							default:
								if (noteElements.contains(localname)) {
									flagNote = false;
									if (noteContent.length() > 0) {
										notes << noteContent;
										String tmp = noteContent.trim().replaceAll("  ", " ")
										if (tmp.startsWith("<") && tmp.endsWith(">")) {
											tmp = tmp.replaceAll("<[^>]+>", "")
										}
										pagedWriter.writeStartElement("a", ["class": rend, "href":"#note_"+notes.size(), "name":"noteref_"+notes.size(), "title":tmp, "type":noteType!=null?noteType:localname]);
										pagedWriter.writeStartElement("sup")
										pagedWriter.writeCharacters(""+notes.size())
										pagedWriter.writeEndElement() // </sub>
										pagedWriter.writeEndElement() // </a>
									}
								} else if (allTags && !flagW && localname != paginationElement) {
									pagedWriter.writeEndElement() // </span@class=localname>
								}  else if (ignoreUnmanagedTags && !flagW && localname != paginationElement) {
									// do nothing
								} else {
									pagedWriter.writeEndElement() // the element
								}

								spaceAlreadyInserted = false;
							//							else {
							//								pagedWriter.writeEndElement() // the element
							//							}
								break;
						}
						break;
					case XMLStreamConstants.CHARACTERS:
						if (flagform && parser.getText().length() > 0) {
							wordvalue+=(parser.getText())
							if (flagNote == parser.getText().length() > 0) {
								noteContent += parser.getText().replace("\n", " ")
							}
						} else	if (flaginterp && parser.getText().length() > 0) {
							anaValue.append(parser.getText())
						} else if (flagNote == parser.getText().length() > 0) {
							noteContent += parser.getText().replace("\n", " ")
						} else if (writeOutOfTextToEditText) {
							pagedWriter.writeCharacters(parser.getText())
						}
						break;
				}
			}
			closeMultiWriter();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
		} catch(Exception e) {
			println "** Fail to build $infile edition: $e at "+parser.getLocation()
			println "** resulting file: $outfile"
			println "** Stax stack: "+pagedWriter.getTagStack()
			e.printStackTrace();
			pagedWriter.close()
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}
		return true;
	}

	private void insertPopup(String localname, String popupReason) {
		
		if (popupReason != null && popupReason.length() > 0) {
			pagedWriter.writeStartElement("span", ["class":"pop-up"])
			pagedWriter.writeCharacters(localname+": "+popupReason);
			pagedWriter.writeEndElement() // span@class=pop-up
			popupReason = null;
		}

	}

	private void writeMediaAccess(def time) {

		pagedWriter.writeCharacters(" ");
		pagedWriter.writeStartElement("a");
		pagedWriter.writeAttribute("onclick", "txmcommand('id', 'org.txm.backtomedia.commands.function.BackToMedia', 'corpus', '"+basename+"', 'text', '"+txtname+"', 'time', '"+time+"')");
		pagedWriter.writeAttribute("style", "cursor: pointer;")
		pagedWriter.writeAttribute("class", "play-media")
		pagedWriter.writeCharacters("▶");
		pagedWriter.writeEndElement(); // a
	}
}
