package org.txm.scripts.importer.limsi

import javax.xml.stream.*
import java.net.URL

class LimsiToCQP {

	File xmlfile;

	public LimsiToCQP(File xmlfile) {
		this.xmlfile = xmlfile;
	}

	public boolean process(File outfile) {
		if (!xmlfile.exists()) return false;
		
		URL url = xmlfile.toURI().toURL();
		String filename = outfile.getName()
		filename = filename.substring(0, filename.length()-4); // remove ".cqp"
		def inputData = url.openStream();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(inputData);

		def output = new OutputStreamWriter(new FileOutputStream(outfile) , "UTF-8");
		
		boolean flagWord = false
		String word = ""
		

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event)
			{
				case XMLStreamConstants.START_ELEMENT:
					String localname = parser.getLocalName()
					//println "start $localname"
					switch(localname) {
						case "AudioDoc":
							output.println "<text id=\"$filename\">"
							break
						case "Word":
							flagWord = true
							word = ""
						break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					String localname = parser.getLocalName()
					switch(localname) {
						case "AudioDoc":
						output.println "</text>"
						break
						case "Word":
							flagWord = false
							output.println word.trim()
						//	println "WORD: $word"
						break;
					}
					break
				case XMLStreamConstants.CHARACTERS:
					if (flagWord) {
						word += parser.getText();
					}
					break
			}
		}
		output.flush()
		output.close()
		//println "$xmlfile -> $outfile"
		return true;
	}

	public static void main(String[] args) {
		File infile = new File("/home/mdecorde/xml/limsi","20071220_1900_1920_inter.xml")
		File outfile = new File("/home/mdecorde/xml/limsi","out.cqp")
		def processor = new LimsiToCQP(infile);
		println processor.process(outfile);
	}
}
