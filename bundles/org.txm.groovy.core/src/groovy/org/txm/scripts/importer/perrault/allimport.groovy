/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.perrault;

import javax.xml.stream.*

import org.txm.scripts.importer.*
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.scripts.filters.*
import org.txm.scripts.*
import org.txm.importer.scripts.xmltxm.*
import org.txm.utils.treetagger.TreeTagger

import org.txm.scripts.filters.CutHeader.*
import org.txm.scripts.filters.FusionHeader.*
import org.txm.scripts.filters.Tokeniser.*

// TODO: Auto-generated Javadoc
/**
 * Split.
 *
 * @param file the file
 * @return the list
 */
public static List<File> split(File file)
{
	File f  = file;

	println "split file "+f;
	String rootDir = f.getParent()+"/";
	String xslfile = rootDir+"splitcorpus.xsl";
	String outfile = rootDir+"split_temp.xml";

	//get the splited file name
	//String outfilename = new XPathResult(f).getXpathResponse("//TEI/text/body/div/head","");

	ApplyXsl a = new ApplyXsl(xslfile);
	//a.SetParam("xpathtag", "//TEI");//coupe //text
	//a.SetParam("xpathfilename", "/body/div/head");//cherche a partir de //xpathtag
	a.process(f.getPath(),outfile);

	new File(outfile).delete();

	List<File> files = new File(f.getParent(),"split").listFiles();
}

/**
 * Run1.
 *
 * @param srcfiles the srcfiles
 */
public void run1(File[] srcfiles)
{
	List<File> files = null;
	File fullfile;
	String rootDir ="";
	ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
	milestones.add("tagUsage");
	milestones.add("pb");
	milestones.add("lb");

	for(File f : srcfiles)
	{
		files = split(f);
		rootDir = f.getParent()+"/"
		fullfile = f;
		new File(rootDir+"tokenized").deleteDir();
		new File(rootDir+"tokenized").mkdir();
		new File(rootDir+"split").deleteDir();
		new File(rootDir+"split").mkdir();
		new File(rootDir+"txm").deleteDir();
		new File(rootDir+"txm").mkdir();

		String xslfile = rootDir+"splitcorpus.xsl";
		String infile = f.getPath();
		String outfile = rootDir+"split_temp.xml";

		ApplyXsl a = new ApplyXsl(xslfile);
		a.process(infile,outfile);

		new File(outfile).delete();
	}

	//get header
	String header =""
	Reader reader = new FileReader(fullfile);
	String cline = reader.readLine();
	while(!cline.trim().contains("<text>"))
	{
		header += cline+"\n";
		cline = reader.readLine();
	}

	//put splited into tei file
	for(File f : files)
	{
		File temp = new File(f.getParent(),"temp");
		Writer writer = new FileWriter(temp)
		writer.write(header);
		writer.write("""<text>\n<body>\n""")
		f.eachLine{String line->
			if(!line.startsWith("<?xml"))
				writer.write(line+"\n");
		}

		writer.write("""</body>\n</text>\n</TEI>""")
		writer.close();

		if (!(f.delete() && temp.renameTo(f))) println "Warning can't rename file "+temp+" to "+f
	}

	//PREPARE EACH SPLITED FILE TO BE TOKENIZED
	println files
	for(File f : files)
	{
		File srcfile = f;
		File resultfile = new File(rootDir+"tokenized",f.getName()+"-src.xml");
		println("prepare tokenizer file : "+srcfile+" to : "+resultfile );
		def builder = new OneTagPerLine(srcfile.toURL(), milestones);
		builder.process(resultfile);
	}

	//TOKENIZE FILES
	//Manager<Filter> filterManager = new FilterManager(ActionHome);
	for(File f : files)
	{
		Sequence S = new Sequence();
		Filter F1 = new CutHeader();
		Filter F6 = new Tokeniser(f);
		Filter F11 = new FusionHeader();
		S.add(F1);
		S.add(F6);
		S.add(F11);
		File infile = new File(rootDir+"tokenized",f.getName()+"-src.xml");
		File xmlfile = new File(rootDir+"tokenized",f.getName()+"-out.xml");
		File headerfile = new File(rootDir+"/tokenized/",f.getName()+"header.xml");
		println("Tokenize "+xmlfile)
		S.SetInFileAndOutFile(infile.getPath(), xmlfile.getPath());
		S.setEncodages("UTF-8","UTF-8");
		Object[] arguments1 = [headerfile.getAbsolutePath()];
		F1.SetUsedParam(arguments1);
		Object[] arguments2 = [headerfile.getAbsolutePath(),F1];
		F11.SetUsedParam(arguments2);
		S.proceed();
		S.clean();
		infile.delete();//remove the prepared file to clean
		headerfile.delete();//remove the prepared file to clean
	}
	files = new File(rootDir,"tokenized").listFiles()


	//TRANSFORM INTO XML-TEI-TXM
	for(File f : files)
	{
		//ArrayList<String> milestones = new ArrayList<String>();
		println("build xml-tei-txm "+f+ " >> "+f.getName()+"-TXM.xml")
		File file = f;
		String txmfile = f.getName()+"-TXM.xml";

		def correspType = new HashMap<String,String>()
		def correspRef = new HashMap<String,String>()
		//il faut lister les id de tous les respStmt
		def respId = [];
		//fait la correspondance entre le respId et le rapport d'execution de l'outil
		def applications = new HashMap<String,HashMap<String,String>>();
		//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
		//pour construire les ref vers les taxonomies
		def taxonomiesUtilisees = new HashMap<String,String[]>();
		//associe un id d'item avec sa description et son URI
		def itemsURI = new HashMap<String,HashMap<String,String>>();
		//informations de respStmt
		//resps (respId <voir ci-dessus>, [description, person, date])
		def resps = new HashMap<String,String[]>();
		//lance le traitement
		String wordprefix = "w_c_";
		def builder = new Xml2Ana(file);
		builder.setCorrespondances(correspRef, correspType);
		builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
		builder.transformFile(rootDir+"txm/",txmfile);
	}

	//rename files correctly
	files = new File(rootDir,"txm").listFiles();
	for(File file : files)
	{
		String txmfile = file.getName();
		txmfile = txmfile.tokenize(".").get(0)+".xml"
		file.renameTo(new File(file.getParent(),txmfile));
	}

}

/**
 * Run2.
 *
 * @param rootDirFile the root dir file
 */
public void run2(File rootDirFile)
{
	String rootDir = rootDirFile.getAbsolutePath()+"/";
	if(!new File(rootDir,"models").exists() || !new File(rootDir,"txm").exists())
	{
		println "no models dir or no txm dir, check rootDir "+rootDirFile;
		return;
	}
	//cleaning
	new File(rootDir,"annotations").deleteDir();
	new File(rootDir,"annotations").mkdir();
	new File(rootDir,"treetagger").deleteDir();
	new File(rootDir,"treetagger").mkdir();

	ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
	milestones.add("tagUsage");
	milestones.add("pb");
	milestones.add("lb");

	List<File> files = new File(rootDir,"txm").listFiles()
	//BUILD TT FILE READY TO BE TAGGED
	for(File f : files)
	{
		File srcfile = f;
		File resultfile = new File(rootDir+"treetagger/",f.getName()+".tt");
		new BuildTTSrc(srcfile.toURL()).process(resultfile)
	}

	//APPLY TREETAGGER
	files = new File(rootDir,"treetagger").listFiles()
	for(File f : files)
	{
		File modelfile = new File(rootDir+"models/","fr.par");
		File infile = f
		File outfile = new File(f.getParent(),f.getName()+"-out.tt");
		println("3- APPLY TT on : "+infile+" with : "+modelfile +" >>  "+outfile);

		TreeTagger tt = new TreeTagger(System.getProperty("user.home")+"/TXM/treetagger/bin/");
		tt.settoken();
		tt.setlemma();
		tt.setquiet();
		tt.setsgml();
		tt.setnounknown();
		tt.seteostag("<s>");
		tt.treetagger( modelfile.getAbsolutePath(), infile.getAbsolutePath(), outfile.getAbsolutePath())
		infile.delete();
	}

	//BUILD STAND-OFF FILES
	//contains txm:application/txm:commandLine
	File reportFile = new File(rootDir,"NLPToolsParameters.xml");

	String respPerson = System.getProperty("user.name");
	String respId = "txm";
	String respDesc = "NLP annotation tool";
	String respDate = "";
	String respWhen = ""

	String appIdent = "TreeTagger";
	String appVersion = "3.2";

	String distributor = "";
	String publiStmt = """""";
	String sourceStmt = """""";

	def types = ["pos","lemme"];
	def typesTITLE = ["",""];
	def typesDesc = ["",""];
	def typesTAGSET = ["",""];
	def typesWEB = ["",""];
	String idform ="w_c_";

	files = new File(rootDir,"treetagger").listFiles()
	for(File f : files)
	{
		String target = f.getAbsolutePath();
		File ttfile = f
		File posfile = new File(rootDir+"annotations/",f.getName()+"-STOFF.xml");

		def encoding ="UTF-8";
		def transfo = new CSV2W_ANA();
		println("build w-interp "+ttfile.getName()+ ">>"+posfile.getName())
		transfo.setAnnotationTypes( types, typesDesc, typesTAGSET, typesWEB, idform);
		transfo.setResp(respId, respDesc,respDate, respPerson, respWhen);
		transfo.setApp(appIdent, appVersion);
		transfo.setTarget(target, reportFile);
		transfo.setInfos(distributor,  publiStmt, sourceStmt);
		transfo.process( ttfile, posfile, encoding );
	}

	files = new File(rootDir,"annotations").listFiles();
	List<File> txmfiles = new File(rootDir,"txm").listFiles();
	for(int i = 0 ; i< files.size();i++)
	{
		File srcfile = txmfiles.get(i);
		File pos1file = files.get(i);
		File temp = new File(rootDir,"temp");

		println("5- inject annotation in file : "+srcfile+" with : "+pos1file );

		def builder = new org.txm.scripts.teitxm.AnnotationInjection(srcfile.toURL(), pos1file.toURL(), milestones);
		builder.transfomFile(temp.getParent(),temp.getName());

		if (!(srcfile.delete() && temp.renameTo(srcfile))) println "Warning can't rename file "+temp+" to "+srcfile
	}

}

def inputData;
def factory;
XMLStreamReader parser;
def dir;
def output;
def url;
HashMap<String,String> anahash =new HashMap<String,String>() ;
String text="";
String base="";
String project="";

/**
 * initialize.
 *
 * @param url the url
 * @param text the text
 * @param base the base
 * @param project the project
 */
public void compil(URL url,String text,String base, String project)
{
	this.text = text
	this.base = base;
	this.project = project;
	try {
		this.url = url;
		inputData = url.openStream();

		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
	} catch (XMLStreamException ex) {
		System.out.println(ex);
	}catch (IOException ex) {
		System.out.println("IOException while parsing ");
	}
}

/**
 * Creates the output.
 *
 * @param dirPathName the dir path name
 * @param fileName the file name
 * @return true, if successful
 */
private boolean createOutput(String dirPathName, String fileName){
	try {
		File f = new File(dirPathName, fileName)
		output = new java.io.FileWriter(f,f.exists())
		return true;
	} catch (Exception e) {
		System.out.println(e.getLocalizedMessage());
		return false;
	}
}

/**
 * clear anaHash variable, it is used to store ana tags values then print it when the end element </ana> is found.
 */
private void fillanaHash()
{
	anahash.clear();
	for(String s : types)
		anahash.put( s,"-" );
}


/**
 * Go to text.
 */
private void GoToText()
{
	for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
	{
		if(event == XMLStreamConstants.END_ELEMENT)
			if(parser.getLocalName().equals("teiHeader"))
				return;
	}
}

/**
 * Transfom file cqp.
 *
 * @param dirPathName the dir path name
 * @param fileName the file name
 * @return true, if successful
 */
public boolean transfomFileCqp(String dirPathName, String fileName)
{
	createOutput(dirPathName, fileName);

	String headvalue=""
	String vAna = "";
	String vForm = "";
	String wordid= "";
	String vHead = "";

	int p_id = 0;
	int q_id = 0;
	int lg_id = 0;
	int l_id = 0;

	boolean flaglg = false;
	boolean flaghead = false;
	boolean flagAuthor = false;
	boolean flagDate = false;
	boolean flagForm = false;
	boolean flagAna = false;

	this.GoToText()

	try
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event)
			{
				case XMLStreamConstants.START_ELEMENT:
					switch (parser.getLocalName())
					{

						case "head"://get attr lang
						flaghead =true;
						vHead="";
						break;

						case "text":
						output.write("<text id=\""+text+"\" base=\""+base+"\" project=\""+project+"\">\n");
						break;

						case "p":
						output.write("<"+parser.getLocalName()+" id=\""+text+"_p_"+(p_id++)+"\">\n");
						break;
						case "q":
						output.write("<"+parser.getLocalName()+" id=\""+text+"_q_"+(q_id++)+"\">\n");
						break;
						case "l":
						output.write("<"+parser.getLocalName()+" id=\""+text+"_l_"+(l_id++)+"\">\n");
						break;

						case "lg":
						flaglg = true;
						break;

						case "s":
						output.write( "<s>\n");
						break;

						case "w":
						for(int i = 0 ; i < parser.getAttributeCount(); i++)
							if(parser.getAttributeLocalName(i).equals("id"))
						{
							wordid = parser.getAttributeValue(i);
						}
						break;
						case "form":
						flagForm = true;
						vForm = "";
						vAna ="";
						break;

						case "ana":
						flagAna = true;
						break;
					}
					break;

				case XMLStreamConstants.END_ELEMENT:
					switch (parser.getLocalName())
					{
						case "head"://get attr lang
						flaghead =false;
						if(flaglg)
							output.write("<moral id=\""+text+"_moral_"+(lg_id++)+"\" head=\""+vHead+"\">\n");
						break;

						case "text":
						output.write("</text>\n");
						break;

						case "p":
						case "q":
						case "l":
						output.write("</"+parser.getLocalName()+">\n");
						break;

						case "lg":
						output.write("</moral>\n");
						flaglg = false;
						break;

						case "s":
						output.write( "</s>\n");
						break;

						case "w":
						if(!(flaghead && flaglg))
							if(vAna != null)
								output.write( vForm +vAna+"\t"+wordid+"\n");
						vAna = "";
						vForm = "";
						break;

						case "form":
						flagForm = false;
						break;

						case "ana":
						flagAna = false;
						break;
					}
					break;

				case XMLStreamConstants.CHARACTERS:
					if(flagForm)
						vForm += parser.getText().trim();
					if(flagAna)
						vAna += "\t" +parser.getText().trim();
					if(flaghead && flaglg)
						vHead += parser.getText().trim();
					break;
			}
		}
		output.close();
		parser.close();
	}
	catch (XMLStreamException ex) {
		System.out.println(ex);
	}
	catch (IOException ex) {
		System.out.println("IOException while parsing " + inputData);
	}

	return true;
}

/**
 * Run3.
 *
 * @param files the files
 * @return true, if successful
 */
public boolean run3(List<File> files)
{
	String rootDir ="";
	if (files.size() > 0)
		rootDir = files.get(0).getParentFile().getParentFile().getAbsolutePath()+"/";//"~/xml/perrault/";

	if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
		println ("Error: CWB executables not well set.")
		return false;
	}
	if(!new File(rootDir).exists())
	{
		println ("binary directory does not exists: "+rootDir)
		return false;
	}
	new File(rootDir+"cqp/","perrault.cqp").delete();//cleaning&preparing
	new File(rootDir+"cqp/").deleteDir();
	new File(rootDir+"cqp/").mkdir();
	new File(rootDir+"registry/").mkdir();

	String textid="";
	int counttext =0;
	//1- Transform into CQP file
	for(File f : files)
	{
		counttext++;
		if(!f.exists())
		{
			println("file "+f+ " does not exists")
		}
		else
		{
			println("process file "+f)
			compil(f.toURL(),"text"+counttext,"perrault","default");
			transfomFileCqp(rootDir+"cqp","perrault.cqp");
		}
	}

	//2- Import into CWB
	def outDir =rootDir;
	def outDirTxm = rootDir;

	CwbEncode cwbEn = new CwbEncode();
	CwbMakeAll cwbMa = new CwbMakeAll();

	String[] pAttributes = ["id","pos","lemme"];
	//String[] pAttributes = ["id"];
	String[] sAttributes = ["text:0+id+base+project","p:0+id","q:0+id","moral:0+head+id","l:0+id"];

	try
	{
		cwbEn.run(outDirTxm + "data/"+"PERRAULT", outDir + "/cqp/"+"perrault.cqp", outDirTxm + "registry/"+"perrault",pAttributes, sAttributes);
		cwbMa.run("PERRAULT", outDirTxm + "registry");

	} catch (Exception ex) {System.out.println(ex); return false;}

	System.out.println("Done.")

	return true;
}

////FIN
println "IMPORTER"
File[] files = [new File("~/xml/perrault/perrault.xml")];
run1(files);

println "ANNOTATE"
File rootDir = new File("~/xml/perrault/");
run2(rootDir);

println "COMPIL"
File directory = new File("~/xml/perrault/txm/");
files = directory.listFiles();
ArrayList<File> Lfiles = new ArrayList<File>();
for(File f : files)
	Lfiles.add f
run3(Lfiles);

//move registry file to cwb registry dir
File registryfile = new File("~/xml/perrault/txm/registry/perrault");