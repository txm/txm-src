package org.txm.scripts.importer.transcriber

import java.text.DecimalFormat;

import org.txm.utils.xml.DomUtils;
import org.txm.utils.ExecTimer;
import org.txm.utils.FileUtils
import org.w3c.dom.*;
import groovy.xml.XmlParser

/**
 * TRS file time validation for the STDDAD master corpus
 * @author mdecorde
 *
 */
class ValidateTRS {
	def doc;
	def root;
	def episodeElem;
	def topicsElem;
	def speakersElem;

	def declaredTopics = [];
	def topics = [];

	def mandatoryTopics = ["parcours", "ressources", "débouchés", "acteurs", "métier", "images", "autres"]
	def declaredSpeakers = [];
	def speakers = [];
	def spkDict = [:];
	def topicDict = [:];

	def sections = [];
	def turns = new LinkedHashMap();
	def syncs = new LinkedHashMap();
	def entities = [];

	def messages = [];

	float sectionsTime = 0;
	int numberOfSection = 0;
	HashMap<String, Float> sectionsTimesByTopic = new HashMap<String, Float>();
	HashMap<String, Integer> numberOfSectionByTopic = new HashMap<String, Integer>();

	float turnsTime = 0;
	int numberOfTurn = 0;
	HashMap<String, Float> turnsTimesBySpk = new HashMap<String, Float>();
	HashMap<String, Integer> numberOfTurnsBySpk = new HashMap<String, Integer>();

	int totalWords = 0;
	HashMap<String, Integer> totalWordsByTopic = new HashMap<String, Integer>();
	HashMap<String, Integer> totalWordsBySpk = new HashMap<String, Integer>();

	def static okTopics = ["débouchés", "parcours","ressources",
		"métier", "acteurs", "autres", "images"];

	def static spkPattern = "((int|enq)[0-9][0-9])|(ext[0-9][0-9][0-9])"

	public static boolean checkSpk(String spk)
	{
		return spk.matches("((int|enq)[0-9][0-9])|(ext[0-9][0-9][0-9])");
	}

	public ValidateTRS(File trsFile)
	{
		def slurper = new XmlParser();
		root = slurper.parse(trsFile.toURI().toString())
		episodeElem = root.Episode;
		speakersElem = root.Speakers;
		topicsElem = root.Topics
		init();
	}

	public boolean test(){
		//ckeckTimes();
	}

	public void init() {
		initSpeakersDecl();
		initTopicsDecl();

		initSections();
		initTurns();
		initSyncs();

		initSpeakers();
		initTopics();
		initEntities();
	}

	public void initEntities() {
		for(def section : sections)
			for(def turn : turns.get(section))
				for(def entity : turn.Event)
					if (entity.@type == "entities")
						entities.add(entity.@desc)
	}

	public void initTopicsDecl(){
		declaredTopics = topicsElem.Topic;
		for (def topic:declaredTopics) {
			topicDict[topic.@id]=topic.@desc
		}
	}

	public void initSpeakersDecl(){
		declaredSpeakers = speakersElem.Speaker;
		for (def spk:declaredSpeakers) {
			spkDict[spk.@id]=spk.@name
		}
	}

	public void initSections() {
		sections = episodeElem.Section;
		numberOfSection = sections.size()
	}

	public void initTurns() {
		for(def section : sections) {
			turns[section] = section.Turn
			numberOfTurn += turns[section].size()
			//numberOfTurn
		}
	}

	public void initSyncs() {
		for(def section : sections)
			for(def turn : turns.get(section))
				syncs[turn] = turn.Sync
	}

	public void initTopics(){
		for(def section : sections) {
			String topic = section.@topic;
			if (section.@type != "report")
				topic = section.@type
			if (topic == null) topic = "notopic"	
			topics.add(topic);
		}

		topics.sort()
	}

	public void initSpeakers(){
		for(def section : sections)
			for(def turn : turns.get(section)) {
				String spk = turn.@speaker;
				if (spk == null) spk = "nospk"
				speakers.add(spk);
			}
		speakers.sort();
	}

	public boolean topicsCheck() {
		messages = []
		boolean ret = false;
		def topicsIds = declaredTopics.collect { topic -> topic.@id }
		for(def topic : topics)
			if( topic != null && !topicsIds.contains(topic) && topic != "nontrans" && topic != "notopic") {
				messages << "missing topic declaration: "+topic
				ret = true;
			}
		return ret;
	}

	public def speakersCheck() {
		messages = []
		boolean ret = false;
		def spksIds = declaredSpeakers.collect { spk -> spk.@id }
		for(def spk : speakers)  {
			if (spk == null) continue; // ignore null spk
			def split = spk.split(" ")
			for(def sspk : split) {
				if(!spksIds.contains(sspk) && sspk != "nospk") {
					println "missing spk declaration: "+sspk
					ret = true;
				}
			}
		}
		return ret;
	}

	String NULL = "null"
	public void printSortedList(def list, def dict) {
		def all = [:]
		for(def value : list) {
			if (value == null) {
				if(!all.containsKey(NULL))
					all[NULL] = 0
				all[NULL] = all[NULL] + 1
			} else {
				def split = value.split(" ")
				if (dict != null) {
					for(def sspk : split) {
						if (!(all.containsKey(sspk)))
							all[sspk] = 0;
						all[sspk] = all[sspk] + 1
					}
				} else {
					if (!(all.containsKey(value)))
						all[value] = 0;
					all[value] = all[value] + 1
				}
			}
		}

		def toprint = [];
		for(def speaker : all.keySet())
			toprint.add([speaker, all[speaker]]);

		toprint = toprint.sort{it -> it.get(1)}

		for(def line : toprint)
			if (dict != null) {
				if (dict[line.get(0)] != null)
					println line.get(0)+"\t"+dict[line.get(0)]+"\t"+line.get(1)
				else
				println line.get(0)+"\t"+line.get(0)+"\t"+line.get(1)
			} else {
				println line.get(0)+"\t"+line.get(1)
			}
	}

	public def timeCheck() {
		messages = [];
		boolean warning = false;
		boolean error = false
		def previousSectionStart = -1.0f;
		def previousSectionEnd = -1.0f;

		def previousTurnStart = -1.0f;
		def previousTurnEnd = -1.0f;

		for(def section : sections) {
			def sectionStart = Float.parseFloat(section.'@startTime')
			def sectionEnd =  Float.parseFloat(section.'@endTime')
			def topic = section.@topic;
			if (section.@type != "report")
				topic = section.@type;
			if (topic == null) topic = "notopic"
			
			def time = sectionEnd - sectionStart;
			if (!sectionsTimesByTopic.containsKey(topic)) {
				sectionsTimesByTopic.put(topic, 0);
				numberOfSectionByTopic.put(topic, 0);
				totalWordsByTopic.put(topic, 0);
			}
			if (topic != null && topic != "null" && topic.length() > 0) {
				sectionsTime += time;
			}
			sectionsTimesByTopic.put(topic, sectionsTimesByTopic.get(topic) + time);
			numberOfSectionByTopic.put(topic, numberOfSectionByTopic.get(topic) + 1);

			boolean sectionBug = false;

			if ( sectionEnd <= sectionStart) { sectionBug = true;
				messages << "sectionEnd <= sectionStart : $sectionEnd <= $sectionStart"
			}

			if ( sectionStart < previousSectionStart) { sectionBug = true;
				messages << "sectionStart < previousSectionStart : $sectionStart < $previousSectionStart"
			}
			if ( sectionStart < previousSectionEnd) { sectionBug = true;
				messages << "sectionStart < previousSectionEnd : $sectionStart < $previousSectionEnd"
			}
			if ( sectionEnd < previousSectionStart) { sectionBug = true;
				messages << "sectionEnd < previousSectionStart : $sectionEnd < $previousSectionStart"
			}
			if ( sectionEnd < previousSectionEnd) { sectionBug = true;
				messages << "sectionEnd < previousSectionEnd : $sectionEnd < $previousSectionEnd"
			}

			for(def turn : turns.get(section)) {
				def turnStart = Float.parseFloat(turn.'@startTime')
				def turnEnd =  Float.parseFloat(turn.'@endTime')
				def spk = ""+turn.@speaker;
				if (turn.@speaker == "null") spk = "nospk"
				time = turnEnd - turnStart;
				int nbwords = turn.text().split().length

				for(String subspk : spk.split(" ")) {
					String spkname = spkDict[subspk];
					if (spkname == null) spkname = "nospk"
					if (!totalWordsBySpk.containsKey(spkname)) {
						totalWordsBySpk.put(spkname, 0);
						turnsTimesBySpk.put(spkname, 0);
						numberOfTurnsBySpk.put(spkname, 0);
					}
				}

				if (spk != "null") turnsTime += time;
				totalWords += nbwords
				for(String subspk : spk.split(" ")) {
					String spkname = spkDict[subspk];
					if (spkname == null) spkname = "nospk"
					totalWordsBySpk.put(spkname, totalWordsBySpk.get(spkname) + nbwords)
					numberOfTurnsBySpk.put(spkname, numberOfTurnsBySpk.get(spkname) + 1);
					turnsTimesBySpk.put(spkname, turnsTimesBySpk.get(spkname) + time);
				}
				totalWordsByTopic.put(topic,totalWordsByTopic.get(topic) +nbwords);

				boolean bug = false;

				if ( turnEnd <= turnStart) { bug = true;
					messages << " turnEnd <= turnStart : $turnEnd <= $turnStart"
				}

				//				if ( turnStart < previousSectionStart) { bug = true;
				//					println " turnStart < previousSectionStart : $turnStart < $previousSectionStart"
				//				}
				//				if ( turnStart < previousSectionEnd) { bug = true;
				//					println " turnStart < previousSectionEnd : $turnStart < $previousSectionEnd"
				//				}
				//				if ( turnEnd < previousSectionStart) { bug = true;
				//					println " turnEnd < previousSectionStart : $turnEnd < $previousSectionStart"
				//				}
				//				if ( turnEnd < previousSectionEnd) { bug = true;
				//					println " turnEnd < previousSectionEnd : $turnEnd < $previousSectionEnd"
				//				}

				if ( turnStart < previousTurnStart) { bug = true;
					messages << " turnStart < previousTurnStart : $turnStart < $previousTurnStart"
				}
				if ( turnStart < previousTurnEnd) { bug = true;
					messages << " turnStart < previousTurnEnd : $turnStart < $previousTurnEnd"
				}
				if ( turnEnd < previousSectionStart) { bug = true;
					messages << " turnEnd < previousTurnStart : $turnEnd < $previousTurnStart"
				}
				if ( turnEnd < previousTurnEnd) { bug = true;
					messages << " turnEnd < previousTurnEnd : $turnEnd < $previousTurnEnd"
				}
				if (turnStart < sectionStart) { bug = true;
					messages << " turnStart < sectionStart : $turnStart < $sectionStart"
				}
				if (turnStart > sectionEnd) { bug = true;
					messages << " turnStart > sectionEnd : $turnStart > $sectionEnd"
				}
				if ( turnEnd < sectionStart) { bug = true;
					messages << " turnEnd < sectionStart : $turnEnd < $sectionStart"
				}
				if (turnEnd > sectionEnd) { bug = true;
					messages << " turnEnd > sectionEnd : $turnEnd > $sectionEnd"
				}

				for(def sync : syncs.get(turn)) {
					boolean syncBug = false;
					def syncTime = Float.parseFloat(sync.'@time')
					if ( syncTime < turnStart) { syncBug = true;
						messages << "  syncTime < turnStart : $syncTime < $turnStart"
					}
					if ( syncTime > turnEnd) { syncBug = true;
						messages << "  syncTime > turnEnd : $syncTime > $turnEnd"
					}

					if (syncBug) {
						warning = true;
					}
				}

				previousTurnStart = turnStart;
				previousTurnEnd = turnEnd;
				if (bug) {
					error = true;
				}
			}

			previousSectionStart = sectionStart;
			previousSectionEnd = sectionEnd;
			if ( sectionBug) {
				error = true;
			}
		}

		return [error, warning];
	}

	public void printErrors()
	{
		for(String mess : messages)
			println mess;
	}

	public void printAll()
	{
		println "Declared topics: $declaredTopics"
		println "Topics: $topics"

		println "Declared speakers: $declaredSpeakers"
		println "Speakers: $speakers"

		println "Sections: "+sections.size()
		println "Turns: "+syncs.size()
		println "Entities: $entities"
	}

	public static HashSet<String> allTopicDesc = new HashSet<String>();
	public static HashSet<String> allSpkName = new HashSet<String>();
	public static void checkDirectory(File dir) {
		allTopicDesc = new HashSet<String>();
		allSpkName = new HashSet<String>();
		//println "trs dir: $dir"
		for (File trsFile : FileUtils.listFiles(dir, ".*\\.trs")) {
			checkTRS(trsFile)
		}

		//println allTopicDesc;
		//println allSpkName;

	}

	public static boolean checkTRS(File trsFile) {
		boolean ret = true;
		println "\n***** $trsFile.name *****"
		def tester = new ValidateTRS(trsFile);
		//tester.printAll();

		def (error, warning) = tester.timeCheck();
		if( error ) {
			print "\nTIMES: "
			//if( error || warning) {
			println "errors: "+error+" warnings: "+warning;
			tester.printErrors();
			ret = false;
		} //else println "all declared"

		println "\nSPEAKERS: id, name, frequency"
		if (tester.speakersCheck()) {
			println "errors:"
			tester.printErrors();
			ret = false;
		} //else println "all declared"
		tester.printSortedList(tester.speakers, tester.spkDict);

		println "\nSECTIONS: id, title, frequency"//+ tester.declaredTopics
		if (tester.topicsCheck()) {
			println "errors:"
			tester.printErrors();
			ret = false;
		} //else println "all declared"
		tester.printSortedList(tester.topics, tester.topicDict);

		println "\nENTITIES: value, frequency"
		tester.printSortedList(tester.entities, null);

		println ""
		for(def topic : tester.declaredTopics) {
			allTopicDesc.add(topic.@desc)
			if (!okTopics.contains(topic.@desc)) {
				if(tester.topics.contains(topic.@id))
					println "topic error: "+topic.@desc

			}
		}
		//println ""
		for(def spk : tester.declaredSpeakers) {
			allSpkName.add(spk.@name)
			if(!checkSpk(spk.@name)) {
				if(tester.speakers.contains(spk.@id))
					println "spk error: "+spk.@name
			}
		}
		DecimalFormat formater = new DecimalFormat("0.00");

		println "Statistics:"
		println " Approx nb words: "+tester.totalWords;
		println " Total time: "+ExecTimer.formatSecs(tester.turnsTime)
		println " Approx word rate (word/sec): "+formater.format(tester.totalWords/tester.turnsTime)
		println " Approx word rate (word/min): "+formater.format(60*(tester.totalWords/tester.turnsTime))
		println ""

		def meanSections = [:];
		for(String key : tester.sectionsTimesByTopic.keySet())
			meanSections.put(key, ExecTimer.formatSecs(tester.sectionsTimesByTopic.get(key) / tester.numberOfSectionByTopic.get(key)))
		def meanTurns = [:];
		for(String key : tester.turnsTimesBySpk.keySet())
			meanTurns.put(key, ExecTimer.formatSecs(tester.turnsTimesBySpk.get(key) / tester.numberOfTurnsBySpk.get(key)))

		for(String key : tester.sectionsTimesByTopic.keySet())
			tester.sectionsTimesByTopic.put(key, ExecTimer.formatSecs(tester.sectionsTimesByTopic.get(key)))
		for(String key : tester.turnsTimesBySpk.keySet())
			tester.turnsTimesBySpk.put(key, ExecTimer.formatSecs(tester.turnsTimesBySpk.get(key)))

		println " Number of sections: "+tester.numberOfSection
		println " Sections time: "+ExecTimer.formatSecs(tester.sectionsTime);
		println " Mean time of sections: "+ExecTimer.formatSecs(tester.sectionsTime/tester.numberOfSection)

		println " Sections time by topic: "+tester.sectionsTimesByTopic
		println " Number of sections by topic: "+tester.numberOfSectionByTopic
		println " Mean time of sections by topic: "+meanSections
		println ""

		println " Number of turns: "+tester.numberOfTurn
		println " Turns time: "+ExecTimer.formatSecs(tester.turnsTime);
		println " Mean time of turns: "+ExecTimer.formatSecs(tester.turnsTime/tester.numberOfTurn)

		println " Turns time by spk: "+tester.turnsTimesBySpk
		println " Number of turns by spk: "+tester.numberOfTurnsBySpk
		println " Mean time of turns by spk: "+meanTurns
		println ""

		return ret;
	}

	public static void main(String[] args) {
		//File dir = new File("/home/sheiden/enslyon_projets/Textométrie/SpUV/EVS/master 1 STDDAD 2011/enquête 2011/Reçues")
		File dir = new File("/home/mdecorde/xml/concattrs/ready")
		ValidateTRS.checkDirectory(dir);
		//ValidateTRS.checkTRS(new File("/home/mdecorde/xml/concattrs/int40-21.trs"))
	}
}
