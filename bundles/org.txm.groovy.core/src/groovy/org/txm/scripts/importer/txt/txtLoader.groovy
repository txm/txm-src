// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-04-11 15:30:35 +0200 (mar. 11 avril 2017) $
// $LastChangedRevision: 3426 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.txt

import org.txm.scripts.importer.txt.importer
import org.txm.scripts.importer.txt.compiler
import org.txm.scripts.importer.xml.pager
import org.txm.objects.*
import org.txm.importer.scripts.xmltxm.*
import org.txm.*
import org.txm.objects.*
import org.txm.core.engines.*
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.i18n.*
import org.txm.utils.FileUtils
import org.txm.metadatas.*
import org.txm.utils.io.FileCopy
import org.w3c.dom.Element
import org.txm.utils.xml.DomUtils
import org.txm.importer.*

String userDir = System.getProperty("user.home")

def MONITOR
Project project

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName()
String basename = corpusname
String rootDir = project.getSrcdir()
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL()
def xslParams = project.getXsltParameters()
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs()
if (!binDir.exists()) {
	println "Error: could not create corpus binary directory: "+binDir
	return
}

File txmDir = new File(binDir, "txm/$corpusname")
txmDir.deleteDir()
txmDir.mkdirs()

//get metadata values from CSV
Metadatas metadatas // text metadata
File allMetadataFile = Metadatas.findMetadataFile(srcDir)

if (allMetadataFile != null && allMetadataFile.exists()) {
	println "Trying to read metadata from: "+allMetadataFile
	File copy = new File(binDir, allMetadataFile.getName())
	if (!FileCopy.copy(allMetadataFile, copy)) {
		println "Error: could not create a copy of metadata file "+allMetadataFile.getAbsoluteFile()
		return
	}
	metadatas = new Metadatas(copy, Toolbox.getMetadataEncoding(), 
		Toolbox.getMetadataColumnSeparator(), 
		Toolbox.getMetadataTextSeparator(), 1)
}

def suffixes = ["txt", "TXT"]
println "-- IMPORTER - Reading source files with extension "+suffixes
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "IMPORTER - Reading source files with extension "+suffixes)
if (!new importer().run(srcDir, binDir, txmDir,encoding, suffixes, basename, lang, project)) {
	println "Import process stopped"
	return
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "INJECTING METADATA - from csv file: "+allMetadataFile)
if (metadatas != null) {
	println "-- INJECTING METADATA - from csv file: "+allMetadataFile
	println("Injecting metadata: "+metadatas.getHeadersList()+" in texts of directory "+txmDir)
	def filesToInject = FileUtils.listFiles(txmDir)
	ConsoleProgressBar cpb = new ConsoleProgressBar(filesToInject.size())
	for (File infile : filesToInject) {
		cpb.tick()
		File outfile = File.createTempFile("temp", ".xml", infile.getParentFile())

		if (!metadatas.injectMetadatasInXml(infile, outfile, "text", null)) {
			outfile.delete();
		} else {
			if (!(infile.delete() && outfile.renameTo(infile))) println "Warning can't rename file "+outfile+" to "+infile
			if (!infile.exists()) {
				println "Error: could not replace $infile by $outfile"
				return false
			}
		}
	}
	cpb.done()
}


if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "ANNOTATE - Running NLP tools")
boolean annotationSuccess = true;
if (annotate) {
	println "-- ANNOTATE - Running NLP tools"
	String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
	def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
	
	if (engine == null) {
		println "$engineName extension is not installed."
	} else if (engine.processDirectory(txmDir, binDir, ["lang":model])) {
		annotationSuccess = true
		if (project.getCleanAfterBuild()) {
			new File(binDir, "treetagger").deleteDir()
			new File(binDir, "ptreetagger").deleteDir()
			new File(binDir, "annotations").deleteDir()
		}
	}
}

println "-- COMPILING - Building Search Engine indexes"
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "COMPILING - Building Search Engine indexes")
def c = new compiler(null, "", corpusname, "default")
//c.setCwbPath(userDir+"/TXM/cwb/bin/")// for developers
if (metadatas != null)
	c.setMetadataAttributes(metadatas.getSattributes())
c.setLang(lang);
c.setAnnotationSuccess(annotationSuccess)
if (debug) c.setDebug();
if (!c.run(project)) {
	println "Import process stopped"
	return
}
//println "basename :"+basename;
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
new File(binDir,"HTML/$corpusname").deleteDir()
new File(binDir,"HTML/$corpusname").mkdirs()
if (build_edition) {

	println "-- EDITION - Building edition"
	if (MONITOR != null) MONITOR.worked(20, "EDITION - Building edition")
	
	File outdir = new File(binDir, "HTML/$corpusname/default/")
	outdir.mkdirs()
	List<File> filelist = FileUtils.listFiles(txmDir)
	Collections.sort(filelist)
	def second = 0

	ConsoleProgressBar cpb = new ConsoleProgressBar(filelist.size())
	for (File srcfile : filelist) {
		cpb.tick()
		String txtname = srcfile.getName()
		int i = txtname.lastIndexOf(".")
		if (i > 0) txtname = txtname.substring(0, i)

		List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang)
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang)

		Text t = new Text(project)
		t.setName(txtname)
		t.setSourceFile(srcfile)
		t.setTXMFile(srcfile)
		
		def ed = new pager(srcfile, outdir, txtname, NoSpaceBefore, NoSpaceAfter, basename, project)
		Edition edition = new Edition(t)
		edition.setName("default")
		edition.setIndex(outdir.getAbsolutePath())
		for (i = 0 ; i < ed.getPageFiles().size();) {
			File f = ed.getPageFiles().get(i)
			String wordid = "w_0"
			if (i < ed.getIdx().size()) {
				wordid = ed.getIdx().get(i)
			}
			edition.addPage(""+(++i), wordid)
		}
	}
	cpb.done()
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FINALIZING")


	
readyToLoad = project.save()
