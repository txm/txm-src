// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//

package org.txm.scripts.importer.perrault

import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;
import javax.xml.stream.*;
import java.net.URL;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.FusionHeader.*;

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {
	
	/**
	 * Split.
	 *
	 * @param file the file
	 * @return the list
	 */
	public static List<File> split(File file)
	{
		File f  = file;
		
		println "split file "+f;
		String rootDir = f.getParent()+"/";
		String xslfile = rootDir+"/xsl/splitcorpus.xsl";
		String outfile = rootDir+"split_temp.xml";
		
		//get the splited file name
		//String outfilename = new XPathResult(f).getXpathResponse("//TEI/text/body/div/head","");
		
		ApplyXsl a = new ApplyXsl(xslfile);
		//a.SetParam("xpathtag", "//TEI");//coupe //text
		//a.SetParam("xpathfilename", "/body/div/head");//cherche a partir de //xpathtag
		a.process(f.getPath(),outfile);
		
		new File(outfile).delete();
		
		List<File> files = new File(f.getParent(),"split").listFiles();
	}
	
	/**
	 * Run.
	 *
	 * @param srcfiles the srcfiles
	 */
	public static void run(File[] srcfiles)
	{
		List<File> files = null;
		File fullfile;
		String rootDir ="";
		ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		
		for(File f : srcfiles)
		{
			files = importer.split(f);
			rootDir = f.getParent()+"/"
			fullfile = f;
			new File(rootDir+"ptokenized").deleteDir();
			new File(rootDir+"ptokenized").mkdir();
			new File(rootDir+"tokenized").deleteDir();
			new File(rootDir+"tokenized").mkdir();
			new File(rootDir+"split").deleteDir();
			new File(rootDir+"split").mkdir();
			new File(rootDir+"txm").deleteDir();
			new File(rootDir+"txm").mkdir();
			
			String xslfile = rootDir+"xsl/splitcorpus.xsl";
			String infile = f.getPath();
			String outfile = rootDir+"split_temp.xml";
			
			ApplyXsl a = new ApplyXsl(xslfile);
			a.process(infile,outfile);
			
			new File(outfile).delete();
		}
		
		//get header
		String header =""
		Reader reader = new FileReader(fullfile);
		String cline = reader.readLine();
		while(!cline.trim().contains("<text>"))
		{
			header += cline+"\n";
			cline = reader.readLine();
		}
		
		//put splited into tei file
		for(File f : files)
		{
			File temp = new File(f.getParent(),"temp");
			Writer writer = new OutputStreamWriter(new FileOutputStream(temp) , "UTF-8");
			writer.write(header);
			writer.write("""<text>\n<body>\n""")
			f.eachLine{String line->
				if(!line.startsWith("<?xml"))
				writer.write(line+"\n");
			}
			
			writer.write("""</body>\n</text>\n</TEI>""")
			writer.close();
			
			if (!(f.delete() && temp.renameTo(f))) println "Warning can't rename file "+temp+" to "+f
		}
		
		//PREPARE EACH SPLITED FILE TO BE TOKENIZED
		println files
		for(File f : files)
		{
			File srcfile = f;
			File resultfile = new File(rootDir+"ptokenized",f.getName()+"-src.xml");
			println("prepare tokenizer file : "+srcfile+" to : "+resultfile );
			def builder = new OneTagPerLine(srcfile.toURL(), milestones);
			builder.process(resultfile);
		}
		
		//TOKENIZE FILES
		files = new File(rootDir,"ptokenized").listFiles()	
		for(File f : files)
		{
			Sequence S = new Sequence();
			Filter F1 = new CutHeader();
			Filter F6 = new Tokeniser(f);
			Filter F11 = new FusionHeader();
			S.add(F1);
			S.add(F6);
			S.add(F11);
			File infile = new File(rootDir+"ptokenized",f.getName());
			File xmlfile = new File(rootDir+"tokenized",f.getName()+"-out.xml");
			File headerfile = new File(rootDir+"/ptokenized/",f.getName()+"header.xml");
			println("Tokenizing "+xmlfile)
			S.SetInFileAndOutFile(infile.getPath(), xmlfile.getPath());
			S.setEncodages("UTF-8","UTF-8");
			Object[] arguments1 = [headerfile.getAbsolutePath()];
			F1.SetUsedParam(arguments1);
			Object[] arguments2 = [headerfile.getAbsolutePath(),F1];
			F11.SetUsedParam(arguments2);
			S.proceed();

			System.out.println("exits src tokenized : "+infile.exists());
			S= null;
			F1=null;
			F6=null;
			F11=null;
			System.out.println("delete src tokenized : "+infile.delete());//remove the prepared file to clean
			headerfile.delete();//remove the prepared file to clean
		}

		//TRANSFORM INTO XML-TEI-TXM
		files = new File(rootDir,"tokenized").listFiles()	
		for(File f : files)
		{
			//ArrayList<String> milestones = new ArrayList<String>();
			println("Building xml-tei-txm "+f+ " >> "+f.getName()+"-TXM.xml")
			File file = f; 
			String txmfile = f.getName()+"-TXM.xml";
			
			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()
			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>();	
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
					//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();
			//lance le traitement
			String wordprefix = "w_c_";
			def builder = new Xml2Ana(file);
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			builder.transformFile(rootDir+"txm/",txmfile);
		}
		
		//rename files correctly
		files = new File(rootDir,"txm").listFiles();
		for(File file : files)
		{
			String txmfile = file.getName();
			txmfile = txmfile.tokenize(".").get(0);
			txmfile = txmfile.substring(0, txmfile.length()-1);
			txmfile += ".xml"
			file.renameTo(new File(file.getParent(),txmfile));
		}
		
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File[] files = [new File("~/xml/perrault/perrault.xml")];
		new importer().run(files);
	}
}
