// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.hyperprince;

import org.txm.objects.*;
import org.txm.scripts.importer.XPathResult;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;
import org.txm.utils.i18n.*;

// TODO: Auto-generated Javadoc
/** Build Discours corpus simple edition from a xml-tei. @author mdecorde */
class pager {
	List<String> NoSpaceBefore;
	
	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;
	
	/** The pagecount. */
	int pagecount = 0;
	
	/** The wordmax. */
	int wordmax = 0;

	/** The wordid. */
	String wordid;
	
	/** The first word. */
	boolean firstWord = true;
	
	/** The wordvalue. */
	String wordvalue;
	
	/** The interpvalue. */
	String interpvalue;
	
	/** The lastword. */
	String lastword = " ";
	
	/** The wordtype. */
	String wordtype;
	
	/** The flagform. */
	boolean flagform = false;
	
	/** The flaginterp. */
	boolean flaginterp = false;
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The writer. */
	def writer;
	
	/** The multiwriter. */
	def multiwriter = null;
	
	/** The infile. */
	File infile;
	
	/** The outfile. */
	File outfile;
	
	/** The pages. */
	ArrayList<File> pages = new ArrayList<File>();
	
	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>();
	
	/** The segid. */
	String segid;
	
	/** The title. */
	String title;

	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 */
	pager(File infile, File outfile, List<String> NoSpaceBefore,
			List<String> NoSpaceAfter, int max) {
		this.wordmax = max;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();
		this.infile = infile;

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		createOutput(outfile);
		process();
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput()
	{
		try {
			
			if(multiwriter != null)
			{
				multiwriter.write("</body>");
				multiwriter.write("</html>");
				multiwriter.close();
			}
			pagecount++;
			File f = new File(outfile.getParent()+"/multi/",outfile.getName().substring(0,2)+"_"+"page_"+pagecount+".html");
			pages.add(f);
			idxstart.add(wordid)
			multiwriter = new OutputStreamWriter(new FileOutputStream(f) , "UTF-8");
			
			multiwriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			multiwriter.write("<html>");
			multiwriter.write("<head>");
			multiwriter.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
			
			if(segid != null)
			{
				multiwriter.write("<title>Hyperprince "+segid+"</title>");
			}else {
				multiwriter.write("<title>Hyperprince "+title+"</title>");
			}
			multiwriter.write("</head>");
			multiwriter.write("<body>");
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			this.outfile = outfile;

			writer = new OutputStreamWriter(new FileOutputStream(outfile),
					"UTF-8");
			createNextOutput();
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	/**
	 * Process.
	 */
	void process()
	{
		String localname = "";
		String query = "//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/text()";
		title = XPathResult.getXpathResponse(infile, query);
		
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		writer.write("<html>");
		writer.write("<head>");
		writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
		writer.write("<title>Hyperprince "+title+"</title>");
		writer.write("</head>");
		writer.write("<body>");
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			
			
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{	
						case "text":
							//XPathResult testG = new XPathResult(infile);
							
							//String title = testG.getXpathResponse("//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title","")
							//testG = null;
							writer.write("<h1>"+title+"</h1>\n")
							multiwriter.write("<h1>"+title+"</h1>\n")
							break;
						
						case "front":
							writer.write("<h2>"+parser.getAttributeValue(null, "type")+"</h2>\n")
							multiwriter.write("<h2>"+parser.getAttributeValue(null, "type")+"</h2>\n")
							break;
						
						case "div0":
							writer.write("<h2>Book</h2>\n")
							multiwriter.write("<h2>Book</h2>\n")
							break;
						
						case "back":
							writer.write("<h2>"+parser.getAttributeValue(null, "type")+"</h2>\n")
							multiwriter.write("<h2>"+parser.getAttributeValue(null, "type")+"</h2>\n")
							break;
						
						case "seg":
							segid=parser.getAttributeValue(null,"id");
							createNextOutput();
							
							writer.write("<p>\n");
							multiwriter.write("<p>\n");
							break;
						
						case "w":
							wordid=parser.getAttributeValue(null,"id");
							if(firstWord)
							{
								firstWord=false;
								this.idxstart.set(0,wordid);
							}
							/*wordcount++;
							if(wordcount >= wordmax)
							{
								createNextOutput();
								wordcount=0;
							}*/
							break;
						
						case "ana":
							flaginterp=true;
							interpvalue+=" "+parser.getAttributeValue(null,"type")+":"
							break;
						
						case "form":
							wordvalue="";
							interpvalue ="";
							flagform=true;
							break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{
						case "teiHeader":
						
							break;
						
						case "seg":
							writer.write("</p>\n");
							break;
						
						case "form":
							flagform = false
						
							break;
						
						case "ana":
							flaginterp = false
							break;
						
						case "w":
							int l = lastword.length();
							String endOfLastWord = "";
							if(l > 0)
								endOfLastWord = lastword.subSequence(l-1, l);
							
							if(interpvalue != null)
								interpvalue = interpvalue.replace("\"","&quot;");
							
							if(NoSpaceBefore.contains(wordvalue) || 
									NoSpaceAfter.contains(lastword) || 
									wordvalue.startsWith("-") || 
									NoSpaceAfter.contains(endOfLastWord))
							{
								writer.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
								multiwriter.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
							}
							else
							{
								writer.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
								multiwriter.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
							}
							writer.write(wordvalue.replace("<", "&lt;")+"</span>");
							multiwriter.write(wordvalue.replace("<", "&lt;")+"</span>");
							lastword=wordvalue;
							break;
						
					}
					break;
				
				case XMLStreamConstants.CHARACTERS:
					if(flagform)
						if(parser.getText().length() > 0)
							wordvalue+=(parser.getText());
					if(flaginterp)
						if(parser.getText().length() > 0)
							interpvalue+=(parser.getText());
					break;
			}
		}	
		writer.write("</body>");
		writer.write("</html>");
		writer.close();
		multiwriter.write("</body>");
		multiwriter.write("</html>");
		multiwriter.close();
		
		parser.close();
		inputData.close();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		/*String rootDir = System.getProperty("user.home")+"/xml/hyperprince/";
		new File(rootDir,"HTML/").deleteDir();
		new File(rootDir+"HTML/").mkdir();
		new File(rootDir+"HTML/multi/").mkdir();
		List<File> filelist = new File(rootDir,"txm").listFiles();
		
		for(File srcfile : filelist)
		{
			File resultfile = new File(rootDir+"HTML",srcfile.getName().substring(0,2)+".html");
			List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
			List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
			println("build discours xml-tei file : "+srcfile+" to : "+resultfile );
			
			def builder = new pager(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter,1500);
		}
		
		return;*/
		
		
		String userDir = System.getProperty("user.home");
		String lang= "fr"
		String rootDir;
		println "DEV MODE";//exception means we debug
		if(!org.txm.Toolbox.isInitialized()){
			rootDir = "D:\\Travail_Sev\\Projets\\Corpus\\TXM\\hyperprince"; //userDir+"/xml/hyperprince/";//"D:\\Travail_Sev\\Projets\\Corpus\\TXM\\hyperprince";
			org.txm.Toolbox.workspace = new Workspace(new File(userDir,"TXM/corpora/default.xml"));
			org.txm.Toolbox.setParam(org.txm.Toolbox.INSTALL_DIR,new File(userDir,"TXM"));
		}
		
		Workspace w = org.txm.Toolbox.workspace;
		Project p = w.getProject("default")
		p.removeBase("hyperprince")
		Base b = p.addBase("hyperprince");
		b.addDirectory(new File(rootDir,"txm"));
		
		
		println "-- EDITION"
		new File(rootDir+"/HTML/").deleteDir();
		new File(rootDir+"/HTML/").mkdir();
		new File(rootDir,"/HTML/multi").mkdir();
		List<File> filelist = new File(rootDir,"txm").listFiles();

		for(String textname : b.getTextsID())
		{
			Text text = b .getText(textname);
			File srcfile = text.getSource();
			File resultfile = new File(rootDir+"/HTML",srcfile.getName().substring(0,srcfile.getName().length()-4)+".html");
			List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
			List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
			println("build hyperprince xml-tei file : "+srcfile+" to : "+resultfile );
			
			def ed = new pager(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter,1000);
			
			Edition editionweb = text.addEdition("default","html",resultfile);
			//println("pages "+ed.getPageFiles())
			//println("idx "+ed.getIdx())
			for(int i = 0 ; i < ed.getPageFiles().size();i++)
			{
				File f = ed.getPageFiles().get(i);
				String idx = ed.getIdx().get(i);
				editionweb.addPage(f,idx);
			}
			
			//Edition (version Pincemin)
//			Edition editionbp = text.addEdition("onepage","html",resultfile);
//			editionbp.addPage(resultfile,ed.getIdx().get(0));
		}
		w.save();
	}
}