/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.hyperprince;

import org.txm.scripts.importer.hyperprince.importer;
import org.txm.scripts.importer.hyperprince.compiler;
import org.txm.objects.*;
import org.txm.Toolbox;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
String userDir = System.getProperty("user.home");
String rootDir;
String lang;
String encoding;
String model;
try{rootDir = rootDirBinding;lang=langBinding;encoding=encodingBinding;model=modelBinding;}
catch(Exception)
{	println "DEV MODE";//exception means we debug
	if(!org.txm.Toolbox.isInitialized())
	{
		rootDir = userDir+"/xml/hyperprince/";
		lang="fr";
		encoding= "UTF-8";
		model="rgaqcj";
		Toolbox.workspace = new Workspace(new File(userDir,"TXM/corpora/default.xml"));
		Toolbox.setParam(Toolbox.INSTALL_DIR,new File(userDir,"TXM"));
		Toolbox.setParam(Toolbox.USER_TXM_HOME, new File(System.getProperty("user.home"),"TXM"));
	}
}

File homedir = new File(rootDir);
String corpusSrc = "Corpus-Hyperprince_2009-06-10.xml";//"Corpus-Hyperprince_2010-04-06.xml";

//IMPORT TXT
if(!new File(rootDir,corpusSrc).exists())
{
	println("source file : "+new File(rootDir,corpusSrc)+" does not exists")
	return false;
}

println "-- IMPORTER - Reading source files"
List<File> srcfiles = [new File(rootDir,corpusSrc)];
new importer().run(rootDir, corpusSrc);

//ANNOTATE
println "-- ANNOTATE - Running NLP tools"
if (annotate) {
	String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
	def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
	if (engine.processDirectory(txmDir, binDir, ["lang":model])) {
		annotationSuccess = true;
	}
}

//COMPILATION
println "COMPILING"
def c = new compiler()
//c.setCwbPath("D:\\Travail_Sev\\Logiciels\\TXM\\cwb\\bin"); // for developers
c.setLang(lang);
c.run(rootDir);

//move registry file to cwb registry dir
File registryfile = new File(rootDir+"/registry","hyperprince");
if(registryfile.exists())
	org.txm.utils.io.FileCopy.copy(registryfile,new File(Toolbox.getTxmHomePath(),"registry/hyperprince"))

Workspace w = org.txm.Toolbox.workspace;
Project p = w.getProject("default")
p.removeBase("hyperprince")
Base b = p.addBase("hyperprince");
b.addDirectory(new File(rootDir,"txm"));
b.setAttribute("lang", lang)
b.propagateAttribute("lang")

println "-- EDITION"
new File(rootDir+"/HTML/").deleteDir();
new File(rootDir+"/HTML/").mkdir();
new File(rootDir,"/HTML/default").mkdir();
List<File> filelist = new File(rootDir,"txm").listFiles();

for(String textname : b.getTextsID())
{
	Text text = b .getText(textname);
	File srcfile = text.getSource();
	File resultfile = new File(rootDir+"/HTML",srcfile.getName().substring(0,srcfile.getName().length()-4)+".html");
	List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
	List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
	println("build hyperprince xml-tei file : "+srcfile+" to : "+resultfile );
	
	def ed = new pager(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter,1000);
	
	Edition editionweb = text.addEdition("default","html",resultfile);
	//println("pages "+ed.getPageFiles())
	//println("idx "+ed.getIdx())
	for(int i = 0 ; i < ed.getPageFiles().size();i++)
	{
		File f = ed.getPageFiles().get(i);
		String idx = ed.getIdx().get(i);
		editionweb.addPage(f,idx);
	}
	
//	Edition editionbp = text.addEdition("onepage","html",resultfile);
//	editionbp.addPage(resultfile,ed.getIdx().get(0));
}

w.save()