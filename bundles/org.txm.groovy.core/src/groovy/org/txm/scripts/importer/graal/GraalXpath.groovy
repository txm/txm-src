// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.graal

import org.txm.scripts.importer.*;
import java.io.IOException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.stream.*;
import java.net.URL;


// TODO: Auto-generated Javadoc
/**
 * The Class GraalXpath.
 *
 * @authors ayepdieu, mdecorde, vchabanis
 * 
 * return the id of a bfm tag <milestone/>
 */
public class GraalXpath
{
	
	/**
	 * Gets the xpath response.
	 *
	 * @param fileName the xmlfile
	 * @param N the id of the milestone
	 * @return the id of a bfm tag <milestone/>
	 */
	public static String getXpathResponse(def fileName, String N) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException 
	{	
		if (System.getProperty("os.name").contains("Windows"))
		{
			
			XMLInputFactory factory = XMLInputFactory.newInstance();
			FileInputStream inputStream = new FileInputStream(new File(fileName));
			XMLStreamReader parser = factory.createXMLStreamReader(inputStream);

			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
			{
				if( event== XMLStreamConstants.START_ELEMENT)
				{
					if(parser.getLocalName() == "milestone")
					{
						if(parser.getAttributeValue(null , "n")== N)
						{
							if(parser.getAttributeValue(null , "unit")== "column")
							{
								//TODO : changer l'acces par une clef String au lieu d'un index, prob avec le namespace
								return parser.getAttributeValue(1);
							}
						}		
					}
				}
			}
			println( "FAILED TO FIND MILESTONE WITH N = "+N);
			return null;
		}
		else
		{
			def testG = new XPathResult(new File(fileName));
			String npage =testG.getXpathResponse("//tei:milestone[@unit='column' and @n='"+N+"']","xml:id")
			return npage;
			/*String Query = "//tei:milestone[@unit='column' and @n='"+N+"']"
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		    domFactory.setNamespaceAware(true); // never forget this!
		    DocumentBuilder builder = domFactory.newDocumentBuilder();
		    Document doc = builder.parse(fileName);
		   
		    XPathFactory factory = XPathFactory.newInstance();
		    XPath xpath = factory.newXPath();
		    xpath.setNamespaceContext(new PersonalNamespaceContext());
		    XPathExpression expr = xpath.compile(Query);

		    Object result = expr.evaluate(doc, XPathConstants.NODESET); // don't work on windows
		    NodeList nodes = (NodeList) result;
		    return nodes.item(0).getAttributes().getNamedItem("xml:id").getNodeValue();
		    */
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		def testG = new GraalXpath();
		String npage =testG.getXpathResponse("C:\\Documents and Settings\\H\\Bureau\\resourcesTXM\\corpora\\data\\qgraal\\qgraal_cm_2009_03.xml","2")
		println "found npage : "+npage;
	}
}