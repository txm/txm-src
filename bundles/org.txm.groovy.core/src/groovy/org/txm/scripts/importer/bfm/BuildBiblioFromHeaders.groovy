package org.txm.scripts.importer.bfm;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.txm.utils.*;
import org.txm.scripts.importer.*;

import javax.xml.stream.*;
import java.net.URL;
	

String xslfile = "G:\\Feuilles de style\\teiHeader2ficheBiblio.xsl";
ApplyXsl a = new ApplyXsl(xslfile);

File srcdirectory = new File("C:\\Documents and Settings\\alavrent\\TXM-SRC\\corptef")
File outdirectory = new File("C:\\Documents and Settings\\alavrent\\TXM-SRC\\corptef-biblio")
outdirectory.mkdir();
assert(srcdirectory.exists())
assert(outdirectory.exists())
assert(srcdirectory.isDirectory())
assert(srcdirectory.canRead())
assert(srcdirectory.canExecute())

List<File> files = FileUtils.listFiles(srcdirectory);

files = files.findAll{item-> item.getName().endsWith(".xml")}

long bigstart = System.currentTimeMillis()
for(File infile : files)
{
	String outfile = outdirectory.getAbsolutePath()+"/"+infile.getName().substring(0,infile.getName().length()-4)+"-biblio.html";
	if(infile.canRead())
	{
		long start = System.currentTimeMillis()
		println "Process : "+infile.getName()
		// String[] params = ["pbval1",1,"pbval2",2];
		a.process(infile.getAbsolutePath(), outfile); // no parameters
		// a.process( infile.getAbsolutePath(),outfile,params);
		println "Done : "+(System.currentTimeMillis()-start)+"ms"
	}
}
println "Total time : "+(System.currentTimeMillis()-bigstart)+"ms"
/*
class BuildBiblioFromHeaders {
	
	public static boolean process(File dir, File xslfile, File outputfile) {
		if(!(xslfile.exists() && xslfile.canRead()))
		{
			println "xslfile does not exist"
			return false;
		}
		
		if(!outputfile.getParentFile().exists())
		{
			println "outputfile's parent does not exist"
			return false;
		}
		
		
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		FileOutputStream output = new FileOutputStream(outputfile)
		XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8");
		
		
		writer.writeStartDocument("UTF-8","1.0");
		writer.writeStartElement("html");
		//header
		writer.writeStartElement("head");
		//writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
		writer.writeStartElement("meta");
		writer.writeAttribute("http-equiv","Content-Type");
		writer.writeAttribute("content","text/html");
		writer.writeAttribute("charset","UTF-8");
		writer.writeEndElement();//meta
		writer.writeStartElement("title");
		writer.writeCharacters("Fiches biblio BFM")
		writer.writeEndElement();//title
		writer.writeEndElement();//head
		
		writer.writeStartElement("body");
		
		String rootDir = dir.getAbsolutePath()+"/";
		
		List<File> files = FileUtils.listFiles(new File(rootDir,""));//scan directory split
		
		for(File f : files) {
			
//			ApplyXsl a = new ApplyXsl(xslfile.getAbsolutePath());
//			String[] args = ["outputDir",xxx];
//			a.process(f.getAbsolutePath(), "./HTML",args);
			
			

		}
		
		
		writer.writeEndElement();//body
		writer.close();
		return true;
	}
	
	
	
	public static void main(String[] args)
	{
	
		String rootDir;
		String htmloutdir;
		File dir = new File(rootDir);
		
		rootDir = "C:\\Documents and Settings\\alavrent\\TXM-SRC\\corptef";
		File xslfile = new File("G:\\Feuilles de style\\teiHeader2ficheBiblio.xsl")
		File outputfile = new File("C:\\Documents and Settings\\alavrent\\Mes documents\\bfm - local\\Bases bibliographiques (copie)\\Fiches_biblio_TXM_test.html")

		
		if(!BuildBiblioFromHeaders.process(xslfile, outputfile, false))
			println("Error.")
		else
			println "Success";
	}
}
*/