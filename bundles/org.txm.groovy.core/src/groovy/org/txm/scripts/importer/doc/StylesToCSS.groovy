package org.txm.scripts.importer.doc
import groovy.xml.XmlSlurper

class StylesToCSS {

	File cssFile, styleFile, contentFile;
	def rootStyles, rootContent;

	def styles = [:]
	def translatedStyles = [:]
	
	def beforebreaks = []
	def afterbreaks = []
	def parentStyles = [:]

	public StylesToCSS(File unzipDir) {
		this.styleFile= new File(unzipDir, "styles.xml");
		this.contentFile= new File(unzipDir, "content.xml");
		rootStyles = new XmlSlurper().parse(styleFile);
		rootContent = new XmlSlurper().parse(contentFile);
	}

	public boolean process(File css) {
		//println "StylesToCSS: "+css
		this.cssFile = cssFile;
		
		//INDEXING STYLES BY ".NAME_FAMILY"
		// for each style of styles.xml
		for( def style : rootStyles.styles.style) {
			String family = ""+style.@family;
			String name = ""+style.@name
			if ("paragraph".equals(family)) {
				styles[name+"_"+family] = style
			} else if ("text".equals(family)) {
				if (!name.startsWith("WW8Num"))
					styles[name+"_"+family] = style
			} else if ("graphic".equals(family)) {
				styles[name+"_"+family] = style
			}
		}
		
		// for each automatic style of content.xml
		for( def style : rootContent['automatic-styles'].style) {
			//println "style: "+style.@name
			String family = ""+style.@family;
			String name = ""+style.@name
			String parent = ""+style['@parent-style-name'] // get parent style name
			String breakpage = ""; // get soft page break
			for (def pproperty : style['paragraph-properties']) {
				//println " paragraph-properties: "+pproperty['@break-before']
				//println " paragraph-properties: "+pproperty['@break-after']
				if ("page" == pproperty['@break-before'].toString())
					beforebreaks << name
				else if ("page" == pproperty['@break-after'].toString())
					afterbreaks << name
			}
			
			parentStyles[name] = parent
			
			//println "auto $name $family"
			if ("paragraph".equals(family)) {
				styles[name+"_"+family] = style
			} else if ("text".equals(family)) {
				if (!name.startsWith("WW8Num"))
					styles[name+"_"+family] = style
			} else if ("graphic".equals(family)) {
				styles[name+"_"+family] = style
			}
		}
		
		// for each default style of styles.xml
		for( def style : rootStyles.styles['default-style']) {
			String family = ""+style.@family;
			String name = ""+style.@name
			if ("paragraph".equals(family)) {
				styles[name+"_"+family] = style
			}
		}
		//println styles.keySet()
		
		//BUILDING CSS
		// build styles.xml css
		for( def style : rootStyles.styles.style) {
			String name = ""+style.@name
			if (!name.startsWith("WW8Num")) buildStyle(style);
		}
		
		// for each default style of styles.xml
		for( def style : rootStyles.styles['default-style']) {
			String family = ""+style.@family;
			String name = ""+style.@name
			if ("paragraph".equals(family)) {
				buildStyle(style)
			}
		}
		
		// build content.xml css
		for( def style : rootContent['automatic-styles'].style) {
			String name = ""+style.@name
			if (!name.startsWith("WW8Num")) buildStyle(style);
		}
		
		def writer = css.newPrintWriter("UTF-8")
		def keys = new ArrayList<String>(translatedStyles.keySet());
		keys.sort()
		//println keys
		for (String name : keys) {
			if (name.equals("")) {
				writer.println("body{\n")
			} else {
				writer.println(".$name{")
			}
			def props = translatedStyles[name]
			for (def key : props.keySet())
				writer.println(""+key+":"+props[key]+";")
			writer.println("}\n")
		}
		writer.close()

		return true;
	}
	
	private void buildDefaultStyle(def style) {
		
	}

	private void buildStyle (def style) {
		
		StringBuffer str = new StringBuffer();
		String name = style.@name
		//if (name.length() == 1) name = ".body"
		if (translatedStyles.containsKey(name)) return
		def props = [:];
		
		String family = ""+style.@family
		String parentname = style['@parent-style-name']
		if ( parentname != null && parentname.length() > 0) {
			String fullparentname = parentname+"_"+family
			//println "build parent: "+parentname+" for $name"
			buildStyle(styles[fullparentname]); // build its parent if any
			def parentprops = translatedStyles[parentname]
			for (def key : parentprops.keySet())
				props[key] = parentprops[key]
		}

		//writer.println(".$name {")
		for (def property : style['paragraph-properties']) {
			for (def attr : property.attributes()) {
				//println attr.getClass()
				switch (attr.getKey()) {
					case "text-align": props[attr.getKey()] = attr.getValue(); break;
					case "margin": props[attr.getKey()] = attr.getValue(); break;
					case "margin-left": props[attr.getKey()] = attr.getValue(); break;
					case "margin-right": props[attr.getKey()] = attr.getValue(); break;
					case "margin-top": props[attr.getKey()] = attr.getValue(); break;
					case "margin-bottom": props[attr.getKey()] = attr.getValue(); break;
					case "padding": props[attr.getKey()] = attr.getValue(); break;
					case "padding-left": props[attr.getKey()] = attr.getValue(); break;
					case "padding-right": props[attr.getKey()] = attr.getValue(); break;
					case "padding-top": props[attr.getKey()] = attr.getValue(); break;
					case "padding-bottom": props[attr.getKey()] = attr.getValue(); break;
					//case "writing-mode": println(attr); break;
				}
			}
			break;
		}

		for (def property : style['text-properties']) {
			//println property.attributes()
			for (def attr : property.attributes()) {
				switch (attr.getKey()) {
					//case "fo:language": println(attr); break;
					case "font-size": props[attr.getKey()] = attr.getValue(); break;
					case "font-name": props["font-family"] = "\""+attr.getValue()+"\""; break;
					case "font-style": props[attr.getKey()] = attr.getValue(); break;
					case "font-weight": props[attr.getKey()] = attr.getValue(); break;
					//case "text-position": str.append(""+attr+"\n"); break;
					case "color": props[attr.getKey()] = attr.getValue(); break;
				}
			}
			break;
		}
				
		translatedStyles[name] = props;
		//println "builded $name"
	}

	static main(args) {
		File unzipDir = new File("/home/mdecorde/xml/txmrefman/TEI/files-Manuel de Référence TXM 0.6_FR.odt.xml")
		File cssFile = new File("/home/mdecorde/xml/txmrefman/TEI/files-Manuel de Référence TXM 0.6_FR.odt.xml", "style.css")

		StylesToCSS converter = new StylesToCSS(unzipDir);
		println converter.process(cssFile);
	}
}
