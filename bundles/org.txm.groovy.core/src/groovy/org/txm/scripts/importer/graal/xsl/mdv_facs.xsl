<?xml version="1.0" encoding="UTF-8"?>
<!-- This is a "facsimilary" stylesheet for Old French XML transcriptions: 
	- most abbreviations and medieval letter variants are rendered with the corresponding 
	Unicode and MUFI characters. "Andron Scriptor Web" font should be installed 
	for correct visualization; - agglutinations and deglutinations are presented 
	"as they are" in the original; - direct speech is marked with darkblue color; 
	- uncertain readings are marked with grey background color; - text deleted 
	in the original is marked with rose background color; - notes are rendered 
	with [*] sign in the text body; the text of the note appears on "mouse-over" 
	and is reproduced in the end. -->

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">
	<xsl:param name="form">
		facsimilaire
	</xsl:param>
	<xsl:variable name="mode">
		facs
	</xsl:variable>

	<xsl:output method="html" encoding="UTF-8" version="4.0"
		indent="yes" />

	<xsl:strip-space elements="*" />

	<!-- <xsl:variable name="title-formal" select="//fileDesc/titleStmt/title[@type='formal']"/> 
		<xsl:variable name="title-normal" select="//fileDesc/titleStmt/title[@type='normal']"/> 
		<xsl:variable name="title-ref" select="//fileDesc/titleStmt/title[@type='reference']"/> 
		<xsl:variable name="author" select="//titleStmt/author"/> -->

	<xsl:include href="mdv_common.xsl" />

	<xsl:template match="//tei:p|//tei:head">
		<p>
			<xsl:if test="@n">
				[ §
				<xsl:value-of select="@n" />
				]&#xa0;
			</xsl:if>
			<xsl:apply-templates mode="facs" />
		</p>
	</xsl:template>

	<xsl:template match="tei:pb" mode="facs">
		<xsl:call-template name="pb_common" />
	</xsl:template>

	<xsl:template match="tei:cb" mode="facs">
		<xsl:call-template name="cb_common" />
	</xsl:template>

	<xsl:template match="tei:note" mode="facs">
		<xsl:call-template name="note_common" />
	</xsl:template>

	<xsl:template match="tei:q" mode="facs">
		<span style="background-color:khaki">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<xsl:template match="tei:gap" mode="facs">
		<xsl:call-template name="gap_common" />
	</xsl:template>


	<xsl:template match="//tei:lb" mode="facs">
		<xsl:choose>
			<xsl:when test="position()=1 and not(ancestor::tei:seg)" />
			<xsl:when test="@ed='norm'" />
			<xsl:when test="preceding::tei:w[1][descendant::bfm:headlb]" />
			<xsl:when test="preceding::tei:w[1][descendant::tei:lb]" />
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//bfm:headlb" mode="facs">
		<br />
	</xsl:template>

	<xsl:template match="//tei:w|//bfm:punct" mode="facs">
		<span style="font-family:Andron Scriptor Web" class="word"
			title="{@type}" id="{@xml:id}">
			<xsl:apply-templates select="descendant::me:facs"
				mode="facs" />
		</span>
		<xsl:choose>
			<xsl:when test="@bfm:aggl" />
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="text()[ancestor::me:facs]"
		mode="facs">
		<xsl:value-of select="translate(., 'i', '&#x0131;')" />
	</xsl:template>

	<xsl:template match="bfm:mdvAbbr" mode="facs">
		<span style="color:darkgreen">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<xsl:template match="tei:sic" mode="facs">
		<span style="color:darkred" title="sic!">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<xsl:template match="tei:subst" mode="facs">
		[
		<xsl:choose>
			<xsl:when test="child::tei:del[@rend='line-through']">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				\
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when test="child::tei:del[@rend='dotobl']">
				<span style="color:red">
					<xsl:value-of select="child::tei:del" />
				</span>
				\
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when test="child::tei:del[@rend='transform']">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				&gt;
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when
				test="child::tei:del[@rend='unmarked'] and child::tei:add[@place='overwrite']">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				+
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when
				test="child::tei:del[@rend='unmarked'] and child::tei:add[contains(@place,'linear')]">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				+ \
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				/
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:otherwise>
		</xsl:choose>
		]
	</xsl:template>

	<xsl:template match="tei:add" mode="facs">
		<span style="color:blue;text-decoration:underline">
			<xsl:choose>
				<xsl:when
					test="@place='interlinear' or @place='supralinear'">
					\
					<xsl:apply-templates mode="facs" />
					/
				</xsl:when>
				<xsl:when test="starts-with(@place,'margin')">
					\\
					<xsl:apply-templates mode="facs" />
					//
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="facs" />
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>

	<xsl:template match="tei:del" mode="facs">
		<xsl:choose>
			<xsl:when test="@rend='dotbl'">
				<span style="color:red">
					<xsl:apply-templates mode="facs" />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="text-decoration:line-through;color:red">
					<xsl:apply-templates mode="facs" />
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:unclear" mode="facs">
		<span style="background-color:lightgrey">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<xsl:template match="bfm:lettrine" mode="facs">
		<font color="{@color}" size="{@size}00%" style="font-weight:bold">
			<xsl:apply-templates mode="facs" />
		</font>
	</xsl:template>

	<xsl:template match="bfm:sb" mode="facs">
		<xsl:text>&#xa0;</xsl:text>
	</xsl:template>

	<xsl:template match="//tei:hi[@rend='sup']" mode="facs">
		<sup>
			<xsl:apply-templates mode="facs" />
		</sup>
	</xsl:template>


	<!-- <xsl:template match="tei:name"> <xsl:value-of select="@reg"/> </xsl:template> -->
</xsl:stylesheet>
