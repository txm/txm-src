// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
// $LastChangedDate: 2011-06-03 12:37:57 +0200 (Fri, 03 Jun 2011) $
// $LastChangedRevision: 1867 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.xmltxmpara;

import org.txm.scripts.importer.xmltxmpara.compiler;
import org.txm.scripts.importer.xml.pager_old;
import org.txm.objects.*;
import org.txm.utils.*
import org.txm.utils.io.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;
import org.txm.scripts.importer.*;
import org.txm.importer.cwb.CwbAlign;
import org.txm.importer.cwb.PatchCwbRegistry;
import org.w3c.dom.Element

String userDir = System.getProperty("user.home");
String rootDir;
String lang;
String encoding;
String model;
String basename;
String xsl;

try {rootDir = rootDirBinding;}
catch (Exception)
{	println "DEV MODE";//exception means we debug
	if (!org.txm.Toolbox.isInitialized()) {
		rootDir = userDir+"/xml/qgraal";
		Toolbox.setParam(Toolbox.INSTALL_DIR,new File("/usr/lib/TXM"));
		Toolbox.setParam(Toolbox.METADATA_ENCODING, "UTF-8");
		Toolbox.setParam(Toolbox.METADATA_COLSEPARATOR, ",");
		Toolbox.setParam(Toolbox.METADATA_TXTSEPARATOR, "\"");
		Toolbox.setParam(Toolbox.USER_TXM_HOME, new File(System.getProperty("user.home"),"TXM"));
	}
}

File srcDir = new File(rootDir);
File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+srcDir.getName());
binDir.deleteDir();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

File srcParamFile = new File(rootDir, "import.xml");
File paramFile = new File(binDir, "import.xml")
if (!srcParamFile.exists()) {
	println "Stop import: can't find params file: "+srcParamFile
	return;
}
FileCopy.copy(srcParamFile, paramFile);
if (!paramFile.exists()) {
	println "Stop import: failed to copy param file: "+srcParamFile+" to "+paramFile
	return;
}
BaseParameters params = new BaseParameters(paramFile);
params.load();
basename = params.name;

File paraDir = new File(binDir, "paracorpus/");
paraDir.deleteDir();
paraDir.mkdir();

// Apply XSL
if (xsl != null && xsl.trim().length() > 0) {
	if (!ApplyXsl2.processImportSources(new File(xsl, new File(rootDir), srcDir)))
	return; // error durring process
	srcDir = new File(binDir, "src");
}

//copy txm files
List<File> srcfiles = srcDir.listFiles();
for (File f : srcfiles) {// check XML format, and copy file into binDir
	if (f.getName().equals("import.xml") || f.getName().matches("metadata\\.....?") || f.getName().endsWith(".properties"))
	continue;
	if (ValidateXml.test(f)) {
		FileCopy.copy(f, new File(paraDir, f.getName()));
	} else {
		println "Won't process file "+f;
	}
}

if (paraDir.listFiles() == null) {
	println "No txm file to process"
	return;
}

//contains the text files associated to a corpus
println "Getting lang of each corpus and splitting corpus: "+params.corpora.keySet()
for (String corpusname : params.corpora.keySet()) {
	
	lang = params.corpora.get(corpusname).getAttribute("lang");
	encoding = params.corpora.get(corpusname).getAttribute("encoding");
	model = lang
	
	print "."
	println "split in"
	File txmDir = new File(binDir,"txm/$corpusname");
	txmDir.deleteDir();
	txmDir.mkdirs();
	
	def corpusElem = params.corpora.get(corpusname)
	String corpuslang = corpusElem.getAttribute("lang")
	
	println "split corpus $corpusname by TEI@id"
	File f = new File(paraDir, corpusname+".xml")
	if (!f.exists()) {
		println "source file is missing: "+f;
	}
	def splitter = new SplitBy(f);
	if (splitter.process(txmDir, "TEI", "id")) {
		for (File textfile : splitter.getFiles()) {
			params.addText(corpusElem, basename, textfile)
		}
	} else {
		println "failed to split corpus "+corpusname;
		return false;
	}
}

println "-- COMPILING - Building Search Engine indexes"
for (String corpusname : base.getCorpora().keySet()) {
	println "Build corpus "+corpusname
	def c = new compiler();
	c.setDebug();
	//c.setCwbPath("~/TXM/cwb/bin");
	if (!c.run(corpusfiles.get(corpusname), binDir, corpusname, basename)) {
		println "Compiler failed"
		return;
	}
}

println "-- ALIGN - aligning CQP corpus with each others"
for (String alignement : parameters.getAlignements()) {
	println "aligning..."+alignement
		String alignStructure = parameters.getStructure(alignement);
		String alignAttribute = "align";
		
		String regpath = new File(binDir,"registry").getAbsolutePath();
		
		if (!(CwbAlign.isExecutableAvailable())) {
					println ("Error: CWB executables not well set.")
					return false;
				}
		
		println "aligned corpora: "+parameters.getTargets(alignement);
		for (String corpusName : parameters.getTargets(alignement)) {
			for (String targetName : parameters.getTargets(alignement)) {
				if (corpusName != targetName) {
					try {
						println "build align index: "+corpusName+" with "+targetName
						//./cwb-align -V seg_id -r ~/TXM/corpora/tmxtest/registry/ tmxen tmxfr seg
						CwbAlign tt = new CwbAlign();
						tt.setV(alignStructure+"_"+alignAttribute);
						tt.setr(regpath);
						tt.cwbalign(corpusName, targetName, alignStructure);
						
						//patch registry
						println " patch reg : "+new File(regpath, corpusName.toLowerCase())
						if (PatchCwbRegistry.patchAlignment(new File(regpath, corpusName.toLowerCase()), targetName.toLowerCase()))
						{
							// ./cwb-align-encode -D -r ~/TXM/corpora/tmxtest/registry/ -v out.align
							tt = new CwbAlign();
							tt.setD();
							tt.setv();
							tt.setr(regpath);
							tt.cwbalignencode("out.align");
						}
					} catch (IOException e) {
						org.txm.utils.logger.Log.printStackTrace(e);
						return;
					}
				}
			}
		}

}

println "-- EDITION - Building edition"
new File(binDir,"HTML").deleteDir();
new File(binDir,"HTML").mkdir();
new File(binDir,"HTML/default").mkdir();
List<File> filelist = new File(binDir,"txm").listFiles();
def second = 0

println "Paginating text: "
for(String textname : base.getTextsID())
{
	Text text = base.getText(textname);
	File srcfile = text.getSource();
	File resultfile = new File(binDir,"HTML/"+srcfile.getName().substring(0,srcfile.getName().length()-4)+".html");
	lang = text.getAttribute("lang").toString();
	List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
	List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
		
	if (second) { print(", ") }
	if (second > 0 && (second % 5) == 0) println ""
	print(srcfile.getName());
	second++
	
	def ed = new pager_old(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter, 1000, basename, "pb");
	
	Edition editionweb = text.addEdition("default", "html", resultfile);
	for(int i = 0 ; i < ed.getPageFiles().size();i++)
	{
		File f = ed.getPageFiles().get(i);
		String idx = ed.getIdx().get(i);
		editionweb.addPage(f,idx);
	}
}

DomUtils.print(params.root.getOwnerDocument())