/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-09-16 14:31:48 +0200 (lun. 16 sept. 2013) $
// $LastChangedRevision: 2535 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.bvh;

import java.io.File;
import org.txm.scripts.importer.bvh.importer;
import org.txm.scripts.importer.bvh.compiler;
import org.txm.scripts.importer.bvh.annotate;
import org.txm.scripts.importer.bvh.pager;
import org.txm.objects.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;

// TODO: Auto-generated Javadoc
/**
 * Run.
 *
 * @return the java.lang. object
 * @author mdecorde
 */

String userDir = System.getProperty("user.home");
String lang;
String encoding;
String rootDir;
String model;
String basename;
try{rootDir = rootDirBinding;lang=langBinding;encoding=encodingBinding;model=modelBinding;basename=basenameBinding}
catch(Exception)
{	println "DEV MODE";//exception means we debug
	if(!org.txm.Toolbox.isInitialized())
	{
		rootDir = userDir+"/xml/bvh/";
		lang="fr";
		encoding= "UTF-8";// not used by bvh
		model="rgaqcj"; //not used by bvh
		basename="bvh"
		Toolbox.workspace = new Workspace(new File(userDir,"TXM/corpora/default.xml"));
		Toolbox.setParam(Toolbox.INSTALL_DIR,new File(userDir,"TXM"));
		Toolbox.setParam(Toolbox.METADATA_ENCODING, "UTF-8");
		Toolbox.setParam(Toolbox.METADATA_COLSEPARATOR, ",");
		Toolbox.setParam(Toolbox.METADATA_TXTSEPARATOR, "\"");
		Toolbox.setParam(Toolbox.USER_TXM_HOME, new File(System.getProperty("user.home"),"TXM"));
	}
}

println "IMPORTER"
new importer().run(new File(rootDir), basename);

File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+basename);
rootDir = binDir.getAbsolutePath();

println "ANNOTATE - fr "
new Annotate().run(new File(rootDir),"fr.par");//the files in ./txm

println "ANNOTATE - afr"
def secondannotator = new Annotate()
secondannotator.run(new File(rootDir),"afr.par");//the files in ./txm

println "--COMPILING"
File dir = new File(rootDir);
def c = new compiler();
c.setDebug();
//c.setCwbPath("~/TXM/cwb/bin");
c.setLang(lang);
if (!c.run(dir)) {
	println "Compiler failed"
	return;
}

//move registry file to cwb registry dir
File registryfile = new File(rootDir+"/registry",basename);
if(registryfile.exists())
	org.txm.utils.io.FileCopy.copy(registryfile,new File(Toolbox.getTxmHomePath(),"registry/"+basename))

Workspace w = org.txm.Toolbox.workspace;
Project p = w.getProject("default")
p.removeBase(basename)
Base b = p.addBase(basename);
b.addDirectory(new File(rootDir,"txm"));
b.setAttribute("lang", lang)
b.propagateAttribute("lang")

println "-- EDITION"
new File(rootDir+"/HTML/").deleteDir()
new File(rootDir+"/HTML/").mkdir();
new File(rootDir+"/HTML/default/").mkdir();
files = new File(rootDir,"txm").listFiles();

for(Text text : b.getTexts())
{
	File srcfile = text.getSource();
	File resultfile = new File(rootDir+"/HTML",srcfile.getName().substring(0,srcfile.getName().length()-4)+".html");
	List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
	List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
	println("Building edition  : "+srcfile+" to : "+resultfile );
	
	def ed = new pager(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter,600);
	Edition editionweb = text.addEdition("default","html",resultfile);

	for(int i = 0 ; i < ed.getPageFiles().size();i++)
	{
		File f = ed.getPageFiles().get(i);
		String idx = ed.getIdx().get(i);
		editionweb.addPage(f,idx);
	}
	
//	Edition editionbp = text.addEdition("onepage","html",resultfile);
//	editionbp.addPage(resultfile,ed.getIdx().get(0));
}

w.save();