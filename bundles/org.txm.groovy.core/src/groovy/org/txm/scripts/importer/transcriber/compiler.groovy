// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.importer.transcriber

import java.io.File;

import javax.xml.stream.*;

import java.io.OutputStreamWriter;
import java.util.LinkedHashMap;
import org.txm.objects.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.cwb.CwbProcess;
import org.txm.importer.cwb.PatchCwbRegistry;
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.Pair;

/**
 * The Class compiler.
 */
class compiler {
	
	boolean ADD_TEXTID_TO_REF = true
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The output. */
	OutputStreamWriter output;
	
	/** The basename. */
	String corpusname;
	
	/** The projectname. */
	String projectname
	
	/** The outdir. */
	String outdir;
	
	/** The debug. */
	boolean debug = false;
	
	/** The indexInterviewer: index interviewer speech if true. */
	boolean indexInterviewer = true;
	
	/** The trans. */
	HashMap<String, ArrayList<Pair<String, String>>> trans;
	
	/** The speakers. */
	HashMap<String, ArrayList<Pair<String, String>>> speakers;
	
	/** The speakersname. */
	HashMap<String, String> speakersname = new HashMap<String, String>();
	
	/** The topics. */
	HashMap<String, ArrayList<Pair<String, String>>> topics;
	
	
	/** The interviewers regex */
	def interviewers = null
	static LinkedHashSet<String> sectionAttrs;
	static LinkedHashSet<String> spAttrs;
	static LinkedHashSet<String> uAttrs;
	
	/** The anatypes. */
	private static anatypes = []
	private static anavalues = [:]
	
	/**
	 * Removes the interviewers.
	 *
	 * @param value the value
	 * @return the java.lang. object
	 */
	public setIndexInterviewer(boolean value) {
		this.indexInterviewer = value;
	}
	
	File cqpFile
	LinkedHashMap<String, LinkedHashMap<String, String>> projectionsFromValues = new LinkedHashMap<String, LinkedHashMap<String, String>>(); // values of properties to inject
	LinkedHashMap<String, LinkedHashMap<String, ArrayList<ArrayList>>> projectionsToDo = new LinkedHashMap<String, LinkedHashMap<String, ArrayList<ArrayList>>>(); // list of projections to do
	
	/**
	 * Run.
	 *
	 * @param xmlfiles the xmlfiles
	 * @param basename the basename
	 * @param projectname the projectname
	 * @param outdir the outdir
	 * @return true, if successful
	 */
	public boolean run(Project project, List<File> xmlfiles, String corpusname, String projectname, File binDir) {
		
		//println "run compiler with $xmlfiles, $basename and $outdir"
		this.outdir = binDir;
		this.corpusname = corpusname;
		this.projectname = projectname;
		
		anatypes = ["event"] // reset
		anavalues = [:] // reset
		
		sectionAttrs = new LinkedHashSet<String>() // reset section attributs set
		spAttrs = new LinkedHashSet<String>() // reset section attributs set
		uAttrs = new LinkedHashSet<String>() // reset section attributs set
		
		CorpusBuild corpus = project.getCorpusBuild(project.getName(), MainCorpus.class);
		if (corpus != null) {
			if (project.getDoUpdate()) {
				corpus.clean(); // remove old files
			} else {
				corpus.delete(); // remove old files and TXMResult children
			}
		} else {
			corpus = new MainCorpus(project);
			corpus.setID(project.getName());
			corpus.setName(project.getName());
		}
		corpus.setDescription("Built with the XML-TRS import module");
		
		cqpFile = new File(binDir,"cqp/"+corpusname+".cqp");
		cqpFile.delete()
		new File(binDir,"cqp").mkdirs()
		new File(binDir,"data").mkdirs()
		new File(binDir,"registry").mkdirs()
		
		// get all anatypes
		for (File f : xmlfiles) {
			getAnaTypes(f)
		}
		
		// Building projections datas to use for each step
		String projectionsParameterValue = project.getTextualPlan("Projections").trim()
		projectionsParameterValue = projectionsParameterValue.replace("\n", "\t")
		def projectionsParameter = projectionsParameterValue.split("\t");
		if (projectionsParameterValue.length() > 0 && projectionsParameterValue.contains("->")) {
			for (def projection : projectionsParameter) {
				if (!projection.contains("->")) continue;
				String[] fromTo = projection.split("->", 2)
				String from = fromTo[0].trim()
				String to = fromTo[1].trim()
				if (projection.contains("->") && from.contains("_") && to.contains("_")) {
					String toStructure = to.substring(0, to.indexOf("_"))
					String toStructureProperty = to.substring(to.indexOf("_") + 1)
					String fromStructure = from.substring(0, from.indexOf("_"))
					String fromStructureProperty = from.substring(from.indexOf("_") + 1)
					
					if (!projectionsToDo.containsKey(toStructure)) {
						projectionsToDo[toStructure] = new LinkedHashMap<String, ArrayList<ArrayList>>();
					}
					if (!projectionsToDo[toStructure].containsKey(fromStructure)) {
						projectionsToDo[toStructure][fromStructure] = new ArrayList<ArrayList>();
					}
					projectionsToDo[toStructure][fromStructure].add([toStructureProperty, fromStructureProperty])
					
					if (!projectionsFromValues.containsKey(fromStructure)) projectionsFromValues[fromStructure] = new LinkedHashMap<String, String>();
					projectionsFromValues[fromStructure][fromStructureProperty] = "";
				}
			}
		}
		
		//println "ANATYPES: "+anatypes
		if (!createOutput(cqpFile)) return false;
		output.write("<txmcorpus lang=\"fr\">\n")
		output.close();
		
		println("Compiling "+xmlfiles.size()+" files")
		ConsoleProgressBar cpb = new ConsoleProgressBar(xmlfiles.size())
		for (File txmFile :xmlfiles) {
			if (txmFile.exists()) {
				cpb.tick()
				if (!process(txmFile)) {
					println("Failed to compile "+txmFile)
				}
			}
		}
		cpb.done()
		if (!createOutput(cqpFile)) return false;
		output.write("</txmcorpus>\n")
		output.close();
		
		//2- Import into CWB
		File registryFile = new File(binDir, "registry/"+corpusname.toLowerCase())
		File dataDir = new File(binDir, "data/$corpusname")
		
		new File(binDir, "registry").mkdir();
		if (!new File(binDir, "registry").exists()) {
			println "Can't create registry directory"
			return false;
		}
		
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		CwbEncode cwbEn = new CwbEncode();
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbEn.setDebug(debug);
		cwbMa.setDebug(debug);
		
		String uAttr = "u:0";
		for (String attr : uAttrs) {
			uAttr += "+"+attr
		}
		
		String spAttr = "sp:0";
		for (String attr : spAttrs) {
			spAttr += "+"+attr
		}
		
		String sectionAttr = "div:0" // "div:0+id+topic+endtime+starttime+type"
		for (String attr : sectionAttrs) {
			sectionAttr += "+"+attr
		}
		
		String textAttr ="text:0+base+project"
		if (trans != null) {
			for (String key : trans.keySet()) {
				for (Pair p : trans.get(key)) {
					if (ignoreTranscriberMetadata) {
						String meta =p.getFirst();
						if (meta != "scribe" && meta != "audio_filename" &&
								meta != "version" && meta != "version_date")
							textAttr+="+"+meta
					} else {
						textAttr+="+"+p.getFirst()
					}
				}
				break;
			}
		}
		
		List<String> pargs = ["spk", "id", "entitytype", "entityid"]
		for (String ana : anatypes) if (!pargs.contains(ana)) pargs.add(ana)
		
		String[] pAttributes = pargs
		
		String[] sAttributes = ["txmcorpus:0+lang", uAttr , textAttr, "event:0+id+desc+type+extent", sectionAttr, spAttr];
		
		// registering the projected structure properties
		//println "registering: $projectionsToDo"
		for (String struct : projectionsToDo.keySet()) {
			for (String struct2 : projectionsToDo[struct].keySet()) {
				for (def couple : projectionsToDo[struct][struct2]) {
					//sattrsListener.getStructs()[struct].add(couple[0])
					//println "add $struct $couple"
					for (int i = 0 ; i < sAttributes.size() ; i++) {
						if (sAttributes[i].startsWith(struct+":") && !sAttributes[i].contains(couple[0])) {
							sAttributes[i] = sAttributes[i] + "+"+couple[0]
						}
					}
				}
			}
		}
		
		println "pAttributes: $pAttributes"
		println "sAttributes: $sAttributes"
		//return;
		try {
			cwbEn.run(dataDir.getAbsolutePath(), cqpFile.getAbsolutePath(),
					registryFile.getAbsolutePath(), pAttributes, sAttributes);
			if (!registryFile.exists()) {
				println "Error: The registry file was not created: $registryFile. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(corpusname, registryFile.getParent());
			
		} catch (Exception ex) {System.out.println(ex); return false;}
		
		if (project.getCleanAfterBuild()) {
			new File(project.getProjectDirectory(), "cqp").deleteDir()
		}
		
		return true;
	}
	
	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(File f){
		try {
			//File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(f,f.exists())) , "UTF-8");
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/** The text_id. */
	String text_id
	
	/** The u opened. */
	boolean uOpened = false;
	
	/** The idturn. */
	int idturn = 1;
	
	/** The idsection. */
	int idsection = 1;
	
	/** The idu. */
	int idu = 1;
	
	/** The idevent. */
	int idevent = 1;
	
	/** The events. */
	List<String> events = [];
	static int vEntityId = 0;
	static int vEntityIdCount = 1;
	
	protected void writeProjections(String localname) {
		if (projectionsToDo.containsKey(localname)) {
			for (String from : projectionsToDo[localname].keySet()) {
				for (def couple : projectionsToDo[localname][from]) {
					def o = couple[0]
					def p = couple[1]
					def r = projectionsFromValues[from][p]
					//println "o=$o p=$p r=$r"
					output.write(" "+o+"=\""+r+"\"");
				}
			}
		}
	}
	
	/**
	 * Process.
	 *
	 * @param xmlfile the xmlfile
	 * @return true, if successful
	 */
	private boolean process(File xmlfile) {
		text_id = xmlfile.getName();
		text_id = text_id.substring(0, text_id.length() -4);
		
		idturn = 1;
		idsection = 1;
		idu = 1;
		
		boolean flagAna;
		boolean flagForm;
		boolean flagWord;
		String vWord="";
		String vForm="";
		String vAna="";
		String vEvents = "N/A";
		String vEntityType = "N/A"
		String wordid= "";
		//def wordattributes = [:];
		String anatype = "";
		String anaresp = "";
		String anavalue = "";
		
		String formatedTime;
		
		LinkedHashMap<String, String> anahash = new LinkedHashMap<String, String>();
		String currentType;
		
		URL url = xmlfile.toURI().toURL();
		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		String filename = xmlfile.getName()
		String textid = filename.substring(0, filename.length() - 4);
		
		createOutput(cqpFile);
		String localname;
		
		//get all metadatas declared before Episode tag
		speakers = new HashMap<String, ArrayList<Pair<String, String>>>();
		trans = new HashMap<String, ArrayList<Pair<String, String>>>();
		topics = new HashMap<String, ArrayList<Pair<String, String>>>();
		//println "parse infos"
		parseInfos();
		
		//		println "Trans: $trans"
		//		println "Topics: $topics"
		//		println "Speakers: $speakers"
		//		def transproperties = ""
		//		for (String key : trans.keySet()) {
		//			for (Pair p : trans.get(key))
		//				transproperties+="\t"+p.getSecond();
		//			break;
		//		}
		//		println "Trans properties: "+transproperties
		List<String> localspeakers;
		
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			//print "event: "+event +" "
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
				
					if (projectionsFromValues.containsKey(localname)) { // get projections values
						for (String attr : projectionsFromValues[localname].keySet()) {
							projectionsFromValues[localname][attr] = parser.getAttributeValue(null, attr);
						}
					}
				
				//println localname
					switch(localname) {
						
						case "div":
							output.write("<div");
							for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
								String name = parser.getAttributeLocalName(i).replace("_","").toLowerCase()
								output.write(" "+name+"=\""+parser.getAttributeValue(i).replace("\"", "&quot;")+"\"");
								sectionAttrs << name
							}
							writeProjections(localname)
							output.write ">\n"
							break;
						case "sp":
							output.write("<sp");
							for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
								String name = parser.getAttributeLocalName(i).replace("_","").toLowerCase()
								output.write(" "+name+"=\""+parser.getAttributeValue(i).replace("\"", "&quot;")+"\"");
								spAttrs << name
							}
							writeProjections(localname)
							output.write ">\n"
							break;
						case "u":
							output.write("<u");
							for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
								String name = parser.getAttributeLocalName(i).replace("_","").toLowerCase()
								output.write(" "+name+"=\""+parser.getAttributeValue(i).replace("\"", "&quot;")+"\"");
								if (name == "time") {
									formatedTime = parser.getAttributeValue(i)
								} else if (name == "who") {
									u_name = parser.getAttributeValue(i)
								}
								
								uAttrs << name
							}
							writeProjections(localname)
							output.write ">\n"
							break;
						case "event":
							output.write("<event");
							writeAttributes();
							writeProjections(localname)
							output.write ">\n"
						
							if (parser.getAttributeValue(null, "type") == "entities") {
								if (parser.getAttributeValue(null, "extent") == "begin") {
									vEntityType = parser.getAttributeValue(null, "desc");
									vEntityId = vEntityIdCount++;
								} else {
									vEntityType = "N/A";
									vEntityId = 0;
								}
							} else if (parser.getAttributeValue(null, "type") == "pronounce") {
								if (parser.getAttributeValue(null, "extent") == "begin")
									events.add(parser.getAttributeValue(null, "desc"))
								else if (parser.getAttributeValue(null, "extent") == "end")
									events.remove(parser.getAttributeValue(null, "desc"))
								vEvents = "";
								for (String s : events) {
									vEvents += s+"#";
								}
								if (vEvents.length() > 0) {
									vEvents = vEvents.substring(0, vEvents.length()-1);
								} else {
									vEvents = ""
								}
							}
							break;
						case "w":
							for (int i = 0 ; i < parser.getAttributeCount(); i++) {
								if (parser.getAttributeLocalName(i).equals("id")) {
									wordid = parser.getAttributeValue(i);
									break;
								}
							}
							anavalues = [:];
							break;
						case "form":
							flagForm = true;
							vForm = "";
							vAna ="";
							break;
						case "ana":
							flagAna = true;
							anavalue = ""
							anaresp = ""
							anatype = ""
							for (int i = 0 ; i < parser.getAttributeCount(); i++) {
								if (parser.getAttributeLocalName(i).equals("type")) {
									anatype = parser.getAttributeValue(i).substring(1);//remove the #
									continue;
								}
								if (parser.getAttributeLocalName(i).equals("resp")) {
									anaresp = parser.getAttributeValue(i).substring(1);//remove the #
									continue;
								}
							}
							break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
				
					if (projectionsFromValues.containsKey(localname)) { // reset values
						for (String attr : projectionsFromValues[localname].keySet()) {
							projectionsFromValues[localname][attr] = "";
						}
					}
				
					switch (localname) {
						case "text":
							output.write("</text>\n")
							break;
						case "Topics":
							break;
						case "Topic":
							break;
						case "Speakers":
							break;
						case "Speaker":
							break;
						case "Episode":
							break;
						case "div":
							output.write("</div>\n")
							break;
						case "sp":
							output.write("</sp>\n")
							break;
						case "u":
							output.write("</u>\n")
							break;
						case "event":
							output.write("</event>\n")
							break;
						case "form":
							flagForm = false;
							break;
						case "ana":
							if (!anavalues.containsKey(anatype)) { // overwrite existing ana only if it's not the #txm resp
								anavalues.put(anatype, anavalue)
							} else if (!anaresp.equals("txm")) {
								anavalues.put(anatype, anavalue)
							}
							flagAna = false;
							break;
						case "w":
							String isEnq = interviewers != null && u_name =~ interviewers?"*":"";
							
							vForm +="\t"+u_name
						
						// concat entity and entity ID
							vAna+= "\t"+vEntityType+"\t"+vEntityId;
						
						//concat ana values
							for (String type : anatypes) {
								def v = anavalues.get(type);
								if (v == null) v = "";
								
								if ("event" == type) {
									if (v.length() > 0)
										vAna+="\t#"+v;
									else
										vAna+="\t";
									
									//concat <Event> values
									if (vEvents != null && vEvents.length() > 0 && vEvents != "N/A")
										vAna += "#"+vEvents;
								} else {
									vAna+="\t"+v;
								}
							}
						
							vForm = vForm.replaceAll("\n", "").replaceAll("&", "&amp;").replaceAll("<", "&lt;");
						
							if (interviewers != null && !indexInterviewer) { // we must remove some words
								if (!(u_name =~ interviewers)) { // keep what is now an interviewer
									output.write(vForm+"\t"+wordid+vAna+"\n");
								}
							} else {
								output.write(vForm+"\t"+wordid+vAna+"\n");
							}
						
							vAna = "";
							vForm = "";
							break;
					}
					break
				case XMLStreamConstants.CHARACTERS:
					if (flagForm)
						vForm += parser.getText().trim();
					if (flagAna) {
						anavalue += parser.getText().trim();
					}
					break;
			}
		}
		
		parser.close();
		inputData.close();
		output.close();
		return true;
	}
	
	/** The u_name. */
	String u_name;
	
	/**
	 * Write start tag.
	 */
	private void writeStartTag() {
		output.write("<"+parser.getLocalName());
		writeAttributes();
		output.write ">\n"
	}
	
	/**
	 * Write attributes.
	 */
	private void writeAttributes() {
		for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
			output.write(" "+parser.getAttributeLocalName(i).replace("_","").toLowerCase()+"=\""+parser.getAttributeValue(i).replace("\"", "&quot;")+"\"");
		}
	}
	
	private void getAnaTypes(File xmlFile) {
		inputData = xmlFile.toURI().toURL().openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		String ana = "ana"
		HashSet<String> types = new HashSet<String>();
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) { // start elem
				if (ana.equals(parser.getLocalName())) { // ana elem
					for (int i = 0 ; i < parser.getAttributeCount(); i++) { // find @type
						if ("type".equals(parser.getAttributeLocalName(i))) { // @type
							types.add(parser.getAttributeValue(i).substring(1)); //remove the #
							break;
						}
					}
				}
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		
		for (String type : types) {
			if (!anatypes.contains(type)) {
				anatypes << type
			}
		}
	}
	
	/**
	 * Write start tag.
	 *
	 * @param id the id
	 */
	private void writeStartTag(int id) {
		output.write("<"+parser.getLocalName().toLowerCase());
		output.write(" id=\""+id+"\"");
		writeAttributes();
		output.write ">\n"
	}
	
	/**
	 * Write end tag.
	 */
	private void writeEndTag() {
		output.write("</"+parser.getLocalName().toLowerCase()+">\n");
	}
	
	/** The ignore transcriber metadata. */
	boolean ignoreTranscriberMetadata = false;
	
	/**
	 * Sets the ignore transcriber metadata.
	 *
	 * @param state the new ignore transcriber metadata
	 */
	public void setIgnoreTranscriberMetadata(boolean state) {
		this.ignoreTranscriberMetadata = state;
	}
	
	public static int MAXATTRIBUTEVALUELENGTH = 8191;
	
	/**
	 * Parses the infos.
	 */
	private void parseInfos() { //until tag Episode
		String localname;
		//assert(parser != null);
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) {
				localname = parser.getLocalName()
				
				if (projectionsFromValues.containsKey(localname)) { // get projections values
					for (String attr : projectionsFromValues[localname].keySet()) {
						projectionsFromValues[localname][attr] = parser.getAttributeValue(null, attr)
					}
				}
				
				switch (localname) {
					case "text":
						output.write("<text project=\""+projectname+"\" base=\""+corpusname+"\"")
						for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
							
							String value = parser.getAttributeValue(i).replace("\"", "&quot;")
							if (value.length() > MAXATTRIBUTEVALUELENGTH) {
								//								value = value.substring(0, MAXATTRIBUTEVALUELENGTH-1)
								println "WARNING: attribute value is too long ( > $MAXATTRIBUTEVALUELENGTH). The value will be truncated to: $value"
							}
							
							if (ignoreTranscriberMetadata) {
								if (parser.getAttributeLocalName(i) != "scribe" &&
										parser.getAttributeLocalName(i) != "audio_filename" &&
										parser.getAttributeLocalName(i) != "version" &&
										parser.getAttributeLocalName(i) != "version_date") {
									output.write(" "+parser.getAttributeLocalName(i).replace("_","").toLowerCase()+"=\""+value+"\"")
								}
							} else {
								output.write(" "+parser.getAttributeLocalName(i).replace("_","").toLowerCase()+"=\""+value+"\"")
							}
						}
					
						output.write ">\n"
					
						ArrayList list = new ArrayList<Pair<String, String>>()
						trans.put("trans", list)
					
						for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
							list.add(new Pair(parser.getAttributeLocalName(i).replace("_","").toLowerCase(), parser.getAttributeValue(i)))
							if (parser.getAttributeLocalName(i).equals("interviewer-id-regex"))
								interviewers = /${parser.getAttributeValue(i)}/
						}
						return
					case "Topic":
						String id = parser.getAttributeValue(null, "id")
						if (id != null) {
							ArrayList list = new ArrayList<Pair<String, String>>()
							topics.put(id, list)
						} else {
							println "found tag $localname with no id"
						}
						break;
					case "Speaker":
					//case "Trans":
						String id = parser.getAttributeValue(null, "id")
						String name = parser.getAttributeValue(null, "name")
						if (id != null && name != null) {
							speakersname.put(id, name);
						} else {
							println "found tag $localname with no id ($id)or name ($name)"
							return;
						}
						if (id != null) {
							ArrayList list = new ArrayList<Pair<String, String>>()
							speakers.put(id, list);
							
							for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
								list.add(new Pair(parser.getAttributeLocalName(i), parser.getAttributeValue(i)));
							}
						} else {
							println "found tag $localname with no id"
							return;
						}
						break;
				}
			}
		}
	}
	
	/**
	 * Sets the debug.
	 */
	public void setDebug() {
		debug = true;
	}
}
