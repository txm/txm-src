// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.importer.transcriber

import java.io.File;

import java.util.ArrayList;

import javax.xml.stream.*

import org.txm.importer.ApplyXsl2
import org.txm.metadatas.MetadataGroup
import org.txm.metadatas.Metadatas
import org.txm.utils.TimeFormatter
import org.txm.utils.io.FileCopy;
import org.txm.objects.Project

/**
 *  Transcriber pager
 *  
 *  @author mdecorde
 *  
 */
class pager {
	
	boolean SIMPLE_TOOLTIP = false; // show less properties in word tooltips
	String ENQ_HIGHLIGHT_ELEMENT = "b"
	
	List<String> NoSpaceBefore;
	
	/** The No space after. */
	List<String> NoSpaceAfter;
	
	/** The pages. */
	def pages = []
	def indexes = []
	
	/** The wordcount. */
	int wordcount = 0
	
	/** The pagecount. */
	int pagecount = 0
	
	/** The wordmax. */
	int wordmax = 10
	
	/** The wordid. */
	String wordid;
	
	/** The first word. */
	boolean firstWord = true
	
	boolean paginate = true
	boolean paginateSections = true
	boolean enableCollapsibles = false;
	boolean displayLocutors = true;
	
	/** The wordvalue. */
	String wordvalue;
	
	/** The interpvalue. */
	String interpvalue;
	
	/** The lastword. */
	String lastword = " ";
	
	/** The wordtype. */
	String wordtype;
	
	/** The flagword. */
	boolean flagword = false;
	/** The flagu. */
	boolean flagu = false;
	
	/** The flagform. */
	boolean flagform = false;
	
	/** The flaginterp. */
	boolean flaginterp = false;
	
	boolean flagcomment = false;
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The writer. */
	XMLStreamWriter writer;
	BufferedOutputStream output;
	
	File txmfile;
	
	File outfile;
	
	String corpusname =""
	String cuttingTag = "pb"
	String txtname
	File htmlDir
	File defaultDir
	Metadatas metadatas
	
	def interviewers = null
	def eventTranslations = ["^^":"mot inconnu", "?":"orthographe incertaine",
		"()":"rupture de syntaxe", "b":"bruit indéterminé",
		"*":"mot corrigé",
		"bb":"bruit de bouche", "bg":"bruit de gorge",
		"ch":"voix chuchotée", "conv":"conversations de fond",
		"e":"expiration", "i":"inspiration",
		"mic":"bruits micro", "n":"reniflement",
		"nontrant":"non transcrit", "pap":"froissement de papiers",
		"pf":"souffle", "pi":"inintelligible",
		"pif":"inaudible", "r":"respiration",
		"rire":"rire du locuteur", "shh":"soufle électrique",
		"sif":"sifflement du locuteur", "tx":"toux"];
	String currentUTime = ""
	String startTimeSp = ""
	String endTimeSp = ""
	String startTimeU = "0"
	String previousStartTimeU = "0"
	boolean bold = false
	int writenLength = 0
	boolean spokenTurn = false
	boolean firstSync = false
	boolean firstWho = false
	int nSilence = 0
	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 * @param metadatas the metadatas
	 */
	pager(File txmfile, File htmlDir, String txtname, List<String> NoSpaceBefore,
	List<String> NoSpaceAfter, String corpusname, String cuttingTag, Metadatas metadatas, Project project) {
		this.metadatas = metadatas
		this.cuttingTag = cuttingTag;
		this.corpusname = corpusname;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = txmfile.toURI().toURL();
		this.txmfile = txmfile;
		this.htmlDir = htmlDir;
		this.txtname = txtname;
		this.wordmax = project.getEditionDefinition("default").getWordsPerPage();
		this.paginate = project.getEditionDefinition("default").getPaginateEdition()
		this.paginateSections = project.getImportParameters().getBoolean("create_section_pages", true)
		this.enableCollapsibles = project.getEditionDefinition("default").getEnableCollapsibleMetadata();
		this.displayLocutors = project.getImportParameters().getBoolean("display_locutors", true);
		
		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		
		defaultDir = new File(htmlDir, "default")
		defaultDir.mkdir()
		new File(htmlDir, "onepage").mkdir()
		outfile = new File(htmlDir, "onepage/${txtname}.html");
		createOutput(outfile)
		
		try {
			process();
		} catch(Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			if (writer != null) {
				writer.close();
				output.close();
			}
		}
	}
	
	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			//println "write html in : "+outfile
			XMLOutputFactory outfactory = XMLOutputFactory.newInstance();
			output = new BufferedOutputStream(new FileOutputStream(outfile))
			writer = outfactory.createXMLStreamWriter(output, "UTF-8");//create a new file
			
			return true;
		} catch (Exception e) {
			println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/** The events. */
	List<String> events = [];
	String previousEvent = "", nextEvent = "";
	/**
	 * Process.
	 */
	void process() {
		
		String previousElem = "";
		boolean parolesRaportees = false;
		boolean firstWord = true;
		boolean shouldBreak = false;
		boolean overlapping = false;
		int nbBreak = 0;
		String previousSPK;
		String localname = "";
		ArrayList<String> whos = [];
		HashMap<String, String> speakers = new HashMap<String, String>();
		HashMap<String, String> topics = new HashMap<String, String>();
		
		writer.writeStartDocument("UTF-8","1.0");
		writer.writeDTD("<!DOCTYPE html>")
		writer.writeStartElement("html");
		//<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		writer.writeStartElement("meta");
		writer.writeAttribute("http-equiv", "Content-Type");
		writer.writeAttribute("content", "text/html");
		writer.writeAttribute("charset", "UTF-8");
		writer.writeEndElement(); // meta
		writer.writeCharacters("\n")
		writer.writeStartElement("head");
		//<link rel="stylesheet" type="text/css" href="class.css" />
		writer.writeStartElement("link");
		writer.writeAttribute("rel", "stylesheet");
		writer.writeAttribute("type", "text/css");
		writer.writeAttribute("href", "css/txm.css");
		writer.writeEndElement(); // link
		writer.writeCharacters("\n")
		writer.writeStartElement("link");
		writer.writeAttribute("rel", "stylesheet");
		writer.writeAttribute("type", "text/css");
		writer.writeAttribute("href", "css/transcriber.css");
		writer.writeEndElement(); // link
		writer.writeCharacters("\n")
		writer.writeStartElement("link");
		writer.writeAttribute("rel", "stylesheet");
		writer.writeAttribute("type", "text/css");
		writer.writeAttribute("href", "css/"+corpusname+".css");
		writer.writeEndElement(); // link
		writer.writeCharacters("\n")
		writer.writeStartElement("link");
		writer.writeAttribute("rel", "stylesheet");
		writer.writeAttribute("type", "text/css");
		writer.writeAttribute("href", "css/"+corpusname+"_"+txtname+".css");
		writer.writeEndElement(); // link
		writer.writeCharacters("\n")
		writer.writeStartElement("script");
		writer.writeAttribute("src", "js/collapsible.js");
		writer.writeCharacters("\n")
		writer.writeEndElement(); // link
		writer.writeCharacters("\n")
		writer.writeEndElement(); // head
		writer.writeCharacters("\n")
		nbBreak++
		writer.writeStartElement("body");
		writer.writeAttribute("class", "txmeditionpage")
		writer.writeEmptyElement("pb");
		writer.writeAttribute("id", ""+nbBreak);
		pages << new File(defaultDir, "${txtname}_${nbBreak}.html")
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "text":
						
							writer.writeStartElement("h1");
							writer.writeAttribute("class", "title");
							String title = parser.getAttributeValue(null, "title");
						
							if (title != null) {
								writer.writeCharacters(title);
							} else {
								writer.writeCharacters("Transcription "+txmfile.getName().substring(0, txmfile.getName().length() - 4));
							}
						
							writeMediaAccess("0.0")
						
							writer.writeEndElement(); // h1
							writer.writeCharacters("\n")
						
							String subtitle = parser.getAttributeValue(null, "subtitle");
							if (subtitle != null && subtitle.length() > 0) {
								writer.writeStartElement("h2");
								writer.writeAttribute("class", "subtitle");
								writer.writeCharacters(subtitle);
								writer.writeEndElement(); // h2
								writer.writeCharacters("\n")
							}
						
							if (enableCollapsibles && parser.getAttributeCount() > 2) {
								writer.writeStartElement("button");
								writer.writeAttribute("class", "collapsible");
								writer.writeAttribute("onclick", "onCollapsibleClicked(this)");
								writer.writeCharacters("➕");
								writer.writeEndElement()
								writer.writeCharacters("\n")
							}
							writer.writeStartElement("table");
							if (enableCollapsibles && parser.getAttributeCount() > 2) {
								writer.writeAttribute("class", "transcription-table collapsiblecontent")
								writer.writeAttribute("style", "display:none;")
							} else {
								writer.writeAttribute("class", "transcription-table");
							}
							boolean grey = false;
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								String name = parser.getAttributeName(i);
								String value = parser.getAttributeValue(i);
								
								if ("title" == name) {
									continue; // ignore "title" metadata
								}
								
								grey = !grey;
								writer.writeStartElement("tr");
								if (grey) {
									writer.writeAttribute("style","background-color:lightgrey;")
								}
								
								if (value != null) {
									writer.writeStartElement("td");
									writer.writeCharacters(name);
									writer.writeEndElement(); // td
									writer.writeStartElement("td");
									writer.writeCharacters(value);
									writer.writeEndElement(); // td
								}
								//get enqueteur to style their names
								if (name.equals("interviewer-id-regex")) {
									interviewers = /$value/
								}
								writer.writeEndElement(); // tr
							}
							writer.writeEndElement(); // table
						//							}
							break;
						case "Topics":
						/*writer.writeStartElement("h2");
					 writer.writeCharacters("Topics");
					 writer.writeEndElement();
					 writer.writeStartElement("ul");
					 */
							break;
						case "Topic":
							topics.put(parser.getAttributeValue(null,"id"), parser.getAttributeValue(null,"desc"))
						/*writer.writeStartElement("li");
					 writer.writeCharacters(parser.getAttributeValue(null,"desc"));
					 writer.writeStartElement("ul");
					 for(int i = 0 ; i < parser.getAttributeCount() ; i++)
					 {
					 if(parser.getAttributeLocalName(i) != "desc")
					 {
					 writer.writeStartElement("li");
					 writer.writeCharacters(parser.getAttributeLocalName(i)+": "+parser.getAttributeValue(i));
					 writer.writeEndElement();
					 }
					 }
					 writer.writeEndElement();
					 writer.writeEndElement();
					 */
							break;
						case "Speakers":
						/*writer.writeStartElement("h2");
					 writer.writeCharacters("Speakers");
					 writer.writeEndElement();
					 writer.writeStartElement("ul");*/
							break;
						case "Speaker":
							whos.add(parser.getAttributeValue(null,"name"));
							speakers.put(parser.getAttributeValue(null,"id"), parser.getAttributeValue(null,"name"))
						/*writer.writeStartElement("li");
					 writer.writeStartElement("ul");
					 writer.writeCharacters(parser.getAttributeValue(null,"name"));
					 for(int i = 0 ; i < parser.getAttributeCount() ; i++)
					 {
					 if(parser.getAttributeLocalName(i) != "name")
					 {
					 writer.writeStartElement("li");
					 writer.writeCharacters(parser.getAttributeLocalName(i)+": "+parser.getAttributeValue(i));
					 writer.writeEndElement();
					 }
					 }
					 writer.writeEndElement();
					 writer.writeEndElement();*/
							break;
						case "Comment":
							spokenTurn = true;
							writenLength++;
							writer.writeStartElement("span");
							writer.writeAttribute("class", "comment");
							writer.writeCharacters(" ["+parser.getAttributeValue(0)+"] ");
							writer.writeEndElement();
							flagcomment = true;
							break;
						case "div":
						
							if (paginate && paginateSections) {
								nbBreak++
								writer.writeEmptyElement("pb");
								writer.writeAttribute("id", ""+nbBreak);
								writer.writeCharacters("\n");
								
								pages << new File(defaultDir, "${txtname}_${nbBreak}.html")
								indexes << wordid
							}
						
							wordcount = 0;
							shouldBreak = false;
						
							writer.writeStartElement("div")
							writer.writeAttribute("class", "section")
						
							String type = parser.getAttributeValue(null, "type")
							writer.writeAttribute("type", ""+type)
						
							String desc = parser.getAttributeValue(null, "topic")
						
							if (type != null && type.length() > 0) {
								writer.writeStartElement("h2");
								writer.writeAttribute("class", "section-title")
								writer.writeCharacters(type);
								
								if (parser.getAttributeValue(null,"startTime") != null) {
									writeMediaAccess(parser.getAttributeValue(null,"startTime"))
								}
								
								writer.writeEndElement(); // h1
							}
						
							if (desc != null && desc.length() > 0) {
								writer.writeStartElement("h2");
								writer.writeAttribute("class", "section-desc")
								writer.writeCharacters(desc)
								writer.writeEndElement(); // h2
							}
						
							def metadata = new LinkedHashMap<String, String>() // temp to store attributes
							def metadataGroups = ["metadata":[]] // default metadata group
							def metadataDeclared = false
							if (parser.getAttributeValue(null, "metadata") != null && parser.getAttributeValue(null, "metadata_groups") != null) {
								def l1 = parser.getAttributeValue(null, "metadata").split("\\|");
								def l2 = parser.getAttributeValue(null, "metadata_groups").split("\\|");
								for (int i = 0 ; i < l1.size() ; i++) {
									def m = l1[i]
									def g = l2[i]
									metadata[m] = "" // forcing order of metadata by pre-declaring
									
									if (!metadataGroups.containsKey(g)) {
										metadataGroups[g] = []
									}
									metadataGroups[g] << m // declaring a metadata type
								}
								metadataDeclared = true
							}
						
						//store attributes values in HashMap
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								String name = parser.getAttributeLocalName(i)
								if (!"type".equals(name)
								&& !"topic".equals(name)
								&& !"metadata".equals(name)
								&& !"metadata_groups".equals(name)
								&& !"startTime".equals(name)
								&& !"endTime".equals(name)) {
									metadata[name] = parser.getAttributeValue(i)
									
									if (!metadataDeclared && !metadataGroups["metadata"].contains(name)) {
										metadataGroups["metadata"] << name
									}
								}
							}
						
						// write metadata HTML
							if (metadataGroups.keySet().size() > 0) {
								
								if (enableCollapsibles && metadataGroups.keySet().size() > 2) {
									writer.writeStartElement("button");
									writer.writeAttribute("class", "collapsible");
									writer.writeAttribute("onclick", "onCollapsibleClicked(this)");
									writer.writeCharacters("➕");
									writer.writeEndElement()
									writer.writeCharacters("\n")
								}
								writer.writeStartElement("div");
								if (enableCollapsibles && metadataGroups.keySet().size() > 2) {
									writer.writeAttribute("class", "section-all-metadata collapsiblecontent")
									writer.writeAttribute("style", "display:none;")
								} else {
									writer.writeAttribute("class", "section-all-metadata");
								}
								for (String groupName : metadataGroups.keySet()) {
									def group = metadataGroups[groupName]
									if (group.size() > 0) {
										if (groupName.equals("text")) {
											writer.writeStartElement("div")
											writer.writeAttribute("class", "section-"+groupName);
											for (String k : group) {
												writer.writeStartElement("p")
												writer.writeAttribute("class", ""+groupName)
												writer.writeStartElement("h4")
												writer.writeCharacters(k)
												writer.writeEndElement() // h4
												writer.writeCharacters(metadata[k])
												writer.writeEndElement() // p
												writer.writeCharacters("\n")
											}
											writer.writeEndElement(); // div
										} else { // 'metadata' and other groups
											writer.writeStartElement("ul")
											writer.writeAttribute("class", "section-"+groupName);
											for (String k : group) {
												writer.writeStartElement("li")
												writer.writeAttribute("class", ""+groupName)
												writer.writeCharacters(""+k+": "+metadata[k])
												writer.writeEndElement() // li
											}
											writer.writeEndElement(); // ul
											writer.writeCharacters("\n")
										}
									}
								}
								writer.writeEndElement(); // div
								writer.writeCharacters("\n")
								writer.writeEmptyElement("hr")
							}
						
							break;
						case "sp":
							endBoldIfNeeded()
							firstSync = true;
							firstWho = true;
							spokenTurn = false;
							overlapping = false
							nSilence = 0 // will count the number of silence written to avoid writting [silence] at the sp end
						
							writer.writeStartElement("p");
							writer.writeAttribute("class", "turn");
							writer.writeCharacters("\n");
						
							this.startTimeSp = parser.getAttributeValue(null, "start")
							this.endTimeSp = parser.getAttributeValue(null, "end")
						
							overlapping = ("true" == parser.getAttributeValue(null,"overlap"))
							String spid = parser.getAttributeValue(null, "who");
						
							whos = []
							if (overlapping) {
								//writer.writeEmptyElement("br"); // write all overlaping speakers
								//writeSpeaker(""+parser.getAttributeValue(null, "who"), false)
								
								writer.writeEmptyElement("br");
								whos = spid.split(" ")
							}
						
							break;
						case "u":
							writer.writeCharacters("\n");
							flagu = true
							this.previousStartTimeU = this.startTimeU
							this.startTimeU = parser.getAttributeValue(null, "start");
							this.currentUTime = parser.getAttributeValue(null, "time");
						
							if (previousElem == "u" && writenLength == 0) { // if previous u had no words, it was a silence
								def duration = ""
								try {
									def d = Float.parseFloat(this.startTimeU) - Float.parseFloat(this.previousStartTimeU)
									duration = " "+TimeFormatter.formatTime(d);
								} catch(Exception e) { e.printStackTrace()}
								writer.writeStartElement("span");
								writer.writeAttribute("class", "event");
								writer.writeCharacters("[silence$duration]");
								writer.writeEndElement(); // span
								nSilence++
								//writer.writeEmptyElement("br");
							}
						
							String spk = parser.getAttributeValue(null, "who")
							if (spk != null && spk != previousSPK) {
								endBoldIfNeeded()
								writer.writeEmptyElement("br");
								writeSpeaker(parser.getAttributeValue(null, "who"), overlapping)
								startBoldIfNeeded()
							}
						
							writeCurrentTime()
							previousSPK = spk
							if (overlapping) previousSPK = null
						
							writenLength = 0;
						//							writenLength = 0;
						/*writer.writeStartElement("span");
					 writer.writeAttribute("class", "sync");
					 writer.writeCharacters("["+parser.getAttributeValue(null,"time")+"]");
					 writer.writeEndElement();*/
						
							break;
						case "event":
							spokenTurn = true;
							writenLength++;
							String desc = parser.getAttributeValue(null,"desc");
							desc = translateEvent(desc);
							String type = parser.getAttributeValue(null,"type");
							if (desc.equals("paroles rapportées")) {
								if (parser.getAttributeValue(null, "extent") == "end") {
									writer.writeCharacters("» ");
								}
								else if (parser.getAttributeValue(null, "extent") == "begin") {
									writer.writeCharacters(" «");
								}
							} else {
								writer.writeStartElement("span");
								writer.writeAttribute("class", "event");
								if (parser.getAttributeValue(null, "extent") == "end") {
									writer.writeCharacters(" <"+desc+"] ");
									if(events.size() > 0)
										events.remove(events.size()-1)
								}
								else if (parser.getAttributeValue(null, "extent") == "begin") 	{
									
									writer.writeCharacters(" ["+desc+"> ");
									events.add(desc)
								}
								else if (parser.getAttributeValue(null, "extent") == "previous") {
									if (parser.getAttributeValue(null, "type") == "pronounce")
										writer.writeCharacters("_["+desc+"] ");
									else
										writer.writeCharacters("_["+desc+"] ");
									previousEvent = desc;
								}
								else if (parser.getAttributeValue(null, "extent") == "next") {
									writer.writeCharacters(" ["+desc+"]_");
									nextEvent = desc
								}
								else {
									writer.writeCharacters(" ["+desc+"] ");
								}
								writer.writeEndElement(); // span@class=event
							}
							break;
						case "w":
						flagword = true
							for(int i = 0 ; i < parser.getAttributeCount() ; i++)
								if(parser.getAttributeLocalName(i) == "id") {
									wordid = (parser.getAttributeValue(i));
									break;
								}
						
							wordcount++;
							if (paginate && wordcount >= wordmax) {
								shouldBreak = true;
							}
						
							if (firstWord) {
								indexes << wordid
								firstWord = false;
							}
						
							break;
						
						case "ana":
						
							String type = parser.getAttributeValue(null,"type").substring(1);
							if (SIMPLE_TOOLTIP) {
								if (type.contains("lemma") || type.contains("pos")) {
									flaginterp=true;
									interpvalue+="\n- ";
								}
							} else {
								flaginterp=true;
								interpvalue+="\n- "+type+"="
							}
							break;
						
						case "form":
							wordvalue="";
							interpvalue ="";
							flagform=true;
							break;
					}
					previousElem = localname;
					break;
				
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch(localname) {
						case "text":
							break;
						case "Topics":
						//writer.writeEndElement();
							break;
						case "Topic":
							break;
						case "Speakers":
						//println "Speakers: "+speakers
						//writer.writeEndElement();
							break;
						case "Speaker":
							break;
						
						case "div":
						//writer.writeCharacters("}");
						
							writer.writeEndElement(); // div
							writer.writeCharacters("\n");
							break;
						case "sp":
						//println "CLOSING: "+parser.getLocalName()
							endBoldIfNeeded()
							if (!spokenTurn && nSilence == 0) {
								writer.writeStartElement("span");
								writer.writeAttribute("class", "event");
								String duration = ""
								try {
									def d = Float.parseFloat(endTimeSp)-Float.parseFloat(startTimeSp)
									duration = " "+TimeFormatter.formatTime(d);
								} catch (Exception e) {e.printStackTrace()}
								writer.writeCharacters("[silence$duration]");
								writer.writeEndElement();
								writer.writeEmptyElement("br");
							}
						
							writer.writeEndElement(); // p
						
							if (shouldBreak) {
								nbBreak++
								writer.writeEmptyElement("pb");
								writer.writeAttribute("id", ""+nbBreak);
								writer.writeCharacters("\n");
								
								pages << new File(defaultDir, "${txtname}_${nbBreak}.html")
								indexes << wordid
								
								wordcount = 0;
								shouldBreak = false;
							}
							writer.writeCharacters("\n");
							break;
						case "u":
						//writer.writeEndElement() // span@class=u
						//writer.writeEmptyElement("br");
						//if (overlapping) writer.writeEndElement(); // b
							writer.writeCharacters("\n");
							flagu = false
							break;
						case "event":
							break;
						case "form":
							flagform = false
							break;
						case "ana":
							flaginterp = false
							break;
						case "w":
							flagword = false
							writenLength++;
							spokenTurn = true;
							int l = lastword.length();
							String endOfLastWord = "";
							if (l > 0) {
								endOfLastWord = lastword.subSequence(l-1, l);
							}
							if (interpvalue != null) {
								interpvalue = interpvalue.replace("\"","&quot;");
							}
							if (events.size() > 0) {
								interpvalue = interpvalue.replace("event=", "event="+events.toString().replace("\"","&quot;")); // remove ", "
							}
							if (nextEvent.length() > 0) {
								interpvalue = interpvalue.replace("event=", "event="+nextEvent+", ")
								nextEvent = ""
							}
							interpvalue = interpvalue.replace("=, ","='', "); // add '' to empty interp value
							if (interpvalue.startsWith(", ")) interpvalue = interpvalue.substring(2)
						//							println "** SPACE TEST"
						//							println "NoSpaceBefore: "+NoSpaceBefore+" contains ? "+wordvalue
						//							println "NoSpaceAfter: "+NoSpaceAfter+" contains ? "+lastword
						//							println "wordvalue starts with '-' ? "+wordvalue
						//							println "NoSpaceAfter: "+NoSpaceAfter+" contains endOfLastWord ? "+endOfLastWord
							if (NoSpaceBefore.contains(wordvalue) ||
							NoSpaceAfter.contains(lastword) ||
							wordvalue.startsWith("-") ||
							NoSpaceAfter.contains(endOfLastWord)) {
								//								println " NO SPACE"
							} else {
								//								println " SPACE"
								writer.writeCharacters(" ");
							}
						
							if (interpvalue.contains("rapp1")) {
								writer.writeCharacters(" «");
							}
						
							writer.writeStartElement("span");
							writer.writeAttribute("class", "word");
							writer.writeAttribute("title", interpvalue);
							writer.writeAttribute("id", wordid);
							writer.writeCharacters(wordvalue);
							writer.writeEndElement();
						
							if (interpvalue.contains("orth")) {
								writer.writeStartElement("span");
								writer.writeAttribute("class", "event");
								writer.writeCharacters("_[?]");
								writer.writeEndElement();
							}
							if (interpvalue.contains("corr")) {
								writer.writeStartElement("span");
								writer.writeAttribute("class", "event");
								writer.writeCharacters("_[!]");
								writer.writeEndElement();
							}
							if (interpvalue.contains("rapp2")) {
								writer.writeCharacters("» ");
							}
						
							lastword=wordvalue;
							break;
					}
				
					break;
				
				case XMLStreamConstants.CHARACTERS:
					if (flagform) {
						if (parser.getText().length() > 0) {
							wordvalue+=(parser.getText().trim());
						}
					}
					if (flagu && bold && !flagword){ // no
						writer.writeCharacters(parser.getText());
					}
					if (flaginterp) {
						if (parser.getText().length() > 0) {
							interpvalue+=(parser.getText().trim());
						}
					}
					
					
					break;
			}
		}
		writer.writeEndElement(); // body
		
		if (paginate) {
			writer.writeEmptyElement("pb");
			nbBreak++
			writer.writeAttribute("id", ""+nbBreak);
		}
		
		writer.writeEndElement(); // html
		writer.close();
		output.close();
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		
		File txmhome = new File(org.txm.Toolbox.getTxmHomePath());
		File xlsDir  = new File(txmhome, "xsl");
		File xslfile = new File(xlsDir,"breakByMilestone.xsl");
//		if (!xslfile.exists()) {
//			println ""
//		}
		//		println "xsl: "+xslfile
		//		println "html: "+outfile
		//		println "pages: "+pages
		//		println "words: "+indexes
		
		if (pages.size() > 1) {

			for (int i = 1 ; i < nbBreak ; i++) {
				ApplyXsl2 a = new ApplyXsl2(xslfile.getAbsolutePath());
				String[] params = ["pbval1", i, "pbval2", i+1];
				
				File resultfile = pages[i-1]
				//println "BBmilestones: "+i+" "+(i+1)+" in file "+resultfile
				//println "process $outfile -> $resultfile"
				a.process(outfile.getAbsolutePath(), resultfile.getAbsolutePath(), params);
			}
		} else {
			File page = pages[0]
			FileCopy.copy(outfile, page)
		}
		outfile.delete() // onepage edition -> no more needed
	}
	
	private void writeCurrentTime() {
		writer.writeStartElement("span");
		writer.writeAttribute("class", "sync");
		writer.writeCharacters(currentUTime);
		
		writeMediaAccess(currentUTime)
		
		writer.writeEndElement() // span
	}
	
	private void writeMediaAccess(def time) {
		writer.writeCharacters(" ");
		writer.writeStartElement("a");
		writer.writeAttribute("onclick", "txmcommand('id', 'org.txm.backtomedia.commands.function.BackToMedia', 'corpus', '"+corpusname+"', 'text', '"+txtname+"', 'time', '"+time+"')");
		writer.writeAttribute("style", "cursor: pointer;")
		writer.writeAttribute("class", "play-media")
		writer.writeCharacters("▶");
		writer.writeEndElement(); // a
	}
	
	private void writeSpeaker(String spk, boolean overlapping) {
		
		writer.writeStartElement("span");
		writer.writeAttribute("class", "spk");
		bold = interviewers != null && spk =~ interviewers
		spk = spk.replaceAll('^([^0-9]*)([0-9]+)$', '$1 $2');
		if (overlapping) {
			writer.writeCharacters("// ")
		}
		if (displayLocutors) {
			writer.writeCharacters(spk+": ")
		} else {
			writer.writeCharacters(" ")
		}
		
		writer.writeEndElement(); // span@class=spk
	}
	
	private String translateEvent(String desc) {
		if (eventTranslations.containsKey(desc)) {
			return eventTranslations.get(desc);
		} else {
			return desc;
		}
	}
	
	boolean boldOpenned = false;
	private void startBoldIfNeeded() {
		if (bold) {
			writer.writeStartElement(ENQ_HIGHLIGHT_ELEMENT);
			boldOpenned = true;
		}
	}
	
	private endBoldIfNeeded() {
		if (boldOpenned) {
			//			println "CLOSE BOLD"
			writer.writeEndElement(); // b
			boldOpenned = false;
		}
	}
	
	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}
	
	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return indexes;
	}
}
