package org.txm.scripts.importer.doc

import org.txm.utils.FileUtils

File dir = new File("/home/mdecorde/TXM/corpora/temoignages/docfiles/")
File outdir = new File("/home/mdecorde/TXM/corpora/temoignages/HTML/TEMOIGNAGES/default/Pictures")
outdir.deleteDir()
outdir.mkdir()

for(def subdir : FileUtils.listDirectories(dir)) {
	File picturesDir = new File(subdir, "Pictures")
	for (def img : FileUtils.listFiles(picturesDir)) {
		img.renameTo(new File(outdir, img.getName()))
	}
}