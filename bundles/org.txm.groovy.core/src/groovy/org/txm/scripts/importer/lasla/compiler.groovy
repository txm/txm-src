// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-05-26 17:42:36 +0200 (jeu. 26 mai 2016) $
// $LastChangedRevision: 3219 $
// $LastChangedBy: mdecorde $ 
//


package org.txm.scripts.importer.lasla;

import java.util.ArrayList;;

import org.txm.importer.cwb.BuildCwbEncodeArgs;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;

import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class compiler.
 */
class compiler 
{
	
	/** The debug. */
	private boolean debug= false;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	/** The url. */
	private def url;
	
	/** The anahash. */
	private HashMap<String,String> anahash =new HashMap<String,String>() ;
	
	/** The text. */
	String text="";
	
	/** The base. */
	String base="";
	
	/** The project. */
	String project="";
	
	/** The text attributes. */
	String[] textAttributes = null;
	
	/** The lang. */
	private String lang ="fr";
	
	/**
	 * initialize.
	 *
	 */
	public compiler(){}
	
	/**
	 * Instantiates a new compiler.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 */
	public compiler(URL url,String text,String base, String project)
	{
		this.text = text
		this.base = base;
		this.project = project;
		this.textAttributes = textAttributes;
		try {
			this.url = url;
			inputData = url.openStream();
			
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.err.println("IOException while parsing ");
		}
	}
	
	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}
	
	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8");
			return true;
		} catch (Exception e) {
			System.err.println(e);
			
			return false;
		}
	}
	
	/**
	 * Go to text.
	 */
	private void GoToText()
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
		{
			if(event == XMLStreamConstants.END_ELEMENT)
				if(parser.getLocalName().equals("teiHeader"))
					return;
		}
	}
	
	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param basename the basename
	 * @param textAttributes the text attributes
	 * @return true, if successful
	 */
	public boolean run(File rootDirFile,String basename, String[] textAttributes) 
	{
		String rootDir =rootDirFile.getAbsolutePath();
		
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		if(!new File(rootDir).exists())
		{
			println ("binary directory does not exists: "+rootDir)
			return false;
		}

		new File(rootDir,"/cqp/").deleteDir();
		new File(rootDir,"/cqp/").mkdir();
		new File(rootDir,"/data/").deleteDir();
		new File(rootDir,"/data/").mkdir();
		new File(rootDir,"registry/").mkdir();
		
		String textid="";
		int counttext =0;
		List<File> files = new File(rootDirFile,"txm").listFiles();
		//1- Transform into CQP file
		def builder = null;
		for(File f : files)
		{
			counttext++;
			if(!f.exists())
			{
				println("file "+f+ " does not exists")	
			}
			else
			{	
				cqpbuilder = new XMLTXM2CQP(f.toURL(), cqpbuilder);
				String txtname = f.getName().substring(0,f.getName().length()-4);
				cqpbuilder.setTextInfo(txtname, basename, "project");

				cqpbuilder.setBalisesToKeep(["text"]);
				cqpbuilder.setSendToPAttributes([]);
				cqpbuilder.setLang(lang);
				if(!cqpbuilder.transformFile(new File(rootDir,"cqp/"+basename+".cqp")))
				{
					println("Failed to compile "+f)
				}
			}
		}
		
		//2- Import into CWB
		def outDir =rootDir;
		def outDirTxm = rootDir;
		
		CwbEncode cwbEn = new CwbEncode();
		cwbEn.setDebug(debug);
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbMa.setDebug(debug);
		String[] pAttributes = ["ref","s","sent","lemme","line","pos"];
		
		BuildCwbEncodeArgs argsgetter = new BuildCwbEncodeArgs();

		ArrayList<String> wordstag = ["w"];
		//argsgetter.process(new File(rootDir+"/txm",basename+".cqp"), wordstag);
		
		String[] sAttributes = ["txmcorpus:0+lang", "text:0+id+base+project"]
		
		try {
			String regPath = outDirTxm + "/registry/"+basename.toLowerCase();
			cwbEn.run(outDirTxm + "/data", outDir + "/cqp/"+basename+".cqp", regPath, pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(basename.toUpperCase(), outDirTxm + "/registry");
			
		} catch (Exception ex) {System.out.println(ex); return false;}
		
		System.out.println("Done.") 
		
		return true;
	}
	
	/**
	 * Sets the debug.
	 */
	public void setDebug()
	{
		this.debug = true;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/geo");
		def c = new compiler();
		c.setDebug();
		c.setCwbPath("~/TXM/cwb/bin");
		c.run(dir,"geo");
	}
}
