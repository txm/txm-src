// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.RGAQCJ

import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;
import javax.xml.stream.*;
import java.net.URL;
import java.util.Properties;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.FusionHeader.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.commons.lang.StringUtils;
import org.txm.*;
import org.txm.core.engines.*;

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {
	
	/** The tokenize. */
	boolean tokenize = true;
	
	/**
	 * Run.
	 *
	 * @param dir the dir
	 * @param paramfile the paramfile
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(File dir, File paramfile, String basename)
	{
		if(!paramfile.exists())
		{
			System.err.println("Parameter file does not exists: "+paramfile.getAbsolutePath());
			return false;
		}
		Properties properties = new Properties();
		BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(paramfile) , "UTF-8"));
		String line = input.readLine();
		while(line != null)
		{
			String[] split = line.split("="); //$NON-NLS-1$
			if(split.length == 2)
			{
				properties.put(split[0], split[1]);
			}
			else if(split.length > 2)
			{
				String[] subsplit = new String[split.length -1 ]; 
				System.arraycopy(split, 1, subsplit, 0, split.length-1);
				properties.put(split[0], StringUtils.join(subsplit, "=")); //$NON-NLS-1$
			}
			line = input.readLine();
		}
		
		if(!(properties.containsKey("nbP") && properties.containsKey("nbTaxo") && properties.containsKey("nbResp")))
		{
			System.err.println("Missing property: nbP or nbTaxo or nbResp");
			return false;
		}
		int nbP = Integer.parseInt(properties.getProperty("nbP"));
		int nbTaxo = Integer.parseInt(properties.getProperty("nbTaxo"));
		int nbResp = Integer.parseInt(properties.getProperty("nbResp"));
		
		if(nbResp == 0)
		{
			System.err.println("No resp ");
			return false;
		}
		String rootDir = dir.getAbsolutePath()+"/";
		ArrayList<String> milestones = new ArrayList<String>();
		
		//where the binaries will be created
		File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+basename);
		binDir.deleteDir();
		binDir.mkdir();
		
		new File(binDir,"txm").deleteDir();
		new File(binDir,"txm").mkdir();
		
		List<File> files = new File(rootDir,"").listFiles();
		
		//set working directory
		rootDir = binDir.getAbsolutePath()+"/";
		
		//Set import parameters
		def correspType = new HashMap<String,String>();
		// correspType(attribut word wlx, attribut type de la propriété ana du w txm)
		for(int p = 1 ; p <= nbP ; p++)
		{
			correspType.put("p"+p, properties.get(properties.get("P"+p+"_taxo")));
		}
		
		def correspRef = new HashMap<String,String>()
		// correspRef (attribut word wlx, attribut ref de la propriété ana du w txm. ref pointe vers l'identifiant du respStmt du TEIheader)
		for(int p = 1 ; p <= nbP ; p++)
		{
			String taxo=properties.get("P"+p+"_taxo")
			String resp=properties.get(taxo+"_resp")
			correspRef.put("p"+p,properties.get(resp));
		}
		
		//il faut lister les id de tous les respStmt
		def respId = [];
		for(int r = 1 ; r <= nbResp ; r++)
			respId << properties.get("R"+r);
		
		//fait la correspondance entre le respId et le rapport d'execution de l'outil
		
		def applications = new HashMap<String,HashMap<String,String>>();	
		//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
		//pour construire les ref vers les taxonomies
		for(int r = 1 ; r <= nbResp ; r++)
		{
			applications.put(properties.get("R"+r), new ArrayList<String>());
			applications.get(properties.get("R"+r)).add(properties.get("R"+r+"_app_id"));//app ident
			applications.get(properties.get("R"+r)).add(properties.get("R"+r+"_app_version"));//app version
			applications.get(properties.get("R"+r)).add(properties.get("R"+r+"_app_reportfile"));//app report file path
		}

		
		def taxonomiesUtilisees = new HashMap<String,String[]>();
		//associe un id d'item avec sa description et son URI
		for(int t = 1 ; t <= nbTaxo ; t++)
		{
			String resp_id = properties.get(properties.get("T"+t+"_resp"))
			if(!taxonomiesUtilisees.containsKey(resp_id))
				taxonomiesUtilisees.put(resp_id,[]);//,"lemma","lasla","grace"]);
			taxonomiesUtilisees.get(resp_id) << properties.get("T"+t)
		}
		
		def itemsURI = new HashMap<String,HashMap<String,String>>();
		//informations de respStmt
		//resps (respId <voir ci-dessus>, [description, person, date])
		for(int t = 1 ; t <= nbTaxo ; t++)
		{
			String taxo = properties.get("T"+t);
			itemsURI.put(taxo,new HashMap<String,String>());
			itemsURI.get(taxo).put("tagset",properties.get("T"+t+"_tagset"));
			itemsURI.get(taxo).put("website",properties.get("T"+t+"_web"));
		}
		def resps = new HashMap<String,String[]>();
		for(int r = 1 ; r <= nbResp ; r++)
		{
			resps.put(properties.get("R"+r), [properties.get("R"+r+"_desc"),properties.get("R"+r+"_who"),properties.get("R"+r+"_when"),properties.get("R"+r+"_day")])
		}
		
		println("Weblex import parameters : ")
		println("resps id "+respId);
		println("resps infos"+resps);
		println("applications "+applications);
		
		println("correspType "+correspType)
		println("correspRef "+correspRef)
		
		println("taxonomiesUtilisees "+taxonomiesUtilisees)
		println("itemsURI "+itemsURI)
				
		//TRANSFORM INTO XML-TEI-TXM
		for(File f : files)
		{
			//ArrayList<String> milestones = new ArrayList<String>();
			File file = f; 
			String txmfile = f.getName();
			println("Building xml-tei-txm "+f+ " >> "+rootDir+"txm/"+txmfile)
			
			//lance le traitement
			def builder3 = new Xml2Ana(file);
			builder3.setCorrespondances(correspRef, correspType);
			builder3.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			builder3.transformFile(rootDir+"txm/",txmfile);
		}
		return true;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/bfm/")
		new importer().run(dir);
	}
}

/* PARAM FILE EXAMPLE
nbP=3
nbTaxo=2
nbResp=2

R1=init
R1_desc=initial taggin
R1_who=al
R1_when=2010
R1_day=Tue Mar  2 21:02:55 Paris, Madrid 2010
R1_app_id=appR1
R1_app_version=app1V
R1_app_reportfile=

R2=apinit
R2_desc=second taggin
R2_who=slh
R2_when=2010
R2_day=Tue Mar  2 21:02:55 Paris, Madrid 2010
R2_app_id=appR2
R2_app_version=app2V
R2_app_reportfile=

P1=truc1
P1_taxo=T1

P2=bidul2
P2_taxo=T1

P3=machin3
P3_taxo=T2

T1=CATTEX
T1_web=www.google.fr
T1_tagset=www.google.fr
T1_resp=R1

T2=TTFR
T2_web=www.bing.fr
T2_tagset=www.bing.fr
T2_resp=R2 
*/