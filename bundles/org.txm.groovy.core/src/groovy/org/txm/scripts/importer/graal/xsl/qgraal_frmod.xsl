<?xml version='1.0'?>

<!-- ####################################### Qgraal_cm BASIC stylesheet ####################################### 
	author: Alexei Lavrentiev version: 1.2 date: 2009-02-16 version: 1.1 date: 
	2008-09-30 version: 1.0 date: 2008-02-21 ####################################### 
	Description This is a basic stylesheet for viewing BFM texts in XML-TEI Modified 
	for QGRAAL modern French translation -->
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">

	<xsl:output method="html" encoding="utf-8" version="4.0"
		indent="yes" />

	<xsl:strip-space elements="*" />


	<xsl:template match="/">
		<!-- <html> <head> <title>Quête du Graal (français moderne)</title> </head> 
			<body style="margin-left:10%;margin-right:10%"> -->
		<xsl:apply-templates select="//tei:text" />
		<!-- </body> </html> -->
	</xsl:template>

	<!-- TEI-Header Processing -->

	<xsl:template match="//tei:teiHeader">
	</xsl:template>

	<!-- End of TEI-header processing -->

	<!-- Text body processing -->

	<!-- "Original" titles found sometimes immediately before text body -->
	<xsl:template match="//tei:div[@type='titre']">
		<p align="center">
			<b>
				<xsl:apply-templates />
			</b>
		</p>
	</xsl:template>

	<!-- Text divisions (general) -->
	<xsl:template match="//tei:div[not(@type='titre')]">
		<xsl:if test="child::tei:head">
			<xsl:apply-templates select="head" />
			<br />
		</xsl:if>
		<xsl:apply-templates
			select="*[not(self::tei:head)]" />
	</xsl:template>

	<!-- Paragraphs -->
	<xsl:template match="//tei:text//tei:p">
		<p align="justify">
			<xsl:apply-templates />
		</p>
	</xsl:template>



	<!-- Page breaks -->

	<xsl:template match="//tei:pb[@ed='Pauphilet1923']">
		(p.
		<xsl:value-of select="@n" />
		)
	</xsl:template>

	<xsl:template match="//tei:milestone[@unit='column']">
		<br />
		<span style="color:darkviolet;font-family:arial;font-size:80%">
			&lt;
			<xsl:value-of select="@xml:id" />
			&gt;
		</span>
	</xsl:template>


	<!-- Line breaks : note special processing for numbered lb used in BFM verse 
		texts -->
	<xsl:template match="tei:lb">
		<xsl:if test="not(parent::tei:p and not(preceding-sibling::*))">
			<br />
		</xsl:if>
	</xsl:template>


	<!-- Processing elements with "rend" attributes -->

	<xsl:template match="//*[@rend='ital']">
		<i>
			<xsl:apply-templates />
		</i>
	</xsl:template>

	<xsl:template match="//*[@rend='gras']">
		<b>
			<xsl:apply-templates />
		</b>
	</xsl:template>

	<xsl:template match="//*[@rend='exp']">
		<sup>
			<xsl:apply-templates />
		</sup>
	</xsl:template>

	<xsl:template match="//*[@rend='ind']">
		<sub>
			<xsl:apply-templates />
		</sub>
	</xsl:template>

	<xsl:template match="//*[@rend='maj']">
		<span style="text-transform:uppercase">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<xsl:template match="//*[@rend='pmaj']">
		<span style="font-variant:small-caps">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<xsl:template match="//*[@rend='crochets']">
		[
		<xsl:apply-templates />
		]
	</xsl:template>

	<xsl:template match="//*[@rend='chevrons']">
		<xsl:text>&lt;</xsl:text>
		<xsl:apply-templates />
		<xsl:text>&gt; </xsl:text>
	</xsl:template>

	<xsl:template match="//*[@rend='crochets-ital']">
		<xsl:text>[</xsl:text>
		<i>
			<xsl:apply-templates />
		</i>
		<xsl:text>]</xsl:text>
	</xsl:template>


	<!-- Numbers -->
	<xsl:template match="tei:num">
		<span style="color:blue">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<!-- Notes -->
	<xsl:template match="tei:note">
		<span style="color:violet">
			[
			<b>Note :</b>
			<xsl:apply-templates />
			]
		</span>
	</xsl:template>

	<!-- Word formatting for morphologically tagged texts -->
	<xsl:strip-space elements="tei:s" />
	<xsl:template
		match="//text()[starts-with(., ' ')][preceding-sibling::*[1][self::tei:w]]">
		<xsl:copy-of select="substring(., 2)" />
	</xsl:template>
	<xsl:template match="//tei:w">
		<span title="{@type}" class="word" id="{@xml:id}">
			<xsl:variable name="apostrophe" select='"&apos;"' />
			<xsl:apply-templates />
			<xsl:choose>
				<xsl:when
					test="following::tei:w[1][starts-with(@type, 'pon')][contains(., ',') or contains(., '.') or contains(., ']')or contains(., ')')]">
				</xsl:when>
				<xsl:when
					test="following::node()[1][self::text()][starts-with(normalize-space(.), ',') or starts-with(normalize-space(.), '.') or starts-with(normalize-space(.), ']') or starts-with(normalize-space(.), ')')]">
				</xsl:when>
				<xsl:when
					test="self::tei:w[starts-with(@type, 'pon')][contains(., '[') or contains(., '(')]">
				</xsl:when>
				<xsl:when test="substring(., string-length(.))=$apostrophe">
				</xsl:when>
				<xsl:when
					test="following::tei:w[1][starts-with(@type, 'pon')][contains(., ':') or contains(., ';') or contains(., '!')or contains(., '?')]">
					<xsl:text>&#xa0;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>

	<xsl:template match="tei:ptr" />

	<!-- Direct speech, not currently tagged -->
	<xsl:template match="tei:q[ancestor::tei:q]">
		<span style="background-color:cyan">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<xsl:template match="tei:q[not(ancestor::tei:q)]">
		<span style="background-color:lightcyan">
			<xsl:apply-templates />
		</span>
	</xsl:template>

</xsl:stylesheet>