// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-06-26 16:53:47 +0200 (lun. 26 juin 2017) $
// $LastChangedRevision: 3451 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.txt

import javax.xml.stream.*

import org.txm.*
import org.txm.scripts.importer.*
import org.txm.importer.scripts.filters.*
import org.txm.scripts.*
import org.txm.importer.scripts.xmltxm.*
import org.txm.utils.*
import org.txm.utils.i18n.DetectBOM

import org.txm.scripts.filters.CutHeader.*
import org.txm.scripts.filters.FusionHeader.*
import org.txm.scripts.filters.TagSentences.*
import org.txm.scripts.filters.Tokeniser.*
import org.txm.tokenizer.TokenizerClasses
/**
 * The Class importer.
 */
class importer {

	/**
	 * Run.
	 *
	 * @param rootfile the rootfile
	 * @param encoding the encoding
	 * @param suffixes the suffixes
	 * @param basename the basename
	 * @return true, if successful
	 */
	public static boolean run(File srcDir, File binDir, File txmDir, String encoding, List<String> suffixes, String basename, String lang, def project) {
		
		File stokenizedDir = new File(binDir,"stokenized");
		stokenizedDir.deleteDir();
		stokenizedDir.mkdir();
		File ptokenizedDir = new File(binDir,"ptokenized");
		ptokenizedDir.deleteDir();
		ptokenizedDir.mkdir();
		File tokenizedDir = new File(binDir,"tokenized");
		tokenizedDir.deleteDir();
		tokenizedDir.mkdir();

		List<File> files = null;

		ArrayList<String> milestones = new ArrayList<String>();//the tags who stay milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");

		// Build the list of src files
		List<File> srcfiles = DeleteDir.scanDirectory(srcDir, true, false)
		for (int i = 0; i < srcfiles.size(); i++) {
			File srcfile = srcfiles.get(i);
			int point = srcfile.getName().lastIndexOf(".");
			String filename, fileext;
			if (point >= 1 && point < srcfile.getName().length()) {
				filename = srcfile.getName().substring(0, point)
				fileext = srcfile.getName().substring(point+1, srcfile.getName().length());
			} else {
				filename = srcfile.getName()
				fileext = "";
			}

			if (!suffixes.contains(fileext)) {
				srcfiles.remove(i);
				i--;
			}
		}
		
		if (srcfiles != null && srcfiles.size() == 0) {
			println "No 'txt' file to process in directory $srcDir"
			return false;
		}

		//CREATE SIMPLE XML FILE
		HashSet<String> existingfiles = new HashSet<String>();
		String currentpath = srcDir.getAbsolutePath()

		String encodingAll = null;
		if (encoding == "??") {
			encodingAll = new CharsetDetector(srcDir).getEncoding();
			println "Guessed encoding: $encodingAll"
		}

		println("Create simple XML files ("+srcfiles.size()+" files)")
		ConsoleProgressBar cpb = new ConsoleProgressBar(srcfiles.size())
		for (File srcfile : srcfiles) {
			if (srcfile.length() == 0) {
				println "Skipping empty file: "+srcfile
				continue;
			}
			cpb.tick()
			File xmlfile;
			try {
				int point = srcfile.getName().lastIndexOf(".");
				String filename;
				String fileext;
				if (point >= 1 && point < srcfile.getName().length() ) {
					filename = srcfile.getName().substring(0, point)
					fileext = srcfile.getName().substring(point+1, srcfile.getName().length());
				} else {
					filename = srcfile.getName()
					fileext = "";
				}

				String filepath = srcfile.getParentFile().getAbsolutePath();
				filepath = filepath.substring(currentpath.length());
				xmlfile = new File(txmDir, filename+".xml");

				while (existingfiles.contains(xmlfile.getName()))//to not erase a file with the same name
				{
					xmlfile = new File(txmDir, "_"+xmlfile.getName());
				}
				existingfiles.add(xmlfile.getName());

				//println "ENCODING "+encoding
				String tmpEncoding = encoding;
				if (encodingAll != null) {
					tmpEncoding = encodingAll
					if (srcfile.length() > CharsetDetector.MINIMALSIZE) {
						tmpEncoding = new CharsetDetector(srcfile).getEncoding();
						//println "file encoding: $tmpEncoding"
					}
				}
				def input = new FileInputStream(srcfile)
				Reader reader = new InputStreamReader(input , tmpEncoding);
				DetectBOM bomdetector = new DetectBOM(srcfile);
				for (int ibom = 0 ; ibom < bomdetector.getBOMSize() ; ibom++) input.read()

				int lb = 1;
				int np = 1;
				int countlb = 0;

				BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(xmlfile))
				XMLOutputFactory factory = XMLOutputFactory.newInstance();
				XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8");

				writer.writeStartDocument("UTF-8","1.0");
				writer.writeStartElement ("TEI");
				writer.writeDefaultNamespace("http://www.tei-c.org/ns/1.0");
				writer.writeNamespace("txm", "http://textometrie.org/1.0");
				writer.writeStartElement ("teiHeader");
				writer.writeStartElement("fileDesc");
				writer.writeStartElement("titleStmt");
				writer.writeStartElement("title")
				writer.writeCharacters(xmlfile.getName())
				writer.writeEndElement(); // title
				writer.writeEndElement(); //titleStmt
				writer.writeStartElement("publicationStmt");
				writer.writeEndElement(); // publicationStmt
				writer.writeStartElement("sourceDesc");
				writer.writeStartElement("p");
				writer.writeCharacters("Generated by TXM TXT+CSV import module - TXM project - http://textometrie.org");
				writer.writeEndElement(); // p
				writer.writeEndElement(); // sourceDesc
				writer.writeEndElement(); // fileDesc
				writer.writeStartElement("encodingDesc")
				writer.writeStartElement("http://textometrie.org/1.0", "applicationDesc")
				writer.writeEndElement();
				writer.writeEndElement();
				writer.writeEndElement (); // teiHeader
				writer.writeStartElement ("text");
				writer.writeAttribute("id",filename)

				reader.eachLine{String line ->
					line = CleanFile.clean(line); // remove ctrl and surrogate chars
					
					//writer.writeStartElement("p");
					//writer.writeAttribute("id",""+np++)
					line = FixString.normalize(line);
					writer.writeStartElement("lb")
					writer.writeAttribute("n",""+lb++)
					writer.writeEndElement(); // close lb
					writer.writeCharacters(line.replace("\t"," "))
					//writer.writeEndElement(); // p
					writer.writeCharacters("\n"); // XML readability
					//}
				}

				writer.writeEndElement();//text
				writer.writeEndElement();// TEI
				
				writer.close();
				output.close();
			}
			catch(Exception e) {
				println("Failed to process file "+srcfile);
				xmlfile.delete()
			}
		}
		cpb.done()

		def toTokenizeFiles = FileUtils.listFiles(txmDir)
		println("Tokenizing "+toTokenizeFiles.length+" files")
		cpb = new ConsoleProgressBar(toTokenizeFiles.length)
		for (File pfile : toTokenizeFiles) {
			cpb.tick()
			File tfile = new File(tokenizedDir, pfile.getName());

			SimpleTokenizerXml tokenizer = new SimpleTokenizerXml(pfile, tfile, TokenizerClasses.newTokenizerClasses(project.getPreferencesScope(), lang));
			tokenizer.setStartTag("text")
			if (!tokenizer.process()) {
				println("Failed to tokenize file "+pfile)
				tfile.delete()
			}
		}
		cpb.done()
		
		// Tag sentences
		List<File> stokenfiles = FileUtils.listFiles(tokenizedDir)
		File stokenizeDir = new File(tokenizedDir.getParentFile(), "stokenized")
		stokenizeDir.mkdir();
		cpb = new ConsoleProgressBar(stokenfiles.size())
		println("Tagging sentences of "+stokenfiles.size()+" files")
		for (File f : stokenfiles) {
			cpb.tick()
			Sequence S = new Sequence();
			Filter F1 = new CutHeader();
			Filter F7 = new TagSentences(TokenizerClasses.newTokenizerClasses(project.getPreferencesScope(), lang));
			Filter F11 = new FusionHeader();
			S.add(F1);
			S.add(F7);
			S.add(F11);
			File infile = new File(tokenizedDir, f.getName());
			File xmlfile = new File(stokenizeDir, f.getName());
			File headerfile = new File(tokenizedDir, f.getName()+"header.xml");

			S.SetInFileAndOutFile(infile.getPath(), xmlfile.getPath());
			S.setEncodages("UTF-8","UTF-8");
			Object[] arguments1 = [headerfile.getAbsolutePath()];
			F1.SetUsedParam(arguments1);
			Object[] arguments2 = [headerfile.getAbsolutePath(), F1];
			F11.SetUsedParam(arguments2);
			if (!S.proceed()) {
				println "Failed to tag file: "+f
			}
			
			S.clean();
			S = F1 = F7 = F11 = null;
			headerfile.delete();//remove the prepared file to clean
		}
		cpb.done()

		//TRANSFORM INTO XML-TEI-TXM
		def toTransformtoTeiFiles = FileUtils.listFiles(stokenizeDir)
		println("Building XML-TXM ${toTransformtoTeiFiles.length} file"+(toTransformtoTeiFiles.length > 1?"s":""))
		cpb = new ConsoleProgressBar(toTransformtoTeiFiles.length)
		for (File tfile : toTransformtoTeiFiles) {
			cpb.tick()
			File xmlfile = new File(txmDir, tfile.getName());

			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()
			def respId = [];
			def applications = new HashMap<String,HashMap<String,String>>();
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			def resps = new HashMap<String,String[]>();

			//println "file : $tfile"
			Xml2Ana builder = new Xml2Ana(tfile);
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			if (!builder.process(xmlfile)) {
				println("Failed to process file "+tfile)
				new File(xmlfile.getParent(),xmlfile.getName()).delete()
			}
		}
		cpb.done()
		
		files = FileUtils.listFiles(txmDir)
		if (files == null || files.size() == 0) return false
		
		if (project.getCleanAfterBuild()) {
			new File(project.getProjectDirectory(), "tokenized").deleteDir()
			new File(project.getProjectDirectory(), "ptokenized").deleteDir()
			new File(project.getProjectDirectory(), "stokenized").deleteDir()
			new File(project.getProjectDirectory(), "src").deleteDir()
			new File(project.getProjectDirectory(), "split").deleteDir()
		}
		
		return true;
	}
}
