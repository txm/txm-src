// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.fleurs;

import javax.xml.stream.*;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * The Class BuildTEI.
 */
class BuildTEI {
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The output. */
	private OutputStreamWriter output;

	/**
	 * Instantiates a new builds the tei.
	 *
	 * @param discoursxml the discoursxml
	 */
	public BuildTEI(File discoursxml) {
		this.url = discoursxml.toURI().toURL();

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			output = new OutputStreamWriter(new FileOutputStream(outfile),
					"UTF-8");
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Write header.
	 */
	public void writeHeader() {
		output.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		output
				.write("<teiCorpus xmlns=\"http://www.tei-c.org/ns/1.0\" xmlns:txm=\"http://textometrie.org/1.0\">\n");
		output.write("<teiHeader type=\"corpus\">\n");
		output.write("</teiHeader>\n");
	}

	/**
	 * Write footer.
	 */
	public void writeFooter() {
		output.write("</teiCorpus>\n");
	}

	/**
	 * Process.
	 *
	 * @param outfile the outfile
	 */
	public void process(File outfile)
	{
		createOutput(outfile);
		int sentence=0;
		int paragraph=0;
		int idword=0;
		
		String lastopenlocalname= "";
		String localname = "";
				
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();
					lastopenlocalname = localname;
					switch (localname) {
						case "corpus":
							this.writeHeader();
						break;
						case "w":
							idword++;	
							
							int para = Integer.parseInt(parser.getAttributeValue(null,"para"));
							int sent = Integer.parseInt(parser.getAttributeValue(null,"sent"));
							if(sent != sentence)
							{
								output.write("</s>\n")
								
								if(para != paragraph)
								{
									output.write("</p>\n")
									output.write("<p id=\"p_"+para+"\">\n");
									paragraph = para;
								}
								
								output.write("<s id=\"s_"+sent+"\">\n");
								sentence = sent;
							}

							output.write("<w id=\"w_"+idword+"\">\n");
							output.write(" <txm:form>"+parser.getAttributeValue(null,"form")+"</txm:form>\n");
							output.write(" <txm:ana resp=\"#cordial\" type=\"#pos\">"+parser.getAttributeValue(null,"pos")+"</interp>\n");
							output.write(" <txm:ana resp=\"#cordial\" type=\"#func\">"+parser.getAttributeValue(null,"func")+"</interp>\n");
							output.write(" <txm:ana resp=\"#cordial\" type=\"#lemma\">"+parser.getAttributeValue(null,"lem")+"</interp>\n");
							output.write("</w>\n");
							
						break;
						case "text":
							output.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
							output.write("<TEI xmlns=\"http://www.tei-c.org/ns/1.0\" xmlns:txm=\"http://textometrie.org/1.0\">\n");
							output.write("<teiHeader type=\"text\">\n");
							output.write("<fileDesc>\n");
							output.write("<titleStmt>\n");
							output.write("<title>"+parser.getAttributeValue(null,"file")+"</title>\n");
							output.write("<respStmt>\n");
							output.write("<resp id=\"cordial\">initial tagging</resp>")
							output.write("</respStmt>\n");
							output.write("</titleStmt>\n");
							output.write("</fileDesc>\n");
							output.write("<encodingDesc>\n");
							output.write("<classDecl>\n");
							output.write("<taxonomy id=\"pos\"><bibl type=\"tagset\"/></taxonomy>\n")
							output.write("<taxonomy id=\"func\"><bibl type=\"tagset\"/></taxonomy>\n")
							output.write("<taxonomy id=\"lemma\"><bibl type=\"tagset\"/></taxonomy>\n")
							output.write("</classDecl>\n");
							output.write("</encodingDesc>\n");
							output.write("</teiHeader>\n");
							output.write("<text id=\""+parser.getAttributeValue(null,"id")+"\" type=\""+parser.getAttributeValue(null,"type")+"\" loc=\""+parser.getAttributeValue(null,"loc")+"\" date=\""+parser.getAttributeValue(null,"date")+"\" file=\""+parser.getAttributeValue(null,"file")+"\">\n");
							output.write("<p id=\"p_1\">\n");
							output.write("<s id=\"s_1\">\n");
							paragraph=1;
							sentence=1;
						break;
					}
					break;	
					
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
					switch (localname) {				
						
						case "w":
						break;
						
						case "text":
							output.write("</s>\n");
							output.write("</p>\n");
							paragraph=1;
							sentence=1;
							output.write("</text>\n");
							output.write("</TEI>\n");
						break;
						
						case "corpus":
							this.writeFooter();
						break;
					}
					break;
					
					case XMLStreamConstants.CHARACTERS:
						//output.write(parser.getText().trim());				
					break;
				}
			}
			output.close();
			parser.close();
			inputData.close();
		}
		catch (XMLStreamException ex) {
			System.out.println(ex);
		}
		catch (IOException ex) {
			System.out.println("IOException while parsing " + inputData);
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String rootDir = "~/xml/discours/src";

		File srcfile = new File(rootDir, "discours.xml");
		File resultfile = new File(rootDir, "discours-tei.xml");
		println("build discours xml-tei file : " + srcfile + " to : "
				+ resultfile);

		def builder = new BuildTEI(srcfile);
		builder.process(resultfile);

		return;
	}
}
