// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.fleurs;

import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.BuildTTSrc;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;

import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class compiler.
 */
class compiler{
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private def parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	/** The url. */
	private def url;
	
	/** The text. */
	String text="";
	
	/** The base. */
	String base="";
	
	/** The project. */
	String project="";
	
	/** The lang. */
	private String lang ="fr";
	
	/**
	 * Instantiates a new compiler.
	 */
	public compiler(){}
	
	/**
	 * initialize.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 */
	public compiler(URL url,String text,String base, String project)
	{
		this.text = text
		this.base = base;
		this.project = project;
		
		try {
			this.url = url;
			inputData = url.openStream();
			
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}
	
	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8")
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
		
	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(String dirPathName, String fileName)
	{
		createOutput(dirPathName, fileName);
		
		String vForm = "";
		String wordid= "";
		String w_l= "";
		String w_lemme= "";
		String w_pos= "";
		String w_x= "";
		String w_z= "";
		
		boolean flagForm = false;
		output.write("<txmcorpus lang=\""+lang+"\">\n"); 
		try 
		{
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
			{
				switch (event) 
				{
					case XMLStreamConstants.START_ELEMENT:
					switch (parser.getLocalName()) 
					{
					case "recueil":
						output.write("<text id=\""+text+"\" base=\"fleurs\" project=\"default\">\n");
						break;
					case "w":
						flagForm= true;
						w_l = parser.getAttributeValue(null,"l");
						w_lemme = parser.getAttributeValue(null,"lemme");
						w_pos = parser.getAttributeValue(null,"pos");
						w_x= parser.getAttributeValue(null,"x");
						w_z= parser.getAttributeValue(null,"z");
						wordid= parser.getAttributeValue(null,"id");
						vForm ="";
						break;
					default:
						output.write("<"+parser.getLocalName())
						for(int i =0; i < parser.getAttributeCount() ; i++)
								output.write(" "+parser.getAttributeLocalName(i)+"=\""+parser.getAttributeValue(i)+"\"")
							output.write(">\n");
						break;
					}
					break;
					
					case XMLStreamConstants.END_ELEMENT:
					switch (parser.getLocalName()) 
					{
					case "recueil":
						output.write("</text>\n");
						break;
					case "w":
						flagForm= false;
						output.write(vForm+"\t"+wordid+"\t"+w_l+"\t"+w_lemme+"\t"+w_pos+"\t"+w_x+"\t"+w_z+"\n")
						break;
					default:
						output.write("</"+parser.getLocalName()+">\n");
						break;
					}
					break;
					
					case XMLStreamConstants.CHARACTERS:
						if(flagForm)
							vForm += parser.getText().trim();
					
					break;
				}
			}
			output.write("</txmcorpus>"); 
			output.close();
			parser.close();
			inputData.close();
		}
		catch (XMLStreamException ex) {
			System.out.println(ex);
		}
		catch (IOException ex) {
			System.out.println("IOException while parsing " + inputData);
		}
		
		return true;
	}
	
	/**
	 * Run.
	 *
	 * @param rootDir the root dir
	 */
	public boolean run(String rootDir) 
	{
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		new File(rootDir,"cqp/fleurs.cqp").delete();//cleaning&preparing
		new File(rootDir,"cqp/").deleteDir();
		new File(rootDir,"cqp/").mkdir();
		new File(rootDir,"registry/").mkdir();
		
		//1- Transform into CQP file
		List<File> files = new File(rootDir,"txm").listFiles();
		for (File f : files) {
			if (!f.exists()) {
				println("file "+f+ " does not exists")	
			}
			else
			{	
				println("process file "+f)
				String txtname = f.getName().substring(0,f.getName().length()-4);
				def builder = new compiler(f.toURL(), txtname, "fleurs", "default");
				builder.setLang lang
				builder.transfomFileCqp(rootDir,"cqp/fleurs.cqp");
			}
		}
		
		//2- Import into CWB
		def outDir =rootDir;
		def outDirTxm = rootDir;
		
		CwbEncode cwbEn = new CwbEncode();
		cwbEn.setDebug true;
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbMa.setDebug true;
		
		String[] pAttributes = ["id","l","lemme","pos","x","z"];
		String[] sAttributes = ["txmcorpus:0+lang", "text:0+id+base+project","poeme:0+titre+genre+type+section+tranche_chrono+date+annee+titre_abr+preorig+schema1+schema2+n","strophe:0+ordre+nb_vers+structure","div:0+type+structure","vers:0+numvers+tphon+stru+n_syl+n_pied+cesure+rime"];
		
		try {
			String regPath = outDirTxm + "/registry/"+"fleurs"
			cwbEn.run(outDirTxm + "data", outDir + "/cqp/"+"fleurs.cqp", regPath,pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run("FLEURS", outDirTxm + "/registry");
			
		} catch (Exception ex) {System.out.println(ex); return false;}
		
		System.out.println("Done.") 
		return true;
	}
}