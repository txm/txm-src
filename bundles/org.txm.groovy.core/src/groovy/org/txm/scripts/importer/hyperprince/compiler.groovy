// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.hyperprince
;

import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.BuildTTSrc;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;

import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class compiler.
 */
class compiler 
{
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private def parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	/** The url. */
	private def url;
	
	/** The anahash. */
	private HashMap<String,String> anahash =new HashMap<String,String>() ;
	
	/** The text. */
	String text="";
	
	/** The base. */
	String base="";
	
	/** The project. */
	String project="";
	
	/** The lang. */
	private String lang ="fr";
	
	/**
	 * Instantiates a new compiler.
	 */
	public compiler(){}
	
	/**
	 * initialize.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 */
	public compiler(URL url,String text,String base, String project)
	{
		this.text = text
		this.base = base;
		this.project = project;
		
		try {
			this.url = url;
			inputData = url.openStream();
			
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}
	
	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8")
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/**
	 * clear anaHash variable, it is used to store ana tags values then print it when the end element </ana> is found.
	 */
	private void fillanaHash()
	{
		anahash.clear();
		for(String s : types)
		anahash.put( s,"-" );
	}
	
	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @param idtext the idtext
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(String dirPathName, String fileName,idtext)
	{
		createOutput(dirPathName, fileName);
		
		String author = "";
		String date = "";
		String lang = "";
		String vAna = "";
		String vForm = "";
		String wordid= "";
		
		boolean flagAuthor = false;
		boolean flagDate = false;
		boolean flagForm = false;
		boolean flagAna = false;
		output.write("<txmcorpus lang=\""+lang+"\">\n"); 
		try 
		{
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
			{
				switch (event) 
				{
					case XMLStreamConstants.START_ELEMENT:
					switch (parser.getLocalName()) 
					{
						
						case "monogr"://get attr lang
						lang = parser.getAttributeValue(null,"lang");
						//idtext = parser.getAttributeValue(null,"id");
						break;
						
						case "author"://get text
						flagAuthor = true;
						author ="";
						break;
						
						case "date"://get text
						flagDate = true;
						date = "";
						break;
						
						case "text"://write tag+ lang + date + author
						output.write("<text lang=\""+lang+"\" auteur=\""+author+"\" date=\""+date+"\" id=\""+idtext+"\" base=\""+base+"\" project=\""+project+"\">\n");
						break;
						
						case "front"://get type,id, write tag
						case "back"://get type,id, write tag
						case "head"://get type,id, write tag
						case "seg"://get type,id,n write tag
						output.write("<"+parser.getLocalName()+" id=\""+parser.getAttributeValue(null,"id")+"\" type=\""+parser.getAttributeValue(null,"type")+"\">\n");
						break;
						
						case "body"://get type,id, write tag BODY
						//	for(int i = 0 ; i < parser.getAttributeCount(); i++)
								
						output.write("<body id=\""+parser.getAttributeValue(null,"id")+"\" type=\""+parser.getAttributeValue(null,"type")+"\">\n");
						break;
						
						case "s":
						output.write( "<s>\n");
						break;
						
						case "w":
						wordid = parser.getAttributeValue(null,"id")
						break;
						case "form":
						flagForm = true;
						vForm = "";
						vAna ="";
						break;
						
						case "ana":
						flagAna = true;
						break;
					}
					break;
					
					case XMLStreamConstants.END_ELEMENT:
					switch (parser.getLocalName()) 
					{
						case "author"://get text
						flagAuthor = false;
						break;
						
						case "date"://get text
						flagDate = false;
						break;
						
						case "text"://write tag+ lang + date + author
						output.write("</text>\n");
						break;
						
						case "front"://get type,id, write tag
						case "back"://get type,id, write tag
						case "head"://get type,id, write tag
						case "seg"://get type,id,n write tag
						output.write("</"+parser.getLocalName()+">\n");
						break;
						
						case "div0"://get type,id, write tag BODY
						output.write("</body>\n");
						break;
						
						case "s":
						output.write( "</s>\n");
						break;
						
						case "w":
						output.write( vForm +vAna+"\t"+wordid+"\n");
						vAna = "";
						vForm = "";
						break;
						
						case "form":
						flagForm = false;
						break;
						
						case "ana":
						flagAna = false;
						break;
					}
					break;
					
					case XMLStreamConstants.CHARACTERS:
					if(flagForm)
					vForm += parser.getText().trim();
					if(flagAna)
					vAna += "\t" +parser.getText().trim();
					if(flagDate)
					date += parser.getText().trim();
					if(flagAuthor)
					author += parser.getText().trim();
					break;
				}
			}
			output.write("</txmcorpus>"); 
			output.close();
			parser.close();
			inputData.close();
		}
		catch (XMLStreamException ex) {
			System.out.println(ex);
		}
		catch (IOException ex) {
			System.out.println("IOException while parsing " + inputData);
		}
		
		return true;
	}
	
	/**
	 * Run.
	 *
	 * @param rootDir the root dir
	 */
	public boolean run(String rootDir) 
	{
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		new File(rootDir,"cqp/hyperprince.cqp").delete();//cleaning&preparing
		new File(rootDir,"cqp/").deleteDir();
		new File(rootDir,"cqp/").mkdir();
		new File(rootDir,"registry/").mkdir();
		
		//1- Transform into CQP file
		List<File> files = new File(rootDir,"txm").listFiles();
		for(File f : files) {
			if(!f.exists()) {
				println("file "+f+ " does not exists")	
			}
			else
			{	
				println("process file "+f)
				String txtname = f.getName().substring(0,f.getName().length()-4);
				def builder = new compiler(f.toURL(),txtname, "hyperprince", "default");
				builder.setLang lang
				builder.transfomFileCqp(rootDir,"cqp/hyperprince.cqp",f.getName());
			}
		}
		
		//2- Import into CWB
		def outDir =rootDir;
		def outDirTxm = rootDir;
		
		CwbEncode cwbEn = new CwbEncode();
		cwbEn.setDebug true;
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbMa.setDebug true;
		
		String[] pAttributes = ["pos","lemme","id"];
		String[] sAttributes = ["txmcorpus:0+lang", "text:0+auteur+date+id+lang+base+project","front:0+type+id","body:0+type+id","back:0+type+id","head:0+type+id", "seg:0+type+n+id"];
		
		try {
			String regPath =outDirTxm + "/registry/"+"hyperprince"
			cwbEn.run(outDirTxm + "data", outDir + "/cqp/"+"hyperprince.cqp", regPath,pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run("HYPERPRINCE", outDirTxm + "/registry");
		} catch (Exception ex) {System.out.println(ex); return false;}
		
		System.out.println("Done.") 
		return true;
	}
}
