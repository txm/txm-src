// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.graal;

import org.txm.scripts.importer.*
// TODO: Auto-generated Javadoc

/**
 * The Class BuildGraalGSP.
 *
 * @author sheiden
 */

public class BuildGraalGSP{
 	
	/** The root dir. */
	String rootDir;
	
	/** The in dir. */
	String inDir;
	
	/** The in dir xsl. */
	String inDirXsl;
	 	
	//dossier qui contiendra les gsp et vues dipl, cour, facs, trad et cqp
	
	/** The out dir. */
	String outDir;
	//dossiers qui contient le registre cqp et le dossier data (qui contient les corpus)
	
	/** The out dir txm. */
	String outDirTxm;
	//dossier qui contient les xml sources découpés
	
	/** The out dir cut. */
	String outDirCut;
	
	/** The in file. */
	String inFile;
	
	/** The in file2. */
	String inFile2;
	
	/** The out file. */
	String outFile;
	
	/** The out file2. */
	String outFile2;
	
	/**
	 * Instantiates a new builds the graal gsp.
	 *
	 * @param rootDir the directory which contains sources and which will contains result files
	 * @param inFile the path to the graalcm file
	 * @param inFile2 the path to the graalfrmod file
	 * @param outFile the prefix of gsp outfiles
	 * @param outFile2 the prefix of gsp outfiles
	 */
	public BuildGraalGSP(String	rootDir, String inFile,
	String inFile2,	String outFile,	String outFile2)
	{
		this.rootDir = rootDir;
		this.inFile = inFile;
		this.inFile2 = inFile2;
		this.outFile = outFile;
		this.outFile2 = outFile2;
		this.inDir = this.rootDir;
		this.inDirXsl = this.rootDir + "/src/xsl";
		this.outDir = this.rootDir + "/imported/form/";//outdir of the .gsp
		this.outDirTxm = this.rootDir + "/imported/corpora/";//outdir for data files & registry
		this.outDirCut = this.rootDir + "/imported/cut/";
		this.createDirs();
	}
	
	/**
	 * create outputs dir to prevent from "dir not found" error.
	 */
	private void createDirs()
	{
		new File(outDir).mkdir();
		new File(outDir+"/dipl").mkdir();
		new File(outDir+"/trad").mkdir();
		new File(outDir+"/facs").mkdir();
		new File(outDir+"/cour").mkdir();
		new File(outDirTxm).mkdir();
		new File(outDirCut).mkdir();
	}

	/**
	 * process !!!.
	 */
	public boolean start()
	{
		if(!ValidateXml.test(new File(inDir + "src/"+inFile+".xml")))
		{
			println "Invalid source: "+inDir + "src/"+inFile+".xml";
			return false;
		}
		
		if(!ValidateXml.test(new File(inDir + "src/"+inFile2+".xml")))
		{
			println "Invalid source: "+inDir + "src/"+inFile2+".xml";
			return false;
		}
		
		def timportCQPestG
		def nPage;
		def temp;
		def BBM;
		def MAN;
		def MAF;
		def MAD;
		def MTR;
		//Ouverture des Xsl
		//Decoupe et mise en forme des page
		println "start"
		for (int index = 1; index < 268; index++) 
		{
			println "loop" + index
			
			//URL url = new URL("file:///" + inDirXsl + "/breakByMilestone.xsl");
			BBM = new ApplyXsl( inDirXsl + "/breakByMilestone.xsl");
			//println "xsl applied"
			MAN = new GraalApplyXsl(new URL("file:///" + inDirXsl + "/qgraal_cour.xsl"));
			//println "xsl applied 2"
			MAF = new GraalApplyXsl(new URL("file:///" + inDirXsl + "/qgraal_facs.xsl"));
			//println "xsl applied 3 "
			MAD = new GraalApplyXsl(new URL("file:///" + inDirXsl + "/qgraal_dipl.xsl"));
			//println "xsl applied 4"
			MTR = new GraalApplyXsl(new URL("file:///" + inDirXsl + "/qgraal_frmod.xsl"));
			//println "xsl applied 5"
			def testG = new GraalXpath();
			//println "xpath done"
			
			nPage = testG.getXpathResponse(inDir + "src/"+inFile+ ".xml",  index+"").substring(4);//recuperation du numero de page
			//nPage=index
			println "npage = " + nPage
			
			URL url = new URL("file:///" + inDir + inFile+ ".xml");
			//System.out.println("--Converting " + url.getFile() + " to web pages.");
			
			//BBM.setParam("pbval1", index);
			//println "params set 1 : "
			//BBM.setParams("pbval2", index + 1);
			String[] params = ["pbval1", index,"pbval2", index+1]
			//println "params set 2"
			BBM.process(new URL("file:///" + inDir + "/src/" + inFile+ ".xml").getFile(), outDirCut+"/"+ inFile + "_" + nPage + ".xml",params);//decoupe ancienne version
			println "BBM Done : "+inFile + "_" + nPage + ".xml"
			//System.out.println "L'index est : " + index + " npage " + nPage;
				System.out.println "----Pages gsp";
				if (index<260) {
				BBM.process(new URL("file:///" + inDir + "/src/" + inFile2+ ".xml").getFile(), outDirCut+"/"+ inFile2 + "_" + nPage + ".xml",params);//decoupe nouvelle version
				}
				System.out.println "----Break By MileStone.";
				if(index<10)
				{					
				MAF.ApplyXsl(new URL("file:///" + outDirCut + "/" + inFile + "_" + nPage+ ".xml"), outDir + "facs", "_" + outFile + "_facs_" + nPage + ".gsp");//mise en page facsimilaire
				System.out.println "----Building facsimile version.";
				}
				MAD.ApplyXsl(new URL("file:///" + outDirCut + "/" + inFile + "_" + nPage+ ".xml"), outDir + "dipl", "_" + outFile + "_dipl_" + nPage + ".gsp");//mise en page diplomatique
				System.out.println "----Building diplomatic version.";
				if(index<260)
				{					
				MTR.ApplyXsl(new URL("file:///" + outDirCut + "/" + inFile2 + "_" + nPage+ ".xml"), outDir + "trad", "_" + outFile2 + "_trad_" + nPage + ".gsp");//mise en forme traduction
				System.out.println "----Building traduction version.";
				}

			MAN.ApplyXsl(new URL("file:///" + outDirCut + "/" + inFile + "_" + nPage + ".xml"), outDir + "cour", "_" + outFile + "_norm_" + nPage + ".gsp");//mise en forme courante
			System.out.println "----Building current version.";
			System.out.println "--Page " + nPage + " OK";
		}
		println "end"
		return true;
	}
}