// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2015-12-17 12:11:39 +0100 (jeu. 17 déc. 2015) $
// $LastChangedRevision: 3087 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.tmx;

import org.txm.utils.FileUtils
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.stream.*;

import org.txm.objects.Project;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;


import java.net.URL;

import org.txm.objects.*;
// TODO: Auto-generated Javadoc

/**
 * Split a TMX file into N xml files, N the number of lang declared in the <tuv> tags.
 *
 * @author mdecorde
 */
class Tmx2XmlFiles {

	/** The lang. */
	String lang;

	/** The type. */
	String type;

	/** The infile. */
	File infile;

	/** The outdir. */
	File outdir;

	/** The writers. */
	ArrayList<XMLStreamWriter> writers;
	def outputs;

	/** The textslangs. */
	HashMap<String, String> textslangs;
	HashMap<Integer, ArrayList<String>> langGroups;
	Set<String> langs;
	def header = [:];

	/**
	 * Instantiates a new tmx2 xml files.
	 *
	 * @param base the base
	 */
	public Tmx2XmlFiles(HashMap<String, String> textslangs, HashMap<Integer, ArrayList<String>> langGroups)
	{
		this.textslangs = textslangs;
		this.langGroups = langGroups;
	}

	/**
	 * Gets the text langs.
	 *
	 * @return the langs of the TMX files
	 */
	public HashMap<String, String> getTextLangs()
	{
		return textslangs;
	}

	/**
	 * Gets the original texts
	 *
	 * @return the langs of the TMX files
	 */
	public HashMap<String, String> getLangGroups()
	{
		return langGroups;
	}
	
	def corpusIDS = [:];
	int noCorpusID = 0;
	public def getCorpusIDS() {
		return corpusIDS;
	}

	int TOTALTUV = 0;
	public def initWriters(File tmxFile) {
		println "initialize writers for : $tmxFile"
		def inputData = infile.toURI().toURL().openStream();
		def inputfactory = XMLInputFactory.newInstance();
		XMLStreamReader parser = inputfactory.createXMLStreamReader(inputData);

		TOTALTUV = 0;
		writers = new ArrayList<XMLStreamWriter>(); // one writer per text
		outputs = []; // one writer per text
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			if (event == XMLStreamConstants.START_ELEMENT) {
				if (parser.getLocalName() == "tuv") {

					String lang = "FR";
					for(int i = 0 ; i < parser.getAttributeCount() ; i++)
						if (parser.getAttributeLocalName(i) == "lang")
					{
						lang = (""+parser.getAttributeValue(i)).toUpperCase()
						break;
					}

					buildWriter(TOTALTUV, lang);
					TOTALTUV++
				}
			} else if (event == XMLStreamConstants.END_ELEMENT) {
				if (parser.getLocalName() == "tu") {
					break; // stop at the end of the first </tu>
				}
			}
		}

		parser.close()
		inputData.close();
	}

	String textname;
	int noTuv = -1;
	XMLOutputFactory factory = XMLOutputFactory.newInstance();
	public def buildWriter(int no, String lang) {
		//println "build Writer : $no $lang"
		if (writers.size() <= no) {
			File outfile = new File(outdir, textname+no+".xml");
			textLangs.put(outfile.getName(), lang);
			if (!langGroups.containsKey(no)) langGroups[no] = [];
			langGroups.get(no).add(outfile.getName());

			FileOutputStream output = new FileOutputStream(outfile)
			def writer = factory.createXMLStreamWriter(output, "UTF-8")
			writers.add(no, writer);
			outputs.add(no, output);

			writer.writeStartDocument("UTF-8","1.0");
			writer.writeStartElement ("TEI");
			writer.writeStartElement ("teiHeader");
			writer.writeEndElement();
		}
		return writers.get(no);
	}

	XMLStreamReader parser;
	int tuCounter = 0;
	/**
	 * start the processing.
	 *
	 * @param infile the infile
	 * @param outdir the outdir
	 */
	public void run(File indir, File outdir)
	{
		boolean isTuv = false;
		boolean isProp = false;
		boolean isTu = false;
		boolean flagHeader = false;
		String propvalue;
		String localname;

		def tuvProps = [:];
		def tuProps = [:];
		def corpusTITLES = [];
		def corpusSUBTITLES = [];
		def corpusAUTHORS = [];
		def corpusTRANSLATORS = [];
		
		def tuTitles = [];
		boolean endOfTuProp = false;

		for (File infile : FileUtils.listFiles(indir)) {
			if (!infile.canRead() || infile.getName().startsWith("import") || infile.getName().endsWith(".css") || infile.isHidden() || infile.isDirectory()) {
				println "skip file : "+infile;
				continue;
			}
			
			this.infile = infile;
			this.outdir = outdir;
			outdir.mkdir(); // ensure the directory exists
			// get the filename without ".xml"
			textname = infile.getName().substring(0,infile.getName().length()-4)+"_";
			initWriters(infile);

			// create XML reader
			def inputData = infile.toURI().toURL().openStream();
			def inputfactory = XMLInputFactory.newInstance();
			parser = inputfactory.createXMLStreamReader(inputData);
			def headerProps = [:];
			noTuv = -1;
			//getWriter(noTuv);
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();

						if (localname == "tuv") {
							noTuv++;
							
							if (endOfTuProp) {
								endOfTuProp = false;
								//println "localname: "+localname+" $tuTitles "//+parser.getLocation()
								for(int i = 0 ; i < writers.size() && i < tuTitles.size() ; i++) {
									writers.get(i).writeAttribute("title", tuTitles.get(i));
								}
							}
							
							this.writeStartElement("seg");
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) // get the lang attribute of the tuv
								if (parser.getAttributeLocalName(i) == "lang")
							{
								lang = (""+parser.getAttributeValue(i)).toUpperCase()
								break;
							}
							
							
							
							this.writeAttribute("id", "seg_$tuCounter");
						} else if (localname == "seg") {
							// do nothing :)
						} else if (localname == "prop") {
							type = null;
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) { // get the type attribute of the prop
								if (parser.getAttributeLocalName(i) == "type") {
									type = parser.getAttributeValue(i)
									break;
								}
							}
							if (type == null) {
								println "ERROR NO PROP TYPE : "+parser.getLocation()
							}
							propvalue= "";
							isProp= true;
						} else if (localname == "header") {
							flagHeader = true;
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) { // get the type attribute of the prop
								headerProps[parser.getAttributeLocalName(i)] = parser.getAttributeValue(i)
							}
						} else {

							if (endOfTuProp && localname == "note") {
								//println "localname: note $tuTitles "
								for (int i = 0 ; i < writers.size() ; i++) {
									writers.get(i).writeAttribute("title", tuTitles.get(i));
								}
								endOfTuProp = false;
							}
							
							if (localname == "tmx") continue;
							if (localname == "tu") {
								tuTitles = [];
								isTu= true;
								noTuv = -1;
								endOfTuProp = true; // wait for the next start element that is not note or prop
								tuCounter++;
							}
							if (localname == "body")
								localname = "text"
							this.writeStartElement(localname); // create text tag in each xml file

							for (int i= 0 ; i < parser.getAttributeCount() ;i++) {
								this.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
							}
							if (localname == "text") {
								println "add header : $headerProps"
								//for(String key : headerProps.keySet())	this.writeAttribute(key, headerProps[key]);

								for (int i = 0 ; i < writers.size() ; i++) {
									if (corpusIDS.size() > i)
										writers.get(i).writeAttribute("corpusid", corpusIDS[i]);
									if (corpusTITLES.size() > i)
										writers.get(i).writeAttribute("title", corpusTITLES.get(i));
									if (corpusSUBTITLES.size() > i)
										writers.get(i).writeAttribute("subtitle", corpusSUBTITLES.get(i));
									if (corpusAUTHORS.size() > i)
										writers.get(i).writeAttribute("author", corpusAUTHORS.get(i));
									if (corpusTRANSLATORS.size() > i)
										writers.get(i).writeAttribute("translator", corpusTRANSLATORS.get(i));
								}
							}
						}
						break;

					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						switch (localname) {
							case "tuv":
								lang = null
								this.writeEndElement();
								break
							case "seg":
								break;
							case "prop":
								//println "flagHeader $flagHeader isTu $isTu type $type"
								if (flagHeader) {
									if (type == "corpusId") {
										corpusIDS[noCorpusID++] = propvalue.toLowerCase();
									} else if (type == "title") {
										corpusTITLES << propvalue;
									} else if (type == "subtitle") {
										corpusSUBTITLES << propvalue;
									} else if (type == "author") {
										corpusAUTHORS << propvalue;
									} else if (type == "translator") {
										corpusTRANSLATORS << propvalue;
									}
								} else if (isTu && type == "tuTitle") {
									tuTitles << propvalue
								} else {
									writeAttribute(type, propvalue); // transform all properties to attributes
								}
								isProp= false;
								break;
							case "tmx":
								break;
							case "tu":
								isTu= false;
								endOfTuProp = false;
								noTuv = -1;
								this.writeEndElement();
							case "header":
								flagHeader = false;
								break;
							default:
								this.writeEndElement();
								break;
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						if (isProp) {
							propvalue += parser.getText(); // get prop tag text
						} else {
							this.writeCharacters(parser.getText());
						}
						break;

				}
			}

			this.writeEndElement(); // close TEI
			parser.close();
			inputData.close();

			for (XMLStreamWriter writer : writers) { // close all xml file output streamS
				writer.close();
			}
			for (def output : outputs) { // close all xml file output streamS
				output.close();
			}
		}
	}

	/**
	 * call writeEndElement in all outputstream
	 * if lang field is set, write only in the stream of the file with the corresponding lang.
	 */
	private void writeEndElement()
	{
		if (noTuv == -1) {
			for (def writer : writers) writer.writeEndElement();
		} else
			writers.get(noTuv).writeEndElement();
	}

	/**
	 * call writeStartElement in all outputstream
	 * if lang field is set, write only in the stream of the file with the corresponding lang.
	 *
	 * @param localname the localname
	 */
	private void writeStartElement(String localname)
	{
		if (noTuv == -1) {
			for (def writer : writers) writer.writeStartElement(localname);
		} else
			writers.get(noTuv).writeStartElement(localname);
	}

	/**
	 * call writeStartDocument in all outputstream
	 * if lang field is set, write only in the stream of the file with the corresponding lang.
	 *
	 * @param text the text
	 */
	private void writeCharacters(String text)
	{
		//System.err.println "write chars elem : '"+text+"' >> "+lang
		text = text.trim();
		if (text.length() > 0) {
			if (noTuv == -1) {
				for (def writer : writers) writer.writeCharacters(text);
			} else
				writers.get(noTuv).writeCharacters(text);
		}
	}

	/**
	 * call writeAttribute in all outputstream
	 * if lang field is set, write only in the stream of the file with the corresponding lang.
	 *
	 * @param name the name
	 * @param value the value
	 */
	private void writeAttribute(String name, String value)
	{
		try {
			if (noTuv == -1) {
				for (def writer : writers) writer.writeAttribute(name, value);
			} else
				writers.get(noTuv).writeAttribute(name, value);
		} catch (Exception e) { org.txm.utils.logger.Log.printStackTrace(e); System.err.println("ERROR: "+parser.getLocation());}
	}

	/**
	 * Main.
	 *
	 * @param args the args
	 */
	static main(args)
	{
		File infile = new File(System.getProperty("user.home"),"xml/tmx/sample.tmx");
		File outfile = new File(System.getProperty("user.home"),"TXM/corpora/tmx/");
		new Tmx2XmlFiles().run(infile, outfile);
		println "done"
	}
}

