// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//

package org.txm.scripts.importer.limsi

import java.util.ArrayList;
import org.txm.utils.Pair;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.txm.utils.logger.Log;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.ConsoleProgressBar

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.xpath.*;

import java.util.HashMap;
import org.txm.scripts.importer.*;
import org.txm.utils.*;
import org.txm.metadatas.*;

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {

	/** The speakers. */
	HashMap<String, String> speakers = ["spk1":"//Speaker[@id='spk1']", "spk2":"//Speaker[@id='spk2']"];

	/** The topics. */
	HashMap<String, String> topics = ["to1":"//Topic[@id='to1']", "to2":"//Topic[@id='to2']"];

	/** The trans. */
	HashMap<String, String> trans = ["trans":"//Trans"];

	/** The doc. */
	def doc;

	/** The infile. */
	File infile;

	/** The outfile. */
	File outfile;

	/** The outdir. */
	File txmDir;
	File binDir;

	/** The trsfiles. */
	ArrayList<String> trsfiles;

	/** The metadata. */
	Metadatas metadatas;

	/**
	 * Instantiates a new importer.
	 *
	 * @param trsfiles the trsfiles
	 * @param outdir the outdir
	 * @param metadata the metadata
	 */
	public importer(ArrayList<File> trsfiles, File binDir, File txmDir)
	{
		this.trsfiles = trsfiles;
		this.txmDir = txmDir;
		this.binDir = binDir;
	}

	/**
	 * Run.
	 *
	 * @return true, if successful
	 */
	public boolean run()
	{
		if (trsfiles == null) {
			println "no files to process"
			return false;
		}
		txmDir.mkdir();
		if (!txmDir.exists()) {
			println "can't create txmDir: "+txmDir.getAbsolutePath()
		}

		println "Convert from LIMSI files to CQP files ("+trsfiles.size()+")."
		ConsoleProgressBar cpb = new ConsoleProgressBar(trsfiles.size());
		for (File infile : trsfiles) {
			cpb.tick();

			String filename = infile.getName();
			int idx = filename.lastIndexOf(".");
			if (idx > 0) filename = filename.substring(0, idx)

			File outfile = new File(txmDir, filename+".cqp")
			
			if (outfile.exists() && outfile.lastModified() >= infile.lastModified()) {
				// skip
			} else {
				def processor = new LimsiToCQP(infile);
				processor.process(outfile);
			}
		}

		println ""
		return txmDir.listFiles() != null;
	}

	/**
	 * Process.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param metas the metas
	 * @return true, if successful
	 */
	public boolean process(File infile, File outfile, ArrayList<Pair<String, String>> metas)
	{
		//inject metadata into
		this.infile = infile;
		this.outfile = outfile;
		def factory = DocumentBuilderFactory.newInstance()
		factory.setXIncludeAware(true);
		def builder = factory.newDocumentBuilder()
		
		doc = builder.parse(infile)
		insert(trans.get("trans"), metas);
		return save();
	}

	/**
	 * Insert.
	 *
	 * @param xpath the xpath
	 * @param pairs the pairs
	 */
	public void insert(String xpath, List<Pair<String, String>> pairs)
	{
		println ("insert $pairs into $xpath")
		def expr = XPathFactory.newInstance().newXPath().compile(xpath)
		def nodes = expr.evaluate(doc, XPathConstants.NODESET)

		for (Node node : nodes) {
			Element elem = (Element)node;
			for (Pair<String, String> p : pairs) {
				elem.setAttribute(p.getFirst(), p.getSecond());
			}
		}
	}

	/**
	 * Save.
	 *
	 * @return true, if successful
	 */
	private boolean save()
	{
		try {
			// Création de la source DOM
			Source source = new DOMSource(doc);

			// Création du fichier de sortie
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8"));
			Result resultat = new StreamResult(writer);

			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			return true;
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String userhome = System.getProperty("user.home");
		File metadatasfile = new File(userhome,"/xml/transcriber/metadatas.xml");
		File infile = new File(userhome,"/xml/transcriber/dialogue.trs");
		File outfile = new File(userhome,"/xml/transcriber/dialogue-out.trs");
		new importer().run(infile, outfile, metadatasfile);
	}
}
