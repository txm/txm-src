// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.fleurs

import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.BuildTTSrc;
import org.txm.importer.scripts.xmltxm.*;
import javax.xml.stream.*;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URL;
import java.util.List;

import org.txm.*;
import org.txm.core.engines.*;

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer 
{
	
	/**
	 * Run.
	 *
	 * @param dir the dir
	 * @param basename the basename
	 */
	public void run(File dir, String basename)
	{
		String rootDir = dir.getAbsolutePath()+"/"
		//cleaning
		File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+basename);
		binDir.deleteDir();
		binDir.mkdir();
		new File(binDir,"txm").deleteDir();
		new File(binDir,"txm").mkdir();
		
		File srcfile = new File(rootDir).listFiles()[0];
		File resultfile = new File(binDir,"txm/"+srcfile.getName());
		
		this.process(srcfile, resultfile);		
	}
	
	/**
	 * Process.
	 *
	 * @param srcfile the srcfile
	 * @param resultfile the resultfile
	 */
	public void process(File srcfile, File resultfile)
	{
		
		String localname= "";
		URL url = srcfile.toURI().toURL();
		def inputData = url.openStream();
		def factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(inputData);
		OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(resultfile) , "UTF-8");
		
		println resultfile.getAbsolutePath()
		
		String poeme_titre ="";
		String poeme_type ="";
		String poeme_genre ="";
		String poeme_section ="";
		String poeme_tranchechrono ="";
		String poeme_date ="";
		String poeme_annee ="";
		String poeme_titreabr ="";
		String poeme_preorig ="";
		String poeme_schema1 ="";
		String poeme_schema2 ="";
		
		int wordid = 1;
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
		{
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "M"://f, l, x test si null
						case "MC":
						case "PC":
							String x = parser.getAttributeValue(null,"x");
							String z = parser.getAttributeValue(null,"z");
							String l = parser.getAttributeValue(null,"l");
							String f = parser.getAttributeValue(null,"f");
							String pos = "none";
							String lemme = "none";
							if(l != null)
							{
								String[] split = l.split("_");
								if(split.length == 2)
								{
									pos = split[1];
									lemme = split[0];
								}
							}
						
							if(x == null)
								x = "n/a";
							if(z == null)
								z = "1";
							output.write("<w id=\"w_"+(wordid++)+"\" l=\""+l+"\" lemme=\""+lemme+"\" pos=\""+pos+"\" x=\""+x+"\" z=\""+z+"\">"+f+"</w>\n")
							break;
						
						case "RECUEIL":
							output.write("<recueil>\n");
						
							break;
						case "POEME":
							poeme_titre = parser.getAttributeValue(null,"TITRE");
							poeme_type = parser.getAttributeValue(null,"TYPE");
							poeme_genre = parser.getAttributeValue(null,"GENRE");
							poeme_section = parser.getAttributeValue(null,"SECTION");
							poeme_tranchechrono = parser.getAttributeValue(null,"TRANCHE_CHRONO");
							poeme_date = parser.getAttributeValue(null,"DATE");
							poeme_annee = parser.getAttributeValue(null,"ANNEE");
							poeme_titreabr = parser.getAttributeValue(null,"TITRE_ABR");
							poeme_preorig = parser.getAttributeValue(null,"PREORIG");
							poeme_schema1 = parser.getAttributeValue(null,"SCHEMA1");
							poeme_schema2 = parser.getAttributeValue(null,"SCHEMA2");
							break;	
						case "ASTX_ATTR":
							output.write("<poeme");
							output.write(" titre=\""+poeme_titre+"\"");
							if(poeme_type == "")
								output.write(" type=\"v\"");
							else
								output.write(" type=\""+poeme_type+"\"");
							if(poeme_genre == "")
								output.write(" genre=\"v\"");
							else
								output.write(" genre=\""+poeme_genre+"\"");
							output.write(" section=\""+poeme_section+"\"");
							output.write(" tranche_chrono=\""+poeme_tranchechrono+"\"");
							output.write(" date=\""+poeme_date+"\"");
							output.write(" annee=\""+poeme_annee+"\"");
							output.write(" titre_abr=\""+poeme_titreabr+"\"");
							output.write(" preorig=\""+poeme_preorig+"\"");
							output.write(" schema1=\""+poeme_schema1+"\"");
							output.write(" schema2=\""+poeme_schema2+"\"");
							output.write(" n=\""+poeme_schema2+"\"");
							output.write(">\n");
							break;
						
						case "HUITAIN":
						case "SIZAIN":
							output.write("<div type=\""+localname.toLowerCase()+"\"");
							for(int i =0; i < parser.getAttributeCount() ; i++)
								output.write(" "+parser.getAttributeLocalName(i).toLowerCase()+"=\""+parser.getAttributeValue(i)+"\"")
							output.write(">\n");
							break;
						case "STROPHE":
						case "VERS":
							output.write("<"+localname.toLowerCase());
							for(int i =0; i < parser.getAttributeCount() ; i++)
								output.write(" "+parser.getAttributeLocalName(i).toLowerCase()+"=\""+parser.getAttributeValue(i)+"\"")
							output.write(">\n");
							break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						
						case "RECUEIL":
							output.write("</recueil>\n");
							break;
						
						case "ASTX_ATTR":
							output.write("</poeme>\n");
							break;
						
						case "HUITAIN":
						case "SIZAIN":
							output.write("</div>\n");
							break;
						case "STROPHE":
						case "VERS":
							output.write("</"+localname.toLowerCase()+">\n");
							break;
					}
					break;
				case XMLStreamConstants.CHARACTERS:
				//output.write(parser.getText().trim());				
					break;
			}
		}
		output.close();
		parser.close();
		inputData.close();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File(System.getProperty("user.home"),"xml/fleurs/");
		String rootDir = dir.getAbsolutePath();
		File homedir = new File(rootDir);
		def imp = new importer();
		imp.run(homedir);
	}
}