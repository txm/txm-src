// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2015-06-03 15:04:53 +0200 (mer. 03 juin 2015) $
// $LastChangedRevision: 2984 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.hyperbase;


import org.txm.scripts.importer.hyperbase.importer;
import org.txm.scripts.importer.hyperbase.compiler;
import org.txm.scripts.importer.xml.pager;
import org.txm.objects.*;
import org.txm.utils.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;
import org.w3c.dom.Element
import org.txm.utils.xml.DomUtils;

String userDir = System.getProperty("user.home");

def MONITOR;
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName();
String basename = corpusname
String rootDir = project.getSrcdir();
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL();
def xslParams = project.getXsltParameters();
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
String page_element = project.getEditionDefinition("default").getPageElement()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

File txmDir = new File(binDir,"txm/$corpusname");
txmDir.deleteDir();
txmDir.mkdirs();

if (MONITOR != null) MONITOR.worked(1, "IMPORTER")
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
println "-- IMPORTER - Reading source files"

if (!(new importer().run(srcDir, binDir, txmDir, encoding, basename, lang, project))) {
	println "import process stopped";
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "ANNOTATE")
println "-- ANNOTATE - Running NLP tools"
boolean annotationSuccess = false;
if (annotate) {
	String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
	def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
	if (engine.processDirectory(txmDir, binDir, ["lang":model])) {
		annotationSuccess = true;
		if (project.getCleanAfterBuild()) {
			new File(binDir, "treetagger").deleteDir()
			new File(binDir, "ptreetagger").deleteDir()
			new File(binDir, "annotations").deleteDir()
		}
	}
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(25, "COMPILING")
println "-- COMPILING - Building Search Engine indexes"
def c = new compiler();
c.setAnnotationSuccess(annotationSuccess);
if (debug) c.setDebug();
c.setLang(lang);
if (!c.run(project, binDir, txmDir, corpusname)) {
	println "import process stopped";
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }

new File(binDir,"HTML/$corpusname").deleteDir();
new File(binDir,"HTML/$corpusname").mkdirs();
if (build_edition) {

	println "-- EDITION - Building edition"
	if (MONITOR != null) MONITOR.worked(20, "EDITION")
	
	File outdir = new File(binDir,"/HTML/$corpusname/default/");
	outdir.mkdirs();
	List<File> filelist = FileUtils.listFiles(txmDir)
	Collections.sort(filelist);
	def second = 0

	println "Paginating "+filelist.size()+" texts"
	ConsoleProgressBar cpb = new ConsoleProgressBar(filelist.size());
	for (File txmFile : filelist) {
		cpb.tick()
		String txtname = txmFile.getName();
		int i = txtname.lastIndexOf(".");
		if(i > 0) txtname = txtname.substring(0, i);

		List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);

		Text t = new Text(project);
		t.setName(txtname);
		t.setSourceFile(txmFile)
		t.setTXMFile(txmFile)

		def ed = new pager(txmFile, outdir, txtname, NoSpaceBefore, NoSpaceAfter, basename, project);
		Edition edition = new Edition(t);
		edition.setName("default");
		edition.setIndex(outdir.getAbsolutePath());

		for (i = 0 ; i < ed.getPageFiles().size();) {
			File f = ed.getPageFiles().get(i);
			String wordid = "w_0";
				if (i < ed.getIdx().size()) wordid = ed.getIdx().get(i);
			edition.addPage(""+(++i), wordid);
		}
	}
	cpb.done()
}
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FINALIZING")

readyToLoad = project.save();
