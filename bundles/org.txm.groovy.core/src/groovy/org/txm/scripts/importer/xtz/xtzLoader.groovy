// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2015-06-03 15:04:53 +0200 (mer., 03 juin 2015) $
// $LastChangedRevision: 2984 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.xtz;

import javax.xml.stream.*

import org.txm.*
import org.txm.metadatas.*
import org.txm.objects.*
import org.txm.importer.scripts.xmltxm.*
import org.txm.utils.*
import org.txm.utils.i18n.*
import org.txm.importer.xtz.*

String userDir = System.getProperty("user.home");

def MONITOR;
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }
XTZImport i = new XTZImport(project);
i.setMonitor(monitor)
/*
 * To customize the XTZ import, replace the importer, compiler, annotater or pager objects before calling process()
		i.importer = new XTZImporter(i)
		i.compiler = new XTZCompiler(i)
		i.annotater = new TTAnnotater(i);
		i.pager = new XTZPager(i)
 */
i.process();
readyToLoad = i.isSuccessful && project.save()
