/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.corptef;

import org.txm.scripts.importer.corptef.importer;
import org.txm.scripts.importer.corptef.compiler;
import org.txm.scripts.importer.corptef.annotate;
import org.txm.scripts.importer.corptef.pager;
import org.txm.objects.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
String userDir = System.getProperty("user.home");
String rootDir;
String lang;
String encoding;
String model;
try{rootDir = rootDirBinding;lang=langBinding;encoding=encodingBinding;model=modelBinding;}
catch(Exception)
{	println "DEV MODE";//exception means we debug
	if(!org.txm.Toolbox.isInitialized())
	{
		//rootDir = "C:\\Documents and Settings\\alavrent\\TXM\\sources\\corptef\\src";
		rootDir = userDir+"/xml/corptef"
		lang="fr";
		encoding= "UTF-8";
		model="rgaqcj";
		Toolbox.workspace = new Workspace(new File(userDir,"TXM/corpora/default.xml"));
		Toolbox.setParam(Toolbox.INSTALL_DIR,new File(userDir,"txminstall"));
		Toolbox.setParam(Toolbox.TREETAGGER_INSTALL_PATH,new File(userDir,"txminstall/treetagger"));
		Toolbox.setParam(Toolbox.TREETAGGER_MODELS_PATH,new File(userDir,"txminstall/treetagger/models"));
		Toolbox.setParam(Toolbox.USER_TXM_HOME, new File(System.getProperty("user.home"),"TXM"));
	}
}

File dir = new File(rootDir);

println "-- IMPORTER - Reading source files"
new importer().run(dir);

File binDir = new File(Toolbox.getTxmHomePath(),"corpora/corptef");
rootDir = binDir.getAbsolutePath();
dir = new File(rootDir);

println "-- ANNOTATE - Running NLP tools"
new Annotate().run(dir,model+".par");

println "-- COMPILING - Building Search Engine indexes"
def c = new compiler();
c.setDebug();
//c.setCwbPath("C:/Program Files/TXM/cwb/bin");
c.setLang(lang);
c.run(dir);


//move registry file to cwb registry dir
File registryfile = new File(rootDir+"/registry","corptef");
if(registryfile.exists())
	org.txm.utils.io.FileCopy.copy(registryfile,new File(Toolbox.getTxmHomePath(),"registry/corptef"))

Workspace w = org.txm.Toolbox.workspace;
Project p = w.getProject("default")
p.removeBase("corptef")
Base b = p.addBase("corptef");
b.addDirectory(new File(rootDir,"txm"));
b.setAttribute("lang", lang)
b.propagateAttribute("lang")

println "-- EDITION"
new File(rootDir+"/HTML/").deleteDir()
new File(rootDir+"/HTML/").mkdir();
new File(rootDir+"/HTML/default/").mkdir();
files = new File(rootDir,"txm").listFiles();

for(String textname : b.getTextsID())
{
	Text text = b .getText(textname);
	File srcfile = text.getSource();
	File resultfile = new File(rootDir+"/HTML",srcfile.getName().substring(0,srcfile.getName().length()-4)+".html");
	List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
	List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
	println("Building edition  : "+srcfile+" to : "+resultfile );
	
	def ed = new pager(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter,600);
	ed.writecorrespondancesFile(new File(rootDir,"edition_correspondances.txt"))
	Edition editionweb = text.addEdition("default","html",resultfile);

	for(int i = 0 ; i < ed.getPageFiles().size();i++)
	{
		File f = ed.getPageFiles().get(i);
		String idx = ed.getIdx().get(i);
		editionweb.addPage(f,idx);
	}
	
//	Edition editionbp = text.addEdition("onepage","html",resultfile);
//	editionbp.addPage(resultfile,ed.getIdx().get(0));
}

w.save();