<?xml version='1.0'?>

<!-- This is a "facsimilary" stylesheet for Old French XML transcriptions: 
	- most abbreviations and medieval letter variants are rendered with the corresponding 
	Unicode and MUFI characters. "Andron Scriptor Web" font should be installed 
	for correct visualization; - agglutinations and deglutinations are presented 
	"as they are" in the original; - direct speech is marked with darkblue color; 
	- uncertain readings are marked with grey background color; - text deleted 
	in the original is marked with rose background color; - notes are rendered 
	with [*] sign in the text body; the text of the note appears on "mouse-over" 
	and is reproduced in the end. -->

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">
	<xsl:import href="mdv_common.xsl" />
	<xsl:param name="form">
		fac-similaire
	</xsl:param>
	<xsl:variable name="mode">
		facs
	</xsl:variable>

	<xsl:output method="html" encoding="utf-8" version="4.0"
		indent="yes" />

	<xsl:strip-space elements="*" />

	<!-- <xsl:variable name="title-formal" select="//fileDesc/titleStmt/title[@type='formal']"/> 
		<xsl:variable name="title-normal" select="//fileDesc/titleStmt/title[@type='normal']"/> 
		<xsl:variable name="title-ref" select="//fileDesc/titleStmt/title[@type='reference']"/> 
		<xsl:variable name="author" select="//titleStmt/author"/> -->



	<xsl:template
		match="//tei:text//tei:p|//tei:text//tei:head">
		<table width="100%" style="border:none">
			<tr style="border:none">
				<td width="10%" valign="top"
					style="white-space:nowrap;padding-right:10pt;line-height:18px;font-family:Arial;font-size:14px;border:none;text-align:right">
					<xsl:if test="@n">
						<span style="color:gray;font-size:12px">
							[§
							<xsl:value-of select="@n" />
							]
						</span>
					</xsl:if>
					<xsl:apply-templates
						select="descendant::tei:lb|descendant::tei:milestone[@unit='column']|descendant::bfm:lettrine"
						mode="number" />
				</td>
				<td valign="top"
					style="white-space:nowrap;line-height:18px;font-family:Andron Scriptor Web;font-size:14px;border:none">
					<xsl:apply-templates mode="facs" />
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="tei:pb" mode="facs">
		<xsl:call-template name="pb_common" />
	</xsl:template>

	<xsl:template match="tei:cb" mode="facs">
		<xsl:call-template name="cb_common"></xsl:call-template>
	</xsl:template>

	<xsl:template match="tei:note" mode="facs">
		<xsl:call-template name="note_common"></xsl:call-template>
	</xsl:template>

	<xsl:template match="tei:q" mode="facs">
		<span style="background-color:lightcyan">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<xsl:template match="tei:gap" mode="facs">
		<xsl:call-template name="gap_common"></xsl:call-template>
	</xsl:template>

	<xsl:template match="//tei:lb" mode="facs">
		<xsl:choose>
			<xsl:when test="@ed='facs'">
				<br />
			</xsl:when>
			<!--<xsl:when test="position()=1 and not(ancestor::tei:seg)"></xsl:when> -->
			<xsl:when
				test="@ed='norm' or preceding::tei:lb[1][@ed='facs']"></xsl:when>
			<xsl:when
				test="position() = 1 and ancestor::tei:s[not(preceding-sibling::*)]"></xsl:when>
			<xsl:when test="preceding::tei:w[1][descendant::bfm:headlb]"></xsl:when>
			<xsl:when test="preceding::tei:w[1][descendant::tei:lb]"></xsl:when>
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:lb" mode="number">
		<xsl:choose>
			<xsl:when test="@ed='facs'"></xsl:when>
			<xsl:when
				test="position() = 1 and ancestor::tei:s[not(preceding-sibling::*)]"></xsl:when>
			<xsl:otherwise>
				<br />
				&#xa0;
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if
			test="substring(@n, string-length(@n)) = 0 or substring(@n, string-length(@n)) = 5">
			<xsl:value-of select="@n" />
		</xsl:if>
	</xsl:template>



	<xsl:template match="//bfm:headlb" mode="facs">
		<br />
	</xsl:template>

	<xsl:template match="//tei:w" mode="facs">
		<span title="{@type}" class="word" id="{@xml:id}">
			<xsl:if test="descendant::me:facs">
				<xsl:apply-templates
					select="descendant::me:facs" mode="facs" />
				<xsl:choose>
					<xsl:when test="@rend='aggl' or @rend='elision'"></xsl:when>
					<xsl:otherwise>
						<xsl:text> </xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</span>
	</xsl:template>

	<xsl:template match="text()[ancestor::me:facs]"
		mode="facs">
		<xsl:value-of select="translate(., 'i', '&#x0131;')" />
	</xsl:template>

	<xsl:template match="bfm:mdvAbbr" mode="facs">
		<xsl:apply-templates mode="facs" />
	</xsl:template>

	<xsl:template match="tei:sic" mode="facs">
		<span style="color:darkred" title="sic!">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<xsl:template match="tei:subst" mode="facs">
		[
		<xsl:choose>
			<xsl:when test="child::tei:del[@rend='line-through']">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				\
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when test="child::tei:del[@rend='dotobl']">
				<span style="color:red">
					<xsl:value-of select="child::tei:del" />
				</span>
				\
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when test="child::tei:del[@rend='transform']">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				&gt;
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when
				test="child::tei:del[@rend='unmarked'] and child::tei:add[@place='overwrite']">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				+
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:when
				test="child::tei:del[@rend='unmarked'] and child::tei:add[contains(@place,'linear')]">
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				+ \
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red;text-decoration:line-through">
					<xsl:value-of select="child::tei:del" />
				</span>
				/
				<span style="color:blue;text-decoration:underline">
					<xsl:value-of select="child::tei:add" />
				</span>
			</xsl:otherwise>
		</xsl:choose>
		]
	</xsl:template>

	<xsl:template match="tei:add" mode="facs">
		<span style="color:blue;text-decoration:underline">
			<xsl:choose>
				<xsl:when
					test="@place='interlinear' or @place='supralinear'">
					\
					<xsl:apply-templates mode="facs" />
					/
				</xsl:when>
				<xsl:when test="starts-with(@place,'margin')">
					\\
					<xsl:apply-templates mode="facs" />
					//
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates mode="facs" />
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>

	<xsl:template match="tei:del" mode="facs">
		<xsl:choose>
			<xsl:when test="@rend='dotbl'">
				<span style="color:red">
					<xsl:apply-templates mode="facs" />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="text-decoration:line-through;color:red">
					<xsl:apply-templates mode="facs" />
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:unclear" mode="facs">
		<span style="background-color:lightgrey">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<xsl:template match="bfm:lettrine" mode="facs">
		<font color="{@color}" size="{@size}00%" style="font-weight:bold">
			<xsl:apply-templates mode="facs" />
		</font>
	</xsl:template>

	<xsl:template match="bfm:lettrine" mode="number">
		<font color="black" size="{@size}00%" style="font-weight:bold">&#xa0;</font>
	</xsl:template>

	<xsl:template match="bfm:sb" mode="facs">
		<xsl:text>&#xa0;</xsl:text>
	</xsl:template>

	<xsl:template match="//tei:hi[@rend='sup']" mode="facs">
		<sup>
			<xsl:apply-templates mode="facs" />
		</sup>
	</xsl:template>

	<xsl:template match="tei:supplied" mode="facs">
		<span style="color:blue">
			[
			<xsl:apply-templates mode="facs" />
			]
		</span>
		<xsl:if test="not(ancestor::tei:w)">
		</xsl:if>
	</xsl:template>

	<xsl:template match="tei:surplus">
		<span style="color:darkred" title="sic!">
			<xsl:apply-templates mode="facs" />
		</span>
	</xsl:template>

	<!-- <xsl:template match="tei:name"> <xsl:value-of select="@reg"/> </xsl:template> -->
</xsl:stylesheet>