// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.corptef;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

// TODO: Auto-generated Javadoc
/** Build BFM corpus simple edition from a xml-tei. @author mdecorde */
class pager {
	List<String> NoSpaceBefore;
	
	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;
	
	/** The pagecount. */
	int pagecount = 0;
	
	/** The wordmax. */
	int wordmax = 0;

	/** The wordid. */
	String wordid;
	
	/** The first word. */
	boolean firstWord = true;
	
	/** The wordvalue. */
	String wordvalue;
	
	/** The interpvalue. */
	String interpvalue;
	
	/** The lastword. */
	String lastword = " ";
	
	/** The wordtype. */
	String wordtype;
	
	/** The flagform. */
	boolean flagform = false;
	
	/** The flaginterp. */
	boolean flaginterp = false;
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The writer. */
	OutputStreamWriter writer;
	
	/** The multiwriter. */
	OutputStreamWriter multiwriter = null;
	
	/** The infile. */
	File infile;
	
	/** The outfile. */
	File outfile;
	
	/** The pages. */
	ArrayList<File> pages = new ArrayList<File>();
	
	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>();
	
	/** The titre id. */
	String titreId;

	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 */
	pager(File infile, File outfile, List<String> NoSpaceBefore,
			List<String> NoSpaceAfter, int max) {
		this.wordmax = max;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();
		this.infile = infile;

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		createOutput(outfile);
		process();
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput()
	{
		try {
			if(multiwriter != null)
			{
				multiwriter.write("</body>");
				multiwriter.write("</html>");
				multiwriter.close();
			}
			pagecount++;
			File f = new File(outfile.getParent()+"/multi/",outfile.getName().substring(0,outfile.getName().length()-5)+"_"+pagecount+".html");
			pages.add(f);
			idxstart.add(wordid)
			multiwriter = new OutputStreamWriter(new FileOutputStream(f) , "UTF-8");
			
			multiwriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			multiwriter.write("<html>");
			multiwriter.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
			multiwriter.write("<head>");
			multiwriter.write("<title>BFM - "+titreId+" - "+pagecount+"</title>");
			multiwriter.write("</head>");
			multiwriter.write("<body>");
			if(wordid != null)
			{
				String[] splited = wordid.split("_");
				int no = Integer.parseInt(splited[1]);
				multiwriter.write("<span id=\""+splited[0]+"_"+(no++)+"\"/>");
			}
						
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			this.outfile = outfile;

			writer = new OutputStreamWriter(new FileOutputStream(outfile),
					"UTF-8");

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	/**
	 * Process.
	 */
	void process()
	{
		String localname = "";
		
		boolean closeTitre = false;
		boolean closeAmen = false;
		
		boolean flagchoice = false;
		boolean flagcorr = false;
		boolean flagsic = false;
		boolean flagreg = false;
		boolean flagexpan = false;
		boolean flagorig = false;
		boolean flagabbr = false;
		boolean flagforeign = false;
				
		File xpathfile = infile;
		titreId = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='normal']/text()");
		String auteur = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author");
		String datecompo = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo']/@when");
		String ssiecle = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo_sous_siecle']/@n");
		String domaine = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:textDesc/tei:domain/@type");
		String genre = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:textDesc/@n");
		String forme = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:catRef/@target[contains(.,'forme')]");
		String dialecte = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:region[@type='dialecte_auteur']");
		if(titreId == null)
			System.err.println("titreId regexp error");
		if(auteur == null)
			System.err.println("auteur regexp error");
		if(datecompo == null)
			System.err.println("datecompo regexp error");
		if(ssiecle == null)
			System.err.println("ssiecle regexp error");
		if(domaine == null)
			System.err.println("domaine regexp error");
		if(genre == null)
			System.err.println("genre regexp error");
		if(forme == null)
			System.err.println("forme regexp error");
		
		if(dialecte == null)
			System.err.println("dialecte regexp error");
		if(titreId == null || auteur == null || datecompo == null || ssiecle == null || domaine == null || genre == null || forme == null || dialecte == null)
			return;
		forme = forme.substring(1);
		createNextOutput();
		
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.write("<html>");
		writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
		writer.write("<head>");
		writer.write("<title>BFM - "+titreId+"</title>");
		writer.write("</head>");
		writer.write("<body>");
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			
			
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{
					case "text":
						
						writer.write("<h1 align=\"center\" style=\"color:darkviolet;font-family:Times;font-weight:bold;font-style:italic;font-size=200%\" class=\"text\">"+titreId+"</h1>")
						writer.write("<p align=\"center\">")
						writer.write("_____________________________________________________<br/>\n")
						writer.write("auteur : "+auteur+"<br/>");
						writer.write("date de composition : "+datecompo+"<br/>");
						writer.write("sous-siècle : "+ssiecle+"<br/>");
						writer.write("domaine : "+domaine+"<br/>");
						writer.write("genre : "+genre+"<br/>");
						writer.write("forme : "+forme+"<br/>");
						writer.write("dialecte : "+dialecte+"<br/>");
						writer.write("_____________________________________________________<br/>\n")
						writer.write("</p>")
						
						
						multiwriter.write("<h1 align=\"center\" style=\"color:darkviolet;font-family:Times;font-weight:bold;font-style:italic;font-size=200%\" class=\"text\">"+titreId+"</h1>")
						multiwriter.write("<p align=\"center\">")
						multiwriter.write("_____________________________________________________<br/>\n")
						multiwriter.write("auteur : "+auteur+"<br/>");
						multiwriter.write("date de composition : "+datecompo+"<br/>");
						multiwriter.write("sous-siècle : "+ssiecle+"<br/>");
						multiwriter.write("domaine : "+domaine+"<br/>");
						multiwriter.write("genre : "+genre+"<br/>");
						multiwriter.write("forme : "+forme+"<br/>");
						multiwriter.write("dialecte : "+dialecte+"<br/>");
						multiwriter.write("_____________________________________________________<br/>\n")
						multiwriter.write("</p>")
						
						break;
						break;
						case "head":
							writer.write("<h2>\n")
							multiwriter.write("<h2>\n")
							break;
						
						case "lg":
							writer.write("<p>\n")
							multiwriter.write("<p>\n")
							break;
						
						case "pb":
							if(wordcount > 0)
								createNextOutput();
							if(parser.getAttributeValue(null,"n") != null)
							{
								writer.write("<p style=\"color:red\" align=\"center\">- "+parser.getAttributeValue(null,"n")+" -</p>\n")
								multiwriter.write("<p style=\"color:red\" align=\"center\">- "+parser.getAttributeValue(null,"n")+" -</p>\n")
							}
							break;
						
						case "ab":
							if(parser.getAttributeValue(null,"n") != null)
							{
								writer.write("<p align=\"center\">"+parser.getAttributeValue(null,"n")+"</p>\n")
								multiwriter.write("<p align=\"center\">"+parser.getAttributeValue(null,"n")+"</p>\n")
							}
							break;
						case "l":
							writer.write("<br/>\n")
							multiwriter.write("<br/>\n")
							break;
						
						case "p":
						case "q":
							writer.write("<p>\n")
							multiwriter.write("<p>\n")
							break;
						
						case "div":
							if(parser.getAttributeValue(null,"type") == "titre")
							{
								writer.write("<h3>\n")
								multiwriter.write("<h3>\n")
								closeTitre = true;
							}
							else if(parser.getAttributeValue(null,"type") == "amen")
							{
								writer.write("<h3>\n")
								multiwriter.write("<h3>\n")
								closeAmen = true;
							}
							break;
													
						case "w":
							wordid = parser.getAttributeValue(null,"id");
							if(firstWord)
							{
								firstWord=false;
								this.idxstart.set(0,wordid);
							}
							wordcount++;
							if(wordcount >= wordmax)
							{
								
								wordcount=0;
							}
							break;
						
						case "choice":
							flagchoice = true;
							break;
						case "corr":
							flagcorr = true;
							break;
						case "sic":
							flagsic = true;
							break;
						case "reg":
							flagreg = true;
							break;
						case "orig":
							flagorig = true;
							break;
						case "foreign":
							flagforeign = true;
							break;
						
						case "ana":
							flaginterp=true;
							interpvalue+=" "+parser.getAttributeValue(null,"type").substring(1)+":"
							break;
						
						case "form":
							wordvalue="";
							interpvalue ="";
							flagform=true;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{
						case "head":
							writer.write("</h2>\n")
							multiwriter.write("</h2>\n")
							break;
						
						case "div":
							if(closeTitre)
							{
								writer.write("</h3>\n")
								multiwriter.write("</h3>\n")
								closeTitre = false;
							}
							else if(closeAmen)
							{
								writer.write("</h3>\n")
								multiwriter.write("</h3>\n")
								closeTitre = false;
							}
							break;
						
						case "lg":
							writer.write("</p>\n")
							multiwriter.write("</p>\n")
							break;
						
						case "lb":
							writer.write("<br/>\n")
							multiwriter.write("<br/>\n")
							break;
						
						case "p":
						case "q":
							writer.write("</p>\n")
							multiwriter.write("</p>\n")
							break;
						
						case "choice":
							flagchoice = false;
							break;
						case "corr":
							flagcorr = false;
							break;
						case "sic":
							flagsic = false;
							break;
						case "reg":
							flagreg = false;
							break;
						case "orig":
							flagorig = false;
							break;
						case "foreign":
							flagforeign = false;
							break;
						
						case "form":
							flagform = false
							break;
						
						case "ana":
							flaginterp = false
							break;
						
						case "w":
							int l = lastword.length();
							String color = "";
							if(flagcorr)
								color = "style=\"color: green;\"";
							else if(flagreg)
								color = "style=\"color: blue;\"";
							else if(flagforeign)
								color = "style=\"color: blue;\"";
							if(!flagchoice || flagcorr || flagreg)
							{
								String endOfLastWord = "";
								if(l > 0)
									endOfLastWord = lastword.subSequence(l-1, l);
								
								if(interpvalue != null)
									interpvalue = interpvalue.replace("\"","&quot;");
								
								if(NoSpaceBefore.contains(wordvalue) || 
										NoSpaceAfter.contains(lastword) || 
										wordvalue.startsWith("-") || 
										NoSpaceAfter.contains(endOfLastWord))
								{
									writer.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\" "+color+">");
									multiwriter.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\" "+color+">");
								}
								else
								{
									writer.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\" "+color+">");
									multiwriter.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\" "+color+">");
								}
								writer.write(wordvalue.replace("<", "&lt;")+"</span>");
								multiwriter.write(wordvalue.replace("<", "&lt;")+"</span>");
								lastword=wordvalue;
							}
						
						
							break;
					}
					break;
				
				case XMLStreamConstants.CHARACTERS:
					if(flagform)
						if(parser.getText().length() > 0)
							wordvalue+=(parser.getText());
					if(flaginterp)
						if(parser.getText().length() > 0)
							interpvalue+=(parser.getText());
					break;
			}
		}	
		writer.write("</body>");
		writer.write("</html>");
		writer.close();
		multiwriter.write("</body>");
		multiwriter.write("</html>");
		multiwriter.close();
		inputData.close();
	}

	/**
	 * Writecorrespondances file.
	 *
	 * @param file the file
	 * @return the java.lang. object
	 */
	public writecorrespondancesFile(File file)
	{
		Writer w = new OutputStreamWriter(new FileOutputStream(file,file.exists()) , "UTF-8");
		for(int i = 0 ; i < pages.size(); i ++)
		{
			w.write(pages.get(i).getName()+"\t"+idxstart.get(i)+"\n")
		}
		w.close();
	}
}
