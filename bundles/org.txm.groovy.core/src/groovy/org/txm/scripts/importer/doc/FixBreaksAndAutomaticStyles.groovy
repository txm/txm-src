package org.txm.scripts.importer.doc

import org.txm.utils.io.IOUtils;
import org.txm.utils.NameSpaceXmlNodePrinter
import org.txm.utils.NameSpaceXmlNodePrinter.NamespaceContext

import groovy.namespace.QName
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlParser

class FixBreaksAndAutomaticStyles {

	def nsText = new groovy.xml.Namespace("urn:oasis:names:tc:opendocument:xmlns:text:1.0", 'tei')
	def sytleAt;
	def pbAt;

	FixBreaksAndAutomaticStyles() {

	}
	def befores
	def afters
	def parents

	public boolean run(File xmlFile, def befores, def afters, def parents) {

		sytleAt = nsText.get("rend")
		pbAt = nsText.get("soft-page-break")


		def parser = new XmlParser(false, true)
		def root = parser.parse(xmlFile)

		this.befores = befores
		this.afters = afters
		this.parents = parents
		process(root, null)


		//println root.children()
		File tmp = new File(xmlFile.getParentFile(), "test.txt")
		def writer = IOUtils.getWriter(tmp, "UTF-8")
		writer.write('<?xml version="1.0" encoding="UTF-8"?>\n')
		def printer = new NameSpaceXmlNodePrinter(new PrintWriter(writer))
		//create namespace context
		NamespaceContext namespaceContext = new NamespaceContext(printer);
		namespaceContext.registerNamespacePrefix("office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0");
		namespaceContext.registerNamespacePrefix("style", "urn:oasis:names:tc:opendocument:xmlns:style:1.0");
		namespaceContext.registerNamespacePrefix("text", "urn:oasis:names:tc:opendocument:xmlns:text:1.0");
		namespaceContext.registerNamespacePrefix("table", "urn:oasis:names:tc:opendocument:xmlns:table:1.0");
		namespaceContext.registerNamespacePrefix("draw", "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0");
		namespaceContext.registerNamespacePrefix("fo", "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0");
		namespaceContext.registerNamespacePrefix("xlink", "http://www.w3.org/1999/xlink");
		namespaceContext.registerNamespacePrefix("dc", "http://purl.org/dc/elements/1.1/");
		namespaceContext.registerNamespacePrefix("meta", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0");
		namespaceContext.registerNamespacePrefix("number", "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0");
		namespaceContext.registerNamespacePrefix("svg", "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0");
		namespaceContext.registerNamespacePrefix("chart", "urn:oasis:names:tc:opendocument:xmlns:chart:1.0");
		namespaceContext.registerNamespacePrefix("dr3d", "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0");
		namespaceContext.registerNamespacePrefix("math", "http://www.w3.org/1998/Math/MathML");
		namespaceContext.registerNamespacePrefix("form", "urn:oasis:names:tc:opendocument:xmlns:form:1.0");
		namespaceContext.registerNamespacePrefix("script", "urn:oasis:names:tc:opendocument:xmlns:script:1.0");
		namespaceContext.registerNamespacePrefix("ooo", "http://openoffice.org/2004/office");
		namespaceContext.registerNamespacePrefix("ooow", "http://openoffice.org/2004/writer");
		namespaceContext.registerNamespacePrefix("oooc", "http://openoffice.org/2004/calc");
		namespaceContext.registerNamespacePrefix("dom", "http://www.w3.org/2001/xml-events");
		namespaceContext.registerNamespacePrefix("xforms", "http://www.w3.org/2002/xforms");
		namespaceContext.registerNamespacePrefix("xsd", "http://www.w3.org/2001/XMLSchema");
		namespaceContext.registerNamespacePrefix("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		namespaceContext.registerNamespacePrefix("rpt", "http://openoffice.org/2005/report");
		namespaceContext.registerNamespacePrefix("of", "urn:oasis:names:tc:opendocument:xmlns:of:1.2");
		namespaceContext.registerNamespacePrefix("xhtml", "http://www.w3.org/1999/xhtml");
		namespaceContext.registerNamespacePrefix("grddl", "http://www.w3.org/2003/g/data-view#");
		namespaceContext.registerNamespacePrefix("tableooo", "http://openoffice.org/2009/table");
		namespaceContext.registerNamespacePrefix("field", "urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0");
		namespaceContext.registerNamespacePrefix("formx", "urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0");
		namespaceContext.registerNamespacePrefix("css3t", "http://www.w3.org/TR/css3-text/");
		namespaceContext.registerNamespacePrefix("tei", "http://www.tei-c.org/ns/1.0");

		printer.setNamespaceAware(true);
		printer.print(root, namespaceContext)
		writer.close();

		if (xmlFile.delete()) tmp.renameTo(xmlFile);
		else println "FixBreaksAndAutomaticStyles: Error: could not replace "+xmlFile
	}

	public void process(def node, def parent) {
		if (!(node instanceof groovy.util.Node)) return;

		def list = new ArrayList(node.children())
		for( def child : list) {
			process(child, node)
		}
		String name = node.name().getLocalPart()
		if ("p" == name || "div" == name) {
			//println "P: "+node.attribute("rend")
			String style = node.attribute("rend")
			if (befores.contains(style)) {
				def newChild = new Node(null, "pb")
				newChild.parent = parent
				def idx = parent.children().indexOf(node)
				//println "b idx: $idx"
				parent.children().add(idx, newChild)
			} else if (afters.contains(style)) {
				def newChild = new Node(null, "pb")
				newChild.parent = parent
				def idx = parent.children().indexOf(node)
				//println "a idx: $idx"
				parent.children().add(idx+1, newChild)
			}
			if (parents.containsKey(style)) {
				//println "parent of "+style+ " = "+parents[style]
				node.attributes()["rend"] = parents[style]
			}
		}
	}

	public static void main(String[] args) {
		File xmlFile = new File("/home/mdecorde/xml/temoignages/content.xml")
		new FixBreaksAndAutomaticStyles().run(xmlFile, ["P1", "P2"], ["P3"], ["P1": "Standard", "P2":"Standard", "P3":"Standard"])
	}
}
