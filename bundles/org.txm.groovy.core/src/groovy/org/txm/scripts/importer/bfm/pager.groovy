// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
//
//
// $LastChangedDate: 2017-05-02 11:55:17 +0200 (mar. 02 mai 2017) $
// $LastChangedRevision: 3436 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.bfm;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

// TODO: Auto-generated Javadoc
/**
 * Build BFM texts HTML edition from the TEI-TXM files. <br/>
 * The result is similar to the one of the XSLT script made by Alexis Lavrentiev
 * (BFM & ICAR3). <br/>
 * 
 * @author mdecorde
 */
class pager {
	private List<String> NoSpaceBefore;

	/** The No space after. */
	private List<String> NoSpaceAfter;

	/** The wordcount. */
	private int wordcount = 0;

	/** The pagecount. */
	private int pagecount = 0;

	/** The wordmax. */
	private int wordmax = 0;

	/** The wordid. */
	private String wordid;

	/** The first word. */
	private boolean firstWord = true;

	/** The wordvalue. */
	private String wordvalue;

	/** The interpvalue. */
	private String interpvalue;

	/** The lastword. */
	private String lastword = " ";

	/** The wordtype. */
	private String wordtype;

	/** The flagform. */
	private boolean flagform = false;

	/** The flaginterp. */
	private boolean flaginterp = false;

	/** The url. */
	private URL url;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The writer. */
	private OutputStreamWriter writer;

	/** The multiwriter. */
	private OutputStreamWriter multiwriter = null;
	StaxStackWriter pagedWriter = null;

	/** The infile. */
	private File infile;

	/** The outfile. */
	private File outfile;
	private File outDir;

	/** The pages. */
	ArrayList<File> pages = new ArrayList<File>();

	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>();

	/** The titre id. */
	private String titreId;

	private String basename;
	private String textname;

	private def xpathProperties;

	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the file to convert
	 * @param outfile the result file
	 * @param NoSpaceBefore the punctuation marks that don't have a space before
	 * @param NoSpaceAfter the punctuation marks that don't have a space after
	 * @param max the max number of word per page
	 */
	pager(File infile, File outDir, String textname, List<String> NoSpaceBefore,
	List<String> NoSpaceAfter, int max, String basename, Properties xpathprops) {
		this.wordmax = max;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.basename = basename;
		this.textname = textname;
		this.outDir = outDir;
		this.xpathProperties = xpathprops;
		this.url = infile.toURI().toURL();
		this.infile = infile;

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		process();
	}

	private def closeMultiWriter()
	{
		if (pagedWriter != null) {
			def tags = pagedWriter.getTagStack().clone();

			if (firstWord) { // there was no words
				this.idxstart.add("w_0")
				pagedWriter.write("<span id=\"w_0\"/>");
			}
			pagedWriter.writeEndElements();
			pagedWriter.close();
			return tags;
		} else {
			return [];
		}
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput()
	{
		wordcount = 0;
		try {
			def tags = closeMultiWriter();
			for (int i = 0 ; i < tags.size() ; i++) {
				String tag = tags[i]
				if ("body" != tag) {
					tags.remove(i--)
				} else {
					tags.remove(i--) // remove "body"
					break; // remove elements until "body tag
				}
			}
			File outfile = new File(outDir, textname+"_"+(++pagecount)+".html")
			pages.add(outfile);
			firstWord = true; // waiting for next word

			pagedWriter = new StaxStackWriter(outfile, "UTF-8");

			pagedWriter.writeStartDocument("UTF-8", "1.0")
			pagedWriter.writeStartElement("html")
			pagedWriter.writeEmptyElement("meta", ["http-equiv":"Content-Type", "content":"text/html","charset":"UTF-8"]);
			pagedWriter.writeStartElement("head")
			pagedWriter.writeStartElement("title")
			pagedWriter.writeCharacters(basename.toUpperCase()+" - "+textname+" - Edition - Page "+pagecount)
			pagedWriter.writeEndElement() // </title>
			pagedWriter.writeEndElement() // </head>
			pagedWriter.writeStartElement("body") //<body>
			pagedWriter.writeStartElements(tags);
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			return createNextOutput();
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the pages create during the processing
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the word's indexes
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	/**
	 * process the infile.
	 */
	void process() {
		String localname = "";

		boolean closeTitre = false;
		boolean closeAmen = false;

		boolean flagchoice = false;
		boolean flagcorr = false;
		boolean flagsic = false;
		boolean flagreg = false;
		boolean flagexpan = false;
		boolean flagorig = false;
		boolean flagabbr = false;
		boolean flagforeign = false;
		int levelSupplied = 0;
		boolean flagSurplus = false;
		boolean flagDel = false;

		// option for metadata from BFM TEI header
		def mValues = [:];
		def xpathprocessor = new XPathResult(infile)
		for (String name : xpathProperties.keySet()) {
			String value = xpathprocessor.getXpathResponse(xpathProperties.get(name), "N/A");
			mValues.put(name, value.replace("\n", " "));
		}
		if (mValues.containsKey("forme") && !mValues.get("forme").equals("N/A")) {
			if (mValues.get("forme").startsWith("#forme_")) {
				mValues.put("forme", mValues.get("forme").substring(7));
			} else if (mValues.get("forme").startsWith("#")) {
				mValues.put("forme", mValues.get("forme").substring(1));
			} else {
				mValues.put("forme", mValues.get("forme"));
			}
		}
		createNextOutput();
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "text":
							pagedWriter.writeStartElement("h1",
							["align":"center", "style":"color:darkviolet;font-family:Times;font-weight:bold;font-style:italic;font-size=200%", "class":"text"])
							pagedWriter.writeCharacters(mValues.get("titre"))
							pagedWriter.writeEndElement(); // h1
							pagedWriter.writeStartElement("p")
							pagedWriter.writeEmptyElement("hr")
							pagedWriter.writeCharacters("auteur : "+mValues.get("auteur"))
							pagedWriter.writeEmptyElement("br");
							pagedWriter.writeCharacters("date de composition : "+mValues.get("datecompolibre"));
							pagedWriter.writeEmptyElement("br");
							pagedWriter.writeCharacters("domaine : "+mValues.get("domaine"))
							pagedWriter.writeEmptyElement("br");
							pagedWriter.writeCharacters("genre : "+mValues.get("genre"))
							pagedWriter.writeEmptyElement("br");
							pagedWriter.writeCharacters("forme : "+mValues.get("forme"))
							pagedWriter.writeEmptyElement("br");
							pagedWriter.writeCharacters("dialecte : "+mValues.get("dialecte"))
							pagedWriter.writeEmptyElement("br");
							pagedWriter.writeEmptyElement("hr")
							pagedWriter.writeEndElement(); // p
							break;
						case "head":
							pagedWriter.writeStartElement("h2")
							break;
						case "lg":
						case "p":
							pagedWriter.writeStartElement("p")
							break;
						case "pb":
							createNextOutput();
							if (parser.getAttributeValue(null,"n") != null) {
								pagedWriter.writeStartElement("p", ["style":"color:red", "align":"center"]);
								pagedWriter.writeCharacters("- "+parser.getAttributeValue(null,"n")+" -")
								pagedWriter.writeEndElement() // p
							}
							break;

						case "ab":
							if (parser.getAttributeValue(null,"n") != null) {
								pagedWriter.writeStartElement("p", ["align":"center"]);
								pagedWriter.writeCharacters(parser.getAttributeValue(null,"n"))
								pagedWriter.writeEndElement() // p
							}
							break;
						case "l":
							pagedWriter.writeEmptyElement("br");
							break;
						case "div":
							if (parser.getAttributeValue(null,"type") == "titre") {
								pagedWriter.writeStartElement("h3")
								closeTitre = true;
							} else if (parser.getAttributeValue(null,"type") == "amen") {
								pagedWriter.writeStartElement("h3")
								closeAmen = true;
							}
							break;
						case "w":
							interpvalue = "";
							for (int i = 0 ; i < parser.getAttributeCount(); i++) {
								if (parser.getAttributeLocalName(i).equals("type")) {
									interpvalue = " pos: "+parser.getAttributeValue(i);
								}
							}
							wordid = parser.getAttributeValue(null, "id");

							wordcount++;
							if (wordcount >= wordmax) {
								createNextOutput();
							}
							
							if (firstWord) {
								firstWord = false;
								this.idxstart.add(wordid);
							}
							
							break;

						case "choice":
							flagchoice = true;
							break;
						case "corr":
							flagcorr = true;
							break;
						case "sic":
							flagsic = true;
							break;
						case "reg":
							flagreg = true;
							break;
						case "orig":
							flagorig = true;
							break;
						case "foreign":
							flagforeign = true;
							break;
						case "supplied":
							levelSupplied = levelSupplied +1;
							if(flagform)
								wordvalue = wordvalue+"[";
							break;

						case "surplus":
							flagSurplus = true;
							pagedWriter.writeStartElement("span", ["class": "surplus", "style":"color:red;"])
							break;

						case "del":
							flagDel = true;
							pagedWriter.writeStartElement("span", ["class": "del", "style":"color:red;text-decoration:line-through;"])
							break;

						case "ana":
							flaginterp=true;
							interpvalue+=" "+parser.getAttributeValue(null,"type").substring(1)+":"
							break;

						case "form":
							wordvalue="";
							flagform=true;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "head":
							pagedWriter.writeEndElement() // h2
							break;

						case "div":
							if (closeTitre) {
								pagedWriter.writeEndElement() // h3
								closeTitre = false;
							} else if(closeAmen) {
								pagedWriter.writeEndElement() // h3
								closeTitre = false;
							}
							break;

						case "lb":
							pagedWriter.writeEmptyElement("br")
							break;
						case "lg":
						case "p":
							pagedWriter.writeEndElement() // p
							break;

						case "choice":
							flagchoice = false;
							break;
						case "corr":
							flagcorr = false;
							break;
						case "sic":
							flagsic = false;
							break;
						case "reg":
							flagreg = false;
							break;
						case "orig":
							flagorig = false;
							break;
						case "foreign":
							flagforeign = false;
							break;
						case "supplied":
							levelSupplied = levelSupplied -1;
							if (flagform)
								wordvalue = wordvalue+"]";
							break;
						case "surplus":
							flagSurplus = false;
							pagedWriter.writeCharacters(")")
							pagedWriter.writeEndElement() // span
							break;

						case "del":
							flagDel = false;
							pagedWriter.writeEndElement() // span
							break;

						case "form":
							flagform = false
							break;

						case "ana":
							flaginterp = false
							break;

						case "w":
							int l = lastword.length();
							String color = "";
							if (flagcorr)
								color = "color: green;";
							else if (flagreg)
								color = "color: darkgreen;";
							else if (flagforeign)
								color = "color: darkred;";
							else if (levelSupplied == 1)
								color = "color: blue;";
							else if (levelSupplied == 2)
								color = "color: darkblue;";
							if (!flagchoice || flagcorr || flagreg) {
								String endOfLastWord = "";
								if (l > 0)
									endOfLastWord = lastword.subSequence(l-1, l);

								if (NoSpaceBefore.contains(wordvalue) ||
								NoSpaceAfter.contains(lastword) ||
								wordvalue.startsWith("-") ||
								NoSpaceAfter.contains(endOfLastWord))
								{
									//multiwriter.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\" "+color+">");
								} else {
									//multiwriter.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\" "+color+">");
									pagedWriter.writeCharacters(" ")
								}
								
								pagedWriter.writeStartElement("span", ["title":interpvalue, "id":wordid, "style":color]) // span
								pagedWriter.writeCharacters(wordvalue) 
								pagedWriter.writeEndElement() // span
								lastword=wordvalue;
							}
							break;
					}
					break;

				case XMLStreamConstants.CHARACTERS:
					if (flagform && parser.getText().length() > 0)
							wordvalue += (parser.getText());
					if (flaginterp && parser.getText().length() > 0)
							interpvalue += (parser.getText());
					break;
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		closeMultiWriter();
	}
}
