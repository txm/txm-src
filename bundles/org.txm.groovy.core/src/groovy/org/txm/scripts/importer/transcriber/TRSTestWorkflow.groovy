package org.txm.scripts.importer.transcriber

import org.txm.utils.FileUtils
import org.txm.utils.io.FileCopy;

/**
 * Test and Fix workflow for the STDDAD master corpus
 * @author mdecorde
 *
 */

File rootDir = new File("/home/mdecorde/xml/leman/")
if (!rootDir.exists()) println "Error rootDir: "+rootDir;

File dtdFile = new File(rootDir, "trans-14.dtd")
File renamedTRSDir = new File(rootDir, "final 2012-03-26")
File corrTRSDir = new File(rootDir, "corr")
File okTRSDir = new File(rootDir, "ready")

corrTRSDir.delete();
corrTRSDir.mkdir();
FileCopy.copy(dtdFile, new File(corrTRSDir, dtdFile.getName()));
//okTRSDir.delete();
okTRSDir.mkdir();
FileCopy.copy(dtdFile, new File(okTRSDir, dtdFile.getName()));

println "1- FixTimming"
def files = FileUtils.listFiles(renamedTRSDir);
for (File trsFile : files) {
	if (!trsFile.getName().endsWith(".trs")) continue;
	println "FIX: "+trsFile
	File newTRSFile = new File(corrTRSDir, trsFile.getName())
	new FixTiming().fixTRS(trsFile, newTRSFile);
}

println "2- Validate"
files = FileUtils.listFiles(corrTRSDir);
for(File trsFile : files) {
	if(!trsFile.getName().endsWith(".trs")) continue;
	File newTRSFile = new File(okTRSDir, trsFile.getName())
	println "VALIDATE: "+trsFile
	if(ValidateTRS.checkTRS(trsFile))
		FileCopy.copy(trsFile, newTRSFile);
}
/*
println "3- FINALLY"
if (renamedTRSDir.listFiles().size() != corrTRSDir.listFiles().size) {
	println "Missing corr transcriptions"
	return;
}

if (corrTRSDir.listFiles().size() != okTRSDir.listFiles().size) {
	println "Missing ok transcriptions"
	return;
}
*/