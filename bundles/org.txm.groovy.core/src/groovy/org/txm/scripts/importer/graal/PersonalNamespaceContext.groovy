// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-02-01 09:53:38 +0100 (lun. 01 févr. 2016) $
// $LastChangedRevision: 3101 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.graal

import java.util.Iterator;

import javax.xml.*;
import javax.xml.namespace.NamespaceContext;

import org.xml.sax.helpers.NamespaceSupport;

/**
 * Instantiates a new personal namespace context.
 *
 * @author ayepdieu, mdecorde
 * 
 * do correpondances between namespace and uri
 */
public class PersonalNamespaceContext implements NamespaceContext {
	
	public static HashMap<String, String> adress2name = new HashMap<String, String>();
	public static HashMap<String, String> name2adress = new HashMap<String, String>();
	static {
		addNamespace("tei", "http://www.tei-c.org/ns/1.0")
		addNamespace("me", "http://www.menota.org/ns/1.0")
		addNamespace("bfm", "http://bfm.ens-lsh.fr/ns/1.0")
		addNamespace("txm", "http://textometrie.org/1.0")
		addNamespace("xi", "http://www.w3.org/2001/XInclude")
		addNamespace("fn", "http://www.w3.org/2005/xpath-functions")
		addNamespace("xhtml", "http://www.w3.org/1999/xhtml")
		addNamespace("svg", "http://www.w3.org/2000/svg")
		addNamespace(XMLConstants.XML_NS_PREFIX, XMLConstants.XML_NS_URI)
	}
	/*
	String TEINS = "http://www.tei-c.org/ns/1.0"
	String MENS = "http://www.menota.org/ns/1.0";
	String BFMNS = "http://bfm.ens-lsh.fr/ns/1.0";
	String TXMNS = "http://textometrie.org/1.0";
	String XINS = "http://www.w3.org/2001/XInclude";
	String FNNS = "http://www.w3.org/2005/xpath-functions";
	String XHTMLNS = "http://www.w3.org/1999/xhtml";
	String SVGNS = "http://www.w3.org/2000/svg"
	String XMLNS = "http://www.w3.org/XML/1998/namespace"

	String TEINSNAME = "tei"
	String MENSNAME = "me";
	String BFMNSNAME = "bfm";
	String TXMNSNAME = "txm";
	String XINSNAME = "xi";
	String FNNSNAME = "fn";
	String XHTMLNSNAME = "xhtml";
	String SVGNSNAME = "svg"
*/
	public PersonalNamespaceContext() {
		super();
	}

	public static void addNamespace(String name, String address) {
		adress2name.put(address, name);
		name2adress.put(name, address);
	}
	
	/* (non-Javadoc)
	 * @see javax.xml.namespace.NamespaceContext#getNamespaceURI(java.lang.String)
	 */
	public String getNamespaceURI(String prefix) {
		if (!name2adress.containsKey(prefix)) return XMLConstants.NULL_NS_URI;
		return name2adress.get(prefix)
	}

	private static String NONE = "";
	/**
	 * 
	 * @param uri the uri
	 * @return the prefix
	 */
	public String getPrefix(String uri) {
		
		if (uri == null) return NONE;
		if (!adress2name.containsKey(uri)) return NONE;
		return adress2name.get(uri)
	}

	/**
	 * This method isn't necessary for XPath processing either.
	 *
	 * @param uri the uri
	 * @return the prefixes
	 */
	public Iterator getPrefixes(String uri) {
		throw new UnsupportedOperationException();
	}
}