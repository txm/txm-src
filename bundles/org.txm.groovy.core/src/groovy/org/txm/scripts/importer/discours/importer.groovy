// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-06-26 16:53:47 +0200 (lun. 26 juin 2017) $
// $LastChangedRevision: 3451 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.discours

//import org.txm.scripts.filters.TabulatedToXml.*;
import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.metadatas.Metadatas
import org.txm.importer.scripts.xmltxm.*;

import javax.xml.stream.*;

import org.txm.core.preferences.TBXPreferences
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.txm.*;
import org.txm.core.engines.*;
import org.txm.utils.*
import org.txm.utils.FileUtils
import org.txm.utils.io.*;
import org.txm.utils.i18n.DetectBOM;

/**
 * The Class importer.
 */
class importer {
	
	/**
	 * Run.
	 *
	 * @param dir the dir
	 * @param encoding the encoding
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(File srcDir, File binDir, File txmDir, String encoding, String basename, def project) {
		
		String rootDir = srcDir.getAbsolutePath()+"/"
		// scanning directory brut/*.cnr
		String extension = "cnr";
		File[] list = FileUtils.listFiles(srcDir, ".*\\.cnr")
		
		if (list == null || list.length == 0) return false;
		
		LinkedList filelist = new LinkedList();
		for (File cnr : list) {
			if (cnr.getName().endsWith("."+extension)) {
				filelist.add(cnr.getName());
			}
		}
		Collections.sort(filelist);
		
		// get the headers
		def csvfile = Metadatas.findMetadataFile(srcDir);
		Metadatas metadatas; // text metadata
		
		
		if (csvfile != null && csvfile.exists()) {
			println "Trying to read metadata from: "+csvfile
			File copy = new File(binDir, csvfile.getName())
			if (!FileCopy.copy(csvfile, copy)) {
				println "Error: could not create a copy of metadata file "+csvfile.getAbsoluteFile();
				return;
			}
			metadatas = new Metadatas(copy, Toolbox.getMetadataEncoding(),
					Toolbox.getMetadataColumnSeparator(),
					Toolbox.getMetadataTextSeparator(), 1)
		} else {
			println "No metadata file: "+csvfile
			println "Aborting"
			return false;
		}
		
		// build each xml file from cnr
		def attrs = ["para", "sent", "form", "lem", "pos", "func"];
		int formid = 2;
		String separator = "\t";
		
		String encodingAll = null;
		if (encoding == "??") {
			encodingAll = new CharsetDetector(srcDir).getEncoding();
			println "Guessed encoding: $encodingAll"
		}
		
		print "Converting CNR to XML:"
		def second = 0
		for (String id : metadatas.keySet()) { // loop on text nodes
		
			int sentence=-1, paragraph=-1, idword=0;
			String lastopenlocalname= "", localname = "";
			int para = -1, sent = -1;
			
			// = textnode.attribute("id")
			ArrayList<org.txm.metadatas.Entry> metas = metadatas.get(id);
			
			String filename = id +".cnr"
			File srcfile = new File(srcDir, filename)
			if (!srcfile.exists()) {
				println "Can't find CNR file : "+filename
				continue
			}
			File teifile = new File(txmDir, id+".xml")
			//println("||"+srcfile+">>"+teifile.getAbsolutePath());
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(teifile), "UTF-8")
			
			//			if (second) { print(", ") }
			//			if ((second % 5) == 0) println ""
			//			print(filename.replaceFirst("\\.cnr", ""));
			//			second++
			
			// HEADER
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			writer.write("<TEI xmlns=\"http://www.tei-c.org/ns/1.0\" xmlns:txm=\"http://textometrie.org/1.0\">\n");
			writer.write("<teiHeader type=\"text\">\n");
			writer.write("<fileDesc>\n");
			writer.write("<titleStmt>\n");
			writer.write("<title>"+id+"</title>\n");
			writer.write("<respStmt>\n");
			writer.write("<resp id=\"cordial\">initial tagging</resp>")
			writer.write("</respStmt>\n");
			writer.write("</titleStmt>\n");
			writer.write("</fileDesc>\n");
			writer.write("<encodingDesc>\n");
			writer.write("<classDecl>\n");
			writer.write("<taxonomy id=\"pos\"><bibl type=\"tagset\"/></taxonomy>\n")
			writer.write("<taxonomy id=\"func\"><bibl type=\"tagset\"/></taxonomy>\n")
			writer.write("<taxonomy id=\"lemma\"><bibl type=\"tagset\"/></taxonomy>\n")
			writer.write("</classDecl>\n");
			writer.write("</encodingDesc>\n");
			writer.write("</teiHeader>\n");
			writer.write("<text ")
			for (org.txm.metadatas.Entry metadata : metas) {
				String name = metadata.getId()
				String value = metadata.getValue()
				if (name != "id")
					writer.write(" "+name+"=\""+value.replace("&", "&amp;").replace("<", "&lt;").replace("\"", "&quot;")+"\"");
			}
			writer.write(" id=\""+id.replace("&", "&amp;").replace("<", "&lt;").replace("\"", "&quot;")+"\"");
			writer.write(">\n");
			
			//def content = srcfile.getText(encoding)
			String tmpEncoding = encoding;
			if (encodingAll != null) {
				tmpEncoding = encodingAll
				if (srcfile.length() > CharsetDetector.MINIMALSIZE) {
					tmpEncoding = new CharsetDetector(srcfile).getEncoding()
					//println "file encoding: $tmpEncoding"
				}
			}
			def input = new FileInputStream(srcfile)
			def reader = new BufferedReader(new InputStreamReader(input , tmpEncoding))
			DetectBOM bomdetector = new DetectBOM(srcfile);
			for (int ibom = 0 ; ibom < bomdetector.getBOMSize() ; ibom++) input.read()
			
			// BODY
			String line = reader.readLine();
			int nline = 0;
			def errors = [];
			while (line != null) {
				line = CleanFile.cleanAllButTabs(line); // remove ctrl and surrogate chars
				
				nline++;
				String[] fields = line.split(separator);
				if (fields.size() == attrs.size()) {
					
					idword++;
					
					String paraV = fields[0].replaceAll("\\p{C}", "")
					String sentV = fields[1].replaceAll("\\p{C}", "")
					String formV = fields[2].replaceAll("\\p{C}", "").replace("&","&amp;").replace("<","&lt;");
					String lemV = fields[3].replaceAll("\\p{C}", "").replace("&","&amp;").replace("<","&lt;");
					String posV = fields[4].replaceAll("\\p{C}", "").replace("&","&amp;").replace("<","&lt;");
					String funcV = fields[5].replaceAll("\\p{C}", "").replace("&","&amp;").replace("<","&lt;");
					
					try {
						para = Integer.parseInt(paraV);
					} catch(Exception e){}
					if (para != paragraph) {
						if (paragraph != -1) {
							if (sentence != -1) {
								writer.write("</s>\n")
								sentence = -1;
							}
							writer.write("</p>\n")
						}
						writer.write("<p id=\"p_${para}\">\n");
						paragraph = para;
					}
					
					try {
						sent = Integer.parseInt(sentV);
					} catch(Exception e){}
					if (sent != sentence) {
						if (sentence != -1)
							writer.write("</s>\n")
						writer.write("<s id=\"s_${sent}\">\n");
						sentence = sent;
					}
					
					writer.write("<w id=\"w_${id}_${idword}\">\n");
					writer.write(" <txm:form>$formV</txm:form>\n");
					writer.write(" <txm:ana resp=\"#cordial\" type=\"#pos\">$posV</txm:ana>\n");
					writer.write(" <txm:ana resp=\"#cordial\" type=\"#func\">$funcV</txm:ana>\n");
					writer.write(" <txm:ana resp=\"#cordial\" type=\"#lemma\">$lemV</txm:ana>\n");
					writer.write(" <txm:ana resp=\"#cordial\" type=\"#sent\">$sentV</txm:ana>\n");
					writer.write(" <txm:ana resp=\"#cordial\" type=\"#para\">$paraV</txm:ana>\n");
					writer.write(" <txm:ana resp=\"#txm\" type=\"#ref\">${id}, §${paraV}</txm:ana>\n");
					writer.write("</w>\n");
				} else {
					//println "error : $nline : '$line' -> $fields"
					if (line.length() > 0) errors << nline
				}
				line = reader.readLine();
			}
			
			// CLOSE
			if(sentence != -1) { writer.write("</s>\n") }
			if(paragraph != -1) { writer.write("</p>\n") }
			paragraph=-1;
			sentence=-1;
			writer.write("</text>\n");
			writer.write("</TEI>\n");
			writer.close();
			
			if (errors.size() > 0) {
				println "Errors in file $filename. Wrong number of columns at lines: "+errors
			} else print "."
		}
		println ""
		
		if (project.getCleanAfterBuild()) {
			
			new File(project.getProjectDirectory(), "tokenized").deleteDir()
			new File(project.getProjectDirectory(), "ptokenized").deleteDir()
			new File(project.getProjectDirectory(), "stokenized").deleteDir()
			new File(project.getProjectDirectory(), "src").deleteDir()
			new File(project.getProjectDirectory(), "split").deleteDir()
		}
		return true;
	}
	
	/**
	 * Gets the metadata.
	 *
	 * @return the metadata
	 */
	public List<String> getMetadatas() {
		
		return metadatas;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		File dir = new File(System.getProperty("user.home"), "xml/discours/");
		new importer().run(dir);
	}
}