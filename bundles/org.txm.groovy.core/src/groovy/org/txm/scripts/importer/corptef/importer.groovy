// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.corptef;

import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;
import javax.xml.stream.*;
import java.net.URL;

import org.txm.Toolbox;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.ReunitBrokenWords.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.TagSentences.*;
import org.txm.scripts.filters.FusionHeader.*;

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {
	
	/**
	 * Run.
	 *
	 * @param dir the dir
	 */
	public void run(File dir)
	{
		//prepare
		String rootDir = dir.getAbsolutePath()+"/";

		//clean directories
		File binDir = new File(Toolbox.getTxmHomePath(),"corpora/corptef");
		binDir.deleteDir();
		binDir.mkdir();
		new File(binDir,"ptokenized").deleteDir();
		new File(binDir,"ptokenized").mkdir();
		new File(binDir,"tokenized").deleteDir();
		new File(binDir,"tokenized").mkdir();
		new File(binDir,"headers").deleteDir();
		new File(binDir,"headers").mkdir();
		new File(binDir,"txm").deleteDir();
		new File(binDir,"txm").mkdir();

		ArrayList<String> milestones = new ArrayList<String>();

		List<File> files = new File(rootDir,"").listFiles();//scan directory split

		//PREPARE EACH FILE TO BE TOKENIZED
		for(File f : files)
		{
			File srcfile = f;
			File resultfile = new File(binDir,"ptokenized/"+f.getName());
			println("prepare tokenizer file : "+srcfile+" to : "+resultfile );

			def builder2 = new OneTagPerLine(srcfile.toURL(), milestones);
			builder2.process(resultfile);
		}

		rootDir = binDir.getAbsolutePath();

		//TOKENIZE FILES
		//Manager<Filter> filterManager = new FilterManager(ActionHome);	
		files = new File(rootDir,"ptokenized").listFiles();//scan directory split
		for(File infile : files)
		{
			Sequence S = new Sequence();
			Filter F1 = new CutHeader();
			Filter F2 = new ReunitBrokenWords();
			Filter F6 = new Tokeniser(infile);
			Filter F7 = new TagSentences();
			Filter F11 = new FusionHeader();
			S.add(F1);
			S.add(F2);
			S.add(F6);
			//S.add(F7);
			S.add(F11);

			File xmlfile = new File(rootDir,"tokenized/"+infile.getName());
			println("Tokenizing "+xmlfile)
			S.SetInFileAndOutFile(infile.getAbsolutePath(), xmlfile.getAbsolutePath());
			S.setEncodages("UTF-8","UTF-8");

			Object[] arguments1 = [rootDir+"/headers/"+infile.getName()+"header.xml"];
			F1.SetUsedParam(arguments1);
			Object[] arguments2 = [rootDir+"/headers/"+infile.getName()+"header.xml",F1];
			F11.SetUsedParam(arguments2);

			S.proceed();
			S.clean();
		}

		files = new File(rootDir,"tokenized").listFiles()
		//TRANSFORM INTO XML-TEI-TXM
		for(File f : files)
		{
			//ArrayList<String> milestones = new ArrayList<String>();
			println("Building xml-tei-txm "+f+ " >> "+f.getName())
			File file = f; 
			String txmfile = f.getName();

			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()

			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>();	
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
			//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();

			//lance le traitement
			def builder3 = new Xml2Ana(file);
			builder3.setCorrespondances(correspRef, correspType);
			builder3.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			builder3.transformFile(rootDir,"txm/"+txmfile);
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("C:/TXM/corpora/corptef/")
		new importer().run(dir);
	}
}
