<?xml version="1.0" encoding="UTF-8"?>

<!-- This is a stylesheet that gives a "normalized" presentation of old French 
	manuscript transcriptions - abbreviations are resolved; - agglutinations 
	and deglutinations are not marked (word limits are "normalized"); - punctuation 
	is normalized as it is in the edition; - direct speech is marked with quotation 
	marks and darkblue color; - uncertain readings are not marked; - text deleted 
	in the original is not visualized; - corrections are marked with darkred 
	color; - notes are rendered with [*] sign in the text body; the text of the 
	note appears on "mouse-over" and is reproduced in the end. -->



<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">
	<xsl:param name="form">
		normalisée
	</xsl:param>

	<xsl:variable name="mode">
		facs
	</xsl:variable>

	<xsl:output method="html" encoding="UTF-8" version="4.0"
		indent="yes" />

	<xsl:strip-space elements="*" />

	<!-- <xsl:variable name="title-formal" select="//fileDesc/titleStmt/title[@type='formal']"/> 
		<xsl:variable name="title-normal" select="//fileDesc/titleStmt/title[@type='normal']"/> 
		<xsl:variable name="title-ref" select="//fileDesc/titleStmt/title[@type='reference']"/> 
		<xsl:variable name="author" select="//titleStmt/author"/> -->

	<xsl:include href="mdv_common.xsl" />

	<xsl:template match="//tei:p|//tei:head">
		<p>
			<xsl:if test="@n">
				[ §
				<xsl:value-of select="@n" />
				]&#xa0;
			</xsl:if>
			<xsl:apply-templates mode="norm" />
		</p>
	</xsl:template>

	<xsl:template match="tei:pb" mode="norm">
		<xsl:call-template name="pb_common" />
	</xsl:template>

	<xsl:template match="tei:cb" mode="norm">
		<xsl:call-template name="cb_common" />
	</xsl:template>

	<xsl:template match="tei:note" mode="norm">
		<xsl:call-template name="note_common" />
	</xsl:template>

	<xsl:template match="tei:q" mode="norm">
		<span style="background-color:khaki">
			<xsl:apply-templates mode="norm" />
		</span>
	</xsl:template>

	<xsl:template match="tei:gap" mode="norm">
		<xsl:call-template name="gap_common" />
	</xsl:template>

	<xsl:template match="//tei:lb" mode="norm">
		<xsl:choose>
			<xsl:when test="position()=1 and not(ancestor::tei:seg)" />
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//tei:w" mode="norm">
		<span class="word" id="{@xml:id}" title="{@type}">
			<xsl:if
				test="starts-with(.,':') or starts-with(.,'!') or starts-with(.,'?') or starts-with(.,';')">
				&#xa0;
			</xsl:if>
			<xsl:apply-templates select="descendant::me:norm"
				mode="norm" />
			<xsl:choose>
				<xsl:when
					test="following::*[1][self::bfm:punct][starts-with(., ',') or starts-with(., '.') or starts-with(., ']') or starts-with(., ')') or starts-with(.,':') or starts-with(.,'!') or starts-with(.,'?') or starts-with(.,';')]" />
				<xsl:when test="child::me:norm[string-length(.) = 0]" />
				<xsl:when test="@bfm:aggl='elision'">
					'
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>

	<xsl:template match="tei:sic" mode="norm">
		<span style="text-decoration:line-through;color:red">
			<xsl:apply-templates mode="norm" />
		</span>
	</xsl:template>

	<xsl:template match="tei:del" mode="norm">
	</xsl:template>


	<xsl:template match="//tei:hi[@rend='sup']" mode="norm">
		<sup>
			<xsl:apply-templates mode="norm" />
		</sup>
	</xsl:template>



	<!-- <xsl:template match="del"> </xsl:template> <xsl:template match="name"> 
		<xsl:value-of select="@reg"/> </xsl:template> -->
</xsl:stylesheet>
