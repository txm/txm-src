<?xml version='1.0'?>

<!-- ####################################### Qgraal_cm BASIC stylesheet ####################################### 
	author: Alexei Lavrentiev version: 3.3 date: 2011-06-03 (handling agglutinations 
	of "en cours de figement" phrases) version: 3.2 date: 2011-01-19 (handling 
	inline and final notes) version: 3.1 date: 2010-06-17 (utf-8 encoding; correcting 
	spacing after "sic : " and "comma" sign) version: 3.0 date: 2010-01-25 (adding 
	table with line numbers and text in different columns) version: 2.0 date: 
	2009-08-24 (including multi-facet transcription) version: 1.2 date: 2009-02-16 
	version: 1.1 date: 2008-09-30 version: 1.0 date: 2008-02-21 ####################################### 
	Description This is a basic stylesheet for viewing BFM texts in XML-TEI -->
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0" version="1.0">

	<xsl:output method="html" encoding="utf-8" indent="yes" />

	<xsl:strip-space elements="*" />

	<xsl:variable name="title-source"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='source']" />

	<xsl:variable name="title-normal"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='normal']" />

	<xsl:variable name="title-ref"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='reference']" />


	<xsl:variable name="author"
		select="//tei:titleStmt/tei:author" />

	<xsl:variable name="apostrophe" select='"&apos;"' />

	<!-- <xsl:template match="/"> <html> <head> <meta http-equiv="Content-Type" 
		content="text/html;charset=UTF-8" /> <title> <xsl:if test="$author[not(contains(.,'anonym'))]"> 
		<xsl:value-of select="$author"/><xsl:text> : </xsl:text> </xsl:if> <xsl:value-of 
		select="$title-normal"/> </title> <xsl:apply-templates select="//tei:teiHeader"/> 
		</head> <body style="margin-left:10%;margin-right:10%"> <div class="text"> 
		<xsl:apply-templates select="//tei:text"/> </div> <div class="notes"> <p 
		align="center"><b>Notes</b></p> <xsl:apply-templates select="//tei:note[not(@place='inline')]" 
		mode="notes"/> </div> </body> </html> </xsl:template> -->

	<xsl:template match="/">
		<xsl:apply-templates
			select="//tei:text//tei:p|//tei:note[@place='inline']" />
	</xsl:template>

	<!-- TEI-Header Processing -->

	<xsl:template match="//tei:teiHeader">
		<p align="center">

			<!-- Author (only appears if not anonymous) -->
			<xsl:if test="$author[not(contains(.,'anonym'))]">
				<span style="font-variant:small-caps;font-weight:bold">
					<xsl:value-of select="$author" />
				</span>
				<br />
			</xsl:if>
			<!-- Title -->
			<span
				style="color:darkviolet;font-family:Times;font-weight:bold;font-style:italic;font-size:200%">
				<xsl:value-of select="$title-normal" />
			</span>
			<br />
			<!-- Scientific editor(s) -->
			Edition par
			<xsl:apply-templates
				select="//tei:titleStmt/tei:editor[@role='editor']" />
			<br />
			Avec la collaboration de
			<xsl:apply-templates
				select="//tei:titleStmt/tei:editor[@role='associate_editor']" />
			<br />

			<!-- Series Statement, Publisher, Year <xsl:apply-templates select="fileDesc/seriesStmt/title"/><br/> 
				<xsl:apply-templates select="//sourceDesc//publicationStmt/pubPlace"/>, <xsl:apply-templates 
				select="//sourceDesc//publicationStmt/publisher"/>, <xsl:apply-templates 
				select="//sourceDesc//publicationStmt/date"/> -->
		</p>

		<!-- Information on the electronic version -->
		<p align="center">
			_____________________________________________________
			<br />
			<br />
			<span style="font-size:75%">
				Edition électronique
				<br />
				Dernière révision :
				<xsl:value-of
					select="//tei:revisionDesc/tei:change[1]/@when" />
			</span>
			<br />
			_____________________________________________________
		</p>
		<p align="center">
			Version courante
			<br />
			_____________________________________________________
		</p>

	</xsl:template>

	<!-- Last change date formatting <xsl:template match="//revisionDesc/change/date[following-sibling::item[contains(.,'Dernière 
		mise à jour')]]"> <xsl:value-of select="substring-before(., ' 00:')"/> </xsl:template> -->

	<!-- End of TEI-header processing -->

	<!-- Text body processing -->

	<!-- "Original" titles found sometimes immediately before text body -->
	<xsl:template match="//tei:div[@type='titre']">
		<p align="center">
			<b>
				<xsl:apply-templates />
			</b>
		</p>
	</xsl:template>

	<!-- Text divisions (general) -->
	<xsl:template match="//tei:div[not(@type='titre')]">
		<xsl:if test="child::tei:head">
			<xsl:apply-templates select="head" />
			<br />
		</xsl:if>
		<xsl:apply-templates
			select="*[not(self::tei:head)]" />
	</xsl:template>

	<!-- Paragraphs -->
	<xsl:template match="//tei:text//tei:p">
		<table width="100%" style="border:none">
			<tr style="border:none">
				<td width="10%" valign="top"
					style="white-space:nowrap;padding-right:10pt;line-height:18px;font-family:Arial;font-size:14px;border-:none;text-align:right">
					<xsl:if test="@n">
						<span style="color:gray;font-size:12px">
							[§
							<xsl:value-of select="@n" />
							]
						</span>
					</xsl:if>
					<xsl:apply-templates
						select="descendant::tei:lb|descendant::tei:milestone[@unit='column']"
						mode="number" />
				</td>
				<td valign="top"
					style="white-space:nowrap;line-height:18px;font-family:Arial;font-size:14px;border:none">
					&#xa0;
					<xsl:apply-templates />
				</td>
			</tr>
		</table>
	</xsl:template>



	<!--<xsl:template match="//p[@rend]/lb[1][@n]"> <br/>[<xsl:value-of select="@n"/>] 
		</xsl:template> -->

	<!-- Page breaks -->

	<xsl:template match="//tei:pb[@ed='Pauphilet1923']">
		(p.
		<xsl:value-of select="@n" />
		)
	</xsl:template>

	<xsl:template
		match="//tei:milestone[@unit='column'][ancestor::tei:p]">
		<br />
		&#xa0;
		<br />
		<span style="color:darkviolet;font-family:arial;font-size:80%">
			&lt;
			<xsl:value-of select="substring-after(@xml:id,'col_')" />
			&gt;
		</span>
		<br />
	</xsl:template>

	<xsl:template
		match="//tei:milestone[@unit='column'][ancestor::tei:p]" mode="number">
		<br />
		&#xa0;
		<br />
		<span style="color:darkviolet;font-family:arial;font-size:80%">&#xa0;</span>
		<br />
	</xsl:template>

	<xsl:template
		match="//tei:milestone[@unit='column'][not(ancestor::tei:p)]">
		<table width="100%">
			<tr>
				<td width="10%" align="right" valign="top"
					style="padding-right:10pt">
					<span style="color:darkviolet;font-family:arial;font-size:80%">&#xa0;</span>
				</td>
				<td valign="top" style="white-space:nowrap">
					<span style="color:darkviolet;font-family:arial;font-size:80%">
						&lt;
						<xsl:value-of
							select="substring-after(@xml:id,'col_')" />
						&gt;
					</span>
				</td>
			</tr>
		</table>
	</xsl:template>


	<xsl:template match="tei:lb">
		<xsl:choose>
			<xsl:when test="@ed='facs'"></xsl:when>
			<xsl:when
				test="position() = 1 and ancestor::tei:s[not(preceding-sibling::*)]"></xsl:when>
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:lb" mode="number">
		<xsl:choose>
			<xsl:when test="@ed='facs'"></xsl:when>
			<xsl:when
				test="position() = 1 and ancestor::tei:s[not(preceding-sibling::*)]"></xsl:when>
			<xsl:otherwise>
				<br />
				&#xa0;
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if
			test="substring(@n, string-length(@n)) = 0 or substring(@n, string-length(@n)) = 5">
			<xsl:value-of select="@n" />
		</xsl:if>
	</xsl:template>


	<!-- Processing elements with "rend" attributes -->

	<xsl:template match="//*[@rend='ital']">
		<i>
			<xsl:choose>
				<xsl:when test="self::tei:hi">
					<span style="color:blue">
						<xsl:apply-templates />
					</span>
				</xsl:when>
				<xsl:when test="self::tei:foreign">
					<span style="color:darkblue">
						<xsl:apply-templates />
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span style="color:red">
						<xsl:apply-templates />
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</i>
	</xsl:template>

	<xsl:template match="//*[@rend='gras']">
		<b>
			<xsl:choose>
				<xsl:when test="self::tei:hi">
					<span style="color:blue">
						<xsl:apply-templates />
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span style="color:red">
						<xsl:apply-templates />
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</b>
	</xsl:template>

	<xsl:template match="//*[@rend='exp']">
		<sup>
			<xsl:choose>
				<xsl:when test="self::tei:hi">
					<span style="color:blue">
						<xsl:apply-templates />
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span style="color:red">
						<xsl:apply-templates />
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</sup>
	</xsl:template>

	<xsl:template match="//*[@rend='ind']">
		<sub>
			<xsl:choose>
				<xsl:when test="self::tei:hi">
					<span style="color:blue">
						<xsl:apply-templates />
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span style="color:red">
						<xsl:apply-templates />
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</sub>
	</xsl:template>

	<xsl:template match="//*[@rend='maj']">
		<xsl:choose>
			<xsl:when test="self::tei:hi">
				<span style="text-transform:uppercase;color:blue">
					<xsl:apply-templates />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="text-transform:uppercase;color:red">
					<xsl:apply-templates />
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//*[@rend='pmaj']">
		<xsl:choose>
			<xsl:when test="self::tei:hi">
				<span style="font-variant:small-caps;color:blue">
					<xsl:apply-templates />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="font-variant:small-caps;color:red">
					<xsl:apply-templates />
				</span>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="//*[@rend='crochets']">
		<xsl:choose>
			<xsl:when test="self::tei:corr">
				<span style="color:brown">
					[
					<xsl:apply-templates />
					]
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red">
					[
					<xsl:apply-templates />
					]
				</span>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="//*[@rend='chevrons']">
		<xsl:choose>
			<xsl:when test="self::tei:corr">
				<span style="color:brown">
					<xsl:text>&lt;</xsl:text>
					<xsl:apply-templates />
					<xsl:text>&gt; </xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red">
					<xsl:text>&lt;</xsl:text>
					<xsl:apply-templates />
					<xsl:text>&gt; </xsl:text>
				</span>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template match="//*[@rend='crochets-ital']">
		<xsl:choose>
			<xsl:when test="self::tei:head">
				<span style="color:indigo">
					<xsl:text>[</xsl:text>
					<i>
						<xsl:if test="parent::div[@n]">
							<xsl:value-of select="parent::div/@n" />
							<xsl:text>. </xsl:text>
						</xsl:if>
						<xsl:apply-templates />
					</i>
					<xsl:text>]</xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red">
					<xsl:text> [</xsl:text>
					<i>
						<xsl:if test="parent::div[@n]">
							<xsl:value-of select="parent::div/@n" />
							<xsl:text>. </xsl:text>
						</xsl:if>
						<xsl:apply-templates />
					</i>
					<xsl:text>] </xsl:text>
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//*[@rend='crochets-maj']">
		<xsl:choose>
			<xsl:when test="self::tei:head">
				<span
					style="font-family:Times;text-transform:uppercase;color:indigo">
					<xsl:text>[</xsl:text>
					<xsl:if test="parent::div[@n]">
						<xsl:value-of select="parent::div/@n" />
						<xsl:text>. </xsl:text>
					</xsl:if>
					<xsl:apply-templates />
					<xsl:text>]</xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="text-transform:uppercase;color:red">
					[
					<xsl:apply-templates />
					]
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//*[@rend='crochets-pmaj']">
		<xsl:choose>
			<xsl:when test="self::tei:head">
				<span
					style="font-family:Times;font-variant:small-caps;color:indigo">
					<xsl:text>[</xsl:text>
					<xsl:if test="parent::tei:div[@n]">
						<xsl:value-of select="parent::tei:div/@n" />
						<xsl:text>. </xsl:text>
					</xsl:if>
					<xsl:apply-templates />
					<xsl:text>]</xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="font-variant:small-caps;color:red">
					[
					<xsl:apply-templates />
					]
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//*[@rend='points']">
		<xsl:choose>
			<xsl:when test="self::tei:gap">
				<span style="color:brown">
					<xsl:text> . . . . . . . . .  </xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red">
					<xsl:text> . . . . . . . . .  </xsl:text>
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//*[@rend='susp']">
		<xsl:choose>
			<xsl:when test="self::tei:gap">
				<span style="color:brown">
					<xsl:text> ... </xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red">
					<xsl:text> ... </xsl:text>
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//*[@rend='crochets-susp']">
		<xsl:choose>
			<xsl:when test="self::tei:gap">
				<span style="color:brown">
					<xsl:text> [...] </xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:red">
					<xsl:text> [...] </xsl:text>
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- End of @rend processing -->

	<!-- Gaps without normalized @rend -->
	<xsl:template match="tei:gap[not(@rend)]">
		<span style="color:darkblue">
			<xsl:text> [...] </xsl:text>
		</span>
	</xsl:template>

	<xsl:template
		match="tei:gap[@rend][not(@rend='susp' or @rend='points' or @rend='crochets-susp')]">
		<span style="color:orange">
			<xsl:text> [...] </xsl:text>
		</span>
	</xsl:template>

	<!-- Corrections without normalized @rend -->

	<xsl:template match="tei:choice">
		<span title="{child::tei:corr/@type}">
			<xsl:apply-templates select="tei:corr" />
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="tei:sic" />
		</span>
		<xsl:if test="not(ancestor::tei:w)">
			<xsl:call-template name="spacing" />
		</xsl:if>
	</xsl:template>

	<xsl:template match="tei:corr[not(@rend)]">
		<span style="color:darkblue;background-color:yellow">
			<xsl:text> </xsl:text>
			<xsl:apply-templates />
			<xsl:text> </xsl:text>
		</span>
	</xsl:template>

	<!-- <xsl:template match="tei:corr[@rend][not(@rend='crochets' or @rend='chevrons')]"> 
		<span style="color:orange"> <i><xsl:value-of select="@rend"/></i><xsl:text> 
		</xsl:text></span><xsl:apply-templates/> </xsl:template> -->
	<xsl:template match="tei:sic">
		(
		<i>sic&#xa0;:</i>
		<xsl:text> </xsl:text>
		<span style="background-color:pink">
			<xsl:apply-templates />
		</span>
		)
	</xsl:template>
	<!-- Supplied text -->

	<xsl:template match="tei:supplied">
		<span style="color:blue">
			[
			<xsl:apply-templates />
			]
		</span>
		<xsl:if test="not(ancestor::tei:w)">
			<xsl:call-template name="spacing" />
		</xsl:if>
	</xsl:template>

	<!-- Deletions -->

	<xsl:template match="tei:del">
		<span style="background-color:lightpink">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<xsl:template match="tei:add">
		<span style="background-color:lightgreen">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<!-- Redundant text -->

	<xsl:template match="tei:surplus">
		(
		<span style="background-color:lightpink">
			<xsl:apply-templates />
		</span>
		<xsl:text> </xsl:text>
		<i>répété</i>
		)
		<xsl:call-template name="spacing"></xsl:call-template>
	</xsl:template>


	<!-- Numbers -->
	<xsl:template match="tei:num">
		<span style="color:blue">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<!-- Notes -->
	<xsl:template match="tei:note[not(@place='inline')]">
		<xsl:variable name="note_count">
			<xsl:value-of
				select="count(preceding::tei:note[not(@place='inline')]) + 1" />
		</xsl:variable>
		<!--<a title="{.}" style="color:violet;font-size:75%" href="#note_{$note_count}" 
			name="noteref_{$note_count}">[Note <xsl:value-of select="$note_count"/>]</a> -->
		<span title="{.}" style="color:violet">
			[
			<xsl:value-of select="$note_count" />
			]
		</span>
		<xsl:call-template name="spacing" />
	</xsl:template>

	<xsl:template match="tei:note[not(@place='inline')]"
		mode="notes">
		<xsl:variable name="note_count">
			<xsl:value-of
				select="count(preceding::tei:note[not(@place='inline')]) + 1" />
		</xsl:variable>
		<p>
			<xsl:value-of select="$note_count" />
			.
			<a href="#noteref_{$note_count}" name="note_{$note_count}">
				[
				<xsl:value-of
					select="preceding::tei:milestone[@unit='column'][1]/@xml:id" />
				, l.
				<xsl:value-of select="preceding::tei:lb[1]/@n" />
				]
			</a>
			<xsl:text> </xsl:text>
			<xsl:value-of select="." />
		</p>
	</xsl:template>

	<xsl:template match="tei:note[@place='inline']">
		<p
			style="text-align:justify;text-indent:30px;padding-right:20px;padding-left:20px">
			<span
				style="color:blue;font-family:Times New Roman;font-style:italic">
				<xsl:value-of select="." />
			</span>
		</p>
	</xsl:template>

	<!-- Word formatting for morphologically tagged texts -->
	<xsl:strip-space elements="tei:s" />
	<xsl:template
		match="//text()[starts-with(., ' ')][preceding-sibling::*[1][self::tei:w]]">
		<xsl:copy-of select="substring(., 2)" />
	</xsl:template>

	<xsl:template match="//tei:w"> <!-- modified 2011-06-03 -->
		<xsl:element name="span">
			<xsl:attribute name="class">word</xsl:attribute>
			<xsl:attribute name="title"><xsl:value-of
				select="@type" /></xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of
				select="@xml:id" /></xsl:attribute>
			<xsl:if test="@rend='fgmt'">
				<xsl:attribute name="style">color:red</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="descendant::me:norm">
					<xsl:apply-templates
						select="descendant::me:norm" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
				</xsl:otherwise>
			</xsl:choose>

		</xsl:element>
		<xsl:call-template name="spacing" />
		<!--<xsl:choose> <xsl:when test="following::tei:w[1][starts-with(@type, 
			'PON')][contains(., ',') or contains(., '.') or contains(., ']')or contains(., 
			')')]"> </xsl:when> <xsl:when test="following::node()[1][self::text()][starts-with(normalize-space(.), 
			',') or starts-with(normalize-space(.), '.') or starts-with(normalize-space(.), 
			']') or starts-with(normalize-space(.), ')')]"> </xsl:when> <xsl:when test="self::tei:w[starts-with(@type, 
			'PON')][contains(., '[') or contains(., '(')]"> </xsl:when> <xsl:when test="substring(., 
			string-length(.))=$apostrophe"> </xsl:when> <xsl:when test="following::tei:w[1][starts-with(@type, 
			'PON')][contains(., ':') or contains(., ';') or contains(., '!')or contains(., 
			'?')]"> <xsl:text>&#xa0;</xsl:text> </xsl:when> <xsl:when test="@rend='elision'">&apos;</xsl:when> 
			<xsl:otherwise> <xsl:text> </xsl:text> </xsl:otherwise> </xsl:choose> -->
	</xsl:template>

	<xsl:template
		match="//tei:w[not(starts-with(@type,'PON'))]/text()">
		<xsl:choose>
			<xsl:when test="substring(., string-length(.))=$apostrophe">
				<xsl:value-of select="translate(.,$apostrophe,'’')" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="." />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="spacing">
		<xsl:choose>
			<xsl:when
				test="following::tei:w[1][starts-with(@type, 'PON')][starts-with(normalize-space(descendant::text()[1]), ',') or starts-with(normalize-space(descendant::text()[1]), '.') or starts-with(normalize-space(descendant::text()[1]), ']')or contains(., ')')]">
			</xsl:when>
			<xsl:when
				test="following::node()[1][self::text()][starts-with(normalize-space(.), ',') or starts-with(normalize-space(.), '.') or starts-with(normalize-space(.), ']') or starts-with(normalize-space(.), ')')]">
			</xsl:when>
			<xsl:when
				test="self::tei:w[starts-with(@type, 'PON')][starts-with(normalize-space(descendant::text()[1]), '[') or starts-with(normalize-space(descendant::text()[1]), '(')]">
			</xsl:when>
			<xsl:when
				test="substring(., string-length(.))=$apostrophe and not(@type='PONfbl' or @type='PONpdr' or self::tei:note)">
			</xsl:when>
			<xsl:when
				test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')] or ancestor::tei:surplus)"></xsl:when>
			<xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when>
			<xsl:when
				test="following::tei:w[1][starts-with(@type, 'PON')][contains(., ':') or contains(., ';') or contains(., '!') or contains(., '?')]">
				<xsl:text>&#xa0;</xsl:text>
			</xsl:when>
			<!-- <xsl:when test="following::tei:w[1][starts-with(@type, 'PON')][contains(descendant::me:norm, 
				':') or contains(descendant::me:norm, ';') or contains(descendant::me:norm, 
				'!')or contains(descendant::me:norm, '?')]"> <xsl:text>&#xa0;</xsl:text> 
				</xsl:when> -->
			<xsl:when test="@rend='elision'">
				’
			</xsl:when>
			<!-- new 201106-03 -->
			<xsl:when test="@rend='fgmt'"></xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

	<xsl:template
		match="tei:q[ancestor::tei:q[ancestor::tei:q]]">
		<span style="background-color:darkcyan">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<xsl:template
		match="tei:q[ancestor::tei:q][not(ancestor::tei:q[ancestor::tei:q])]">
		<span style="background-color:cyan">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<xsl:template match="tei:q[not(ancestor::tei:q)]">
		<span style="background-color:lightcyan">
			<xsl:apply-templates />
		</span>
	</xsl:template>

	<xsl:template match="bfm:sb">
		<span style="color:violet">_</span>
	</xsl:template>

</xsl:stylesheet>