// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2014-09-15 10:51:15 +0200 (lun. 15 sept. 2014) $
// $LastChangedRevision: 2836 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.graal

import javax.xml.stream.*;
import java.net.URL;
import java.lang.Boolean
import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource
import java.util.Map

// TODO: Auto-generated Javadoc
/** The trans data. @author ayepdieu previous version of the script ApplyXsl */
public class GraalApplyXsl{
	private def transData;
	
	/** The factory. */
	private def factory;
	
	/** The source. */
	private def source;
	
	/** The transformer. */
	private def transformer;
	
	/**
	 * Instantiates a new graal apply xsl.
	 *
	 * @param urlXsl the url xsl
	 */
	public GraalApplyXsl(URL urlXsl){
		try 
		{
			FileInputStream inputStream = new FileInputStream(new File(urlXsl.getFile()));
			factory = new net.sf.saxon.TransformerFactoryImpl();
			source = new StreamSource(inputStream)
			source.setSystemId(urlXsl.getPath());
			transformer = factory.newTransformer(source)
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * Sets the params.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public void setParams(String name, int value) {
	    transformer.setParameter(name, value);
	}
	
	/**
	 * Sets the params.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public void setParams(String name, String value) {
	    transformer.setParameter(name, value);
	}
	
	/**
	 * Apply xsl.
	 *
	 * @param urlXml the url xml
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean ApplyXsl(URL urlXml, String dirPathName, String fileName) {
		FileInputStream inputData = new FileInputStream(new File(urlXml.getFile()));
		def output = new File(dirPathName, fileName)

		transformer.transform( new StreamSource(inputData), new StreamResult(output) )
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) 
	{
		String sortie = "Page-test"
		for (int index = 0; index < 1; index++) {
			GraalApplyXsl DecTxt = new GraalApplyXsl(new URL("file:///home/ayepdieu/srcQuete/Sources/breakByMilestone.xsl"));
			DecTxt.setParams("pbval1", index + 1);
			DecTxt.setParams("pbval2", index + 2);
			DecTxt.ApplyXsl(new URL("file:///home/ayepdieu/srcQuete/Sources/qgraal_cm_2009-07-d.xml"), "/home/ayepdieu/srcQuete/Decoupe", sortie + "_" + (index+ 1) + ".xml");
		}
	}
}
