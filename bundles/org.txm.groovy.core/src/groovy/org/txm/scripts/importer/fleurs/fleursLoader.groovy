/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.fleurs
;

import java.io.File;
import org.txm.scripts.importer.fleurs.compiler;
import org.txm.scripts.importer.fleurs.pager;
import org.txm.scripts.importer.fleurs.importer;
import org.txm.objects.*;
import org.txm.utils.*
import org.txm.utils.io.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
String userDir = System.getProperty("user.home");
String rootDir;
String lang;
String encoding;
String model;
try{rootDir = rootDirBinding;lang=langBinding;encoding=encodingBinding;model=modelBinding;}
catch(Exception)
{	println "DEV MODE";//exception means we debug
	if(!org.txm.Toolbox.isInitialized())
	{
		rootDir = userDir+"/xml/fleurs/";
		lang="fr";
		encoding= "UTF-8";// not used
		model="rgaqcj"; // not used
		Toolbox.setParam(Toolbox.INSTALL_DIR,new File(userDir,"TXMinstall"));
		Toolbox.setParam(Toolbox.METADATA_ENCODING, "UTF-8");
		Toolbox.setParam(Toolbox.METADATA_COLSEPARATOR, ",");
		Toolbox.setParam(Toolbox.METADATA_TXTSEPARATOR, "\"");
		Toolbox.setParam(Toolbox.USER_TXM_HOME, new File(System.getProperty("user.home"),"TXM"));
	}
}

String basename = "fleurs";
System.out.println("basename : "+basename);
println "-- IMPORTER - Reading source files"
File homedir = new File(rootDir);
def imp = new importer();
imp.run(homedir, basename);

File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+basename);
rootDir = binDir.getAbsolutePath()+"/";

println "-- ANNOTATE - Running NLP tools"
println "No annotation to do"
//new Annotate().run(new File(rootDir),model+".par");

println "-- COMPILING - Building Search Engine indexes"
List<File> files = new File(rootDir,"txm").listFiles();
def c = new compiler()
//c.setCwbPath("")// for developers
c.setLang(lang);
c.run(rootDir);

//move registry file to cwb registry dir
File registryfile = new File(rootDir,"registry/"+basename);
if(registryfile.exists())
	FileCopy.copy(registryfile,new File(Toolbox.getTxmHomePath(),"registry/"+basename.toLowerCase()))

Workspace w = org.txm.Toolbox.workspace;

Project p = w.getProject("default")
p.removeBase(basename)
Base b = p.addBase(basename);
b.addDirectory(new File(rootDir,"txm"));
b.setAttribute("lang", lang)
b.propagateAttribute("lang")

println "-- EDITION - Building edition"
new File(rootDir,"HTML").deleteDir();
new File(rootDir,"HTML").mkdir();
new File(rootDir,"HTML/default").mkdir();
List<File> filelist = new File(rootDir,"txm").listFiles();
def second = 0

println "Paginating text: "
for(Text text : b.getTexts())
{
	File srcfile = text.getSource();
	File resultfile = new File(rootDir,"HTML/"+srcfile.getName().substring(0,srcfile.getName().length()-4)+".html");
	List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);
		
		if (second) { print(", ") }
		if (second > 0 && (second % 5) == 0) println ""
		print(srcfile.getName());
		second++
	
	def ed = new pager(srcfile,resultfile, NoSpaceBefore, NoSpaceAfter, p.getEditionDefinition("default").wordsPerPage,basename);
	
	Edition editionweb = text.addEdition("default","html",resultfile);
	for(int i = 0 ; i < ed.getPageFiles().size();i++)
	{
		File f = ed.getPageFiles().get(i);
		String idx = ed.getIdx().get(i);
		editionweb.addPage(f,idx);
	}
	
//	Edition editionbp = text.addEdition("onepage","html",resultfile);
//	editionbp.addPage(resultfile,ed.getIdx().get(0));
}

w.save();
println ""
