package org.txm.scripts.importer.xtz

import java.io.File;

import org.txm.core.preferences.TBXPreferences
import org.txm.importer.xmltxm.Annotate
import org.txm.importer.xtz.*
import org.txm.Toolbox
import org.txm.core.engines.*
import org.txm.importer.ApplyXsl2

/**
 * Wraps current Annotate class into the import workflow
 * 
 * @author mdecorde
 *
 */
class TTAnnotater extends Annotater {

	public TTAnnotater(ImportModule module) {
		super(module);
	}

	@Override
	public void process() {

		//String model = module.getParameters().get(ImportKeys.TTMODEL);
		boolean cleanDirectories = module.getProject().getCleanAfterBuild();
		
		boolean expertMode = TBXPreferences.getInstance().getBoolean(TBXPreferences.EXPERT_USER);
		
		String corpusname = module.getProject().getName();
		String lang = module.getProject().getLang();

		String engineName = module.getProject().getImportParameters().node("annotate").get("engine", "TreeTagger")
		def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
		if (module.isMultiThread()) {
			def hash = [:]
			for (File txmFile : inputDirectory.listFiles()) {
				hash[txmFile.getName()] = lang;
			}
			if (engine.processDirectory(inputDirectory, module.getBinaryDirectory(), ["langs":hash])) {
				isSuccessFul = true;
			}
		} else {
			if (engine.processDirectory(inputDirectory, module.getBinaryDirectory(), ["lang":lang])) {
				isSuccessFul = true;
			}
		}
		
		if (isSuccessFul) doPostNLPXSLStep(); // HOOK
		
		if (cleanDirectories) {
			new File(module.getBinaryDirectory(), "treetagger").deleteDir()
			new File(module.getBinaryDirectory(), "ptreetagger").deleteDir()
			new File(module.getBinaryDirectory(), "annotations").deleteDir()
		}
		
		if (!isSuccessFul) reason = "TreeTagger annotation failed."
	}
	
	/**
	 * read from $inputDirectory and write the result in $bindir/txm
	 *
	 */
	public boolean doPostNLPXSLStep() {
		
		//filesToProcess = inputDirectory.listFiles();
		
		File postnlpXSLdirectory = new File(module.getSourceDirectory(), "xsl/4-postnlp")
		
		def xslFiles = postnlpXSLdirectory.listFiles(new FileFilter() {
			public boolean accept(File pathname) { return pathname.getName().endsWith(".xsl");}
		});
		def xslParams = project.getXsltParameters()
		xslParams["output-directory"] = outputDirectory.getAbsoluteFile().toURI().toString();
		
		if (postnlpXSLdirectory.exists() && xslFiles != null && xslFiles.size() > 0) {
			
			def files = inputDirectory.listFiles()
			
			println "-- PostNLP XSL Step with the $postnlpXSLdirectory directory."
			xslFiles.sort()
			for (File xslFile : xslFiles) {
				if (xslFile.isDirectory() || xslFile.isHidden() || !xslFile.getName().endsWith(".xsl")) continue;
				//if (!xslFile.getName().matches("[1-9]-.+")) continue;
				
				if (ApplyXsl2.processImportSources(xslFile, files, xslParams)) {
					inputDirectory = outputDirectory; // the files to process are now in the "txm" directory
					println ""
				} else {
					reason = "Fail to apply post NLP XSL: $xslFile"
					return false;
				}
			}
		} else {
			//println "Nothing to do."
		}
		return true;
	}

	@Override
	public void cancel() {
		//a.setCancelNow();
	}
}
