// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-05-11 08:38:47 +0200 (mer. 11 mai 2016) $
// $LastChangedRevision: 3211 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.bfm;

import org.txm.importer.*;
import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.FileUtils
import org.txm.utils.io.FileCopy;
import org.txm.utils.treetagger.TreeTagger;

import javax.xml.stream.*;

import java.io.File;
import java.net.URL;
import java.util.Properties;

import org.txm.*;
import org.txm.core.engines.*;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.ReunitBrokenWords.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.TagSentences.*;
import org.txm.scripts.filters.FusionHeader.*;

// TODO: Auto-generated Javadoc
/**
 * Convert TEI-BFM to TEI-TXM: <br/>
 * 1- tokenize : manage BFM's &lt;choice> policy (see <a
 * href="http://bfm.ens-lsh.fr">BFM website</a>).<br/>
 * 2- launch XML2TEITXM.groovy with no annotation to import. <br/>
 *
 * @param dir contains the sources files
 * @param basename the basename
 * @return true, if successful
 * @author mdecorde
 */
class importer {
	
	public boolean run(File srcDir, File binDir, File txmDir, String basename, Properties metadataXPath, def project)
	{
		new File(binDir, "ptokenized").deleteDir();
		new File(binDir, "ptokenized").mkdir();
		new File(binDir, "tokenized").deleteDir();
		new File(binDir, "tokenized").mkdir();
		new File(binDir, "headers").deleteDir();
		new File(binDir, "headers").mkdir();

		ArrayList<String> milestones = new ArrayList<String>();
		milestones.add("lb");
		milestones.add("pb");
		milestones.add("milestone");

		List<File> files = FileUtils.listFiles(txmDir);
		if (files == null || files.size() == 0) return false;

		for (int i = 0 ; i < files.size() ; i++)
			if (!(files.get(i).getName().endsWith(".xml"))
			|| files.get(i).getName().equals("import.xml"))
				files.remove(i--);

		for (File f : files) {
			//System.out.println("Get metadata from file $xpathfile");
			HashMap<String, String> metadataValues = new HashMap<String, String>();
			if (metadataXPath != null) {
				def xpathprocessor = new XPathResult(f)
				for (String name : metadataXPath.keySet()) {
					String value = xpathprocessor.getXpathResponse(metadataXPath.get(name), "N/A");
					value = value.trim().replace("\n", " ");
					//println "xpath: "+metadataXPath.get(name)+" >> "+value.trim()
					metadataValues.put(name, value);
				}
				xpathprocessor.close();
			}
			
			//forme is mandatory !!!
			String forme = "prose";
			if (metadataValues.containsKey("forme")) {
				String tmp = metadataValues.get("forme");
				if (tmp.startsWith("#forme_"))
					tmp = tmp.substring(7)
				metadataValues.put("forme", tmp)
				forme = metadataValues.get("forme");
			}

			if (metadataValues.containsKey("morphosynt")) {
				//println "MORPHOSYNT: "+metadataValues.get("morphosynt");
				if (metadataValues.get("morphosynt").equals("N/A"))
					metadataValues.put("morphosynt","non vérifié");
				else
					metadataValues.put("morphosynt","vérifié");
			}

			String txtname = f.getName().substring(0, f.getName().length()-4);
			String titreId = txtname;
			if (metadataValues.containsKey("sigle")) {
				titreId = metadataValues.get("sigle");
			} else if (metadataValues.containsKey("idbfm")) {
				titreId = metadataValues.get("idbfm");
			}
			metadataValues.put("sigle", titreId);
			
			// inject attributes in $f
			AddAttributeInXml builder = new AddAttributeInXml(f, "text", metadataValues);
			builder.onlyOneElement();
			//println "INJECTING $metadataValues"
			File outfile = new File(txmDir, "copy"+f.getName())
			
			if (!builder.process(outfile)) {
				println "Error while injecting metadata in $f"
				return false;
			}
			builder= null;
			if (f.delete()) {
				outfile.renameTo(f)
			} else {
				println "ERROR could not delete $f for metadatainjection"
				return false
			}
		}

		files = FileUtils.listFiles(txmDir);
		if (files == null || files.size() == 0) return false;
		
		//return false; // TEMP TEMP TEMP
		
		//PREPARE EACH FILE TO BE TOKENIZED
		println("preparing "+files.size()+" files for the tokenizer");

		for (File f : files) {
			if (f.isDirectory()) continue;
			print "."//+f.getAbsolutePath()
			File srcfile = f;
			File resultfile = new File(binDir,"ptokenized/"+f.getName());

			def builder2 = new OneTagPerLine(srcfile.toURI().toURL(), milestones);
			if (!builder2.process(resultfile)) {
				println "Failed to tokenize file: "+f;
				resultfile.delete();
			}
		}
		println ""

		//TOKENIZE FILES
		files = FileUtils.listFiles(new File(binDir, "ptokenized"));//scan directory split
		println("Tokenizing "+files.size()+" files")
		for (File infile : files) {
			print "."
			try {
				Sequence S = new Sequence();
				Filter F1 = new CutHeader();
				Filter F2 = new ReunitBrokenWords();
				Filter F6 = new Tokeniser(infile);
				Filter F7 = new TagSentences();
				Filter F11 = new FusionHeader();
				S.add(F1);
				//S.add(F2);
				S.add(F6);
				//S.add(F7);
				S.add(F11);

				File xmlfile = new File(binDir,"tokenized/"+infile.getName());

				S.SetInFileAndOutFile(infile.getAbsolutePath(), xmlfile.getAbsolutePath());
				S.setEncodages("UTF-8","UTF-8");

				Object[] arguments1 = [new File(binDir,"/headers/"+infile.getName()+"header.xml").getAbsolutePath()];
				F1.SetUsedParam(arguments1);
				Object[] arguments2 = [new File(binDir,"/headers/"+infile.getName()+"header.xml").getAbsolutePath(),F1];
				F11.SetUsedParam(arguments2);

				S.proceed();
				S.clean();
			} catch(Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				return false;
			}
		}
		println ""


		files = FileUtils.listFiles(new File(binDir,"tokenized"))
		println "Validating XML of "+files.size()+" files"
		for (File f : files) {
			print "."
			if (!ValidateXml.test(f)) {
				System.out.println("XML NOT VALID "+f);
				new File(binDir, "tokenizerError").mkdir();
				FileCopy.copy(f,new File(binDir, "tokenizerError/"+f.getName()));
				f.delete();
			}
		}
		println("");

		//TRANSFORM INTO XML-TEI-TXM
		files = FileUtils.listFiles(new File(binDir, "tokenized"))
		println("Building xml-tei-txm "+files.size()+ " files")
		if (files.size() == 0) return false;
		for (File file : files) {
			print "."
			String txmfile = file.getName();

			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()

			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>();
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
			//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();

			//lance le traitement
			def builder3 = new Xml2Ana(file);
			builder3.setCorrespondances(correspRef, correspType);
			builder3.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			if (!builder3.process(new File(txmDir,txmfile))) {
				println "Failed to build xml-txm file from file: "+file;
				return false;
			}
		}
		println("")
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "tokenized").deleteDir()
			new File(binDir, "stokenized").deleteDir()
			new File(binDir, "headers").deleteDir()
		}
		return true;
	}

	/**
	 * for test purpose.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		File dir = new File("/home/mdecorde/xml/qgraal")
		File srcfile = new File(dir, "qgraal_cm-detoken.xml");
		File resultfile = new File(dir,"QGRAALFRO-pt1.xml");
		File xmlfile = new File(dir,"QGRAALFRO-t1.xml");

		def builder2 = new OneTagPerLine(srcfile.toURI().toURL(), []);
		if (!builder2.process(resultfile)) {
			println "Failed to tokenize file: "+srcfile;
			resultfile.delete();
		}

		try {
			Sequence S = new Sequence();
			Filter F1 = new CutHeader();
			Filter F2 = new ReunitBrokenWords();
			Filter F6 = new Tokeniser(resultfile);
			Filter F7 = new TagSentences();
			Filter F11 = new FusionHeader();
			S.add(F1);
			//S.add(F2);
			S.add(F6);
			//S.add(F7);
			//S.add(F11);

			S.SetInFileAndOutFile(resultfile.getAbsolutePath(), xmlfile.getAbsolutePath());
			S.setEncodages("UTF-8","UTF-8");

			Object[] arguments1 = [new File(dir,"header.xml").getAbsolutePath()];
			F1.SetUsedParam(arguments1);
			Object[] arguments2 = [new File(dir,"header.xml").getAbsolutePath(),F1];
			F11.SetUsedParam(arguments2);

			S.proceed();
			S.clean();
		} catch(Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		println "time: "+(System.currentTimeMillis()-start)/1000
	}
}