// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.RGAQCJ

import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll

import javax.xml.stream.*;
import java.net.URL;

import org.txm.importer.scripts.filters.*;
// TODO: Auto-generated Javadoc

/**
 * The Class BuildXmlRGAQCJ.
 *
 * @author mdecorde
 * simple import in cwb of RGAQCJ
 * structunits : initial, s, lb ...
 * wordproperties : form, pos and n
 */

public class BuildXmlRGAQCJ
{
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private def parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	/** The url. */
	private def url;
	
	/** The anahash. */
	private HashMap<String,String> anahash =new HashMap<String,String>() ;
	
	/** The types. */
	private ArrayList<String> types;
	
	/** The current type. */
	private String currentType;
	
	/** The cat ref. */
	private String catRef="";
	
	/** The initial. */
	private String initial;
	
	/**
	 * initialize.
	 *
	 * @param url the url
	 * @param initial the initial
	 * @param types the types
	 */
	public BuildXmlRGAQCJ(URL url,String initial,ArrayList<String> types){
		try {
			this.url = url;
			this.types = types;
			this.initial = initial;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * clear anaHash variable, it is used to store ana tags values then print it when the end element </ana> is found.
	 */
	private void fillanaHash()
	{
		anahash.clear();
		for(String s : types)
			anahash.put( s,"-" );
	}
	
	/**
	 * Creates the output.
	 *
	 * @param dirPathName output directory
	 * @param fileName output file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			dir = new File(dirPathName)
			File f = new File(dir, fileName);
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8");

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/**
	 * Checks if is body.
	 *
	 * @param name the name
	 * @return true, if is body
	 */
	private static boolean isBody(String name) {
		if (name.equals("body")) return true;
	}
	
	/**
	 * Find body.
	 *
	 * @return true, if successful
	 */
	private boolean findBody(){
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()){
			if(event == XMLStreamConstants.START_ELEMENT){
				if (parser.getLocalName() == "catRef"){
					if(parser.getAttributeValue(null,"target") == "#forme_prose")
						catRef = "prose";
					else if(parser.getAttributeValue(null,"target") == "#forme_vers")
						catRef = "vers";
					else
						catRef = "mixte";
					//println("catRef : "+catRef);
				}
				if (isBody(parser.getLocalName())){
					return true;
				}
			}
		}
		return false;
	} 
	
	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(String dirPathName, String fileName)
	{
		if(findBody() && createOutput(dirPathName, fileName))
		{
			def idPb = "";
			def idLb = "";
			String idLinesuiv;
			
			boolean flagForm = false;
			boolean flagAna = false;
			String vAna = "";
			String vWord = "";
			String vForm = "";
			
			try 
			{
				File inputfile = new File(this.url.getFile());
				output.write( "<text initiale=\""+initial+"\" forme=\""+catRef+"\">\n");
				for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
				{
					switch (event) 
					{
						case XMLStreamConstants.START_ELEMENT:
						switch (parser.getLocalName()) 
						{
							case "p":
								output.write "<p n=" + parser.getAttributeValue(null,"n") +">\n";
								break;
							
							case "s":
								output.write "<s n=" + parser.getAttributeValue(null,"n")+ "\">\n";
								break;
							
							case "pb":
								idLinesuiv = parser.getAttributeValue(null,"n");
								idPb = idLinesuiv;
								break;
								
							case "lb":
								idLb = parser.getAttributeValue(null,"n");
								break;
							
							case "w":
								fillanaHash();
								vWord ="";
							break;
							
							case "form":
								flagForm =true;
								vForm ="";
							break;
							case "ana":
								flagAna = true;
								vAna ="";
								
								currentType = (parser.getAttributeValue(null,"ref"));
								if(currentType != null)
									currentType= currentType.substring(1)
								else
									flagAna = false;
							break;
						}
						break;
						
						case XMLStreamConstants.END_ELEMENT:
						switch (parser.getLocalName()) 
						{
							case "p":
								output.write( "</p>\n");
								break;
							
							case "s":
								output.write( "</s>\n");
								break;

							case "w":
								for(String type : types)
									vWord+="\t"+anahash.get(type)
								if(catRef.equals("vers"))
									output.write( vWord+ "\t" +idLb+ "\n");
								else
									output.write( vWord+ "\t" +idPb+ "\n");
								vWord= "";
								break;
							
							case "form":
								flagForm = false;
								vWord +=vForm; 
								break;
								
							case "ana":
								if(flagAna)
									anahash.put (currentType,vAna);
								flagAna = false;
								break;
						}
						break;
						
						case XMLStreamConstants.CHARACTERS:
							if(flagForm){
								vForm += parser.getText().trim();
							}
							if(flagAna){
								vAna += parser.getText().trim();
							}
						break;
					}
				}
				output.write( "</text>\n");
				output.close();
				parser.close();
			}
			catch (XMLStreamException ex) {
				System.out.println(ex);
			}
			catch (IOException ex) {
				System.out.println("IOException while parsing " + inputData);
			}
		}
		return true;
	}
	
	/**
	 * Gets the anatypes.
	 *
	 * @param rootDir the root dir
	 * @param xmltxmfiles the xmltxmfiles
	 * @return the anatypes
	 */
	public static def getAnatypes(String rootDir, List<String> xmltxmfiles)
	{
		ArrayList<String> types = new ArrayList<String>()
		for(int i=0; i < xmltxmfiles.size();i++)
		{
			//println("look in "+xmltxmfiles[i]);
			URL url = new File(rootDir+"/anainline/", xmltxmfiles[i]).toURL();
			def inputData = url.openStream();
			def factory = XMLInputFactory.newInstance();
			XMLStreamReader parser = factory.createXMLStreamReader(inputData);
			
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
				if(event == XMLStreamConstants.START_ELEMENT)
					if(parser.getLocalName().equals("ana"))
					{
						String s = parser.getAttributeValue(null, "ref");
						if(s != null)
						{
							s = s.substring(1, s.length());
							if(!types.contains(s))
							{
								types.add(s);
							}
						}
					}
			
			inputData=null;
			factory=null
			parser=null;
		}
		return types;		
	}
		
	/**
	 * Process.
	 *
	 * @param files the files
	 * @param rootDir the root dir
	 */
	public static void process(List<String> files, String rootDir) 
	{
		new File(rootDir+"cqp/").mkdir();
		new File(rootDir+"registry/").mkdir();
		//String[] files = ["roland-ana.xml","qgraal_cm-ana.xml","artu-ana.xml","qjm-ana.xml","commyn1-ana.xml","jehpar-ana.xml"];
		//String rootDir = "~/xml/rgaqcj/";
		ArrayList<String> types = BuildXmlRGAQCJ.getAnatypes(rootDir, files);
		println(types);
		
		//1- Transform into CQP file
		File f = new File(rootDir+"cqp/","RGAQCJ.cqp");
		f.delete();
		
		def output = new OutputStreamWriter(new FileOutputStream(f) , "UTF-8");
		output.write("<corpus name=\"RGAQCJ\">\n");
		output.close();
		
		
		def initiales = "RAQCJ";
		for(int i=0; i < files.size();i++)
		{
			println("process file "+files[i])
			String file = files[i]; 
			def builder = new BuildXmlRGAQCJ(new File(rootDir+"/anainline/",file).toURL(),""+initiales.charAt(i),types);
			builder.transfomFileCqp(rootDir+"cqp","RGAQCJ.cqp");
		}
		
		output = new OutputStreamWriter(new FileOutputStream(f,true) , "UTF-8");
		output.write("</corpus>\n");
		output.close();

		//2- Import into CWB
		
		def inDir = rootDir+"src/";
		def outDir =rootDir;
		def outDirTxm = rootDir;
		
		CwbEncode cwbEn = new CwbEncode();
		CwbMakeAll cwbMa = new CwbMakeAll();
		
		types.add("line");
		String[] pAttributes = types;
		String[] sAttributes = ["corpus:0+name","text:0+initiale+forme", "s:0+n","p:0+n"];
		
		try
		{
			if (System.getProperty("os.name").contains("Windows"))
			{
				cwbEn.run(outDirTxm + "data/"+"RGAQCJ", outDir +"cqp/"+"RGAQCJ"+".cqp", outDirTxm + "registry/"+"rgaqcj",pAttributes, sAttributes);
				cwbMa.run("RGAQCJ", outDirTxm + "registry");
			}
			else
			{
				cwbEn.run(outDirTxm + "data/"+"RGAQCJ", outDir + "/cqp/"+"RGAQCJ.cqp", outDirTxm + "registry/"+"rgaqcj",pAttributes, sAttributes);
				cwbMa.run("RGAQCJ", outDirTxm + "registry");

			}
		} catch (Exception ex) {System.out.println(ex);}
		System.out.println("Done.") 
		
		return
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		List<String> files = ["roland-ana.xml","qgraal_cm-ana.xml","artu-ana.xml","qjm-ana.xml","commyn1-ana.xml","jehpar-ana.xml"];
		String rootDir = "~/xml/rgaqcj/";
		BuildXmlRGAQCJ.process( files,  rootDir);
	}
}