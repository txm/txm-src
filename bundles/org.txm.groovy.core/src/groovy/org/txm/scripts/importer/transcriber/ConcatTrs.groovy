// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.scripts.importer.transcriber

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;

import javax.xml.stream.*;
import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;

// TODO: Auto-generated Javadoc
/**
 * The Class ConcatTrs.
 */
class ConcatTrs {

	/** The localname. */
	String localname;

	/** The prefix. */
	String prefix;

	/** The input data1. */
	InputStream inputData1,/** The input data2. */
	inputData2;

	/** The parser1. */
	XMLStreamReader parser1,
	/** The parser2. */
	parser2;

	/** The output. */
	BufferedOutputStream output;

	/** The writer. */
	XMLStreamWriter writer;

	/** The inputfactory. */
	XMLInputFactory inputfactory = XMLInputFactory.newInstance();

	/** The factory. */
	XMLOutputFactory factory = XMLOutputFactory.newInstance();

	/** The speakers. */
	HashMap<File, String> speakers = new HashMap<File, String>();

	/**
	 * Instantiates a new concat trs.
	 *
	 * @param trs1 the trs1
	 * @param trs2 the trs2
	 * @param result the result
	 */
	public ConcatTrs(File trs1, File trs2, File result)
	{
		// t1<-open("t1.trs")
		// t2<-open("t2.trs")
		// out<-open("t3.trs", "w")
		inputData1 = trs1.toURI().toURL().openStream();
		parser1 = inputfactory.createXMLStreamReader(inputData1);

		inputData2 = trs2.toURI().toURL().openStream();
		parser2 = inputfactory.createXMLStreamReader(inputData2);

		output = new BufferedOutputStream(new FileOutputStream(result))
		writer = factory.createXMLStreamWriter(output, "UTF-8");

		// check-heads(t1, t2) // same speakers
		if(!testSpeakers())
		{
			println "stop"
			return;
		}

		//get shift time trs1
		inputData1.close();
		inputData1 = trs1.toURI().toURL().openStream();
		parser1 = inputfactory.createXMLStreamReader(inputData1);
		float shift = getLastTime(parser1);

		//get last time trs2
		inputData2.close();
		inputData2 = trs2.toURI().toURL().openStream();
		parser2 = inputfactory.createXMLStreamReader(inputData2);
		float end = getLastTime(parser2);

		end += shift;
		println "shift: $shift ; end: $end"

		// copy-head-body-no-tail(t1, out) // shift<-last("sp", t1).time
		inputData1.close();
		inputData1 = trs1.toURI().toURL().openStream();
		parser1 = inputfactory.createXMLStreamReader(inputData1);
		writeFirstPartAndGetLastTime(parser1, end);

		// copy-body+tail-time-shifted(t2, out, shift)
		inputData2.close();
		inputData2 = trs2.toURI().toURL().openStream();
		parser2 = inputfactory.createXMLStreamReader(inputData2);
		writeSecondPartAndShiftTime(parser2, shift);

		writer.flush();
		writer.close();
		output.close();

		inputData1.close();
		inputData2.close();
	}

	/**
	 * Test speakers.
	 *
	 * @return true, if successful
	 */
	protected boolean testSpeakers()
	{
		String speakers1 = getSpeaker(parser1);
		String speakers2 = getSpeaker(parser2);

		boolean ret = speakers1 == speakers2;

		if(!ret)
		{
			println "Speakers are differents : "
			println "  speakers1= $speakers1"
			println "  speakers2= $speakers2"
		}
		else
			println "speakers OK $speakers1 and $speakers2"
		return ret;
	}

	/**
	 * Gets the speaker.
	 *
	 * @param parser the parser
	 * @return the speaker
	 */
	protected String getSpeaker(XMLStreamReader parser)
	{
		//println "get speakers of $parser"

		ArrayList<String> spksvalues = [];
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event)
			{
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
				//println localname
					if(localname == "Speaker")
					{
						spksvalues << "("+parser.getAttributeValue(null,"id")+"-"+parser.getAttributeValue(null,"name")+")";
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					if(localname == "Speakers")
					{
						Collections.sort(spksvalues);
						return spksvalues.toString();
					}
					break;
			}
		}
	}

	/**
	 * Gets the last time.
	 *
	 * @param parser the parser
	 * @return the last time
	 */
	protected float getLastTime(XMLStreamReader parser)
	{
		float timef = 0.0f

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event)
			{
				case XMLStreamConstants.START_ELEMENT:

					localname = parser.getLocalName();
					prefix = parser.getPrefix();

					if(localname == "Turn")
					{
						String time = parser.getAttributeValue(null, "endTime")
						if(time != null)
							timef = Float.parseFloat(time)
					}
			}
		}
		return timef;
	}

	/**
	 * Write first part and get last time.
	 *
	 * @param parser the parser
	 * @param end the end
	 */
	protected void writeFirstPartAndGetLastTime(XMLStreamReader parser, float end)
	{
		println "First part"

		DecimalFormat f = new DecimalFormat("#.000"); //$NON-NLS-1$
		writer.writeStartDocument("UTF-8", "1.0");
		writer.writeCharacters("\n")
		writer.writeDTD("<!DOCTYPE Trans SYSTEM \"trans-14.dtd\">")
		writer.writeCharacters("\n")
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event)
			{
				case XMLStreamConstants.START_ELEMENT:

					localname = parser.getLocalName();
					prefix = parser.getPrefix();

					if(prefix != null && prefix.length() > 0)
						writer.writeStartElement(prefix+":"+localname);
					else
						writer.writeStartElement(localname);

					for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
					{
						String name = parser.getAttributeLocalName(i)
						String value = parser.getAttributeValue(i);

						if(parser.getAttributePrefix(i)!= "")
							writer.writeAttribute(parser.getAttributePrefix(i)+":"+name, value);
						else
							writer.writeAttribute(name, value);
					}


					break;

				case XMLStreamConstants.END_ELEMENT:

					localname = parser.getLocalName()
				// dont close the transcription here
					if(localname != "Trans" && localname != "Episode")
						writer.writeEndElement();
					break;

				case XMLStreamConstants.CHARACTERS:
					writer.writeCharacters(parser.getText());
					break;
			}
		}
	}

	/**
	 * Write second part and shift time.
	 *
	 * @param parser the parser
	 * @param shift the shift
	 */
	protected void writeSecondPartAndShiftTime(XMLStreamReader parser, float shift)
	{
		println "Second part"
		DecimalFormat f = new DecimalFormat("#.000"); //$NON-NLS-1$

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event)
			{
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					prefix = parser.getPrefix();

				// skip header
					if(localname == "Trans" || localname == "Episode" || localname == "Topics" || localname == "Topic"
					|| localname == "Speakers" || localname == "Speaker")
						break;

					if(prefix != null && prefix.length() > 0)
						writer.writeStartElement(prefix+":"+localname);
					else
						writer.writeStartElement(localname);

					for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
					{
						String name = parser.getAttributeLocalName(i);
						String value = parser.getAttributeValue(i);

						if(name == "time" || name == "endTime" || name == "startTime")
						{
							float newf = Float.parseFloat(value)+shift;
							value = f.format(newf)
							value = value.replace(",",".")
						}

						if(parser.getAttributePrefix(i)!= "")
							writer.writeAttribute(parser.getAttributePrefix(i)+":"+name, value);
						else
							writer.writeAttribute(name, value);
					}


					break;

				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName()
					if(localname == "Topics" || localname == "Topic"
					|| localname == "Speakers" || localname == "Speaker" )
						break;
					writer.writeEndElement();
					break;

				case XMLStreamConstants.CHARACTERS:
					if (parser.getText().trim().length() > 0)
						writer.writeCharacters(parser.getText());
					break;
			}
		}
	}

	/**
	 * Process.
	 */
	protected void process()
	{
		writer.writeStartDocument("UTF-8", "1.0");

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			switch (event)
			{
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					prefix = parser.getPrefix();

					if(prefix != null && prefix.length() > 0)
						writer.writeStartElement(prefix+":"+localname);
					else
						writer.writeStartElement(localname);


					for(int i= 0 ; i < parser.getAttributeCount() ;i++ )
					{
						String prefix = parser.getAttributePrefix(i)
						String attname = parser.getAttributeLocalName(i)
						String attvalue = parser.getAttributeValue(i);

						if(prefix != "")
							writer.writeAttribute(prefix+":"+attname, attvalue);
						else
							writer.writeAttribute(attname, attvalue);
					}


					break;

				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName()
					writer.writeEndElement();
					break;

				case XMLStreamConstants.CHARACTERS:
					writer.writeCharacters(parser.getText());
					break;
			}
		}
	}


	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File homedir = new File(System.getProperty("user.home"),"xml/concattrs/new");
		//		File trs1 = new File(homedir, "int17A.trs")
		//		File trs2 = new File(homedir, "int17B.trs")
		//		File trs3 = new File(homedir, "int17C.trs")
		//		File trs4 = new File(homedir, "int17D.trs")
		//		File tmp1 = new File(homedir, "int17tmp1.trs")
		//		File tmp2 = new File(homedir, "int17tmp2.trs")
		//		File trs =  new File(homedir, "int17.trs")

		//		File trs1 = new File(homedir, "int24.trs")
		//		File trs2 = new File(homedir, "int24-2.trs")
		//		File trs3 = new File(homedir, "int24-3.trs")
		//		File trs4 = new File(homedir, "int24-4.trs")
		//		File trs5 = new File(homedir, "int24-5.trs")
		//		File trs6 = new File(homedir, "int24-6.trs")
		//		File tmp1 = new File(homedir, "int24tmp1.trs")
		//		File tmp2 = new File(homedir, "int24tmp2.trs")
		//		File tmp3 = new File(homedir, "int24tmp3.trs")
		//		File tmp4 = new File(homedir, "int24tmp4.trs")
		//		File trs =  new File(homedir, "int24-corr.trs")

		File trs1 = new File(homedir, "int14.trs")
		File trs2 = new File(homedir, "int14bis.trs")
		File trs =  new File(homedir, "int14-corr.trs")

		new ConcatTrs(trs1, trs2, trs)

		ValidateTRS.checkTRS(trs)
	}
}
