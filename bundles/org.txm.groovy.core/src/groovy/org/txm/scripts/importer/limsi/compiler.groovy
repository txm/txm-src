

// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2012-01-05 14:27:34 +0100 (jeu., 05 janv. 2012) $
// $LastChangedRevision: 2096 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.limsi

import org.txm.Toolbox;
import org.txm.importer.cwb.*
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.io.FileCopy;
import org.txm.utils.treetagger.TreeTagger;
import java.util.ArrayList;
import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class compiler.
 */
class compiler
{
	/** The debug. */
	boolean debug = false

	/** The dir. */
	private def dir

	boolean annotationSuccess = false

	public def pAttributesList = []
	public def sAttributesList = []

	/**
	 * Sets the debug.
	 *
	 * @return the java.lang. object
	 */
	public setDebug()
	{
		debug = true
	}

	/**
	 * Sets the annotation success.
	 *
	 * @param val the new annotation success
	 */
	public void setAnnotationSuccess(boolean val)
	{
		this.annotationSuccess = val
	}

	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(File binDir, File txmDir, String corpusname)
	{
		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		if (!binDir.exists()) {
			println ("binary directory does not exists: "+binDir)
			return false
		}

		//File cqpFile = new File(binDir,"cqp/"+corpusname+".cqp")
		new File(binDir, "cqp").deleteDir()
		new File(binDir, "cqp").mkdir()
		new File(binDir, "data").deleteDir()
		new File(binDir, "data").mkdir()
		new File(binDir, "registry").mkdir()

		//1- Merge CQP files
		if (annotationSuccess) {
			File annotationDir = new File(binDir, "annotations")
			if (annotationDir.exists()) txmDir = annotationDir;
		}
		def files = txmDir.listFiles()
		files.sort(); // sort files by name

		//2- Import into CWB
		def outDir = binDir.getAbsolutePath()+"/";

		CwbEncode cwbEn = new CwbEncode()
		CwbMakeAll cwbMa = new CwbMakeAll()
		cwbEn.setDebug(debug)
		cwbMa.setDebug(debug)

		def pAttrs = []; // cast to array
		if (annotationSuccess) {
			pAttrs << "frpos"
			pAttrs << "frlemma"
		}
		def sAttrs = ["text:0+id", "txmcorpus:0+lang"]; // cast to array


		String[] pAttributes = pAttrs // cwb-encode needs String[]
		String[] sAttributes = sAttrs // cwb-encode needs String[]
		println "pAttrs : "+pAttrs
		println "sAttrs : "+sAttrs

		try {
			cwbEn.setDebug(debug)
			cwbMa.setDebug(debug)
			String regPath =outDir + "/registry/"+corpusname.toLowerCase()
			cwbEn.run(outDir + "/data/$corpusname",
					null,
					regPath, pAttributes, sAttributes);

			def output = new OutputStreamWriter(cwbEn.getOutputStream());
			if (output == null) {println "CWB-ENCODE OUTPUT NULL"; return false;}
			ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
			println "processing "+files.size()+" files."
			output.write("<txmcorpus lang=\"fr\">\n")
			for (File f : files) {
				cpb.tick()
				output.write(f.getText("UTF-8"))
				output.flush();
				output.write("\n")
			}
			output.write("</txmcorpus>\n")
			output.close()
			cwbEn.endProcess();
			
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(corpusname, outDir + "/registry")
			return true
		} catch (Exception ex) {System.out.println("Error while compiling indexes: $ex"); ex.printStackTrace(); return false;}

		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/perrault/txm/")
		List<File> files = dir.listFiles()
		new compiler().run(files)
	}
}
