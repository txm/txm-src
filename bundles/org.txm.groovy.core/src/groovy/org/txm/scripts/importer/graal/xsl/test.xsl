<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="html" />
	<xsl:output method="html" encoding="UTF-8" version="4.0"
		indent="yes" />
	<xsl:strip-space elements="*" />

	<xsl:template match="/">
		<xsl:apply-templates select="//tei:text" />
	</xsl:template>


	<xsl:template match="//tei:body/tei:div">
		<br />
		<span style="font-family:Times;color:indigo">
			[
			<i>
				<xsl:value-of select="@type" />
			</i>
			&#xa0;
			<xsl:value-of select="@n" />
			]
		</span>
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="//tei:body/tei:div/tei:div">
		<br />
		<span style="font-family:Times;color:indigo">
			[
			<i>
				<xsl:value-of select="@type" />
			</i>
			&#xa0;
			<xsl:value-of select="@n" />
			]
		</span>
		<br />
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="tei:head">
		<span style="font-family:Arial; color:darkred">
			<xsl:apply-templates />
		</span>
		<br />
	</xsl:template>

	<xsl:template match="//tei:p">
		<p>
			<xsl:if test="@n">
				[ §
				<xsl:value-of select="@n" />
				]&#xa0;
			</xsl:if>
			<xsl:apply-templates />
		</p>
	</xsl:template>

	<xsl:template match="//tei:lb">
		<xsl:choose>
			<xsl:when test="position()=1 and not(ancestor::tei:seg)" />
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:q" mode="norm">
		<span style="background-color:khaki">
			<xsl:apply-templates />
		</span>
	</xsl:template>
	<xsl:template match="//tei:w">
		<span class="word" id="{@xml:id}" title="{@type}">
			<xsl:choose>
				<xsl:when test="@type[starts-with(., 'PON')]">
					<xsl:apply-templates />
				</xsl:when>
				<xsl:otherwise>
					&#xa0;
					<xsl:apply-templates />
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>
	<xsl:template match="tei:supplied">
		[
		<xsl:apply-templates />
		]
	</xsl:template>
</xsl:stylesheet>
