// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.perrault;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

// TODO: Auto-generated Javadoc
/** Build Discours corpus simple edition from a xml-tei. @author mdecorde */
class pager {
	List<String> NoSpaceBefore;
	
	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;
	
	/** The pagecount. */
	int pagecount = 0;
	
	/** The wordmax. */
	int wordmax = 0;

	/** The wordid. */
	String wordid;
	
	/** The first word. */
	boolean firstWord = true;
	
	/** The wordvalue. */
	String wordvalue;
	
	/** The interpvalue. */
	String interpvalue;
	
	/** The lastword. */
	String lastword = " ";
	
	/** The wordtype. */
	String wordtype;
	
	/** The flagform. */
	boolean flagform = false;
	
	/** The flaginterp. */
	boolean flaginterp = false;
	
	/** The url. */
	private def url;
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The writer. */
	def writer;
	
	/** The multiwriter. */
	def multiwriter = null;
	
	/** The infile. */
	File infile;
	
	/** The outfile. */
	File outfile;
	
	/** The pages. */
	ArrayList<File> pages = new ArrayList<File>();
	
	/** The idxstart. */
	ArrayList<String> idxstart = new ArrayList<String>();

	/**
	 * Instantiates a new pager.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 */
	pager(File infile, File outfile, List<String> NoSpaceBefore,
			List<String> NoSpaceAfter, int max) {
		this.wordmax = max;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.url = infile.toURI().toURL();
		this.infile = infile;

		inputData = url.openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		createOutput(outfile);
		process();
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput()
	{
		try {
			if(multiwriter != null)
			{
				multiwriter.write("</body>");
				multiwriter.write("</html>");
				multiwriter.close();
			}
			pagecount++;
			File f = new File(outfile.getParent()+"/multi/",outfile.getName().substring(0,outfile.getName().length()-6)+"_"+pagecount+".html");
			pages.add(f);
			idxstart.add(wordid)
			multiwriter = new OutputStreamWriter(new FileOutputStream(f) , "UTF-8");
			
			multiwriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			multiwriter.write("<html>");
			multiwriter.write("<head>");
			multiwriter.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
			multiwriter.write("<title>Perrault Edition - Page "+pagecount+"</title>");
			multiwriter.write("</head>");
			multiwriter.write("<body>");
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput(File outfile) {
		try {
			this.outfile = outfile;

			writer = new OutputStreamWriter(new FileOutputStream(outfile),
					"UTF-8");
			createNextOutput();
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public ArrayList<File> getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public ArrayList<String> getIdx() {
		return idxstart;
	}

	/**
	 * Process.
	 */
	void process()
	{
		String localname = "";
		
		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		writer.write("<html>");
		writer.write("<head>");
		writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
		writer.write("<title>Perrault Edition</title>");
		writer.write("</head>");
		writer.write("<body>");
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			
			
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{				
						case "text":
						XPathResult testG = new XPathResult(infile);
						String title =testG.getXpathResponse("//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[type='sub']","")
						writer.write("<h1>"+title+"</h1>\n")
						multiwriter.write("<h1>"+title+"</h1>\n")
						break;
						
						case "head":
						writer.write("<h2>\n")
						multiwriter.write("<h2>\n")
						break;
						
						case "lg":
						writer.write("<p>\n")
						multiwriter.write("<p>\n")
						break;
						
						case "p":
						case "q":
						writer.write("<p>\n")
						multiwriter.write("<p>\n")
						break;
						
						case "w":
						wordid=parser.getAttributeValue(null,"id");
						if(firstWord)
						{
							firstWord=false;
							this.idxstart.set(0,wordid);
						}
						wordcount++;
						if(wordcount >= wordmax)
						{
							createNextOutput();
							wordcount=0;
						}
						break;
						
						case "ana":
						flaginterp=true;
						interpvalue+=" "+parser.getAttributeValue(null,"type")+":"
						break;
						
						case "form":
						wordvalue="";
						interpvalue ="";
						flagform=true;
						break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{				
						case "head":
						writer.write("</h2>\n")
						multiwriter.write("</h2>\n")
						break;
						
						case "lg":
						writer.write("</p>\n")
						multiwriter.write("</p>\n")
						break;
						
						case "l":
							writer.write("<br/>\n")
							multiwriter.write("<br/>\n")
							break;
						
						case "p":
						case "q":
						writer.write("</p>\n")
						multiwriter.write("</p>\n")
						break;
						
						case "form":
						flagform = false
						
						break;
						
						case "ana":
						flaginterp = false
						break;
						
						case "w":
							int l = lastword.length();
							String endOfLastWord = "";
							if(l > 0)
								endOfLastWord = lastword.subSequence(l-1, l);
							
							if(interpvalue != null)
								interpvalue = interpvalue.replace("\"","&quot;");
							
							if(NoSpaceBefore.contains(wordvalue) || 
									NoSpaceAfter.contains(lastword) || 
									wordvalue.startsWith("-") || 
									NoSpaceAfter.contains(endOfLastWord))
							{
								writer.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
								multiwriter.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
							}
							else
							{
								writer.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
								multiwriter.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
							}
							writer.write(wordvalue.replace("<", "&lt;")+"</span>");
							multiwriter.write(wordvalue.replace("<", "&lt;")+"</span>");
							lastword=wordvalue;
						break;
						
					}
					break;
				
				case XMLStreamConstants.CHARACTERS:
					if(flagform)
						if(parser.getText().length() > 0)
							wordvalue+=(parser.getText());
					if(flaginterp)
						if(parser.getText().length() > 0)
							interpvalue+=(parser.getText());
					break;
			}
		}	
		writer.write("</body>");
		writer.write("</html>");
		writer.close();
		multiwriter.write("</body>");
		multiwriter.write("</html>");
		multiwriter.close();
	}
}
