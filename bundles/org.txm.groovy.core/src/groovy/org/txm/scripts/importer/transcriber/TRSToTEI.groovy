package org.txm.scripts.importer.transcriber

import java.io.File;

import javax.xml.stream.*;

import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.txm.importer.StaxIdentityParser;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.cwb.CwbProcess;
import org.txm.importer.cwb.PatchCwbRegistry;
import org.txm.utils.FileUtils;
import org.txm.utils.Pair;

class TRSToTEI extends StaxIdentityParser {
	
	// Transcription informations
	def informations = [:]
	def speakers = [:];
	HashMap<String, String> speakersname = new HashMap<String, String>();
	def topics = [:];

	List<String> localspeakers; // speakers of the current Turn

	// IDs
	int idturn = 1;
	int idsection = 1;
	int idu = 1;
	int idevent = 1;

	boolean uOpened = false;

	// values stored
	List<String> events = [];
	static int vEntityId = 0;
	static int vEntityIdCount = 1;
	String vSpeaker="";
	String u_name;
	boolean flagAna;
	boolean flagForm;
	boolean flagWord;
	String vWord="";
	String vForm="";
	String vAna="";
	String vEvents = "N/A";
	String vEntityType = "N/A"
	String wordid= "";
	String anatype = "";
	String anavalue = "";

	String formatedTime;
	String currentType;
	String lastTime = ""
	String textid;
	public TRSToTEI(File infile) {
		super(infile.toURI().toURL());
		textid = FileUtils.stripExtension(infile)
	}

	boolean beforeBody = true;
	protected void processStartElement() {
		if (beforeBody) {
			parseInfos()
		} else {
			processEpisodeStartElement()
		}
	}

	protected void processEndElement() {
		if (beforeBody) {
			parseInfosEndElements()
		} else {
			processEpisodeEndElement()
		}
	}

	/**
	 * Parses the infos.
	 */
	private void parseInfos() { //until tag Episode
		//println "parse infos: $localname"
		switch (localname) {
			case "Trans":
				for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
					informations.put(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
				}
				writer.writeStartElement("TEI");
				writer.writeStartElement("teiHeader");
				writer.writeStartElement("fileDesc");
				writer.writeEndElement();
				writer.writeStartElement("encodingDesc");
				writer.writeEndElement();
				writer.writeStartElement("profileDesc");
				writer.writeEndElement();
				writer.writeStartElement("application");
				break;
			case "Topics":
				super.processStartElement();
				break;
			case "Topic":
				super.processStartElement();
				for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
					topics.put(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
				}
				break;
			case "Speakers":
				super.processStartElement();
				break;
			case "Speaker":
				super.processStartElement();
				String id = parser.getAttributeValue(null, "id");
				String name = parser.getAttributeValue(null, "name");
				if (id != null && name != null) {
					speakersname.put(id, name);
				} else {
					println "found tag $localname with no id ($id) nor name ($name)"
					return;
				}

				if (id != null) {
					ArrayList list = new ArrayList<Pair<String, String>>()
					speakers.put(id, list);

					for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
						list.add(new Pair(parser.getAttributeLocalName(i), parser.getAttributeValue(i)));
					}
				} else {
					println "found tag $localname with no id"
					return;
				}
				break;
			case "Episode":
				writer.writeEndElement() // application
				writer.writeEndElement() // teiHeader
				writer.writeStartElement("text")
				writer.writeAttribute("id", textid)
				for (def k : informations.keySet()) writer.writeAttribute(k, informations[k])
				beforeBody = false; // end of info parsing
				break;
		}
	}

	private void parseInfosEndElements() { //until tag Episode
		//println "parse infos: $localname"
		switch (localname) {
			case "Topics":
				super.processEndElement();
				break;
			case "Topic":
				super.processEndElement();
				break;
			case "Speakers":
				super.processEndElement();
				break;
			case "Speaker":
				super.processEndElement();
				break;
		}
	}

	
	boolean overlapingTurn = false // need to skip fist Sync when multiple locutors in Turn
	/**
	 * Process.
	 *
	 * @param xmlfile the xmlfile
	 * @return true, if successful
	 */
	private boolean processEpisodeStartElement() {
		//println "parse start: $localname"
		switch(localname) {
			case "Section": // >> div
				testCloseU();
				writer.writeStartElement("div")
				writer.writeAttribute("n", Integer.toString(idsection++))
				writeAttributes();
				break;
			case "Turn": // >> sp
				testCloseU();
				vSpeaker = parser.getAttributeValue(null, "speaker");
				overlapingTurn = false
				if (vSpeaker == null) { vSpeaker="N/A"	// no spk
				} else {
					
					if (speakersname.containsKey(vSpeaker)) {
						//vSpeaker = speakersname.get(vSpeaker);
					} else {
						localspeakers = vSpeaker.split(" ")
						if (localspeakers.size() > 1) { // only one speaker
							overlapingTurn = true
						}
					}
				}

				writer.writeStartElement("sp")
				writer.writeAttribute("n", Integer.toString(idturn++))
				overlapingTurn = vSpeaker.contains(" ") // need to skip fist Sync when multiple locutors in Turn
				writer.writeAttribute("overlap", ""+overlapingTurn)
				
				String time = parser.getAttributeValue(null, "startTime");
				formatedTime = formatTime(time)
				writer.writeAttribute("time", formatedTime)
				
				writer.writeAttribute("start", time)
				writer.writeAttribute("end", ""+parser.getAttributeValue(null, "endTime"))
				writer.writeAttribute("who", ""+parser.getAttributeValue(null, "speaker"))
				
				for (int i = 0; i < parser.getAttributeCount(); i++) { // write other attributes if any
					String v = parser.getAttributeLocalName(i);
					if (!("who".equals(v)) && !("overlap".equals(v)) && !("time".equals(v)) && !("speaker".equals(v)) && !("endTime".equals(v)) && !("startTime".equals(v))) {
						writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), parser.getAttributeValue(i));
					}
				}
				break;
			case "Sync": // >> u
				lastTime = parser.getAttributeValue(null, "time")
				testCloseU();
//				if (overlapingTurn) { // need to skip fist Sync when multiple locutors in Turn
//					overlapingTurn = false;
//				} else {
//					
//				}
				if (!overlapingTurn) {
					writeU()
				}
				break;
			case "Who": // >> u
				testCloseU();
				int n = Integer.parseInt(parser.getAttributeValue(null, "nb")) -1;
				if (localspeakers.size() <= n || n < 0) {
					println "\nWarning: Mismatch speaker number declaration between <Who> and <Turn> tags at line "+parser.getLocation().getLineNumber()+" of the '"+inputurl.getFile()+"' transcription file.";
					vSpeaker = "#"+(n+1)+"?";
				} else {
					vSpeaker = localspeakers.get(n);
				}
				writeU()
				break;
			case "Event": // >> event
				writer.writeStartElement("event")
				writer.writeAttribute("id", Integer.toString(idevent++))
				writeAttributes();
				break;
			default:
				super.processStartElement();
		}
	}

	void processEpisodeEndElement() {
		switch (localname) {
			case "Section":
				if (uOpened) {
					super.processEndElement(); // u
					uOpened = false;
				}
				super.processEndElement(); //div
				break;
			case "Turn":
				if (uOpened) {
					super.processEndElement(); // u
					uOpened = false;
				}
				super.processEndElement(); // sp
				break;
			case "Sync":
			case "Who":
			case "Episode":
				break
			case "Trans":
				writer.writeEndElement(); // text
				writer.writeEndElement(); //TEI
				break
			default:
				super.processEndElement();
		}
	}

	/**
	 * If a 'u' tag is opened, close it.
	 */
	private testCloseU() {
		if (uOpened) {
			super.processEndElement(); // u
			writer.writeCharacters("\n")
			uOpened = false;
		}
	}

	/**
	 * Write u.
	 */
	private void writeU() {
		writer.writeStartElement("u")
		writer.writeAttribute("start", lastTime);
		writer.writeAttribute("time", formatTime(lastTime));
		writer.writeAttribute("n", ""+idu++);
//		println "write u for vSpeaker=$vSpeaker"
//		println "getting spk name? ="+speakers.get(vSpeaker)
//		println "speakers: $speakers"
		def attributes = speakers.get(vSpeaker)
		//println "ATTRIBUTES="+attributes+" vSpeaker='$vSpeaker'"
		if (attributes == null) { // in case of Who@n wrong number
//			if (vSpeaker.startsWith("#") && vSpeaker.endsWith("?")) { // don't show "N/A" vSpeaker
				writer.writeAttribute("who", vSpeaker)
				writer.writeAttribute("spkid", vSpeaker)
				//writeAttributes();
//			} else {
//				
//			}
		} else {
			for (Pair p : attributes) {
//				println " write attribute "+p.getFirst()+" "+p.getSecond()
				String attrn = p.getFirst().toString();
				if (attrn == "name") { // rename @name to @spk
					attrn = "who"
					u_name = p.getSecond();
				} else if (attrn == "id") { // rename @id to @spkid
					attrn = "spkid"
				}

				writer.writeAttribute(attrn, p.getSecond())
			}
		}
		uOpened = true;
	}
	
	private String formatTime(String time) {
		try {
			return formatTime(Float.parseFloat(time))
		} catch(Exception e) {return formatTime(0.0f)}
	}

	private String formatTime(float time) {
		String rez = " ";
		float h = time / 3600;
		time = time%3600;
		float min = (time%3600) / 60;
		int sec = (int)time%60
		if (min < 10)
			rez = ""+(int)h+":0"+(int)min;//+":"+time%60;
		else
			rez = ""+(int)h+":"+(int)min;//+":"+time%60;

		if (sec >= 10)
			rez += ":"+sec;
		else
			rez += ":0"+sec;

		return rez;
	}

	public static void main(String[] args) {
		File infile = new File("/home/mdecorde/xml/trs/int01.trs")
		File outfile = new File("/home/mdecorde/xml/trs/test-tei.xml")

		TRSToTEI p = new TRSToTEI(infile);
		p.process(outfile)
	}

}
