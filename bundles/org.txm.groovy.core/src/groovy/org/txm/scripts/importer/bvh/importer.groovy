// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.bvh

import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;
import javax.xml.stream.*;
import java.io.File;
import java.net.URL;

import org.txm.*;
import org.txm.core.engines.*;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.FusionHeader.*;
import org.txm.scripts.filters.TagSentences.TagSentences;

// TODO: Auto-generated Javadoc
/**
 * Convert TEI-BVH to TEI-TXM <br/>
 * 1- tokenize<br/>
 * 2- launch XML2TEITXM with no annotation import <br/>.
 *
 * @author mdecorde
 */
class importer {
	
	/**
	 * Run.
	 *
	 * @param rootDirFile contains the source files
	 */
	public static void run(File rootDirFile, String basename)
	{
		
		File fullfile;
		String rootDir =rootDirFile.getAbsolutePath();
		
		ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		
		File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+basename);
		binDir.deleteDir();
		binDir.mkdir();
		new File(binDir,"ptokenized").deleteDir();
		new File(binDir,"ptokenized").mkdir();
		new File(binDir,"tokenized").deleteDir();
		new File(binDir,"tokenized").mkdir();
		new File(binDir,"txm").deleteDir();
		new File(binDir,"txm").mkdir();
		
		//PREPARE EACH SPLITED FILE TO BE TOKENIZED
		List<File> srcfiles = rootDirFile.listFiles();
		println("Preparing Tokenizing "+srcfiles.size()+" files")
		for(File f : srcfiles)
		{
			print "."
			File srcfile = f;
			File resultfile = new File(binDir,"ptokenized/"+f.getName());
			//println("prepare tokenizer file : "+srcfile+" to : "+resultfile );
			def builder = new OneTagPerLine(srcfile.toURL(), milestones);
			builder.process(resultfile);
		}
		println ""
		rootDir = binDir.getAbsolutePath();
		
		//TOKENIZE FILES
		List<File> ptokenfiles = new File(rootDir,"ptokenized").listFiles()	
		println("Tokenizing "+ptokenfiles.size()+" files")
		for(File f : ptokenfiles)
		{
			print "."
			Sequence S = new Sequence();
			Filter F1 = new CutHeader();
			Filter F6 = new Tokeniser(f);
			Filter F7 = new TagSentences();
			Filter F11 = new FusionHeader();
			S.add(F1);
			S.add(F6);
			S.add(F7);
			S.add(F11);
			File infile = new File(rootDir+"/ptokenized",f.getName());
			File xmlfile = new File(rootDir+"/tokenized",f.getName());
			File headerfile = new File(rootDir+"/ptokenized/",f.getName()+"header.xml");
			
			S.SetInFileAndOutFile(infile.getPath(), xmlfile.getPath());
			S.setEncodages("UTF-8","UTF-8");
			Object[] arguments1 = [headerfile.getAbsolutePath()];
			F1.SetUsedParam(arguments1);
			Object[] arguments2 = [headerfile.getAbsolutePath(),F1];
			F11.SetUsedParam(arguments2);
			S.proceed();
			
			S.clean();
			//headerfile.delete();//remove the prepared file to clean
		}
		println ""
		
		//TRANSFORM INTO XML-TEI-TXM
		List<File> tokenfiles = new File(rootDir,"tokenized").listFiles()	
		println("Building "+tokenfiles.size()+" xml-tei-txm files")
		for(File f : tokenfiles)
		{
			print "."
			File file = f; 
			String txmfile = f.getName();
			
			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()
			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>();	
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
					//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();
			//lance le traitement
			String wordprefix = "w_";
			def builder = new Xml2Ana(file);
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			builder.process(new File(new File(rootDir,"txm"),txmfile));
		}
		println ""
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		new importer().run(new File("~/xml/bvh"));
	}
}
