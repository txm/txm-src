// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2016-05-26 17:42:36 +0200 (jeu. 26 mai 2016) $
// $LastChangedRevision: 3219 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.graal;

import java.net.URL;
import java.lang.Boolean
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll


// TODO: Auto-generated Javadoc
/**
 * The Class ImportInCWB.
 *
 * @author sheiden,mdecorde
 * 
 * import graal cqp fiel into cwb
 */
public class ImportInCWB{

	/** The root dir. */
	def rootDir;//chemin du dossier qui contien src/ et xsl/
	
	/** The in dir. */
	def inDir;
	 	
	//dossier qui contiendra les gsp et vues dipl, cour, facs, trad et cqp
	
	/** The out dir. */
	def outDir;
	//dossiers qui contient le registre cqp et le dossier data (qui contient les corpus)
	
	/** The out dir txm. */
	def outDirTxm;
	
 	/** The in file. */
	 def inFile;
	
	/** The in file2. */
	def inFile2;
	
	/** The out file. */
	def outFile;
	
	/** The out file2. */
	def outFile2;
	
	/**
	 * same args as GraalInportCqp.
	 *
	 * @param rootDir the root dir
	 * @param inFile the in file
	 * @param inFile2 the in file2
	 * @param outFile the out file
	 * @param outFile2 the out file2
	 */
	public ImportInCWB(	rootDir,
						inFile,
						inFile2,
						outFile,
						outFile2)
	{
		this.rootDir = rootDir;
		
		this.inFile = inFile;
		this.inFile2 = inFile2;
		this.outFile = outFile;
		this.outFile2 = outFile2;
		this.inDir = this.rootDir+"src" ;
		this.outDir = this.rootDir ;//outdir of the .gsp
		this.outDirTxm = this.rootDir ;//outdir for data files & registry
		this.createDirs();
	}
	
	/**
	 * create dirs to prevent errors.
	 */
	private void createDirs()
	{
		new File(outDir).mkdir();
		new File(outDir+"/cqp").mkdir();
		new File(outDirTxm).mkdir();
		new File(outDirTxm+"registry").mkdir();
		new File(outDirTxm+"data").mkdir();
	}
	
	/**
	 * call :
	 * GraalImportCqp
	 * cwb-encode
	 * cwb-makeall.
	 */
	public void start()
	{//transfome en cqp et compile les index
		System.out.println "file:///"+ inDir + "/" + inFile+ ".xml";
		CwbEncode cwbEn = new CwbEncode();
		CwbMakeAll cwbMa = new CwbMakeAll();
		
		GraalImportCqp traitTxt = new GraalImportCqp(new URL("file:///"+ inDir + "/" + inFile+ ".xml"));
		traitTxt.transfomFileCqp(outDir+"/cqp/", outFile + ".cqp");
		
		traitTxt = new GraalImportCqp(new URL("file:///"+ inDir + "/" + inFile2+ ".xml"));
		traitTxt.transfomFileCqpBrut(outDir+"/cqp/", outFile2 + ".cqp");
		
		System.out.println("--Building index files ...") 
		String[] pAttributecm = ["pos", "q", "supplied", "col", "line", "id", "dipl", "facs"];
		String[] sAttributecm = ["p:0+n", "q:1+n", "s:0+n+id"];
		
		String[] pAttributes = ["pos", "id", "col"];
		String[] sAttributes = ["p:0+n", "s:0+n"];
		try {
			if (System.getProperty("os.name").contains("Windows"))
			{
				cwbEn.run(outDirTxm + "data/"+outFile, outDir +"/cqp/"+outFile+".cqp", outDirTxm + "registry/"+outFile,pAttributecm, sAttributecm);
				cwbMa.run(outFile.toUpperCase(), outDirTxm + "registry");
				
				cwbEn.run(outDirTxm + "data/"+outFile2, outDir + "/cqp/"+outFile2+".cqp", outDirTxm + "registry/"+outFile2, pAttributes, sAttributes);
				cwbMa.run(outFile2.toUpperCase(), outDirTxm + "registry");
			}
			else
			{
				cwbEn.run(outDirTxm + "data/"+outFile, outDir + "/cqp/"+outFile+".cqp", outDirTxm + "registry/"+outFile,pAttributecm, sAttributecm);
				cwbMa.run(outFile.toUpperCase(), outDirTxm + "registry");
				
				cwbEn.run(outDirTxm + "data/"+outFile2, outDir +"/cqp/"+ outFile2+".cqp", outDirTxm + "registry/"+outFile2, pAttributes, sAttributes);
				cwbMa.run(outFile2.toUpperCase(), outDirTxm + "registry");
			}
		} catch (Exception ex) {System.out.println(ex);}
				System.out.println("Done.") 
	}
	
	/**
	 * test.
	 */
	public void run() 
	{
		System.out.println "Starting script : importCQP.groovy ...";
		System.setProperty("javax.xml.transform.TransformerFactory",  
				 			"net.sf.saxon.TransformerFactoryImpl");  
		this.importCQP();
		System.out.println "GraalImport.groovy done."
	}
}