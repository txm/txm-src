// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS  FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2011-11-01 16:12:36 +0100 (mar., 01 nov. 2011) $
// $LastChangedRevision: 2049 $
// $LastChangedBy: sheiden $
//

package org.txm.scripts.importer.transcriber
import java.text.DecimalFormat;
import groovy.xml.XmlParser
// parameters

String userdir = System.getProperty("user.home")
File infile = new File(userdir, "xml/concattrs/Reçues/int23.trs")
File outfile = new File(userdir, "xml/concattrs/Reçues/int23-corr.trs")

public boolean checkSpk(String spk) {
	return spk.matches(ValidateTRS.spkPattern);
}

public fixTopics(def trs) {
	
	def okTopics = ValidateTRS.okTopics;
	
	// get Topic ids
	def ids2desc = [:];
	def declaredTopics = trs.Topics.Topic;
	for (def topic : declaredTopics) {
		ids2desc[topic.@id]=topic.@desc
	}
	
	// fix Sections
	for(def section : trs.Episode.Section) {

		if (section.@topic != null)
		if (okTopics.contains(ids2desc[section.@topic])) {
			section.@topic = ids2desc[section.@topic]
		}
	}
	
	// fix Section decl
	for(def topic : trs.Topics.Topic) {
		if (okTopics.contains(ids2desc[topic.@id])) {
			topic.@id = topic.@desc
		}
	}
}

def fixTurn(prevTurn, turn) // end < start
{
	def start =  Float.parseFloat(turn.'@startTime')
	def startS = turn.'@startTime'
	def end =  Float.parseFloat(turn.'@endTime')
	def endS = turn.'@endTime'

	// patch previous turn
	if (prevTurn != null) {
		def prevstart = Float.parseFloat(prevTurn.'@startTime')
		if (prevstart > end) {
			println "** FixTiming: Warning: previous turn not updated ($prevstart > $end): $prevTurn"
		} else {
			prevTurn.'@endTime' = endS;
		}
	} else {
		turn.parent().'@startTime' = endS
	}

	// patch current turn
	turn.'@startTime' = endS;
	turn.'@endTime' = startS;

	// patch next turn
	def i = turn.parent().children().indexOf(turn)
	if (turn.parent().children().size() > i+1) {
		def nextTurn = turn.parent().children().get(i+1)
		if (nextTurn != null) {
			def nextend = Float.parseFloat(nextTurn.'@endTime')
			nextTurn.'@startTime' = startS; // patch next Turn even if nextTurn.end < start, loop will continue to patch next turns
		}
	} else {
		turn.parent().'@endTime' = startS
	}
	println "** FixTiming: fixed Turn: old time[$startS, $endS] to ["+turn.@startTime+", "+turn.@endTime+"]"
}

def fixTurn2(turn, timeResolution) // start == end
{
	DecimalFormat formater = DecimalFormat.getInstance(Locale.ENGLISH)
	formater.setMaximumFractionDigits(3)
	//DecimalFormat formater = new DecimalFormat("#.000");
	def start =  Float.parseFloat(turn.'@startTime')
	def startS = turn.'@startTime'
	def end =  Float.parseFloat(turn.'@endTime')+timeResolution*10
	def endS = formater.format(end)
	println "ENDS: "+endS

	// patch current turn
	turn.'@endTime' = endS;

	// patch next turn
	def i = turn.parent().children().indexOf(turn)
	if (turn.parent().children().size() > i+1) {
		def nextTurn = turn.parent().children().get(i+1)
		if (nextTurn != null) {
			def nextstart = Float.parseFloat(nextTurn.'@startTime')
			nextTurn.'@startTime' = endS; // patch next Turn even if nextTurn.end < start, loop will continue to patch next turns
		}
	} else {
		turn.parent().'@endTime' = endS
	}

	println "** FixTiming: fixed Turn: equal old time[$startS, $endS] to ["+turn.@startTime+", "+turn.@endTime+"]"
}

def fixSync(turn)
{
	def start =  Float.parseFloat(turn.'@startTime')
	def end =  Float.parseFloat(turn.'@endTime')
	def syncs = turn.Sync
	def badSyncList = []

	def lastValidSyncTime = start
	def firstSync = true

	for (def sync : syncs) {
		if (firstSync) {
			if (sync.@time != turn.'@startTime') {
				println "** FixTiming: fixed first Sync: old time "+sync.@time+" to "+turn.'@startTime'
			}
			sync.@time = turn.'@startTime'
			lastValidSyncTime = start
			firstSync = false
		} else {
			def time = Float.parseFloat(sync.@time)
			if (time > lastValidSyncTime && time < end) {
				fixBadSyncList(badSyncList, lastValidSyncTime, time)
				lastValidSyncTime = time
				badSyncList = []
			} else {
				badSyncList << sync
			}
		}
	}
	fixBadSyncList(badSyncList, lastValidSyncTime, end)
}

def fixBadSyncList(list, start, end) {
	def timeResolution = 0.001
	if (start >= end) {
		println("** FixTiming: fixBadSyncList: start($start) >= end($end)")
		return
	}
	if (list.size() > 0) {
		def dt = (end - start)/(list.size()+1)
		if (dt < timeResolution) {
			println("** FixTiming: fixBadSyncList: dt($dt) < $timeResolution seconds")
			return
		}
		def st = start
		list.each { sync ->
			st += dt
			def oldtime = sync.@time
			sync.@time = ""+st
			println "** FixTiming: fixed Sync: old time $oldtime to $st"
		}
	}
}

public def fixTRS(File infile, File outfile) {
	def timeResolution = 0.001
	URL u = infile.toURI().toURL()
	InputStream ins = u.openStream()

	// Open input file
	def slurper = new XmlParser();
	def trs = slurper.parse(infile.toURI().toString())

	// First fix all <Turn>
	def prevTurn = null
	for (def section : trs.Episode.Section) {
		section.Turn.each{ turn ->
			def start = Float.parseFloat(turn.'@startTime')
			def end =  Float.parseFloat(turn.'@endTime')
			if (end < start) {
				fixTurn(prevTurn, turn)
			} else if (start == end) {
				fixTurn2(turn, timeResolution)
			}
			prevTurn = turn
			//} catch (Exception e){ org.txm.utils.logger.Log.printStackTrace(e); println "start: "+turn.'@startTime' println "end: "+turn.'@endTime'}
		}
	}

	// Then fix all <Sync>s of Turns
	for (def section : trs.Episode.Section) {
		section.Turn.each{ turn ->
			fixSync(turn)
		}
	}
	
	// Finally fix topics declarations
	fixTopics(trs);

	String xml = "";

	println ""+xml
	outfile.withWriter("UTF-8"){ writer ->
		writer.write('<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE Trans SYSTEM "trans-14.dtd">\n')
		new groovy.xml.XmlNodePrinter(new PrintWriter(writer)).print(trs) }
}

/// MAIN ///
fixTRS(infile, outfile);