// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-05-26 17:42:36 +0200 (jeu. 26 mai 2016) $
// $LastChangedRevision: 3219 $
// $LastChangedBy: mdecorde $
//


package org.txm.scripts.importer.xmltxm;

import java.util.ArrayList;;

import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.cwb.BuildCwbEncodeArgs;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.scripts.importer.*;
import org.txm.objects.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;
import org.txm.searchengine.cqp.corpus.*
import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * The Class compiler.
 */
class compiler
{
	String sortMetadata;
	/** The debug. */
	private boolean debug= false;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The dir. */
	private def dir;

	/** The output. */
	private Writer output;

	/** The url. */
	private def url;

	/** The text. */
	String text="";

	/** The base. */
	String base="";

	/** The project. */
	String projectName="";

	/** The lang. */
	private String lang ="fr";

	/** The s attribs. */
	private static HashMap<String, List<String>> sAttribs;

	/** The anatypes. */
	private static anatypes = []
	private static anavalues = [:]
	
	/**
	 * initialize.
	 *
	 */
	public compiler(){}

	/**
	 * Instantiates a new compiler.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 */
	public compiler(URL url,String text,String base, String projectName)
	{
		this.text = text
		this.base = base;
		this.projectName = projectName;
		try {
			this.url = url;
			inputData = url.openStream();

			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (Exception ex) {
			System.out.println("Error while creating indexes: $ex");
			ex.printStackTrace();
		}
	}

	public void setSortMetadata(String sortMetadata)
	{
		this.sortMetadata = sortMetadata;
	}

	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}

	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(File f) {
		try {
			//File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(f,f.exists())) , "UTF-8");
			return true;
		} catch (Exception e) {
		println "Error while create CQP otput file: "+e
			e.printStackTrace();

			return false;
		}
	}

	/**
	 * Go to text.
	 */
	private boolean GoToText()
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.END_ELEMENT)
				if (parser.getLocalName().equals("teiHeader") || parser.getLocalName().equals("teiheader")) {
					return true;
				}
		}
		return false;
	}

	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(File cqpFile)
	{
		createOutput(cqpFile);
		String headvalue=""
		String vAna = "";
		String vForm = "";
		String wordid= "";
		String vHead = "";
		String anatype = null;
		String anavalue = null;
		int p_id = 0;
		int s_id = 0;

		boolean captureword = false;
		boolean flagForm = false;
		boolean flagAna = false;
		boolean inW = false;
		int wcounter = 1;
		if (!GoToText()) {
			println "Error: no teiHeader tag found in text '"+this.text+"' (please check file format or content)"
			return false;
		}
		int wc = 0; // TEMP FOR TEST ONLY
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
			{
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						//println "start: "+parser.getLocalName()
						switch (parser.getLocalName()) {
							case "w":
							
							inW = true;
							anavalues = [:]
							wordid = parser.getAttributeValue(null, "id")
							if (wordid == null)
								wordid = "w_"+text+"_"+(wcounter++)

							vAna ="";
							break;

							case "form":
							String type2 = parser.getAttributeValue(null, "type");
							if(type2 == null || type2.equals("default")) {
								flagForm = true;
								vForm = "";
							} else {
								flagAna = true;
								vAna += "\t";
								if(!anatypes.contains(type2))
									anatypes << type2;
							}
							
							break;

							case "ana":
							flagAna = true;
							anavalue = "";
							anatype = parser.getAttributeValue(null, "type");
							if (anatype != null) {
								if(anatype.startsWith("#"))
									anatype = anatype.substring(1)
								break;
							}
							break;

							default:
							if (!inW) {
								output.write("<"+parser.getLocalName().toLowerCase());
								if (!sAttribs.containsKey(parser.getLocalName()))
									sAttribs.put(parser.getLocalName().toLowerCase(), []);

								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									String attrname = parser.getAttributeLocalName(i).toLowerCase();
									String attrvalue = parser.getAttributeValue(i);
									if (!(parser.getLocalName() == "text" && attrname == "id"))
										output.write(" "+attrname+"=\""+attrvalue.replace("\"", "'")+"\"");

									if (!sAttribs.get(parser.getLocalName().toLowerCase()).contains(attrname))
										sAttribs.get(parser.getLocalName().toLowerCase()).add(attrname)
								}

								if (parser.getLocalName() == "text") {
									output.write(" id=\""+text+"\" base=\""+base+"\" project=\""+projectName+"\"");
								}
								output.write(">\n");
							}
						}
						break;

					case XMLStreamConstants.END_ELEMENT:
						switch (parser.getLocalName()) {
							case "TEI":
								break;
							case "w":
							for (String t : anatypes) {
								def v = anavalues.get(t);
								if (v != null) vAna +="\t"+v;
								else vAna +="\t";
							}
							
							output.write( vForm.replaceAll("&", "&amp;").replaceAll("<", "&lt;") +"\t"+wordid+vAna+"\n");
							vAna = "";
							vForm = "";
							inW = false;
							break;

							case "form":
							flagForm = false;
							flagAna = false;
							break;

							case "ana":
							anavalues.put(anatype, anavalue)
							flagAna = false;
							break;

							default:
							if(!inW)
								output.write("</"+parser.getLocalName().toLowerCase()+">\n");

						}
						break;

					case XMLStreamConstants.CHARACTERS:
						if(inW)
						{	
							if(flagForm) {	
								vForm += parser.getText().trim();
							}
							else if (flagAna) {
								anavalue += parser.getText().trim();
							}
						}
						break;
				}
			}

			output.close();
			if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		}
		catch (Exception ex) {
			System.out.println("Error while writing CQP file $ex");
			ex.printStackTrace();
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}
		return true;
	}



	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(Project project, File binDir, File txmDir, String basename, String corpusname, List<File> files)
	{
		anatypes = new ArrayList<String>();// init only 1 time
		anavalues = [:]
		sAttribs = new HashMap<String, List<String>>();// init only 1 time
		String rootDir = binDir.getAbsolutePath();

		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		CorpusBuild corpus = project.getCorpusBuild(project.getName(), MainCorpus.class);
		if (corpus != null) {
			if (project.getDoUpdate()) {
				corpus.clean(); // remove old files
			} else {
				corpus.delete(); // remove old files and TXMResult children
			}
		} else {
			corpus = new MainCorpus(project);
			corpus.setID(project.getName());
			corpus.setName(project.getName());
		}
		corpus.setDescription("Built with the XML-TXM import module");
		
		File cqpFile = new File(binDir,"cqp/"+corpusname+".cqp");
cqpFile.delete()
		new File(binDir,"cqp").mkdirs()
		new File(binDir,"data").mkdirs()
		new File(binDir,"registry").mkdirs()

		String textid = "";
		int counttext = 0;
		
		// get all anatypes
		for (File f : files) {
			getAnaTypes(f)
		}

		//0 set Lang
		if (createOutput(cqpFile)) {
			output.write("<txmcorpus lang=\""+lang+"\">\n");
			output.close();
		}
		//1- Transform into CQP file
		def builder = null;
		for (File f : files) {
			counttext++;
			if (!f.exists()) {
				println("file "+f+ " does not exists")
			} else {
				//println("process file "+f)
				String txtname = f.getName().substring(0,f.getName().length()-4);
				builder = new compiler(f.toURI().toURL(), txtname, corpusname.toLowerCase(), "default");
				builder.setLang(lang);
				if(!builder.transfomFileCqp(cqpFile))
					return false;
			}
		}

		//end corpus
		if (createOutput(cqpFile)) {
			output.write("</txmcorpus>\n");
			output.close();
		}

		//2- Import into CWB

		CwbEncode cwbEn = new CwbEncode();
		cwbEn.setDebug(debug);
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbMa.setDebug(debug);
		List<String> pargs = ["id"];
		for(String ana : anatypes)
			pargs.add(ana);

		List<String> sargs = [];
		//println "Found Sattributes "+this.sAttribs;
		if(sAttribs.containsKey("text")) {
			if(!sAttribs.get("text").contains("id"))
				sAttribs.get("text").add("id");
			if(!sAttribs.get("text").contains("base"))
				sAttribs.get("text").add("base");
			if(!sAttribs.get("text").contains("project"))
				sAttribs.get("text").add("project");
		} else {
			sargs.add("text:0+id+base+project")
		}

		if (sAttribs.containsKey("txmcorpus")) {
			if(!sAttribs.get("txmcorpus").contains("lang"))
				sAttribs.get("txmcorpus").add("lang");
		} else {
			sargs.add("txmcorpus:0+lang")
		}

		for (String tag : this.sAttribs.keySet()) {
			String sAttr = tag;
			if(sAttribs.get(tag).size() > 0)
				sAttr += ":";
			for(String attr : sAttribs.get(tag))
				sAttr +="+"+attr;
			sargs.add(sAttr)
		}



		String[] sAttributes = sargs;
		String[] pAttributes = pargs;
		println "sAttributes : "+sAttributes;
		println "pAttributes : "+pAttributes;
		try {
			String regPath = rootDir + "/registry/"+corpusname.toLowerCase() 
			cwbEn.run(
				rootDir + "/data/$corpusname", 
				cqpFile.getAbsolutePath(), 
				regPath, pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(corpusname, rootDir + "/registry");

		} catch (Exception ex) {
			System.out.println("Error while creating indexes with CQP tools: $ex");
			ex.printStackTrace();
			return false;
		}
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "cqp").deleteDir()
		}

		return true;
	}

	/**
	 * Sets the debug.
	 */
	public void setDebug()
	{
		this.debug = true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/geo");
		def c = new compiler();
		c.setDebug();
		c.setCwbPath("~/TXM/cwb/bin");
		c.run(dir,"geo");
	}
	
	private void getAnaTypes(File xmlFile) {
		inputData = xmlFile.toURI().toURL().openStream();
		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		String ana = "ana"
		HashSet<String> types = new HashSet<String>();
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			if (event == XMLStreamConstants.START_ELEMENT) { // start elem
				if (ana.equals(parser.getLocalName())) { // ana elem
					for (int i = 0 ; i < parser.getAttributeCount(); i++) { // find @type
						if ("type".equals(parser.getAttributeLocalName(i))) { // @type
							types.add(parser.getAttributeValue(i).substring(1)); //remove the #
							break;
						}
					}
				}
			}
		}
		
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();

		for (String type : types)
			if (!anatypes.contains(type))
				anatypes << type
	}
}
