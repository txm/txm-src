package org.txm.scripts.importer.xtz

import org.txm.scripts.importer.*
import org.xml.sax.Attributes
import org.txm.importer.scripts.filters.*
import java.util.ArrayList
import javax.xml.parsers.*
import javax.xml.stream.*
import java.net.URL
import org.xml.sax.InputSource
import org.xml.sax.helpers.DefaultHandler
import java.io.FileFilter
import org.txm.importer.xtz.*

class XTZFacsPagerStep extends Step {

	private def url
	private def inputData
	private def factory
	private XMLStreamReader parser
	OutputStreamWriter writer
	StaxStackWriter pagedWriter = null

	File editionDir
	File imageDirectory // contains all the images sub-diretories
	int iImages
	File textImagesDirectory // contains the images for the txtname
	File xmlFile
	File htmlFile

	def pages = []
	def imageAttributes = [:];
	
	def tag, attribute, txtname, corpusname
	boolean firstWord
	boolean cutBefore = true;
	String wtag;
	boolean debug = false;

	public XTZFacsPagerStep(ImportStep importStep, File xmlFile, File editionDir, File imageDirectory, String txtname, String corpusname, String tag, String attribute, String wtag, boolean debug) {
		super(importStep)
		
		inputData = xmlFile.toURI().toURL().openStream()
		factory = XMLInputFactory.newInstance()
		parser = factory.createXMLStreamReader(inputData)

		this.xmlFile = xmlFile
		this.editionDir = editionDir
		editionDir.mkdirs()
		this.imageDirectory = imageDirectory
		this.tag = tag
		this.attribute = attribute
		this.txtname = txtname
		this.corpusname= corpusname
		this.wtag= wtag;
		this.debug = debug

		imageAttributes = ["src":"", 
			"id":"txm_image", 
			"alt":"Le fac-similé de cette page n'est pas accessible", 
			//"style":"display:block;width:100%;", 
			"onLoad":"viewer.toolbarImages='images/icons';viewer.onload=viewer.toolbar;new viewer({image: this, frame: ['99%','100%']});"
			]
		
		if (imageDirectory != null)
			textImagesDirectory = new File(imageDirectory, txtname)
		iImages = 0;
	}

	int n = 1;
	private boolean createNextOutput() {
		try {
			def tags = closeMultiWriter();
			for (int i = 0 ; i < tags.size() ; i++) {
				String tag = tags[i]
				if ("div" != tag) {
					tags.remove(i--)
				} else {
					tags.remove(i--) // remove first "div"
					break; // remove elements until first "div" tag
				}
			}

			if (wordid != null) {//wordid = "w_0";
				//println " add page $n $wordid, page=$pages"
				pages << [htmlFile, wordid] // store the previous page
				n++
			}

			// Page suivante
			htmlFile = new File(editionDir, "${txtname}_${n}.html")

			firstWord = true
			//println "SET FIRST WORD=true"
			pagedWriter = new StaxStackWriter(htmlFile, "UTF-8");
			if (debug) println "Create file $htmlFile"
			
			pagedWriter.writeStartDocument("UTF-8", "1.0")
			pagedWriter.writeDTD("<!DOCTYPE html>")
			pagedWriter.writeStartElement("html");
			pagedWriter.writeStartElement("head");
			pagedWriter.writeStartElement("title")
			pagedWriter.writeCharacters(corpusname+" Edition - Page "+n)
			pagedWriter.writeEndElement(); // </title>
			pagedWriter.writeStartElement("meta", ["http-equiv":"Content-Type", "content":"text/html","charset":"UTF-8"]);
			pagedWriter.writeEndElement()
			pagedWriter.writeStartElement("link", ["rel":"stylesheet", "media":"all", "type":"text/css","href":"txm.css"]);
			pagedWriter.writeEndElement()
			pagedWriter.writeStartElement("link", ["rel":"stylesheet", "media":"all", "type":"text/css","href":"${corpusname}.css"]);
			pagedWriter.writeEndElement()
			pagedWriter.writeStartElement("script", ["type":"text/javascript", "src":"js/viewer/Simple_Viewer_beta_1.1-min.js"]);
			pagedWriter.writeEndElement()
			pagedWriter.writeStartElement("script", ["type":"text/javascript", "src":"js/viewer/toolbar-ext.js"]);
			pagedWriter.writeEndElement()
			pagedWriter.writeStartElement("link", ["rel":"stylesheet", "media":"all", "type":"text/css","href":"js/viewer/toolbar-ext.css"]);
			pagedWriter.writeEndElement()
			pagedWriter.writeEndElement() // </head>
			pagedWriter.writeStartElement("body") //<body>
			pagedWriter.writeStartElement("div", ["id":"txmeditionpage"]);
			pagedWriter.writeStartElements(tags);

			wordid = "${wtag}_0"; // default value if no word is found
			return true;
		} catch (Exception e) {
			System.out.println("Error while creating HTML file: "+e);
			return false;
		}
	}

	private def closeMultiWriter()
	{
		if (pagedWriter != null) {
			def tags = pagedWriter.getTagStack().clone();

			if (firstWord) { // there was no words
				pagedWriter.writeCharacters("");
				pagedWriter.write("<span id=\"${wtag}_0\"/>");
			}
			pagedWriter.writeEndElements();
			pagedWriter.close();
			return tags;
		} else {
			return [];
		}
	}

	private writeImg(String src) {
		
		
		if (src == null || src.length() == 0) {
			pagedWriter.writeStartElement("p", ["class":"no-img"]);
			pagedWriter.writeCharacters("Cette page n'est pas disponible.");
			pagedWriter.writeEndElement(); // </div>
		} else {
			imageAttributes["src"] = src
			pagedWriter.writeEmptyElement("img", imageAttributes);
		}
		
	}

	/**
	 * Process the XML-TXM file. if imageDirectory is not null, then it will be use to fetch the images, if not then the pb@facs attribute contains the image path
	 */
	public boolean process() {
		if (textImagesDirectory != null) {
			return processWithImages();
		} else {
			return processWithoutImages();
		}
	}
	
	String wordid = null;
	def imagesFiles;
	public boolean processWithImages() {

		if (!textImagesDirectory.exists()) {
			println ("No images directory found: "+textImagesDirectory)
			return false;
		}

		// Scan textImagesDirectory for images
		imagesFiles = textImagesDirectory.listFiles(new FileFilter() {
					public boolean accept(File f) {
						return !f.isHidden() && !f.isDirectory()
					}
				});
		if (imagesFiles == null || imagesFiles.size() == 0) {
			println ("No facs images found in "+textImagesDirectory)
			return false;
		}
		imagesFiles.sort(); // sort by name
		iImages = 0;
		//println "For text "+xmlFile.getName()
		//println " using images : $imagesFiles"

		// parse XML-TXM files
		boolean start = false
		String localname

		createNextOutput(); // empty page

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "text":
							start = true
							printStartPage();
							break;
							break;
						case wtag:
							if (firstWord) {
								wordid = parser.getAttributeValue(null, "id");
								//println "found word: $wordid"
								firstWord = false;
							}
							break;
						case tag:
							if (debug) println "** TAG $tag $attribute : "+parser.getAttributeValue(null, attribute);

							if (iImages >= imagesFiles.size() ) {
								println " ERROR Could not find the ${iImages+1}th image for file $xmlFile at location "+parser.getLocation().getLineNumber()
								writeImg(null)
								createNextOutput()
							} else {
								String imgPath = "res/images/$corpusname/${editionDir.getName()}/$txtname/"+imagesFiles[iImages].getName()
								if (imgPath == null) {
									println " ERROR in $xmlFile no value found for tag=$tag attribute=@$attribute iImages=@iImages at location "+parser.getLocation().getLineNumber()
									writeImg(null)
									createNextOutput()
								} else {
									if (cutBefore) {
										if (debug) println " cut before"
										createNextOutput()
										if (debug) println " write img $imgPath"
										writeImg(imgPath)
									} else {
										if (debug) println " write img $imgPath"
										writeImg(imgPath)
										if (debug) println " cut after"
										createNextOutput()
									}
								}
							}
							iImages++ // next image file
							break;
					}
					break;
			}
		}
		closeMultiWriter()
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		
		pages << [htmlFile, wordid] // add the last page (no pb encountered

		return pages.size() > 1
	}

	public boolean processWithoutImages() {

		// parse XML-TXM files
		boolean start = false
		String localname

		createNextOutput(); // empty page

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch (localname) {
						case "text":
							start = true
							printStartPage()
							break;
							
						case wtag:
							if (firstWord) {
								wordid = parser.getAttributeValue(null, "id");
								//println "found word: $wordid"
								firstWord = false;
							}
							break;
							
						case tag:
							String url = "" // get image URL 
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								if (parser.getAttributeLocalName(i).equals(attribute)) {
									url = parser.getAttributeValue(i);
									continue;
								}
							}
							if (debug) println "** TAG $tag $attribute : $url"

							if (cutBefore) {
								if (debug) println " cut before"
								createNextOutput()
								if (debug) println " write img $url"
								writeImg(url)
							} else {
								if (debug) println " write img $url"
								writeImg(url)
								if (debug) println " cut after"
								url()
							}

							break;
					}
					break;
			}
		}
		closeMultiWriter()
		pages << [htmlFile, wordid] // add the last page (no pb encountered)

		return pages.size() >= 1
	}

	public void printStartPage() {
		LinkedHashMap attributes = new LinkedHashMap();
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			attributes[parser.getAttributeLocalName(i)] = parser.getAttributeValue(i).toString()
		}
		pagedWriter.write("\n");
		pagedWriter.writeStartElement("p")
		if (attributes.containsKey("id")) {
			pagedWriter.writeElement("h3", attributes["id"])
		}
		
		pagedWriter.writeStartElement("table");
		for (String k : attributes.keySet()) {
			if (k == "id") continue;
			pagedWriter.writeStartElement("tr");
			pagedWriter.writeElement("td", k);
			pagedWriter.writeElement("td", attributes[k]);
			pagedWriter.writeEndElement();
		}
		pagedWriter.writeEndElement() // table
		pagedWriter.writeEndElement() // p
		
		pagedWriter.writeCharacters("");
	}
	
	public def getPageFiles() {
		return pages
	}
	
	public static void main(String[] args) {
		File txmFile = new File(System.getProperty("user.home"), "TXM/corpora/BVHEPISTEMON2016/txm/BVHEPISTEMON2016/1546_RabelaisTL.xml")
		String txtname = "1532_RabelaisPrnstctn"
		String corpusname = "BVH"
		File newEditionDirectory = new File("/tmp/xtzpagertest/")
		newEditionDirectory.deleteDir()
		newEditionDirectory.mkdir()
		File imageDirectory = null;
		
		def ed = new XTZFacsPagerStep(txmFile, newEditionDirectory, imageDirectory, txtname, corpusname, "pb", "facs", "w", true);
		if (!ed.process()) {
			println "Fail to build edition for text: $txmFile"
		}
	}
}