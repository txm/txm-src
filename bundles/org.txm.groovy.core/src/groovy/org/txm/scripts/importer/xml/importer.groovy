// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-06-26 16:53:47 +0200 (lun. 26 juin 2017) $
// $LastChangedRevision: 3451 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.xml

//import org.txm.scripts.filters.TabulatedToXml.*;
import java.io.File

import javax.xml.stream.*

import org.txm.*
import org.txm.scripts.importer.*
import org.txm.utils.FileUtils
import org.txm.utils.io.FileCopy
import org.txm.importer.scripts.filters.*
import org.txm.objects.*
import org.txm.scripts.*
import org.txm.importer.scripts.xmltxm.*

import org.txm.scripts.filters.CutHeader.*
import org.txm.scripts.filters.FusionHeader.*
import org.txm.scripts.filters.TagSentences.*
import org.txm.scripts.filters.Tokeniser.*
import org.txm.importer.ValidateXml
import org.txm.tokenizer.TokenizerClasses

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {

	/** The validation. */
	boolean validation

	/** The tokenize. */
	boolean tokenize

	/** The sentence. */
	boolean sentence = false

	/**
	 * Do tokenize.
	 *
	 * @param b the b
	 */
	public void doTokenize(boolean b) { tokenize = b; }

	/**
	 * Do validation.
	 *
	 * @param b the b
	 */
	public void doValidation(boolean b) { validation = b; }

	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(File srcDir, File binDir, File txmDir, String basename, String ignoredElements, String lang, def project)
	{
		new File(binDir,"tokenized").deleteDir()
		new File(binDir,"tokenized").mkdir()

		ArrayList<String> milestones = new ArrayList<String>()//the tags who you want them to stay milestones

		//TEST EACH XML FILE IF VALID
		List<File> okfiles = []

		println "Sources cleaning & validation"
		def files = FileUtils.listFiles(txmDir)
		for (File f : files) { // clean directory
			String name = f.getName()
			if (f.isHidden() || !name.toLowerCase().endsWith(".xml")) {
				if (!f.delete()) {
					println "WARNING: could not clean $txmDir directory: TXM could not delete $f"
					return false
				}
			}
		}
		
		files = FileUtils.listFiles(txmDir)
		if (files == null || files.size() == 0) {
			println "No XML file (*.xml) to process. Aborting"
			return false
		}
		ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
		for (File f : files) {
			if (!f.getName().toLowerCase().endsWith(".xml")) {
				continue
			}
			if (!f.isHidden() && f.canRead() && !f.getName().endsWith(".properties") && !f.getName().startsWith("metadata") && !f.isDirectory()) {
				cpb.tick()
				if (!ValidateXml.test(f)) {
					if (stopIfMalformed) {
						return
					} else {
						continue
					}
				} else {
					if (!f.getName().equals("import.xml")) {
						okfiles.add(f)
					}
				}
			}
		}
		cpb.done()

		okfiles.sort()
		if (okfiles.size() == 0) {
			println "No file. Check if the file extensions are '.xml'"
			return false
		}
		
		// Fix surrogates
		File srcDirectory = new File(binDir, "src")
		srcDirectory.deleteDir()
		srcDirectory.mkdir()
		for (File f : okfiles) {
			File outputFile = new File (srcDirectory, f.getName())
//			println "TEMP REMOVED SURROGATE FIX"
//			FileCopy.copy(f, outputFile)
			CleanFile.removeSurrogateFromXmlFile(f, outputFile)
		}
		okfiles = FileUtils.listFiles(srcDirectory)
		okfiles.sort()
		
		//println "ptokenfiles "+ptokenfiles
		//TOKENIZE FILES
		List<File> tokenfiles;
		if (tokenize || sentence) {
			println "Tokenizing "+okfiles.size()+" files"
			ConsoleProgressBar cpb2 = new ConsoleProgressBar(files.size())
			for (File f : okfiles) {
				cpb2.tick()
				File infile = f
				File outfile = new File(binDir, "tokenized/"+f.getName())
				SimpleTokenizerXml tokenizer = new SimpleTokenizerXml(infile, outfile, TokenizerClasses.newTokenizerClasses(project.getPreferencesScope(), lang))
				tokenizer.setRetokenize("true" == project.getTokenizerParameter("doRetokenizeStep", "false"))
				tokenizer.setDoBuildWordIDs("true" == project.getTokenizerParameter("doBuildWordIds", "true"))
				if (ignoredElements != null && ignoredElements.trim().length() > 0) {
					tokenizer.setOutSideTextTagsAndKeepContent(ignoredElements)
				}
				if (!tokenizer.process()) {
					println("Failed to process "+f)
					outfile.delete()
				}
			}
			tokenfiles = FileUtils.listFiles(new File(binDir, "tokenized"))
			cpb2.done()
		} else {
			tokenfiles = okfiles
		}

//		Change XML tag "text" to "textunit"
//		for (File f : tokenfiles) {
//			if (!RenameTag.rename(f, "text", "textunit")) {
//				println "Failed to rename <text> tag to <textunit> in file "+f
//				return false;
//			}
//		}
		if (tokenfiles.size() == 0) {
			return false;
		}
		//TRANSFORM INTO XML-TEI-TXM
		println("Building XML-TXM ${tokenfiles.size()} file"+(tokenfiles.size() > 1?"s":""))
		cpb = new ConsoleProgressBar(tokenfiles.size())
		for (File f : tokenfiles) {
			//ArrayList<String> milestones = new ArrayList<String>();
			cpb.tick()
			File file = f
			String txmfile = f.getName()

			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()
			//il faut lister les id de tous les respStmt
			def respId = []
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>()
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
			//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>()
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>()
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>()
			//lance le traitement
			def builder = new Xml2Ana(file)
			builder.setConvertAllAtrtibutes true
			builder.setCorrespondances(correspRef, correspType)
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			//builder.setAddTEIHeader();
			if (!builder.process(new File(txmDir,txmfile))) {
				println("Failed to process "+f)
				new File(txmDir,txmfile).delete()
			}
			if (builder.getMissingWordIDS().size() > 0) {
				println "Warning: some words are missing IDs at : "+builder.getMissingWordIDS().join(", ")
			}
		}
		cpb.done()
		okfiles = FileUtils.listFiles(txmDir)
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "tokenized").deleteDir()
			new File(binDir, "ptokenized").deleteDir()
			new File(binDir, "stokenized").deleteDir()
			new File(binDir, "src").deleteDir()
			new File(binDir, "split").deleteDir()
		}
		
		return okfiles != null && okfiles.size() > 0;
	}

	boolean stopIfMalformed = false;
	public void setStopIfMalformed(boolean b) {
		stopIfMalformed = b;
	}
}
