// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-06-03 15:04:53 +0200 (mer. 03 juin 2015) $
// $LastChangedRevision: 2984 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.hyperprince

import org.txm.scripts.importer.*;
import org.txm.importer.scripts.filters.*;
import org.txm.scripts.*;
import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;
import javax.xml.stream.*;
import java.net.URL;

import org.txm.scripts.filters.CutHeader.*;
import org.txm.scripts.filters.Tokeniser.*;
import org.txm.scripts.filters.TagSentences.*;
import org.txm.scripts.filters.FusionHeader.*;

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 *
 * @author mdecorde
 * simple import in cwb of RGAQCJ
 * structunits : initial, s, lb ...
 * wordproperties : form, pos and n
 */

class importer
{		
	
	/** The align list. */
	HashSet<String>  alignList;
	
	/** The aa. */
	AnalyzeAlignement aa ;

	/**
	 * Run.
	 *
	 * @param rootDir the root dir
	 * @param corpusSrc the corpus src
	 */
	public void run(String rootDir, String corpusSrc)//List<File> srcfiles = [new File(rootDir,"Corpus-Hyperprince_2009-06-10.xml")];
	{
		//String ActionHome = "~/Bureau/trunkToolbox/0.4.6/org.txm.core/GroovyImportScripts/filters";
		
		//-0 split, parceque c'est un corpus et ca a pls header partout partout
		ArrayList<String> milestones = new ArrayList<String>();//the tags which stay milestones
		milestones.add("tagUsage");
		milestones.add("pb");
		milestones.add("lb");
		
		File corpussrc = new File(rootDir,corpusSrc);
		rootDir = corpussrc.getParent()+"/";
		
		new File(rootDir+"ptokenized").deleteDir();
		new File(rootDir+"ptokenized").mkdir();
		new File(rootDir+"tokenized").deleteDir();
		new File(rootDir+"tokenized").mkdir();
		new File(rootDir+"split").deleteDir();
		new File(rootDir+"split").mkdir();
		new File(rootDir+"txm").deleteDir();
		new File(rootDir+"txm").mkdir();
		
		String xslfile = rootDir+"/xsl/splitcorpus.xsl";
		String outfile = rootDir+"split_temp.xml";
		
		aa = new AnalyzeAlignement(corpussrc);
		alignList = aa.process()
		ApplyXsl a = new ApplyXsl(xslfile);
		a.process(corpussrc.getAbsolutePath(),outfile);
		new File(outfile).delete()

		
		List<File> files = new File(rootDir,"split").listFiles();//scan directory split
		//PREPARE EACH FILE TO BE TOKENIZED
		for(File f : files)
		{
			File srcfile = f;
			//new EncodingConverter(srcfile, "iso-8859-1");
			File resultfile = new File(rootDir+"ptokenized",f.getName());
			println("prepare tokenizer file : "+srcfile+" to : "+resultfile );
			
			def builder = new OneTagPerLine(srcfile.toURL(), milestones);
			builder.process(resultfile);
		}
		
		//TOKENIZE FILES
		//Manager<Filter> filterManager = new FilterManager(ActionHome);
		files = new File(rootDir,"ptokenized").listFiles();//scan directory split
		for(File infile : files)
		{
			Sequence S = new Sequence();
			Filter F1 = new CutHeader();
			Filter F6 = new Tokeniser(infile);
			Filter F7 = new TagSentences();
			Filter F11 = new FusionHeader();
			S.add(F1);
			S.add(F6);
			//S.add(F7);
			S.add(F11);
			
			//File infile = new File(rootDir+"tokenized",f.getName()+"-src.xml");
			File xmlfile = new File(rootDir+"tokenized",infile.getName());
			println("Tokenizing "+xmlfile)
			S.setEncodages("UTF-8","UTF-8");
			S.SetInFileAndOutFile(infile.getAbsolutePath(), xmlfile.getAbsolutePath());
			
			Object[] arguments1 = [infile.getParent()+"/"+infile.getName()+"header.xml"];
			F1.SetUsedParam(arguments1);
			Object[] arguments2 = [infile.getParent()+"/"+infile.getName()+"header.xml",F1];
			F11.SetUsedParam(arguments2);
			
			S.proceed();
		}
		
		files = new File(rootDir,"tokenized").listFiles()			
		//TRANSFORM INTO XML-TEI-TXM
		for(File f : files)
		{
			//ArrayList<String> milestones = new ArrayList<String>();
			println("Building xml-tei-txm "+f+ " >> "+f.getName()+".xml")
			File file = f; 
			String txmfile = f.getName().subSequence(0, 2)+".xml";
			
			def correspType = new HashMap<String,String>()
			def correspRef = new HashMap<String,String>()
			
			//il faut lister les id de tous les respStmt
			def respId = [];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String,HashMap<String,String>>();	
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
					//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();
			
			//lance le traitement
			def builder = new Xml2Ana(file);
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			builder.transformFile(rootDir+"txm/",txmfile);
		}
		
		//rename files correctly
		files = new File(rootDir,"txm").listFiles();
		for(File file : files)
		{
			String txmfile = file.getName();
			file = aa.addAlignement(file, alignList);
			txmfile = txmfile.tokenize(".").get(0)+".xml"
			file.renameTo(new File(file.getParent(),txmfile));
		}
	}
}