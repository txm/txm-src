<?xml version="1.0" encoding="UTF-8"?>

<!-- This is a stylesheet that gives a "diplomatic" presentation of old French 
	manuscript transcriptions - abbreviations are resolved and rendered in italics; 
	- agglutinations and deglutinations are not marked (word limits are "normalized"); 
	- Strong punctuation is marked with a dot followed by a capital letter; weak 
	punctuation is marked with a comma. - direct speech is marked with darkblue 
	color; - uncertain readings are marked with grey background color; - text 
	deleted in the original is not visualized; - text corrected in the normalized 
	version is marked with darkred color and followed by "(sic!)" note; - notes 
	are rendered with [*] sign in the text body; the text of the note appears 
	on "mouse-over" and is reproduced in the end. -->

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">
	<xsl:include href="mdv_common.xsl" />

	<xsl:param name="form">
		diplomatique
	</xsl:param>

	<xsl:variable name="mode">
		dipl
	</xsl:variable>

	<xsl:output method="html" encoding="UTF-8" version="4.0"
		indent="yes" />

	<xsl:strip-space elements="*" />

	<!-- <xsl:variable name="title-formal" select="//fileDesc/titleStmt/title[@type='formal']"/> 
		<xsl:variable name="title-normal" select="//fileDesc/titleStmt/title[@type='normal']"/> 
		<xsl:variable name="title-ref" select="//fileDesc/titleStmt/title[@type='reference']"/> 
		<xsl:variable name="author" select="//titleStmt/author"/> -->

	<xsl:template match="//tei:p|//tei:head">
		<p>
			<xsl:if test="@n">
				[ §
				<xsl:value-of select="@n" />
				]&#xa0;
			</xsl:if>
			<xsl:apply-templates mode="dipl" />
		</p>
	</xsl:template>

	<xsl:template match="tei:pb" mode="dipl">
		<xsl:call-template name="pb_common" />
	</xsl:template>

	<xsl:template match="tei:cb" mode="dipl">
		<xsl:call-template name="cb_common" />
	</xsl:template>

	<xsl:template match="tei:note" mode="dipl">
		<xsl:call-template name="note_common" />
	</xsl:template>

	<xsl:template match="tei:q" mode="dipl">
		<span style="background-color:khaki">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:gap" mode="dipl">
		<xsl:call-template name="gap_common" />
	</xsl:template>


	<xsl:template match="//tei:lb[@ed='norm']" mode="dipl">
	</xsl:template>

	<xsl:template match="//tei:lb[not(@ed)]" mode="dipl">
		<xsl:choose>
			<xsl:when test="position()=1 and not(ancestor::tei:seg)" />
			<xsl:when test="preceding::tei:w[1][descendant::bfm:headlb]" />
			<xsl:when test="preceding::tei:w[1][descendant::tei:lb]" />
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//tei:lb[@ed='facs']" mode="dipl">
		<xsl:text>-</xsl:text>
		<br />
	</xsl:template>

	<xsl:template match="//tei:w|//bfm:punct" mode="dipl">
		<span class="word" id="{@xml:id}" title="{@type}">
			<xsl:apply-templates select="descendant::me:dipl"
				mode="dipl" />
			<xsl:if
				test="descendant::tei:sic and not(following-sibling::tei:w[1][descendant::tei:sic])">
				<i>
					<xsl:text> (sic !)</xsl:text>
				</i>
			</xsl:if>
			<xsl:choose>
				<xsl:when
					test="following::*[1][self::bfm:punct][contains(., ',') or contains(., '.') or contains(., ']')or contains(., ')')]" />
				<xsl:when test="@bfm:aggl='elision'">
					'
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>

	<xsl:template match="tei:ex" mode="dipl">
		<xsl:choose>
			<xsl:when test="@cert='high'">
				<span style="color:darkgreen;font-style:italic">
					<xsl:apply-templates mode="dipl" />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:darkred;font-style:italic">
					<xsl:apply-templates mode="dipl" />
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:sic" mode="dipl">
		<span style="color:darkred" title="sic!">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:subst" mode="dipl">
		[
		<xsl:apply-templates mode="dipl" />
		]
	</xsl:template>

	<xsl:template match="tei:del" mode="dipl">
		<span style="color:red;text-decoration:line-through">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:add[not(@seq)]" mode="dipl">
		<span style="color:blue;text-decoration:underline">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:add[@seq]" mode="dipl">
		<span style="color:blue">
			<sup>
				&lt;
				<xsl:value-of select="@seq" />
			</sup>
			<xsl:apply-templates mode="dipl" />
			<sup>
				<xsl:value-of select="@seq" />
				&gt;
			</sup>
		</span>
	</xsl:template>

	<xsl:template match="tei:unclear" mode="dipl">
		<span style="background-color:lightgrey">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:hi[@rend='initiale']" mode="dipl">
		<span style="font-weight:bold;font-size:xx-large">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="//tei:hi[@rend='sup']" mode="dipl">
		<sup>
			<xsl:apply-templates mode="dipl" />
		</sup>
	</xsl:template>



	<!-- <xsl:template match="tei:name"> <xsl:value-of select="@reg"/> </xsl:template> -->
</xsl:stylesheet>
