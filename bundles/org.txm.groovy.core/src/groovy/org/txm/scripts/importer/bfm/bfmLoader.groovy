// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
//
//
// $LastChangedDate: 2016-03-01 10:10:58 +0100 (mar. 01 mars 2016) $
// $LastChangedRevision: 3133 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.bfm;

import javax.xml.stream.XMLStreamReader;
import org.txm.importer.ApplyXsl2;
import org.txm.scripts.importer.bfm.importer;
import org.txm.scripts.importer.bfm.compiler;
import org.txm.scripts.importer.bfm.pager;
import org.txm.objects.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.utils.i18n.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.scripts.importer.*;
import org.w3c.dom.Element
import org.txm.utils.xml.DomUtils;
import org.txm.utils.*
import org.txm.utils.io.*;
import org.txm.importer.*

String userDir = System.getProperty("user.home");
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName();
String basename = corpusname
String rootDir = project.getSrcdir();
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL();
def xslParams = project.getXsltParameters();
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
String page_element = project.getEditionDefinition("default").getPageElement()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

File txmDir = new File(binDir,"txm/$corpusname");
txmDir.deleteDir();
txmDir.mkdirs();

// BFM XPATH PARAMETERS
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(1, "READ XPATH PARAMETERS")
File paramDeclarationFile = new File(srcDir, "import.properties");
Properties metadataXPath = new Properties();
if (paramDeclarationFile.exists() && paramDeclarationFile.canRead()) {
	InputStreamReader input = new InputStreamReader(new FileInputStream(paramDeclarationFile) , "UTF-8");
	metadataXPath.load(input);
	input.close();

	if (!metadataXPath.containsKey("titre"))
		println "Warning: parameters property file does not contain the 'titre' metadata"
	if (!metadataXPath.containsKey("forme"))
		println "Warning: parameters property file does not contain the 'forme' metadata in param values. The default value is 'prose'"
} else {
	println "No '$paramDeclarationFile' file found"
}
// Apply XSL
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(1, "APPLYING XSL")
if (xsl != null && xslParams != null && xsl.trim().length() > 0) {
	if (ApplyXsl2.processImportSources(new File(xsl), srcDir, new File(binDir, "src"), xslParams))
	// return; // error during process
	srcDir = new File(binDir, "src");
	println ""
}

// copy txm files
println "-- VALIDATION - checking XML source files well-formedness"
List<File> srcfiles = FileUtils.listFiles(srcDir);
for (File f : srcfiles) { // check XML format, and copy file into binDir
	if (f.isHidden() || f.getName().equals("import.xml") || f.getName().matches("metadata\\.....?") || f.getName().endsWith(".properties"))
		continue;
	if (ValidateXml.test(f)) {
		FileCopy.copy(f, new File(txmDir, f.getName()));
	} else {
		println "Won't process file "+f;
	}
}

if (FileUtils.listFiles(txmDir) == null) {
	println "No txm file to process"
	return;
}

if (MONITOR != null) MONITOR.worked(1, "IMPORTER")
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
println "-- IMPORTER - Reading source files"
def imp = new importer()
if (!imp.run(srcDir, binDir, txmDir, basename, metadataXPath, project)) {
	println "import process stopped";
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "ANNOTATE")

boolean annotate_status = true;
if (annotate) {
	println "-- ANNOTATE - Running NLP tools - $model model"
	String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
	def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
	if (engine.processDirectory(txmDir, binDir, ["lang":model])) {
		annotate_status = true;
		
		if (project.getCleanAfterBuild()) {
			new File(binDir, "treetagger").deleteDir()
			new File(binDir, "ptreetagger").deleteDir()
			new File(binDir, "annotations").deleteDir()
		}
	} else {
		println "Stop import."
		return false
	}
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(25, "COMPILING")
println "-- COMPILING - Building Search Engine indexes"
def c = new compiler();
if (debug) c.setDebug();
//c.setCwbPath("~/TXM/cwb/bin");
c.setLang(lang);
c.setAnnotationDone(annotate_status)
if (!c.run(project, binDir, txmDir, corpusname, metadataXPath)) {
	println "import process stopped";
	return false;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }

new File(binDir,"HTML/$corpusname").deleteDir();
new File(binDir,"HTML/$corpusname").mkdirs();
if (build_edition) {

	println "-- EDITION"
	if (MONITOR != null) MONITOR.worked(25, "EDITION")
	File outdir = new File(binDir,"/HTML/$corpusname/default/");
	outdir.mkdirs();
	files = c.getOrderedTxmFiles();

	println("Building editions: "+files.size()+" files" );
	for (File txmFile : files) {
		print "."
		String txtname = txmFile.getName();
		int idx = txtname.lastIndexOf(".");
		if(idx > 0) txtname = txtname.substring(0, idx);
		List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);//["'","(","[","{","«"];

		Text t = new Text(project);
		t.setName(txtname);
		t.setSourceFile(txmFile)
		t.setTXMFile(txmFile)
		
		def ed = new pager(txmFile, outdir, txtname, NoSpaceBefore, NoSpaceAfter, wordsPerPage, corpusname, metadataXPath);
		Edition edition = new Edition(t);
		edition.setName("default");
		edition.setIndex(outdir.getAbsolutePath());
		for (i = 0 ; i < ed.getPageFiles().size();) {
			File f = ed.getPageFiles().get(i);
			String wordid = "w_0";
				if (i < ed.getIdx().size()) wordid = ed.getIdx().get(i);
			edition.addPage(""+(++i), wordid);
		}
	}
}
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FINALIZING")

readyToLoad = project.save();