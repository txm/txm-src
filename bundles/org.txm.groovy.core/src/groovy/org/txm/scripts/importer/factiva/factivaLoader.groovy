// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2012-02-13 11:42:24 +0100 (lun., 13 févr. 2012) $
// $LastChangedRevision: 2126 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.factiva;

import javax.xml.stream.XMLStreamReader;
import org.txm.importer.ApplyXsl2;
import org.txm.scripts.importer.Factiva2XmlTxm;
import org.txm.importer.ValidateXml;
import org.txm.scripts.importer.xml.importer;
import org.txm.scripts.importer.xml.compiler;
import org.txm.scripts.importer.xml.pager;
import org.txm.objects.*;
import org.txm.utils.*
import org.txm.utils.io.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;
import org.txm.metadatas.*;
import javax.xml.stream.*;
import org.w3c.dom.Element
import org.txm.utils.xml.DomUtils;

String userDir = System.getProperty("user.home");

def MONITOR;
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName();
String basename = corpusname
String rootDir = project.getSrcdir();
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL();
def xslParams = project.getXsltParameters();
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
String page_element = project.getEditionDefinition("default").getPageElement()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

File txmDir = new File(binDir,"txm/$corpusname");
txmDir.deleteDir();
txmDir.mkdirs();

File propertyFile = new File(rootDir, "import.properties")//default
Properties props = new Properties();
String[] metadatasToKeep;

String textSortAttribute = null;
String paginationElement = null;
boolean normalizeMetadata = false;
String ignoredElements = null;
boolean stopIfMalformed = false;

println "Reading metadata values from:"+propertyFile
if (propertyFile.exists() && propertyFile.canRead()) {
	InputStreamReader input = new InputStreamReader(new FileInputStream(propertyFile) , "UTF-8");
	props.load(input);
	input.close();
	if(props.getProperty("sortmetadata") != null)
		textSortAttribute = props.get("sortmetadata").toString();
	if(props.getProperty("editionpage") != null)
		paginationElement = props.get("editionpage").toString();
	if(props.getProperty("normalizemetadata") != null)
		normalizeMetadata = Boolean.parseBoolean(props.get("normalizemetadata").toString());
	if(props.getProperty("ignoredelements") != null)
		ignoredElements = props.get("ignoredelements").toString();
	if(props.getProperty("stopIfMalformed") != null)
		stopIfMalformed = Boolean.parseBoolean(props.get("stopIfMalformed").toString());

	println "import properties: "
	println " sort metadata: "+textSortAttribute
	println " edition page tag: "+paginationElement
	println " normalize attributes: "+normalizeMetadata
	println " ignored elements: "+ignoredElements
	println " stop if a XML source is malformed: "+stopIfMalformed
}

File allMetadataFile = Metadatas.findMetadataFile(srcDir);

// Apply Factiva2XMLTXM
println "Factiva -> xml/w"
File factivaOutDir = new File(binDir, "src")
new Factiva2XmlTxm().run(srcDir, factivaOutDir.getAbsolutePath());
if (FileUtils.listFiles(factivaOutDir) == null) {
	println "Error: no file to process"
	return;
}
srcDir = factivaOutDir;

// Apply XSL
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(1, "APPLYING XSL")
if (xsl != null && xslParams != null && xsl.trim().length() > 0) {
	if (ApplyXsl2.processImportSources(new File(xsl), srcDir, new File(binDir, "src"), xslParams))
	// return; // error during process
	srcDir = new File(binDir, "src");
	println ""
}

// copy txm files
List<File> srcfiles = FileUtils.listFiles(srcDir);
for (int i = 0 ; i < srcfiles.size() ; i++) {// check XML format, and copy file into binDir
	File f = srcfiles.get(i)
	if (f.getName().equals("import.xml") || f.getName().matches("metadata\\.....?") || f.getName().endsWith(".properties")) {
		srcfiles.remove(i);
		i--;
		continue;
	}
	if (ValidateXml.test(f)) {
		FileCopy.copy(f, new File(txmDir, f.getName()));
	} else {
		println "Won't process file "+f;
	}
}

if (FileUtils.listFiles(txmDir) == null) {
	println "No txm file to process"
	return;
}

//get metadata values from CSV
Metadatas metadatas; // text metadata

if (allMetadataFile != null && allMetadataFile.exists()) {
	println "Trying to read metadata from: "+allMetadataFile
	File copy = new File(binDir, allMetadataFile.getName())
	if (!FileCopy.copy(allMetadataFile, copy)) {
		println "Error: could not create a copy of metadata file "+allMetadataFile.getAbsoluteFile();
		return;
	}
	metadatas = new Metadatas(copy, Toolbox.getMetadataEncoding(), Toolbox.getMetadataColumnSeparator(), Toolbox.getMetadataTextSeparator(), 1)
} else
	println "no metadata file: "+(allMetadataFile != null? ""+allMetadataFile:"none found")

if (MONITOR != null) MONITOR.worked(1, "IMPORTER")
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
println "-- IMPORTER - Reading source files"
def imp = new importer();
imp.doValidation(true) // change this to not validate xml
imp.doTokenize(true) // change this, to not tokenize xml
imp.setStopIfMalformed(stopIfMalformed);
if (!imp.run( srcDir, binDir, txmDir, basename, null, lang, project)) {
	println "import process stopped";
	return;
}

if (MONITOR != null) MONITOR.worked(20, "INJECTING METADATA")
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
println "-- INJECTING METADATA - from csv file: "+allMetadataFile
if (metadatas != null) {
	println("Injecting metadata: "+metadatas.getHeadersList()+" in texts of directory "+new File(binDir,"txm"))
	for (File infile : FileUtils.listFiles(txmDir)) {
		print "."
		File outfile = File.createTempFile("temp", ".xml", infile.getParentFile());
		if (!metadatas.injectMetadatasInXml(infile, outfile, "text", null)) {
			outfile.delete();
		} else {
			if (!(infile.delete() && outfile.renameTo(infile))) println "Warning can't rename file "+outfile+" to "+infile
			if (!infile.exists()) {
				println "Error: could not replace $infile by $outfile"
				return false;
			}
		}
	}
	println ""
}

List<File> files = FileUtils.listFiles(txmDir)
if (files == null || files.size() == 0) {
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(10, "ANNOTATE")

boolean annotationSuccess = false;
if (annotate) {
	println "-- ANNOTATE - Running NLP tools"
	String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
	def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
	if (engine.processDirectory(txmDir, binDir, ["lang":model])) {
		annotationSuccess = true;
		if (project.getCleanAfterBuild()) {
			new File(binDir, "treetagger").deleteDir()
			new File(binDir, "ptreetagger").deleteDir()
			new File(binDir, "annotations").deleteDir()
		}
	}
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(25, "COMPILING")
println "-- COMPILING - Building Search Engine indexes"
def c = new compiler();
if(debug) c.setDebug();
//c.setCwbPath("~/TXM/cwb/bin");
c.setOptions(textSortAttribute, normalizeMetadata);
c.setAnnotationSuccess(annotationSuccess)
c.setLang(lang);
if (!c.run(project, binDir, txmDir, corpusname, null, srcfiles, metadatas)) {
	println "import process stopped";
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }

new File(binDir,"HTML/$corpusname").deleteDir();
new File(binDir,"HTML/$corpusname").mkdirs();
if (build_edition) {

	println "-- EDITION - Building edition"
	if (MONITOR != null) MONITOR.worked(20, "EDITION")
	
	File outdir = new File(binDir,"/HTML/$corpusname/default/");
	outdir.mkdirs();
	List<File> filelist = FileUtils.listFiles(txmDir);
	Collections.sort(filelist);
	def second = 0

	println "Paginating "+filelist.size()+" texts"
	ConsoleProgressBar cpb = new ConsoleProgressBar(filelist.size());
	for (File txmFile : filelist) {
		cpb.tick()
		String txtname = txmFile.getName();
		int i = txtname.lastIndexOf(".");
		if(i > 0) txtname = txtname.substring(0, i);

		List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);

		Text t = new Text(project);
		t.setName(txtname);
		t.setSourceFile(txmFile)
		t.setTXMFile(txmFile)

		def ed = new pager(txmFile, outdir, txtname, NoSpaceBefore, NoSpaceAfter, basename, project);
		Edition edition = new Edition(t);
		edition.setName("default");
		edition.setIndex(outdir.getAbsolutePath());

		for (i = 0 ; i < ed.getPageFiles().size();) {
			File f = ed.getPageFiles().get(i);
			String wordid = "w_0";
				if (i < ed.getIdx().size()) wordid = ed.getIdx().get(i);
			edition.addPage(""+(++i), wordid);
		}
	}
	cpb.done()
}

if (MONITOR != null) MONITOR.worked(20, "FINALIZING")
	
readyToLoad = project.save();