// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS  FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2011-11-01 16:12:36 +0100 (mar., 01 nov. 2011) $
// $LastChangedRevision: 2049 $
// $LastChangedBy: sheiden $
//

package org.txm.scripts.importer.transcriber
import java.text.DecimalFormat;

import org.txm.utils.FileUtils

import groovy.xml.XmlParser
// parameters


formater = DecimalFormat.getInstance(Locale.ENGLISH)

def getTurnDuration(element) // end < start
{
	float d = 0;
	
		def timeS = element.attributes()["startTime"]
		def time =  Float.parseFloat(timeS)
		def timeE = element.attributes()["endTime"]
		def time2 =  Float.parseFloat(timeE)
		
		d = time2 - time;
		
		String content = element.text()
		if (content.length() > 0) {
			return d
		} else {
			return 0.0f;
		}
}

public float getTotalTime(File infile) {
	def timeResolution = 0.001
	URL u = infile.toURI().toURL()
	InputStream ins = u.openStream()

	// Open input file
	def slurper = new XmlParser();
	slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
	def trs = slurper.parse(infile.toURI().toString())

	float total = 0.0f;
	// Then fix all <Sync>s of Turns
	for (def section : trs.Episode.Section) {
		
		section.Turn.each{ turn ->
			int t = getTurnDuration(turn);
			total += t
		}
	}

	println "T "+infile.getName()+" : "+total
	return total
}


/// MAIN ///

String userdir = System.getProperty("user.home")
//File inDirectory = new File(userdir, "xml/cedre") // 
File inDirectory = new File(userdir, "xml/leman/final 2012-03-26")

float totalDirectory = 0;
for (File f : FileUtils.listFiles(inDirectory)) {
	if (f.getName().endsWith(".trs")) {
		float t = getTotalTime(f);
		totalDirectory += t;
	}
}

println "T "+inDirectory.getName()+" : "+totalDirectory+" s "+" -> "+(totalDirectory/60)+" m" +" -> "+(totalDirectory/60/60)+" h"
