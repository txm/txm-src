package org.txm.scripts.importer.cqp;
//Copyright © - ANR Textométrie - http://textometrie.ens-lyon.fr
//
//This file is part of the TXM platform.
//
//The TXM platform is free software: you can redistribute it and/or modif y
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//The TXM platform is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
//
//
//$LastChangedDate: 2012-06-01 17:47:31 +0200 (ven., 01 juin 2012) $
//$LastChangedRevision: 2185 $
//$LastChangedBy: mdecorde $
//

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.stream.*;
import java.net.URL;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

/** Build CQP corpus simple edition from a CQP file. 
 * 
 * @author mdecorde
 */
class pager {
	List<String> NoSpaceBefore;

	/** The No space after. */
	List<String> NoSpaceAfter;

	/** The wordcount. */
	int wordcount = 0;

	/** The pagecount. */
	int pagecount = 0;

	/** The wordmax. */
	int wordmax = 0;

	/** The basename. */
	String basename = "";
	String txtname = "";
	File outdir;

	/** The wordid. */
	String wordid;

	/** The first word. */
	boolean firstWord = true;

	/** The wordvalue. */
	String wordvalue;

	/** The interpvalue. */
	String interpvalue;

	/** The lastword. */
	String lastword = " ";

	/** The wordtype. */
	String wordtype;

	/** The flagform. */
	boolean flagform = false;

	/** The flaginterp. */
	boolean flaginterp = false;

	/** The url. */
	private def url;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The writer. */
	OutputStreamWriter writer;

	/** The pagedWriter. */
	OutputStreamWriter pagedWriter = null;

	/** The cqpFile. */
	File cqpFile;

	/** The outfile. */
	File outfile;

	/** The pages. */
	def pages = [:];

	/** The idxstart. */
	def idxstart = [:]
	String editionPage;
	ArrayList<Integer> splitTUs; // contains the tu ids used to split pages
	boolean shouldSplit = false;
	boolean useSplitTUs = false;
	boolean hasWordId = false;
	int noWordIdProperty = -1;
	/**
	 * Instantiates a new pager.
	 *
	 * @param cqpFile the cqpFile
	 * @param outfile the outfile
	 * @param NoSpaceBefore the no space before
	 * @param NoSpaceAfter the no space after
	 * @param max the max
	 * @param basename the basename
	 */
	pager(File cqpFile, File outdir, List<String> NoSpaceBefore,
	List<String> NoSpaceAfter, int max, String basename, boolean hasWordId, List pAttrs) {
		this.editionPage = editionPage;
		this.basename = basename;
		this.txtname = "text1";
		this.outdir = outdir;
		this.wordmax = max;
		this.NoSpaceBefore = NoSpaceBefore;
		this.NoSpaceAfter = NoSpaceAfter;
		this.cqpFile = cqpFile;
		this.hasWordId = hasWordId;
		if (hasWordId) {
			noWordIdProperty = pAttrs.indexOf("id") + 1;
			if (noWordIdProperty == 0)
			hasWordId = false;
		}
		
		process();
	}

	private void closeMultiWriter()
	{
		if (pagedWriter != null) {
			if (firstWord) { // there was no words
				this.idxstart[txtname] = ["w_0"]
				pagedWriter.write("<span id=\"w_0\"/>");
			}
			pagedWriter.write("</p>\n")
			pagedWriter.write("</body>");
			pagedWriter.write("</html>");
			pagedWriter.close();
		}
	}

	/**
	 * Creates the next output.
	 *
	 * @return true, if successful
	 */
	private boolean createNextOutput()
	{
		wordcount = 0;
		shouldSplit = false;
		try {
			closeMultiWriter();
			File outfile = new File(outdir, txtname+"_"+(++pagecount)+".html")
			//println "outfile: "+outfile
			if (pages[txtname] == null) pages[txtname] = []
			pages[txtname] << outfile;
			firstWord = true; // waiting for next word

			pagedWriter = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(outfile)) , "UTF-8");
			pagedWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			pagedWriter.write("<!DOCTYPE html>\n")
			pagedWriter.write("<html>");
			pagedWriter.write("<head>");
			pagedWriter.write("<title>"+basename.toUpperCase()+" $txtname Edition - Page "+pagecount+"</title>");
			pagedWriter.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"tmx.css\"/>");
			pagedWriter.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>");
			pagedWriter.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>");
			pagedWriter.write("</head>");
			pagedWriter.write("<body>");

			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Creates the output.
	 *
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean createOutput() {
		try {
			return createNextOutput();
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Gets the page files.
	 *
	 * @return the page files
	 */
	public def getPageFiles() {
		return pages;
	}

	/**
	 * Gets the idx.
	 *
	 * @return the idx
	 */
	public def getIdx() {
		return idxstart;
	}

	def texts = []

	/**
	 * Process.
	 */
	void process() {

		String localname = "";
		String lastword= "";
		createNextOutput();
		int wcounter = 1;
		int txtwcounter = 1;

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(cqpFile) , "UTF-8"));
		String line = reader.readLine();
		int cline = 0;
		while (line != null) {
			pagedWriter.flush();
			cline++;
			//println "line: "+line
			if (line.startsWith("<")) {
				if (line.startsWith("<lb/") || line.startsWith("<br/") || line.startsWith("</p>") || 
					line.startsWith("<lb>") || line.startsWith("<br>")) {
					pagedWriter.write("<br/>");
				} 
				if (line.startsWith("<text>") || line.startsWith("<text ")) {
					String tmp = line;
					int idx = tmp.indexOf("id=\"");
					if (idx > 0) {
						tmp = tmp.substring(idx+4);
						//println "tmp1: "+tmp
						int idx2 = tmp.indexOf("\"");
						//println "tmp2: "+tmp
						if (idx2 > 0) {
							tmp = tmp.substring(0, idx2);
							txtname = tmp
							texts << txtname;
							pagecount = 0;
							createNextOutput();
						}
					}
				} else if (line.startsWith("<s>") || line.startsWith("<s ")) {
					pagedWriter.write(line.replaceAll("<s", "<sent "));
				} else if (line.startsWith("</s>")) {
					pagedWriter.write("</sent>");
				}  else if (line.startsWith("<pb/>") || line.startsWith("<pb>")) {
					pagedWriter.write(line);
					txtwcounter = 1;
					createNextOutput();
				} else {
					pagedWriter.write(line);
				}
			} else { // word
				if (txtwcounter > wordmax) {
					txtwcounter = 1;
					createNextOutput();
				}
				def split = line.split("\t");
				if (split == null || split.size() == 0) {
					// empty line
				} else {
					String wordid;
					if (hasWordId) {
						if (split.size() <= noWordIdProperty) { // +1 since line contains word
							println "Error: line $cline"
						} else {
							wordid = split[noWordIdProperty];
						}
					} else {
						wordid = "w_"+(wcounter++);
					}
					txtwcounter++;
					String wordvalue = split[0];

					if (firstWord) {
						firstWord = false;
						if (this.idxstart[txtname] == null) {
							this.idxstart[txtname] = []
						}
						this.idxstart[txtname] << [wordid];
					}

					int l = lastword.length();
					String endOfLastWord = "";
					if (l > 0) {
						endOfLastWord = lastword.subSequence(l-1, l);
					}

					String interpvalue = "";
					if (split.size() > 1) {
						interpvalue = split[1..split.size()-1].join(" ")
						interpvalue = interpvalue.replace("&", "&amp;").replace("<", "&lt;").replace("\"","&quot;");
					}


					if (NoSpaceBefore.contains(wordvalue) ||
					NoSpaceAfter.contains(lastword) ||
					wordvalue.startsWith("-") ||
					NoSpaceAfter.contains(endOfLastWord)) {
						pagedWriter.write("<span title=\""+interpvalue+"\" id=\""+wordid+"\">");
					} else {
						pagedWriter.write(" <span title=\""+interpvalue+"\" id=\""+wordid+"\">");
					}

					pagedWriter.write(wordvalue.replace("&", "&amp;").replace("\"","&quot;").replace("<", "&lt;")+"</span>\n");
					pagedWriter.flush()
					lastword = wordvalue;
				}
			}
			line = reader.readLine();
		}

		closeMultiWriter();
	}

	def getTextNames() {
		return texts;
	}
}
