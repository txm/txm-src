// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
// $LastChangedDate: 2016-05-11 08:38:47 +0200 (mer. 11 mai 2016) $
// $LastChangedRevision: 3211 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.xmltxm

import javax.xml.stream.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.io.File;
import org.txm.scripts.importer.*;
import org.txm.scripts.importer.xmltxm.compiler;
import org.txm.scripts.importer.xml.pager;
import org.txm.objects.*;
import org.txm.utils.*
import org.txm.utils.io.*;
import org.txm.*;
import org.txm.core.engines.*;

import org.txm.importer.scripts.xmltxm.*;
import org.txm.metadatas.*;
import org.txm.utils.i18n.*;
import org.txm.utils.xml.*
import org.w3c.dom.Element;
import org.txm.importer.*

String userDir = System.getProperty("user.home");
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName();
String basename = corpusname
String rootDir = project.getSrcdir();
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL();
def xslParams = project.getXsltParameters();
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
String page_element = project.getEditionDefinition("default").getPageElement()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

File txmDir = new File(binDir,"txm/$corpusname");
txmDir.deleteDir();
txmDir.mkdirs();

// Apply XSL
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(1, "APPLYING XSL")
if (xsl != null && xslParams != null && xsl.trim().length() > 0) {
	if (ApplyXsl2.processImportSources(new File(xsl), srcDir, new File(binDir, "src"), xslParams))
	// return; // error during process
	srcDir = new File(binDir, "src");
	println ""
}

// copy txm files
println "Copying XML-TXM files..."
List<File> srcfiles = FileUtils.listFiles(srcDir);
for (File f : srcfiles) {// check XML format, and copy file into binDir
	if (f.isHidden() || f.getName().equals("import.xml") || f.getName().matches("metadata\\.....?") || f.getName().endsWith(".properties"))
		continue;
	if (ValidateXml.test(f)) {
		FileCopy.copy(f, new File(txmDir, f.getName()));
	} else {
		println "Won't process file "+f;
	}
}

def files = FileUtils.listFiles(txmDir);
if (files == null || files.size() == 0) {
	println "No txm file to process"
	return;
}

//sorting files per metadata "date" with format
def dates = [:]
def useDates = false
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
for (File infile : files) { // get dates
	String str = GetAttributeValue.process(infile, "/TEI/text", "date")

	if (str != "N/A") {
		dates[infile] = simpleDateFormat.parse(str)
		useDates = true
	}
}

//sort
if (useDates) {
	files.sort() { f-> dates[f] }
} else {
	files.sort()
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(25, "COMPILING")
println "-- COMPILING - Building Search Engine indexes"
def c = new compiler();
if (debug) c.setDebug();
c.setLang(lang);
if (!c.run(project, binDir, txmDir, basename, corpusname, Arrays.asList(files))) {
	println "import process stopped";
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }

new File(binDir,"HTML/$corpusname").deleteDir();
new File(binDir,"HTML/$corpusname").mkdirs();
if (build_edition) {

	if (MONITOR != null) MONITOR.worked(20, "EDITION")
	println "-- EDITION - Building edition"
	
	File outdir = new File(binDir,"/HTML/$corpusname/default/");
	outdir.mkdirs();

	def second = 0

	println "Paginating text: "
	ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
	for (File txmFile : files) {

		String txtname = txmFile.getName();
		int i = txtname.lastIndexOf(".");
		if(i > 0) txtname = txtname.substring(0, i);

		List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(lang);
		List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(lang);

		Text t = new Text(project);
		t.setName(txtname);
		t.setSourceFile(txmFile)
		t.setTXMFile(txmFile)

		if (second) { print(", ") }
		if (second > 0 && (second++ % 5) == 0) println ""
		cpb.tick()

		def ed = new pager(txmFile, outdir, txtname, NoSpaceBefore, NoSpaceAfter, basename, project);
		Edition edition = new Edition(t);
			edition.setName("default");
			edition.setIndex(outdir.getAbsolutePath());
		for (i = 0 ; i < ed.getPageFiles().size();) {
			File f = ed.getPageFiles().get(i);
			String wordid = "w_0";
				if (i < ed.getIdx().size()) wordid = ed.getIdx().get(i);
			edition.addPage(""+(++i), wordid);
		}
	}
	cpb.done()
}
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FINALIZING")
readyToLoad = project.save();