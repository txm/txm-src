// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.corptef


import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.BuildTTSrc;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;

import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class compiler.
 */
class compiler
{

	/** The debug. */
	private boolean debug= false;

	/** The input data. */
	private def inputData;

	/** The factory. */
	private def factory;

	/** The parser. */
	private XMLStreamReader parser;

	/** The dir. */
	private def dir;

	/** The output. */
	private def output;

	/** The url. */
	private def url;

	/** The anahash. */
	private HashMap<String,String> anahash =new HashMap<String,String>() ;

	/** The text. */
	String text="";

	/** The base. */
	String base="";

	/** The project. */
	String project="";

	/** The lang. */
	private String lang ="fr";

	/**
	 * initialize.
	 *
	 */
	public compiler(){}

	/**
	 * Instantiates a new compiler.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 */
	public compiler(URL url,String text,String base, String project)
	{
		this.text = text
		this.base = base;
		this.project = project;
		try {
			this.url = url;
			inputData = url.openStream();

			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}

	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8");
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * Go to text.
	 */
	private void GoToText()
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			if(event == XMLStreamConstants.END_ELEMENT)
				if(parser.getLocalName().equals("teiHeader"))
					return;
		}
	}

	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(String dirPathName, String fileName)
	{
		createOutput(dirPathName, fileName);

		String headvalue=""
		String vAna = "";
		String vForm = "";
		String wordid= "";
		String vHead = "";

		int p_id = 0;
		int q_id = 0;
		int body_id = 0;
		int front_id = 0;
		int back_id = 0;
		String lb_id = 0;
		int lb_count = 0;
		String pb_id = 0;
		int pb_count = 0;
		String ab_id = 0;

		boolean captureword = false;

		String vExpan = "";
		String vCorr = "";
		String vReg = "";
		String vOrig = "";
		String vSic = "";
		String vAbbr = "";

		boolean flaglg = false;
		boolean flaghead = false;
		boolean flagAuthor = false;
		boolean flagDate = false;
		boolean flagWord = false;
		boolean flagForm = false;
		boolean flagAna = false;

		boolean flagchoice = false;
		boolean flagcorr = false;
		boolean flagsic = false;
		boolean flagreg = false;
		boolean flagexpan = false;
		boolean flagorig = false;
		boolean flagabbr = false;
		boolean flagfw = false;

		File xpathfile = new File(url.getFile());
		String titreId = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='reference']/text()");
		String auteur = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/text()");
		String datecompo = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo']/@when");
		String ssiecle = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:date[@type='compo_sous_siecle']/@n");
		String domaine = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:textDesc/tei:domain/@type");
		String genre = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:textDesc/@n");
		String forme = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:catRef/@target[contains(.,'forme')]").substring(1);
		String dialecte = XPathResult.getXpathResponse(xpathfile,"tei:TEI/tei:teiHeader/tei:profileDesc/tei:creation/tei:region[@type='dialecte_auteur']/text()");

		this.GoToText()
		output.write("<txmcorpus lang=\""+lang+"\">\n");
		try
		{
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
			{
				switch (event)
				{
					case XMLStreamConstants.START_ELEMENT:
						switch (parser.getLocalName())
						{
							case "text":
							output.write("<text id=\""+text+"\"" +
							" titre=\""+titreId+"\"" +
							" auteur=\""+auteur+"\"" +
							" datecompo=\""+datecompo+"\"" +
							" ssiecle=\""+ssiecle+"\"" +
							" domaine=\""+domaine+"\"" +
							" genre=\""+genre+"\"" +
							" forme=\""+forme+"\"" +
							" dialecte=\""+dialecte+"\"" +
							" base=\""+base+"\"" +
							" project=\""+project+"\">\n");
							captureword=true;
							break;

							case "div":
							//output.write("<div type=\""+parser.getAttributeValue(null,"type")+"\">\n");
							break;

							case "p":
							output.write("<p n=\""+(p_id++)+"\">\n");
							break;
							case "ab":
							output.write("<ab n=\""+ab_id+++"\" rend=\""+parser.getAttributeValue(null,"rend")+"\">\n");
							break;
							case "q":
							output.write("<q n=\""+(q_id++)+"\">\n");
							break;
							case "front":
							output.write("<front n=\""+(front_id++)+"\">\n");
							break;
							case "body":
							output.write("<body n=\""+(body_id++)+"\">\n");
							break;
							case "back":
							output.write("<back n=\""+(back_id++)+"\">\n");
							break;
							case "lb":
							String n = parser.getAttributeValue(null,"n")
							if(n != null)
								lb_id = n;
							else
								lb_id =""+lb_count++;
							break;
							case "pb":
							String n = parser.getAttributeValue(null,"n");
							if(n != null)
								pb_id = n;
							else
								pb_id = ""+(pb_count++);

							break;

							case "choice":
							flagchoice = true;
							break;
							case "corr":
							flagcorr = true;
							vCorr= "";
							break;
							case "reg":
							flagreg = true;
							vReg= "";
							break;
							case "expan":
							flagexpan = true;
							vExpan= "";
							break;
							case "orig":
							flagreg = true;
							vOrig= "";
							break;
							case "sic":
							flagsic = true;
							vSic= "";
							break;
							case "abbr":
							flagreg = true;
							vAbbr= "";
							break;

							case "w":
							for(int i = 0 ; i < parser.getAttributeCount(); i++)
								if(parser.getAttributeLocalName(i).equals("id"))
							{
								wordid = parser.getAttributeValue(i);
							}
							vAna = "";
							vForm = "";
							flagWord = true;
							break;
							case "form":
							flagForm = true;
							vForm = "";
							vAna ="";
							break;

							case "ana":
							flagAna = true;
							break;
						}
						break;

					case XMLStreamConstants.END_ELEMENT:
						switch (parser.getLocalName())
						{
							case "div":
							break;

							case "text":
							output.write("</text>\n");
							captureword=false;
							break;
							case "p":
							output.write("</p>\n");
							break;
							case "ab":
							output.write("</ab>\n");
							break;
							case "q":
							output.write("</q>\n");
							break;
							case "front":
							output.write("</front>\n");
							break;
							case "body":
							output.write("</body>\n");
							break;
							case "back":
							output.write("</back>\n");
							break;

							case "fw":
							flagfw = false;
							break;
							case "choice":

							if(vCorr != "")
							{
								//System.out.println(vCorr+" >> write corr "+vForm +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr)
								output.write( vCorr +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
							}
							else if(vReg != "")
							{
								//System.out.println("write reg "+vForm)
								output.write( vReg +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
							}
							else if(vExpan != "")
							{
								//System.out.println("write expan "+vForm)
								output.write( vExpan +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
							}
							flagchoice = false;
							vCorr= "";
							vSic= "";
							break;
							case "corr":
							flagcorr = false;

							break;
							case "reg":
							flagreg = false;
							vReg = "";
							break;
							case "expan":
							flagexpan = false;
							vExpan= "";
							break;
							case "orig":
							flagreg = false;
							vOrig= "";
							break;
							case "sic":
							flagsic = false;

							break;
							case "abbr":
							flagreg = false;
							vAbbr= "";
							break;

							case "w":
							if(vAna != null)
								if(captureword)
							{
								if(flagchoice)
								{

								}
								else if(flagfw)
								{

								}
								else
								{
									output.write( vForm +"\t"+vAna+wordid+"\t"+pb_id+"\t"+lb_id+"\t"+vOrig+"\t"+vSic+"\t"+vAbbr+"\n");
								}

							}

							flagWord = false;
							break;

							case "form":
							flagForm = false;
							break;

							case "ana":
							vAna += "\t";
							flagAna = false;
							break;
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						if(flagAna)
						{
							vAna += parser.getText().trim();
						}

						if(flagForm)
						{
							vForm += parser.getText().trim();
							if(flagchoice)
							{
								if(flagsic)
								{
									vSic += parser.getText().trim();
								}
								if(flagorig)
								{
									vOrig += parser.getText().trim();
								}
								if(flagabbr)
								{
									vAbbr += parser.getText().trim();
								}
								if(flagcorr)
								{
									vCorr += parser.getText().trim();
								}
							}
						}
				}
			}
			output.write("</txmcorpus>");
			output.close();
			parser.close();
			inputData.close();
		}
		catch (XMLStreamException ex) {
			System.out.println(ex);
		}
		catch (IOException ex) {
			System.out.println("IOException while parsing " + inputData);
		}

		return true;
	}



	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @return true, if successful
	 */
	public boolean run(File rootDirFile)
	{
		String rootDir =rootDirFile.getAbsolutePath();

		if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
			println ("Error: CWB executables not well set.")
			return false;
		}
		if(!new File(rootDir).exists())
		{
			println ("binary directory does not exists: "+rootDir)
			return false;
		}
		new File(rootDir+"/cqp/","corptef.cqp").delete();//cleaning&preparing
		new File(rootDir,"/cqp/").deleteDir();
		new File(rootDir,"/cqp/").mkdir();
		new File(rootDir,"/data/").deleteDir();
		new File(rootDir,"/data/").mkdir();
		new File(rootDir,"registry/").mkdir();

		String textid="";
		int counttext =0;
		List<File> files = new File(rootDirFile,"txm").listFiles();
		Collections.sort(files);
		//1- Transform into CQP file
		for(File f : files)
		{
			counttext++;
			if(!f.exists())
			{
				println("file "+f+ " does not exists")
			}
			else
			{
				println("process file "+f)
				String txtname = f.getName().substring(0,f.getName().length()-4);
				def builder = new compiler(f.toURL(),txtname, "corptef", "default");
				builder.setLang lang
				builder.transfomFileCqp(rootDir+"/cqp","corptef.cqp");
			}
		}

		//2- Import into CWB
		def outDir = rootDir;
		CwbEncode cwbEn = new CwbEncode();
		cwbEn.setDebug(debug);
		CwbMakeAll cwbMa = new CwbMakeAll();
		cwbMa.setDebug(debug);
		String[] pAttributes = ["ttpos","ttlemme","id","pb","lb","orig","sic","abbr"];
		String[] sAttributes = ["txmcorpus:0+lang", "text:0+id+titre+auteur+datecompo+ssiecle+domaine+genre+forme+dialecte+base+project","front:0+n","body:0+n","ab:0+n+rend","div:0+id+type","p:0+n","back:0+n"];
		try {

			String regPath = new File(outDir , "registry/corptef").getAbsolutePath();
			cwbEn.run(
					new File(outDir, "/data").getAbsolutePath(),
					new File(outDir, "cqp/"+"corptef.cqp").getAbsolutePath(),
					regPath,
					pAttributes, sAttributes);
			if (!new File(regPath).exists()) {
				println "Error: The registry file was not created: $regPath. See https://groupes.renater.fr/wiki/txm-users/public/faq"
				return false;
			}
			cwbMa.run(
					"CORPTEF",
					new File(outDir, "registry").getAbsolutePath());
		} catch (Exception ex) {System.out.println(ex); return false;}
		// "C:\Documents and Settings\alavrent\TXM\cwb\bin\cwb-encode" -d "C:\Documents and Settings\alavrent\TXM\corpora\corptef\data" -f "C:\Documents and Settings\alavrent\TXM\corpora\corptef\cqp\corpte.cqp" -R "C:\Documents and Settings\alavrent\TXM\corpora\corptef\registry\corptef" -c utf8 -xsB -xsB -P ttpos -P ttlemme -P id -P pb -P lb -P orig -P sic -P abbr -S text:0+id+titre+auteur+datecompo+ssiecle+domaine+genre+forme+dialecte+base+project -S front:0+n -S body:0+n -S ab:0+n+rend -S div:0+id+type -S p:0+n -S back:0+n
		System.out.println("Done.")

		return true;
	}

	/**
	 * Sets the debug.
	 */
	public void setDebug()
	{
		this.debug = true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("C:/TXM/corpora/corptef/");
		def c = new compiler();
		c.setDebug();
		c.setCwbPath("C:/Program Files/TXM/cwb/bin");
		c.run(dir);
	}
}
