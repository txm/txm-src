package org.txm.scripts.importer.xmltxmpara

import javax.xml.stream.*;
import java.net.URL;

class ValidateXmlTXM {
	
	public static String XMLNS = "http://www.w3.org/XML/1998/namespace";
	public static String TXMNS = "http://textometrie.org/1.0";
	public static String TEINS = "http://www.tei-c.org/ns/1.0";
	

	boolean checkteiCorpus = false;
	boolean checkTEI = true;
	boolean checkAlignStruct = false;
	boolean checkW = true;

	boolean hasTeiCorpus = false;
	boolean hasTeiCorpusID = false;
	boolean hasTeiCorpusHeader = false;
	boolean hasTeiCorpusHeaderAppDesc = false;
	boolean hasTeiCorpusHeaderVersion = false;
	int hasTei = 0;
	int hasTeiID = 0;
	boolean hasAlignStruct = false
	String alignStruct = "";
	boolean hasTXMW = false;

	public boolean validate(File sourceDir)
	{
		boolean ret = true;
		//get infos from import.xml if any
		File alignxml = sourceDir.listFiles().find{it.getName() == "align.xml"}
		if(alignxml != null)
		{
			infosFromImportXML(alignxml)
			checkteiCorpus = true;
			// checkAlignStruct = true;
			// mainLang = ...
			// corpusnames = ...
			// alignStructs = 
		}
		
		sourceDir.eachFileMatch(~/.+.xml/){f->

			if (f.getName() != "import.xml" && f.getName() != "align.xml" && f.isFile())
				ret = ret & validateXMLTXM(f);
		}

		return ret;
	}
	
	HashMap<String, ArrayList<String>> links = [:];
	protected boolean infosFromImportXML(File alignxml)
	{
		this.url = xmlfile.toURI().toURL();;
		inputData = url.openStream();

		factory = XMLInputFactory.newInstance();
		parser = factory.createXMLStreamReader(inputData);
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			if(event == XMLStreamConstants.START_ELEMENT)
			{
				if(parser.getLocalName() == "link")
				{
					links.put(parser.getAttributeValue(null, "target"), 
						[parser.getAttributeValue(null, "alignElement"), parser.getAttributeValue(null, "alignLevel")])
					
				}
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		println "get infos from $alignxml"
		return true;
	}
	
	private void printAttributes(def parser)
	{
		for(int i = 0 ; i < parser.getAttributeCount(); i++)
		println( "$path "+parser.getAttributeLocalName(i)+
		 " = "+ parser.getAttributeValue(i))
	}
	
	def url;
	def inputData;
	def factory;
	XMLStreamReader parser;
	String path = ""
	protected boolean validateXMLTXM(File xmlfile)
	{
		println "validate $xmlfile"
		boolean inTeiCorpus = false;
		boolean inTeiHeader = false;
		boolean inMetadata = false;
		boolean inTEI = false;
		boolean inW = false;
		boolean inForm = false;

		try {
			this.url = xmlfile.toURI().toURL();;
			inputData = url.openStream();

			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		
		String localname = ""
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
		{
			if(event == XMLStreamConstants.START_ELEMENT)
			{
				localname = parser.getLocalName();
				path += "/"+localname
				switch (localname)
				{
					case "teiCorpus":
						inTeiCorpus = true;
						hasTeiCorpus = true;
						//printAttributes(parser);
						if(parser.getAttributeValue(XMLNS, "id") != null)
							hasTeiCorpusID = true;
						break;
					case "teiHeader":
						inTeiHeader = true;
						if(inTeiCorpus)
							hasTeiCorpusHeader = true;
						break;
					case "metadata":
						inMetadata = true;
						//printAttributes(parser);
						if(parser.getAttributeValue(null, "name") == "version")
							hasTeiCorpusHeaderVersion = true;
						break;
					case "TEI":
						inTEI = true;
						hasTei++;
						if(parser.getAttributeValue(XMLNS, "id") != null)
							hasTeiID++;
						break;
					case "w":
						inW = true;
						String id = parser.getAttributeValue(XMLNS, "id");
						if(!(id != null && id.matches("w_.+_.+")))
							hasTXMW = false;
						break;
					case "form":
						inForm = true;
						break;
					case alignStruct:
					if(parser.getAttributeValue(null, "align") == null)
						hasAlignStruct = false;
						break;
				}
			}
			else if(event == XMLStreamConstants.END_ELEMENT)
			{
				localname = parser.getLocalName()
				if(path.lastIndexOf("/") > 0)
					path = path.substring(0, path.lastIndexOf("/"))
				switch (localname)
				{
					case "teiCorpus":
						inTeiCorpus = false;
						break;
					case "teiHeader":
						inTeiHeader = false;
						break;
					case "metadata":
						inMetadata = false;
						break;
					case "TEI":
						inTEI = false;
						break;
					case "w":
						inW = false;
						break;
					case "form":
						inForm = false;
						break;
				}
			}
		}
		if (parser != null) parser.close();
			if (inputData != null) inputData.close();
		} catch (XMLStreamException ex) {
			System.out.println(ex);
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}
		if(checkteiCorpus &
			!(hasTeiCorpus & hasTeiCorpusID & hasTeiCorpusHeader & hasTeiCorpusHeaderVersion))
		{	println "wrong <teiCorpus> format: $hasTeiCorpus & $hasTeiCorpusID & $hasTeiCorpusHeader & $hasTeiCorpusHeaderVersion"
			return false;
		}
		
		if(checkAlignStruct &
			!hasAlignStruct)
		{   println "wrong align structure format: $hasAlignStruct"
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}
		
		if(checkTEI & !(hasTei == hasTeiID))
		{	println "wrong <TEI> format: $hasTei == $hasTeiID"
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
			return false;
		}
		
		return true;
	}
	
	public static void main(String[] args)
	{
		File srcdir = new File("/home/mdecorde/xml/xmltxmpara");
		def checker = new ValidateXmlTXM();
		println checker.validate(srcdir)
	}
}


