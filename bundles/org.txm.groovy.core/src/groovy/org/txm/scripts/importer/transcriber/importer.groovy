// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//

package org.txm.scripts.importer.transcriber

import java.util.ArrayList;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.txm.utils.logger.Log;
import org.txm.xml.XMLProcessor
import org.txm.xml.IdentityHook
import org.txm.xml.LocalNamesHookActivator
import org.txm.importer.scripts.xmltxm.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.stream.XMLStreamException
import javax.xml.xpath.*;

import java.util.HashMap;
import org.txm.scripts.importer.*;
import org.txm.utils.*;
import org.txm.utils.io.*
import org.txm.objects.Project;
import org.txm.metadatas.*;

/**
 * The Class importer.
 */
class importer {

	/** The trans. */
	HashMap<String, String> trans = ["trans":"//Trans"];

	/** The doc. */
	def doc;

	/** The infile. */
	File infile;

	/** The outfile. */
	File outfile;

	/** The outdir. */
	File txmDir;
	File binDir;

	public boolean doTokenizeStep = true;

	/** The trsfiles. */
	ArrayList<String> trsfiles;

	/** The metadatas. */
	Metadatas metadatas;

	String lang; // language used by the tokenizer

	Project project;

	/** The indexInterviewer: index interviewer speech if true. */
	boolean indexInterviewer = true;

	/**
	 * Removes the interviewers.
	 *
	 * @param value the value
	 * @return the java.lang. object
	 */
	public setIndexInterviewer(boolean value) {
		this.indexInterviewer = value;
	}

	/**
	 * Instantiates a new importer.
	 *
	 * @param trsfiles the trsfiles
	 * @param outdir the outdir
	 * @param metadatas the metadatas
	 */
	public importer(ArrayList<File> trsfiles, File binDir, File txmDir, Metadatas metadatas, lang, Project project) {
		this.trsfiles = trsfiles;
		this.txmDir = txmDir;
		this.binDir = binDir;
		this.metadatas = metadatas;
		this.lang = lang;
		this.project = project;
		this.doTokenizeStep = project.getDoTokenizerStep()
	}

	/**
	 * Run.
	 *
	 * @return true, if successful
	 */
	public boolean run() {
		if (trsfiles == null) {
			println "no files to process"
			return false;
		}
		txmDir.mkdir();
		if (!txmDir.exists()) {
			println "can't create txmDir: "+txmDir.getAbsolutePath()
		}

		// TRS -> TEI
		println "Converting TRS to TEI "+trsfiles.size()+" files"
		ConsoleProgressBar cpb = new ConsoleProgressBar(trsfiles.size())
		for (File infile : trsfiles) {
			cpb.tick()
			String textid = FileUtils.stripExtension(infile);
			File outfile = new File(txmDir, textid+".xml")
			TRSToTEI p = new TRSToTEI(infile);
			if (!p.process(outfile)) {
				println "Error while converting TRS to TEI: "+infile
				//return false;
			}
		}
		cpb.done()

		if (metadatas != null) {

			if (metadatas.getHeadersList().size() > 0) {

				println "Injecting metadata "+metadatas.getHeadersList()+" in "+trsfiles.size()+" files"

				trsfiles = FileUtils.listFiles(txmDir);
				cpb = new ConsoleProgressBar(trsfiles.size())
				for (File infile : trsfiles) {
					File outfile = new File(txmDir, "tmp.xml")
					if (metadatas != null && metadatas.isInitialized()) {
						cpb.tick()
						if (!metadatas.injectMetadatasInXml(infile, outfile, "text")) {
							println("Failed to inject metadata in "+infile)
							//outfile.delete()
						}
						if (!infile.delete()) {
							println "ERROR: could not delete $infile"
							return false
						}
						outfile.renameTo(infile)
					}
				}
				cpb.done()
			}
		}


		// TOKENIZER ENTITIES
		def files = FileUtils.listFiles(txmDir);
		println "Tokenizing entities "+files.length+" files"
		cpb = new ConsoleProgressBar(files.length)
		for (File pfile : files) {

			cpb.tick()
			TokenizeEntities tokenizer = new TokenizeEntities(pfile.toURI().toURL());
			File outfile = File.createTempFile("tok", ".xml", pfile.getParentFile());
			if (tokenizer.process(outfile)) {
				if (!(pfile.delete() && outfile.renameTo(pfile))) println "Warning can't rename file "+outfile+" to "+pfile
			}
			outfile.delete();
		}
		cpb.done()

		//TOKENIZE
		File tokenizedDir = new File(binDir, "tokenized")
		tokenizedDir.deleteDir() // delete previous outputed files
		tokenizedDir.mkdir()

		if (!doTokenizeStep) {

			println "No tokenization to do."
			cpb = new ConsoleProgressBar(files.length)
			for (File f : files) {
				File outfile = new File(tokenizedDir, f.getName());
				FileCopy.copy(f, outfile);
				cpb.tick()
			}
			cpb.done()
		} else {

			println "Tokenizing "+files.length+" files"
			cpb = new ConsoleProgressBar(files.length)
			for (File pfile : files) {
				cpb.tick()
				String filename = pfile.getName().substring(0, pfile.getName().length()-4)
				File tfile = new File(tokenizedDir, pfile.getName())
				try {
					TranscriberTokenizer tokenizer = new TranscriberTokenizer(pfile, tfile, lang)
					tokenizer.setRetokenize("true" == project.getTokenizerParameter("doRetokenizeStep", "false"))
					tokenizer.setDoBuildWordIDs("true" == project.getTokenizerParameter("doBuildWordIds", "true"))
					if (!tokenizer.process()) {
						println("Failed to tokenize "+pfile)
					}
				} catch (Exception e) {
					println "Error tokenizer: "+pfile
					org.txm.utils.logger.Log.printStackTrace(e);
					return false;
				}
			}
			cpb.done()
		}

		// REMOVE INTERVIEWERS WORDS tags (not content)
		if (!indexInterviewer) {
			files = FileUtils.listFiles(tokenizedDir)
			println "Remove interviewers words in "+files.length+" files"
			cpb = new ConsoleProgressBar(files.length)
			for (File tfile : files) {
				cpb.tick()
				try {
					File tfile2 = new File(tfile.getAbsolutePath()+".tmp")
					XMLProcessor processor = new XMLProcessor(tfile);

					// tells when to activate the hook
					LocalNamesHookActivator activator = new LocalNamesHookActivator("text", "w", "u", "sp")

					// stax hook

					new IdentityHook("word", activator, processor) {
								boolean removeInterviewerTag = false;
								/** The interviewers regex */
								def interviewers = null

								public boolean _activate() { return true;}

								public boolean deactivate() { return true;}

								protected void processStartElement() throws XMLStreamException, IOException {
									String localname = parser.getLocalName()

									if ("text".equals(localname)) {
										for (int i = 0 ; i < parser.getAttributeCount() ; i ++) {
											if (parser.getAttributeLocalName(i).equals("interviewer-id-regex")) { // interviewer-id-regex
												interviewers = /${parser.getAttributeValue(i)}/
											}
										}
									} else if ("u".equals(localname)) {
										String u_name = parser.getAttributeValue(null, "who")
										removeInterviewerTag = interviewers != null && u_name =~ interviewers
										println "interviewers=$interviewers uname=$u_name removeInterviewerTag=$removeInterviewerTag"
									} else if ("w".equals(localname)) {
										if (removeInterviewerTag) return; // remove the start tag
									}
									
									super.processStartElement();
								}

								protected void processEndElement() throws XMLStreamException {
									String localname = parser.getLocalName()
									if ("u".equals(localname)) {
										removeInterviewerTag = false;
									} else if ("w".equals(localname)) {
										if (removeInterviewerTag) return; // remove the end tag
									}
									
									super.processEndElement();
								}
							}

					if (processor.process(tfile2)) {
						tfile.delete();
						tfile2.renameTo(tfile);
					} else {
						tfile2.delete();
					}
				} catch(Exception e) {
					e.printStackTrace()
				}
			}

			cpb.done()
		}

		//TRANSFORM INTO XML-TEI-TXM
		files = FileUtils.listFiles(tokenizedDir)
		println("Building ${files.length} XML-TXM file"+(files.length > 1?"s":""))
		cpb = new ConsoleProgressBar(files.length)
		for (File tfile : files) {

			cpb.tick()
			String filename = tfile.getName().substring(0, tfile.getName().length()-4)
			File xmlfile = new File(txmDir, tfile.getName())

			def correspType = new HashMap<String,String>()
			correspType.put("event","event");
			correspType.put("audio","audio");
			correspType.put("notation","notation");
			def correspRef = new HashMap<String,String>();
			correspRef.put("event","trs");
			correspRef.put("audio","trs");
			correspRef.put("notation","trs");
			def respId = ["trs"];
			def applications = new HashMap<String,HashMap<String,String>>();
			applications.put("trs",new ArrayList<String>());
			applications.get("trs").add("Transcriber");//app ident
			applications.get("trs").add("");//app version
			applications.get("trs").add(null);//app report file path
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			taxonomiesUtilisees.put("ctx1",["event", "audio", "notation"]);//,"lemma","lasla","grace"]);
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			itemsURI.put("event",new HashMap<String,String>());
			itemsURI.get("event").put("tagset","orth|corr");
			itemsURI.get("event").put("website","");
			itemsURI.put("audio",new HashMap<String,String>());
			itemsURI.get("audio").put("tagset","present|absent|partiel");
			itemsURI.get("audio").put("website","");
			itemsURI.put("notation",new HashMap<String,String>());
			itemsURI.get("notation").put("tagset","");
			itemsURI.get("notation").put("website","");
			def resps = new HashMap<String,String[]>();
			resps.put("trs", ["Transcriber annotations","TXM","",""])
			String wordprefix = "w_";

			Xml2Ana builder = new Xml2Ana(tfile);
			builder.setConvertAllAtrtibutes true;
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			if (!builder.process(xmlfile)) {
				println("Failed to process "+tfile)
				xmlfile.delete();
			}

			if (builder.getMissingWordIDS().size() > 0) {
				println "Warning: some words are missing IDs at : "+builder.getMissingWordIDS().join(", ")
			}
		}

		if (project.getCleanAfterBuild()) {
			new File(project.getProjectDirectory(), "tokenized").deleteDir()
			new File(project.getProjectDirectory(), "src").deleteDir()
			new File(project.getProjectDirectory(), "split").deleteDir()
		}

		cpb.done()

		return FileUtils.listFiles(txmDir) != null;
	}

	/**
	 * Process.
	 *
	 * @param infile the infile
	 * @param outfile the outfile
	 * @param metas the metas
	 * @return true, if successful
	 */
	public boolean process(File infile, File outfile, ArrayList<Pair<String, String>> metas) {
		//inject metadatas into
		this.infile = infile;
		this.outfile = outfile;
		def factory = DocumentBuilderFactory.newInstance()
		factory.setXIncludeAware(true);
		def builder = factory.newDocumentBuilder()
		doc = builder.parse(infile)
		insert(trans.get("trans"), metas);
		return save();
	}

	/**
	 * Insert.
	 *
	 * @param xpath the xpath
	 * @param pairs the pairs
	 */
	public void insert(String xpath, List<Pair<String, String>> pairs) {
		println ("insert $pairs into $xpath")
		def expr = XPathFactory.newInstance().newXPath().compile(xpath)
		def nodes = expr.evaluate(doc, XPathConstants.NODESET)

		for (Node node : nodes) {
			Element elem = (Element)node;
			for (Pair<String, String> p : pairs) {
				elem.setAttribute(p.getFirst(), p.getSecond());
			}
		}
	}

	/**
	 * Save.
	 *
	 * @return true, if successful
	 */
	private boolean save() {
		try {
			// Création de la source DOM
			Source source = new DOMSource(doc);

			// Création du fichier de sortie
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8"));
			Result resultat = new StreamResult(writer);

			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			return true;
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}
}
