// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.importer.transcriber

import java.io.File;
import javax.xml.stream.*;

import org.txm.utils.FileUtils

import java.io.OutputStreamWriter;

// TODO: Auto-generated Javadoc
/**
 * The Class CheckWhos.
 */
class CheckWhos {
	
	/**
	 * Test.
	 *
	 * @param xmlfile the xmlfile
	 * @return true, if successful
	 */
	public static boolean test(File xmlfile)
	{
		boolean ret = true;
		URL url = xmlfile.toURI().toURL();
		def inputData = url.openStream();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		def parser = factory.createXMLStreamReader(inputData);
		
		String localname;
		String vSpeaker;
		
		List<String> localspeakers;
		
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
		{
			switch (event) 
			{
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName();
					switch(localname)
					{
						case "Turn":
						
							vSpeaker = parser.getAttributeValue(null, "speaker");			
							if(vSpeaker == null)
							{
								localspeakers = [];
								vSpeaker = "N/A"
							}
							else
								localspeakers = vSpeaker.split(" ")
							break;
						
						case "Who":
						
							int n = Integer.parseInt(parser.getAttributeValue(null, "nb")) -1;
							if(localspeakers.size() <= n)
							{
								println "$xmlfile: Who error : line "+parser.getLocation().lineNumber+" col "+parser.getLocation().columnNumber+" in file "+xmlfile;
								vSpeaker = "N/A";
								ret = false;
							}
						
							break;
							
					}
			}
		}
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		return ret;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String userdir = System.getProperty("user.home")
		for(File trsfile : FileUtils.listFiles(new File(userdir, "xml/cedre")))
		{
			if(trsfile.getName().endsWith(".trs"))
			{
				if(!CheckWhos.test(trsfile))
					println trsfile.getName()+" not ok"
			}
		}
	}
	
}
