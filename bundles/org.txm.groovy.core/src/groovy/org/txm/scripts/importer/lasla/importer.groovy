// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.lasla

import javax.xml.stream.*

import org.txm.*
import org.txm.scripts.importer.*
import org.txm.importer.scripts.filters.*
import org.txm.objects.*
import org.txm.scripts.*
import org.txm.importer.scripts.xmltxm.*

import org.txm.scripts.filters.CutHeader.*
import org.txm.scripts.filters.FusionHeader.*
import org.txm.scripts.filters.TagSentences.*
import org.txm.scripts.filters.Tokeniser.*

// TODO: Auto-generated Javadoc
/**
 * The Class importer.
 */
class importer {
	
	/**
	 * Run.
	 *
	 * @param rootDirFile the root dir file
	 * @param encoding the encoding
	 * @param basename the basename
	 * @return true, if successful
	 */
	public boolean run(File rootDirFile, String encoding, String basename)
	{
		String filename = rootDirFile.getName();
		String rootDir = rootDirFile.getAbsolutePath()+"/"
		
		//cleaning
		File binDir = new File(Toolbox.getTxmHomePath(),"corpora/"+basename);
		
		binDir.deleteDir();
		binDir.mkdir();
		new File(binDir,"src").deleteDir();
		new File(binDir,"src").mkdir();
		new File(binDir,"txm").deleteDir();
		new File(binDir,"txm").mkdir();
		
		ArrayList<String> milestones = new ArrayList<String>();//the tags who you want them to stay milestones
		
		//separates values and build simple xml
		List<File> srcfiles = new File(rootDir).listFiles();
		if(srcfiles == null)
		{
			println "No file to process"
			return false;
		}
		//println "BYEBYE FORTRAN";
		println("Converting to xml "+srcfiles.size()+ " files")
		for(File f : srcfiles)
		{
			print "."
			File outfile = new File(binDir,"src/"+f.getName()+".xml")
			FortranColumns2XML builder = new FortranColumns2XML(f, outfile, encoding);
			builder.setColumnIndexes([0, 3, 4, 8, 29, 55, 67],	[3, 4, 8, 28, 54, 66, 78]);
			builder.setColumnsLinesNames(["ref","s","sent","lemme","word","line","pos"], "w", 4);
			builder.setAddIds(true);
			//builder.setLineCheck(3, "l");
			if(!builder.process())
			{
				println "Failed to convert file: "+f
			}
		}
		println ""
		
		rootDir = binDir.getAbsolutePath();
		srcfiles = new File(rootDir, "src").listFiles();
		//TRANSFORM INTO XML-TEI-TXM
		println("Building xml-tei-txm "+srcfiles.size()+ " files")
		for(File f : srcfiles)
		{
			print "."
			//ArrayList<String> milestones = new ArrayList<String>();
			
			File file = f; 
			String txmfile = f.getName();
			def correspType = new HashMap<String,String>()
			//["ref","s","sent","word","lemme","line","pos"]
			correspType.put("ref","ref");
			correspType.put("s","s");
			correspType.put("sent","sent");
			correspType.put("lemme","lemme")
			correspType.put("line","line")
			correspType.put("pos","pos");
			correspType.put("form","form");
			
			def correspRef = new HashMap<String,String>()
			correspRef.put("ref","lasla");
			correspRef.put("s","lasla");
			correspRef.put("sent","lasla");
			correspRef.put("lemme","lasla")
			correspRef.put("line","lasla")
			correspRef.put("pos","lasla");
			correspRef.put("form","lasla");
			
			//il faut lister les id de tous les respStmt
			def respId = ["lasla"];
			//fait la correspondance entre le respId et le rapport d'execution de l'outil
			def applications = new HashMap<String, ArrayList<String>>();
			applications.put("lasla", new ArrayList<String>());
			applications.get("lasla").add("txm");//app ident
			applications.get("lasla").add("0.6");//app version
			applications.get("lasla").add(null);//app report file path
			
			//fait la correspondance entre le respId et les attributs type de la propriété ana du w txm
					//pour construire les ref vers les taxonomies
			def taxonomiesUtilisees = new HashMap<String,String[]>();
			taxonomiesUtilisees.put("lasla",["LASLA"]);//,"lemma","lasla","grace"]);
			
			//associe un id d'item avec sa description et son URI
			def itemsURI = new HashMap<String,HashMap<String,String>>();
			itemsURI.put("LASLA",new HashMap<String,String>());
			itemsURI.get("LASLA").put("tagset","http://bfm.ens-lsh.fr/IMG/xml/cattex2009.xml");
			itemsURI.get("LASLA").put("website","http://bfm.ens-lsh.fr/article.php3?id_article=176");
			
			//informations de respStmt
			//resps (respId <voir ci-dessus>, [description, person, date])
			def resps = new HashMap<String,String[]>();
			resps.put("lasla", ["initial tagging","lasla","2010-03-02",""])
			//lance le traitement
			String wordprefix = "w_";
			def builder = new Xml2Ana(file);
			builder.setCorrespondances(correspRef, correspType);
			builder.setHeaderInfos(respId,resps, applications, taxonomiesUtilisees, itemsURI)
			//builder.setAddTEIHeader();
			if(!builder.process(new File(new File(rootDir,"txm"),txmfile)))
			{
				println "Failed to build xml-txm of file: "+f;
			}
		}
		println ""
		return true;
	}
}