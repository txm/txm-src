<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">

	<!-- Eléments communs à toutes les présentations -->

	<xsl:variable name="text_id">
		<xsl:value-of
			select="//tei:teiHeader//tei:title[@type='reference']" />
	</xsl:variable>

	<!-- <xsl:template match="/"> <html> <head><meta http-equiv="Content-Type" 
		content="text/html;charset=UTF-8"/> </head> <body> <div class="head"><xsl:apply-templates 
		select="//tei:teiHeader"/></div> <xsl:apply-templates select="//tei:text"> 
		<xsl:with-param name="form"><xsl:value-of select="$form"/></xsl:with-param> 
		</xsl:apply-templates> <!-\-<xsl:apply-templates select="//tei:note" mode="final"></xsl:apply-templates>-\-> 
		</body> </html> </xsl:template> -->
	<xsl:template match="/">
		<xsl:apply-templates select="//tei:text">
			<xsl:with-param name="form">
				<xsl:value-of select="$form" />
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>

	<!-- TEI-Header Processing -->

	<xsl:variable name="title-source"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='source']" />

	<xsl:variable name="title-normal"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='normal']" />

	<xsl:variable name="title-ref"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='reference']" />


	<xsl:variable name="author"
		select="//tei:titleStmt/tei:author" />

	<xsl:variable name="apostrophe" select='"&apos;"' />



	<xsl:template match="//tei:teiHeader">
		<p align="center">

			<!-- Author (only appears if not anonymous) -->
			<xsl:if test="$author[not(contains(.,'anonym'))]">
				<span style="font-variant:small-caps;font-weight:bold">
					<xsl:value-of select="$author" />
				</span>
				<br />
			</xsl:if>
			<!-- Title -->
			<span
				style="color:darkviolet;font-family:Times;font-weight:bold;font-style:italic;font-size:200%">
				<xsl:value-of select="$title-normal" />
			</span>
			<br />
			<!-- Scientific editor(s) -->
			Edition par
			<xsl:apply-templates
				select="//tei:titleStmt/tei:editor[@role='editor']" />
			<br />
			Avec la collaboration de
			<xsl:apply-templates
				select="//tei:titleStmt/tei:editor[@role='associate_editor']" />
			<br />

			<!-- Series Statement, Publisher, Year <xsl:apply-templates select="fileDesc/seriesStmt/title"/><br/> 
				<xsl:apply-templates select="//sourceDesc//publicationStmt/pubPlace"/>, <xsl:apply-templates 
				select="//sourceDesc//publicationStmt/publisher"/>, <xsl:apply-templates 
				select="//sourceDesc//publicationStmt/date"/> -->
		</p>

		<!-- Information on the electronic version -->
		<p align="center">
			_____________________________________________________
			<br />
			<br />
			<span style="font-size:75%">
				Edition électronique
				<br />
				Dernière révision :
				<xsl:value-of
					select="//tei:revisionDesc/tei:change[1]/@when" />
			</span>
			<br />
			_____________________________________________________
		</p>
		<p align="center">
			Version
			<xsl:value-of select="$form" />
			<br />
			_____________________________________________________
		</p>
	</xsl:template>



	<xsl:template match="//tei:body/tei:div">
		<br />
		<span style="font-family:Times;color:indigo">
			[
			<i>
				<xsl:value-of select="@type" />
			</i>
			&#xa0;
			<xsl:value-of select="@n" />
			]
		</span>
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="//tei:body/tei:div/tei:div">
		<br />
		<span style="font-family:Times;color:indigo">
			[
			<i>
				<xsl:value-of select="@type" />
			</i>
			&#xa0;
			<xsl:value-of select="@n" />
			]
		</span>
		<br />
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="tei:head">
		<span style="font-family:Arial; color:darkred">
			<xsl:apply-templates />
		</span>
		<br />
	</xsl:template>
	<!-- <xsl:template match="//tei:p"> <p> <xsl:if test="@n"> [ § <xsl:value-of 
		select="@n"/>]&#xa0; </xsl:if> <xsl:apply-templates/> </p> </xsl:template> -->
	<xsl:template name="pb_common" match="tei:pb">
		<!-- <br/><br/> <span style="font-family:Times" id="{$text_id}-{@n}">[folio 
			<xsl:value-of select="@n"/>] <a href="{$text_id}-compare.html#{$text_id}-{@n}">[autres 
			facettes]</a> <xsl:if test="@facs"> <xsl:text> </xsl:text><a href="../{@facs}" 
			target="_blank">[Image]</a> </xsl:if> </span> <xsl:if test="not(following-sibling::*[1][self::cb])"> 
			<br/> </xsl:if> -->
	</xsl:template>

	<xsl:template match="//tei:cb" name="cb_common">
		<br />
		<xsl:if test="not(preceding-sibling::*[1][self::pb])">
			<br />
		</xsl:if>
		<span style="font-family:Times">
			[colonne
			<xsl:value-of select="@n" />
			]
		</span>
		<br />
	</xsl:template>

	<xsl:template match="tei:note" name="note_common">
		<!-- <xsl:variable name="note_num"> <xsl:choose> <xsl:when test="@n"><xsl:value-of 
			select="@n"/></xsl:when> <xsl:otherwise><xsl:value-of select="count(preceding::tei:note) 
			+ 1"/></xsl:otherwise> </xsl:choose> </xsl:variable> <a style="font-size:75%;color:blue;font-family:Times" 
			title="{.}" id="noteref_{$note_num}" href="#note_{$note_num}"><sup>[<xsl:value-of 
			select="count(preceding::tei:note) + 1"/>]</sup></a> <xsl:if test="following::*[1][self::tei:w]"><xsl:text> 
			</xsl:text></xsl:if> -->
	</xsl:template>

	<xsl:template match="tei:note" mode="final">
		<!-- <xsl:variable name="note_num"> <xsl:choose> <xsl:when test="@n"><xsl:value-of 
			select="@n"/></xsl:when> <xsl:otherwise><xsl:value-of select="position()"/></xsl:otherwise> 
			</xsl:choose> </xsl:variable> <span id="note_{$note_num}"><a href="#noteref_{$note_num}">[<xsl:value-of 
			select="$note_num"/>]</a> (fol. <xsl:value-of select="preceding::tei:pb[1]/@n"/>, 
			col. <xsl:value-of select="preceding::tei:cb[1]/@n"/>, l. <xsl:value-of select="preceding::tei:lb[1]/@n"/>) 
			<xsl:apply-templates/></span><br/> -->
	</xsl:template>

	<!-- <xsl:template match="tei:q" name="q_common"> <span style="color:darkblue"><xsl:apply-templates/></span> 
		</xsl:template> -->

	<xsl:template match="tei:gap" name="gap_common">
		<xsl:text> [...] </xsl:text>
	</xsl:template>

	<xsl:template name="supplied_common">
		<xsl:text>[</xsl:text>
		<xsl:apply-templates />
		<xsl:text>]</xsl:text>
	</xsl:template>

	<!-- fin éléments communs -->


</xsl:stylesheet>
