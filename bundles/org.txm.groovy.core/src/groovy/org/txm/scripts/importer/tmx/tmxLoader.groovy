// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2015-08-31 13:45:22 +0200 (lun. 31 août 2015) $
// $LastChangedRevision: 3028 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.importer.tmx;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.ConsoleProgressBar

import java.io.File;
import org.txm.scripts.importer.tmx.importer;
import org.txm.scripts.importer.tmx.compiler;
import org.txm.scripts.importer.tmx.pager;
import org.txm.objects.*;
import org.txm.utils.*
import org.txm.utils.io.*;
import org.txm.*;
import org.txm.core.engines.*;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.i18n.*;
import org.w3c.dom.Element
import org.txm.utils.xml.DomUtils;
import org.txm.utils.FileUtils

String userDir = System.getProperty("user.home");
def MONITOR;
Project project;

try {project=projectBinding;MONITOR=monitor} catch (Exception)
{	}
if (project == null) { println "no project set. Aborting"; return; }

String corpusname = project.getName();
String basename = corpusname
String rootDir = project.getSrcdir();
String lang = project.getLang()
String model = lang
String encoding = project.getEncoding()
boolean annotate = project.getAnnotate()
String xsl = project.getFrontXSL();
def xslParams = project.getXsltParameters();
int wordsPerPage = project.getEditionDefinition("default").getWordsPerPage()
String page_element = project.getEditionDefinition("default").getPageElement()
boolean build_edition = project.getEditionDefinition("default").getBuildEdition()

File srcDir = new File(rootDir);
File binDir = project.getProjectDirectory();
binDir.mkdirs();
if (!binDir.exists()) {
	println "Could not create binDir "+binDir
	return;
}

File txmDir = new File(binDir, "txm");
txmDir.deleteDir();
txmDir.mkdirs();
HashMap<String, String> textLangs = [:];
def langGroups = [:];

println "-- IMPORTER - Reading source files"
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "IMPORTER - Reading source files");
def imp = new importer()
imp.run(srcDir, binDir, txmDir, textLangs, langGroups, project);
def corpusIDS = imp.getCorpusIDS()

if (MONITOR != null) MONITOR.worked(20, "ANNOTATE - Running NLP tools");
if (annotate) {
	println "-- ANNOTATE - Running NLP tools"
	String engineName = project.getImportParameters().node("annotate").get("engine", "TreeTagger")
	def engine = Toolbox.getEngineManager(EngineType.NLP).getEngine(engineName)
	def mapForTreeTagger = [:]
	for (def k : textLangs.keySet()) mapForTreeTagger[k] = textLangs[k].toLowerCase();
		println "TreeTagger models to use per text: $mapForTreeTagger"
	if (engine.processDirectory(txmDir, binDir, ["langs":mapForTreeTagger])) {
		annotationSuccess = true;
		if (project.getCleanAfterBuild()) {
			new File(binDir, "treetagger").deleteDir()
			new File(binDir, "ptreetagger").deleteDir()
			new File(binDir, "annotations").deleteDir()
		}
	}
}
//println "langs : "+textLangs
//println "texts : "+langGroups

println "-- COMPILING - Building Search Engine indexes"
if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "COMPILING - Building Search Engine indexes")
def c = new compiler();
if (debug) c.setDebug();
c.setAlignment("seg","id");
c.setLangs(textLangs);
c.setCorpusIDS(corpusIDS);
c.setLangGroups(langGroups);
if (!c.run(project, binDir, txmDir, basename)) {
	println "Compiler failed"
	return;
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
new File(binDir,"HTML").deleteDir();
new File(binDir,"HTML").mkdirs();
if (build_edition) {

	println "-- EDITION - Building edition"
	if (MONITOR != null) MONITOR.worked(20, "EDITION - Building edition")

	List<File> filelist =  FileUtils.listFiles(new File(binDir,"txm"))
	def second = 0

	def splitTUsPerText = [:]

	def txmdirs =  FileUtils.listDirectories(txmDir);
	int c2 = 0;
	
	for (File txmCorpusDir : txmdirs) c2 += FileUtils.listFiles(txmCorpusDir).length;
	
	ConsoleProgressBar cpb = new ConsoleProgressBar(c2);
	for (File txmCorpusDir : txmdirs) {
		
		def txmFiles =  FileUtils.listFiles(txmCorpusDir)
		txmFiles.sort() // same order
		//println "paging txmFiles : "+txmFiles

		File outdir = new File(binDir,"/HTML/"+txmCorpusDir.getName()+"/default/");
		outdir.mkdirs();
		//println "processing pages of corpus "+txmCorpusDir.getName()

		//		Element paraCorpusElem = corpusElem.cloneNode(true);
		//		paraCorpusElem.setAttribute("name", txmCorpusDir.getName());

		for (File txmFile : txmFiles) {
			String txtname = FileUtils.stripExtension(txmFile);

			//println ""+srcfile.getName()+" -> "+splitTUsPerText[txtname]
			def splitTUs = splitTUsPerText[txtname];
			if (splitTUs == null) {
				splitTUs = [];
				splitTUsPerText[txtname] = splitTUs;
			}

			String l = textLangs.get(txmFile.getName()).toLowerCase()
			if (l == null) l = "fr";
			List<String> NoSpaceBefore = LangFormater.getNoSpaceBefore(l);
			List<String> NoSpaceAfter = LangFormater.getNoSpaceAfter(l);

			Text t = new Text(project);
			t.setName(txtname);
			t.setSourceFile(txmFile)
			t.setTXMFile(txmFile)

			def ed = new pager(txmFile, outdir, txtname, NoSpaceBefore, NoSpaceAfter, wordsPerPage, basename, page_element,splitTUs);
			Edition edition = new Edition(t);
			edition.setName("default");
			edition.saveParameter("corpus", txmCorpusDir.getName());
			edition.setIndex(outdir.getAbsolutePath());
			for (i = 0 ; i < ed.getPageFiles().size();) {
				File f = ed.getPageFiles().get(i);
				String wordid = "w_0";
				if (i < ed.getIdx().size()) wordid = ed.getIdx().get(i);
				edition.addPage(""+(++i), wordid);
			}
			
			cpb.tick()
		}

		File cssfile = new File(Toolbox.getTxmHomePath(), "css/tmx.css")
		if (cssfile.exists()) {
			FileCopy.copy(cssfile, new File(outdir, "tmx.css"));
		}
		File stylefile = new File(binDir, "style.css")
		if (stylefile.exists()) {
			FileCopy.copy(stylefile, new File(outdir, "style.css"));
		}
	}
	cpb.done();
}

if (MONITOR != null && MONITOR.isCanceled()) { return MONITOR.done(); }
if (MONITOR != null) MONITOR.worked(20, "FINALIZING")

readyToLoad = project.save();