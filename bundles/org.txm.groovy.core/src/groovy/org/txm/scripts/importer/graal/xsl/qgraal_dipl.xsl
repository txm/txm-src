<?xml version='1.0'?>

<!-- This is a stylesheet that gives a "diplomatic" presentation of old French 
	manuscript transcriptions - abbreviations are resolved and rendered in italics; 
	- agglutinations are marked with a red + - deglutinations are marked with 
	e red _ - Strong punctuation is marked with a dot followed by a capital letter; 
	weak punctuation is marked with a comma. - direct speech is marked with darkblue 
	color; - uncertain readings are marked with grey background color; - text 
	deleted in the original is not visualized; - text corrected in the normalized 
	version is marked with darkred color and followed by "(sic!)" note; - notes 
	are rendered with [*] sign in the text body; the text of the note appears 
	on "mouse-over" and is reproduced in the end. -->



<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:me="http://www.menota.org/ns/1.0"
	xmlns:bfm="http://bfm.ens-lsh.fr/ns/1.0">
	<xsl:import href="mdv_common.xsl" />
	<xsl:param name="form">
		diplomatique
	</xsl:param>
	<xsl:variable name="mode">
		dipl
	</xsl:variable>

	<xsl:output method="html" encoding="utf-8" indent="yes" />

	<xsl:strip-space elements="*" />

	<!-- <xsl:variable name="title-formal" select="//fileDesc/titleStmt/title[@type='formal']"/> 
		<xsl:variable name="title-normal" select="//fileDesc/titleStmt/title[@type='normal']"/> 
		<xsl:variable name="title-ref" select="//fileDesc/titleStmt/title[@type='reference']"/> 
		<xsl:variable name="author" select="//titleStmt/author"/> -->

	<!-- <xsl:template match="//tei:p|//tei:head"> <p> <xsl:if test="@n"> [ 
		§ <xsl:value-of select="@n"/>]&#xa0; </xsl:if> <xsl:apply-templates mode="dipl"/> 
		</p> </xsl:template> -->

	<xsl:variable name="title-source"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='source']" />

	<xsl:variable name="title-normal"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='normal']" />

	<xsl:variable name="title-ref"
		select="//tei:fileDesc/tei:titleStmt/tei:title[@type='reference']" />


	<xsl:variable name="author"
		select="//tei:titleStmt/tei:author" />

	<xsl:variable name="apostrophe" select='"&apos;"' />


	<!-- <xsl:template match="/"> <html> <head> <meta http-equiv="Content-Type" 
		content="text/html;charset=UTF-8" /> <title> <xsl:if test="$author[not(contains(.,'anonym'))]"> 
		<xsl:value-of select="$author"/><xsl:text> : </xsl:text> </xsl:if> <xsl:value-of 
		select="$title-normal"/> </title> <xsl:apply-templates select="//tei:teiHeader"/> 
		</head> <body style="margin-left:10%;margin-right:10%"> <div class="text"> 
		<xsl:apply-templates select="//tei:text"/> </div> <div class="notes"> <p 
		align="center"><b>Notes</b></p> <xsl:apply-templates select="//tei:note[not(@place='inline')]" 
		mode="notes"/> </div> </body> </html> </xsl:template> -->
	<xsl:template match="/">
		<xsl:apply-templates
			select="//tei:note[@place='inline']" mode="dipl" />
		<xsl:apply-templates
			select="//tei:text//tei:p|//tei:text//tei:head" />
	</xsl:template>



	<!-- TEI-Header Processing -->

	<xsl:template match="//tei:teiHeader">
		<p align="center">

			<!-- Author (only appears if not anonymous) -->
			<xsl:if test="$author[not(contains(.,'anonym'))]">
				<span style="font-variant:small-caps;font-weight:bold">
					<xsl:value-of select="$author" />
				</span>
				<br />
			</xsl:if>
			<!-- Title -->
			<span
				style="color:darkviolet;font-family:Times;font-weight:bold;font-style:italic;font-size:200%">
				<xsl:value-of select="$title-normal" />
			</span>
			<br />
			<!-- Scientific editor(s) -->
			Edition par
			<xsl:apply-templates
				select="//tei:titleStmt/tei:editor[@role='editor']" />
			<br />
			Avec la collaboration de
			<xsl:apply-templates
				select="//tei:titleStmt/tei:editor[@role='associate_editor']" />
			<br />

			<!-- Series Statement, Publisher, Year <xsl:apply-templates select="fileDesc/seriesStmt/title"/><br/> 
				<xsl:apply-templates select="//sourceDesc//publicationStmt/pubPlace"/>, <xsl:apply-templates 
				select="//sourceDesc//publicationStmt/publisher"/>, <xsl:apply-templates 
				select="//sourceDesc//publicationStmt/date"/> -->
		</p>

		<!-- Information on the electronic version -->
		<p align="center">
			_____________________________________________________
			<br />
			<br />
			<span style="font-size:75%">
				Edition électronique
				<br />
				Dernière révision :
				<xsl:value-of
					select="//tei:revisionDesc/tei:change[1]/@when" />
			</span>
			<br />
			_____________________________________________________
		</p>
		<p align="center">
			Version diplomatique
			<br />
			_____________________________________________________
		</p>
	</xsl:template>

	<!-- Gaps without normalized @rend -->
	<xsl:template match="tei:gap[not(@rend)]" mode="dipl">
		<span style="color:darkblue">
			<xsl:text> [...] </xsl:text>
		</span>
	</xsl:template>

	<xsl:template
		match="tei:gap[@rend][not(@rend='susp' or @rend='points' or @rend='crochets-susp')]"
		mode="dipl">
		<span style="color:orange">
			<xsl:text> [...] </xsl:text>
		</span>
	</xsl:template>

	<!-- Corrections without normalized @rend -->

	<xsl:template match="tei:choice" mode="dipl">
		<span title="{child::tei:corr/@type}">
			<xsl:apply-templates select="tei:corr"
				mode="dipl" />
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="tei:sic" mode="dipl" />
		</span>
		<xsl:if test="not(ancestor::tei:w)">
			<xsl:call-template name="spacing" />
		</xsl:if>
	</xsl:template>

	<xsl:template match="tei:corr[not(@rend)]" mode="dipl">
		<span style="color:darkblue;background-color:yellow">
			<xsl:text> </xsl:text>
			<xsl:apply-templates mode="dipl" />
			<xsl:text> </xsl:text>
		</span>
	</xsl:template>

	<!-- <xsl:template match="tei:corr[@rend][not(@rend='crochets' or @rend='chevrons')]"> 
		<span style="color:orange"> <i><xsl:value-of select="@rend"/></i><xsl:text> 
		</xsl:text></span><xsl:apply-templates/> </xsl:template> -->
	<xsl:template match="tei:sic" mode="dipl">
		(
		<i>sic&#xa0;:</i>
		<xsl:text> </xsl:text>
		<span style="background-color:pink">
			<xsl:apply-templates mode="dipl" />
		</span>
		)
	</xsl:template>


	<!-- Supplied text -->

	<xsl:template match="tei:supplied" mode="dipl">
		<span style="color:blue">
			[
			<xsl:apply-templates mode="dipl" />
			]
		</span>
		<xsl:if test="not(ancestor::tei:w)">
			<xsl:call-template name="spacing" />
		</xsl:if>
	</xsl:template>

	<!-- Deletions -->

	<xsl:template match="tei:del" mode="dipl">
		<span style="background-color:lightpink">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:add" mode="dipl">
		<span style="background-color:lightgreen">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<!-- Redundant text -->

	<xsl:template match="tei:surplus" mode="dipl">
		(
		<span style="background-color:lightpink">
			<xsl:apply-templates mode="dipl" />
		</span>
		<xsl:text> </xsl:text>
		<i>répété</i>
		)
		<xsl:call-template name="spacing"></xsl:call-template>
	</xsl:template>

	<!-- Numbers -->
	<xsl:template match="tei:num" mode="dipl">
		<span style="color:blue">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>


	<!-- NOTES -->

	<xsl:template match="tei:note[not(@place='inline')]"
		mode="dipl">
		<xsl:variable name="note_count">
			<xsl:value-of
				select="count(preceding::tei:note[not(@place='inline')]) + 1" />
		</xsl:variable>
		<!--<a title="{.}" style="color:violet;font-size:75%" href="#note_{$note_count}" 
			name="noteref_{$note_count}">[Note <xsl:value-of select="$note_count"/>]</a> -->
		<span title="{.}" style="color:violet">
			[
			<xsl:value-of select="$note_count" />
			]
		</span>
		<xsl:call-template name="spacing" />
	</xsl:template>

	<xsl:template match="tei:note[not(@place='inline')]"
		mode="notes">
		<xsl:variable name="note_count">
			<xsl:value-of
				select="count(preceding::tei:note[not(@place='inline')]) + 1" />
		</xsl:variable>
		<p>
			<xsl:value-of select="$note_count" />
			.
			<a href="#noteref_{$note_count}" name="note_{$note_count}">
				[
				<xsl:value-of
					select="preceding::tei:milestone[@unit='column'][1]/@xml:id" />
				, l.
				<xsl:value-of select="preceding::tei:lb[1]/@n" />
				]
			</a>
			<xsl:text> </xsl:text>
			<xsl:value-of select="." />
		</p>
	</xsl:template>
	<xsl:template match="tei:note[@place='inline']"
		mode="dipl">
		<p
			style="text-align:justify;text-indent:30px;padding-right:20px;padding-left:20px">
			<span style="color:blue;font-family:Times;font-style:italic">
				<xsl:value-of select="." />
			</span>
		</p>
	</xsl:template>



	<xsl:template
		match="//tei:text//tei:p|//tei:text//tei:head">
		<table width="100%" style="border:none">
			<tr>
				<td width="10%" valign="top"
					style="white-space:nowrap;padding-right:10pt;line-height:18px;font-family:Arial;font-size:14px;border:none;text-align:right">
					<xsl:if test="@n">
						<span style="color:gray;font-size:12px">
							[§
							<xsl:value-of select="@n" />
							]
						</span>
					</xsl:if>
					<xsl:apply-templates
						select="descendant::tei:lb|descendant::tei:milestone[@unit='column']|descendant::tei:hi[@rend='initiale']"
						mode="number" />
				</td>
				<td valign="top"
					style="white-space:nowrap;line-height:18px;font-family:Arial;font-size:14px;border:none">
					&#xa0;
					<xsl:apply-templates mode="dipl" />
				</td>
			</tr>
		</table>
	</xsl:template>


	<!-- <xsl:template match="tei:pb" mode="dipl"> <xsl:call-template name="pb_common"/> 
		</xsl:template> <xsl:template match="tei:cb" mode="dipl"> <xsl:call-template 
		name="cb_common"></xsl:call-template> </xsl:template> <xsl:template match="tei:note" 
		mode="dipl"> <xsl:call-template name="note_common"></xsl:call-template> </xsl:template> 
		<xsl:template match="tei:q" mode="dipl"> <span style="background-color:lightcyan"><xsl:apply-templates 
		mode="dipl"/></span> </xsl:template> <xsl:template match="tei:gap" mode="dipl"> 
		<xsl:call-template name="gap_common"></xsl:call-template> </xsl:template> -->

	<!-- Page breaks -->

	<xsl:template match="//tei:pb[@ed='Pauphilet1923']"
		mode="dipl">
		(p.
		<xsl:value-of select="@n" />
		)
	</xsl:template>

	<xsl:template
		match="//tei:milestone[@unit='column'][ancestor::tei:p]" mode="dipl">
		<br />
		<br />
		<span style="color:darkviolet;font-family:arial;font-size:80%">
			&lt;
			<xsl:value-of select="substring-after(@xml:id,'col_')" />
			&gt;
		</span>
		<br />
	</xsl:template>

	<xsl:template
		match="//tei:milestone[@unit='column'][ancestor::tei:p]" mode="number">
		<br />
		&#xa0;
		<br />
		<span style="color:darkviolet;font-family:arial;font-size:80%">&#xa0;</span>
		<br />
	</xsl:template>

	<xsl:template
		match="//tei:milestone[@unit='column'][not(ancestor::tei:p)]">
		<table width="100%">
			<tr>
				<td width="10%" align="right" valign="top"
					style="padding-right:10pt">
					<span style="color:darkviolet;font-family:arial;font-size:80%">&#xa0;</span>
				</td>
				<td valign="top" style="white-space:nowrap">
					<span style="color:darkviolet;font-family:arial;font-size:80%">
						&lt;
						<xsl:value-of
							select="substring-after(@xml:id,'col_')" />
						&gt;
					</span>
				</td>
			</tr>
		</table>
	</xsl:template>


	<xsl:template match="tei:lb" mode="dipl">
		<xsl:choose>
			<xsl:when test="@ed='facs'">
				<xsl:text>-</xsl:text>
				<br />
			</xsl:when>
			<xsl:when
				test="@ed='norm' or preceding::tei:lb[1][@ed='facs']"></xsl:when>
			<xsl:when
				test="position() = 1 and ancestor::tei:s[not(preceding-sibling::*)]"></xsl:when>
			<xsl:otherwise>
				<br />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tei:lb" mode="number">
		<xsl:choose>
			<xsl:when test="@ed='facs'"></xsl:when>
			<xsl:when
				test="position() = 1 and ancestor::tei:s[not(preceding-sibling::*)]"></xsl:when>
			<xsl:otherwise>
				<br />
				&#xa0;
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if
			test="substring(@n, string-length(@n)) = 0 or substring(@n, string-length(@n)) = 5">
			<xsl:value-of select="@n" />
		</xsl:if>
	</xsl:template>


	<!--<xsl:template match="//tei:lb[@ed='norm']" mode="dipl"> </xsl:template> 
		<xsl:template match="//tei:lb[not(@ed)]" mode="dipl"> <xsl:choose> <xsl:when 
		test="position()=1 and not(ancestor::tei:seg)"></xsl:when> <xsl:when test="preceding::tei:w[1][descendant::bfm:headlb]"></xsl:when> 
		<xsl:when test="preceding::tei:w[1][descendant::tei:lb]"></xsl:when> <xsl:otherwise><br/></xsl:otherwise> 
		</xsl:choose> </xsl:template> <xsl:template match="//tei:lb[@ed='facs']" 
		mode="dipl"> <xsl:text>-</xsl:text><br/> </xsl:template> -->




	<xsl:template match="//tei:w" mode="dipl">
		<span title="{@type}" class="word" id="{@xml:id}">
			<xsl:if test="@rend='fgmt'">
				<xsl:attribute name="style">color:red</xsl:attribute>
			</xsl:if>
			<xsl:if test="descendant::me:dipl">
				<xsl:apply-templates
					select="descendant::me:dipl" mode="dipl" />
				<xsl:if
					test="descendant::tei:sic and not(following-sibling::tei:w[1][descendant::tei:sic])">
					<i>
						<xsl:text> (sic !)</xsl:text>
					</i>
				</xsl:if>
			</xsl:if>
			<xsl:call-template name="spacing" />
		</span>
	</xsl:template>

	<xsl:template name="spacing">
		<xsl:choose>
			<xsl:when
				test="following::tei:w[1][starts-with(@type, 'PON')][starts-with(normalize-space(descendant::text()[1]), ',') or starts-with(normalize-space(descendant::text()[1]), '.') or starts-with(normalize-space(descendant::text()[1]), ']')or contains(., ')')]">
			</xsl:when>
			<xsl:when
				test="following::node()[1][self::text()][starts-with(normalize-space(.), ',') or starts-with(normalize-space(.), '.') or starts-with(normalize-space(.), ']') or starts-with(normalize-space(.), ')')]">
			</xsl:when>
			<xsl:when
				test="self::tei:w[starts-with(@type, 'PON')][starts-with(normalize-space(descendant::text()[1]), '[') or starts-with(normalize-space(descendant::text()[1]), '(')]">
			</xsl:when>
			<xsl:when
				test="substring(., string-length(.))=$apostrophe and not(@type='PONfbl' or @type='PONpdr' or self::tei:note)">
			</xsl:when>
			<xsl:when
				test="position()=last() and (ancestor::tei:choice or ancestor::tei:supplied[not(@rend='multi_s')] or ancestor::tei:surplus)"></xsl:when>
			<xsl:when test="following-sibling::*[1][self::tei:note]"></xsl:when>
			<xsl:when
				test="following::tei:w[1][starts-with(@type, 'PON')][contains(., ':') or contains(., ';') or contains(., '!') or contains(., '?')]">
				<xsl:text>&#xa0;</xsl:text>
			</xsl:when>
			<!-- <xsl:when test="following::tei:w[1][starts-with(@type, 'PON')][contains(descendant::me:norm, 
				':') or contains(descendant::me:norm, ';') or contains(descendant::me:norm, 
				'!')or contains(descendant::me:norm, '?')]"> <xsl:text>&#xa0;</xsl:text> 
				</xsl:when> -->
			<xsl:when test="@rend='elision'">
				’
			</xsl:when>
			<!-- new 201106-03 -->
			<xsl:when test="@rend='fgmt'"></xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>



	<xsl:template match="tei:ex" mode="dipl">
		<xsl:choose>
			<xsl:when test="@cert='high'">
				<span style="color:darkgreen;font-style:italic">
					<xsl:apply-templates mode="dipl" />
				</span>
			</xsl:when>
			<xsl:otherwise>
				<span style="color:darkred;font-style:italic">
					<xsl:apply-templates mode="dipl" />
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- added degl -->
	<xsl:template match="bfm:sb" mode="dipl">
		<span style="color:red">_</span>
	</xsl:template>

	<!-- <xsl:template match="tei:sic" mode="dipl"> <span style="color:darkred" 
		title="sic!"><xsl:apply-templates mode="dipl"/></span> </xsl:template> <xsl:template 
		match="tei:subst" mode="dipl">[<xsl:apply-templates mode="dipl"/>]</xsl:template> 
		<xsl:template match="tei:del" mode="dipl"> <span style="color:red;text-decoration:line-through"><xsl:apply-templates 
		mode="dipl"/></span> </xsl:template> <xsl:template match="tei:add[not(@seq)]" 
		mode="dipl"> <span style="color:blue;text-decoration:underline"><xsl:apply-templates 
		mode="dipl"/></span> </xsl:template> -->
	<xsl:template match="tei:add[@seq]" mode="dipl">
		<span style="color:blue">
			<sup>
				&lt;
				<xsl:value-of select="@seq" />
			</sup>
			<xsl:apply-templates mode="dipl" />
			<sup>
				<xsl:value-of select="@seq" />
				&gt;
			</sup>
		</span>
	</xsl:template>

	<xsl:template match="tei:unclear" mode="dipl">
		<span style="background-color:lightgrey">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>
	<!-- font-size:xx-large enlevé -->
	<xsl:template match="tei:hi[@rend='initiale']" mode="dipl">
		<span style="font-weight:bold;font-size:large">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:hi[@rend='initiale']"
		mode="number">
		<span style="font-weight:bold;font-size:large">&#xa0;</span>
	</xsl:template>

	<xsl:template match="//tei:hi[@rend='sup']" mode="dipl">
		<sup>
			<xsl:apply-templates mode="dipl" />
		</sup>
	</xsl:template>

	<!-- Discours direct -->
	<xsl:template
		match="tei:q[ancestor::tei:q[ancestor::tei:q]]" mode="dipl">
		<span style="background-color:darkcyan">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template
		match="tei:q[ancestor::tei:q][not(ancestor::tei:q[ancestor::tei:q])]"
		mode="dipl">
		<span style="background-color:cyan">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<xsl:template match="tei:q[not(ancestor::tei:q)]"
		mode="dipl">
		<span style="background-color:lightcyan">
			<xsl:apply-templates mode="dipl" />
		</span>
	</xsl:template>

	<!--<xsl:template match="bfm:sb" mode="dipl"><span style="color:violet">_</span></xsl:template> -->



	<!-- <xsl:template match="tei:name"> <xsl:value-of select="@reg"/> </xsl:template> -->
</xsl:stylesheet>