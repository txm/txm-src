package org.txm.scripts.importer.teicmc

import java.io.File;
import java.net.URL;

import javax.xml.stream.*;

import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.StaxStackWriter;

public class SplitTEICMRPerLocutor extends StaxIdentityParser {

	File outputDirectory;
	def writers = [:];

	public SplitTEICMRPerLocutor(File inputFile, File outputDirectory) {
		super(inputFile);
		this.outputDirectory = outputDirectory;
		outputDirectory.mkdir()
	}

	protected void processStartElement() {
		if (parser.getLocalName() == "posting") {
			for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
				if (parser.getAttributeLocalName(i) == "who") {
					String loc = parser.getAttributeValue(i)
					writer = writers.get(loc) // switch writer on locutor
					break;
				}
			}
		}

		if (writer != null) {
			processStartElement(writer);
		} else {
			for (def swriter : writers.values()) processStartElement(swriter);
		}
	}

	protected void processStartElement(def swriter)
	{
		String prefix = parser.getPrefix();

		if (prefix != null && prefix.length() > 0)
			swriter.writeStartElement(Nscontext.getNamespaceURI(prefix), localname)
		else
			swriter.writeStartElement(localname);

		for (int i = 0 ; i < parser.getNamespaceCount() ; i++) {
			swriter.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
		}

		writeAttributes(swriter);
	}

	private void _processStartElement() {
		String prefix = parser.getPrefix();

		if (prefix != null && prefix.length() > 0)
			writer.writeStartElement(Nscontext.getNamespaceURI(prefix), localname)
		else
			writer.writeStartElement(localname);

		for (int i = 0 ; i < parser.getNamespaceCount() ; i++) {
			writer.writeNamespace(parser.getNamespacePrefix(i), parser.getNamespaceURI(i));
		}

		writeAttributes();
	}


	protected void processNamespace() {
		if (writer != null) {
			writer.writeNamespace(parser.getPrefix(), parser.getNamespaceURI());
		} else {
			for (def swriter : writers.values()) swriter.writeNamespace(parser.getPrefix(), parser.getNamespaceURI());
		}
	}

	protected void writeAttributes() {
		if (writer != null) {
			writeAttributes(writer);
		} else {
			for (def swriter : writers.values()) writeAttributes(swriter);
		}
	}

	protected void writeAttributes(def swriter) {
		for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
			String attrPrefix = parser.getAttributePrefix(i);
			if (attrPrefix != null && attrPrefix.length() > 0)
				swriter.writeAttribute(attrPrefix+":"+parser.getAttributeLocalName(i), parser.getAttributeValue(i));
			else
				swriter.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i));
		}
	}

	protected void processCharacters()
	{
		if (writer != null) {
			writer.writeCharacters(parser.getText());
		} else {
			for (def swriter : writers.values()) swriter.writeCharacters(parser.getText());
		}
	}

	protected void processProcessingInstruction()
	{
		if (writer != null) {
			writer.writeProcessingInstruction(parser.getPITarget(), parser.getPIData());
		} else {
			for (def swriter : writers.values()) swriter.writeProcessingInstruction(parser.getPITarget(), parser.getPIData());
		}
	}

	protected void processDTD()
	{
		if (writer != null) {
			writer.writeDTD(parser.getText());
		} else {
			for (def swriter : writers.values()) swriter.writeDTD(parser.getText());
		}
	}

	protected void processCDATA()
	{
		if (writer != null) {
			writer.writeCData(parser.getText())
		} else {
			for (def swriter : writers.values()) swriter.writeCData(parser.getText())
		}
	}

	protected void processComment()
	{
		if (writer != null) {
			writer.writeComment(parser.getText());
		} else {
			for (def swriter : writers.values()) swriter.writeComment(parser.getText());
		}
	}

	protected void processEndElement()
	{
		if (writer != null) {
			writer.writeEndElement();
		} else {
			for (def swriter : writers.values()) swriter.writeEndElement();
		}
		if (parser.getLocalName() == "posting") writer = null;
	}

	protected void processEndDocument() {
		if (writer != null) {
			writer.writeEndDocument();
		} else {
			for (def swriter : writers.values()) swriter.writeEndDocument();
		}
	}

	protected void processEntityReference() {
		if (writer != null) {
			writer.writeEntityRef(parser.getLocalName());
		} else {
			for (def swriter : writers.values()) swriter.writeEntityRef(parser.getLocalName());
		}
	}

	public boolean processLocutors(def locutors) {
		for (String loc : locutors) {
			println "Create writer for $loc"
			writers.put(loc, new StaxStackWriter(new File(outputDirectory, loc+"-teicmr.xml"), "UTF8"))
		}
		println "writers: $writers"

		for (def swriter : writers.values()) {
			swriter.writeStartDocument("UTF-8", "1.0");
			swriter.writeCharacters("\n");
		}

		boolean ret = process(writer);

		for (def swriter : writers.values()) {
			try {swriter.close();} catch(Exception e){println "close writer exep: "+e}
		}

		if (parser != null)
			try {parser.close()} catch(Exception e){println "parser exep: "+e}

		return ret;
	}

	public static void main(String[] args) {
		File inputFile = new File("/home/mdecorde/xml/comere/ismael-textchat.xml")
		File outputDirectory = new File("/home/mdecorde/xml/comere/split_out")

		def p = new SplitTEICMRPerLocutor(inputFile, outputDirectory)
		println p.processLocutors(["s_1", "s_10", "s_11", "s_12", "s_13", "s_14", "s_3", "s_4", "s_5", "s_6", "s_7", "s_8", "s_9", "tt_1", "tt_12", "tt_2", "tt_3", "tt_4", "tt_5", "tt_6", "tt_7", "tt_8"])
	}
}