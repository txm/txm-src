// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.scripts.importer.perrault

import org.txm.importer.cwb.CwbEncode
import org.txm.importer.cwb.CwbMakeAll
import org.txm.scripts.importer.*;
import org.txm.scripts.*;
import org.txm.importer.scripts.xmltxm.BuildTTSrc;
import org.txm.importer.scripts.xmltxm.*;
import org.txm.utils.treetagger.TreeTagger;

import javax.xml.stream.*;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class compiler.
 */
class compiler 
{
	
	/** The input data. */
	private def inputData;
	
	/** The factory. */
	private def factory;
	
	/** The parser. */
	private XMLStreamReader parser;
	
	/** The dir. */
	private def dir;
	
	/** The output. */
	private def output;
	
	/** The url. */
	private def url;
	
	/** The anahash. */
	private HashMap<String,String> anahash =new HashMap<String,String>() ;
	
	/** The text. */
	String text="";
	
	/** The base. */
	String base="";
	
	/** The project. */
	String project="";
	
	/** The lang. */
	private String lang ="fr";
	
	/**
	 * initialize.
	 *
	 */
	public compiler(){}
	
	/**
	 * Instantiates a new compiler.
	 *
	 * @param url the url
	 * @param text the text
	 * @param base the base
	 * @param project the project
	 */
	public compiler(URL url,String text,String base, String project)
	{
		this.text = text
		this.base = base;
		this.project = project;
		try {
			this.url = url;
			inputData = url.openStream();
			
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		}catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}
	
	/**
	 * set the language of the corpus.
	 *
	 * @param lang the lang
	 * @return the java.lang. object
	 */
	public setLang(String lang)
	{
		this.lang = lang;
	}
	
	/**
	 * Creates the output.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	private boolean createOutput(String dirPathName, String fileName){
		try {
			File f = new File(dirPathName, fileName)
			output = new OutputStreamWriter(new FileOutputStream(f,f.exists()) , "UTF-8");
			return true;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return false;
		}
	}
	
	/**
	 * Go to text.
	 */
	private void GoToText()
	{
		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
		{
			if(event == XMLStreamConstants.END_ELEMENT)
				if(parser.getLocalName().equals("teiHeader"))
					return;
		}
	}
	
	/**
	 * Transfom file cqp.
	 *
	 * @param dirPathName the dir path name
	 * @param fileName the file name
	 * @return true, if successful
	 */
	public boolean transfomFileCqp(String dirPathName, String fileName)
	{
		createOutput(dirPathName, fileName);
		
		String headvalue=""
		String vAna = "";
		String vForm = "";
		String wordid= "";
		String vHead = "";
		
		int p_id = 0;
		int q_id = 0;
		int lg_id = 0;
		int l_id = 0;
		
		boolean flaglg = false;
		boolean flaghead = false;
		boolean flagAuthor = false;
		boolean flagDate = false;
		boolean flagForm = false;
		boolean flagAna = false;
		
		this.GoToText()
		output.write("<txmcorpus lang=\""+lang+"\">\n"); 
		try 
		{
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) 
			{
				switch (event) 
				{
					case XMLStreamConstants.START_ELEMENT:
					switch (parser.getLocalName()) 
					{
						
						case "head"://get attr lang
							flaghead =true;
							vHead="";
						break;
												
						case "text":
						output.write("<text id=\""+text+"\" base=\""+base+"\" project=\""+project+"\">\n");
						break;
						
						case "p":
						output.write("<"+parser.getLocalName()+" id=\""+text+"_p_"+(p_id++)+"\">\n");
						break;
						case "q":
						output.write("<"+parser.getLocalName()+" id=\""+text+"_q_"+(q_id++)+"\">\n");
						break;
						case "l":
							output.write("<"+parser.getLocalName()+" id=\""+text+"_l_"+(l_id++)+"\">\n");
						break;
						
						case "lg":
							flaglg = true;
						break;
						
						case "s":
						output.write( "<s>\n");
						break;
						
						case "w":
							for(int i = 0 ; i < parser.getAttributeCount(); i++)
								if(parser.getAttributeLocalName(i).equals("id"))
								{	
									wordid = parser.getAttributeValue(i);
								}
						break;
						case "form":
						flagForm = true;
						vForm = "";
						vAna ="";
						break;
						
						case "ana":
						flagAna = true;
						break;
					}
					break;
					
					case XMLStreamConstants.END_ELEMENT:
					switch (parser.getLocalName()) 
					{
						case "head"://get attr lang
						flaghead =false;
						if(flaglg)
							output.write("<moral id=\""+text+"_moral_"+(lg_id++)+"\" head=\""+vHead+"\">\n");
						break;
						
						case "text":
						output.write("</text>\n");
						break;
						
						case "p":
						case "q":
						case "l":
						output.write("</"+parser.getLocalName()+">\n");
						break;
						
						case "lg":
						output.write("</moral>\n");
						flaglg = false;
						break;
						
						case "s":
						output.write( "</s>\n");
						break;
						
						case "w":
						if(!(flaghead && flaglg))
							if(vAna != null)
								output.write( vForm +vAna+"\t"+wordid+"\n");
						vAna = "";
						vForm = "";
						break;
						
						case "form":
						flagForm = false;
						break;
						
						case "ana":
						flagAna = false;
						break;
					}
					break;
					
					case XMLStreamConstants.CHARACTERS:
					if(flagForm)
					vForm += parser.getText().trim();
					if(flagAna)
					vAna += "\t" +parser.getText().trim();
					if(flaghead && flaglg)
						vHead += parser.getText().trim();
					break;
				}
			}
			output.write("</txmcorpus>"); 
			output.close();
			parser.close();
		}
		catch (XMLStreamException ex) {
			System.out.println(ex);
		}
		catch (IOException ex) {
			System.out.println("IOException while parsing " + inputData);
		}
		
		return true;
	}
	
	/**
	 * Run.
	 *
	 * @param files the files
	 * @return true, if successful
	 */
	public boolean run(List<File> files) 
	{
		String rootDir ="";
		if(files.size() > 0)
			rootDir = files.get(0).getParentFile().getParentFile().getAbsolutePath()+"/";//"~/xml/perrault/";
		
			if (!(CwbEncode.isExecutableAvailable() && CwbMakeAll.isExecutableAvailable())) {
				println ("Error: CWB executables not well set.")
				return false;
			}
		if(!new File(rootDir).exists())
		{
			println ("binary directory does not exists: "+rootDir)
			return false;
		}
		new File(rootDir+"cqp/","perrault.cqp").delete();//cleaning&preparing
		new File(rootDir+"cqp/").deleteDir();
		new File(rootDir+"cqp/").mkdir();
		new File(rootDir+"registry/").mkdir();
		
		String textid="";
		int counttext =0;
		//1- Transform into CQP file
		for(File f : files)
		{
			counttext++;
			if(!f.exists())
			{
				println("file "+f+ " does not exists")	
			}
			else
			{	
				println("process file "+f)
				String txtname = f.getName().substring(0,f.getName().length()-4);
				def builder = new compiler(f.toURL(), txtname, "perrault", "default");
				builder.setLang(lang);
				builder.transfomFileCqp(rootDir+"cqp","perrault.cqp");
			}
		}
		
		//2- Import into CWB
		def outDir =rootDir;
		def outDirTxm = rootDir;
		
		CwbEncode cwbEn = new CwbEncode();
		CwbMakeAll cwbMa = new CwbMakeAll();
		
		String[] pAttributes = ["pos","lemme","id"];
		//String[] pAttributes = ["id"];
		String[] sAttributes = ["txmcorpus:0+lang","text:0+id+base+project","p:0+id","q:0+id","moral:0+head+id","l:0+id"];
		
		try
		{
			cwbEn.run(outDirTxm + "data", outDir + "/cqp/"+"perrault.cqp", outDirTxm + "registry/"+"perrault",pAttributes, sAttributes);
			cwbMa.run("PERRAULT", outDirTxm + "registry");
			
		} catch (Exception ex) {System.out.println(ex); return false;}
		
		System.out.println("Done.") 
		
		return true;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		File dir = new File("~/xml/perrault/txm/");
		List<File> files = dir.listFiles();
		new compiler().run(files);
	}
}
