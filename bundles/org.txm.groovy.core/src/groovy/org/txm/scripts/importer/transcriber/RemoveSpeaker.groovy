// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.importer.transcriber;


import org.txm.scripts.importer.*;
import org.txm.scripts.importer.graal.PersonalNamespaceContext
import org.txm.utils.*;
import org.txm.utils.xml.DomUtils
import org.txm.metadatas.*;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;



// TODO: Auto-generated Javadoc
/**
 * Removes "u" tags of TRS file given an "u@name" value. 
 * @author mdecorde
 *
 */
public class RemoveSpeaker {
	File outfile;
	
	/** The doc. */
	Document doc;
	
	/**
	 * Instantiates a new removes the speaker.
	 *
	 * @param transcriptionfile the transcriptionfile
	 * @param outfile the outfile
	 * @param id the id
	 */
	public RemoveSpeaker(File transcriptionfile, File outfile, String idRegex) {
		System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
				
		this.outfile = outfile;
//		String xpathString = "//u";
		//println "removing $xpathString in $transcriptionfile"
//		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
//		//println "domFactory: $domFactory"
//		domFactory.setNamespaceAware(true); // never forget this!
//		domFactory.setXIncludeAware(true);
//		DocumentBuilder builder = domFactory.newDocumentBuilder();
		//println "builder $builder"
		//doc = builder.parse(transcriptionfile);
		//println "doc $doc"
		doc = DomUtils.load(transcriptionfile)
		//def xpath = XPathFactory.newInstance().newXPath()
//		xpath.setNamespaceContext(new PersonalNamespaceContext());
		//def expr = xpath.compile(xpathString);
		def nodes = doc.getDocumentElement().getElementsByTagName("u")
		
		def reg = /$idRegex/
		
		for (def node : nodes) {
			
			//Element elem = (Element)node;
			
			String who = node.getAttribute("who") // [@who='"+idRegex+"']
			if (reg.matches(who)) {
				node.getParentNode().removeChild(node);
			}
		}
		save()
	}
	
	/**
	 * Save.
	 *
	 * @return true, if successful
	 */
	private boolean save() {
		try {
			// Création de la source DOM
			Source source = new DOMSource(doc);
			
			// Création du fichier de sortie
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8")); 
			Result resultat = new StreamResult(writer);
			
			// Configuration du transformer
			//TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			//println "fabrique $fabrique"
			Transformer transformer = fabrique.newTransformer();
			//println "transformer $transformer"
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8"); 
			
			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			return true;
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}
}
