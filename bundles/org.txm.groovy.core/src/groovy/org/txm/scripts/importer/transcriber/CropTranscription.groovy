// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS  FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2011-11-01 16:12:36 +0100 (mar., 01 nov. 2011) $
// $LastChangedRevision: 2049 $
// $LastChangedBy: sheiden $
//

package org.txm.scripts.importer.transcriber

import java.text.DecimalFormat;
import groovy.xml.XmlParser
// parameters

String userdir = System.getProperty("user.home")
File infile = new File(userdir, "xml/minitranscriber/int01.trs")
File outfile = new File(userdir, "xml/minitranscriber/int01-cropped.trs")

float start = 10.0f;
float end = 500.0f;
formater = DecimalFormat.getInstance(Locale.ENGLISH)

public def cropTRS(File infile, File outfile, float start, end) {
	def timeResolution = 0.001
	URL u = infile.toURI().toURL()
	InputStream ins = u.openStream()

	// Open input file
	def slurper = new XmlParser();
	slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
	def trs = slurper.parse(infile.toURI().toString())

	println "TO BE DONE: remove elements before 'start' and after 'end'"
	println "REMOVE TEXT NODES BEFORE removed Sync"
	println "REMOVE TEXT NODES AFTER removed Sync"
	
	// Then fix all <Sync>s of Turns
	for (def section : trs.Episode.Section) {
		
		section.Turn.each{ turn ->
			
			turn.Sync.each(){ sync ->
				
			}
		}
	}

	String xml = "";
	println ""+xml
	outfile.withWriter("UTF-8"){ writer ->
		writer.write('<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE Trans SYSTEM "trans-14.dtd">\n')
		new groovy.xml.XmlNodePrinter(new PrintWriter(writer)).print(trs) }
}

/// MAIN ///
File tmpfile = new File(infile.getAbsolutePath()+".tmp")
cropTRS(infile, tmpfile, start, end)
new ShiftTiming().shiftTRS(tmpfile, outfile, start)
tmpfile.delete()
