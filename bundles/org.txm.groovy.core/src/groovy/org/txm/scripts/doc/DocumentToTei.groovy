// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2016-03-14 17:07:32 +0100 (lun. 14 mars 2016) $
// $LastChangedRevision: 3165 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.doc

import java.io.File;

import org.txm.importer.ApplyXsl2;
import org.txm.utils.zip.Zip
import org.txm.utils.FileUtils
import org.txm.libs.jodconverter.ConvertDocument

/**
 * The Class DocumentToTei.
 */
class DocumentToTei {

	static boolean DEBUG = true;

	public static boolean processFiles(def files, File outdir, File xsldir) {
		boolean ret = true;

		File xslOdtTei = new File(xsldir, "tei/odt/odttotei.xsl");

		if (!xsldir.exists()) { println "XslDir does not exists: "+xsldir; return false;}
		if (!xslOdtTei.exists()) { println "xslOdtTei file does not exists: "+xslOdtTei; return false;}

		for (File file : files) {
			print "."
			String name = file.getName();
			if (FileUtils.isExtension(file, "odt")) {
				name = FileUtils.stripExtension(file);
				File teifile = new File(outdir, name+".xml");
				try {
					if (!new DocumentToTei().run(file, xslOdtTei, teifile)) {
						println "ODT to TEI failed: "+file
						//						ret = false;
						//						break;
					}
				} catch(Exception e) { println "ODT to TEI failed: $file: $e"; }
			} else if (FileUtils.isExtension(file, "docx")) {
				name = FileUtils.stripExtension(file);
				File teifile = new File(outdir, name+".xml");
				File odtFile = File.createTempFile("workflowdocx", "sfsdf.odt", outdir);
				
				try {
					ConvertDocument.convert(file, odtFile)
					if (!new DocumentToTei().run(odtFile, xslOdtTei, teifile)) {
						println "Docx to Odt to Tei failed: "+file
						odtFile.delete();
					}
				} catch(Exception e) { println "DOCX to ODT to TEI failed: $file: $e"; }
				finally { 
					odtFile.delete();
				}
				
			} else if (FileUtils.isExtension(file, "doc")) {
				name = FileUtils.stripExtension(file);
				File teifile = new File(outdir, name+".xml");
				File odtFile = File.createTempFile("workflowdoc", "sfsdf.odt", outdir);
				
				try {
					ConvertDocument.convert(file, odtFile)
					if (!new DocumentToTei().run(odtFile, xslOdtTei, teifile)) {
						println "DOC to ODT to TEI failed: "+file
						odtFile.delete();
					}
				} catch(Exception e) { println "DOC to ODT to TEI failed: $file: $e"; }
				finally {
					odtFile.delete();
				}
			}
		}

		return ret;
	}

	/**
	 * Run.
	 *
	 * @param odtfile the odtfile
	 * @param xslfile the xslfile
	 * @param teifile the teifile
	 * @return true, if successful
	 */
	public boolean run(File odtfile, File xslfile, File teifile)
	{
		println "** ODT TO TEI $odtfile $teifile"
		File unzipdir = new File(teifile.getParent(), "files-"+teifile.getName());
		unzipdir.deleteDir()
		Zip.decompress(odtfile.getAbsolutePath(), unzipdir.getAbsolutePath(), false);

		//println "get content.xml"
		File content = new File(unzipdir, "content.xml");
		if (!content.exists()) {
			println "OdtToTei: failed to get 'content.xml' file from "+unzipdir
			return false;
		}

		//println "apply xsl oo-to-tei"
		ApplyXsl2 a = new ApplyXsl2(xslfile.getAbsolutePath());
		a.process(content.getAbsolutePath(), teifile.getAbsolutePath());

		//unzipdir.deleteDir()
		return true;
	}

	/**
	 * Run.
	 *
	 * @param odtfile the odtfile
	 * @param xslfile the xslfile
	 * @param teifile the teifile
	 * @return true, if successful
	 */
	public boolean rundocx(File docxfile, File xslfile, File teifile)
	{
		//println "** ODT TO TEI $docxfile $teifile"
		File unzipdir = new File(docxfile.getParent(), "files-"+teifile.getName());
		unzipdir.deleteDir()
		Zip.decompress(docxfile.getAbsolutePath(), unzipdir.getAbsolutePath(), false);

		//println "get content.xml"
		File content = new File(unzipdir, "word/document.xml");
		if (!content.exists()) {
			println "failed to get $content"
			return false;
		}

		//println "apply xsl oo-to-tei"
		ApplyXsl2 a = new ApplyXsl2(xslfile.getAbsolutePath());
		a.setParam("word-directory", unzipdir.getAbsolutePath())
		a.process(content.getAbsolutePath(), teifile.getAbsolutePath());

		return true;
	}
	
	public static void main(String[] args) {
		File odtfile = new File("/home/mdecorde/xml/manual/Manuel de TXM 0.7 FR.odt")
		File xslfile = new File("/home/mdecorde/TXM/xsl/tei/odt/odttotei.xsl")
		File teifile = new File("/home/mdecorde/xml/manual/Manuel de TXM 0.7 FR.odt.xml")
		new DocumentToTei().run(odtfile, xslfile, teifile)
	}
}
