/**
 * Main.
 *
 * @param args the args
 */
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (lun. 06 mai 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $
//
package org.txm.scripts.doc

import org.txm.libs.jodconverter.ConvertDocument

// TODO: Auto-generated Javadoc
/* (non-Javadoc)
 * @see groovy.lang.Script#run()
 */
String home = System.getProperty("user.home");
File srcdir = new File(home, "xml/txmrefman")
File outdir = new File(home, "xml/txmrefman/TEI")
File xsldir = new File(home, "xsl")
File xslOdtTei = new File(xsldir, "tei/odt/odttotei.xsl");
File xslDocxTei = new File(xsldir, "tei/docx/from/docxtotei.xsl");

if (!xslOdtTei.exists()) {
	println "$xslOdtTei file does not exists"
	return;
}

outdir.deleteDir()
outdir.mkdirs()

println "start"
for (File file : srcdir.listFiles()) {
	try {
		File teifile = new File(outdir, file.getName()+".xml");
		if (file.getName().endsWith(".odt")) {
			if (!new DocumentToTei().run(file, xslOdtTei, teifile)) {
				println "Odt to Tei failed"
				return;
			}
		} else if (file.getName().endsWith(".docx")) {
			File odtFile = File.createTempFile("workflowodt", "sfsdf.odt", srcdir);
			// convert doc to odt
			ConvertDocument.convert(file, odtFile)
			if (!new DocumentToTei().run(odtFile, xslOdtTei, teifile)) {
				println "Docx to Odt to Tei failed: "+file
				odtFile.delete();
				return;
			}
			odtFile.delete();
		} else if (file.getName().endsWith(".doc")) {
			File odtFile = File.createTempFile("workflowodt", "sfsdf.odt", srcdir);
			// convert doc to odt
			ConvertDocument.convert(file, odtFile)
			if (!new DocumentToTei().run(odtFile, xslOdtTei, teifile)) {
				println "Doc to Odt to Tei failed: "+file
				odtFile.delete();
				return;
			}
			odtFile.delete();
		}
	}
	catch(Exception e) { e.printStackTrace() }
	finally { }
}

println "--Done"