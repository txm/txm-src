package org.txm.scripts.sw
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu. 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// parameters

// **change this parameter**
dir = new File(System.getProperty("user.home"), "Bureau/voeux")

// **change this parameter**
ext = "\\.txt"

// **change this parameter**
find = "’"

// **change this parameter**
replaceWith = "'"

// **change this parameter**
encoding = "utf-8"

// **change this parameter**
// 'true' = only display matching lines, 'false' = replace in matching files
showMatchingFilesOnly = false


//find = /date="([0-9]+)-([0-9]+-[0-9]+)"/
		      // **change this parameter**
//replaceWith = 'date="$1-$2" year="$1"'
		      // **change this parameter** (warning: '$1', '$2'... can be interpreted by Groovy in "..." strings)

// main body
println "in $dir ... "+/.*$ext/
dir.eachFileMatch(~/.*$ext/) { file ->               // for each file matching extension
	println file.getName()
	
if (showMatchingFilesOnly) {
	
       file.eachLine(encoding) { line ->                  // for each line
           if (line.matches(".*$find.*")) { println " *match* : "+line }
	   }
} else {
       def tmp = File.createTempFile("SearchReplaceInDirectoryTemp", ".tmp", dir) // create temporary file
       tmp.write('')                                // create empty file
       tmp.withWriter(encoding) { writer ->
           file.eachLine(encoding) { line ->                  // for each line
           	writer.println line.replaceAll(find, replaceWith) // find&replace and print
           }
       }
       file.delete()
       tmp.renameTo(file)                           // save results
 }
}
