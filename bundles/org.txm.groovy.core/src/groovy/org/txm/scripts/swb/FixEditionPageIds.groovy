package org.txm.scripts.scripts

import org.txm.utils.xml.DomUtils

String corpus = "qgraal"
String home = System.getProperty("user.home")
File PARAMS = new File("$home/TXM/corpora/$corpus/import.xml")
File PARAMS2 = new File("$home/TXM/corpora/$corpus/params2.xml")
File HTMLs = new File("$home/TXM/corpora/$corpus/HTML/QGRAALFRO")

def doc = DomUtils.load(PARAMS)
def root = doc.getDocumentElement()

def corporaList = root.getElementsByTagName("corpora")
def corporaElem = corporaList.item(0)

def corpusList = corporaElem.getElementsByTagName("corpus")
def corpusElem = corpusList.item(0)

def textsList = corpusElem.getElementsByTagName("texts")
def textsElem = textsList.item(0);

def textList = textsElem.getElementsByTagName("text")
for (int i = 0 ; i < textList.getLength() ; i++) {
	def textElem = textList.item(i)
	String textname = textElem.getAttribute("name")
	println "process text: "+textname
	def editionsList = textElem.getElementsByTagName("editions")
	def editionsElem = editionsList.item(0)

	def editionList = editionsElem.getElementsByTagName("edition")

	for (int j = 0 ; j < editionList.getLength() ; j++) {
		def editionElem = editionList.item(j)
		String editionName = editionElem.getAttribute("name")

		def pages = new File(HTMLs, editionName).listFiles()
		pages.sort()

		def pageList = editionElem.getElementsByTagName("page")
		println pageList.getLength()
		println pages.size()
		for (int k = 0 ; k < pageList.getLength() ; k++) {
			def pageElem = pageList.item(k)
			String id = pages[k].getName()
			int idx1 = id.lastIndexOf("_")
			int idx2 = id.lastIndexOf(".html")
			id = id.substring(idx1 + 1, idx2)
			pageElem.setAttribute("id", id)
		}

		if (pageList.getLength() != pages.size()) {
			println "Error size pageList.getLength() != pages.size()"+pageList.getLength()+", "+pages.size()
		}
	}
}

DomUtils.save(doc, PARAMS2)

if (!(PARAMS.delete() && PARAMS2.renameTo(PARAMS))) println "Warning can't rename file "+PARAMS2+" to "+PARAMS

