package org.txm.scripts.scripts;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Process tokenized text to find caesura and merge words
 */
class JoinCaesura{
	String caesuraTag = "lb|br"; // caesura
	String caesuraString = "-|$";
	String wordTag = "w";
	String intraWordTag = "hi";
	String extraWordTag = "p|s";

	public boolean process(Element root)
	{
		// find caesuraTags
		ArrayList<Element> currentNodes = new ArrayList<Element>();
		currentNodes.add(root);
		Element currentNode;
		while(currentNodes.size() > 0)
		{
			System.out.println(currentNodes);
			currentNode = currentNodes.remove(0);
			NodeList children = currentNode.getChildNodes();

			if(currentNode.getNodeName().matches(caesuraTag))
			{
				processCaesuraTag(currentNode);
			}

			//System.out.println("children: "+children.getLength());
			int c = 0;
			for(int i = 0 ; i < children.getLength() ; i++)
			{
				if(children.item(i).getNodeType() == Node.ELEMENT_NODE)
				{
					Element child = (Element)children.item(i);
					//System.out.println("child: "+child.getNodeName());
					if(!child.getNodeName().matches(wordTag)) // don't look in the words
					{
						currentNodes.add(c++, child);
					}
				}
			}

		}
		return true;
	}

	protected Node getPreviousWord(Element node)
	{
		int count = 0;
		Node previous = node.getPreviousSibling();
		while(previous != null)
		{
			
			if(previous.getNodeType() == Node.ELEMENT_NODE && previous.getNodeName().matches(wordTag))
			{
				//System.out.print("W:"+previous);
				return previous;
			}
			else if(previous.getNodeType() == Node.ELEMENT_NODE && previous.getNodeName().matches(intraWordTag))
			{
				//System.out.print("P:"+previous);
				if(isLastChildrenWord(previous))
				{
					//System.out.println("P: "+previous+" has a word");
					return previous;
				}
				else
				{
					//System.out.println("P: "+previous+" has no word");
				}
			}
			else if(previous.getNodeType() == Node.ELEMENT_NODE && previous.getNodeName().matches(extraWordTag))
			{
				return null;
			}
			previous = previous.getPreviousSibling();
			if(count++ >= 10)
				return null;
		}
		return null;
	}

	protected boolean isLastChildrenWord(Node node)
	{
		//System.out.println("test "+node);
		NodeList children = node.getChildNodes();
		for(int i = children.getLength() -1 ; i >= 0 ; i--) // begin by the last child
		{
			Node child = children.item(i);
			//System.out.println(" child "+child);
			if(child.getNodeType() == Node.ELEMENT_NODE && child.getNodeName().matches(wordTag))
				return true; // return the parent node
			else if(child.getNodeType() == Node.ELEMENT_NODE && child.getNodeName().matches(intraWordTag))
				return isLastChildrenWord(child);
		}
		return false;
	}
	
	protected boolean isFirstChildrenWord(Node node)
	{
		NodeList children = node.getChildNodes();
		for(int i = 0 ; i < children.getLength() ; i++) // begin by the last child
		{
			Node child = children.item(i);
			if(child.getNodeType() == Node.ELEMENT_NODE && child.getNodeName().matches(wordTag))
				return true; // return the parent node
			else if(child.getNodeType() == Node.ELEMENT_NODE && child.getNodeName().matches(intraWordTag))
				return isLastChildrenWord(child);
		}
		return false;
	}
	
	protected Node getNextWord(Element node)
	{
		int count = 0;
		Node next = node.getNextSibling();
		while(next != null)
		{
			if(next.getNodeType() == Node.ELEMENT_NODE && next.getNodeName().matches(wordTag))
				return next;
			else if(next.getNodeType() == Node.ELEMENT_NODE)
			{
				//System.out.print("P:"+previous);
				if(isFirstChildrenWord(next))
				{
					System.out.println("N: "+next+" has a word");
					return next;
				}
				else
				{
					System.out.println("N: "+next+" has no word");
				}
			}
			else if(next.getNodeType() == Node.ELEMENT_NODE && next.getNodeName().matches(extraWordTag))
			{
				return null;
			}
			next = next.getNextSibling();
			if(count++ >= 10)
				return null;
		}
		return null;
	}

	protected boolean processCaesuraTag(Element caesura)
	{
		Node previous = getPreviousWord(caesura);
		
		if(previous != null)
		{
			Node next = getNextWord(caesura);
			print(previous);
			print(caesura);
			print(next);
			if(next != null)
			{
				// process !!
			}
		}

		System.out.println("\n");
		return true;
	}

	protected void print(Node n)
	{
		if(n != null)
			switch(n.getNodeType())
			{
			case Node.ELEMENT_NODE:
				if(n.getNodeName().matches(wordTag))
					System.out.print("<"+n.getNodeName()+">"+n.getTextContent()+"</"+n.getNodeName()+">");
				else
					System.out.print("<"+n.getNodeName()+">");//+n.getTextContent()+"</"+n.getNodeName()+">");
				break;
			case Node.TEXT_NODE:
				n.getNodeValue();
				break;
			default:
			}
	}

	public static void main(String[] args)
	{
		File infile = new File(System.getProperty("user.home"), "xml/caesura/test.xml");
		File outfile = new File(System.getProperty("user.home"), "xml/caesura/test-c.xml");
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setXIncludeAware(true);
			DocumentBuilder constructeur;

			constructeur = factory.newDocumentBuilder();

			Document document = constructeur.parse(infile);
			Element root = document.getDocumentElement();
			//process the text
			JoinCaesura builder = new JoinCaesura();
			if(builder.process(root))
			{
				System.out.println("done");
			}
			else
			{
				System.out.println("error");
			}

			DomUtils.save(document, outfile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
