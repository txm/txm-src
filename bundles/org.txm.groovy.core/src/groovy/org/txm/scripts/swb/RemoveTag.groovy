package org.txm.scripts.sw

import java.io.File;

import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Element;
import java.io.File;

import org.w3c.dom.Document;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.txm.scripts.importer.graal.PersonalNamespaceContext;

class RemoveTag {

	public static boolean xpath(File xmlfile, String xpath)
	{
		if (!xmlfile.exists()) {
			println "Error: $xmlfile does not exists"
		}

		if (!(xmlfile.canRead() && xmlfile.canWrite())) {
			println "Error: $xmlfile is not readable or writable"
		}

		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		domFactory.setXIncludeAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		Document doc = builder.parse(xmlfile);

		XPathFactory xpathfactory = XPathFactory.newInstance();
		def _xpath = xpathfactory.newXPath();
		_xpath.setNamespaceContext(new PersonalNamespaceContext());
		def expr = _xpath.compile(xpath);
		def nodes = expr.evaluate(doc, XPathConstants.NODESET);

		for(Element node : nodes) {
			def parent = node.getParentNode();
			if (parent != null)
				parent.removeChild(node)
		}
		return DomUtils.save(doc, xmlfile)
	}

	public static boolean xpath(File xmlfile, def xpaths)
	{
		if (!xmlfile.exists()) {
			println "Error: $xmlfile does not exists"
		}

		if (!(xmlfile.canRead() && xmlfile.canWrite())) {
			println "Error: $xmlfile is not readable or writable"
		}

		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true); // never forget this!
		domFactory.setXIncludeAware(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		Document doc = builder.parse(xmlfile);
		XPathFactory xpathfactory = XPathFactory.newInstance();

		for (String xpath : xpaths) {
			def _xpath = xpathfactory.newXPath();
			_xpath.setNamespaceContext(new PersonalNamespaceContext());
			def expr = _xpath.compile(xpath);
			def nodes = expr.evaluate(doc, XPathConstants.NODESET);

			for (Element node : nodes) {
				def parent = node.getParentNode();
				if (parent != null)
					parent.removeChild(node)
			}
		}
		return DomUtils.save(doc, xmlfile)

	}
	public static void main(String[] args) {
		File dir = new File("/home/mdecorde/Bureau/matrice/témoignages")
		File infile = new File(dir, "tei2.xml")
		File outfile = new File(dir, "tei2-out.xml")

		def doc = DomUtils.load(infile);
		def addList = doc.getDocumentElement().getElementsByTagName("add")
		println "nb <add> to delete: "+addList.getLength()
		for(int i = 0 ; i < addList.getLength() ; i++) {
			Element node = addList.item(i);
			String text = node.getTextContent().trim();
			Element parent = node.getParentNode();
			parent.insertBefore(doc.createTextNode(text), node)
			parent.removeChild(node);
			i--
		}

		def delList = doc.getDocumentElement().getElementsByTagName("del")
		println "nb <del> to delete: "+delList.getLength()
		for(int i = 0 ; i < delList.getLength() ; i++) {
			Element node = delList.item(i);
			node.getParentNode().removeChild(node);
			i--
		}

		DomUtils.save(doc, outfile);
	}
}
