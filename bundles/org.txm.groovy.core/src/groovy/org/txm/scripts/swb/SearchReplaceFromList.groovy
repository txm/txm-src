package org.txm.scripts.sw
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author mdecorde
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2012-10-01 08:24:58 +0200 (lun., 01 oct. 2012) $
// $LastChangedRevision: 2269 $
// $LastChangedBy: sheiden $
//

// parameters

file = new File("/home/mdecorde/xml/apollinaire/apollinaire.xml")
out = new File("/home/mdecorde/xml/apollinaire/apo/apollinaire.xml")
def changesFind = [ "</recueil>",	"<paragraphe>", "</paragraphe>", "<racine>",	"</racine>", "<interp",	"</interp>", "<TEI xmlns=\"http://www.tei-c.org/ns/1.0\" xmlns:txm=\"http://textometrie.org/1.0\">"	]
def changesReplace = [ "</recueil><pb/>", "<p>", "</p><lb/>", '<text id ="apollinaire">', '</text',	'<txm:ana',	'</txm:ana>', """<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:txm="http://textometrie.org/1.0">
<teiHeader type="text">
<fileDesc>
<titleStmt>
<title>null</title>
<respStmt>
<resp id="cordial">initial tagging</resp></respStmt>
</titleStmt>
</fileDesc>
<encodingDesc>
<classDecl>
<taxonomy id="pos"><bibl type="tagset"/></taxonomy>
<taxonomy id="func"><bibl type="tagset"/></taxonomy>
<taxonomy id="lemma"><bibl type="tagset"/></taxonomy>
</classDecl>
</encodingDesc>
</teiHeader>""" ]

// main body
out.write('')                                // create empty file
out.withWriter { writer ->
	String text = file.getText("UTF-8");
	for (int i = 0 ; i < changesFind.size() ; i++) {
		println "loop $i: "+changesFind[i] +" >> "+ changesReplace[i]
		text = text.replaceAll(changesFind[i], changesReplace[i])
	}
	out.write(text)
}
