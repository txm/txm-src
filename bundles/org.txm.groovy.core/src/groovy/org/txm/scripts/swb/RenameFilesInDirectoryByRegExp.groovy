package org.txm.scripts.sw
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2012-02-01 17:58:45 +0100 (mer., 01 févr. 2012) $
// $LastChangedRevision: 2122 $
// $LastChangedBy: mdecorde $
//
import org.txm.scripts.importer.*

// PARAMETERS

File dir = new File("/home/mdecorde/TEMP/atelier")
String searchRegexp = "(.+).txt"
String replaceRegexp = 't$1.txt' // WARNING: Use simple quotes only

// END OF PARAMETERS

// main body
dir.eachFileMatch(~/.*.txt/){ file -> // for each file with extension "txt"
	String filename = file.getName()
	filename = filename.replaceAll(searchRegexp, replaceRegexp)
	println "Rename file '$file' to '$filename'"
	file.renameTo(new File(file.getParent(), filename))
}
