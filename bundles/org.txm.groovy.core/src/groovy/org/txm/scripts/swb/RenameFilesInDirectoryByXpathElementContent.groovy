package org.txm.scripts.sw
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 10:05:14 +0200 (jeu. 02 mai 2013) $
// $LastChangedRevision: 2377 $
// $LastChangedBy: sheiden $
//
import org.txm.scripts.importer.*

// parameters

// **change this parameter**
dir = new File(System.getProperty("user.home"), "TXM/sources/Factiva")

// **change this parameter**
xpath = "/article/@date"

// main body
dir.eachFileMatch(~/.*.xml/){ file -> // for each file
	file.renameTo(new File(file.getParent(), XPathResult.getXpathResponse(file, xpath)+"_"+file.name));
}
