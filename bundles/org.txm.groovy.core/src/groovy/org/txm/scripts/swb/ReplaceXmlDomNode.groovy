// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.scripts.sw;

import java.io.File;

import javax.xml.namespace.NamespaceContext;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import org.xml.sax.SAXException;
import javax.xml.stream.*;
import javax.xml.xpath.*;

import org.txm.sw.ReplaceXmlDomNode;
import org.txm.scripts.importer.graal.PersonalNamespaceContext;
import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

// TODO: Auto-generated Javadoc
/**
 * The Class ReplaceXmlDomNode.
 */
class ReplaceXmlDomNode
{
	
	/** The ns context. */
	NamespaceContext nsContext = new PersonalNamespaceContext();
	
	/**
	 * Instantiates a new replace xml dom node.
	 *
	 * @param src the src
	 * @param target the target
	 * @param xpathsrc the xpathsrc
	 * @param xpathtarget the xpathtarget
	 */
	public ReplaceXmlDomNode(File src, File target, String xpathsrc, String xpathtarget)
	{
		def factory = DocumentBuilderFactory.newInstance();
		factory.setXIncludeAware(true)
		def builder = factory.newDocumentBuilder()
		
		def docsrc = builder.parse(src);
		def doctarget = builder.parse(target);
		
		Node srcnode = null;
		Node targetnode = null;
		
		factory = XPathFactory.newInstance();
		XPath XpathObj = factory.newXPath();
		XpathObj.setNamespaceContext(nsContext);
		
		XPathExpression expr = XpathObj.compile(xpathsrc);
		for(Node node : (NodeList) expr.evaluate(docsrc, XPathConstants.NODESET))
		{
			srcnode = node;
			break;
		}
		
		XPathExpression expr2 = XpathObj.compile(xpathtarget);
		for(Node node : (NodeList) expr2.evaluate(doctarget, XPathConstants.NODESET))
		{
			targetnode = node;
			break;
		}
		
		//println "replace : "+targetnode
		//println "by : "+srcnode
		if(targetnode != null && srcnode != null)
		{
			Node parent = targetnode.getParentNode();
			parent.removeChild(targetnode);
			parent.appendChild(doctarget.importNode(srcnode, true));
			
			save(doctarget, target);
		}
		else
		{
			println "Error: "
			println "target node = "+targetnode
			println "src node = "+srcnode
		}
		
	}
	
	/**
	 * Save.
	 *
	 * @param doc the doc
	 * @param outfile the outfile
	 * @return true, if successful
	 */
	private boolean save(def doc, File outfile)
	{
		try {
			// Création de la source DOM
				Source source = new DOMSource(doc);
			
			// Création du fichier de sortie
				Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8")); 
			Result resultat = new StreamResult(writer);
			
			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			
			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			doc = null;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		String home = System.getProperty("user.home")
		File src = new File(home, "xml/subberoul/subcorpus0000.xml")
		File target = new File(home, "xml/subberoul/master.xml")
		String xpathsrc = "//subcorpus"
		String xpathtarget = "//subcorpus[@name=\"subcorpus0000\"]"
		
		new ReplaceXmlDomNode(src, target, xpathsrc, xpathtarget)
	}
}