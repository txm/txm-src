package org.txm.scripts.sw
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2012-02-01 17:58:45 +0100 (mer., 01 févr. 2012) $
// $LastChangedRevision: 2122 $
// $LastChangedBy: mdecorde $
//

import static groovy.io.FileType.*

// PARAMETERS

// dir
def dir = new File("/home/mdecorde/TEMP/atelier")

// inputEncoding
def inputEncoding = "UTF-8"

// outputEncoding
def outputEncoding = "UTF-8"

// head
def head = "<?xml version=\"1.0\" encoding=\"${outputEncoding}\"?>\n<discours>\n" // can be empty

// tail
def tail = "</discours>\n"; // can be empty

// fileExtension
def fileExtension = "txt"

//END OF PARAMETERS

// filePattern
def filePattern = ".*\\.$fileExtension\$"

// main body
dir.eachFileMatch(~/$filePattern/) { inputFile ->				// for each file matching extension
    def outputTmp = File.createTempFile("AddHeadInDirectoryTemp", '.tmp', dir) // create temporary file
    outputTmp.withWriter(outputEncoding) { writer ->
    if (head != null && head.length() > 0)
    	writer.print(head)
	inputFile.withReader(inputEncoding) { reader ->
			reader.eachLine { inputLine ->               		// for each line
				writer.println inputLine
			}
		}
	if (tail != null && tail.length() > 0)
		writer.print(tail)
	writer.close()
    }
	assert outputTmp.renameTo(inputFile)						// save results
}		
