package org.txm.scripts.sw

// PARAMETERS //

infile = new File("/home/mdecorde/xml/test/grep")			// the file to process
outdir = new File("/home/mdecorde/xml/test/result")			// the directory which received result files
regex = /(<\?(.+))/           						// matching line test
prefix = "prefix"							// result files prefix
replace = '$2'								// use the matching line to build the filename
ext = ".xml"                                     			// result files extension
includeSeparatorLine = true						// should include matching line in result files
writeAtStartup = false							// should create files only if a matching line is found
encoding = "UTF-8"
// CALL //
splitByRegExp(infile, outdir, 
//	regex, prefix, replace, ext,
	regex, prefix, null, ext, 
	includeSeparatorLine, writeAtStartup)


// PUBLIC METHODS //
def splitByRegExp(def infile, def outdir, def regex, def prefix, def replace, def ext, def includeSeparatorLine, def writeAtStartup) {
	int count = 1;
	outdir.deleteDir()
	outdir.mkdirs()
	if (!outdir.exists()) return false
	if (!infile.isFile()) return false

	File outfile = null;
	def writer = null

	if (writeAtStartup) {
		if (replace == null) {
			outfile = new File(outdir, prefix+(count++)+ext)
		}
		writer = outfile.newWriter(encoding)
	}
	infile.eachLine(encoding) { line ->
		if (line ==~ regex) { // new file
			if (replace == null) {
				outfile = new File(outdir, prefix+(count++)+ext)
			} else {
				String name = prefix+line.replaceAll(regex, replace)+ext
				outfile = new File(outdir, name)
			}
			if (writer != null)	writer.close() // close previous writer
			writer = outfile.newWriter(encoding)
			println "new file $outfile"
			if (includeSeparatorLine) {
				writer.println line
			}
		} else {
			if (writer != null)
				writer.println line
		}
	}
	if (writer != null)	writer.close()
}
