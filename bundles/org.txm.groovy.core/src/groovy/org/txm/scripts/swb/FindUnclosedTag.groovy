package org.txm.scripts.scripts

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;

File xmlFile = new File("/home/mdecorde/xml/errorxml/error.xml")

def inputurl = xmlFile.toURI().toURL();
def inputData = inputurl.openStream();
def factory = XMLInputFactory.newInstance();
def parser = factory.createXMLStreamReader(inputData);

opened = []
locations = []
structPath = "/"

try {
for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
	switch (event) {
		case XMLStreamConstants.START_ELEMENT:
		String localname = parser.getLocalName();
			startElement(localname, parser.getLocation().getLineNumber());
		break;
		case XMLStreamConstants.END_ELEMENT:
			String localname = parser.getLocalName();
			endElement(localname);
		break
	}
}
} catch(Exception e) {
	println "** Error while parsing: "+e
	println "** Error occured at : "+structPath+" line "+parser.getLocation().getLineNumber()
	println "** Opened tags: "+opened
	println "** at lines: "+locations
}

if (parser != null) parser.close();
if (inputData != null) inputData.close();

public boolean startElement(String localname, def loc) {
	println structPath
	structPath += localname+"/"
	opened << localname
	locations << loc
	return true;
}

public boolean endElement(String localname) {
	if (structPath.length() > 1) {
		def lastTag = opened[-1]
		if (localname != lastTag) {
			println "ERROR: closing tag '$localname' does not match last opened tag '$lastTag'"
			println opened
			return false;
		}
	
		int idx = structPath.substring(0,structPath.length()-1).lastIndexOf("/");
		if (idx > 0)
			structPath = structPath.substring(0, idx+1)
		//
		opened.pop()
		locations.pop()
			
		return true
	}
}