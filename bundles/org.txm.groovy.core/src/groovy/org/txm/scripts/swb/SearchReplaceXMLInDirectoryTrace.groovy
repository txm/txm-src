package org.txm.scripts.sw
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 10:05:14 +0200 (jeu. 02 mai 2013) $
// $LastChangedRevision: 2377 $
// $LastChangedBy: sheiden $
//

// parameters

dir = new File("/home/sheiden/TXM/sources/Factiva") // **change this parameter**
ext = "\\.xml"                                      // **change this parameter**
find = /date="([0-9]+)-([0-9]+-[0-9]+)"/            // **change this parameter**
replaceWith = 'date="$1-$2" year="$1"'              // **change this parameter**

// main body
println "SearchReplaceInDirectory: processing directory '$dir'..."
dir.eachFileMatch(~/.*$ext/){ file ->               // for each file matching extension
    println "SearchReplaceInDirectory: processing file '$file'..."
    println "XML syntax initial check"
    if (org.txm.importer.ValidateXml.test(file)) {
        println "Search&Replace"
        def tmp = File.createTempFile("SearchReplaceInDirectoryTemp", ".xml", dir) // create temporary file
        tmp.write('')                                // create empty file
        tmp.withWriter { writer ->
            file.eachLine { line ->                  // for each line
                writer.println line.replaceAll(find, replaceWith) // find&replace and print
            }
        }
        tmp.renameTo(file)                           // save results
        println "XML syntax final check"
        if (!org.txm.importer.ValidateXml.test(file)) {
            println "** Warning: bad XML syntax for result file '$file'"
        }
    } else {
        println "Skipping file '$file'"
    }
}
