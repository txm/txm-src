package org.txm.scripts.scripts


import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import javax.xml.stream.*;
import java.net.URL;

import org.txm.scripts.importer.*;

/**
 * Counts the number of chars in tokens in a specific element
 * NB: Tokens are separated by whitespaces
 * 
 * Can set attribute filter
 * @author mdecorde
 */
class CountNumberOfChar {
	int total = 0;
	int nbWords = 0;
	String elem = "w";
	String attribute = null;
	String regex = null;

	/**
	 * @param dir a directory
	 * @param elem the element to focus on
	 */
	public CountNumberOfChar(File dir, String elem, String attribute, String regex) {
		this.elem = elem;
		this.attribute  = attribute;
		this.regex = regex;

		for (File f : dir.listFiles())
			if (ValidateXml.test(f))
				countInXML(f)
	}

	protected void countInXML(File f)
	{
		println "count: "+f
		boolean startCount = false
		try {
			URL url = f.toURI().toURL();
			def inputData = url.openStream();
			def factory = XMLInputFactory.newInstance();
			XMLStreamReader parser = factory.createXMLStreamReader(inputData);
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next())
			{
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:

						if (parser.getLocalName() == elem) {
							if (attribute == null || regex == null) {
								startCount = true;
								nbWords++
							} else {
								String attrvalue = parser.getAttributeValue(null, attribute)
								if (attrvalue != null && attrvalue.matches(regex)) {
									startCount = true;
									nbWords++
								}
							}
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						if (parser.getLocalName() == elem) {
							startCount = false
						}
						break;
					case XMLStreamConstants.CHARACTERS:
						if (startCount) {
							for (String tok : parser.getText().trim()) {
								total += tok.length()
							}
							
						}
				}
			}
			
			if (parser != null) parser.close();
			if (inputData != null) inputData.close();
		}
		catch(Exception e){e.printStackTrace();}
	}

	public float getMean() {
		return ((float)total)/(float)nbWords;
	}

	public int getNbWords() {
		return nbWords;
	}

	public int getTotal() {
		return total;
	}

	public static void main(def args)
	{
		CountNumberOfChar c = new CountNumberOfChar(new File(System.getProperty("user.home")+"/TXM/corpora/bfm1/txm"), "form", null, null)
		//CountNumberOfChar c = new CountNumberOfChar(new File(System.getProperty("user.home")+"/TXM/corpora/bfm1/txm"), "form", "type", "pon")
		System.out.println("Moyenne: "+c.getMean());
	}
}
