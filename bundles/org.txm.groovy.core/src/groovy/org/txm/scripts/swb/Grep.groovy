package org.txm.scripts.scripts
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2012-02-01 17:58:45 +0100 (mer., 01 févr. 2012) $
// $LastChangedRevision: 2122 $
// $LastChangedBy: mdecorde $
//

// PARAMETERS //
outfile = new File("/home/mdecorde/xml/test/grep")
dir = new File("/home/mdecorde/xml/test/")			// **change this parameter**
ext = "\\.xml"                                      // **change this parameter**
find = /(<...+)/           							// **change this parameter**
replaceWith = 'gp1=$1'             				// **change this parameter**

// MAIN //

grep(dir, outfile, ext, find, replaceWith) // process a directory
//grep(dir, outfile, null, find, replaceWith) // process a directory with no constraint on files
//grep(dir, outfile, ext, find, null) // directory + don't replace

//grep(infile, outfile, find, replaceWith) // process a file
//grep(dir, outfile, find, null) // file + don't replace

// PUBLIC METHODS //

// to process a directory
def grep(File dir, File outfile, def ext, def regex, def replace) {
	if (dir.isDirectory()) {
		outfile.createNewFile()
		if (ext == null) ext = /.+/
		outfile.write('')                            // create empty file
		outfile.withWriter { writer ->
			dir.eachFileMatch(~/.*$ext/){ file ->    // for each file matching extension
				_grep(file, writer, regex, replace)
			}
		}
		return true;
	} else {
		println "$outfile is not a directory"
		return false;
	}
}

// to process a file with outfile
def grep(File file, File outfile, def regex, def replace) {
	if (file.isDirectory()) return false;
	outfile.withWriter { writer ->
		_grep(file, writer, regex, replace)
	}
	return true;
}

// PRIVATE METHODS //

// to process a file with writer
private def _grep(def file, def writer, def regex, def replace) {
	file.eachLine { line ->
		_testLine(line, writer, regex, replace)
	}
}

private def _testLine(String line, def writer, def regex, def replace) {
	if (replace == null) {
		if (line.matches(find))
			writer.println line
	} else {
		if (line.matches(find))
			writer.println line.replaceAll(regex, replace)
	}	
}