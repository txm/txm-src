package org.txm.scripts.sw
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2012-02-01 17:58:45 +0100 (mer., 01 févr. 2012) $
// $LastChangedRevision: 2122 $
// $LastChangedBy: mdecorde $
//

// PARAMETERS

File dir = new File("/home/mdecorde/TEMP/atelier") // the directory containing the files
String fromEncoding = "UTF-8" // the sources encoding
String toEncoding = "windows-1252" // the final encoding

// END OF PARAMETERS

def processDirectory(File dir, String fromEncoding, String toEncoding) {
	dir.eachFileMatch(~/.*.txt/){ file -> // for each file with the "txt" extension
		processFile(file, fromEncoding, toEncoding)
	}
}

def processFile(File file, String fromEncoding, String toEncoding) {
	println "Convert file: $file"
	String text = file.getText(fromEncoding);
	file.withWriter(toEncoding) {writer -> // read with the "fromEncoding"
		writer.write(text) // write with the "toEncoding"
	}
}

processDirectory(dir, fromEncoding, toEncoding)
println "Done"