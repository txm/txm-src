package org.txm.scripts.utils

File workspace = new File("/home/mdecorde/workspace047")
def langs = ["fr", "ru"]
for (File project : workspace.listFiles()) {
	if (!project.isDirectory()) continue;
	
	File osgiinf = new File(project, "OSGI-INF/l10n")
	if (!osgiinf.exists()) continue;
	
	println project.getName()
	File defaultFile = new File(osgiinf, "bundle.properties")
	
	for (def lang : langs) {
		File langFile = new File(osgiinf, "bundle_"+lang+".properties");
		if (!langFile.exists()) {
			langFile.createNewFile()
		}
	}
}