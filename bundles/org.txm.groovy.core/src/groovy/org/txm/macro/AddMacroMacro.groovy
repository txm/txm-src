// Copyright © 2022 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.Toolbox;
import org.txm.utils.io.FileCopy
import org.txm.rcp.views.fileexplorer.MacroExplorer

// BEGINNING OF PARAMETERS

@Field @Option(name="groovyFiles", usage="Groovy files to install", widget="FilesOpen", metaVar="*.groovy", required=true, def="")
def groovyFiles

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

File rootDir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/")

for (File f : groovyFiles) {

	if (!f.isFile()) continue;
	
	if (f.getName().endsWith(".jar")) {
	
		File f2 = new File(rootDir, "lib/"+f.getName())
		if (f2.exists()) {
			println "Updating library $f2"
			f2.delete()
		} else {
			println "New library: $f2"
		}
		FileCopy.copy(f, f2)
		
	} else if (f.getName().endsWith(".groovy")) {
	
		String ppackage = "org.txm.macro"
		def content = f.readLines("UTF-8")
		for (def line : content) {
			if (line.startsWith("package org.txm.macro")) {
				ppackage = line.substring(8)
				break
			}
		}
		
		File f2 = new File(rootDir, "user/"+ppackage.replace(".", "/")+ "/"+ f.getName())
		if (f2.exists()) {
			println "Update macro: $f2"
			f2.delete()
		} else {
			println "New macro: $f2"
		}
		f2.getParentFile().mkdirs()
		
		FileCopy.copy(f, f2)
		
	} else {
		println "Ignoring $f"
	}
}

// update macros view
monitor.syncExec(new Runnable() {
		public void run() {
			MacroExplorer.refresh();
		}
	});
