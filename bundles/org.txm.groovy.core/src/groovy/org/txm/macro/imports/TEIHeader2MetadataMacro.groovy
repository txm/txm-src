// Copyright © 2017 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

package org.txm.macro.imports

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.scripts.importer.XPathResult

@Field @Option(name="inputDirectory", usage="input directory (contains XML files)", widget="Folder", required=true, def="input")
def inputDirectory

@Field @Option(name="propertiesFile", usage="metadata=XPath properties input file", widget="File", required=true, def="import.properties")
File propertiesFile

@Field @Option(name="CSVoutputFile", usage="CSV output file", widget="File", required=true, def="metadata.csv")
File CSVoutputFile

@Field @Option(name="columnSeparator", usage="column separator for CSV file", widget="String", required=true, def=",")
def columnSeparator

@Field @Option(name="textSeparator", usage="text separator for CSV file", widget="String", required=true, def="\"")
def textSeparator

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

writer = CSVoutputFile.newWriter("UTF-8")

metadataXPaths = new Properties()
if (propertiesFile.exists() && propertiesFile.canRead()) {
	InputStreamReader input = new InputStreamReader(new FileInputStream(propertiesFile) , "UTF-8")
	metadataXPaths.load(input)
	input.close()
} else {
	println "** TeiHeader2MetadataCSV: '$propertiesFile' file not found."
	return
}

List<File> files = inputDirectory.listFiles().sort{ it.name }
if (files == null || files.size() == 0) {
	println "** TeiHeader2MetadataCSV: no files found in '$inputDirectory' directory."
	return
}

for (int i = 0 ; i < files.size() ; i++)
	if (!(files.get(i).getName().endsWith(".xml")) || files.get(i).getName().equals("import.xml"))
		files.remove(i--);

if (files.size() == 0) {
	println "** TeiHeader2MetadataCSV: no usefull files found in '$inputDirectory' directory."
	return
}

// println "files = "+files

writer << "id"+columnSeparator

metadataNames = metadataXPaths.keySet().sort()

isFirst = true
metadataNames.each {

	if (isFirst) { isFirst = false
	} else {
		writer << columnSeparator
	}
	writer << it
}
writer << "\n"

for (File f : files) {
	filename = f.getName()
	filename = filename.substring(0, filename.lastIndexOf("."))
	writer << filename+columnSeparator

	metadataValues = new HashMap<String, String>()
	def xpathprocessor = new XPathResult(f)
	metadataNames.each { name ->
		value = xpathprocessor.getXpathResponse(metadataXPaths.get(name), "N/A")
		value = value.trim().replaceAll(/[ \t\n]+/, " ")
		metadataValues.put(name, value)
		// println sprintf("%s: %s = %s", filename, name, value)
	}
	xpathprocessor.close()

	isFirst = true
	metadataNames.each {

		if (isFirst) { isFirst = false
		} else {
			writer << columnSeparator
		}
		writer << textSeparator+metadataValues.get(it)+textSeparator
	}
	writer << "\n"
}

writer.close()

println "Done: "+CSVoutputFile.getAbsolutePath()