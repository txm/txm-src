package org.txm.macro.imports

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.commands.*
import org.txm.Toolbox

@Field @Option(name="scriptFile", usage="an example file", widget="File", required=true, def="xxxLoader.groovy")
File scriptFile

@Field @Option(name="doUpdate", usage="select to update the corpus", widget="Boolean", required=true, def="false")
def doUpdate

if (!ParametersDialog.open(this)) return;

File script = scriptFile // if not done, using scriptFile raise a Cast exception to 'groovy.lang.Reference'

if (!script.getName().endsWith(".groovy")) {
	println "Error: the selected file is not a Groovy script"
	return
}

println "Opening import form with $scriptFile"
monitor.syncExec(new Runnable() {
	public void run() {
		try {
			String path = script.getAbsolutePath()
			String rootpath = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/scripts/importer")
	
			if (path.startsWith(rootpath)) {
				OpenImportForm.open(script, doUpdate);
			} else {
				println "Error: import start script must be included in $rootpath"
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
});