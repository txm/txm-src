// STANDARD DECLARATIONS
package org.txm.macro.stats

import org.txm.ca.core.functions.CA
import org.txm.statsengine.r.core.RWorkspace
import groovy.transform.Field
// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof CA)) {
	println "Selection is not a CA. Please select a CA result in the Corpus view"
	return;
}

def ca = corpusViewSelection

@Field @Option(name="outputFile", usage="an example file", widget="FileSave", required=true, def="file.svg")
def outputFile

@Field @Option(name="draw", usage="'row' or 'col'", widget="String", required=true, def="row")
def draw
// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

def s = ca.getRSymbol()
def RW = RWorkspace.getRWorkspaceInstance()

def script = """
plot($s);
ellipseCA($s, ellipse=c("$draw"));
"""

RW.plot(outputFile, script);

println "Done: "+outputFile.getAbsolutePath()