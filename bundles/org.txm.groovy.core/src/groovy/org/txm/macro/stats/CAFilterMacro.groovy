// STANDARD DECLARATIONS
package org.txm.macro.stats

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.clientExceptions.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.apache.commons.lang.time.StopWatch
import java.util.Arrays
import org.jfree.chart.renderer.xy.*
import org.jfree.chart.renderer.*
import org.jfree.chart.plot.*
import org.jfree.data.xy.*
import org.jfree.chart.axis.*
import java.awt.*;
import java.awt.geom.*;
import org.jfree.chart.labels.*

import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.*
import org.txm.ca.rcp.editors.*
import org.txm.libs.office.ReadODS
import org.txm.ca.core.chartsengine.jfreechart.datasets.*
import org.jfree.chart.renderer.AbstractRenderer

import org.apache.commons.math3.stat.descriptive.*

println "editor: "+editor

if (!(editor instanceof CAEditor)) {
	println "editor is not a CA editor: $editor, Run the macro with F12 when the editor is selected :-)"
	return
}

def chartEditor = editor.getEditors()[0]
def chartComposite = chartEditor.getComposite()

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

//@Field @Option(name="query", usage="an example query", widget="Query", required=true, def='[pos="V.*"]')
//def query

//@Field @Option(name="file", usage="an example file", widget="File", required=true, def="")
//def file

//@Field @Option(name="folder", usage="an example folder", widget="Folder", required=false, def="")
//def folder

//@Field @Option(name="date", usage="an example date", widget="Date", required=false, def="1984-09-01")
//def date

//@Field @Option(name="integer", usage="an example integer", widget="Integer", required=false, def="42")
//def integer

@Field @Option(name="QMin", usage="min Q", widget="Float", required=true, def="0.01")
def QMin

@Field @Option(name="CTRXMin", usage="min CTR X", widget="Float", required=true, def="0.001")
def CTRXMin

@Field @Option(name="CTRYMin", usage="min CTR Y", widget="Float", required=true, def="0.001")
def CTRYMin

@Field @Option(name="stackX", usage="stack X", widget="Float", required=true, def="0.0")
def stackX

@Field @Option(name="stackY", usage="stack Y", widget="Float", required=true, def="0.0")
def stackY

//@Field @Option(name="string", usage="an example string", widget="String", required=false, def="hello world!")
//def string

@Field @Option(name="regexFilter", usage="row property form regex", widget="Text", required=false, def="")
def regexFilter

@Field @Option(name="debug", usage="debug (verbose) mode", widget="Boolean", required=true, def="true")
def debug

// La fenêtre de résultats
CAresultWindow = editor

// Le tableau de données d'aide à l'interprétation
// Les données de l'AFC sont manipulables par les méthodes documentées ici http://txm.sourceforge.net/javadoc/TXM/TBX/org/txm/stat/engine/r/function/CA.html

ica = CAresultWindow.getCA()
ca = ica.getCA()

rowNames = ica.getRowNames()
rowCos2 = ca.getRowCos2()
rowContrib = ca.getRowContrib()

colNames = ica.getColNames()
colCos2 = ca.getColCos2()
colContrib = ca.getColContrib()

F1 = ica.getFirstDimension()-1
F2 = ica.getSecondDimension()-1

println sprintf("F1 = %d, F2 = %d, %d initial rows, %d initial cols", F1+1, F2+1, rowNames.length, colNames.length)

def stats = { vector, factor ->
	s = new DescriptiveStatistics()
	(vector.length).times {
		s.addValue(vector[it][factor]);
	}

	println "min       quartile  median    third quartile  max"
	println sprintf("%8f  %8f  %8f  %8f        %8f", s.getMin(), s.getPercentile(25), s.getPercentile(50), s.getPercentile(75), s.getMax())
}

// rows

println "ROWS -----"
println "CTR"+(F1+1)
stats(rowContrib, F1)

println "CTR"+(F2+1)
stats(rowContrib, F2)

println "Cos² "+(F1+1)
stats(rowCos2, F1)

println "Cos² "+(F2+1)
stats(rowCos2, F2)

// cols

println "COLS -----"
println "CTR"+(F1+1)
stats(colContrib, F1)

println "CTR"+(F2+1)
stats(colContrib, F2)

println "Cos² "+(F1+1)
stats(colCos2, F1)

println "Cos² "+(F2+1)
stats(colCos2, F2)


def stats2 = { vector, factor1, factor2 ->
	s = new DescriptiveStatistics()
	(vector.length).times {
		s.addValue(vector[it][factor1]+vector[it][factor2]);
	}

	println "min       quartile  median    third quartile  max"
	println sprintf("%8f  %8f  %8f  %8f        %8f", s.getMin(), s.getPercentile(25), s.getPercentile(50), s.getPercentile(75), s.getMax())
}

println "Q"+(F1+1)+(F2+1)
stats2(rowCos2, F1, F2)

println "Q"+(F1+1)+(F2+1)
stats2(colCos2, F1, F2)

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS


filteredRows = []

println ""

if (debug) println "\nROWS -----"

if (regexFilter.length() > 0) {
	nmatched = 0
	rowNames.each { it ->
		if (it ==~ regexFilter) {
			if (debug && !filteredRows.contains(it)) {
				if (nmatched > 0) print ", "
				print it
			}
			filteredRows << it
			nmatched++
		}
	}

	if (debug && nmatched > 0) println ""

	if (debug) {
		println sprintf("\n%d rows filtered by regex", nmatched)
	}

}

nQCTR = 0
rowNames.eachWithIndex { it, i ->
	def cos2 = rowCos2[i]
	if ((rowContrib[i][F1] < CTRXMin) && (rowContrib[i][F2] < CTRYMin) && (cos2[F1] + cos2[F2] < QMin)) {
		if (debug && !filteredRows.contains(rowNames[i])) {
			if (nQCTR > 0) print ", "
			print rowNames[i]
		}
		filteredRows << rowNames[i]
		nQCTR++
	}
}

if (debug) {
	println sprintf("\n%d rows filtered by CTR or Q"+(F1+1)+(F2+1), nQCTR)
}

// cols

filteredCols = []

println ""

if (debug) println "\nCOLS -----"

if (regexFilter.length() > 0) {
	nmatched = 0
	colNames.each { it ->
		if (it ==~ regexFilter) {
			if (debug && !filteredCols.contains(it)) {
				if (nmatched > 0) print ", "
				print it
			}
			filteredCols << it
			nmatched++
		}
	}

	if (debug && nmatched > 0) println ""

	if (debug) {
		println sprintf("\n%d cols filtered by regex", nmatched)
	}

}

nQCTR = 0
colNames.eachWithIndex { it, i ->
	def cos2 = colCos2[i]
	if (!((colContrib[i][F1] > CTRXMin) || (colContrib[i][F2] > CTRYMin) || (cos2[F1] + cos2[F2] > QMin))) {
		if (debug && !filteredCols.contains(colNames[i])) {
			if (nQCTR > 0) print ", "
			print colNames[i]
		}
		filteredCols << colNames[i]
		nQCTR++
	}
}

if (debug) {
	println sprintf("\n%d cols filtered by CTR or Q"+(F1+1)+(F2+1), nQCTR)
}

double stackXv = stackX
double stackYv = stackY


// Visualisation graphique

chartCAresultWindow = CAresultWindow.getEditors()[0]
chartComposite = chartCAresultWindow.getComposite()

monitor.syncExec( new Runnable() {
	public void run() {

				println chartComposite
				def chart = chartEditor.getChart();

				dataset2 = new CAXYDataset(ica)
				
				labels = dataset2.rowLabels
				coords = dataset2.rowCoordinates
				(labels.length).times {
					if (filteredRows.contains(labels[it])) {
						println "Moving "+labels[it]+" row to origin."
						labels[it] = ""
						coords[it][F1] = stackXv
						coords[it][F2] = stackYv
					}
				}
				labels = dataset2.columnLabels
				coords = dataset2.columnCoordinates
				(labels.length).times {
					if (filteredCols.contains(labels[it])) {
						println "Moving "+labels[it]+" col to origin."
						labels[it] = ""
						coords[it][F1] = stackXv
						coords[it][F2] = stackYv
					}
				}

				chart.getXYPlot().setDataset(dataset2)

				ica.getChartCreator().getChartsEngine().getJFCTheme().apply(chart); // need to be call AFTER setRenderer() cause this method changes some renderering parameters
				chartComposite.loadChart()
			}
})
