import org.txm.statsengine.r.core.*

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.commands.OpenBrowser

@Field @Option(name="n_classes", usage="classes number", widget="Integer", required=true, def="10")
def n_classes

//@Field @Option(name="nClassMax", usage="maximum class number", widget="Integer", required=true, def="10")
//def nClassMax

@Field @Option(name="factor", usage="factor number: 1..n", widget="Integer", required=true, def="1")
def factor

def ca = corpusViewSelection
if (editor != null && !ca.getClass().getName().equals("org.txm.ca.core.functions.CA")) {
    ca = editor.getResult()
}
if (!ca.getClass().getName().equals("org.txm.ca.core.functions.CA")) {
    println "Corpus view selection is not a CA and no CA editor is open."
    return
}
ca.compute()

def r = RWorkspace.getRWorkspaceInstance()

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

double[][] allContribs = ca.getRowInfos().getAllContribs()

Double[] contribs = new Double[allContribs.length]

for (int i = 0 ; i < allContribs.length ; i++) contribs[i] = allContribs[i][ca.getFirstDimension() - 1]; // de 0 à n -1 dim

Arrays.sort(contribs, Collections.reverseOrder())

r.addVectorToWorkspace("contrib", contribs)

/* hclust(method)

Name										Formula

"ward.D"
"ward.D2" *
"single" *							min d(a, b)
"complete"							max d(a, b)		[default]
"average"  (= UPGMA) *
"mcquitty" (= WPGMA)
"median"   (= WPGMC)
"centroid" (= UPGMC)

*/

def hc = r.eval('hc <- hclust(dist(contrib), method="single")')

def exp = r.eval("affectations <- cutree(hc, k="+n_classes+")")

int[] affectations = exp.asIntegers()

//println "affectations: $affectations"
int c = 0
def classes = []
for (int i = 0 ; i < allContribs.length ; i++) {
	if (affectations[i] > c) {
		c++
		classes << contribs[i]
	}
}
println "Classes: $classes"

// nClasses = nClassMax-n_classes+1

// ss = Math.ceil(Math.sqrt(nClasses)) as int

File svgFile = new File("AFCFactorContributionClasses.svg")

def script = """

txm_color_palette_17 <- c(
"#FF0000",
"#0000FF",
"#00CD00",
"#FFD700",
"#FF1493",
"#B22222",
"#000080",
"#008B00",
"#FFB90F",
"#800080",
"#00BFFF",
"#00FF00",
"#FFFF00",
"#FF00FF",
"#00FFFF",
"#ADFF2F",
"#54FF9F"
)

svg("${svgFile.getAbsolutePath()}")

cla <- cutree(hc, k=$n_classes)

barplot(contrib, col=txm_color_palette_17[cla], main=sprintf('%d classes', $n_classes), xlab='Rows', ylab='CTR(row)')

dev.off()
"""

/*
def script2 = """
svg("${svgFile.getAbsolutePath()}", width=10, height=100)
par(mfrow=c($nClasses, 1), pty="s")
for (nc in $n_classes:$nClassMax)
{
  cla <- cutree(hc, k=nc)
  barplot(contrib, col=txm_color_palette_17, main=sprintf('%d classes', nc), xlab='Rows', ylab='CTR(row)')
}
dev.off()
"""
*/

r.eval(script)

// r.plot(svgFile, script)

println "SVG FILE: "+svgFile.getAbsolutePath()

// Affichage du fichier de sortie .svg dans une nouvelle fenêtre de TXM
monitor.syncExec(new Runnable() {
	@Override
	public void run() {	OpenBrowser.openfile(svgFile.getAbsolutePath()) }
})
