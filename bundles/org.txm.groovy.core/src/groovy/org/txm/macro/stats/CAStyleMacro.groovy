// STANDARD DECLARATIONS
// @author mdecorde sjacquot sheiden
package org.txm.macro.stats

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.clientExceptions.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.apache.commons.lang.time.StopWatch
import java.util.Arrays
import org.jfree.chart.renderer.xy.*
import org.jfree.chart.renderer.*
import org.jfree.chart.plot.*
import org.jfree.data.xy.*
import org.jfree.chart.axis.*
import java.awt.*;
import java.awt.geom.*;
import org.jfree.chart.labels.*

import org.txm.ca.core.functions.CA
import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.*
import org.txm.ca.rcp.editors.*
import org.txm.libs.office.ReadODS
import org.txm.ca.core.chartsengine.jfreechart.datasets.*
import org.jfree.chart.renderer.AbstractRenderer
import java.awt.geom.Ellipse2D
import org.jfree.chart.util.ShapeUtils


println "editor: "+editor

if (!(editor instanceof CAEditor)) {
	println "editor is not a CA editor: $editor, Run the macro with F12 when the editor is selected :-)"
	return
}

@Field @Option(name="patternsODSFile", usage="The starting word", widget="FileOpen", required=true, def='patterns.ods')
def patternsODSFile
@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;
if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

if (!patternsODSFile.exists()) {
	println "Pattern file not found: $patternsODSFile"
	return false;
}
if (!patternsODSFile.isFile() || !patternsODSFile.getName().toLowerCase().endsWith(".ods")) {
	println "Wrong pattern file: $patternsODSFile"
	return false;
}

def data = ReadODS.toTable(patternsODSFile, "rows")  // read from the "rows" sheet
def keys = data[0]
if (!keys[0].equals("label-pattern")) {
	println "Error: the first column name must be 'label-pattern'"
	return false
}
def row_styles = [:] // reformat data
	for (int i = 1 ; i < data.size() ; i++) {
		def h = data[i]
		def style = [:] // create style entry
		String s = h[0];
		row_styles[/$s/] = style // with a regexp pattern as key
				
		// fill the style
		for (int j = 1 ; j < h.size() ; j++) {
			style[keys[j]] = h[j]
		}
	}
if (debug > 0) {
	println "ROW STYLES=$row_styles"
}

data = ReadODS.toTable(patternsODSFile, "cols") // read from the "cols" sheet
keys = data[0]
if (!keys[0].equals("label-pattern")) {
	println "Error: the first column name must be 'label-pattern'"
	return false
}
def col_styles = [:] // reformat data

for (int i = 1 ; i < data.size() ; i++) {
	def h = data[i]
	def style = [:] // create style entry
	String s = h[0];
	col_styles[/$s/] = style // with a regexp pattern as key
					
	// fill the style
	for (int j = 1 ; j < h.size() ; j++) {
		style[keys[j]] = h[j]
	}
}
if (debug > 0) {
	println "COL STYLES=$col_styles"
}

ica = editor.getCA()
ca = ica.getCA()

// http://txm.sourceforge.net/javadoc/TXM/TBX/org/txm/stat/engine/r/function/CA.html

// SOME DATA
rows = ica.getRowNames()
rowsinfo = ica.getRowInfos()
rowscos2 = ca.getRowCos2()
cols = ica.getColNames()
colssinfo = ica.getColInfos()
colscos2 = ca.getColCos2()
D1 = ica.getFirstDimension() -1;
D2 = ica.getSecondDimension() -1;

// create some AWT shapes for replacement
shapes = new HashMap<String, Shape>();
// dimensions scaling factor (the same used in default theme for the replaced shapes have same dimension than non-replaced)
float itemShapesScalingFactor = 1.2f
shapes["diamond"] = ShapeUtils.createDiamond((float)(itemShapesScalingFactor * 3.4f));
shapes["square"] = AbstractRenderer.DEFAULT_SHAPE;

float circleSize = 6.4f * itemShapesScalingFactor;
shapes["disk"] = new Ellipse2D.Float((float)(-circleSize / 2), (float)(-circleSize / 2), circleSize, circleSize);

shapes["triangle"] = ShapeUtils.createUpTriangle((float)(itemShapesScalingFactor * 3.2f));

//shapes["star"] = AbstractRenderer.DEFAULT_SHAPE;
//shapes["circle"] = AbstractRenderer.DEFAULT_SHAPE;


println "SHAPES=$shapes"

// styles per col index in dataset
// set col label
colLabelStyle = new HashMap<Integer, String>();
// set col visibility
colHiddenStyle = new HashSet<Integer>(); // true false
// set col font size
colFontSizeStyle = new HashMap<Integer, Float>();
// set col font color
colFontColorStyle = new HashMap<Integer, Color>();
// set col font family
colFontFamilyStyle = new HashMap<Integer, String>();
// set col points size
colPointSizeStyle = new HashMap<Integer, Double>();
// set col points RGBA color
colPointColorStyle = new HashMap<Integer, Color>();
// set col points shape (circle, square, triangle, ... + color ? + size ?) -> expert
colPointShapeStyle = new HashMap<Integer, Shape>(); // circle, square, triangle, etc.
// set col font style (1 = bold, 2 = italic, 3 = bold + italic)
colFontStyleStyle = new HashMap<Integer, Integer>();


// set row label
rowLabelStyle = new HashMap<Integer, String>();
// set row visibility
rowHiddenStyle = new HashSet<Integer>(); // true false
// set row font size
rowFontSizeStyle = new HashMap<Integer, Float>();
// set row font size
rowFontColorStyle = new HashMap<Integer, Color>();
// set row font size
rowFontFamilyStyle = new HashMap<Integer, String>();
// set row points size
rowPointSizeStyle = new HashMap<Integer, Double>();
// set row points RGBA color
rowPointColorStyle = new HashMap<Integer, Color>();
// set row points shape (circle, square, triangle, ... + color ? + size ?) -> expert
rowPointShapeStyle = new HashMap<Integer, Shape>(); // circle, square, triangle, etc.
// set row font style (1 = bold, 2 = italic, 3 = bold + italic)
rowFontStyleStyle = new HashMap<Integer, Integer>();

// prepare col style data for the dataset
for (int i = 0 ; i < cols.length ; i++) {
	for (def p : col_styles.keySet()) {
		if (cols[i] ==~ p) {
			def style = col_styles[p]
			if (style["label-replacement"] != null && style["label-replacement"].length() > 0) {
				colLabelStyle[i] = style["label-replacement"]
			}
			if (style["hidden"] != null && style["hidden"].toUpperCase() == "T") {
				colHiddenStyle << i
			}
			if (style["shape-size"] != null && style["shape-size"].length() > 0) {
				colPointSizeStyle[i] = Double.parseDouble(style["shape-size"])
			}
			if (style["shape-color"] != null && style["shape-color"].length() > 0 ) {
				colPointColorStyle[i] = rgbaStringToColor(style["shape-color"])
			}
			if (style["shape-replacement"] != null && shapes.containsKey(style["shape-replacement"])) {
				colPointShapeStyle[i] = shapes[style["shape-replacement"]]
			}
			if (style["label-size"] != null && style["label-size"].length() > 0) {
				colFontSizeStyle[i] = Float.parseFloat(style["label-size"])
			}
			if (style["label-color"] != null && style["label-color"].length() > 0) {
				colFontColorStyle[i] = rgbaStringToColor(style["label-color"])
			}
			if (style["label-font-family"] != null && style["label-font-family"].length() > 0) {
				colFontFamilyStyle[i] = style["label-font-family"]
			}
			if (style["label-style"] != null && style["label-style"].length() > 0) {
				colFontStyleStyle[i] = Integer.parseInt(style["label-style"])
			}
			
		}
	}
}
if (debug > 0) {
	println "COL COL=$colPointColorStyle"
	println "COL SHP=$colPointShapeStyle"
	println "COL LAB=$colLabelStyle"
	println "COL FONT-SIZ=$colFontSizeStyle"
	println "COL SIZ=$colPointSizeStyle"
	println "COL VIZ=$colHiddenStyle"
	println "COL STYLE=$colFontStyleStyle"
}

// prepare row style data for the dataset
for (int i = 0 ; i < rows.length ; i++) {
	for (def p : row_styles.keySet()) {
		if (rows[i] ==~ p) {
			def style = row_styles[p]
			if (style["hidden"] != null && style["hidden"].toUpperCase() == "T") {
				rowHiddenStyle << i
			}
			if (style["label-replacement"] != null && style["label-replacement"].length() > 0) {
				rowLabelStyle[i] = style["label-replacement"]
			}
			if (style["shape-size"] != null && style["shape-size"].length() > 0) {
				rowPointSizeStyle[i] = Double.parseDouble(style["shape-size"])
			}
			if (style["shape-color"] != null && style["shape-color"].length() > 0 ) {
				rowPointColorStyle[i] = rgbaStringToColor(style["shape-color"])
			}
			if (style["shape-replacement"] != null && shapes.containsKey(style["shape-replacement"])) {
				rowPointShapeStyle[i] = shapes[style["shape-replacement"]]
			}
			if (style["label-size"] != null && style["label-size"].length() > 0) {
				rowFontSizeStyle[i] = Float.parseFloat(style["label-size"])
			}
			if (style["label-color"] != null && style["label-color"].length() > 0) {
				rowFontColorStyle[i] = rgbaStringToColor(style["label-color"])
			}
			if (style["label-font-family"] != null && style["label-font-family"].length() > 0) {
				rowFontFamilyStyle[i] = style["label-font-family"]
			}
			if (style["label-style"] != null && style["label-style"].length() > 0) {
				rowFontStyleStyle[i] = Integer.parseInt(style["label-style"])
			}
		}
	}
}
if (debug > 0) {
	println "ROW COL=$rowPointColorStyle"
	println "ROW SHP=$rowPointShapeStyle"
	println "ROW LAB=$rowLabelStyle"
	println "ROW FONT-SIZ=$rowFontSizeStyle"
	println "ROW SIZ=$rowPointSizeStyle"
	println "ROW VIZ=$rowHiddenStyle"
	println "ROW STYLE=$rowFontStyleStyle"
}



// Redefine the chart renderer and update the chart
def chartEditor = editor.getEditors()[0]
def chartComposite = chartEditor.getComposite()

ddebug = debug
monitor.syncExec( new Runnable() {

	public void run() {
		println chartComposite
		def chart = chartEditor.getChart()
		
		/*
		println "chart: "+chart
		println "Plot: "+chart.getPlot()
		println "Dataset: "+chart.getPlot().getDataset()
		println "Renderer 1: "+chart.getPlot().getRenderer()
		 */
		
		def renderer = new CAItemSelectionRenderer(ica, chart) {
			Area EMPTYAREA = new Area()
					
					@Override
					public void initItemLabelGenerator() {
					XYItemLabelGenerator generator = new XYItemLabelGenerator() {
					
					@Override
					public String generateLabel(XYDataset dataset, int series, int item) {
						if (series == 0 && rowHiddenStyle.contains(item)) {
							if (ddebug > 1) println "HIDDING ROW LABEL '$item' '${rows[item]}'"
							return ""
						} else if (series == 1 && colHiddenStyle.contains(item)) {
							if (ddebug > 1) println "HIDDING COL LABEL '$item' '${cols[item]}'"
							return ""
						} else if (series == 0 && rowLabelStyle.containsKey(item)) {
							if (ddebug > 1) println "RENAME ROW LABEL '$item' '${rows[item]}' -> '${rowLabelStyle[item]}'"
							return rowLabelStyle[item]
						} else if (series == 1 && colLabelStyle.containsKey(item)) {
							if (ddebug > 1) println "RENAME COL LABEL '$item' '${cols[item]}' -> '${colLabelStyle[item]}'"
							return colLabelStyle[item]
						} else {
							CAXYDataset caDataset = (CAXYDataset) dataset;
							return caDataset.getLabel(series, item);
						}
					}
				};
				
				// don't use setBaseItemLabelGenerator BUT setDefaultItemLabelGenerator
				this.setDefaultItemLabelGenerator(generator);
				this.setDefaultItemLabelsVisible(true);
			}
			
			
			
			@Override
			public Font getItemLabelFont(int series, int item) {
				Font d = super.getItemLabelFont(series, item);
				Integer style = d.getStyle();
				Integer size = d.getSize();
				String family = d.getFontName();
				
				// size
				if (series == 0 && rowFontSizeStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX ROW FONT-SIZE $item ${rows[item]} -> ${rowFontSizeStyle[item]}"
					size *= rowFontSizeStyle[item];
				} else if (series == 1 && colFontSizeStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX COL FONT-SIZE $item ${cols[item]} -> ${colFontSizeStyle[item]}"
					size *= colFontSizeStyle[item];
				}
				
				// family
				if (series == 0 && rowFontFamilyStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX ROW FONT-Family $item ${rows[item]} -> ${rowFontFamilyStyle[item]}"
					family = rowFontFamilyStyle[item];
				} else if (series == 1 && colFontFamilyStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX COL FONT-Family $item ${cols[item]} -> ${colFontFamilyStyle[item]}"
					family = colFontFamilyStyle[item];
				}
				
				// style
				if (series == 0 && rowFontStyleStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX ROW FONT-Style $item ${rows[item]} -> ${rowFontStyleStyle[item]}"
					style = rowFontStyleStyle[item];
				} else if (series == 1 && colFontFamilyStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX COL FONT-Style $item ${cols[item]} -> ${colFontStyleStyle[item]}"
					style = colFontStyleStyle[item];
				}
				
				
				return new Font(family, style, size);
			}
			
			
			@Override
			public Shape getItemShape(int series, int item) {
				// Rows (series == 0), Cols (series == 1)
				if (series == 0 && rowHiddenStyle.contains(item)) {
					return EMPTYAREA;
				} else if (series == 1 && colHiddenStyle.contains(item)) {
					return EMPTYAREA;
				} else {
					
					Shape shape =  super.getItemShape(series, item);
					
					// not-visible shapes mode
					if(!((CA) this.multipleItemsSelector.getResult()).isShowPointShapes())        {
						return shape;
					}
					
					// shape replacement
					if (series == 0 && rowPointShapeStyle.containsKey(item)) {
						if (ddebug > 1) println "FIX ROW SHAPE $item ${rows[item]} -> ${rowPointShapeStyle[item]}"
						shape = rowPointShapeStyle.get(item);
					} else if (series == 1 && colPointShapeStyle.containsKey(item)) {
						if (ddebug > 1) println "FIX COL SHAPE $item ${cols[item]} -> ${colPointShapeStyle[item]}"
						shape = colPointShapeStyle.get(item);
					}
					
					
					// shape scaling
					AffineTransform t = new AffineTransform();
					if (series == 0 && rowPointSizeStyle.containsKey(item)) {
						if (ddebug > 1) println "FIX ROW POINT SIZE $item ${rows[item]} -> ${rowPointSizeStyle[item]}"
						t.setToScale(rowPointSizeStyle.get(item), rowPointSizeStyle.get(item));
						shape = t.createTransformedShape(shape);
					} else if (series == 1 && colPointSizeStyle.containsKey(item)) {
						if (ddebug > 1) println "FIX COL POINT SIZE $item ${cols[item]} -> ${colPointSizeStyle[item]}"
						t.setToScale(colPointSizeStyle.get(item), colPointSizeStyle.get(item));
						shape = t.createTransformedShape(shape);
					}
					
					return shape;
				}
			}
			
			
			@Override
			public Paint getItemPaint(int series, int item) {
				
				// visible shapes mode
				if (!((CA) this.multipleItemsSelector.getResult()).isShowPointShapes()) {
					return super.getItemPaint(series, item);
				}
				
				if (series == 0 && rowPointColorStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX ROW POINT COLOR $item ${rows[item]} -> ${rowPointColorStyle[item]}"
					return rowPointColorStyle.get(item);
				} else if (series == 1 && colPointColorStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX COL POINT COLOR $item ${cols[item]} -> ${colPointColorStyle[item]}"
					return colPointColorStyle.get(item);
				}
				else {
					return super.getItemPaint(series, item);
				}
			}
			
			
			@Override
			public Paint getItemLabelPaint(int series, int item) {
				if (series == 0 && rowFontColorStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX ROW LABEL COLOR $item ${rows[item]} -> ${rowFontColorStyle[item]}"
					return rowFontColorStyle.get(item);
				} else if (series == 1 && colFontColorStyle.containsKey(item)) {
					if (ddebug > 1) println "FIX COL LABEL COLOR $item ${cols[item]} -> ${colFontColorStyle[item]}"
					return colFontColorStyle.get(item);
				} else {
					return super.getItemLabelPaint(series, item);
				}
			}
			
		};
		
		
		def cp = editor.editors[0].getChart();
		renderer.chart = cp // SJ: seems useless
		renderer.setSeriesVisible(0, ica.isShowVariables()); // Rows  // SJ: seems useless
		renderer.setSeriesVisible(1, ica.isShowIndividuals()); // Columns  // SJ: seems useless
		chart.getXYPlot().setRenderer(renderer)
		
		ica.getChartCreator().updateChart(chartEditor.getResult())
		
	}
});




// creates a Color object from the specified RGB or RGBA string representation separated by spaces ("R G B" or "R G B A") from 0 to 255 for each channel
def rgbaStringToColor(String color) {
	String[] rgbColor = color.split(" ");
	String alpha = "255";
	if(rgbColor.length > 2) {
		if(rgbColor.length > 3) {
			alpha = rgbColor[3];
		}
		return new Color(Integer.parseInt(rgbColor[0]), Integer.parseInt(rgbColor[1]), Integer.parseInt(rgbColor[2]), Integer.parseInt(alpha))
	}
	else {
		println "Error in color format for RGB or RGBA string: $color"
	}
}



