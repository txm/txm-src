package org.txm.macro.stats
// Copyright © 2010-2016 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// @author mdecorde
// @author sheiden
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

// This macro computes the basic vocabulary of a partition
// given a specificities table. The result is saved in a
// TSV file.

import org.txm.Toolbox
import org.txm.searchengine.cqp.clientExceptions.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.statsengine.r.core.RWorkspace
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.specificities.core.functions.Specificities

if (!(corpusViewSelection instanceof Specificities)) {
	println "** You need to select a Specificities result icon in the Corpus view before calling this macro. Aborting."
	return
}

// PARAMETERS

@Field @Option(name="outputDirectory", usage="output directory", widget="Folder", required=true, def=".")
def outputDirectory

@Field @Option(name="outputFile", usage="TSV result file", widget="String", required=true, def="banal.tsv")
def outputFile

@Field @Option(name="scoreMax", usage="score maximum threshold", widget="Float", required=false, def="1.5")
def scoreMax

//@Field @Option(name="suseSum", usage="Use the row sums : y/n", widget="String", required=false, def="n")
def suseSum // not used

// END OF PARAMETERS
if (!ParametersDialog.open(this)) return

if (suseSum == null || suseSum.length() == 0) suseSum = "n"
boolean useSum = suseSum.toLowerCase().charAt(0) == "y"

if (useSum) {
	println "Using line sums..."
}

def output = new File(outputDirectory.toString()+"/"+outputFile)
def writer = output.newWriter("UTF-8")
Specificities specif = corpusViewSelection
def indices = specif.getSpecificitesIndices()
def freqs = specif.getFrequencies()
def rownames = specif.getRowNames()
def colnames = specif.getColumnsNames()

def selected = []

writer.print "unit"
writer.print "\tF"
writer.print "\tscore_max"

for (int j = 0; j < colnames.size() ; j++) {
	writer.print "\t"+colnames[j]
	writer.print "\tscore"
}

writer.println ""

for (int i = 0; i < rownames.length ; i++) {
	boolean add = true
	def totF = 0
	def totscore = 0.0
	def maxS = 0

	for (int j = 0; j < colnames.size() ; j++) {

		def sp = Math.abs(indices[i][j])
		if (sp >= scoreMax) {
			add = false
		} else {
			if (sp >= maxS) maxS = sp
		}
		totF += freqs[i][j]
		totscore += sp
	}
	
	if (useSum) {
		if (totscore >= scoreMax) add = false
		else add = true
	}
	
	if (add) {
		selected << rownames[i]
		writer.print rownames[i]
		writer.print "\t$totF"
		writer.print "\t$maxS"
		for (int j = 0; j < colnames.size() ; j++) {
			writer.print "\t"+freqs[i][j]
			writer.print "\t"+indices[i][j]
		}
		writer.println ""
	}
}

println "Selected: "+selected
println "Saved in $output."

writer.close()

