// STANDARD DECLARATIONS
// @author mdecorde sjacquot sheiden
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.libs.office.ReadODS
import org.txm.libs.office.WriteODS

// ############################################


c = { name ->
	if (!style.containsKey(name)) {
		println "No $name column..."
		return false
	}
	return style[name]
}

def applyRules(def styles, def rules) {
	for (def rule : rules) {
		applyRule(styles, rule)
	}
}

def applyRule(def styles, def rule) {
	String pseudoCode = rule[1]
//	if (pseudoCode.matches("[^(]+\\((.+)\\)")
//	String method = 
	//println "styles="+styles+" rule="+rule+ " "+styles[rule[0]]+" <- "+groovy.evaluate(rule[1])
	styles[rule[0]] = groovy.evaluate(rule[1])	
}

// ############################################

@Field @Option(name="rules_script", usage=".groovy file", widget="FileOpen", required=true, def='')
def rules_script

@Field @Option(name="functions_script", usage="functions used in rules, .groovy file", widget="FileOpen", required=true, def='')
def functions_script

@Field @Option(name="input_style_table", usage="must include 2 sheets named 'rows' and 'cols'", widget="FileOpen", required=true, def='')
def input_style_table

@Field @Option(name="output_style_table", usage="writes the 2 sheets rows and cols", widget="FileSave", required=true, def='')
def output_style_table

@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return
if (debug == "OFF") debug = 0 else if (debug == "ON") debug = 1 else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3

if (!rules_script.exists()) {
	println "Rules file not found: $rules_script. Aborting..."
	return false
}

if (!functions_script.exists()) {
	println "Functions file not found: $functions_script. Aborting..."
	return false
}

groovy = new GroovyShell(binding)
groovy.evaluate(functions_script.getText("UTF-8"))


if (!input_style_table.exists()) {
	println "Pattern file not found: $input_style_table"
	return false
}
if (!input_style_table.isFile() || !input_style_table.getName().toLowerCase().endsWith(".ods")) {
	println "Wrong pattern file: $input_style_table"
	return false
}

cols_rules = null
rows_rules = null

groovy.evaluate(rules_script.getText("UTF-8"))
//def rows_rules = []
//rules_script.getLines("UTF-8") { line ->
//	line = lilne.split("\t", 3)
//	if ("row".equals(line[0])) {
//		rows_rules << [line[1], line[2]]
//	}
//}
//
//def cols_rules = []
//rules_script.getLines("UTF-8") { line ->
//	line = lilne.split("\t", 3)
//	if ("col".equals(line[0])) {
//		cols_rules << [line[1], line[2]]
//	}
//}

if (cols_rules == null) {
	println "Error: no 'cols_rules' defined. Aborting..."
	return false
}

if (rows_rules == null) {
	println "Error: no 'rows_rules' defined. Aborting..."
	return false
}

print "Reading input_style_table rows..."
def data = ReadODS.toTable(input_style_table, "rows")  // read from the "rows" sheet
println "Done."

print "Applying rows styles..."

def keys = data[0]
def row_styles = [] // reformat data
	for (int i = 1 ; i < data.size() ; i++) {
		def h = data[i]
		style = [:] // create style entry
		
		// fill the style
		for (int j = 0 ; j < h.size() ; j++) {
			style[keys[j]] = h[j]
		}
		
		applyRules(style, rows_rules)
		row_styles << style
	}
if (debug > 0) {
	println "ROW STYLES="+row_styles.join("\n")
}
println "Done."

print "Reading input_style_table cols..."
data = ReadODS.toTable(input_style_table, "cols") // read from the "cols" sheet
println "Done."

print "Applying cols styles..."

keys = data[0]
def col_styles = [] // reformat data

for (int i = 1 ; i < data.size() ; i++) {
	def h = data[i]
	style = [:] // create style entry
	
	// fill the style
	for (int j = 0 ; j < h.size() ; j++) {
		style[keys[j]] = h[j]
	}
	
	applyRules(style, cols_rules)
	col_styles << style
}
if (debug > 0) {
	println "COL STYLES="+col_styles.join("\n")
}
println "Done."

print "Writing output_style_table..."
WriteODS writer = new WriteODS(output_style_table)
writer.table.setTableName("rows")
if (row_styles.size() > 0) {

	print "Writing rows styles..."
	
	writer.declareRowsAndColumns(row_styles.size(), row_styles[0].size())
	keys = row_styles[0].keySet()
	try {
		writer.writeLine(keys as ArrayList, 0)
	} catch(e) {
		println "** Error in writer.writeLine(keys as ArrayList, 0)"
		e.printStackTrace()
	}
	int iRow = 1
	for (def style : row_styles) {
		def line = []
		for (def k : keys) line << style[k]
			try {
				writer.writeLine(line, iRow++)
			} catch(e) {
				println "** Error in writer.writeLine('$line', '${iRow+1}')"
				e.printStackTrace()
			}
	}
}

table = writer.newTable("cols")
if (col_styles.size() > 0) {

	print "Writing cols styles..."
	
	writer.declareRowsAndColumns(col_styles.size(), col_styles[0].size())
	
	keys = col_styles[0].keySet()
	try {
		writer.writeLine(keys as ArrayList, 0)
	} catch(e) {
		println "** Error in writer.writeLine(keys as ArrayList, 0)"
		e.printStackTrace()
	}
	int iRow = 1
	for (def style : col_styles) {
		def line = []
		for (def k : keys) line << style[k]
			try {
				writer.writeLine(line, iRow++)
			} catch(e) {
				println "** Error in writer.writeLine('$line', '${iRow+1}')"
				e.printStackTrace()
			}
	}
}
println "Done."

writer.save()
