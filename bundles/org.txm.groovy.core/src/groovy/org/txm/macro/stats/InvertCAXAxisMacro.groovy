// @author Sebastien Jacquot
// STANDARD DECLARATIONS
package org.txm.macro.stats


import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.clientExceptions.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.apache.commons.lang.time.StopWatch
import java.util.Arrays
import org.jfree.chart.renderer.xy.*
import org.jfree.chart.renderer.*
import org.jfree.chart.plot.*
import org.jfree.data.xy.*
import org.jfree.chart.axis.*
import java.awt.*;
import java.awt.geom.*;
import org.jfree.chart.labels.*

import org.txm.ca.core.chartsengine.jfreechart.themes.highcharts.renderers.*
import org.txm.ca.rcp.editors.*
import org.txm.libs.office.ReadODS
import org.txm.ca.core.chartsengine.jfreechart.datasets.*
import org.jfree.chart.renderer.AbstractRenderer

println "editor: "+editor

if (!(editor instanceof CAEditor)) {
	println "editor is not a CA editor: $editor, Run the macro with F12 when the editor is selected :-)"
	return
}

ica = editor.getCA();
chart = ica.getChart();
plot = chart.getXYPlot();
dataset = plot.getDataset(); 

// overrides some dataset methods to return inverted X coordinates for columns and rows
plot.setDataset(new CAXYDataset(ica) {

        public Number getX(int series, int item) {
                if(item == -1)        {
                        System.out.println("CAXYDataset.getX()");
                }
                // Rows
                if(series == 0) {
                        return -this.rowCoordinates[item][this.axis1];
                }
                // Cols
                else {
                        return -this.columnCoordinates[item][this.axis1];
                }
        }
        
        
         
        /**
         * Gets the minimum value in the specified series according to the specified axis.
         * @param series
         * @param axis
         * @return
         */
        public double getMinValue(int series, int axis)        {
                double minValue = 0;
                double tmpMinValue;
                double[][] coordinates = this.rowCoordinates;
                if(series != 0)        {
                        coordinates = this.columnCoordinates;
                }
                
                for(int i = 0; i < coordinates.length; i++) {
                        tmpMinValue = coordinates[i][axis];
                        
						// invert X coordinate
                		if(axis == 0)        {
                			tmpMinValue = -tmpMinValue;
                		}
                        
                        if(tmpMinValue < minValue)        {
                                minValue = tmpMinValue;
                        }
                }
                
                return minValue;
        }
        
        /**
         * Gets the maximum value in the specified series according to the specified axis.
         * @param series
         * @param axis
         * @return
         */
        public double getMaxValue(int series, int axis)        {
                double maxValue = 0;
                double tmpMaxValue;
                double[][] coordinates = this.rowCoordinates;
                if(series != 0)        {
                        coordinates = this.columnCoordinates;
                }
                
                for(int i = 0; i < coordinates.length; i++) {
                        tmpMaxValue = coordinates[i][axis];
                        
						// invert X coordinate
                		if(axis == 0)        {
                			tmpMaxValue = -tmpMaxValue;
                		}
                        
                        if(tmpMaxValue > maxValue)        {
                                maxValue = tmpMaxValue;
                        }
                }
                
                return maxValue;
        }
}
);

// update the limits dotted borders
ica.getChartCreator().createCAFactorialMapChartLimitsBorder(chart);


