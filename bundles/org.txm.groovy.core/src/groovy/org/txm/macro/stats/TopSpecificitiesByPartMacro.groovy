package org.txm.macro.stats
// Copyright © 2010-2016 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// @author mdecorde
// @author sheiden
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

// This macro computes the basic vocabulary of a partition
// given a specificities table. The result is saved in a
// TSV file.

import org.txm.Toolbox
import org.txm.searchengine.cqp.clientExceptions.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.statsengine.r.core.RWorkspace
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.specificities.core.functions.Specificities
import org.txm.specificities.core.preferences.SpecificitiesPreferences
import org.txm.libs.office.WriteODS

if (!(corpusViewSelection instanceof Specificities)) {
	println "** You need to select a Specificities result icon in the Corpus view before calling this macro. Aborting."
	return
}

// PARAMETERS

@Field @Option(name="outputFile", usage="TSV result file", widget="String", required=true, def="top.tsv")
		def outputFile

@Field @Option(name="nTop", usage="Number of words per column", widget="Integer", required=false, def="10")
		def nTop

@Field @Option(name="showPositiveIndices", usage="Show positive indices", widget="Boolean", required=false, def="true")
		def showPositiveIndices

@Field @Option(name="showNegativeIndices", usage="Show negative indices", widget="Boolean", required=false, def="false")
		def showNegativeIndices
@Field @Option(name="printWordsAsList", usage="Print the specifics words as a list in the console", widget="Boolean", required=false, def="true")
		def printWordsAsList

// END OF PARAMETERS
if (!ParametersDialog.open(this)) { return }

if (!showPositiveIndices && !showNegativeIndices) {
	println "Select positive or/and negative indices. Aborting."
	return;
}

Specificities specif = corpusViewSelection
def indices = specif.getSpecificitesIndices()
def freqs = specif.getFrequencies()
def rownames = specif.getRowNames()
def colnames = specif.getColumnsNames()
def banality = SpecificitiesPreferences.getInstance().getDouble(SpecificitiesPreferences.CHART_BANALITY)
println "BANALITY=$banality"
def allTopKeys = new LinkedHashSet()

def output = new File(outputFile)
def writer = output.newWriter("UTF-8")
def allResults = new LinkedHashMap()
for (int j = 0; j < colnames.size() ; j++) {
	
	writer.println "## "+colnames[j]
	writer.println "unit\tF\tindice"
	
	def sorted = []
	def tops = []
	for (int i = 0; i < rownames.length ; i++) {
		sorted << [rownames[i], freqs[i][j], indices[i][j]]
	}
	
	sorted.sort() { -it[2]}
	
	if (showPositiveIndices) {
		for (int i = 0 ; i < nTop ; i++) {
			if (sorted.size() <= i) continue;
			if (sorted[i][2] < banality) continue; // dont keep negative scores
			writer.println sorted[i].join("\t")
			allTopKeys << rownames[i]
			tops << sorted[i]
		}
	}
	
	if (showNegativeIndices) {
		if (showPositiveIndices) {
			writer.println "\n"
		}
		
		for (int i = nTop ; i >= 1 ; i--) {
			if (sorted[sorted.size() - i][2] > -banality) continue; // dont keep positive scores adn scores higher than banlity
			if (sorted.size() <= i) continue;
			writer.println sorted[sorted.size() - i].join("\t")
			allTopKeys << rownames[i]
			tops << sorted[sorted.size() - i]
		}
		writer.println "\n"
	}
	allResults[colnames[j]] = tops
}

writer.close()

println "Result: "+output.getAbsolutePath()

if (printWordsAsList) {
	def unifiedList = new HashSet()
	
	for (def s : allResults.keySet()) {
		for (def w : allResults[s]) {
			unifiedList << w[0]
		}
	}
	
	unifiedList = unifiedList.sort()
	println unifiedList.join(", ")
	println "["+specif.getUnitProperty().getName()+"=\"" + unifiedList.collect(){it.replace("|", "\\|")}.join("|") + "\"]"
}

