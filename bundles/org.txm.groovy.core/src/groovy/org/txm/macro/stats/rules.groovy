// Functions

def RedScale(n, min, max) {

/* target for 4:
40 40 255
80 80 255
120 120 255
160 160 255

RedScale(4, 40, 160)

*/

	delta = ((max-min)/(n-1)).round()
		
	colors = []

	n.times {
		v = min+delta*it
		colors << "255 "+v+" "+v
	}
	
	return colors
}

def GreenScale(n, min, max) {

	delta = ((max-min)/(n-1)).round()
		
	colors = []

	n.times {
		v = min+delta*it
		colors << v+" 255 "+v
	}
	
	return colors
}

def BlueScale(n, min, max) {

	delta = ((max-min)/(n-1)).round()
		
	colors = []

	n.times {
		v = min+delta*it
		colors << v+" "+v+" 255"
	}
	
	return colors
}

int_linear_min = { x, max_input, min_output, max_output ->

	def y = x*(max_output-min_output)/max_input+min_output
	
	if (y < 1) {
		return (int) min_output
	} else {
		return (int) y
	}
}

def sex2Color(sexe) {
	
	switch(sexe) {
		
		case "F":
			return "255 0 255"
		case "M":
			return "0 0 255"
		default:
			println "$sexe is not a sex value..."
			return "0 0 0"
	}
}

sex2Symbol = { sexe ->
	
	switch(sexe) {
		
		case "F":
			return "⬤"
		case "M":
			return "✚"
		default:
			println "$sexe is not a sex value..."
			return "?"
	}
}

sex2Shape = { sexe ->
	
	switch(sexe) {
		
		case "F":
			return "disk"
		case "M":
			return "triangle"
		default:
			println "$sexe is not a sex value..."
			return "?"
	}
}

sex2ColorScale = { sexe, scale ->
	
	switch(sexe) {
		
		case "F":
			return blue_scale[scale]
		case "M":
			return red_scale[scale]
		default:
			println "$sexe is not a sex value..."
			return "0 0 0"
	}
}

colorScale = { color_scale, scale ->

	try {
    		value = color_scale[scale]
    		if (value == null) {
    			println "no scale $scale in $color_scale color_scale..."
			return ""
    		}
	} catch ( IndexOutOfBoundsException e ) {
    		println "no scale $scale in $color_scale color_scale..."
		return ""
	}
	return value
}

parseInt = { str ->
	if (str.isInteger()) {
		return str.toInteger()
	} else {
		println "$str is not an Integer..."
		return 0
	}
}

parseFloat = { str ->

	str = str.replaceAll(",", ".")
	if (str.isFloat()) {
		return str.toFloat()
	} else {
		println "$str is not a float number..."
		return 0.0
	}
}

// globals

red_scale = RedScale(4, 40, 160)
blue_scale = BlueScale(4, 40, 160)

// https://www.schemecolor.com/solar-heat-color-scheme.php

solarScale4 = [
	"199 5 9",
	"255 52 45",
	"255 237 68",
	"249 169 0"
]

mixedTxmPalette4 = [
	"255 0 0",
	"255 215 0",
	"0 139 0",
	"0 0 255"
]

rows_rules = // ajouter des [target, source]
		[
			// ["color",	"""c("autre")"""]
			
			// ...
		]

cols_rules = [

/*

Pattern :

	[<column-name>, <groovy-code>]
	
	e.g. ["color", "1"]
	
	use " " for one space string

- LABEL : cercle
- TAILLE du label : fixe
- COULEUR : fonction du sexe (couleur de base) et du quartile d'âge (dégradé)
- POINT : pas de point

LABEL : numéro du cercle
TAILLE : fixe
COULEUR : fonction du cercle. Prendre 4 couleurs qui suivent un ordre “naturel”, ex. Arc-en-ciel, ou chaud (cercle1) – froid (cercle 4), ex Rouge / Orange / Bleu / Violet. Ou bien un dégradé où l’on arrive bien à distinguer 4 valeurs (pas sûr que ce soit possible ?)
TRANSPARENCE : filtrage éventuel sur cos2 

c("cercle")
c("age-quartile")
sex2ColorScale(c("sexe"), c("age-quartile") as Integer)+" "+c(transparency)
int_linear_min(c("q12") as Integer, 0.48, 80, 255)

Examples:

	["label-size", """1"""],
	["label-replacement", """c("cercle")"""],
	["transparency", """int_linear_min(parseFloat(c("cos2-1")), 0.43, 80, 255)"""],
	["label-color", """sex2ColorScale(c("sexe"), parseInt(c("age-quartile")))+" "+c("transparency")"""],


*/
	["label-size", """1"""],
	["label-replacement", """return(".")"""],
	["shape-replacement", """sex2Shape(c("sexe"))"""],
	["transparency", """int_linear_min(parseFloat(c("cos2-1")), 0.43, 80, 255)"""],
	["shape-color", """colorScale(mixedTxmPalette4, parseInt(c("cercle"))-1)+" "+c("transparency")"""],
	
	// ajouter des [target, source] ici ...
]
