// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

/* --------------   Specif2Throw    --------------

FR:

Macro affichant la probabilité a priori (avant de faire les lancés ou les tirages) d'obtenir N succès consécutifs
dans des jeux de lancés ou de tirages simples en regard avec des valeurs de spécificités.
Il s'agit de jeux de lancés ou tirages connus de tous : le lancé de pièce pour obtenir pile ou face, le lancé de dé
à 6 faces, tirer une carte dans un jeu de 32 ou 52 cartes, etc.
On considère qu'une pièce a 50% de chances (1 chance sur 2) de tomber sur la face 'pile' à chaque lancé.
On considère qu'un dé a 16,6% de chances (1 chance sur 6) de tomber sur '6' à chaque lancé.
On considère qu'on a 3,1% de chances (1 chance sur 32) de tirer un as de trèfle à chaque tirage dans un jeu de 32 cartes.
On considère qu'on a 1,9% de chances (1 chance sur 52) de tirer un as de trèfle à chaque tirage dans un jeu de 52 cartes.
etc.
On considère que les lancés ou les tirages sont indépendants et que les objets ne sont pas biaisés (eg le dé n'est pas pipé).

La probabilité est mise en regard avec la spécificité équivalente pour mettre en lien
des valeurs de spécificités calculées par TXM et l'intuition que l'on a de chances de succès
dans un jeu de lancé ou de tirage.
Il s'agit bien sûr uniquement d'illustrer l'ordre de grandeur de la surprise à avoir en
interprétant directement le modèle des spécificités. Il ne s'agit en aucun cas d'illustrer
une quelconque assimilation de l'apparition de mots dans des textes à un jeu de hasard.

La macro prend deux paramètres :

* cardOmega : le nombre d'issues possibles pour le jeu de lancé ou de tirage considéré
              (Omega est l'univers de l'expérience aléatoire, cardOmega son cardinal)
 - pour un lancé de pièce : choisir 2 pour cardOmega
   (Omega = {pile, face})
 - pour un lancé de dé : choisir 6 pour cardOmega
   (Omega = {1, 2, 3, 4, 5, 6})
 - pour un tirage de carte dans un jeu de 32 cartes : choisir 32 pour cardOmega
   (Omega = {1 de trèfle, 2 de trèfle, 3 de trèfle, 4 de trèfle, 5 de trèfle, 6 de trèfle, 7 de trèfle,
   8 de trèfle, 9 de trèfle, 10 de trèfle, valet de trèfle, dame de trèfle, roi de trèfle, 1 de carreau,
   2 de carreau, etc.})
 - pour un tirage de carte dans un jeu de 52 cartes : choisir 52 pour cardOmega
 - etc.
* nthrows : le nombre maximum de lancés ou de tirages avec succès consécutifs

La macro affiche - selon les options :

* displayProba : toutes les probabilités de 1 à nthrows lancés ou tirages en détaillant :
-  n : le nombre de succès consécutifs
-  p : la probabilité correspondante
-  % : le pourcentage correspondant
- pe : la probabilité exprimée en notation avec exposant en base 10
-  s : la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)

* onlyMainRanks : s'utilise avec l'option displayProba pour n'afficher que les probabilités des rangs décimaux principaux (1, 2..., 10, 20..., 100, 200...)

* displayThrows : plutôt que le détail de la probabilité, liste pour une spécificité S- donnée, le nombre équivalent de lancés ou de tirages avec succès consécutifs correspondants exprimé sous la forme d'un intervalle : nombre de lancés ou tirages minimum - nombre de lancés ou tirages maximum
Par exemple '-10 = 30-33' signifie : une spécificité S- de '-10' équivaut à entre 30 et 33 lancés ou tirages avec succès consécutifs (cas du lancé de pièce, cardOmega = 2)

Les limites du calcul sont liées au modèle de la mémoire de la machine. Une machine 64-bit peut typiquement calculer la probabilité de 1023 lancés de pièce avec succès consécutifs.

*/

// STANDARD DECLARATIONS
package org.txm.macro.stats

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.eclipse.ui.console.*

// BEGINNING OF PARAMETERS

@Field @Option(name="cardOmega", usage="card(Ω): 2=coin, 6=dice, 32=32-card deck, 52=52-card deck, etc.", widget="Integer", required=true, def="2")
def cardOmega

@Field @Option(name="nthrows", usage="maximum number of successful throws or draws", widget="Integer", required=true, def="99")
def nthrows

@Field @Option(name="displayProba", usage="display probabilities", widget="Boolean", required=false, def="true")
def displayProba

@Field @Option(name="onlyMainRanks", usage="only display main rank lines", widget="Boolean", required=false, def="false")
def onlyMainRanks

@Field @Option(name="displayThrows", usage="display equivalent specificity value for each number or interval number of throws or draws", widget="Boolean", required=false, def="true")
def displayThrows

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def clearConsole = { ->
	// clear the console
	(ConsolePlugin.getDefault().getConsoleManager().getConsoles())[0].clearConsole()
}

clearConsole()

min = [:]
max = [:]

pow = cardOmega**(nthrows)
powd = pow.doubleValue()

if (powd == Double.POSITIVE_INFINITY || powd == Double.NEGATIVE_INFINITY) {
	println sprintf("** Impossible to calculate probabilities for this value, try a lower value (impossible to represent cardOmega to the power of %d with this machine). Aborting.", nthrows)
	return
}

d = 1/powd

significandSize = ((-java.lang.Math.log10(d) as double)-1 as int)+4

format = sprintf("%%4d = %%%d.%df = %%0%d.%df%%%% = %%9.2e = %%4d", significandSize, significandSize-2, significandSize-1, significandSize-4)

unit = 1
dec = 1

println "cardOmega = "+cardOmega

if (displayProba) {
	println """Légende :
-  n : le nombre de lancés ou tirages avec succès consécutifs
-  p : la probabilité correspondante
-  % : le pourcentage correspondant
- pe : la probabilité exprimée en notation avec exposant en base 10
-  s : la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)
"""
	spc = ' ' * (significandSize+2)
	println "   n    p"+spc+" %"+spc+"  pe         s"
}

nthrows.times { it -> 
    n = it + 1;
	p = ((1/cardOmega)**(n)) as double;
	s = java.lang.Math.log10(p) - 1 as int
	if (min[s]) { if (min[s] > n) min[s] = n } else min[s] = n
	if (max[s]) { if (max[s] < n) max[s] = n } else max[s] = n
	if (displayProba) {
		s = sprintf(format, n, p, p*100, p, s).replaceAll(/ = 0([^,])/, ' =  $1')
		m0 = s =~ /(....)0+ = ([^-])/
		if (m0.count > 0) {
			spc0 = m0[0][1] + (' ' * (m0[0][0].size()-8)) + ' = ' + m0[0][2]
			s = m0.replaceFirst(spc0)
		}
		m0p = s =~ /(....)0+% = /
		if (m0p.count > 0) {
			spc0p = m0p[0][1] + '%' + (' ' * (m0p[0][0].size()-8)) + ' = '
			s = m0p.replaceFirst(spc0p)
		}

		if (!onlyMainRanks || ((n % dec) == 0) || (n == nthrows)) {
			println s
		}
		
		unit = unit+1
		if (unit >= dec * 10) {
			dec = dec * 10
			unit = 1
		}
	}
}

if (displayThrows) {

	if (displayProba) {
		println ""
	}

	def x = []
	def y = []
	
	println """Légende :
- -S      : spécificité équivalente
-  lancés : intervalle des nombres de lancés ou tirages avec succès consécutifs correspondant
"""
	println " -S   lancés"
	min.each { key, value ->
	 if (min[key] != max[key]) println sprintf("%3d = %d-%d", key, min[key], max[key])
	 else println sprintf("%3d = %d", key, min[key])
	 x.push(key)
	 y.push(min[key])
	}
	
//	println "x = "+x
//	println "y = "+y

}