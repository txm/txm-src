// STANDARD DECLARATIONS
package org.txm.macro.office

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.libs.jodconverter.ConvertDocument

// BEGINNING OF PARAMETERS

@Field @Option(name="office_path", usage="Path to LibreOffice or OpenOffice installation directory", widget="Folder", required=false, def="libreoffice or openoffice install directory")
def office_path

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

if (office_path == null) {
	println "No path to office directory given."
	return
}

if (!office_path.exists()) {
	println "'$office_path' directory not found."
	return
}

if (!office_path.isDirectory()) {
	println "'$office_path' exists but is not a directory."
	return
}

if (!office_path.canExecute()) {
	println "'$office_path' exists but has not sufficent rights to be used."
	return
}

def old = System.getProperty("office.home")
ConvertDocument.setOfficeHome(office_path.getAbsolutePath())
println "Office path set to '"+System.getProperty("office.home")+"'."
if (old != null) {
	println "	Previous path was '${old}'."
}