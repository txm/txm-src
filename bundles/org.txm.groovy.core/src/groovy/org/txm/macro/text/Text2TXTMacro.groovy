package org.txm.macro.text;
// STANDARD DECLARATIONS

import groovy.transform.Field
import org.txm.libs.jodconverter.ConvertDocument

// BEGINNING OF PARAMETERS
@Field @Option(name="inputDirectory", usage="the directory containing the DOC/ODT/RTF files to convert", widget="Folder", required=true, def="")
File inputDirectory

@Field @Option(name="extension", usage="an example file", widget="String", required=true, def="(odt|doc|rtf|html)")
String extension = "doc"

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

boolean debug = false;

def files = []
try {
	inputDirectory.eachFileMatch(~/.+\.$extension/) { docFile ->
		String name = docFile.getName()
		name = name.substring(0, name.lastIndexOf("."))
		def txtFile = new File(docFile.getParentFile(), name+".txt")
		ConvertDocument.convert(docFile, txtFile)
		files << docFile
	}
} catch(Exception e) {
	println "Error while processing directory: "+e;
	if (debug) e.printStackTrace();
}

println "Processed directory: $inputDirectory"
println "files: "+files