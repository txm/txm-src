// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

import ims.tiger.importfilter.*
import ims.tiger.importfilter.bracketing.*
import org.apache.log4j.*

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="SourceFile", usage="SourceFilename", widget="File", required=false, def="input.mrg")
def SourceFile

@Field @Option(name="TargetFile", usage="TargetFilename", widget="File", required=false, def="output.xml")
def TargetFile

@Field @Option(name="XMLTargetID", usage="XMLTargetID", widget="String", required=false, def="XMLTargetID")
def XMLTargetID

@Field @Option(name="SourceFolder", usage="SourceFolder (leave empty if SourceFile set)", widget="Folder", required=false, def="input <remove if SourceFile set>")
def SourceFolder

@Field @Option(name="TargetFolder", usage="TargetFolder (leave empty if TargetFile set)", widget="Folder", required=false, def="output")
def TargetFolder

@Field @Option(name="MaximumNumberOfSentences", usage="MaximumNumberOfSentences", widget="Integer", required=true, def="0")
def MaximumNumberOfSentences

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

BasicConfigurator.configure()

ImportFilter filter = new MyUPennFilter()
ImportFilterHandler handler = new SilentImportFilterHandler()
filter.setImportFilterHandler(handler)

filter.setCompression(false)
filter.setSchemaFilename(System.getProperty("user.home")+"/TXM/xml/xsd/tiger/TigerXML.xsd")
filter.setMaximumNumberOfSentences(MaximumNumberOfSentences-1)

if (SourceFolder == null) {

	def SourceFilename = SourceFile.getAbsolutePath()
	def TargetFilename = TargetFile.getAbsolutePath()

	filter.setSourceFilename(SourceFilename)
	filter.setXMLTargetFilename(TargetFilename)
	filter.setXMLTargetID(XMLTargetID)
	println "Converting '$SourceFilename' to '$TargetFilename'"

	try {
  			filter.startConversion()
		} catch (Exception e)
		{ e.printStackTrace()
	}
} else {

	for (File inputfile : SourceFolder.listFiles()) {
		if (inputfile.isDirectory()) continue // ignore
	
		fileNameE = inputfile.getName()
		i = fileNameE.lastIndexOf('.')
		if (i > 0) extension = fileNameE.substring(i+1) else extension = ""
		fileName = fileNameE.substring(0, i)
		fileId = fileName
		input = inputfile.getAbsolutePath()
		output = TargetFolder.getAbsolutePath()+"/"+fileName+".xml"
		println "Converting '"+input+"' to '"+output+"'"
		filter.setSourceFilename(input)
		filter.setXMLTargetFilename(output)
		filter.setXMLTargetID(fileId)

		try {
  				filter.startConversion()
			} catch (Exception e)
			{ e.printStackTrace()
    	}
	}
}