package org.txm.macro.projects.antract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.UsernamePasswordDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.io.IOUtils;
import java.io.DataOutputStream;
import sun.net.www.http.*;

public class OkapiSaphirAPI {
	
	public static final String baseUrl = "https://okapi.ina.fr/antract/api/saphir/";
	public static final String okapiUrl = "https://okapi.ina.fr/antract/html/resource_587884135/resource_996999332.html";

	public static boolean debug = false;
	
	public static String getDirectAccessURL(String id) {
		return OkapiSaphirAPI.okapiUrl+"?linkedObjectUri="+URLEncoder.encode(id, "UTF-8")
	}
	
	public static String login(String user, String password) {
		URL url1;
		try {
			url1 = new URL(baseUrl+"user/login?user="+URLEncoder.encode(user, "UTF-8")+"&password="+URLEncoder.encode(password, "UTF-8"));
			
			HttpURLConnection conn = (HttpURLConnection)url1.openConnection();
			
			if (debug) System.out.println("conn1: "+conn);
			
			conn.setRequestMethod("GET");
			conn.connect();
			
			int code = conn.getResponseCode();
			if (code != HttpURLConnection.HTTP_OK) {
				System.out.println("Error: during login: "+code);
				return "";
			}

			String sessionID = getOkapiSessionID(conn)
			String content1 = getContent(conn);
			if (debug) {
				println "content="+content1
				println "sessionID="+sessionID
			}
			
			if (sessionID.length() == 0) {
				if (debug) System.out.println("Error: not connected no session ID found");
				return "";
			}
			
			if (!content1.contains("Welcome "+user)) {
				if (debug) System.out.println("Error: not connected, welcome message not found: "+content1);
				return "";
			}
			
			conn.disconnect();
			
			return sessionID;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	public static String update_corpus(String sessionID, String corpusID, String ids) {
		return create_or_update_corpus(sessionID, corpusID, null, ids);
	}
	
	public static String create_corpus(String sessionID, String title, String ids) {
		return create_or_update_corpus(sessionID, null, title, ids);
	}
	
	protected static String create_or_update_corpus(String sessionID, String corpusID, String title, String ids) {
		URL url2;
		String action = "";
		try {
			if (ids == null || ids.length() == 0) {
				if (debug) System.out.println("Error create_or_update_corpus needs a lsit of IDs '|' separated.");
				return "";
			}
			
			if (title != null && title.length() > 0) {
				action = "create";
			} else if (corpusID != null && corpusID.length() > 0) {
				action = "update";
			} else {
				if (debug) System.out.println("Error create_or_update_corpus needs a title or a corpus ID.");
				return "";
			}
			
			if (action == "create") {
				url2 = new URL(baseUrl+"create_corpus?title="+URLEncoder.encode(title, "UTF-8"));
			} else if (action == "update") {
				url2 = new URL(baseUrl+"create_corpus?uri="+URLEncoder.encode(corpusID, "UTF-8"));
			}
			
			if (debug) System.out.println("Requesting: "+action+" : "+url2);
			HttpURLConnection conn2 = (HttpURLConnection) url2.openConnection();
			conn2.setRequestProperty("content-type", "text/plain; charset=UTF-8");
			conn2.setRequestProperty("session", sessionID);
			conn2.setDoOutput(true);
			
			if (debug) System.out.println("Writing POST...");		
			
			DataOutputStream wr = new DataOutputStream(conn2.getOutputStream());
			
			if (debug) println "POST="+URLEncoder.encode(ids, "UTF-8")
			wr.writeBytes(ids);
			wr.flush();
			wr.close();
			
			if (debug) System.out.println("getResponseCode...");	
			int code2 = conn2.getResponseCode();
			if (code2 != HttpURLConnection.HTTP_OK) {
				if (debug) System.out.println("Error: during corpus "+action+" error='"+code2+"', command="+url2+", post="+URLEncoder.encode(ids, "UTF-8"));
				return "";
			}
			if (debug) System.out.println("getContent...");	
			String content2 = getContent(conn2).trim();
			if (content2.startsWith("\"") && content2.endsWith("\"")) {
				content2 = content2.substring(1, content2.length() -1)
			}
			
			conn2.disconnect();
			
			return content2;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static String getCorpusAccessURL(String corpusID, String title) {
		
		String url = okapiUrl+"?linkedObjectUri="+URLEncoder.encode(corpusID, "UTF-8")
		
		if (title != null && title.length() > 0) {
			url += "&linkedObjectLabel="+URLEncoder.encode(title, "UTF-8")
		}
	}
	
	public static String get_user_corpus(String sessionID, String user, String lang, String limit, String offset) {
		URL url2;
		String action = "";
		try {
			if (user == null || user.length() == 0) {
				if (debug) System.out.println("Error get_user_corpus needs a user.");
				return "";
			}
			
			String urlString = baseUrl+"get_user_corpus?user="+URLEncoder.encode(user, "UTF-8");
			
			if (lang != null && lang.length() > 0) {
				urlString += "&lang="+URLEncoder.encode(lang, "UTF-8");
			}
			if (limit != null && lang.length() > 0) {
				urlString += "&limit="+URLEncoder.encode(limit, "UTF-8");
			}
			if (offset != null && offset.length() > 0) {
				urlString += "&offset="+URLEncoder.encode(offset, "UTF-8");
			}
			
			url2 = new URL(urlString);
			if (debug) System.out.println("Requesting get_user_corpus: "+action+" : "+url2);
			HttpURLConnection conn2 = (HttpURLConnection) url2.openConnection();
			conn2.setRequestMethod("GET");
			conn2.setRequestProperty("session", sessionID);
			conn2.connect();
			int code2 = conn2.getResponseCode();
			if (code2 != HttpURLConnection.HTTP_OK) {
				if (debug) System.out.println("Error: during corpus "+action+": "+code2+" and command="+url2);
				return "";
			}
			
			String content2 = getContent(conn2);
			
			conn2.disconnect();
			
			return content2;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static void main(String[] args) throws Exception {
		
		String ids = "AFE86000904|AFE86000904|AFE86000904";
		String title = "txmtest";
		String corpusID = "http://www.campus-AAR.fr/corpus_1307765428";
		String user = 'USER';
		String password = 'PASSWORD';
		
		String sessionID = OkapiSaphirAPI.login(user, password);
		if (sessionID.length() > 0) {
			System.out.println(OkapiSaphirAPI.get_user_corpus(sessionID, user, null, null, null));
			
			String newCorpusID = OkapiSaphirAPI.create_corpus(sessionID, title, "AFE86000905");
			System.out.println("corpus created: "+newCorpusID);
			println "Access URL: "+OkapiSaphirAPI.getCorpusAccessURL(newCorpusID)
			//System.out.println("Updating: "+update_corpus(sessionID, corpusID, "AFE86000905"));
		}
	}
	
	public static String getOkapiSessionID(URLConnection conn) throws Exception {
		
		String encoding = "UTF-8";
		Map<String, List<String>> header1 = conn.getHeaderFields();
		// retrieve the okapi session id
		List<String> cookies = header1.get("Set-Cookie");
		if (cookies == null) {
			if (debug) System.out.println("Error: not connected, 'Set-Cookie' not found");
			return "";
		}
		String sessionID = "";
		for (String c : cookies) {
			if (c.startsWith("okapi=")) {
				sessionID = c.substring("okapi=".length(), c.indexOf(";", "okapi=".length()));
			}
		}
		
		return sessionID
	}
	
	public static String getContent(URLConnection conn) throws Exception {
		
		String encoding = "UTF-8";
		Map<String, List<String>> header1 = conn.getHeaderFields();
		List<String> contentType = header1.get("Content-Type");
		if (contentType != null) {
			for (String c : contentType) {
				if (c.contains("charset=")) {
					encoding = c.substring(c.indexOf("charset=") + 8, c.length());
				}
			}
		}
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), encoding));
		StringBuilder buffer = new StringBuilder();
		String line = reader.readLine();
		while (line != null) {
			buffer.append(line+"\n");
			line = reader.readLine();
		}
		reader.close();
		return buffer.toString();
	}
	
	//TODO remove this and call UsernamePasswordDialog.initializeCredentials
	public static boolean initializeCredentials(Shell shell, String userKey, String passwordKey, String target, String title, String details) {
		
		if (System.getProperty(userKey) == null
			|| System.getProperty(passwordKey) == null
			|| System.getProperty(userKey).length() == 0
			|| System.getProperty(passwordKey).length() == 0) {
			
			UsernamePasswordDialog dialog = new UsernamePasswordDialog(shell, [userKey != null, passwordKey != null ] as boolean[], target);
			if (userKey != null) dialog.setUsername(System.getProperty(userKey));
			dialog.setTitle(title);
			dialog.setDetails(TXMUIMessages.bind(TXMUIMessages.loginToP0, details));
			if (dialog.open() == UsernamePasswordDialog.OK) {
				if (userKey != null) System.setProperty(userKey, dialog.getUser());
				if (passwordKey != null) System.setProperty(passwordKey, dialog.getPassword());
				return true;
			}
			else {
				if (debug)  System.out.println("Cancel credential settings");
				return false;
			}
		}
		return true;
	}
	
	//TODO remove this and call UsernamePasswordDialog.initializeCredentials
	public static boolean initializeCredentials(Shell shell, String userKey, String passwordKey, String target, String title, String details, JobHandler monitor) {
		
		final def ret = [false]
		
		monitor.syncExec(new Runnable() {
			public void run() {
				if (debug) System.out.println("Opening credential dialog...");
				try {
					ret[0] = initializeCredentials(shell, userKey, passwordKey, target, title, details);
				} catch(Throwable t) {
					t.printStackTrace();
				}
			}
		});
		return ret[0];
	}
}
