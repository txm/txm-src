// Copyright © 2021 ENS-LYON
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro.projects.antract

import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.macro.projects.antract.OkapiSaphirAPI
import org.txm.searchengine.cqp.CQPSearchEngine

import org.txm.backtomedia.preferences.BackToMediaPreferences

if (!(corpusViewSelection instanceof CQPCorpus)) {
	monitorShowError("Erreur : la sélection de la vue Corpus n'est pas un corpus ($corpusViewSelection).")
	return false
} 

@Field @Option(name="titre_corpus_okapi", usage="Nom du corpus à créer", widget="String", required=false, def="")
def titre_corpus_okapi

if (!ParametersDialog.open(this)) return;

CQPCorpus corpus = corpusViewSelection
corpus.compute()
CQPCorpus parentCorpus = corpus.getMainCorpus()

String parentCorpusName = parentCorpus.getCqpId()

def struct_prop = null
def finalValues = []
def ids = null

if (parentCorpusName.startsWith("AF-VOIX-OFF-") || parentCorpusName.startsWith("AFNOTICES") || parentCorpusName.startsWith("AF-NOTICES") || parentCorpusName.startsWith("AF-PLAN")) {
	ids = gse.run(ListerIdentifiantsSujetsMacro, ["args":[:], "corpusViewSelection":corpusViewSelection, "monitor":monitor])
} else {
	monitorShowError("Erreur: le corpus n'est pas un corpus d'AF-VOIX-OFF, ni AF-NOTICE, ni AF-PLAN, ni AFNOTICE")
	return false
}

monitor.syncExec(new Runnable() {
	public void run() {
		if (!OkapiSaphirAPI.initializeCredentials(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), BackToMediaPreferences.MEDIA_AUTH_LOGIN, BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "OKAPI - Antract", "S'identifier sur la plateforme Okapi.", "OKAPI - Antract", monitor)) {
			return null;
		}
	}
});

String user = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN)
String password = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD)
if (user == null || user.length() == 0 || password == null || password.length() == 0) {
	println "Annulation."
}

String sessionID = OkapiSaphirAPI.login(user, password)
if (sessionID.length() > 0) {
	String corpusID = OkapiSaphirAPI.create_corpus(sessionID, titre_corpus_okapi, ids)
	if (corpusID.length() > 0 && corpusID.startsWith("http")) {
		println "Le corpus 'titre_corpus_okapi' son identifiant est $corpusID"
		println "Lien d'accès direct : "+OkapiSaphirAPI.getDirectAccessURL(corpusID)
	} else {
		monitorShowError("Erreur : Le corpus Okapi n'a pas été créé (code d'erreur=$corpusID)")
	}
} else {
	monitorShowError("Erreur : La session Okapi n'a pu être démarrée.")
	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN, "")
	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "")
}

def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Erreur", message)
		}
	});
}
