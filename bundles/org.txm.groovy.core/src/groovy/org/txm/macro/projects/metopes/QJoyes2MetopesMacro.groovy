package org.txm.macro.edition
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.objects.*
import org.txm.searchengine.cqp.corpus.*;
import org.txm.utils.ConsoleProgressBar
import org.w3c.dom.*
import org.txm.importer.*
import org.txm.Toolbox
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.txm.rcpapplication.commands.*
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.txm.utils.io.FileCopy

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "This marcro works with a MainCorpus selection. Aborting"
	return;
}
println "Working on $corpusViewSelection corpus"
def corpus = corpusViewSelection

@Field @Option (name="xslEdition", usage="XSL to build an XML-TEI Metopes' file", widget="FileOpen", def="QJoyes-TXM-to-tokenized.xsl")
		def xslEdition

@Field @Option (name="xslEdition1", usage="XSL to build an XML-TEI Metopes' file", widget="FileOpen", def="QJoyes-tokenized-to-metopes1.xsl")
		def xslEdition1

@Field @Option (name="xslEdition2", usage="XSL to build an XML-TEI Metopes' file", widget="FileOpen", def="QJoyes-tokenized-to-metopes2.xsl")
		def xslEdition2

@Field @Option (name="xslEdition3", usage="XSL to build an XML-TEI Metopes' file", widget="FileOpen", def="QJoyes-tokenized-to-metopes3-norm1.xsl")
		def xslEdition3

@Field @Option (name="xslEdition4", usage="XSL to build an XML-TEI Metopes' file", widget="FileOpen", def="QJoyes-tokenized-to-metopes4-norm2.xsl")
		def xslEdition4

@Field @Option (name="xslEdition5", usage="XSL to build an XML-TEI Metopes' file", widget="FileOpen", def="QJoyes-tokenized-to-metopes3-dipl.xsl")
		def xslEdition5

@Field @Option (name="editionName", usage="The edition name to produce", widget="String", required=true, def="normalisee")
		String editionName

@Field @Option(name="useTokenizedDirectory", usage="Use the 'XML/w' of the 'tokenized' directory instead of the 'XML-TXM' files", widget="Boolean", required=false, def="true")
		def useTokenizedDirectory = true

@Field @Option (name="debug", usage="Enable debug mode: temporary files are not deleted", widget="Boolean", required=false, def="false")
		def debug = false

if (!ParametersDialog.open(this)) return;

	//	defaultEditionName = corpus.getDefaultEdition()
	Project project = corpus.getProject()
	String pname = project.getName()
	def defaultEditions = project.getDefaultEditionName().split(",") as List
	defaultEditionName = defaultEditions[0]
	corpusName = corpus.getName()
	//	binDirectory = corpus.getBase().getBaseDirectory()
	//	txmDirectory = new File(binDirectory, "txm/"+corpusName.toUpperCase())
	//	params = corpus.getBase().params
	File binDirectory = corpus.getProject().getProjectDirectory()
	File txmDirectory = new File(binDirectory, "txm/"+corpus.getID().toUpperCase())
	
new File(binDirectory,"Metopes/$corpusName").deleteDir();
new File(binDirectory,"Metopes/$corpusName").mkdirs();

if (editionName == null || editionName.length() == 0) {
	editionName = defaultEditionName
}
println "Parameters:"
println "	xslEdition = $xslEdition"
println "	editionName = $editionName"
println "	useTokenizedDirectory = $useTokenizedDirectory"

File xslDirectory = new File(Toolbox.getTxmHomePath(), "xsl/metopes")
if (xslEdition == null || !xslEdition.exists())
	xslEdition = new File(xslDirectory, "QJoyes-TXM-to-tokenized.xsl")

if (xslEdition1 == null || !xslEdition1.exists())
	xslEdition1 = new File(xslDirectory, "QJoyes-tokenized-to-metopes1.xsl")

if (xslEdition2 == null || !xslEdition2.exists())
	xslEdition2 = new File(xslDirectory, "QJoyes-tokenized-to-metopes2.xsl")

if (xslEdition3 == null || !xslEdition3.exists())
	xslEdition3 = new File(xslDirectory, "QJoyes-tokenized-to-metopes3-norm1.xsl")

if (xslEdition4 == null || !xslEdition4.exists())
	xslEdition4 = new File(xslDirectory, "QJoyes-tokenized-to-metopes4-norm2.xsl")
	
if (xslEdition5 == null || !xslEdition5.exists())
	xslEdition5 = new File(xslDirectory, "QJoyes-tokenized-to-metopes5-dipl.xsl")

if (useTokenizedDirectory) {
	println "Using the 'tokenized' directory to get XML files"
	txmDirectory = new File(binDirectory, "tokenized")
}
File MetopesDirectory = new File(binDirectory, "Metopes")
File MetopesCorpusDirectory = new File(MetopesDirectory, corpusName.toUpperCase())
File defaultEditionDirectory = new File(MetopesCorpusDirectory, editionName)

boolean newEdition = false;
if (!txmDirectory.exists()) {
	println "ERROR: can't find this corpus 'txm' directory: $txmDirectory. Aborting"
	return false;
}
if (!defaultEditionDirectory.exists()) {
	newEdition = true;
	defaultEditionDirectory.mkdir()
	if (!defaultEditionDirectory.exists()) {
		println "Metopes directory couldn't be created: $defaultEditionDirectory. Aborting"
		return false
	}
}

if (!xslEdition.exists()) {
	println "Error: can't find $xslEdition XSL file"
	return false;
}

//defaultEditionDirectory.mkdir()


//1- Back up current "Metopes" directory
if (!newEdition) {
	File backupDirectory = new File(binDirectory, "Metopes-"+defaultEditionDirectory.getName()+"-back")
	backupDirectory.mkdir()
	println "Backup of $defaultEditionDirectory directory to $backupDirectory..."
	def files = defaultEditionDirectory.listFiles()
	ConsoleProgressBar cpb = new ConsoleProgressBar(files.length)
	for (File f : files) {
		String name = f.getName()
		if (f.isDirectory() || f.isHidden()) continue
		
		File rez = new File(backupDirectory, f.getName())
		
		if (debug) println " file $f >> $rez"
		
		cpb.tick()
		
		if (!FileCopy.copy(f, rez)) {
			println "Error: failed to backup $f"
			return false;
		}
	}
	cpb.done()
} else {
	println "No new edition."
}

//2- Apply XSL
if (!useTokenizedDirectory) {
	println "Applying XSL 1: $xslEdition..."
	ApplyXsl2 applier = new ApplyXsl2(xslEdition);
	def MetopesFiles = []
	def files = txmDirectory.listFiles()
	ConsoleProgressBar cpb = new ConsoleProgressBar(files.length)
	for (File f : files) {
		String name = f.getName()
		String txtname = name.substring(0, name.indexOf("."));
		File rez = new File(MetopesCorpusDirectory, txtname+".xml")
		
		if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
			
			if (debug) println " file $f >> $rez"
			cpb.tick()
			
			if (!applier.process(f, rez)) {
				println "Error: failed to process $f"
				return false
			} else {
				MetopesFiles << rez
			}
		}
		
	}
	cpb.done()
	
	println "Applying XSL 2: $xslEdition1..."
	ApplyXsl2 applier1 = new ApplyXsl2(xslEdition1);
	def Metopes1Files = []
	cpb = new ConsoleProgressBar(MetopesFiles.size())
	for (File f : MetopesFiles) {
		String name = f.getName()
		String txtname = name.substring(0, name.indexOf("."));
		File rez = new File(MetopesCorpusDirectory, txtname+".xml")
		
		if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
			
			if (debug) println " file $f >> $rez"
			cpb.tick()
			
			if (!applier1.process(f, rez)) {
				println "Error: failed to process $f"
				return false
			} else {
				Metopes1Files << rez
			}
		}
	}
	cpb.done()
	
	println "Applying XSL 3: $xslEdition2..."
	ApplyXsl2 applier2 = new ApplyXsl2(xslEdition2);
	applier2.setParam("editionname", editionName)
	applier2.setParam("cssname", corpusName)
	def Metopes2Files = []
	cpb = new ConsoleProgressBar(Metopes1Files.size())
	for (File f : Metopes1Files) {
		String name = f.getName()
		String txtname = name.substring(0, name.indexOf("."));
		File rez = new File(MetopesCorpusDirectory, txtname+".xml")
		
		if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
			
			if (debug) println " file $f >> $rez"
			cpb.tick()
			
			if (!applier2.process(f, rez)) {
				println "Error: failed to process $f"
				return false
			} else {
				Metopes2Files << rez
			}
		}
	}
	cpb.done()
	
	println "Applying XSL 4: $xslEdition3... (norm1)"
	ApplyXsl2 applier3 = new ApplyXsl2(xslEdition3);
	def Metopes3Files = []
	cpb = new ConsoleProgressBar(Metopes2Files.size())
	for (File f : Metopes2Files) {
		String name = f.getName()
		String txtname = name.substring(0, name.indexOf("."));
		File rez = new File(MetopesCorpusDirectory, txtname+"_norm1.xml")
		
		if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
			
			if (debug) println " file $f >> $rez"
			cpb.tick()
			
			if (!applier3.process(f, rez)) {
				println "Error: failed to process $f"
				return false
			} else {
				Metopes3Files << rez
			}
		}
	}
	cpb.done()
	
	println "Applying XSL 5: $xslEdition4... (norm2) on $Metopes2Files"
	ApplyXsl2 applier4 = new ApplyXsl2(xslEdition4);
	def Metopes4Files = []
	cpb = new ConsoleProgressBar(Metopes2Files.size())
	for (File f : Metopes2Files) {
		String name = f.getName()
		String txtname = name.substring(0, name.indexOf("."));
		File rez = new File(MetopesCorpusDirectory, txtname+"_norm2.xml")
		
		if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
			
			if (debug) println " file $f >> $rez"
			cpb.tick()
			
			if (!applier4.process(f, rez)) {
				println "Error: failed to process $f"
				return false
			} else {
				Metopes4Files << rez
			}
		}
	}
	cpb.done()
	
	println "Applying XSL 6: $xslEdition5... (dipl) on $Metopes2Files"
	ApplyXsl2 applier5 = new ApplyXsl2(xslEdition5);
	def Metopes5Files = []
	cpb = new ConsoleProgressBar(Metopes2Files.size())
	for (File f : Metopes2Files) {
		String name = f.getName()
		String txtname = name.substring(0, name.indexOf("."));
		File rez = new File(MetopesCorpusDirectory, txtname+"_dipl.xml")
		
		if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
			
			if (debug) println " file $f >> $rez"
			cpb.tick()
			
			if (!applier5.process(f, rez)) {
				println "Error: failed to process $f"
				return false
			} else {
				Metopes5Files << rez
			}
		}
	}
	cpb.done()
	
}
println "Done: "+MetopesCorpusDirectory.getAbsolutePath()

//if(useTokenizedDirectory) {
//println "Applying XSL 2: $xslEdition1..."
//ApplyXsl2 applier1 = new ApplyXsl2(xslEdition1);
//def Metopes1Files = []
//for (File f : txmDirectory.listFiles()) {
//	String name = f.getName()
//	String txtname = name.substring(0, name.indexOf("."));
//	File rez = new File(MetopesCorpusDirectory, txtname+".xml")

//	if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
//
//		if (debug) println " file $f >> $rez"
//		else print "."
//
//		if (!applier1.process(f, rez)) {
//			println "Error: failed to process $f"
//			return false
//		} else {
//			Metopes1Files << rez
//		}
//	}
//}
//println ""

//println "Applying XSL 3: $xslEdition2..."
//ApplyXsl2 applier2 = new ApplyXsl2(xslEdition2);
//applier2.setParam("editionname", editionName)
//applier2.setParam("cssname", corpusName)
//def Metopes2Files = []
//for (File f : Metopes1Files) {
//	String name = f.getName()
//	String txtname = name.substring(0, name.indexOf("."));
//	File rez = new File(MetopesCorpusDirectory, txtname+".xml")

//	if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
//
//		if (debug) println " file $f >> $rez"
//		else print "."
//
//		if (!applier2.process(f, rez)) {
//			println "Error: failed to process $f"
//			return false
//		} else {
//			Metopes2Files << rez
//		}
//	}
//}
//println ""

//println "Applying XSL 4: $xslEdition3..."
//ApplyXsl2 applier3 = new ApplyXsl2(xslEdition3);
//def Metopes3Files = []
//for (File f : Metopes2Files) {
//	String name = f.getName()
//	String txtname = name.substring(0, name.indexOf("."));
//	File rez = new File(MetopesCorpusDirectory, txtname+".xml")

//	if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
//
//		if (debug) println " file $f >> $rez"
//		else print "."
//
//		if (!applier3.process(f, rez)) {
//			println "Error: failed to process $f"
//			return false
//		} else {
//			Metopes3Files << rez
//			}
//		}
//	}
//}
//println ""


//println "Applying XSL 5: $xslEdition4..."
//ApplyXsl2 applier4 = new ApplyXsl2(xslEdition4);
//def Metopes3Files = []
//for (File f : Metopes2Files) {
//	String name = f.getName()
//	String txtname = name.substring(0, name.indexOf("."));
//	File rez = new File(MetopesCorpusDirectory, txtname+".xml")

//	if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
//
//		if (debug) println " file $f >> $rez"
//		else print "."
//
//		if (!applier3.process(f, rez)) {
//			println "Error: failed to process $f"
//			return false
//		} else {
//			Metopes3Files << rez
//			}
//		}
//	}
//}
//println ""


//println "Applying XSL 6: $xslEdition5..."
//ApplyXsl2 applier5 = new ApplyXsl2(xslEdition5);
//def Metopes3Files = []
//for (File f : Metopes2Files) {
//	String name = f.getName()
//	String txtname = name.substring(0, name.indexOf("."));
//	File rez = new File(MetopesCorpusDirectory, txtname+".xml")

//	if (!f.isDirectory() && !f.isHidden() && name.endsWith(".xml") && !name.equals("import.xml")) {
//
//		if (debug) println " file $f >> $rez"
//		else print "."
//
//		if (!applier3.process(f, rez)) {
//			println "Error: failed to process $f"
//			return false
//		} else {
//			Metopes3Files << rez
//			}
//		}
//	}
//}
//println ""


////4- register new edition if any (copy edition)

//if (editionName != defaultEditionName) {
//	println "Update corpus configuration"
//
//	// remove edition declaration if any
//	RemoveTag rt = new RemoveTag(
//			params.root.getOwnerDocument(), // will be updated
//			null, // don't create a new import.xml
//			"//edition[@name='$editionName']"
//			)

//	def corpusElem = params.getCorpusElement()
//	params.addEditionDefinition(corpusElem, editionName, "xsl", "XSLEditionBuilder"); // declare the new edition
//
//	for (def text : corpus.getTexts()) {
//		Element textElem = text.getSelfElement()
//		def defaultEdition = text.getEdition(defaultEditionName)
//		if (defaultEdition == null) { println "Error: no default edition with name="+defaultEditionName; return false}
//		Element editionElem = params.addEdition(textElem, editionName, defaultEditionDirectory.getAbsolutePath(), "html");
//		def pages = defaultEdition.getPages()
//		for (int i = 1 ; i <= pages.size() ; i++) {
//			def page = pages[i-1]
//			params.addPage(editionElem, "$i", page.getWordId());
//		}
//	}
//
//	File paramFile = new File(binDirectory, "import.xml");
//	DomUtils.save(params.root.getOwnerDocument(), paramFile);
//}

////5- Reload Corpora
//if (args.size() == 0) { // the user run the macro
//	Toolbox.restart();
//	monitor.syncExec(new Runnable() {
//			public void run() {
//				RestartTXM.reloadViews();
//			}
//		});
//}
println "XML-TEI Metopes files created."

