// Copyright © 2022 ENS-LYON
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro.projects.antract

import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.macro.projects.antract.OkapiSaphirAPI
import org.txm.searchengine.cqp.CQPSearchEngine

import org.txm.backtomedia.preferences.BackToMediaPreferences

@Field @Option(name="liste_identifiants_sujets", usage="Listes des identifiants sujets à ajouter", widget="String", required=false, def="")
def liste_identifiants_sujets

@Field @Option(name="identifiant_corpus_okapi", usage="Identifiant Okapi du corpus à modifier", widget="String", required=false, def="")
def identifiant_corpus_okapi

if (!ParametersDialog.open(this)) return;

CQPCorpus corpus = corpusViewSelection
corpus.compute()
CQPCorpus parentCorpus = corpus.getMainCorpus()

String parentCorpusName = parentCorpus.getCqpId()

def struct_prop = null
def finalValues = []

monitor.syncExec(new Runnable() {
	public void run() {
		if (!OkapiSaphirAPI.initializeCredentials(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), BackToMediaPreferences.MEDIA_AUTH_LOGIN, BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "OKAPI - Antract", "S'identifier sur la plateforme Okapi.", "OKAPI - Antract", monitor)) {
			return null;
		}
	}
});

String user = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN)
String password = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD)
if (user == null || user.length() == 0 || password == null || password.length() == 0) {
	println "Annulation."
}

// start the Okapi session
String sessionID = OkapiSaphirAPI.login(user, password)
if (sessionID.length() > 0) {
	String corpusID = OkapiSaphirAPI.update_corpus(sessionID, identifiant_corpus_okapi, liste_identifiants_sujets)
	if (corpusID.length() > 0 && corpusID.startsWith("http") && corpusID.equals(identifiant_corpus_okapi)) {
		println "Le corpus Okapi $identifiant_corpus_okapi a été mis à jour avec $liste_identifiants_sujets"
	} else {
		monitorShowError("Erreur : Le corpus Okapi n'a pas été mis à jour  (session=$sessionID, corpus=$identifiant_corpus_okapi et identifiants=$liste_identifiants_sujets)")
	}
} else {
	monitorShowError("Erreur : la session Okapi n'a pas pu être démarrée.")
	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN, "")
	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "")
}

def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Erreur", message)
		}
	});
}
