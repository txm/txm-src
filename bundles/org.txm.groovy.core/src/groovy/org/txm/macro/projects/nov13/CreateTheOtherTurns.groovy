package org.txm.macro.projects.nov13

import javax.xml.stream.*

import org.txm.importer.PersonalNamespaceContext
import org.txm.utils.FileUtils
import org.txm.xml.IdentityHook
import org.txm.xml.*

import java.io.BufferedOutputStream
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.util.*
import java.util.Map.Entry
import java.util.regex.Pattern

class CreateTheOtherTurns extends XMLProcessor {
	
	LocalNamesHookActivator activator;
	IdentityHook hook;
	
	def primarySpeakerIdRegex
	String primarySpeakerId
	
	Boolean debug
	
	String otherNonPrimarySpeakerId = "other"
	
	def warnings = []
	
	public CreateTheOtherTurns(File xmlfile, String primarySpeakerIdRegexString, String otherNonPrimarySpeakerId, Boolean debug) {
		super(xmlfile)
		this.debug = debug
		
		this.otherNonPrimarySpeakerId = otherNonPrimarySpeakerId
		if (primarySpeakerIdRegexString != null && primarySpeakerIdRegexString.length() > 0) {
			String id = FileUtils.stripExtension(xmlfile)
			
			this.primarySpeakerIdRegex = /$primarySpeakerIdRegexString/
			
			def rez = (id =~ primarySpeakerIdRegex).findAll()
			def rez2 = (id =~ /$primarySpeakerIdRegex/).findAll()
			if (rez2.size() != 1) {
				if (debug) println "WARNING: found the ${rez2.size()} matches of primary speaker prefix in the '$id' file name"
				this.primarySpeakerIdRegex = null
			} else {
				primarySpeakerId = rez[0]
				//if (debug) println "Detected primary speaker: $primarySpeakerId"
			}
		}
		
		activator = new LocalNamesHookActivator<>(hook, ["Speaker", "w", "Turn", "Sync"]);
		
		hook = new IdentityHook("word_hook", activator, this) {
					
					boolean inTurn = false;
					
					boolean inW = false;
					StringBuilder wordBuffer = new StringBuilder();
					
					String currentTime;
					LinkedHashMap turnInfos = new LinkedHashMap()
					LinkedHashMap wInfos = new LinkedHashMap()
					boolean other
					
					@Override
					public boolean deactivate() {
						return true;
					}
					
					@Override
					public boolean _activate() {
						return true;
					}
					
					@Override
					protected void processStartElement() throws XMLStreamException, IOException {
						if (localname.equals("Speaker")) { // find out the main speaker
							String id = parser.getAttributeValue(null, "id")// id
							if (id ==~ primarySpeakerIdRegex) {
								primarySpeakerId = id
							}
							super.processStartElement();
						} else if (localname.equals("Turn")) {
							// store values
							inTurn = true;
							turnInfos.clear()
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								turnInfos[parser.getAttributeLocalName(i)] = parser.getAttributeValue(i)
							}
							currentTime = turnInfos["startTime"]
							super.processStartElement();
						} else if (localname.equals("Sync")) {
							currentTime = parser.getAttributeValue(null, "time")
							super.processStartElement();
						} else if (localname.equals("w")) {
							// store values
							inW = true;
							wInfos.clear()
							for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
								wInfos[parser.getAttributeLocalName(i)] = parser.getAttributeValue(i)
							}
							String time = parser.getAttributeValue(null, "time")
							if (time != null && time.length() > 0) {
								currentTime = time
							}
							wordBuffer.setLength(0);
							return; // write w later
						}
						else {
							super.processStartElement();
						}
					}
					
					@Override
					protected void processCharacters() throws XMLStreamException {
						if (inW) {
							wordBuffer.append(parser.getText())
						}
						else {
							super.processCharacters();
						}
					}
					
					protected void writeWord(String word) {
						writer.writeStartElement("w") // start the initial word
						for (String attr : wInfos.keySet() ) {
							writer.writeAttribute(attr, wInfos[attr])
						}
						writer.writeCharacters(word)
						writer.writeEndElement() // w
					}
					
					def startOtherReg = /^([^*])?\*([^\p{Zs}]+.*)$/
					def endOtherReg = /^(.*[^\p{Zs}]+)\*([^*])?$/
					def startAndEndOtherReg = /^([^*])?\*(.*[^\p{Zs}]+)\*([^*])?$/
					String previousOtherStarting = "<none>"
					@Override
					protected void processEndElement() throws XMLStreamException {
						if (localname.equals("w")) {
							
							inW = false
							String word = wordBuffer.toString().trim()
							String wordToWrite = word
							boolean shouldCloseOtherTurn = false;
							
							def m0 = word =~ startAndEndOtherReg
							def m1 = word =~ startOtherReg
							def m2 = word =~ endOtherReg
							
							if (word.trim().equals("*")) {
								if (debug) println "- ligne "+parser.getLocation().getLineNumber()+" : ouverture|fermeture de other avec '$word' -> tours '$turnInfos'"
								
								if (other) { // closing *
									previousOtherStarting = ["word='*' location="+getLocation(true, false, false)]
									
									shouldCloseOtherTurn = true;
									
									wordToWrite = ""
								} else {
									previousOtherStarting = ["word='*' location="+getLocation(true, false, false)]
									
									//if (other) { // don't restart a Turn if already in a Other Turn
										writer.writeEndElement() // current Turn
										writer.writeCharacters("\n")
										
										def tmpInfos = new LinkedHashMap()
										for (String attr : turnInfos.keySet()) tmpInfos[attr] = turnInfos[attr]
										tmpInfos["orig-speaker"] = turnInfos["speaker"]
										
										if (primarySpeakerIdRegex == null || turnInfos["speaker"] ==~ primarySpeakerIdRegex) { // the current speaker is not the primary speaker
											tmpInfos["speaker"] = otherNonPrimarySpeakerId
										} else {
											tmpInfos["speaker"] = primarySpeakerId
										}
										tmpInfos["startTime"] = currentTime
										writer.writeStartElement("Turn")
										for (String attr : tmpInfos.keySet()) {
											writer.writeAttribute(attr, tmpInfos[attr])
										}
										writer.writeCharacters("\n")
										
										writer.writeStartElement("Sync")
										writer.writeAttribute("time", tmpInfos["startTime"])
										writer.writeCharacters("\n")
										writer.writeEndElement()
									//}
									
									other = true
									wordToWrite = ""
								}
							} else if (m0.matches()) {
								if (other) {
									warnings << getLocation(true, false, false)+" with $word: Found a starting&ending * when one 'other' have been started at "+previousOtherStarting
								}
								// else {
								
								if (debug) println "- ligne "+parser.getLocation().getLineNumber()+" : ouverture&fermeture de other avec '$word' -> tours '$turnInfos'"
								
								writer.writeEndElement() // current Turn
								writer.writeCharacters("\n")
								
								def tmpInfos = new LinkedHashMap()
								for (String attr : turnInfos.keySet()) tmpInfos[attr] = turnInfos[attr]
								tmpInfos["orig-speaker"] = turnInfos["speaker"]
								
								if (primarySpeakerIdRegex == null || turnInfos["speaker"] ==~ primarySpeakerIdRegex) { // the current speaker is not the primary speaker
									tmpInfos["speaker"] = otherNonPrimarySpeakerId
								} else {
									tmpInfos["speaker"] = primarySpeakerId
								}
								tmpInfos["startTime"] = currentTime
								writer.writeStartElement("Turn")
								for (String attr : tmpInfos.keySet()) {
									writer.writeAttribute(attr, tmpInfos[attr])
								}
								writer.writeCharacters("\n")
								
								writer.writeStartElement("Sync")
								writer.writeAttribute("time", tmpInfos["startTime"])
								writer.writeCharacters("\n")
								writer.writeEndElement()
								
								String group1 = m0.group(1)
								if (group1 != null && group1.length() > 0) { // write heading chars before the marker
									writeWord(group1)
									writer.writeCharacters("\n")
								}
								
								shouldCloseOtherTurn = true;
								wordToWrite = m0.group(2)
								other = false
								//}
							} else if (m1.matches()) { // not and start&end but only a start
								
								if (other) {
									warnings << getLocation(true, false, false)+" with $word: Found a starting * when one 'other' have been started at "+previousOtherStarting
								}
								// else {
								if (debug) println "- ligne "+parser.getLocation().getLineNumber()+" : ouverture de other avec '$word' -> tours '$turnInfos'"
								//close current Turn and start a 'other' Turn
								previousOtherStarting = ["word="+word+ " location="+getLocation(true, false, false)]
								String group1 = m1.group(1)
								if (group1 != null && group1.length() > 0) { // write heading chars before the marker
									writeWord(group1)
									writer.writeCharacters("\n")
								}
								
								//if (other) { // don't restart a Turn if already in a Other Turn
									writer.writeEndElement() // current Turn
									writer.writeCharacters("\n")
									
									def tmpInfos = new LinkedHashMap()
									for (String attr : turnInfos.keySet()) tmpInfos[attr] = turnInfos[attr]
									tmpInfos["orig-speaker"] = turnInfos["speaker"]
									
									if (primarySpeakerIdRegex == null || turnInfos["speaker"] ==~ primarySpeakerIdRegex) { // the current speaker is not the primary speaker
										tmpInfos["speaker"] = otherNonPrimarySpeakerId
									} else {
										tmpInfos["speaker"] = primarySpeakerId
									}
									tmpInfos["startTime"] = currentTime
									writer.writeStartElement("Turn")
									for (String attr : tmpInfos.keySet()) {
										writer.writeAttribute(attr, tmpInfos[attr])
									}
									writer.writeCharacters("\n")
									
									writer.writeStartElement("Sync")
									writer.writeAttribute("time", tmpInfos["startTime"])
									writer.writeCharacters("\n")
									writer.writeEndElement()
								//}
								
								other = true
								wordToWrite = m1.group(2)
								//}
							} else if (m2.matches()) {
								if (debug) println "- ligne "+parser.getLocation().getLineNumber()+" : fermeture de other avec '$word' -> tours '$turnInfos'"
								
								if (!other) {
									warnings << getLocation(true, false, false)+" with '$word': Found a closing * when one 'other' have been closed at "+previousOtherStarting
								}
								
								previousOtherStarting = ["word='"+wordToWrite+ "' location="+getLocation(true, false, false)]
								
								
								
								shouldCloseOtherTurn = true;
								
								wordToWrite = m2.group(1)
							}
							
							if (wordToWrite.length() > 0) {
								writeWord(wordToWrite)
							}
							
							if (shouldCloseOtherTurn) {
								
								shouldCloseOtherTurn = false;
								//close the current 'other' Turn and restart the actual Turn
								writer.writeCharacters("\n")
								writer.writeEndElement() // current 'other' Turn
								writer.writeCharacters("\n")
								
								writer.writeStartElement("Turn") // rebuild the orig Turn and fix its start-end infos
								turnInfos["startTime"] = wInfos["end"] // fix the startTime using the current word end time
								for (String attr : turnInfos.keySet()) {
									writer.writeAttribute(attr, turnInfos[attr])
								}
								writer.writeCharacters("\n")
								
								writer.writeStartElement("Sync")
								writer.writeAttribute("time", turnInfos["startTime"])
								writer.writeCharacters("\n")
								writer.writeEndElement()
								
								if (m0.matches() && m0.group(3) != null && m0.group(3).length() > 0) {
									writeWord(m0.group(3))
								} else if (m2.matches() && m2.group(2) != null && m2.group(2).length() > 0) {
									writeWord(m2.group(2))
								}
								
								other = false
							}
						} else {
							super.processEndElement();
						}
					}
				}
	}
}
