package org.txm.macro.projects.antract

import java.io.File
import java.nio.charset.Charset

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.Arrays
import java.util.HashMap
import java.util.List

import org.txm.libs.msoffice.ReadExcel
import org.txm.utils.*

@Field @Option(name="tableFile", usage="Tableau des metadonnées de sections", widget="FileOpen", required=true, def="all.xlsx") // /home/mdecorde/TEMP/ANTRACT/AF/all.xlsx
File tableFile;

@Field @Option(name="buildSujetsMetadata", usage="Build the sujets metadata", widget="Boolean", required=true, def="true")
boolean buildSujetsMetadata;

@Field @Option(name="buildEmissionsMetadata", usage="Build the emissions metadata", widget="Boolean", required=true, def="true")
boolean buildEmissionsMetadata;

if (!ParametersDialog.open(this)) return;

File table2File = null;

System.out.println("opening $tableFile...");
ReadExcel excel = new ReadExcel(tableFile, null);

if (buildEmissionsMetadata) {
	//emissions
	table2File = new File(tableFile.getParentFile(), "emissions.xlsx");
	HashMap<String, String> lineRules = new HashMap<>(); // line tests to select line to keep
	List<String> columnsSelection; // list of columns to keep
	List<String> dateColumnsSelection = []
	HashMap<String, String[]> columnsToCopy = new HashMap<>();
	HashMap<String, String> columnsToRenameRules = new HashMap<>();
	HashMap<String, String[]> searchAndReplaceRules = new HashMap<>();
	
	List<String> normalizeSearchAndReplacePattern = [" ++", " "]
	
	columnsSelection = Arrays.asList(
	"Identifiant de la notice", "Titre propre", "Notes du titre", "Date de diffusion", "Durée", "Nom fichier segmenté (info)", "antract_video",
	"antract_debut","antract_fin","antract_duree","antract_tc_type","antract_tc_date");
	
	lineRules.put("Type de notice", "Notice sommaire");
	
	dateColumnsSelection.add("Date de diffusion");
	datePattern = "dd/MM/yyyy"
	
	//columnsToCopy.put("Notes du titre", ["subtitle"] as String[]);
	columnsToCopy.put("Titre propre", ["title"] as String[]);
	columnsToCopy.put("Date de diffusion", ["text-order"] as String[]);
	columnsToCopy.put("Identifiant de la notice", ["id"] as String[])
	
	searchAndReplaceRules.put("text-order", ["([0-9][0-9])/([0-9][0-9])/([0-9][0-9][0-9][0-9])", '$3$2$1'] as String[]);
	
	process(excel, table2File, lineRules, columnsSelection, columnsToCopy, searchAndReplaceRules, columnsToRenameRules, normalizeSearchAndReplacePattern, dateColumnsSelection, datePattern)
}

if (buildSujetsMetadata) {
	// sujets
	table2File = new File(tableFile.getParentFile(), "sujets.xlsx");
	HashMap<String, String> lineRules = new HashMap<>(); // line tests to select line to keep
	List<String> columnsSelection; // list of columns to keep
	List<String> dateColumnsSelection = []
	HashMap<String, String[]> columnsToCopy = new HashMap<>();
	HashMap<String, String> columnsToRenameRules = new HashMap<>();
	HashMap<String, String[]> searchAndReplaceRules = new HashMap<>();
	
	List<String> normalizeSearchAndReplacePattern = [" ++", " "]
	
	columnsSelection = Arrays.asList(
		"Identifiant de la notice", "Titre propre", "Notes du titre", "Lien notice principale",
		"Date de diffusion", "Type de date", "Durée", "Genre", "Langue VO / VE", "Nature de production", "Producteurs (Aff.)", "Thématique",
		"Nom fichier segmenté (info)", "antract_video", "antract_debut", "antract_fin", "antract_duree", "antract_tc_type", "antract_tc_date",
		"Résumé", "Séquences", "Descripteurs (Aff. Lig.)", "Générique (Aff. Lig.)");
	
	lineRules.put("Type de notice", "Notice sujet");

	columnsToCopy.put("Identifiant de la notice", ["id"] as String[])
	
	dateColumnsSelection.add("Date de diffusion");
	datePattern = "dd/MM/yyyy"
	
	searchAndReplaceRules.put("Descripteurs (Aff. Lig.)", ["(.+[^ ])- (.+)", '$1 - $2'] as String[]);
	searchAndReplaceRules.put("Résumé", ["(.+[^ ])- (.+)", '$1 - $2'] as String[]);
	searchAndReplaceRules.put("Séquences", ["(.+[^ ])- (.+)", '$1 - $2'] as String[]);
	searchAndReplaceRules.put("Titre propre", ["(.+[^ ])- (.+)", '$1 - $2'] as String[]);
	
	process(excel, table2File, lineRules, columnsSelection, columnsToCopy, searchAndReplaceRules, columnsToRenameRules, normalizeSearchAndReplacePattern, dateColumnsSelection, datePattern)
}

def process(ReadExcel excel, File table2File, def lineRules, def columnsSelection, def columnsToCopy, def searchAndReplaceRules, def columnsToRenameRules, def normalizeSearchAndReplacePattern, def dateColumnsSelection, String datePattern) {
	System.out.println("Writing: $table2File");
	
	table2File.delete();
	ReadExcel excel2 = new ReadExcel(table2File, null);
	println " Selecting $columnsSelection with lines matching $lineRules"
	if (!excel.extractTo(excel2, lineRules, columnsSelection)) {
		System.out.println("FAIL");
		return;
	}
	
	System.out.println(" Normalize Search&Replace in columns: " + columnsSelection);
	def normalizePatternRules = new HashMap<>();
	for (String col: columnsSelection) {
		normalizePatternRules.put(col, normalizeSearchAndReplacePattern as String[]);
	}
	excel2.searchAndReplaceInLines(normalizePatternRules);
	
	if (columnsToCopy.size() > 0) {
		System.out.println(" Copying columns: " + columnsToCopy);
		excel2.copyColumns(columnsToCopy);
	}
	
	if (dateColumnsSelection.size() > 0) {
		System.out.println(" Format Date columns: " + dateColumnsSelection);
		excel2.formatDateColumns(dateColumnsSelection, datePattern);
	}
	
	if (searchAndReplaceRules.size() > 0) {
		System.out.println(" Search&Replace in columns: " + searchAndReplaceRules);
		excel2.searchAndReplaceInLines(searchAndReplaceRules);
	}
	
	if (columnsToRenameRules.size() > 0) {
		System.out.println(" Renaming columns: " + columnsToRenameRules);
		excel2.renameColumns(columnsToRenameRules);
	}
	
	System.out.println(" Saving&Closing...");
	excel2.save();
	excel2.close();
	excel.close();
	System.out.println("Done: $table2File");
	
	return table2File.exists()
}
