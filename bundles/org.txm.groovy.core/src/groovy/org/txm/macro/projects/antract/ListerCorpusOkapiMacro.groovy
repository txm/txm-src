// Copyright © 2021 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro.projects.antract

import org.txm.macro.projects.antract.OkapiSaphirAPI
import org.txm.searchengine.cqp.CQPSearchEngine

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.Subcorpus
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.rcp.views.corpora.CorporaView
import org.txm.backtomedia.preferences.BackToMediaPreferences

// BEGINNING OF PARAMETERS

monitor.syncExec(new Runnable() {
	public void run() {
		if (!OkapiSaphirAPI.initializeCredentials(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), BackToMediaPreferences.MEDIA_AUTH_LOGIN, BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "OKAPI - Antract", "S'identifier sur la plateforme Okapi.", "OKAPI - Antract", monitor)) {
			return null;
		}
	}
});

String user = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN)
String password = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD)
if (user == null || user.length() == 0 || password == null || password.length() == 0) {
	println "Annulation."
}

String sessionID = OkapiSaphirAPI.login(user, password)
if (sessionID.length() == 0) {
	monitorShowError("Erreur : La session Okapi n'a pu être démarrée.")
	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN, "")
	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "")
}

String corpora = OkapiSaphirAPI.get_user_corpus(sessionID, user, null, null, null)
if (corpora.length() > 0 ) {
	println "CORPORA"
	println corpora
} else {
	monitorShowError("Erreur : la liste des corpus n'a pas pu être récupérée.")
}


def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Erreur", message)
		}
	});
}
