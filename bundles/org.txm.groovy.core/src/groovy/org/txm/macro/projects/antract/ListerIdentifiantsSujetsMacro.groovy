// Copyright © 2021 ENS-LYON
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro.projects.antract

import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.searchengine.cqp.CQPSearchEngine

if (!(corpusViewSelection instanceof CQPCorpus)) {
	monitorShowError("Erreur: la sélection de la vue Corpus n'est pas un corpus ($corpusViewSelection).")
	return false
} 

CQPCorpus corpus = corpusViewSelection
corpus.compute()
CQPCorpus parentCorpus = corpus.getMainCorpus()

String parentCorpusName = parentCorpus.getCqpId()

def struct_prop = null
def finalValues = []
def ids = null

if (parentCorpusName.startsWith("AF-VOIX-OFF-") || parentCorpusName.startsWith("AF-NOTICES") || parentCorpusName.startsWith("AF-PLAN")) {

	struct_prop = corpus.getStructuralUnit("div").getProperty("id")
	values = struct_prop.getValues(corpus)
	for (def id : values) {
		if (id.startsWith("AFE")) {
			finalValues << id
		}
	}
	
} else if (parentCorpusName.startsWith("AFNOTICES")) {

	struct_prop = corpus.getStructuralUnit("notice").getProperty("identifiantdelanotice")
	def values = struct_prop.getValues(corpus)
	
	def struct_prop2 = corpus.getStructuralUnit("notice").getProperty("typedenotice")
	def qr = corpus.query(new CQLQuery("<notice_typedenotice=\"Notice sujet\">[]"), "TMP", false)
	int[] positions = qr.getStarts()
	int[] strucs = CQPSearchEngine.getCqiClient().cpos2Struc(struct_prop2.getQualifiedName(), positions)
	String sujetsIDS = CQPSearchEngine.getCqiClient().struc2Str(struct_prop.getQualifiedName(), strucs)
	for (String id : values) {
		int idx = sujetsIDS.indexOf(id)
		if (idx > 0) {
			finalValues << id // this id is actually in the sujetsIDS
		}
	}

} else {
	monitorShowError("Erreur: le corpus n'est pas un corpus d'AF-VOIX-OFF, ni AF-NOTICE, ni AF-PLAN, ni AFNOTICE")
	return false
}

ids = finalValues.sort().join("|")

println "Liste des identifiants (${finalValues.size()}) : "+ids

monitor.syncExec(new Runnable() {
	public void run() {
		IOClipboard.write(ids)
	}
})

return ids


def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Erreur", message)
		}
	});
}

