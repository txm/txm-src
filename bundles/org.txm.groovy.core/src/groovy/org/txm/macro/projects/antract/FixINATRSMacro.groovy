package org.txm.macro.projects.antract

import org.txm.Toolbox
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.io.FileCopy
import org.txm.utils.io.IOUtils
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

@Field @Option(name="trsDirectory", usage="Dossier qui contient les fichiers TRS à corriger", widget="Folder", required=true, def="AF")
def trsDirectory

if (!ParametersDialog.open(this)) return;

println "Fixing $trsDirectory"
def files = trsDirectory.listFiles().sort{ it.name }
if (files == null) {
	println "No files found in $trsDirectory"
	return 0;
}

ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
for (File trsFile : files) {
//File trsFile = new File(directory, "AFE86004868.trs")
	cpb.tick()
	if (!trsFile.getName().endsWith(".trs")) {
		continue;
	}
	String content = trsFile.getText("ISO-8859-1")
	content = content.replaceAll("punct=\"([^\"]+)\"\">", "punct=\"\$1\">")
	content = content.replaceAll("<unk>", "???")
	content = content.replaceAll(" Time=\"", " time=\"")
	content = content.replaceAll("<w startTime=\"0.00\" endTime=\"0\" conf=\"\" pos=\"\" punct=\"\" case=\"\" ne=\"\"></w>\n", "")
	content = content.replaceAll("<Turn startTime=\"<o,o,o>\" endTime=\"\" speaker=\"0\">\n<Sync time=\"<o,o,o>\"/>\n</Turn>\n", "")
	content = content.replaceAll("<Section type=\"report\" startTime=\"<o,o,o>\" endTime=\"\">", "<Section type=\"report\" startTime=\"0.0\" endTime=\"\">")
	trsFile.setText(content, "ISO-8859-1")
}
cpb.done()

// Add missing the transcriber DTD file
File dtdFile = new File(trsDirectory, "trans-14.dtd")
if (!dtdFile.exists()) {
	File txmDTDFile = new File(Toolbox.getTxmHomePath(), "schema/trans-14.dtd")
	FileCopy.copy(txmDTDFile, dtdFile)	
}
if (IOUtils.replace(dtdFile, "	elapsed_time	CDATA		\"0\"", "	elapsed_time	CDATA		#IMPLIED"))

println "Done"