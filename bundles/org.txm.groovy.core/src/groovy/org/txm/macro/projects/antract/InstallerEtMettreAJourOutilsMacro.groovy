import org.txm.Toolbox
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.io.IOUtils
import org.txm.rcp.views.fileexplorer.MacroExplorer

def groovyUserDirectory = new File(Toolbox.getTxmHomePath() + "/scripts/groovy/user/org/txm/macro/projects/antract");

if (!groovyUserDirectory.exists()) {
	groovyUserDirectory.mkdirs();
}

if (!groovyUserDirectory.exists()) {
	monitorShowError("Le dossier $groovyUserDirectory n'a pas pu être créé. Abandon.")
	return;
}

String svn = "http://forge.cbp.ens-lyon.fr/svn/txm/TXM/trunk/org.txm.groovy.core/src/groovy/org/txm/macro/projects/antract/"
try {
	macros = IOUtils.getText(new URL(svn+"manifest"), "UTF-8")
	macros = macros.split("\n")
} catch(Exception e2) {
	monitorShowError("La liste des outils ${svn+"tools.txt"} n'a pas pu être récupérée (raison : $e2).\nAbandon.")
	return
}

println "Copyping ${macros.size()} files: "
for (def m : macros) {
	def f = new File(groovyUserDirectory.getAbsolutePath()+"/"+m)
	try {
		String remoteUrl = svn+m
		
		if (f.exists()) print "."
		else print "*"
		
		def file = new FileOutputStream(f)
		def out = new BufferedOutputStream(file)
		out << new URL(remoteUrl).openStream()
		out.close()
	} catch(Exception e ) {
		println "\nErreur lors de la récupération de $f: $e"
	}
}

monitor.syncExec(new Runnable() {
	public void run() {
		MacroExplorer.refresh();
		
		println "\nAntract tools have been updated."
	}
});


def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Erreur", message)
		}
	});
}
