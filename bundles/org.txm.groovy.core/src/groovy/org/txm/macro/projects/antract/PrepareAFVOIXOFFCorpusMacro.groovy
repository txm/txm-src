package org.txm.macro.projects.antract

import org.txm.utils.io.IOUtils
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

import org.txm.macro.transcription.*
import org.txm.utils.io.FileCopy
import org.txm.macro.projects.antract.BuildAFMetadataMacro
import org.txm.macro.projects.antract.BuildAFMetadataMacro

@Field @Option(name="tableFile", usage="Tableau des metadonnées de sections", widget="FileOpen", required=true, def="all.xlsx") // /home/mdecorde/TEMP/ANTRACT/AF/all.xlsx
File tableFile;

@Field @Option(name="trsDirectory", usage="Dossier qui contient les fichiers TRS à corriger", widget="Folder", required=true, def="AF")
def trsDirectory

@Field @Option(name="debug", usage="Show debug level messages", widget="Boolean", required=true, def="false")
def debug

if (!ParametersDialog.open(this)) return;

File workingDirectory = tableFile.getParentFile()

// extract infos for sujets and emissions from the main table file
gse.runMacro(BuildAFMetadataMacro, ["tableFile":tableFile, 
				"buildSujetsMetadata": true,
				"buildEmissionsMetadata": true])

File emissionsFile = new File(workingDirectory, "emissions.xlsx")
File sujetsFile = new File(workingDirectory, "sujets.xlsx")

// fix TRS files in the trsDirectory directory
gse.runMacro(FixINATRSMacro, ["trsDirectory":trsDirectory])

// insert the section in the TRS files
gse.runMacro(AddSectionsFromTableMacro, ["metadataFile": sujetsFile
	, "trsDirectory": trsDirectory
	, "joinTRSColumn": "Lien notice principale"
	, "startTimeColumn": "antract_debut"
	, "endTimeColumn": "antract_fin"
	, "typeColumns": "Titre propre"
	, "topicColumns": "Date de diffusion"
	, "metadataColumns": "id;Titre propre;Date de diffusion;Identifiant de la notice;Notes du titre;Type de date;Durée;Genre;Langue VO / VE;Nature de production;Producteurs (Aff.);Thématique;Nom fichier segmenté (info);antract_video;antract_debut;antract_fin;antract_duree;antract_tc_type;antract_tc_date;Résumé;Séquences;Descripteurs (Aff. Lig.);Générique (Aff. Lig.)"
	, "metadataColumnsGroups": "secondary;metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;secondary;secondary;secondary;secondary;secondary;secondary;secondary;text;text;text;text"
	, "fixSectionsLimits":true
	, "sectionsMergeActivationThreashold":4.0
	, "fixTurnsLimits":true
	, "turnsCutActivationThreashold":1.0
	, "debug":debug])

//copy the emissions file in the source directory
FileCopy.copy(emissionsFile, new File(new File(trsDirectory, "out"), "metadata.xlsx"))

File paramFile = new File(new File(trsDirectory, "out"), "parameters/CorpusCommandPreferences.prefs")
paramFile.getParentFile().mkdirs()

// create the corpus parameters file
String content = """backtomedia/backtomedia_endproperty=end
backtomedia/backtomedia_startproperty=start
backtomedia/backtomedia_structure=sp
backtomedia/backtomedia_time_property=time
backtomedia/media_auth=true
backtomedia/media_auth_login=
backtomedia/media_extension=mp4
backtomedia/media_path_prefix=https\\://{0}\\:{1}@okapi.ina.fr/antract/Media/AF/
backtomedia/name=backtomedia
backtomedia/sync_mode=Structure
concordance/context_limits=text
concordance/context_limits_type=list
concordance/view_reference_pattern={"format"\\:"%s, %s, %s","properties"\\:["text_date-de-diffusion-tri","u_time","div_identifiant-de-la-notice"]}
concordance/sort_reference_pattern={"format"\\:"","properties"\\:["text_date-de-diffusion-tri","u_time","div_identifiant-de-la-notice"]}
concordance/name=concordance
eclipse.preferences.version=1"""

IOUtils.write(paramFile, content);

//done \o/
println "Done: finalize the corpus using the "+new File(trsDirectory, "out")+" source directory and the XML-TRS import module."

