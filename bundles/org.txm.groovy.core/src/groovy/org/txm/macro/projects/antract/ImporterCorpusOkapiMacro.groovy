// Copyright © 2022 ENS-LYON
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde, sheiden

// STANDARD DECLARATIONS
package org.txm.macro.projects.antract

import org.txm.macro.projects.antract.OkapiSaphirAPI
import org.txm.searchengine.cqp.CQPSearchEngine

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.Subcorpus
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.rcp.views.corpora.CorporaView
import org.txm.backtomedia.preferences.BackToMediaPreferences

// BEGINNING OF PARAMETERS

@Field @Option(name="liste_identifiants_sujets", usage="Liste des identifiants de notice séparés par des '|'", widget="Text", required=true, def="")
def liste_identifiants_sujets
@Field @Option(name="nom_sous_corpus", usage="Nom du sous-corpus à créer", widget="String", required=true, def="")
def nom_sous_corpus
//@Field @Option(name="identifiant_corpus_okapi", usage="Identifiant du corpus Okapi à importer", widget="String", required=true, def="")
//def identifiant_corpus_okapi

maxCQLLimit = 15500

if (!(corpusViewSelection instanceof CQPCorpus)) {
	monitorShowError("Erreur: la sélection n'est pas un corpus.")
	return false
} 

CQPCorpus corpus = corpusViewSelection
corpus.compute()
CQPCorpus parentCorpus = corpus.getMainCorpus()

String parentCorpusName = parentCorpus.getCqpId()

if (parentCorpusName.startsWith("AF-VOIX-OFF-") || parentCorpusName.startsWith("AFNOTICES") || parentCorpusName.startsWith("AF-NOTICES") || parentCorpusName.startsWith("AF-PLAN")) {
	
} else {
	monitorShowError("Erreur: le corpus n'est pas un corpus d'AF-VOIX-OFF, ni AF-NOTICE, ni AF-PLAN, ni AFNOTICE")
	return false
}

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;


//monitor.syncExec(new Runnable() {
//	public void run() {
//		if (!OkapiSaphirAPI.initializeCredentials(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), BackToMediaPreferences.MEDIA_AUTH_LOGIN, BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "OKAPI - Antract", "S'identifier sur la plateforme Okapi.", "OKAPI - Antract", monitor)) {
//			return null;
//		}
//	}
//});
//
//String user = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN)
//String password = System.getProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD)
//if (user == null || user.length() == 0 || password == null || password.length() == 0) {
//	println "Annulation."
//}
//
//String sessionID = OkapiSaphirAPI.login(user, password)
//if (sessionID.length() == 0) {
//	println "Erreur : La session Okapi n'a pu être démarrée."
//	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_LOGIN, "")
//	System.setProperty(BackToMediaPreferences.MEDIA_AUTH_PASSWORD, "")
//}
//
//String corpora = OkapiSaphirAPI.get_user_corpus(sessionID, user, null, null, null)
//if (corpora.length() > 0 ) {
//	println "CORPORA"
//	println corpora
//} else {
//	println "Erreur : la liste des corpus n'a pas pu être récupérée."
//}

liste_identifiants_sujets = liste_identifiants_sujets.replace("\n", "|").replace("\t", "|").replace(" ", "|").replaceAll("(\\|)++", "|").trim()

liste_identifiants_sujets = (liste_identifiants_sujets.split("\\|") as List)
liste_identifiants_sujets.remove("")
liste_identifiants_sujets.unique()
liste_identifiants_sujets.sort()

nb_identifiers = liste_identifiants_sujets.size()
println "Création du sous-corpus $nom_sous_corpus avec une liste de $nb_identifiers identifiants : $liste_identifiants_sujets"

String prop = "div_id"
if (parentCorpusName.startsWith("AFNOTICES")) {
	prop = "notice_identifiantdelanotice"
}

regex_identifiants_sujets = liste_identifiants_sujets.join('|')

query = "[_.$prop=\"$regex_identifiants_sujets\"] expand to $prop"

// println "query length = "+query.length()

if (query.length() > maxCQLLimit) {
	print "Reducing too long query: ${query.length()} characters..."
	original_regex_identifiants_sujets = regex_identifiants_sujets
	regex_identifiants_sujets = getShortCQL(liste_identifiants_sujets)
	query = "[_.$prop=\"$regex_identifiants_sujets\"] expand to $prop"
	println " to ${query.length()} characters."
	println "("+original_regex_identifiants_sujets.size()+")" + original_regex_identifiants_sujets
	println "-->"
	println "("+regex_identifiants_sujets.size()+")" + regex_identifiants_sujets
	if (query.length() > maxCQLLimit) {
		monitorShowError("Erreur : impossible de créer le sous-corpus, la requête CQL est trop longue (${query.length()}).")
		return
	}
	
}

for (String id : liste_identifiants_sujets) {
	if (!id.matches(regex_identifiants_sujets)) {
		println "ERROR: $id not matching with regex"
	} else {
		//println "   OK: $id matching with regex"
	}
}

println "CQL = $query"

Subcorpus sub = parentCorpus.createSubcorpus(new CQLQuery(query), nom_sous_corpus)

nb_matches = sub.getMatches().size()
// println nom_sous_corpus+" est composé de "+nb_matches+" éléments."

int s = sub.getSize()

	println "$nom_sous_corpus subcorpus is created."
	monitor.syncExec(new Runnable() {
					public void run() {
						CorporaView.refresh();
						CorporaView.expand(parentCorpus);
					}
				});

if (nb_matches != nb_identifiers) {
	println "Attention il y a $nb_matches div dans le sous-corpus alors qu'il y a $nb_identifiers identifiants de sujets dans la liste."
	monitorShowWarning("Attention il y a $nb_matches div dans le sous-corpus alors qu'il y a $nb_identifiers identifiants de sujets dans la liste.")
}

// UTILS UTILS UTILS UTILS UTILS UTILS UTILS

def getShortCQL(def list) {
	def origtree = ["":list]
	breakNode(origtree, "", 0)
	def CQL1 = list.join("|")
	def CQL2 = writeNode(origtree)
	//CQL2 = CQL2.replaceAll("\\(\\[()\\]\\)", "")
	
	
	if (CQL1.length() < CQL2.length()) {
		return CQL1;
	} else {
		CQL2 = CQL2.replace("(\\d)", "\\d")
		CQL2 = CQL2.replaceAll("\\(\\[([0-9])-([0-9])\\]\\)", "[\$1-\$2]") // ([0-9]) -> [0-9]
		CQL2 = CQL2.replaceAll("\\(([0-9])\\|([0-9])\\)", "[\$1\$2]") // (1|2) -> [12]
		return CQL2;
	}
}

def breakNode(def tree, def key, def p) {
	def space = (" "*p)
	//println space + "breaking: $key of $tree at $p"
		
	def children = new LinkedHashMap()
	def ids = tree[key]
	tree[key] = children
	
	for (def child : ids) {
		//println space+"child="+child
		if (child.length() <= p) continue;
		
		def letter = child[p]
		if (!children.containsKey(letter)) {
			children[letter] = []
		}
		
		children[letter].add(child)
	}
	
	for (def k : children.keySet()) {
		breakNode(children, k , p+1)
	}
}


def writeNode(def tree) {
	def s = ""
	//println "tree: "+tree

	int n = 0;
	if (tree.size() > 1) s += "("
	String ss = ""
	for (def child : tree.keySet()) {
		if (n > 0) ss += "|"
		ss += child
		ss += writeNode(tree[child])
		n++
	}
	
	if (ss.matches("([0-9]\\|){2,10}[0-9]")) ss = "["+ss.replace("|", "") +"]"
	if (ss == "[0123456789]") ss = "\\d"
	if (ss == "[012345678]") ss = "[0-8]"
	if (ss == "[01234567]") ss = "[0-7]"
	if (ss == "[0123456]") ss = "[0-6]"
	if (ss == "[012345]") ss = "[0-5]"
	if (ss == "[123456789]") ss = "[1-9]"
	if (ss == "[12345678]") ss = "[1-8]"
	if (ss == "[1234567]") ss = "[1-7]"
	if (ss == "[123456]") ss = "[1-6]"
	if (ss == "[12345]") ss = "[1-5]"
	if (ss == "[23456789]") ss = "[2-9]"
	if (ss == "[2345678]") ss = "[2-8]"
	if (ss == "[3456789]") ss = "[3-9]"
	if (ss == "[345678]") ss = "[3-8]"
	if (ss == "[456789]") ss = "[4-9]"
	if (ss == "[45678]") ss = "[4-8]"
	
	if (ss == "[012345689]") ss = "[0-689]"
	if (ss == "[012356789]") ss = "[0-35-9]";
	if (ss == "[012456789]") ss = "[0-24-9]";
	s += ss
	
	if (tree.size() > 1) s += ")"
	return s
}

def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Erreur", message)
		}
	});
}

def monitorShowWarning(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openWarning(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Attention", message)
		}
	});
}