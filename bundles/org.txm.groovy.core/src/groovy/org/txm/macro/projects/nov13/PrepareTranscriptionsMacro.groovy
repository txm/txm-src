package org.txm.macro.projects.nov13

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.txm.utils.*
import org.txm.utils.logger.*
import org.txm.macro.transcription.RecodeSpeakers

@Field @Option(name="vocapiaDirectory", usage="A Vocapia XML files directory to process", widget="Folder", required=false, def="")
		File vocapiaDirectory

@Field @Option(name="resultDirectory", usage="The result directory", widget="Folder", required=false, def="")
		File resultDirectory

@Field @Option(name="primarySpeakerIdRegex", usage="speaker ID of the primary speaker", widget="String", required=false, def="")
		String primarySpeakerIdRegex

@Field @Option(name="otherNonPrimarySpeakerId", usage="other non primary id of the other turns", widget="String", required=false, def="")
		String otherNonPrimarySpeakerId

@Field @Option(name="nonPrimarySpeakerIdRegex", usage="other non primary id of the other turns", widget="String", required=false, def="")
		String nonPrimarySpeakerIdRegex

//@Field @Option(name="supplementarySpeakerIdRegex", usage="supplementary locutor ids regex", widget="String", required=false, def="UX.+")
//		String supplementarySpeakerIdRegex
//
//@Field @Option(name="supplementarySpeakerId", usage="supplementary locutor id to normalize", widget="String", required=false, def="")
//		String supplementarySpeakerId

@Field @Option(name="spearkerReplacementTableFile", usage="2 columns (from, to) table file", widget="File", required=false, def="replacements.tsv")
		File spearkerReplacementTableFile

@Field @Option(name="newSectionMarker", usage="section marker", widget="String", required=false, def="*#")
		String newSectionMarker

@Field @Option(name="debug", usage="speaker ID of the primary speaker", widget="Boolean", required=false, def="false")
		Boolean debug

@Field @Option(name="cleanWorkingDirectories", usage="Clean working directories is selected", widget="Boolean", required=false, def="true")
		Boolean cleanWorkingDirectories

if (!ParametersDialog.open(this)) return;

if (resultDirectory.equals(vocapiaDirectory)) {
	println "Result directory must differs from vocapiaDirectory: "+vocapiaDirectory
	return false;
}

resultDirectory.mkdirs();

println "CONVERTING VOCAPIA FILES TO TRS FILES..."
File trsDirectory = new File(resultDirectory, "vocapia2trs")
trsDirectory.mkdir()
gse.runMacro(org.txm.macro.transcription.Vocapia2TranscriberMacro, ["vocapiaFile":new File("this file does not exists"), "vocapiaDirectory":vocapiaDirectory, "resultDirectory":trsDirectory, "additionalSpeakers": "UX1:UX1", "debug":debug])

println "SPOTTING SECTION MARKS..."

trsFiles = trsDirectory.listFiles().findAll(){it.getName().toLowerCase().endsWith(".trs")}
if (trsFiles.size() == 0) {
	println "No XML file found in $trsDirectory"
	return false
}

File sectionsDirectory = new File(resultDirectory, "sections")
sectionsDirectory.mkdir()
gse.runMacro(org.txm.macro.transcription.SegmentTRSInSectionFromMarkerMacro, ["trsDirectory":trsDirectory, "resultDirectory":sectionsDirectory, "newSectionMarker":newSectionMarker, "debug":debug])

if (cleanWorkingDirectories) {
	trsDirectory.deleteDir()
}

println "SPOTTING 'OTHER' TURNS..."

trsFiles = sectionsDirectory.listFiles().findAll(){it.getName().toLowerCase().endsWith(".trs")}
if (trsFiles.size() == 0) {
	println "No XML file found in $sectionsDirectory"
	return false
}

File otherDirectory = new File(resultDirectory, "otherturns")
otherDirectory.mkdir()
cpb = new ConsoleProgressBar(trsFiles.size())
for (File file : trsFiles) {
	
	if (debug) println "== $file =="
	//else cpb.tick()
	
	CreateTheOtherTurns fixer = new CreateTheOtherTurns(file, primarySpeakerIdRegex, otherNonPrimarySpeakerId, debug)
	String name = FileUtils.stripExtension(file)
	File outFile = new File(otherDirectory, name+".trs")
	
	if (!fixer.process(outFile)) {
		println "WARNING: ERROR WHILE PROCESSING: "+file
		if (debug) {println "DEBUG ACTIVATED -> STOP"; return;}
	}
	
	if (fixer.getWarnings().size() > 0) {
		println "Warnings while processing $file: \n\t"+fixer.getWarnings().join("\n\t")
	}
}
cpb.done()
if (cleanWorkingDirectories) {
	sectionsDirectory.deleteDir()
}

println "CONVERTING WORD MARKERS TO EVENTS..."

trsFiles = otherDirectory.listFiles().findAll(){it.getName().toLowerCase().endsWith(".trs")}
if (trsFiles.size() == 0) {
	println "No XML file found in $trsDirectory"
	return false
}

File eventsDirectory = new File(resultDirectory, "events")
eventsDirectory.mkdir()
gse.runMacro(org.txm.macro.transcription.AddEventsFromWordMarkersMacro, ["trsDirectory":otherDirectory, "resultDirectory":eventsDirectory, "wordElement": "w", "newEventMarker":"XXX", "eventDescription":"termes incompréhensibles ou inaudibles", "eventType":"pi", "eventExtent": "instantaneous", "debug":debug])

if (cleanWorkingDirectories) {
	otherDirectory.deleteDir()
}

println "NORMALIZING SPEAKER IDs..."

trsFiles = eventsDirectory.listFiles().findAll(){it.getName().toLowerCase().endsWith(".trs")}
if (trsFiles.size() == 0) {
	println "No XML file found in $otherDirectory"
	return false
}

def replacements = []
if (spearkerReplacementTableFile.exists()) {
	for (String line : spearkerReplacementTableFile.readLines("UTF-8")) {
		def split = line.split("\t", 2)
		if (split.length == 2) {
			replacements << [split[0].replaceAll("<SPACE>", " ").replaceAll("<BOM>", "\uFEFF"), split[1]]
		}
	}
}

cpb = new ConsoleProgressBar(trsFiles.size())
for (File file : trsFiles) {
	if (debug) println "== $file =="
	else cpb.tick()
	
	File tmpFile = new File(resultDirectory, "tmp.xml")
	tmpFile.delete();
	
	RecodeSpeakers fixer = new RecodeSpeakers(file, tmpFile, nonPrimarySpeakerIdRegex, null, otherNonPrimarySpeakerId, otherNonPrimarySpeakerId)
	fixer.debug = debug
	
	if (!fixer.process()) {
		println "WARNING: ERROR WHILE PROCESSING: "+file
		if (debug) {println "DEBUG ACTIVATED -> STOP"; return;}
		continue;
	}
	
	File tmpFile2 = new File(resultDirectory, "tmp2.xml")
	def spks = new groovy.xml.XmlSlurper().parse(tmpFile).Speakers.Speaker.collect(){it.attributes()["id"]}
	
	for (def replacement : replacements) {
	
		if (!spks.contains(replacement[0])) continue;
		
		RecodeSpeakers fixer2 = new RecodeSpeakers(tmpFile, tmpFile2, replacement[0], null, replacement[1], replacement[1])
		fixer2.debug = debug
		
		if (!fixer2.process()) {
			println "WARNING: ERROR WHILE PROCESSING: "+file
			if (debug) {println "DEBUG ACTIVATED -> STOP"; return;}
			continue
		}
		tmpFile.delete(); // This tmp file must be removed for the next operation
		tmpFile2.renameTo(tmpFile)
	}
	
	File outFile2 = new File(resultDirectory, file.getName())
	tmpFile.renameTo(outFile2)
	
}
cpb.done()

if (cleanWorkingDirectories) {
	eventsDirectory.deleteDir()
}

println "Done: "+trsFiles.size()+" files processed. Result files in $resultDirectory"
