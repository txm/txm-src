package org.txm.macro.pdf
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.apache.pdfbox.Loader
import org.apache.pdfbox.rendering.PDFRenderer
import org.apache.pdfbox.tools.imageio.ImageIOUtil
import org.apache.pdfbox.rendering.ImageType

// BEGINNING OF PARAMETERS

@Field @Option(name="input_file", usage=".pdf input file", widget="File", required=false, def="")
def input_file

@Field @Option(name="input_dir", usage="The directory containing the .pdf files to read", widget="Folder", required=false, def="")
def input_dir


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (input_dir != null &&  input_dir.exists()) {
	nFiles = 0
	input_dir.eachFileMatch(~/.*.pdf/) { f ->
		if (f.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 		processFile(f)
	 		nFiles++
		}
	}
	println "Processed "+nFiles+" files."
} else {
	if (input_file.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 processFile(input_file)
	}
}

def processFile(input_file) {

	name = input_file.getName()
	dir = input_file.getParentFile()
	idx = name.lastIndexOf(".")
	if (idx > 0) name = name.substring(0, idx)

	pdfFile = input_file.getAbsolutePath()
	if (pdfFile.toUpperCase().endsWith(".PDF")) {
	
		println "Processing "+name+"..."
		
		textFile = pdfFile.substring(0, pdfFile.length() - 3) + "txt"
		try {
			doc = Loader.loadPDF(input_file)
			pdfRenderer = new PDFRenderer(doc)
			for (page = 0; page < doc.getNumberOfPages(); ++page) {
    				bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB)
    				fos = new FileOutputStream(new File(dir, name + "-" + (page+1) + ".png"))
				ImageIOUtil.writeImage(bim, "png", fos, 300)
			}
			fos.close()
			doc.close()
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
}


