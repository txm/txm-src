package org.txm.macro.pdf
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.apache.pdfbox.Loader
import org.apache.pdfbox.text.PDFTextStripper

// BEGINNING OF PARAMETERS

@Field @Option(name="input_file", usage=".pdf input file", widget="File", required=false, def="")
def input_file

@Field @Option(name="input_dir", usage="The directory containing the .pdf files to read", widget="Folder", required=false, def="")
def input_dir


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (input_dir != null &&  input_dir.exists()) {
	nFiles = 0
	println "File\tAuthor\tTitle\tSubject\tCreator\tProducer\tCreation date\tModification date\tKeywords\tPages"
	input_dir.eachFileMatch(~/.*.pdf/) { f ->
		pdfFile = f.getAbsolutePath()
		if (pdfFile.toUpperCase().endsWith(".PDF")) {
			try {
				doc = Loader.loadPDF(f)
				pdd = doc.getDocumentInformation()
      				print "\t\""+ ((pdd.getAuthor()==null)?"":pdd.getAuthor())+"\""
      				print "\t\""+ ((pdd.getTitle()==null)?"":pdd.getTitle())+"\""
      				print "\t\""+ ((pdd.getSubject()==null)?"":pdd.getSubject())+"\""
      				print "\t\""+ ((pdd.getCreator()==null)?"":pdd.getCreator())+"\""
      				print "\t\""+ ((pdd.getProducer()==null)?"":pdd.getProducer())+"\""
      				print "\t"+ ((pdd.getCreationDate()==null)?"":pdd.getCreationDate().toZonedDateTime().toInstant())
      				print "\t"+ ((pdd.getModificationDate()==null)?"":pdd.getModificationDate().toZonedDateTime().toInstant())
      				print "\t\""+ ((pdd.getKeywords()==null)?"":pdd.getKeywords())+"\""
      				println "\t"+ doc.getNumberOfPages()
      				
      				doc.close()
      				
				nFiles++

			} catch (Exception e) {
			// e.printStackTrace()
			}
		}
	}
	println "Processed "+nFiles+" files."
} else {
	pdfFile = input_file.getAbsolutePath()
	if (pdfFile.toUpperCase().endsWith(".PDF")) {
		try {
			doc = Loader.loadPDF(input_file)
			pdd = doc.getDocumentInformation()

      			println "Author: "+((pdd.getAuthor()==null)?"":pdd.getAuthor())
      			println "Title: "+ ((pdd.getTitle()==null)?"":pdd.getTitle())
      			println "Subject: "+ ((pdd.getSubject()==null)?"":pdd.getSubject())
      			println "Creator: "+ ((pdd.getCreator()==null)?"":pdd.getCreator())
      			println "Producer: "+ ((pdd.getProducer()==null)?"":pdd.getProducer())
      			println "Creation date: "+ ((pdd.getCreationDate()==null)?"":pdd.getCreationDate().toZonedDateTime().toInstant())
      			println "Modification date: "+ ((pdd.getModificationDate()==null)?"":pdd.getModificationDate().toZonedDateTime().toInstant())
      			println "Keywords: "+ ((pdd.getKeywords()==null)?"":pdd.getKeywords())
      			println "Pages: "+ doc.getNumberOfPages()
			
			doc.close()
		} catch (Exception e) {
			// e.printStackTrace()
		}
	}
}

