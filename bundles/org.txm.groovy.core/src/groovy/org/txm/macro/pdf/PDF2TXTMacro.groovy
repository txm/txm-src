package org.txm.macro.pdf
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.apache.pdfbox.Loader
import org.apache.pdfbox.text.PDFTextStripper

// BEGINNING OF PARAMETERS

@Field @Option(name="input_file", usage=".pdf input file", widget="File", required=false, def="")
def input_file

@Field @Option(name="input_dir", usage="The directory containing the .pdf files to read", widget="Folder", required=false, def="")
def input_dir

@Field @Option(name="set_sort_by_position", usage="""The order of the text tokens in a PDF file may not be in the same as they appear visually on the screen. For example, a PDF writer may write out all text by font, so all bold or larger text, then make a second pass and write out the normal text.
The default is to not sort by position.

A PDF writer could choose to write each character in a different order. By default PDFBox does not sort the text tokens before processing them due to performance reasons.""", widget="Boolean", required=true, def="false")
def set_sort_by_position

@Field @Option(name="page_break_symbol", usage="page break symbol at beginning of page (e.g. %%PB%% or <pb/>)", widget="String", required=true, def="%%PB%%")
def page_break_symbol


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (input_dir != null &&  input_dir.exists()) {
	nFiles = 0
	input_dir.eachFileMatch(~/.*.pdf/) { f ->
		if (f.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 		processFile(f)
	 		nFiles++
		}
	}
	println "Processed "+nFiles+" files."
} else {
	if (input_file.getAbsolutePath().toUpperCase().endsWith(".PDF")) {
	 processFile(input_file)
	}
}

def processFile(input_file) {

	name = input_file.getName()
	idx = name.lastIndexOf(".")
	if (idx > 0) name = name.substring(0, idx)
	outputFile = new File(input_file.getParentFile(), name + ".txt")

	pdfFile = input_file.getAbsolutePath()
	if (pdfFile.toUpperCase().endsWith(".PDF")) {
	
		println "Processing "+name+"..."
		
		textFile = pdfFile.substring(0, pdfFile.length() - 3) + "txt"
		try {
			outputFile.withWriter("UTF-8") { writer ->
			doc = Loader.loadPDF(input_file)
			strip = new PDFTextStripper()
			
/*
			println "SpacingTolerance = "+strip.getSpacingTolerance()
			// Set the space width-based tolerance value that is used to estimate where spaces in text should be added. Note that the default value for this has been determined from trial and error. Setting this value larger will reduce the number of spaces added.
			println "AverageCharTolerance = "+strip.getAverageCharTolerance()
			// Set the character width-based tolerance value that is used to estimate where spaces in text should be added. Note that the default value for this has been determined from trial and error. Setting this value larger will reduce the number of spaces added.
			println "IndentThreshold = "+strip.getIndentThreshold()
			// sets the multiple of whitespace character widths for the current text which the current line start can be indented from the previous line start beyond which the current line start is considered to be a paragraph start. The default value is 2.0.
			println "DropThreshold = "+strip.getDropThreshold()
			// sets the minimum whitespace, as a multiple of the max height of the current characters beyond which the current line start is considered to be a paragraph start. The default value is 2.5.
*/

/*
			println "SeparateByBeads = "+strip.getSeparateByBeads()
			// this will tell if the text stripper should separate by beads
			println "ArticleStart = "+strip.getArticleStart()
			// the string which will be used at the beginning of an article
			println "ArticleEnd = "+strip.getArticleEnd()
			// the string which will be used at the end of an article
			println "AddMoreFormatting = "+strip.getAddMoreFormatting()
			// this will tell if the text stripper should add some more text formatting
			println "LineSeparator = "+strip.getLineSeparator()
			// the desired line separator for output text. The line.separator system property is used if the line separator preference is not set explicitly
			println "WordSeparator = "+strip.getWordSeparator()
			// set the desired word separator for output text. The PDFBox text extraction algorithm will output a space character if there is enough space between two words. By default a space character is used. If you need and accurate count of characters that are found in a PDF document then you might want to set the word separator to the empty string
*/

			strip.setSortByPosition(set_sort_by_position)
			strip.setPageStart(page_break_symbol)
			// strip.setParagraphStart("\n<p>")
			// strip.setParagraphEnd("</p>")
			writer.print strip.getText(doc)
			doc.close()
			writer.close()
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}
}

