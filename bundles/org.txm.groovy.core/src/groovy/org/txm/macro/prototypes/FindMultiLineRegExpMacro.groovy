package org.txm.macroproto
// Copyright © 2015 - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// STANDARD DECLARATIONS

import org.kohsuke.args4j.*

import groovy.transform.Field

import java.nio.charset.Charset

import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.*

import javax.xml.stream.*

import java.net.URL
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// PARAMETERS

@Field @Option(name="inputDirectory", usage="TXT directory", widget="Folder", required=false, def="")
File inputDirectory;
@Field @Option(name="regexp", usage="Regular expression to match", widget="String", required=false, def="")
String regexp;
@Field @Option(name="encoding", usage="File encoding", widget="String", required=false, def="UTF-8")
String encoding;

// PARAMETERS

if (inputDirectory == null) {
	if (!ParametersDialog.open(this)) return;
}

// SANITY CHECK

if (inputDirectory==null || regexp==null) { println "** FindMultiLineRegExpMacro: the input directory and the regular expression must be specified."; return}

if (!inputDirectory.exists()) { println "** FindMultiLineRegExpMacro: impossible to access the '$inputDirectory' input directory."; return}

// MAIN BODY

def p = /$regexp/

println "-- looking for: $regexp"

for (File inputfile : inputDirectory.listFiles().sort{ it.name }) {

	if (inputfile.isDirectory()) continue // ignore

	def matcher = inputfile.getText() =~ p
	
	println "\n-- file:"+inputfile.getName() + " "+matcher.size() + " match" +((matcher.size() > 1)?"s":"")

	if (matcher.size() > 0) {
		matcher.each { println it }
	}
}
