// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*

// BEGINNING OF PARAMETERS


@Field @Option(name="min", usage="F min", widget="Float", required=false, def="18")
def min

@Field @Option(name="relative", usage="min is a % of the total frequency", widget="Boolean", required=false, def="false")
def relative

@Field @Option(name="doIt", usage="delete the selected parts", widget="Boolean", required=false, def="false")
def doIt

@Field @Option(name="debug", usage="show debug messages", widget="Boolean", required=false, def="false")
def debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

if (!(corpusViewSelection instanceof Partition)) {
	println "Aborting, the selection is not a Partition: $corpusViewSelection"
	return
}

def partition = corpusViewSelection
partition.compute(false) // ensure parts are ready to use

int F = 0;
for (def part : partition.getParts()) {
	F += part.getSize();
}
if (debug) println "F=$F"
if (relative) {
	min = (F*min)/100
}
if (debug)  println "F min threshold=$min"

def toDelete = []
for (def part : partition.getParts()) {
	if (part.getSize() < min) {
		toDelete << part
	}
}
if (toDelete.size() > 0) {
	println (doIt?"Deleting ${toDelete.size()} parts: $toDelete":"Selected parts $toDelete")
	for (def part : toDelete) {
		if (doIt) part.delete()
	}
}
println "Done"