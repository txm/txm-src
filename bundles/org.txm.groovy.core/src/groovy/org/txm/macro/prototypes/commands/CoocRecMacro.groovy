// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.cooccurrence.core.functions.*

// BEGINNING OF PARAMETERS

println "corpora selection: "+corpusViewSelection

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Selection is not a corpus: $corpusViewSelection"
	return false;
}

@Field @Option(name="output_file", usage="Output graph file", widget="File", required=true, def='output.graphml')
		def output_file
@Field @Option(name="starting_word", usage="The starting word", widget="String", required=true, def='Life')
		def starting_word
@Field @Option(name="property", usage="an example query", widget="String", required=true, def='lemma')
		def property
@Field @Option(name="mode", usage="an example query", widget="StringArray", metaVar="Poly	Reccursive", required=true, def='Poly')
		def mode
@Field @Option(name="distance_max", usage="an example query", widget="Integer", required=true, def='9')
		def distance_max
@Field @Option(name="score_minimum", usage="an example query", widget="Float", required=true, def='2.5')
		def score_minimum
@Field @Option(name="profondeur_max", usage="an example query", widget="Integer", required=true, def='3')
		def profondeur_max
@Field @Option(name="debug", usage="Show internal variable content", widget="StringArray", metaVar="OFF	ON	ALL	REALLY ALL", required=true, def="OFF")
		debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;
if (debug == "OFF") debug = 0; else if (debug == "ON") debug = 1; else if (debug == "ALL") debug = 2 else if (debug == "REALLY ALL") debug = 3
mode = mode == "Poly"

output = output_file.newWriter()

lmax = ""
for (int i = 0 ; i < profondeur_max; i++) lmax += "\t"

def corpus = corpusViewSelection
wordProperty = corpus.getProperty(property)
if (wordProperty == null) {
	println "CQP property not found: $property"
	return false
}

def rightCooc = new Cooccurrence(corpus)
r = new HashSet()
def printCoocs(def pivots, def level, def cooc, def right_dir) {
	if (level.length() > lmax.length()) return;
	if (debug > 1) println "#"+level+pivots

	def bouts = []

	for (def s : pivots) {
		bouts << "[$property=\"$s\"%cd]"
	}
	String qs = bouts.join(" []{0, ${distance_max}} ")

	def q = new CQLQuery(qs)
	if (debug > 0) println level+q

	r.addAll(pivots)

	if (mode) {
		if (right_dir)
			cooc.setParameters(q, [wordProperty], null, 0, 0, 1, distance_max+1, 1, score_minimum, 1, false, false);
		else
			cooc.setParameters(q, [wordProperty], null, distance_max+1, 1, 0, 0, 1, score_minimum, 1, false, false);
	} else {
		cooc.setParameters(q, [wordProperty], null, distance_max+1, 1, 1, distance_max+1, 1, score_minimum, 1, false, false);
	}
	cooc.setDirty(true)

	cooc.compute()
	if (debug > 0) println "cooc="+cooc.getLastParametersFromHistory()
	def lines = cooc.getLines()
	def words = [:]
	for (def line : lines) {
		if (!r.contains(line.props[0])) {
			words[line.props[0]] = line
		}
	}
	if (debug > 1) println level+lines.size()

	for (def word : words.keySet()) {
		int size = 10+words[word].score
		if (size > 30) size = 30
		
		def new_pivots = []
		
		if (mode) {
			if (right_dir) {
				def pivot = pivots[-1]
				output.println "$level\"$pivot\" -> \"$word\"	[label=\"${words[word].score}\",fontsize=$size,weight=${words[word].score}];"
				if (mode) new_pivots.addAll(pivots)
				new_pivots << word
			} else {
				def pivot = pivots[0]
				output.println "$level\"$word\" -> \"$pivot\"	[label=\"${words[word].score}\",fontsize=$size,weight=${words[word].score}];"

				new_pivots << word

				if (mode) new_pivots.addAll(pivots)
			}
			printCoocs(new_pivots, level+" ", cooc, right_dir)
		} else {
			def pivot = pivots[0]
			output.println "$level\"$pivot\" -- \"$word\"	[label=\"${words[word].score}\",fontsize=$size,weight=${words[word].score}];"
			printCoocs([word], level+" ", cooc, true)
		}
	}
}


if (mode) {
	output.println "digraph {"
	output.println "rankdir=LR;"
} else {
	output.println "graph {"
}

output.println "\"$starting_word\" [shape=box];"
if (mode) {
	printCoocs([starting_word], "", rightCooc, true)
	r = new HashSet()
	printCoocs([starting_word], "", rightCooc, false)
} else {
	printCoocs([starting_word], "", rightCooc, true)
}

output.println "}"
output.close()
println "Done : "+output_file.getAbsolutePath()
