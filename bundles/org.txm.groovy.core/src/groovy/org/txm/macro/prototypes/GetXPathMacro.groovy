package org.txm.macroproto
// Copyright © 2015 - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

import org.txm.scripts.importer.*

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="srcDirectory", usage="XML source directory", widget="Folder", required=false, def="")
def srcDirectory

@Field @Option(name="srcFile", usage="XML source file", widget="File", required=false, def="")
def srcFile

@Field @Option(name="xPath", usage="XPath expression", widget="String", required=true, def="//tei:title/text()")
def xPath

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

if (srcDirectory==null && srcFile==null) { println "** GetXPathMacro: at least a source file or a source directory must be specified."; return}

if (srcDirectory!=null && srcDirectory.exists()) {

	def files = srcDirectory.listFiles().sort{ it.name }
	if (files == null || files.size() == 0) {
		println "No files in $srcDirectory"
		return;
	}
	files.sort()

	for (File xmlFile : files) {
		String name = xmlFile.getName();
		if (!name.endsWith(".xml")) continue;
	
		println "-- file '$xmlFile':"
		
		XPathResult xpathProcessor = new XPathResult(xmlFile)
		def value = xpathProcessor.getXpathResponse(xPath)
		if (value != null) { println(value) }
}

} else if (srcFile!=null && srcFile.exists()) {
	def xmlFile = srcFile
	String name = xmlFile.getName();
	if (!name.endsWith(".xml")) { println "** GetXPathMacro: source file extension must be '.xml'."; return}
		
	println "-- file '$xmlFile':"

	XPathResult xpathProcessor = new XPathResult(xmlFile)
	def value = xpathProcessor.getXpathResponse(xPath)
	if (value != null) { println(value) } else { println "No result." }
}
