// STANDARD DECLARATIONS
package org.txm.macro.utils

import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.swt.widgets.Display

result = 0

// MessageDialog.NONE, MessageDialog.ERROR, MessageDialog.INFORMATION, MessageDialog.QUESTION, MessageDialog.WARNING
// See http://archive.eclipse.org/eclipse/downloads/documentation/2.0/html/plugins/org.eclipse.platform.doc.isv/reference/api/org/eclipse/jface/dialogs/MessageDialog.html
type = MessageDialog.QUESTION

titre = "Titre de la boîte de dialogue"

message = "Message de la boîte de dialogue. Message de la boîte de dialogue. Message de la boîte de dialogue. Message de la boîte de dialogue. Message de la boîte de dialogue.\n\nRépondre par OK ou Cancel\n(touche [échap]/[Esc] ou fermeture de la fenêtre pour abandonner)"

String[] choices = [ "OK", "Cancel" ]

monitor.syncExec(new Runnable() {
	public void run() {
		result = new MessageDialog(Display.getCurrent().getActiveShell(), titre, null, message, type, choices, 0).open()
	}
})

switch(result) {            
	
	case 0: 
            println("OK")
            break
	case 1: 
            println("Cancel")
            break
	case -1: 
            println("Aborted")
            break
    default: 
            println("Unknown value : "+result)
            break
}
