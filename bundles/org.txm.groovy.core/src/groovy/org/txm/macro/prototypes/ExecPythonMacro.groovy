// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="pythonFile", usage="an example file", widget="FileOpen", required=true, def="script.py")
def pythonFile

// Parameters settings UI
if (!ParametersDialog.open(this)) {
	println("** ExecCQLMacro error: Impossible to open Parameters settings UI dialog box.")
	return
}

def process = "python $pythonFile".execute()
process.text.eachLine {println it}

def exitValue = process.exitValue()
if (exitValue != 0) println "Error during execution: $exitValue"