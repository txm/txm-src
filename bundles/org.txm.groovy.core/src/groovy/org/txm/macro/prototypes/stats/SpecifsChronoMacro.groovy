// Copyright © 2024 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

import org.txm.statsengine.r.core.RWorkspace
import org.txm.lexicaltable.core.functions.LexicalTable

// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof LexicalTable)) {

	println "Selection must be a LexicalTable ($corpusViewSelection). Aborting.";
	return;
}

@Field @Option(name="outputFile", usage="an example file", widget="FileSave", required=true, def="")
def outputFile


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def symbol = corpusViewSelection.getRSymbol()

def script = """
library(textometry)

m <- $symbol
specifs <- specificities(m)
NCOLMATRIX <- length(colnames(m))
NROWMATRIX <- length(rownames(m))
intervals <- c();
intervals_specifs <- c()
rowmargins <- rowSums(m);
df = matrix(NA, nrow=NROWMATRIX)
rownames(df) <- rownames(m)
n <- 2;
intervals <- c()
for(i in 1:NCOLMATRIX) {
	for(j in i:NCOLMATRIX) {
		if (i != j) { # colonnes de i à j
			currentcol <- rowSums(m[,i:j]);
			othercols <- rowmargins - currentcol;
			m2 <- matrix(c(currentcol, othercols), ncol=2);
			specifs2 <- specificities(m2)[,1];

			colname <- paste(colnames(m)[i], "->" , colnames(m)[j])
			interval <- c(i,j)
			intervals <- cbind(intervals, interval)
			df <- cbind(df,specifs2)
			colnames(df)[n] <- colname;
		} else { # colonne seule
			colname <- colnames(specifs)[i]
			interval <- c(i,i)
			intervals <- cbind(intervals, interval)
			df <- cbind(df,specifs[,i])
			colnames(df)[n] <- colname;
			
		}
		n <- n +1;
	}
}

write.table(df, "${outputFile.getAbsolutePath()}")
"""

RWorkspace.getRWorkspaceInstance().eval(script);
println "Result written in: "+outputFile.getAbsolutePath()
