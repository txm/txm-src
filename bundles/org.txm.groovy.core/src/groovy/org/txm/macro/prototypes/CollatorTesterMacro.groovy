package org.txm.macro.prototypes;
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import java.text.Collator

// BEGINNING OF PARAMETERS
@Field @Option(name="string", usage="an example string", widget="String", required=false, def="δὲ ξυνεχὴς εἶχε εἶχεν βληχρός ἐπέβαλε ἔλαβε οὐ ἐπέλαβε ἐπεγίνετο")
def string

@Field @Option(name="localeName", usage="an example string", widget="String", required=false, def="el")
def localeName

@Field @Option(name="strengh", usage="0 1 or 2 for PRIMARY, SECONDARY and TERTIARY", widget="Integer", required=false, def="2")
def strengh

@Field @Option(name="decomposition", usage="0 1 or 2 for NO_DECOMPOSITION, CANONICAL_DECOMPOSITION, FULL_DECOMPOSITION", widget="Integer", required=false, def="2")
def decomposition

if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS

Locale locale = new Locale(localeName)

def collator = Collator.getInstance(locale)

if (strengh < 0) strengh = 0
if (strengh > 2) strengh = 2
collator.setStrength(strengh)

if (decomposition < 0) decomposition = 0
if (decomposition > 2) decomposition = 2
collator.setDecomposition(decomposition)

println "RESULT"
Collections.sort(string.tokenize(), collator);
println tokenize