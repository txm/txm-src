// Copyright © 2019 ENS de Lyon, CNRS, University of Franche-Comté
// @author mdecorde
// @author sheiden

package org.txm.macro.office

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.*

@Field @Option(name="inputDirectory", usage="dossier des fichiers Excel à fusionner", widget="Folder", required=true, def="")
def inputDirectory

@Field @Option(name="outputFile", usage="output file", widget="FileSave", required=true, def="")
def outputFile

@Field @Option(name="sheetName", usage="sheet name (if no name is given the first sheet will be used)", widget="String", required=false, def="")
def sheetName

@Field @Option(name="columnList", usage="list of columns to extract, separated by comma", widget="String", required=false, def="")
def columnList

@Field @Option(name="multipleValues", usage="list of columns with multiple values, separated by comma", widget="String", required=false, def="")
def multipleValues

@Field @Option(name="normalizeIdentifiers", usage="normalize column names to simple lower case letters", widget="Boolean", required=true, def="true")
def normalizeIdentifiers

def stringToIdent = { str -> org.txm.utils.AsciiUtils.buildAttributeId(org.txm.utils.AsciiUtils.convertNonAscii(str)).toLowerCase() }

if (!ParametersDialog.open(this)) return false

if (!inputDirectory.exists()) {
	println "** $scriptName: no '"+inputDirectory.name+"' directory found. Aborting."
	return false
}

if (!inputDirectory.canRead()) {
	println "** $scriptName: '"+inputDirectory.name+"' directory not readable. Aborting."
	return false
}

def aborted = false

def scriptName = this.class.getSimpleName()

columnList = columnList.split(",") as List

println "columnList = "+columnList

multipleValues = multipleValues.split(",") as List

// create output file
out = new FileOutputStream(outputFile)
HSSFWorkbook wb = new HSSFWorkbook()
finalSheet = wb.createSheet()

// create header line
nFinalRows = 1
r = finalSheet.createRow(0)
columnList.eachWithIndex { columnName, i ->
	c = r.createCell(i)
	c.setCellValue(normalizeIdentifiers?stringToIdent(columnName):columnName)
}

def f = []

inputDirectory.eachFileMatch(~/.*\.xlsx/) { f << it }

if (f.size() == 0) {
	println "** $scriptName: no .xlsx file found. Aborting."
	return false
}

debug = false
//ConsoleProgressBar cpb = new ConsoleProgressBar(f.size())

try {

	// for each .xlsx input file (lexicographic order)
	f.sort { it.name }.each { inputFile ->

		if (aborted) return false

		if (debug) println "Processing "+inputFile+"."
		//cpb.tick()
		rownum = 0

		// open input sheet
		wb2 = WorkbookFactory.create(inputFile)
		ws = null
		if (sheetName.length() == 0) {
			ws = wb2.getSheetAt(0)
		} else {
			ws = wb2.getSheet(sheetName)
			if (ws == null) {
				println "** $scriptName: no '"+sheetName+" found. Aborting."
				aborted = true
				return false
			}
		}

		if (ws == null) {
			println "** $scriptName: no sheet found. Aborting."
			aborted = true
			return false
		}

		nRows = ws.getPhysicalNumberOfRows()
		if (debug) println nRows+" rows."

		// get columns positions
		firstRow = ws.getRow(0)
		nCols = firstRow.getPhysicalNumberOfCells()
		def colNames = []
		def cellIndexes = []
		def multipleValuesCols = []
		0.upto(nCols-1, { iCol ->
			value = firstRow.getCell(iCol).getStringCellValue()
			if (columnList.contains(value)) {
				colNames << value
				cellIndexes << iCol
			}
			if (multipleValues.contains(value)) {
				multipleValuesCols << iCol
			}
		})

		if (cellIndexes.size() != columnList.size()) {
			columnList.removeAll(colNames)
			println "** $scriptName: some columns missing in file $inputFile: "+columnList.join(", ")+". Aborting."

			aborted = true
			return false
		}

		// sort columns indexes by columnList parameter order
		cellIndexes.sort { columnList.indexOf(colNames[cellIndexes.indexOf(it)]) }

		// copy column values
		1.upto(nRows-1, { iRow ->
			row = ws.getRow(iRow)
			r = finalSheet.createRow(nFinalRows)
			
			nCellCreated = 0
			cellIndexes.each { iCol ->
				value = row.getCell(iCol).getStringCellValue()
				c = r.createCell(nCellCreated)
				if (iCol in multipleValuesCols && value.size() > 0) {
					(lstr = value.split(";")*.trim() as List).removeAll { it.isEmpty() }
					c.setCellValue("|"+lstr.join("|")+"|")
				} else {
					c.setCellValue(value)
				}
				nCellCreated++
			}
			
			nFinalRows++
		})
	}
} catch(Exception e) {
	aborted = true
	return false
}
//cpb.done()

/* write sheet */

wb.write(out)
out.close()

if (aborted) return false

println nFinalRows+" rows written."

return true

