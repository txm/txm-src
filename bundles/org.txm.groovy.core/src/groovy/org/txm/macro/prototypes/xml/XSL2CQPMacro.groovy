// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.importer.ApplyXsl2;
import groovy.xml.XmlParser

// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Error: Selection must be a corpus"
	return false;
}

@Field @Option(name="xslFile", usage="an example file", widget="FileOpen", required=true, def="")
def xslFile
@Field @Option(name="debug", usage="an example file", widget="Boolean", required=true, def="false")
def debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

println "corpora selection: "+corpusViewSelection
if (!xslFile.getName().endsWith(".xsl")) {
	println "Error: XSL selected file is not a '.xsl' file: $xslFile"
	return false;
}

MainCorpus mainCorpus = ((CQPCorpus)corpusViewSelection).getMainCorpus();
File binDir = mainCorpus.getBaseDirectory();
File txmDir = new File(binDir, "txm/"+mainCorpus.getName());
File resultsDir = new File(binDir, "results");
resultsDir.mkdirs()

if (!txmDir.exists()) {
	println "Error: the 'txm' directory does not exist: $txmDir"
	return false;
}

def xmlFiles = txmDir.listFiles().sort{ it.name };
if (xmlFiles == null || xmlFiles.size() == 0) {
	println "Error: no file found in $txmDir"
	return false;
}

HashSet<List<String>> allmatches = new HashSet<String>();
ApplyXsl2 a = new ApplyXsl2(xslFile.getAbsolutePath());
println "Querying..."
for (File xmlFile : xmlFiles) {
	println "	"+xmlFile.getName()
	File resultFile = new File(resultsDir, "xslqueryresult_"+xmlFile.getName());
	a.process(xmlFile, resultFile);
	
	def matches = new XmlParser().parse(resultFile)
	matches.match.each() { match ->
		def l = [];
		match.wRef.each() { l.add(it.attribute("id")); }
		allmatches << l
	}
	if (!debug) resultFile.delete()
}

//println "Matches: "
//for (def m : allmatches) println " "+m

def subqueries = []
for (def m : allmatches) {
	if (m.size() == 1) subqueries <<  "[id=\""+m[0]+"\"]"
	else if (m.size() == 2) 
		subqueries <<  "[id=\""+m[0]+"\"][]"
	else
		subqueries << "[id=\""+m[0]+"\"]"+"[]{"+(m.size-1)+"}"
}
def query = subqueries.join("|")
def initialquery = query
while (query.length() > 1500) {
	oldquery = query
	query = query.substring(0, query.indexOf("|"))
	println "Warning : query has been truncated: "+oldquery
}

println "CQL: $query"
if (debug) println "See debug files in: "+resultsDir
if (initialquery != query) println "Initial CQL: $initialquery"
