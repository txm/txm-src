// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

@Field @Option(name="file", usage="an example file", widget="FileOpen", required=true, def="")
def file

@Field @Option(name="read_right", usage="read", widget="Boolean", required=true, def="true")
def read_right
@Field @Option(name="write_right", usage="write", widget="Boolean", required=true, def="true")
def write_right
@Field @Option(name="execute_right", usage="execute", widget="Boolean", required=true, def="true")
def execute_right

@Field @Option(name="current_user_only", usage="read", widget="Boolean", required=true, def="false")
def current_user_only

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

if (file.exists()) {

file.setReadable(read_right, current_user_only);
file.setWritable(write_right, current_user_only)
file.setExecutable(execute_right, current_user_only);

} else {
	println "Error: file not found $file"
}