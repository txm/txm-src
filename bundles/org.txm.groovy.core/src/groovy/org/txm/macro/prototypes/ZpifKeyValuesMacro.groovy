// STANDARD DECLARATIONS
package org.txm.macroproto

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.functions.index.*
import org.txm.index.core.functions.Index

if (!(corpusViewSelection instanceof Index)) {
	println "Corpora selection is not an Index. Aborting."
	return
}

def lines = corpusViewSelection.getAllLines();
int i = 1;
while (i < lines.size()) {
	println "Order "+i+" -> "+lines[i-1].getFrequency()
	i = i*10
}

println "Done."