
package org.txm.macro.stats
// Copyright © 2010-2016 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// @author mdecorde
// @author sheiden
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

// This macro computes the basic vocabulary of a partition
// given a specificities table. The result is saved in a
// TSV file.

import org.txm.Toolbox
import org.txm.searchengine.cqp.clientExceptions.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.statsengine.r.core.RWorkspace
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.specificities.core.functions.Specificities
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.*
import org.txm.statsengine.r.core.RWorkspace

if (!(corpusViewSelection instanceof Specificities)) {
    println "** You need to select a Specificities result icon in the Corpus view before calling this macro. Aborting."
    return
}

def specif = corpusViewSelection;

@Field @Option(name="segmentSize", usage="score maximum threshold", widget="Integer", required=false, def="10")
def segmentSize

@Field @Option(name="limitStructure", usage="score maximum threshold", widget="String", required=false, def="s")
def limitStructure

@Field @Option(name="n_sentences", usage="score maximum threshold", widget="Integer", required=false, def="10")
def n_sentences

// END OF PARAMETERS
if (!ParametersDialog.open(this)) return

def CQI = CQPSearchEngine.getCqiClient()
def rw = RWorkspace.getRWorkspaceInstance()
def lt = corpusViewSelection.getParent()
def ltParent = lt.getParent()
def parts = null
if (ltParent instanceof Subcorpus) {
    parts = [ltParent]
} else if (ltParent instanceof Partition) {
    parts = ltParent.getParts();
}

def usedP = lt.getProperty()

println "Analysed property: $usedP"
println "Analysing: ${parts.size()} parts"

for (def part : parts) {


    def matches = part.getMatches()
    if (matches.size() > 1) {
        println "Skip multi-match part."
        continue;
    }
    def match = matches.get(0);
    def start = match.getStart()
    def end = match.getEnd()
    usedP = part.getProperty(usedP.getName())
    println "Part: "+part.getName()+ " from $start to $end"
    
    int maxScore = 0;
    def maxScoreUnits = []
    int minScore = 9999999999;
    def minScoreUnits = []
     
    int[] positions = new int[segmentSize]
    for (int i = 0 ; i < segmentSize ; i++) positions[i] = start + i;
    String[] units = CQI.cpos2Str(usedP.getQualifiedName(), positions)

    int npositions = end-start
    for (int i = segmentSize + 1 ; i < npositions ; i++) {

        // shift units window
        for (int j = 1 ; j < segmentSize ; j++) units[j-1] = units[j];

        // add one new position 
        units[segmentSize-1] = CQI.cpos2Str(usedP.getQualifiedName(), [start+i] as int[])[0]

        // get scores
        def script = specif.getRSymbol()+"[c(";
        script += "\"" + units.join("\", \"");
        script += "\"), \""+part.getName()+"\"]"

        def score = rw.eval("sum(abs("+ script.replace("\"\"\"", "\"\\\"\"") +"))").asDouble();
        
        if (score > maxScore) {
        	maxScore = score
        	maxScoreUnits.add(""+score+" -> "+units.join(" "));
        	if (maxScoreUnits.size() > n_sentences) maxScoreUnits.remove(0)
        }
        if (score < minScore) {
        	minScore = score
        	minScoreUnits.add(""+score+" -> "+units.join(" "));
        	if (minScoreUnits.size() > n_sentences) minScoreUnits.remove(0)
        }
    }
    
    println "Max: $maxScore Sentences:"
    println maxScoreUnits.join("\n")
    println "Min: $minScore Sentences:"
    println minScoreUnits.join("\n")
}





