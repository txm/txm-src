// STANDARD DECLARATIONS
package org.txm.macro.prototypes.xml


import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import net.sf.saxon.*
import net.sf.saxon.query.*
import net.sf.saxon.om.*
import javax.xml.transform.*
import javax.xml.transform.sax.*
import javax.xml.transform.stream.*
import org.xml.sax.*
import javax.xml.xpath.*
import net.sf.saxon.event.*
import org.w3c.dom.*
import net.sf.saxon.s9api.*
import javax.xml.parsers.*

// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Error: Selection must be a corpus"
	return false;
}

@Field @Option(name="xqFile", usage="a Xquery file", widget="FileOpen", required=true, def="")
def xqFile
@Field @Option(name="outFile", usage="optional output file", widget="File", required=false, def="")
def outFile
@Field @Option(name="debug", usage="an example file", widget="Boolean", required=true, def="false")
def debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

println "corpora selection: "+corpusViewSelection
if (!xqFile.getName().endsWith(".xq")) {
	println "Error: Xquery selected file is not a '.xd' file: $xdFile"
	return false;
}

MainCorpus mainCorpus = ((CQPCorpus)corpusViewSelection).getMainCorpus();
File binDir = mainCorpus.getProjectDirectory();
File txmDir = new File(binDir, "txm/"+mainCorpus.getName());

if (!txmDir.exists()) {
	println "Error: the 'txm' directory does not exist: $txmDir"
	return false;
}

def xmlFiles = txmDir.listFiles().sort{ it.name };
if (xmlFiles == null || xmlFiles.size() == 0) {
	println "Error: no file found in $txmDir"
	return false;
}

String query = """<matches>
  { 
    for \$t in fn:collection('$txmDir')
      for \$w in \$t//tei:w
        let \$pos := \$w/txm:ana[@type="#frpos"]/text()
        return <match>{\$w/@id}</match>
  }
</matches>
"""

Processor processor = new Processor(false)
XQueryCompiler xqc = processor.newXQueryCompiler()
xqc.declareNamespace("tei", "http://www.tei-c.org/ns/1.0")
xqc.declareNamespace("txm", "http://textometrie.org/1.0")
xqc.declareNamespace("fn", "http://www.w3.org/2005/xpath-functions")
XQueryExecutable exp = xqc.compile(query)

DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
dfactory.setNamespaceAware(true);
Document dom = dfactory.newDocumentBuilder().newDocument();
exp.load().run(new DOMDestination(dom));
if (outFile instanceof File && outFile.getName().length() > 0) {
	def writer = outFile.newWriter("UTF-8")
	writer.println dom.getDocumentElement()
	writer.close()
	println "Result written in "+outFile.getAbsolutePath()
} else {
	println dom.getDocumentElement()
}