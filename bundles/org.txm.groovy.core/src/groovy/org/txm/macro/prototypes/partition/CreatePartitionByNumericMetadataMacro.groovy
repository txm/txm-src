// STANDARD DECLARATIONS
package org.txm.macroproto.partition

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.utils.logger.Log;
import org.txm.rcp.views.*
import org.txm.statsengine.r.core.RWorkspace
import org.txm.Toolbox

//BEGINNING OF PARAMETERS
def corpus = corpusViewSelection
if (!(corpus instanceof CQPCorpus)) {
	println "Error: this macro should be run with a CQPCorpus selected"
	return
}

@Field @Option(name="property", usage="the structural Unit properties list separated with commas", widget="String", required=true, def="sadnesst1")
def property = "sadnesst1"

@Field @Option(name="values_file", usage="txt file", widget="File", required=true, def="C:\\Users\\Fanny\\Desktop\\values_num.txt")
def values_file

@Field @Option(name="balance", usage="the structural Unit properties list separated with commas", widget="Boolean", required=true, def="true")
def balance = false

@Field @Option(name="expand_struct", usage="expand_struct", widget="String", required=true, def="question")
def expand_struct = "question"

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

def property_values = values_file.getText("UTF-8").replaceAll("\n", ",").replaceAll("\r", "")

println "Balance de la métadonnée '$property' du corpus '$corpus'"

def domain = []
for (def s : property_values.split(",")) {
	s = s.trim()
	try {
		domain << Integer.parseInt(s)
	} catch(Exception e) {
		domain << Float.parseFloat(s)
	}
}
def uniq_domain = new HashSet(domain).sort() 
println "domain size "+domain.size()

def names = []
def queries = []
def p = 2
String values = ""

if (!balance) {

for (def d : uniq_domain) {
	if (d < p) {
		values += "|$d"
	} else {
		if (values.startsWith("|")) values = values.substring(1)
		queries <<  "[_.text_"+property+"=\"$values\"] expand to $expand_struct"
		names << values
		values = d
		p++
	}
}
queries <<  "[_.text_"+property+"=\"$values\"] expand to $expand_struct"
names << values


corpus.createPartition(property+"_non_balanced", queries, names);

} else {
//String v = domain.toString()
//v = "c("+v.substring(1, v.length() -1) + ")"
String v  = "c($property_values)"
println "=========================="
//RWorkspace.getRWorkspaceInstance().addVectorToWorkspace("domain", domain.toArray(new double[domain.size()]))
def res = RWorkspace.getRWorkspaceInstance().eval("quantile($v)")
def ranges = res.asDoubles()
println "quantiles : $ranges"
values = ""
p = ranges[1]
i = 1
names = []
queries = []
for (def d : uniq_domain) {
	//println "d=$d, p=$p"
	if (d < p) {
		values += "|$d"
	} else {
		if (values.startsWith("|")) values = values.substring(1)
		println """[_.text_${property}="$values"]"""
		queries <<  "[_.text_"+property+"=\"$values\"] expand to $expand_struct"
		names << values
		values = d
		i++
		p = ranges[i]
	}
}

queries <<  "[_.text_"+property+"=\"$values\"] expand to $expand_struct"
names << values

corpus.createPartition(property+"_balanced", queries, names);
}


println queries
println names


//display the graphic
monitor.syncExec(new Runnable() {
	@Override
	public void run() {	
	org.txm.rcp.commands.RestartTXM.reloadViews();
	}
});

/*
int[] counts = new int[51]
for (def d : domain) {
	counts[(int)(10*d)]++
}
println counts
*/