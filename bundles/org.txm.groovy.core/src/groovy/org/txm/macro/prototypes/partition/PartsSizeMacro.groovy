// STANDARD DECLARATIONS
package org.txm.macro

import org.txm.searchengine.cqp.corpus.Partition;

if (!(corpusViewSelection instanceof Partition)) {
	println "Select a partition before calling this macro."
	return;
}
Partition p = corpusViewSelection

for (def part : p.getParts()) {
	println part.getName()+"\t"+part.getSize()
}