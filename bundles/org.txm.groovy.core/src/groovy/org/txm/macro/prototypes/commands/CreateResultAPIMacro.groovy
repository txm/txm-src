import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.concordance.core.functions.Concordance


println "corpora selection: "+corpusViewSelection
if (corpusViewSelection == null) return;
if (!(corpusViewSelection instanceof CQPCorpus)) return;

def conc = new Concordance(corpusViewSelection)
conc.saveParameter("query", "je")
conc.autoLoadParametersFromAnnotations()
conc.loadParameters()
conc.compute()

println conc