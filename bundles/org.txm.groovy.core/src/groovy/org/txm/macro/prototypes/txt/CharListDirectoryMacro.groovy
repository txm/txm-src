package org.txm.macroproto.txt;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="inputFile", usage="The file to read", widget="File", required=true, def="")
def inputFile

@Field @Option(name="inputDir", usage="The file to read", widget="Folder", required=false, def="")
def inputDir


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

def dic = [:]

// END OF PARAMETERS
if (inputDir != null && inputDir.exists()) {
if (inputDir.listFiles() == null) {println "no files..."; return;}
	println "processing "+inputDir.listFiles().size()+" files"
	for (def f : inputDir.listFiles()) {
		if (f.isDirectory()) continue;
		print "."
		count(f, dic)
	}
	println ""
} else {
	count(inputFile, dic)
}
int total = 0
for (def e : dic.sort()) {
	println(sprintf("%c\t%d", [e.getKey(), e.getValue()]))
	total += e.getValue()
}
println "TOTAL\t$total"

def count(def f, def dic) {
	String s = f.getText()
	for (char c : s) {
		if (c == '\r' || c == '\n' || c == '\t' || c == ' ' || c == ' ') continue;
		if (!dic.containsKey(c)) { dic[c] = 0 }
		dic[c]++
	}

	return dic
}

