// Copyright © 2024 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.jfree.chart.annotations.*
import java.awt.BasicStroke;
import java.awt.Color;

@Field @Option(name="annotationsFile", usage="Table file, 2 sheets: lines & columns", widget="FileOpen", required=true, def="")
def annotationsFile

@Field @Option(name="color_name", usage="Default color", widget="String", required=false, def="black")
def color_name

@Field @Option(name="line_width", usage="Default line width", widget="Float", required=false, def="1.0f")
def line_width

//@Field @Option(name="draw_arrows", usage="Default lines are arrow", widget="Boolean", required=false, def="false")
//def draw_arrows

@Field @Option(name="draw_dashs", usage="Default dashes", widget="Boolean", required=false, def="false")
def draw_dashs

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

println "corpora selection: "+corpusViewSelection

println editor
if (!(editor instanceof org.txm.ca.rcp.editors.CAEditor)) {
	println "Select the CA editor an call again the macro"
	return
}

def ca = editor.getResult()
def chart = ca.getChart()
def plot = chart.getPlot()
def series = plot.getDataset()
def rows = ca.getRowNames()
def cols = ca.getColNames()

// XYBoxAnnotation, XYDataImageAnnotation, XYDrawableAnnotation, XYImageAnnotation, XYLineAnnotation, XYPointerAnnotation, XYPolygonAnnotation, XYShapeAnnotation, XYTextAnnotation, XYTitleAnnotation

def dash = null
if (draw_dashs) dash = [10f] as float[]
def paint = getColor(color_name)

println "Defaults dash=$dash color=$paint"
println "Clear annotations"
plot.clearAnnotations()
def columns = ["from", "to", "color", "dash", "width"]
TableReader reader = new TableReader(annotationsFile, "rows")
reader.readHeaders()
def header = reader.getHeaders()
for (def c : columns) {
	if (!header.contains(c)) {
		println "Missing '$c' column in 'cols' sheet"
		return
	}
}

def list = Arrays.asList(rows)
while (reader.readRecord()) {
	def line = reader.getRecord()
	println "Add row annotation: "+line
	
	String name_from = line["from"]
	String name_to = line["to"]
	int idx1 = list.indexOf(name_from)
	int idx2 = list.indexOf(name_to)
	
	if (idx1 < 0) {
		println "Row not found: '$name_from' in $line and rows $rows"
		continue
	}
	if (idx2 < 0) {
		println "Row not found: '$name_to' in $line and rows $rows"
		continue
	}
	//println "idx1=$idx1 idx2=$idx2"
	
	def line_width2 = line_width
	if (line["width"] != null && line["width"].length() > 0) line_width2 = Float.parseFloat(line["width"])
	def dash2 = dash
	if (line["dash"] != null && line["dash"].length() > 0) dash2 = [10f] as float[]
	def stroke2 = new BasicStroke(line_width2, 0, 0, 1, dash2, 0)
	def color2 = paint
	if (line["color"] != null && line["color"].length() > 0) color2 = getColor(line["color"])
	//def arrow2 = draw_arrows
	//if (line["arrow"] != null && line["arrow"].length() > 0) arrow2 = "true".equals(line["arrow"])
	if (color2 == null) {
		println "color not found: "+line["color"]
		continue
	}
	
	//def a = new XYLineAnnotation(rowscoords[idx1][F1], rowscoords[idx1][F2], rowscoords[idx2][F1], rowscoords[idx2][F2], stroke2, color2)
	def a = new XYLineAnnotation(series.getX(0, idx1), series.getY(0, idx1), series.getX(0, idx2), series.getY(0, idx2), stroke2, color2)

	plot.addAnnotation(a)
}

reader = new TableReader(annotationsFile, "cols")
reader.readHeaders()
header = reader.getHeaders()
for (def c : columns) {
	if (!header.contains(c)) {
		println "Missing '$c' column in 'cols' sheet."
		return
	}
}

list = Arrays.asList(cols)
while (reader.readRecord()) {
	def line = reader.getRecord()
	println "Add col annotation: "+line
	
	def name_from = line["from"]
	def name_to = line["to"]
	int idx1 = list.indexOf(name_from)
	int idx2 = list.indexOf(name_to)
	
	if (idx1 < 0) {
		println "Col not found: '$name_from' in $line and cols $cols"
		continue
	}
	if (idx2 < 0) {
		println "Col not found: '$name_to' in $line and cols $cols"
		continue
	}
	
	def line_width2 = line_width
	if (line["width"] != null && line["width"].length() > 0) line_width2 = Float.parseFloat(line["width"])
	def dash2 = dash
	if (line["dash"] != null && line["dash"].length() > 0) dash2 = [10f] as float[]
	def stroke2 = new BasicStroke(line_width2, 0, 0, 1, dash2, 0)
	def color2 = paint
	if (line["color"] != null && line["color"].length() > 0) color2 = getColor(line["color"])
	//def arrow2 = draw_arrows
	//if (line["arrow"] != null && line["arrow"].length() > 0) arrow2 = "true".equals(line["arrow"])
	if (color2 == null) {
		println "color not found: "+line["color"]
		continue
	}
	
	//def a = new XYLineAnnotation(colscoords[idx1][F1], colscoords[idx1][F2], colscoords[idx2][F1], colscoords[idx2][F2], stroke2, color2)
	def a = new XYLineAnnotation(series.getX(1, idx1), series.getY(1, idx1), series.getX(1, idx2), series.getY(1, idx2), stroke2, color2)
	
	plot.addAnnotation(a)
}

println "Done."


/// FUNCTIONS

def getColor(String name) {
    try {
        return (Color)Color.class.getField(name.toUpperCase()).get(null);
    } catch (Exception e) {
        println "** no color named: $name"
        try {
        	return Color.decode(name)
   		} catch (Exception e2) {
        	println "** color not decoded: $name"
        	return Color.black;
   		}
    }
}








