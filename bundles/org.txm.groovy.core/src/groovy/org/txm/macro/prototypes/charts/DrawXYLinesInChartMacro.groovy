// Copyright © 2024 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.jfree.chart.annotations.*
import java.awt.BasicStroke;
import java.awt.Color;

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

//@Field @Option(name="query", usage="an example query", widget="Query", required=true, def='[pos="V.*"]')
//def query

//@Field @Option(name="inputFile", usage="an example file", widget="FileOpen", required=true, def="")
//def inputFile

//@Field @Option(name="outputFile", usage="an example file", widget="FileSave", required=true, def="")
//def outputFile

//@Field @Option(name="folder", usage="an example folder", widget="Folder", required=false, def="")
//def folder

//@Field @Option(name="date", usage="an example date", widget="Date", required=false, def="1984-09-01")
//def date

//@Field @Option(name="integer", usage="an example integer", widget="Integer", required=false, def="42")
//def integer

//@Field @Option(name="floatValue", usage="an example float", widget="Float", required=false, def="42.42")
//def floatValue

//@Field @Option(name="string", usage="an example string", widget="String", required=false, def="hello world!")
//def string

//@Field @Option(name="separator", usage="an example separator", widget="Separator", required=false, def="Separator name")
//def separator

//@Field @Option(name="longString", usage="an example longString", widget="Text", required=false, def="hello world!")
//def longString

//@Field @Option(name="arraySelection", usage="an example single value selection in a list", widget="StringArray", metaVar="first	second	third", required=false, def="second")
//def arraySelection

//@Field @Option(name="multipleArraySelection", usage="an example multiple value selection in a list", widget="StringArrayMultiple", metaVar="first	second	third", required=false, def="second")
//def multipleArraySelection

//@Field @Option(name="password", usage="an example password", widget="Password", required=false, def="hello world!")
//def password

//@Field @Option(name="bool", usage="an example boolean", widget="Boolean", required=false, def="true")
//def bool

@Field @Option(name="annotationsFile", usage="Table file, 2 sheets: lines & columns", widget="FileOpen", required=true, def="")
def annotationsFile

@Field @Option(name="color_name", usage="an example string", widget="String", required=false, def="black")
def color_name

@Field @Option(name="line_width", usage="an example integer", widget="Float", required=false, def="1.0f")
def line_width

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

println "corpora selection: "+corpusViewSelection

println editor
if (!(editor instanceof org.txm.ca.rcp.editors.CAEditor)) {
	println "Select the CA editor an call again the macro"
	return
}

def ca = editor.getResult()
def chart = ca.getChart()
def plot = chart.getPlot()

println ca
println chart
println plot

// XYBoxAnnotation, XYDataImageAnnotation, XYDrawableAnnotation, XYImageAnnotation, XYLineAnnotation, XYPointerAnnotation, XYPolygonAnnotation, XYShapeAnnotation, XYTextAnnotation, XYTitleAnnotation

def stroke = new BasicStroke(line_width)
def paint = Color.getColor(color_name)

println "Clear annotations"
plot.clearAnnotations()

TableReader reader = new TableReader(annotationsFile)
reader.readHeaders()
def header = reader.getHeaders()
if (!header.contains("x1")) {
	println "Missing 'x1' column"
	return
}
if (!header.contains("y1")) {
	println "Missing 'y1' column"
	return
}
if (!header.contains("x2")) {
	println "Missing 'x2' column"
	return
}
if (!header.contains("y2")) {
	println "Missing 'y2' column"
	return
}
while (reader.readRecord()) {
def line = reader.getRecord()
	println "Add annotation: "+line
	plot.addAnnotation(new XYLineAnnotation(Float.parseFloat(line["x1"]), Float.parseFloat(line["y1"]), Float.parseFloat(line["x2"]), Float.parseFloat(line["y2"])))
}

println "Done."