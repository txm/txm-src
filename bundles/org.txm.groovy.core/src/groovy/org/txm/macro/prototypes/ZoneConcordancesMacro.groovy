// STANDARD DECLARATIONS
package org.txm.macroproto

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.apache.commons.lang.StringUtils

// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "please select a corpus: $corpusViewSelection"
	return
}

def CQI = CQPSearchEngine.getCqiClient()

@Field @Option(name="query", usage="an example query", widget="Query", required=true, def='[pos="V.*"]')
def query

@Field @Option(name="property", usage="an example query", widget="String", required=true, def='word')
def property

@Field @Option(name="outputFile", usage="an example file", widget="FileSave", required=true, def="")
def outputFile

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

def writer = outputFile.newWriter("UTF-8")

// END OF PARAMETERS

String query_string = query.toString();
if (query_string.startsWith("@")) query_string = query_string.substring(1)

def split = query_string.split("@")
def queries = []
// @ A @ B @ C @ D
println "requête de zones: "+query_string
//println split
String current_q = ""
for (int i = 0 ; i < split.length ;i++) {
	def reconstructed_query = ""
	def q = split[i]
	
	//println "q=$q"
	
	//println "current_q=$current_q"
	
	if (i > 0 && q.length() > 0) {
		reconstructed_query = current_q+"@"+q
		if (i < (split.length-1)) reconstructed_query += split[i+1..split.length-1].join("")
		queries << reconstructed_query
	}
	
	current_q += q
	//println "Q="+current_q
}

if (queries.size() == 0) {
	println "no target @ found in query: $query"
	return
}

def corpus = corpusViewSelection
def wordProperty = corpus.getProperty(property)
def qrs = []
for (def q : queries) {
	qrs << corpus.query(new CQLQuery(q), "TMP", false).getMatches()
}
def qr = qrs[0]
println ""+qr.size()+" matches pour "+(queries.size()+1)+" zones"

writer.print "N\tposition\t"
for (int j = 0 ; j < queries.size()+1 ; j++) {
	writer.print "\t"
	writer.print "Z"+(j+1)
}
writer.println ""

for (int i = 0 ; i < qr.size() ; i++) {
	int[] positions = qr[i].getStart() .. qr[i].getEnd()
	def forms = CQI.cpos2Str(wordProperty.getQualifiedName(), positions)
	
	def targets = []
	for (int j = 0 ; j < queries.size() ; j++) {
		targets <<  (qrs[j][i]).getTarget()
	}
	
	writer.print ""+(i+1)+"\t"+positions[0]+"\t"
	boolean first = true
	for (int j = 0 ; j < forms.size() ; j++) {
		if (targets.contains(positions[j])) {
			writer.print "\t"
			first = true
		}
		
		if (!first) { writer.print " "}
		writer.print forms[j]
		first=false
	}
	writer.println ""
	
}

writer.close()
println "Result written in "+outputFile.getAbsolutePath()