// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.file.Path
import java.nio.file.attribute.FileOwnerAttributeView
import java.nio.file.attribute.UserPrincipal
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.searchengine.cqp.*
import java.io.IOException;
import java.nio.file.*
import java.nio.file.attribute.*;

// Parameter declaration - Déclaration du paramètre
@Field @Option(name="directory", usage="the directory to diagnose", widget="Folder", required=true, def="set da enpos;")
		File directory;

// Parameters settings UI
if (!ParametersDialog.open(this)) {
	println("** ExecCQLMacro error: Impossible to open Parameters settings UI dialog box.")
	return
}

println "full path="+directory.getAbsolutePath()
println " exists? "+directory.exists()
println " read? "+directory.canRead()
println " write? "+directory.canWrite()
println " executable? "+directory.canExecute()
println " hidden? "+directory.isHidden()
def files = directory.listFiles().sort{ it.name }
println " number of files? "+files.size()


Path path = Paths.get(directory.getAbsolutePath());

FileOwnerAttributeView ownerAttributeView = Files.getFileAttributeView(path, FileOwnerAttributeView.class);
if (ownerAttributeView != null) {
	UserPrincipal owner = ownerAttributeView.getOwner();
	if (owner != null) println " file owner attribute: "+owner.getName()
}

AclFileAttributeView aclAttributeView = Files.getFileAttributeView(path, AclFileAttributeView.class);
if (aclAttributeView != null) {
	List<AclEntry> acl = aclAttributeView.getAcl();
	if (acl != null) {
		for (AclEntry entry : acl)
			if (acl != null) println " acl entry: "+entry
	}
}

BasicFileAttributeView basicAttributeView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
if (basicAttributeView != null) {
	BasicFileAttributes attributes = basicAttributeView.readAttributes()
	if (attributes != null)  {
		println " basic attributes: creation time: "+attributes.creationTime()
		println " basic attributes: last access time: "+attributes.lastAccessTime()
		println " basic attributes: last modification time: "+attributes.lastModifiedTime()
		println " basic attributes: file key: "+attributes.fileKey()
		println " basic attributes: directory file?: "+attributes.isDirectory()
		println " basic attributes: symbolic link?: "+attributes.isSymbolicLink()
		println " basic attributes: regular file?: "+attributes.isRegularFile()
	}
}

DosFileAttributeView dosAttributeView = Files.getFileAttributeView(path, DosFileAttributeView.class);
if (dosAttributeView != null) {
	DosFileAttributes attributes = dosAttributeView.readAttributes()
	if (attributes != null) {
		println " dos attributes: creation time: "+attributes.creationTime()
		println " dos attributes: last access time: "+attributes.lastAccessTime()
		println " dos attributes: last modification time: "+attributes.lastModifiedTime()
		println " dos attributes: file key: "+attributes.fileKey()
		println " dos attributes: directory file?: "+attributes.isDirectory()
		println " dos attributes: symbolic link?: "+attributes.isSymbolicLink()
		println " dos attributes: regular file?: "+attributes.isRegularFile()
		println " dos attributes: archive file?: "+attributes.isArchive()
		println " dos attributes: system file?: "+attributes.isSystem()
	}
}

PosixFileAttributeView posixAttributeView = Files.getFileAttributeView(path, PosixFileAttributeView.class);
if (posixAttributeView != null) {
	PosixFileAttributes attributes = posixAttributeView.readAttributes()
	if (attributes != null) {
		println " posix attributes: group: "+attributes.group()
		println " posix attributes: creation time: "+attributes.creationTime()
		println " posix attributes: last access time: "+attributes.lastAccessTime()
		println " posix attributes: last modification time: "+attributes.lastModifiedTime()
		println " posix attributes: file key: "+attributes.fileKey()
		println " posix attributes: directory file?: "+attributes.isDirectory()
		println " posix attributes: symbolic link?: "+attributes.isSymbolicLink()
		println " posix attributes: regular file?: "+attributes.isRegularFile()
		println " posix attributes: permissions: "+attributes.permissions().sort()
	}
}

UserDefinedFileAttributeView userdefinedAttributeView = Files.getFileAttributeView(path, UserDefinedFileAttributeView.class);
if (userdefinedAttributeView != null) {
	def attributes = userdefinedAttributeView.list()
	if (attributes != null) {
		for (def entry : attributes)
			println " user defined attributes: "+entry
	}
}