// STANDARD DECLARATIONS
package org.txm.macroproto.partition

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.utils.logger.Log;
import org.txm.rcp.views.*
import org.txm.statsengine.r.core.RWorkspace
import org.txm.Toolbox

//BEGINNING OF PARAMETERS
def corpus = corpusViewSelection
if (!(corpus instanceof CQPCorpus)) {
	println "Error: this macro should be run with a CQPCorpus selected"
	return
}

@Field @Option(name="values_file", usage="txt file", widget="File", required=true, def="C:\\Users\\Fanny\\Desktop\\values.txt")
def values_file

@Field @Option(name="property", usage="the structural Unit properties list separated with commas", widget="String", required=true, def="emotionalfeelingthe911")
def property = "emotionalfeelingthe911"

@Field @Option(name="expand_struct", usage="expand_struct", widget="String", required=true, def="question")
def expand_struct = "question"

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

println "Partition du corpus '$corpus' à partir de la métadonnée '$property' "

def domain = []
for (def s :  values_file.getText("UTF-8").split("\n")) domain << s.trim()

def uniq_domain = new HashSet(domain).sort() 
println "domain size "+domain.size()
println "uniq domain size "+uniq_domain.size()

def names = []
def queries = []

for (def d : uniq_domain) {
	queries <<  "[_.text_"+property+"=\"$d\"] expand to $expand_struct"
	names << d	
}

corpus.createPartition(property+"_non_balanced", queries, names);

println queries
println names

//display the graphic
monitor.syncExec(new Runnable() {
	@Override
	public void run() {	
	org.txm.rcp.commands.RestartTXM.reloadViews();
	}
});