// STANDARD DECLARATIONS
package org.txm.macro.prototypes.cqp

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.utils.*

if (!(corpusViewSelection instanceof Subcorpus)) {
	println "Selection is not a Corpus: $corpusViewSelection"
	return false
}

def CQI = Toolbox.getCqiClient();
if ((CQI instanceof NetCqiClient)) {
	println "Error: only available in CQP memory mode"
	return;
}

@Field @Option(name="outputDir", usage="an example file", widget="Folder", required=true, def="")
		def outputDir

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

println "corpora selection: "+corpusViewSelection

MainCorpus maincorpus = corpusViewSelection.getMainCorpus()
File outputFile = new File(outputDir, corpusViewSelection.getQualifiedCqpId())
File regFile = new File(maincorpus.getProjectDirectory(), "registry")
if (!regFile.exists()) {
	println "Could not find corpus registry file $regFile"
	return
}

def outputStream = new FileOutputStream(outputFile);
LittleEndianOutputStream outputStreamBin = new LittleEndianOutputStream(outputStream);

outputStreamBin.writeInt(36193928+1);
outputStreamBin.writeBytes(regFile.getAbsolutePath()+"/");
outputStreamBin.write(0)
outputStreamBin.writeBytes(maincorpus.getID());
//outputStreamBin.write(0)
l2 = outputStreamBin.size()-3

//for (int i = 0; (i+l2)%4 != 0; i++)
outputStreamBin.writeInt(0);

def matches = corpusViewSelection.getMatches()
outputStreamBin.writeInt(matches.size())

for (def match : matches) {
	outputStreamBin.writeInt(match.getStart());
	outputStreamBin.writeInt(match.getEnd());
}

outputStreamBin.writeInt(0) // sortidx
outputStreamBin.writeInt(0) // targets
outputStreamBin.writeInt(0) // keywords
outputStreamBin.close()

println "Subcorpus saved in "+outputFile.getAbsolutePath()