package org.txm.macro.txt

import org.kohsuke.args4j.*

import groovy.transform.Field

import java.nio.charset.Charset

import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.*

import javax.xml.stream.*

import java.net.URL
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Field @Option(name="inputDirectory", usage="TXT directory", widget="Folder", required=false, def="txt")
File inputDirectory;
@Field @Option(name="regexp", usage="Regular expression to match", widget="String", required=false, def="p")
String regexp;
@Field @Option(name="encoding", usage="File encoding", widget="String", required=false, def="UTF-8")
String encoding;
@Field @Option(name="reverse", usage="Show the files not matching", widget="Boolean", required=false, def="false")
def reverse;
@Field @Option(name="recursive", usage="recurse in directories", widget="Boolean", required=false, def="false")
def recursive;
@Field @Option(name="filePathFilterRegExp", usage="Show the files not matching", widget="String", required=false, def=".*/MANIFEST.MF")
def filePathFilterRegExp;
@Field @Option(name="directoryPathFilterRegExp", usage="Show the files not matching", widget="String", required=false, def="")
def directoryPathFilterRegExp;

if (!ParametersDialog.open(this)) return;

p = /$regexp/

println "processing: "+inputDirectory

def processDir(def directory) {
	
for (File inputfile : directory.listFiles()) {

	if (inputfile.isDirectory()) {
		if (recursive) {
			if (directoryPathFilterRegExp != null && directoryPathFilterRegExp.length() > 0) {
				if (!inputfile.getAbsolutePath().matches(directoryPathFilterRegExp)) {
					continue // ignore since does not match
				}
			}
		
			processDir(inputfile)
		}
		continue // ignore
	}
	
	if (filePathFilterRegExp != null && filePathFilterRegExp.length() > 0) {
		if (!inputfile.getAbsolutePath().matches(filePathFilterRegExp)) {
			continue // ignore since does not match
		}
	}
	
	def lines = []
	inputfile.eachLine("UTF-8") { line, n ->
		def m = line =~ p
		if ( m.size() > 0) {
			lines << "  line $n: $line"
		}
	}
	
	if (reverse) {
		if (lines.size() == 0) {
			println inputfile.getAbsolutePath()
		}
	} else {
		if (lines.size() > 0) {
			println inputfile.getAbsolutePath() + " "+lines.size() + " match" +((lines.size() > 1)?"s":"")
			for (String s : lines ) println s
		}
	}
}
}

processDir(inputDirectory)