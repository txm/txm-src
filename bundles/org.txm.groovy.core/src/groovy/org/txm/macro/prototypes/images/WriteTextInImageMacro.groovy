// Copyright © 2025 ENS de Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.images

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

import java.awt.Font
import javax.imageio.ImageIO
import java.awt.Color
import java.awt.BasicStroke
import java.awt.RenderingHints

// BEGINNING OF PARAMETERS

@Field @Option(name="inputFile", usage="input image file", widget="FileOpen", required=true, def="")
def inputFile

@Field @Option(name="text", usage="text to write", widget="String", required=true, def="0.8.4")
def text

@Field @Option(name="x", usage="x coordinate", widget="Integer", required=true, def="160")
def x

@Field @Option(name="y", usage="x coordinate", widget="Integer", required=true, def="320")
def y

// 0.8.4 : 160, 320
// 0.8.4.1 : 145, 335
// 0.8.4 r1 : 130, 330

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

// from https://stackoverflow.com/questions/10929524/how-to-add-text-to-an-image-in-java

sname = inputFile.getName()
idx = sname.lastIndexOf(".")
if (idx > 0) {
  name = sname.substring(0, idx)
  ext = sname.substring(idx+1, sname.length())
}

outputFile = new File(inputFile.getParentFile(), name + "-$text." + ext)

image = ImageIO.read(inputFile)

g = image.getGraphics()

// Font.PLAIN
// Font.BOLD
g.setStroke(new BasicStroke(2f))
g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
// Couleur charte graphique TXM : vert #617b1f
// Couleur version Gimp #0f9934
//g.setColor(Color.decode("#0f9934"))
g.setColor(Color.decode("#617b1f"))
g.setFont(new Font("Lato Bold", Font.BOLD, 50))
g.drawString(text, x, y)
// bleu TXM #797da3
g.setColor(Color.decode("#797da3"))
g.setFont(new Font("Lato Bold", Font.BOLD, 17))
// subscript : 110, -25
// superscript : 109, 15
//g.drawString("BETA ①", x+43, y-282)
g.drawString("rev. 1", x+80, y-285)
g.dispose()

ImageIO.write(image, ext, outputFile)

