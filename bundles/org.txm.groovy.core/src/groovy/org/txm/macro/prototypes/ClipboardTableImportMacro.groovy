package org.txm.macro.prototypes

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.commands.*
import org.txm.objects.*
import org.txm.utils.io.*

@Field @Option(name="corpus_name", usage="Corpus name", widget="String", required=true, def="CLIPBOARD")
String corpus_name

if (!ParametersDialog.open(this)) return;

monitor.syncExec(new Runnable() {
	public void run() {
		try {
String result = "<text id=\"$corpus_name\">\n"+org.txm.rcp.utils.IOClipboard.read()+"</text>";
if (result == null || result.length() == 0) {
	System.out.println("No copied text. Aborting.");
	return;
}

def w = Toolbox.workspace
def p = w.getProject(corpus_name)

if (p != null) {
	println "** Corpus $corpus_name already exists. Aborting"
	return;
}

File tmp = new File(System.getProperty("java.io.tmpdir"), corpus_name)
tmp.mkdirs()

File tmp_text_file = new File(tmp, corpus_name+".cqp")
println "tmp_text_file=$tmp_text_file"
IOUtils.setText(tmp_text_file, result, "UTF-8")

p = new Project(w, corpus_name, true)
p.setLang("fr")
p.setEncoding("UTF-8")
p.setSourceDirectory(tmp.getAbsolutePath())
p.setAnnotate(true)
p.setCleanAfterBuild(true)
p.setDoMultiThread(true)
p.setImportModuleName("cqp")


			p.compute()
			
			RestartTXM.reloadViews();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
});