package org.txm.macro.nlp;
	
	import org.kohsuke.args4j.*
	import groovy.transform.Field
	import java.nio.charset.Charset
	import org.txm.rcpapplication.swt.widget.parameters.*
	import org.txm.utils.*
	import javax.xml.stream.*
	
	@Field @Option(name="inputDirectory", usage="TXT directory", widget="Folder", required=true, def="dir")
	File inputDirectory
	
	@Field @Option(name="encoding", usage="File encoding", widget="String", required=false, def="UTF-8")
	String encoding
	
	@Field @Option(name="debug", usage="Debug mode", widget="Boolean", required=false, def="false")
	Boolean debug
	
	if (!ParametersDialog.open(this)) return
	
	encoding = encoding.trim()
	outputDirectory = new File(inputDirectory, "xml")
	outputDirectory.mkdir()
	
	println "Processing: "+inputDirectory
	
	XMLOutputFactory factory = XMLOutputFactory.newInstance()
	
	def files = inputDirectory.listFiles().sort{ it.name }
	if (files == null || files.length == 0) {
		println "Error: no file to process in $inputDirectory"
		return false;
	}
	for (File inputfile : files.sort()) {
		if (inputfile.isDirectory() || inputfile.isHidden() || !inputfile.getName().endsWith(".tt")) continue // ignore
		println " file: "+inputfile
	
		name = inputfile.getName()
		idx = name.lastIndexOf(".")
		if (idx > 0) name = name.substring(0, idx)
		outputfile = new File(outputDirectory, name+".xml")
		
		Writer output = new OutputStreamWriter(new FileOutputStream(outputfile) , "UTF-8")
		XMLStreamWriter writer = factory.createXMLStreamWriter(output)
	
		writer.writeStartDocument("UTF-8","1.0")
		writer.writeCharacters("\n")
		writer.writeStartElement("text")
		writer.writeCharacters("\n")
	
		inSent = 0
		nLine = 0
		inputfile.eachLine(encoding) { line ->
		
			nLine++
			
			if (debug) {
				println nLine+": "+line
			}
			
			def sent = false
	        def sent_pattern = ~/^([^\t]+)\t(SENT)\t([^\t]+)$/
	        def sent_m = (line =~ sent_pattern)
	        if (sent_m) sent = true
	        	
			if (sent && inSent) {
				writer.writeStartElement("w")
				writer.writeAttribute("frpos", sent_m[0][2])
				writer.writeAttribute("frlemma", sent_m[0][3])
				writer.writeCharacters(sent_m[0][3])
				writer.writeEndElement() // close w		
				writer.writeComment("\n")
				writer.writeEndElement() // close s
				writer.writeEndElement() // close p (sent)
				writer.writeCharacters("\n")
				inSent=0
			} else {
				if (!inSent) {
					writer.writeStartElement("p")
					writer.writeStartElement("s")
					inSent=1
				}
				def w_pattern = ~/^([^\t]+)\t([^\t]+)\t([^\t]+)$/
	        	def w_m = (line =~ w_pattern)
	
	        	if (w_m.size() > 0) {
	        		writer.writeStartElement("w")
					writer.writeAttribute("frpos", w_m[0][2])
					writer.writeAttribute("frlemma", w_m[0][3])
					writer.writeCharacters(w_m[0][1])
					writer.writeEndElement() // close w
					writer.writeCharacters("\n")
				} else {
				    writer.writeStartElement("w")
					writer.writeAttribute("frpos", "NAM")
					writer.writeAttribute("frlemma", "<unknown>")
					writer.writeCharacters(line)
					writer.writeEndElement() // close w
					writer.writeCharacters("\n")
					println "** TT2XMLInDirectory: unknown [word] line pattern, "+nLine+": <"+line+">."
				}
			}
		}
	
		if (inSent) {
			writer.writeEndElement() // s
			writer.writeEndElement() // p
			writer.writeCharacters("\n")
		}
		writer.writeEndElement()
		writer.writeCharacters("\n")
		writer.close()
		output.close()
	}
	

