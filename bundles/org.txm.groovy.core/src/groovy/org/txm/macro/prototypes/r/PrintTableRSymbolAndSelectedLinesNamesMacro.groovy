package org.txm.macro.r;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox
import org.txm.statsengine.r.core.RWorkspace
import org.txm.statsengine.r.core.RResult
import org.txm.core.results.TXMResult

if (editor == null) {
	println "No active editor. Aborting"
	return
}

if (!(editor instanceof org.txm.lexicaltable.rcp.editors.LexicalTableEditor)) {
	println "Active editor is not a table lexical editor ($editor). Aborting"
	return
}

def table = editor.getResult()
println "R name is: "+table.getRSymbol()

monitor.syncExec(new Runnable() {
	public void run() {
		def selection = editor.getTableViewer().getSelection()
		def names = table.getRowNames().asStringsArray()
		println "Selection: "+selection.toList()
		println "Names: "+names[selection.toList()]
	}
});

