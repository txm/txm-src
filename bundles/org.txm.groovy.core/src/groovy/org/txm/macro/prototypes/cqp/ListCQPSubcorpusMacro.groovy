// STANDARD DECLARATIONS
package org.txm.macro.prototypes.cqp

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.utils.*
import org.txm.searchengine.cqp.CQPSearchEngine

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Selection is not a CQPCorpus: $corpusViewSelection"
	return false
}

def CQI = CQPSearchEngine.getCqiClient()
if ((CQI instanceof NetCqiClient)) {
	println "Error: only available in CQP memory mode"
	return;
}

@Field @Option(name="subcorpusname", usage="an example file", widget="String", required=true, def="A")
def subcorpusname
@Field @Option(name="inputFile", usage="an example file", widget="FileOpen", required=true, def="")
def inputFile
@Field @Option(name="replace", usage="an example file", widget="Boolean", required=true, def="true")
def replace

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

println "corpora selection: "+corpusViewSelection

MainCorpus maincorpus = corpusViewSelection.getMainCorpus()
def existing = CQI.listSubcorpora(maincorpus.getQualifiedCqpId())
println existing
