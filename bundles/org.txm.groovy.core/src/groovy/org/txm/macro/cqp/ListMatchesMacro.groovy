// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.cqp

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.utils.*


@Field @Option(name="maxMatches", usage="maximum number of intervals to list", widget="Integer", required=true, def="1000")
def maxMatches

@Field @Option(name="pretty", usage="pretty print intervals list", widget="Boolean", required=true, def="true")
def pretty

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return false

def CQI = CQPSearchEngine.getCqiClient()

def scriptName = this.class.getSimpleName()

def utils = new org.txm.macro.urs.exploit.CQPUtils()

def corpora = utils.getCorpora(this)

if ((corpora == null) || corpora.size() == 0) {
	println "** $scriptName: please select a corpus in the Corpus view or provide a corpus name. Aborting."
	return false
}

corpora.each { corpus ->
	
	corpus.compute(false)
	println corpus.getName()+" ("+corpus.getMatches().size()+") : "+utils.corpus2positions(corpus, maxMatches, pretty)
}
