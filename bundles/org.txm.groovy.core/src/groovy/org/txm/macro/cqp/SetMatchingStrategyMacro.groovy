package org.txm.macro.cqp

// STANDARD DECLARATIONS
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.searchengine.cqp.*

def current = System.getProperty("cqp_matching_strategy")
if (current != null)
	println "Current strategy is: $current"

def CQI = CQPSearchEngine.getCqiClient();
if ((CQI instanceof NetCqiClient)) {
	println "Error: only available in CQP memory mode"
	return;
}

// BEGINNING OF PARAMETERS
@Field @Option(name="matchingStrategy", usage="CQL matching strategy (shortest | standard | longest | traditional)", widget="String", required=false, def="shortest | standard | longest | traditional")
def matchingStrategy 
// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS

matchingStrategy = matchingStrategy.trim().toLowerCase();
switch(matchingStrategy ) {
	case "shortest":
	case "standard":
	case "longest":
	case "traditional":
		println "Setting Greedy to $matchingStrategy "
		CQI.query("set MatchingStrategy ${matchingStrategy };")
		System.setProperty("cqp_matching_strategy",matchingStrategy ) 
	break
	case "shortest | standard | longest | traditional":
		println "You must choose between shortest, standard, longest or traditional"
	break
	default:
		println "Unknown matching strategy: ${matchingStrategy}. Aborting "
}