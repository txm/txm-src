// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.cqp

/*
This macro allows you to define CQL lists which you can call in CQP queries during the current session.
To use this macro for your own lists,
1. Find below an example of the type of list you want to create
   (case 1 = explicit list, or case 2 = list from file),
2. Copy then paste this example (5 lines) after the given examples
3. Give the name for your list as a string after list_name=
4. Give the content of the list as a string after
either list_value= (for case 1, explicit list)
or list_path= (for case 2, list from file).
5. Save this file (macro code)
6. Run the macro by double-clicking on it.
The lists are defined only for the current session.
To use the same lists in a new session, just run the macro to generate the lists again.
*/

import org.txm.Toolbox
import org.txm.searchengine.cqp.*

def CQI = CQPSearchEngine.getCqiClient();
if ((CQI instanceof NetCqiClient)) {
	println "Error: only available in CQP memory mode"
	return;
}

def list_name=""
def list_value=""
def list_path=""
def cqp_value=""
File f;
try {

// BEGIN CONFIGURATION HERE

// 1st case : explicit lists
list_name = "negatif"
list_value = "rien non ne pas sans"

cqp_statement = "define \$$list_name = \"$list_value\";"
println "CQP executes : $cqp_statement"
CQI.query(cqp_statement)

// two other examples of 1st case (explicit list)
// 1st explicit list
list_name = "evaluation"
list_value = "trop assez peu bien très beaucoup vraiment"

cqp_statement = "define \$$list_name = \"$list_value\";"
println "CQP executes : $cqp_statement"
CQI.query(cqp_statement)

// 2nd explicit list
list_name = "categoriesmotsvides"
list_value = "ABR ADV DET:ART DET:POS INT KON NUM PRO PRO:DEM PRO:IND PRO:PER PRO:POS PRO:REL PRP PRP:det PUN PUN:cit SENT SYM"

cqp_statement = "define \$$list_name = \"$list_value\";"
println "CQP executes : $cqp_statement"
CQI.query(cqp_statement)

// one example of regular expression list to be used with RE()
list_name = "regexp_categoriesmotsvides"
list_value = "ABR ADV DET.* INT KON NUM PRO.* PRP.* PUN.* SENT SYM"

cqp_statement = "define \$$list_name = \"$list_value\";"
println "CQP executes : $cqp_statement"
CQI.query(cqp_statement)

// 2nd case : list from a file

list_name = "motsvides"
list_path = System.getProperty("user.home")+"/TXM/cqp/listes/stopwords_fr_ranknl.txt"
// Windows users must replace "/" with "\\". 
// for instance : 
// list_path = System.getProperty("user.home")+"\\TXM\\cqp\\listes\\stopwords_fr_ranknl.txt"
f = new File(list_path);
if (f.exists()) {
	cqp_statement = "define \$$list_name < \"$list_path\";"
	println "CQP executes : $cqp_statement"
	CQI.query(cqp_statement)
} else {
	println "Error: \"$f\" file does not exists"
}

/*
You can comment a group of several lines of code
using stars and slash like this.
*/

// END OF CONFIGURATION

} catch (Exception e) {
	println "Error during the \"$list_name\" list creation with the \"$list_value\" value"
	println "CPQ statement was \"$cqp_statement\""
	if (f != null) println "Last CQL file path was \"$f\""
}
