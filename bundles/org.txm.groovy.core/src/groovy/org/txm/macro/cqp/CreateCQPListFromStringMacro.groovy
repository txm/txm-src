// Copyright © 2021 ENS de Lyon
// Licensed under the terms of the GNU General Public License v3.0 (http://www.gnu.org/licenses)
// @author sheiden

package org.txm.macro.cqp

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

import org.txm.Toolbox
import org.txm.searchengine.cqp.*

// parameters

@Field @Option(name="wordList", usage="list of words", widget="Text", required=true, def='')
def wordList

@Field @Option(name="separator", usage="character separating words", widget="String", required=true, def=',')
def separator

@Field @Option(name="name", usage="name of the list", widget="String", required=true, def='')
def name

if (!ParametersDialog.open(this)) return

def CQI = CQPSearchEngine.getCqiClient()
def cqp_statement

if ((CQI instanceof NetCqiClient)) {

	println "** Error: only available in CQP memory mode"
	return
}

try {
	wordList = wordList.split(separator).join(' ')
	cqp_statement = "define \$$name = \"$wordList\";"
	println "Executing CQP statement... $cqp_statement"
	CQI.query(cqp_statement)
	println "\$$name word list defined. You can now use it in queries, like: [word=\$$name]"

} catch (Exception e) {
	println "** Error during the creation of word list \"$name\" with the list \"$wordList\""
	println "CPQ statement was \"$cqp_statement\""
}
