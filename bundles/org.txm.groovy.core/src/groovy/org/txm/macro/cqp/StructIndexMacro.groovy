// Copyright © 2023 ENS de Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.cqp

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.macro.cqp.CQPUtils
import org.txm.searchengine.cqp.CQPSearchEngine

scriptName = this.class.getSimpleName()

// BEGINNING OF PARAMETERS

@Field @Option(name="struct_props", usage="struct@prop1,prop2...", widget="String", required=true, def="")
def struct_props

@Field @Option(name="Vmax", usage="maximum number of values displayed", widget="Integer", required=true, def="100")
def Vmax

@Field @Option(name="groupByValue", usage="group results by value before counting", widget="Boolean", required=false, def="true")
def groupByValue

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

		// END OF PARAMETERS

		utils = new CQPUtils()
		corpusEngine = CQPSearchEngine.getCqiClient()

		// check for a corpus selection
		corpora = utils.getCorpora(this)

		if ((corpora == null) || corpora.size() > 1) {
			println "** $scriptName: please select a corpus in the Corpus view or provide a corpus name. Aborting."
			return false
		}

if ((struct_props == null) || struct_props.size() == 0) {
	println "** $scriptName: please set the 'struct_props' parameter. Aborting."
	return false
}

corpus = corpora[0].getMainCorpus().getName()

globalVmax = Vmax

(struct, prop) = struct_props.tokenize('@')*.trim()

if ((prop == null) || prop.size() == 0) {
	println "** $scriptName: please set @properties in the 'struct_props' parameter."
} else {

	if (prop.indexOf(',') == -1) {
		prop = [prop]
	} else {
		prop = prop.tokenize(',')*.trim()
	}

	occs = (0..corpusEngine.attributeSize("$corpus.${struct}_${prop[0]}")-1).collect { cpos ->
	prop.collect { prop ->
	corpusEngine.struc2Str("$corpus.${struct}_$prop", [cpos] as int[])[0]
	}.join('_')
	}

	if (groupByValue) {
		occs = occs.sort().countBy { it }.sort { a,b -> b.value <=> a.value ?: a.key <=> b.key }
		if (globalVmax == 0) {
			occs.each { println sprintf("%s\t%d", it.key, it.value) }
		} else {
			occs.take(globalVmax).each { println sprintf("%s\t%d", it.key, it.value) }
		}
	} else {
		if (globalVmax == 0) {
			occs.each { println it }
		} else {
			occs.take(globalVmax).each { println it }
		}
	}
}

