package org.txm.macro.cqp

import org.txm.macro.projects.antract.OkapiSaphirAPI
import org.txm.searchengine.cqp.CQPSearchEngine

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.Subcorpus
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.rcp.views.corpora.CorporaView

@Field @Option(name="values", usage="Liste des valeurs séparés par des '|', ' ', une tabulation ou un saut de ligne", widget="Text", required=true, def="")
def values

if (!ParametersDialog.open(this)) return

values = values.replace("\n", "|").replace("\t", "|").replace(" ", "|").replaceAll("(\\|)++", "|").trim()

println "RESULT:"
println getShortCQL(values.split("\\|") as List)


def getShortCQL(def list) {
	//list.sort()
	def origtree = ["":list]
	breakNode(origtree, "", 0)
	def CQL1 = list.join("|")
	def CQL2 = writeNode(origtree)
	//CQL2 = CQL2.replaceAll("\\(\\[()\\]\\)", "")
	
	
	if (CQL1.length() < CQL2.length()) {
		return CQL1;
	} else {
		CQL2 = CQL2.replace("(\\d)", "\\d")
		CQL2 = CQL2.replaceAll("\\(\\[([0-9])-([0-9])\\]\\)", "[\$1-\$2]") // ([0-9]) -> [0-9]
		CQL2 = CQL2.replaceAll("\\(([0-9])\\|([0-9])\\)", "[\$1\$2]") // (1|2) -> [12]
		return CQL2;
	}
}

def breakNode(def tree, def key, def p) {
	def space = (" "*p)
	//println space + "breaking: $key of $tree at $p"
		
	def children = new LinkedHashMap()
	def ids = tree[key]
	tree[key] = children
	
	for (def child : ids) {
		//println space+"child="+child
		if (child.length() <= p) continue;
		
		def letter = child[p]
		if (!children.containsKey(letter)) {
			children[letter] = []
		}
		
		children[letter].add(child)
	}
	
	for (def k : children.keySet()) {
		breakNode(children, k , p+1)
	}
}


def writeNode(def tree) {
	def s = ""
	//println "tree: "+tree

	int n = 0;
	if (tree.size() > 1) s += "("
	String ss = ""
	for (def child : tree.keySet()) {
		if (n > 0) ss += "|"
		ss += child
		ss += writeNode(tree[child])
		n++
	}
	
	if (ss.matches("([0-9]\\|){2,10}[0-9]")) ss = "["+ss.replace("|", "") +"]"
	if (ss == "[0123456789]") ss = "\\d"
	if (ss == "[012345678]") ss = "[0-8]"
	if (ss == "[01234567]") ss = "[0-7]"
	if (ss == "[0123456]") ss = "[0-6]"
	if (ss == "[012345]") ss = "[0-5]"
	if (ss == "[123456789]") ss = "[1-9]"
	if (ss == "[12345678]") ss = "[1-8]"
	if (ss == "[1234567]") ss = "[1-7]"
	if (ss == "[123456]") ss = "[1-6]"
	if (ss == "[12345]") ss = "[1-5]"
	if (ss == "[23456789]") ss = "[2-9]"
	if (ss == "[2345678]") ss = "[2-8]"
	if (ss == "[3456789]") ss = "[3-9]"
	if (ss == "[345678]") ss = "[3-8]"
	if (ss == "[456789]") ss = "[4-9]"
	if (ss == "[45678]") ss = "[4-8]"
	
	if (ss == "[012345689]") ss = "[0-689]"
	if (ss == "[012356789]") ss = "[0-35-9]";
	if (ss == "[012456789]") ss = "[0-24-9]";
	s += ss
	
	if (tree.size() > 1) s += ")"
	return s
}