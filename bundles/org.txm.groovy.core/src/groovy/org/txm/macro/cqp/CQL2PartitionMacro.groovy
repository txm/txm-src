// Copyright © 2022 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

package org.txm.macro.commands

// STANDARD DECLARATIONS
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.rcp.views.corpora.CorporaView

@Field @Option(name="partitionName", usage="partition name", widget="String", required=true, def="partition1")
def partitionName

@Field @Option(name="partNames", usage="one name per line", widget="Text", required=true, def="")
def partNames

/*
4-GEN-referenced-verses
6-RNT-referenced-verses
7-DOT-referenced-verses
8-KJB-referenced-verses
*/

@Field @Option(name="queries", usage="one query per line", widget="Text", required=true, def="")
def queries

/*
undump "/home/sheiden/Documents/projet-dth/références-bibliques/matches-2022-06-15/4-GEN-cqp-matches.tsv";
undump "/home/sheiden/Documents/projet-dth/références-bibliques/matches-2022-06-15/6-RNT-cqp-matches.tsv";
undump "/home/sheiden/Documents/projet-dth/références-bibliques/matches-2022-06-15/7-DOT-cqp-matches.tsv";
undump "/home/sheiden/Documents/projet-dth/références-bibliques/matches-2022-06-15/8-KJB-cqp-matches.tsv";
*/

def scriptName = this.class.getSimpleName()

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return false

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName: please select a Corpus in the Corpus view."
	return 0
}

corpus = corpusViewSelection

// end of PARAMETERS

// BODY

partNames = partNames.tokenize('\n')
queries = queries.tokenize('\n')

if (queries.size() == partNames.size()) {

	def partition
	
	println "Building partition on $corpus (size = "+(corpus.getSize())+")."
	
	partition = new Partition(corpus)
	partition.setParameters(partitionName, queries, partNames)
	partition.compute()
	
	def list = Arrays.asList(partition.getPartSizes())

	println "'$partitionName' partition created, parts sizes = "+list+", Σ(parts) = "+list.sum()+"."

	// println "Total size: "+list.sum()+" - is equal to (sub)corpus size : "+(list.sum() == partition.getCorpus().getSize())
	
	monitor.syncExec(new Runnable() {
	    public void run() {
		CorporaView.refresh()
		CorporaView.expand(partition.getParent())
	    }
        })
} else {
	println "** $scriptName: partNames.size() != queries.size(), ("+partNames.size()+" != "+queries.size()+")."
	return 0
}

