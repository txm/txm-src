// Copyright © 2022 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

package org.txm.macro.commands

// STANDARD DECLARATIONS
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.CQPPreferences
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.importer.cwb.CompressCQPIndexes
import org.txm.libs.cqp.CQPLibPreferences

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return false

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "** $scriptName: please select a MainCorpus in the Corpus view."
	return 0
}

MainCorpus corpus = corpusViewSelection

File registry = corpus.getRegistryFile()
File data = corpus.getDataDirectory()

File wordCorpusFile = new File(data, "word.corpus")
if (!wordCorpusFile.exists()) {
	println "Corpus already compressed or broken"
	return
}

try {
			String userdir = System.getProperty("user.home");
			String cqilib = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB);
			String exec_path = CQPLibPreferences.getInstance().getString(CQPLibPreferences.CQI_SERVER_PATH_TO_EXECUTABLE);
			File toolsDir = null;
			if (!cqilib.isEmpty()) {
				toolsDir = new File(cqilib)
				if (!toolsDir.exists()) {
					toolsDir = null;
				}
			}
			
			if (!exec_path.isEmpty()) {
				toolsDir = new File(exec_path).getParentFile()
				if (!toolsDir.exists()) {
					toolsDir = null;
				}
			}
			
			if (toolsDir == null) {
				println "Could not findout the CQP tools directory. Aboorting."
				return;
			}
			
			
			
			CompressCQPIndexes.compressAll(toolsDir, registry, corpus.getProject().getName(), data, true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}