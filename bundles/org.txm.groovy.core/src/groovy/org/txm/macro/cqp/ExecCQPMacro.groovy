package org.txm.macro.cqp
// Copyright © 2014 ENS de Lyon
//
// Authors:
// - Matthieu Decorde
// - Serge Heiden
//
// Licence:
// This file is part of the TXM platform.
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
// Version:
// $LastChangedDate: 2014-11-01 16:00:01 +0100 (sam., 1 nov. 2014) $
// $LastChangedRevision: XXXX $
// $LastChangedBy: sheiden $
//

//Title:
// EN:
// Macro to call directly internal CQP corpus engine statements
//
// Warning: This macro must be used by advanced users,
// at the risk of compromising CQP engine stability for the current TXM session
//
//FR:
// Macro de demande d'exécution directe d'instructions au moteur de corpus interne CQP
//
// Avertissement : Cette macro doit être utilisée par des utilisateurs avertis,
// au risque de compromettre la stabilité du moteur CQP pour la session TXM courante

// Necessary declarations - Déclarations préalables
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.searchengine.cqp.*

// Sanity check - Vérification de cohérence
def CQI = CQPSearchEngine.getCqiClient();
if ((CQI instanceof NetCqiClient)) {
	println "Error: CQP eval only available in CQP memory mode"
	return;
}

// Parameter declaration - Déclaration du paramètre
@Field @Option(name="statement", usage="CQP statement to execute (mind the ';' at the end)", widget="String", required=true, def="set da enpos;")
String statement;

// Usefull CQP statements - Instructions CQP Utiles //
//
// 1] Set a CQP variable - régler une variable CQP :
// "set variable valeur;"
//
// Useful CQP variables
// A) da = DefaultNonbrackAttr variable setting: the implicit word property in subsequent CQL queries
//    For example, when DefaultNonbrackAttr=word, the "the" CQL query is interpreted by CQP as the '[word="the"]' query
// Example Statements:
// - set da word;     set implicit word property to "word" ('word' meaning graphical form of the word)
// - set da enlemma;	set implicit word property to "enlemma" ('enlemma' meaning TreeTagger based 'En'glish 'Lemma')
// - set da enpos;	set implicit word property to "enpos" ('enpos' meaning TreeTagger based 'En'glish 'P'art 'O'f 'S'peech)
//
// B) ms = MatchingStrategy variable setting: the CQL query word level iterators (?, *, +) resolution strategy used by CQP
// Possible Statements:
// - set ms shortest: ?, *, + word level operators should match sequences with the minimal number of words.
//                    Optional words at the begining or at the end of the query are ignored.
// - set ms standard: early match of ?, *, + word level operators are considered.
//                    Optional words at the end of the query are ignored.
// - set ms longest: ?, *, + word level operators should match sequences with the maximum number of words.
// - set ms traditional: early match of ?, *, + word level operators are considered. Every sequence is returned.
//
// Example resolutions for each MatchingStrategy type 
// (from The CQP Query Language Tutorial, (CWB version 2.2.b90), Stefan Evert, 10 July 2005)
//
// for the query:
// [enpos="DET"]? [enpos="ADJ"]* [enpos="NN"] ([enpos="PREP"] [enpos="DET"]? [enpos="ADJ"]* [enpos="NN"])*
//
// Input:
//     the old book on the table in the room
//
// - 'shortest' match strategy: (3 matches)
// r1=         book
// r2=                     table
// r3=                                  room
//
// - 'longest' match strategy: (1 match)
// r1= the old book on the table in the room
//
// - 'standard' matching strategy: (3 matches)
// r1= the old book
// r2=                 the table
// r3=                              the room
//
// - 'traditional' matching strategy: (7 overlapping matches)
// r1= the old book
// r2=     old book
// r3=         book
// r4=                 the table
// r5=                     table
// r6=                              the room
// r7=                                  room
//
// C) sr = StrictRegions variable setting: CQL queries must match a single structure
// Possible Statements:
// - set sr on: ?, *, + word level operators cannot cross a boundary of the structure involved in the query
// - set sr off: ?, *, + word level operators can cross any number of boundaries of the structure involved in the query

// Parameters settings UI
if (!ParametersDialog.open(this)) {
	println("** ExecCQLMacro error: Impossible to open Parameters settings UI dialog box.")
	return
}

// Actual CQP call
CQI.query(statement)

// End of the macro

/////////////////////////////////////
//// CQP variables documentation ////
/////////////////////////////////////

//// Useful CQP variables ///
// [da]	DefaultNonbrackAttr - implicit word property in queries ('word' by default)
// [ms]	MatchingStrategy    - shortest | standard | longest | traditional
// [sr]	StrictRegions       - single structure match

//// Maybe useful variables ////
// [o]	Optimize - simple regular expressions infixe optimizer
// [hf]	HistoryFile
// [wh]	WriteHistory

//// Useless variables ////
// [p]	 Paging
// [pg]	 Pager
// [h]	 Highlighting
// [col] Colour
// [pb]	 ProgressBar
// [pp]	 PrettyPrint
// [c]	 Context
// [lc]	 LeftContext
// [rc]	 RightContext
// [ld]	 LeftKWICDelim
// [rd]	 RightKWICDelim
// [pm]	 PrintMode
// [po]	 PrintOptions
// [ps]	 PrintStructures
// [sta] ShowTagAttributes
// [st]	 ShowTargets
// [as]	 AutoShow
// [es]	 ExternalSort
// [esc] ExternalSortCommand
// 	     AutoSave
// 	     SaveOnExit
// 	     Timing

//// Variables that must not be changed ////
// [r]	 Registry
// [dd]	 DataDirectory
// [sub] AutoSubquery
