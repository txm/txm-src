package org.txm.macro.urs.democrat;

	// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
	// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
	// @author mdecorde
	// @author sheiden
	
	
	import org.kohsuke.args4j.*
	import groovy.transform.Field
	import org.txm.rcpapplication.swt.widget.parameters.*
	import org.txm.searchengine.cqp.*
	import org.txm.searchengine.cqp.corpus.*
	import org.txm.searchengine.cqp.corpus.query.*
	import org.txm.*
	import org.txm.rcpapplication.views.*
	
	// BEGINNING OF PARAMETERS
	
	// Declare each parameter here
	// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)
	
	@Field @Option(name="name", usage="name of the subcorpus result", widget="String", required=true, def="subcorpus1")
	
	def name
	
	@Field @Option(name="positions", usage="literal corpus position intervals list (aka 'matches list') to build subcorpus (eg '0,1,2-3,4-5,6')", widget="String", required=true, def="0,1,2-3,4-5,6")
	def positions
	
	// Open the parameters input dialog box
	if (!ParametersDialog.open(this)) return false
	
	def CQI = CQPSearchEngine.getCqiClient()
	
	def scriptName = this.class.getSimpleName()
	
	def utils = new org.txm.macro.urs.exploit.CQPUtils()
	
	if ((positions == null) || positions.length() == 0) {
		println "** $scriptName: please provide a 'positions' interval list. Aborting."
		return false
	}
	
	query = utils.positions2cql(positions)
	
	if (query.length() == 0) {
		println "** $scriptName: impossible to interpret the 'positions' value. Incorrect syntax? Aborting."
		return false
	}

	def corpora = utils.getCorpora(this)
	
	if ((corpora == null) || corpora.size() == 0) {
		println "** $scriptName: please select a corpus in the Corpus view or provide a corpus name. Aborting."
		return false
	}
	
	/*
	CorpusManager.getCorpusManager().getCorpora().each {
		if (it.getSubcorpusByName(name)) {
			println "** Subcorpus: sub-corpus name already used. Please use another sub-corpus name. Aborting."
			return false
		}
		// it.getSubcorpora().each { sub -> println it.getName()+" subcorpora: "+sub.getName() }
	}
	*/
	
	String resultCqpId = CqpObject.subcorpusNamePrefix + CQPCorpus.getNextSubcorpusCounter()
	String queryString = "$resultCqpId=$query;"
	
	CQI.query(queryString)
	
	def subcorpus = corpora[0].createSubcorpus(new CQLQuery("$query;"), name)
	
	if (binding.variables["monitor"]) {
		utils.refreshCorpusViewExpand(monitor, subcorpus.getParent())
	}
	
	return true


