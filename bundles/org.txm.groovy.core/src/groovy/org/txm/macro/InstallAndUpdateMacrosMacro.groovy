import org.txm.Toolbox
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.io.IOUtils

def groovyUserDirectory = new File(Toolbox.getTxmHomePath() + "/scripts/groovy/user/org/txm/macro/");

if (!groovyUserDirectory.exists()) {
	groovyUserDirectory.mkdirs();
}

if (!groovyUserDirectory.exists()) {
	monitorShowError("Le dossier $groovyUserDirectory n'a pas pu être créé. Abandon.")
	return;
}
@Field @Option(name="macroPaths", usage="Comma separated list of macro paths", widget="String", required=true, def="/")
def macroPaths

@Field @Option(name="sourcesRepository", usage="http based URL of TXM's SVN ", widget="String", required=true, def="https://gitlab.huma-num.fr/txm/txm-src/-/raw/main/bundles/org.txm.groovy.core/src/groovy/org/txm/macro/")
def sourcesRepository

@Field @Option(name="debug", usage="Show debug messages", widget="Boolean", required=false, def="false")
def debug

if (!ParametersDialog.open(this)) return false;

if (!sourcesRepository.endsWith("/")) sourcesRepository += "/"

if (macroPaths == null) macroPaths = ""
def macros = macroPaths.split(",").collect(){it.trim()}
def copy = []
copy.addAll(macros)
macros.clear()
for (def m : copy) {
	macros.addAll(getFiles(sourcesRepository, m, debug))
}


print "Fetching ${macros.size()} files: "
def errors = new LinkedHashMap()
def updated = []
def news = []
for (def m : macros) {
	def f = new File(groovyUserDirectory, m)
	f.getParentFile().mkdirs()
	String remoteUrl = sourcesRepository+m
	try {
		
		if (debug) print "\n"+remoteUrl
		boolean replaced = f.exists()
		
		def file = new FileOutputStream(f)
		def out = new BufferedOutputStream(file)
		out << new URL(remoteUrl).openStream()
		out.close()
		
		if (replaced) {
			print "*"
			updated << m
		} else {
			print "."
			news << m
		}
	} catch(Exception e ) {
		print "x"
		errors[m] = e;
	}
}

if (news.size() > 0 ) {
	println "\nFiles added: ${news.join(", ")}."
}

if (updated.size() > 0 ) {
	println "\nFiles updated: ${updated.join(", ")}."
}

if (errors.size() > 0) {
	println "\nFiles not updated: ${errors.keySet().join(", ")}:"
	println errors.values().join("\n\t")
}

def getFiles(def sourcesRepository, def m, def debug) {
	if (debug) println "Looking for files in $m"
	def files = []
	try {
		
		//if (debug) println content
		if (m.endsWith("/")) { //  || content.startsWith("<html><head><title>txm") // nag alternative
			if (!m.endsWith("/")) m += "/"
			
			def content = new URL(sourcesRepository+m).getText()
			
			content.findAll(/<li><a href="([^"]+)">([^<]+)<\/a><\/li>/) { li ->
				
						def name =  li[2]
						if (name != "..") {
							files.addAll(getFiles(sourcesRepository, m +name, debug))
							if (debug) println "add the '$name' file from '$m' content"
						}
					}
		} else {
			files << m
		}
		
	} catch(Exception e) {
		println "** Error: "+e.getMessage()
		println "   For address "+sourcesRepository+m
	}
	
	return files
}

def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Error", message)
		}
	});
}

