// STANDARD DECLARATIONS
package org.txm.macro.hyperlink

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.edition.rcp.handlers.OpenEdition
import org.txm.edition.rcp.editors.RGBA

/**
 * The macro use the "stringArgs" Groovy Binding to work. The format is: parameter=value + TAB + parameter2=value2 ...
 * 
 * Parameters needed:
 * - corpus: corpus id
 * - text: text id to display
 * - page: page id to display
 * - editions: optionnal edition ids to display ("default" is used if not set)
 * - wordsids: optionnal word ids to highlight&focus
 * 
 * This macro can be called from whithin TXM editions:
 * 
 * <a onclick="txmcommand('id', 'org.txm.rcp.commands.ExecuteMacro', 'org.txm.rcp.command.parameter.file', 'org/txm/macro/EditionHyperlinkMacro.groovy', 'args' , 'corpus=VOEUX    text=0002    page=3    editions=default')">Open Edition with text+page</a>.
 * 
 * <a onclick="txmcommand('id', 'org.txm.rcp.commands.ExecuteMacro', 'org.txm.rcp.command.parameter.file', 'org/txm/macro/EditionHyperlinkMacro.groovy', 'args' , 'corpus=VOEUX    text=0002    wordids=w_0002_6    editions=default')">Open Edition with text+wordid</a>.
 * 
 * <a onclick="txmcommand('id', 'org.txm.rcp.commands.ExecuteMacro', 'org.txm.rcp.command.parameter.file', 'org/txm/macro/EditionHyperlinkMacro.groovy', 'args' , 'corpus=VOEUX    text=0002    wordids=w_0002_6,w_0002_7,w_0002_8,w_0002_9    editions=default')">Open Edition with text+wordids</a>.
 */


if (stringArgs == null) {
	println "** Error: this macro must be called from an edition hyperlink"
	return
}

def params = stringArgs.split("\t")
def hash = [:]
for (def param : params) {
	def split = param.split("=", 2)
	hash[split[0]] = split[1]
}
corpus = hash["corpus"]
text = hash["text"]
page = hash["page"]
editions = hash["editions"]
if (editions != null) editions = editions.split(",") as List
wordids = hash["wordids"]
if (wordids != null) wordids = wordids.split(",") as List

println "corpus=$corpus editions=$editions text=$text page=$page wordids=$wordids"
corpus = CorpusManager.getCorpusManager().getCorpora()[corpus]

monitor.syncExec(new Runnable() {
	public void run() {
		editor = OpenEdition.openEdition(corpus, editions)
		if (wordids != null && wordids.size() > 0) {
			try {
				editor.backToText(corpus.getProject().getText(text), wordids[0])
				
				editor.removeHighlightWords()
				editor.addHighlightWordsById(new RGBA(249, 208, 208), wordids)
				editor.updateWordStyles()
			}catch(Exception e) { e.printStackTrace()}
		} else {
			editor.goToText(text)
			editor.goToPage(page)
		}
	}
});

