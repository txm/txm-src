// Copyright © 2024 ENS de Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.utils

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
// import static java.lang.Math.*

// BEGINNING OF PARAMETERS

@Field @Option(name="expression", usage="Expression combinant des nombres et des opérations, par exemple : 10194/1711112\nSyntaxe :\n- expression : terme | expression '+' terme | expression '-'  terme ;\n-  terme :  facteur|  terme'*'  facteur|  terme'/'  facteur|  terme'%'  facteur ;\n-  facteur :  élémentaire | '-'  facteur| '+'  facteur ;\n-  élémentaire : IDENTIFIANT | ENTIER | FLOTTANT | '(' expression ')' ;\nOn peut également combiner avec des constantes et des fonctions Java, par exemple :\nMath.PI = 3.141592653589793\nMath.cos(Math.PI/3) = 0.5000000000000001\nVoir les deux constantes et les 73 fonctions disponibles à https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html", widget="Text", required=true, def="10194/1711112")
		def expression

@Field @Option(name="notation_exponent", usage="Affichage du résultat en notation exponentielle", widget="Boolean", required=false, def="false")
		def notation_exponent

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

	// END OF PARAMETERS

	groovy = new GroovyShell(binding)

try {
	expression = expression.trim()
	v = groovy.evaluate("import static java.lang.Math.*\n"+expression) // import all static methods of the java.lan.Math class
	vs = v.toString()
	if (vs.isInteger()) {
		println expression+" = "+v+" ("+java.lang.String.format("%,d", v)+", in French format)"
	} else {
		if (notation_exponent) {
			println expression+" = "+sprintf("%e", v)
		} else {
			comment = ""
			if (Locale.getDefault().getLanguage() != "fr") {
				comment = " ("+java.lang.String.format(java.util.Locale.FRENCH, "%,f", v)+", in French format)"
			}
			println expression+" = "+sprintf("%f", v)+comment
		}
	}
} catch(e) {
	println "** syntax error in expression: "+e
}