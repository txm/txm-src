// Copyright © 2019 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

package org.txm.macro.utils

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.FileUtils
import org.txm.utils.Timer

@Field @Option(name="propertiesFile", usage="properties parameter file", widget="File", required=true, def="")
def propertiesFile

@Field @Option(name="macroFile", usage="macro .groovy file", widget="File", required=true, def="")
def macroFile

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return 1

def scriptName = this.class.getSimpleName()

params = new Properties()
if (propertiesFile.exists() && propertiesFile.canRead()) {
	props = new InputStreamReader(new FileInputStream(propertiesFile) , "UTF-8")
	params.load(props)
	props.close()
} else {
	println "** $scriptName: '$propertiesFile' file not readable."
	return 1
}

macro = FileUtils.stripExtension(macroFile);

pkg = macroFile.getParentFile().getAbsolutePath()
pkg = pkg.substring(pkg.indexOf("scripts/groovy/user/")+"scripts/groovy/user/".length())
pkg = pkg.replace("/", ".")

fullclassname = "$pkg.$macro"
println "Calling $fullclassname..."

clazz = Class.forName(fullclassname)
if (clazz == null) {
	println "** Class for name=$fullclassname not found. Aborting."
	return
}

for (def f : clazz.getDeclaredFields()) {
	def parameter = f.getAnnotation(Option.class);
	if (parameter == null || parameter.widget() == null || parameter.name() == null) {
		continue
	}
	
	String name = parameter.name()
	if (params.get(name) == null) continue
	
	String strValue = params.get(name)
	String widget = parameter.widget()

	switch(widget) {
		case "File":
		case "FileOpen":
		case "FileSave":
		case "Folder":
			params.put(name, new File(strValue))
			break;
//		case "Date":
//			params.put(name, ...)
//			break
		case "Float":
			params.put(name, Float.parseFloat(strValue))
			break
		case "Integer":
			params.put(name, Integer.parseInt(strValue))
			break
		case "String":
		case "Text":
			break
		case "Boolean":
			params.put(name, Boolean.parseBoolean(strValue))
			break
	}
}

res = gse.run(clazz, [
	"args":params,
				"selection":selection,
				"selections":selections,
				"corpusViewSelection":corpusViewSelection,
				"corpusViewSelections":corpusViewSelections,
				"monitor":monitor,
				"editor":editor,
				"stringArgs":stringArgs,
				"timer":timer])

if (!res) println "** $scriptName: problem calling $macro."
