// Copyright © 2022 ENS de Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.files

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

@Field @Option(name="fromFolder", usage="dossier départ racine", widget="Folder", required=true, def="")
def fromFolder

@Field @Option(name="toFolder", usage="dossier arrivée", widget="Folder", required=true, def="")
def toFolder

@Field @Option(name="extensionRegex", usage="regex de l'extension des fichiers à modifier", widget="String", required=true, def='txt')
def extensionRegex

@Field @Option(name="findRegex", usage="regex de nom à chercher", widget="String", required=true, def='’')
def findRegex

@Field @Option(name="recurse", usage="traiter les sous-dossiers", widget="Boolean", required=true, def="false")
def recurse

@Field @Option(name="dryRun", usage="chaîne de remplacement", widget="Boolean", required=true, def="false")
def dryRun

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

println "In $fromFolder..."

if (dryRun) {
  println "** dry run mode: no file will be moved."
}

nMove = 0

if (recurse) {

  fromFolder.eachFileRecurse(groovy.io.FileType.FILES) { file ->
  
	fullname = file.getName()
	idx = fullname.lastIndexOf(".")
	if (idx > 0) {
	
	  name = fullname.substring(0, idx)
	  ext = fullname.substring(idx+1, fullname.length())

	  if (ext ==~ extensionRegex && name ==~ findRegex) {
		
		newfile = new File(toFolder, fullname)
		println file.getAbsolutePath()
		println "->"
	println newfile.getAbsolutePath()

	if (!dryRun) {
	  file.renameTo(newfile)
		}
		nMove++
	  }
	}
  }
} else {

  fromFolder.eachFile(groovy.io.FileType.FILES) { file ->
  
	fullname = file.getName()
	idx = fullname.lastIndexOf(".")
	if (idx > 0) {
	
	  name = fullname.substring(0, idx)
	  ext = fullname.substring(idx+1, fullname.length())

	  if (ext ==~ extensionRegex && name ==~ findRegex) {
		
		newfile = new File(toFolder, fullname)
		println file.getAbsolutePath()
		println "->"
	println newfile.getAbsolutePath()

	if (!dryRun) {
	  file.renameTo(newfile)
		}
		nMove++
	  }
	}
  }
}

if (dryRun) {
  println nMove+" moves to do."
} else {
  println nMove+" moves done."
}

return 1
