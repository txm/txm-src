// STANDARD DECLARATIONS
package org.txm.macro.files

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

@Field @Option(name="inputDirectory", usage="TXT directory", widget="Folder", required=false, def="txt")
File inputDirectory

// **change this parameter**
@Field @Option(name="extension", usage="Regexp de l'extension des fichiers à modifier", widget="String", required=true, def='\\.txt')
def extension

// **change this parameter**
@Field @Option(name="find", usage="Expression régulière", widget="String", required=true, def='’')
def find

// **change this parameter**
@Field @Option(name="replaceWith", usage="Chaîne de remplacement", widget="String", required=false, def='\'')
def replaceWith = ""


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

println "In $inputDirectory..."
inputDirectory.eachFileMatch(~/.*$extension/) { file ->               // for each file matching extension
		println " renaming: "+file.getName()
		String name = file.getName()
		name = name.replaceAll(find, replaceWith)
		file.renameTo(new File(file.getParentFile(), name))
	}