// Copyright © 2016 ENS de Lyon
//
// Authors:
// - Serge Heiden
//
// Licence:
// This file is part of the TXM platform.
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
// Version:
// $LastChangedDate: 2014-11-01 16:00:01 +0100 (sam., 1 nov. 2014) $
// $LastChangedRevision: XXXX $
// $LastChangedBy: sheiden $
//

/* --------------   Specif2Coin    --------------

FR:

Macro affichant la probabilité a priori (avant de faire les lancés) d'obtenir N faces 'pile' consécutives en lançant une pièce au pile ou face.

On considère qu'une pièce a 50% de chances (1 chance sur 2) de tomber sur la face 'pile' à chaque lancé (les lancés sont indépendants).

La probabilité est mise en regard avec la spécificité équivalente.

La macro prend un seul paramètre :

* nthrows : le nombre de lancés 'pile' consécutifs

La macro affiche - selon les options :

* displayProba : toutes les probabilités de 1 à nthrows lancés en détaillant :
-  n : le nombre de 'pile' consécutifs
-  p : la probabilité correspondante
-  % : le pourcentage correspondant
- pe : la probabilité exprimée en notation avec exposant en base 10
-  s : la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)

* onlyMainRanks : s'utilise avec l'option displayProba pour n'afficher que les probabilités des rangs décimaux principaux (1, 2..., 10, 20..., 100, 200...)

* displayCoins : plutôt que le détail de la probabilité, liste pour une spécificité S- donnée, le nombre équivalent de lancés 'pile' consécutifs correspondants exprimé sous la forme d'un intervalle : nombre de lancés minimum - nombre de lancés maximum
Par exemple '-10 = 30-33' signifie : une spécificité S- de '-10' équivaut à entre 30 et 33 lancés 'pile' consécutifs

Les limites du calcul sont celles de la machine. Une machine 64-bit peut typiquement calculer la probabilité de 1023 lancés consécutifs.

*/

// STANDARD DECLARATIONS
package org.txm.macro.misc

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.eclipse.ui.console.*

// BEGINNING OF PARAMETERS

@Field @Option(name="nthrows", usage="maximum number of throws", widget="Integer", required=true, def="99")
def nthrows

@Field @Option(name="displayProba", usage="display probabilities", widget="Boolean", required=false, def="true")
def displayProba

@Field @Option(name="onlyMainRanks", usage="only display main rank lines", widget="Boolean", required=false, def="false")
def onlyMainRanks

@Field @Option(name="displayCoins", usage="display equivalent of coin flipping", widget="Boolean", required=false, def="true")
def displayCoins

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def clearConsole = { ->
	// clear the console
	(ConsolePlugin.getDefault().getConsoleManager().getConsoles())[0].clearConsole()
}

clearConsole()

min = [:]
max = [:]

pow = 2**(nthrows)
powd = pow.doubleValue()

if (powd == Double.POSITIVE_INFINITY || powd == Double.NEGATIVE_INFINITY) {
	println sprintf("** Impossible to calculate probabilities for this value, try a lower value (impossible to represent 2 to the power of %d with this machine). Aborting.", nthrows)
	return
}

d = 1/powd

significandSize = ((-java.lang.Math.log10(d) as double)-1 as int)+4

format = sprintf("%%4d = %%%d.%df = %%0%d.%df%%%% = %%9.2e = %%4d", significandSize, significandSize-2, significandSize-1, significandSize-4)

unit = 1
dec = 1

if (displayProba) {
	println """Légende :
-  n : le nombre de 'pile' consécutifs
-  p : la probabilité correspondante
-  % : le pourcentage correspondant
- pe : la probabilité exprimée en notation avec exposant en base 10
-  s : la spécificité équivalente (l'exposant de la probabilité, soit son logarithme en base 10)
"""
	spc = ' ' * (significandSize+2)
	println "   n    p"+spc+" %"+spc+"  pe         s"
}

nthrows.times {
    n = it+1
	p = 1/2*n as double
	s = java.lang.Math.log10(p)-1 as int
	if (min[s]) { if (min[s] > n) min[s] = n } else min[s] = n
	if (max[s]) { if (max[s] < n) max[s] = n } else max[s] = n
	if (displayProba) {
		s = sprintf(format, n, p, p*100, p, s).replaceAll(/ = 0([^,])/, ' =  $1')
		m0 = s =~ /(....)0+ = ([^-])/
		if (m0.count > 0) {
			spc0 = m0[0][1] + (' ' * (m0[0][0].size()-8)) + ' = ' + m0[0][2]
			s = m0.replaceFirst(spc0)
		}
		m0p = s =~ /(....)0+% = /
		if (m0p.count > 0) {
			spc0p = m0p[0][1] + '%' + (' ' * (m0p[0][0].size()-8)) + ' = '
			s = m0p.replaceFirst(spc0p)
		}

		if (!onlyMainRanks || ((n % dec) == 0) || (n == nthrows)) {
			println s
		}
		
		unit = unit+1
		if (unit >= dec * 10) {
			dec = dec * 10
			unit = 1
		}
	}
}

if (displayCoins) {

	if (displayProba) {
		println ""
	}

	println """Légende :
- -S      : spécificité équivalente
-  lancés : intervalle des lancés 'pile' consécutifs correspondant
"""
	println " -S   lancés"
	min.each { key, value -> if (min[key] != max[key]) println sprintf("%3d = %d-%d", key, min[key], max[key]) else println sprintf("%3d = %d", key, min[key])}
}
