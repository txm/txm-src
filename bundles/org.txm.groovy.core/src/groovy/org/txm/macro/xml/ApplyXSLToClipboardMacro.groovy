package org.txm.macro.xml
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.ApplyXsl2
import org.txm.rcp.utils.IOClipboard

// BEGINNING OF PARAMETERS

// teitxm2xmlw = System.getProperty("user.home")+"/TXM/xsl/txm-front-teitxm2xmlw.xsl"

@Field @Option(name="XSL_file", usage="an example file", widget="File", required=true, def="")
def XSL_file

@Field @Option(name="wrap_with_TEI_tag", usage="wrap XML fragment with TEI tag and namespace declarations", widget="Boolean", required=true, def="true")
def wrap_with_TEI_tag

@Field @Option(name="copy_result_to_clipboard", usage="replace the clipboard content by the result of the XSLT processing", widget="Boolean", required=true, def="true")
def copy_result_to_clipboard

@Field @Option(name="debug", usage="Show debug messages", widget="Boolean", required=true, def="false")
def debug

if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def inputFile = File.createTempFile("ApplyXSLToClipboard-input",".xml")
inputFile.deleteOnExit()

if (wrap_with_TEI_tag) {
	inputFile << """<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:txm="http://textometrie.org/1.0">
"""
}

monitor.syncExec(new Runnable() {
	public void run() {
		inputFile << IOClipboard.read()
	}
})

if (wrap_with_TEI_tag) {
inputFile << """</TEI>
"""
}

def outputFile = File.createTempFile("ApplyXSLToClipboard-output",".xml")
outputFile.deleteOnExit()

xslfactory = new ApplyXsl2(XSL_file.getAbsolutePath())

try {	
	xslfactory.process(inputFile, outputFile)
} catch (e) {

	inputFile.delete()
	outputFile.delete()
	return 0
}

result = outputFile.getText()
print result

if (copy_result_to_clipboard) {
	monitor.syncExec(new Runnable() {
		public void run() {
			IOClipboard.write(result)
		}
	})
}

inputFile.delete()
outputFile.delete()

return 1

