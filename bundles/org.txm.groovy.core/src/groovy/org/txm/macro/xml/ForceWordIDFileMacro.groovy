// Copyright © 2019 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License version 3 or any later version (https://www.gnu.org/licenses/gpl.html)
// @author mdecorde
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.xml

import org.txm.utils.FileUtils;
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.StaxIdentityParser
import javax.xml.stream.XMLStreamException

@Field @Option(name="inputFile", usage="input file (.xml)", widget="FileOpen", required=true, def="")
def inputFile

@Field @Option(name="outputFile", usage="output file", widget="FileSave", required=true, def="")
def outputFile

@Field @Option(name="wordElement", usage="word element regex", widget="String", required=true, def="w|pc")
def wordElement

@Field @Option(name="foreignIDAttribute", usage="forein id attribute to use if 'id' is already set", widget="String", required=true, def="fn-id")
def foreignIDAttribute

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (!(inputFile.exists() && inputFile.canRead())) {
	println "** ForceWordIDFile: cannot find $inputFile"
	return false
}

print "Processing "+inputFile+"..."

outputFile.getParentFile().mkdirs()

// build text id
textid = FileUtils.stripExtension(inputFile)

ForceWordIDParser parser = new ForceWordIDParser(inputFile, wordElement, foreignIDAttribute, textid);

def res = parser.process(outputFile)

println " Done."

return "ok"
