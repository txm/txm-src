package org.txm.scripts.scripts

import javax.xml.stream.*;

import org.txm.importer.StaxIdentityParser;


path = ""
paths = [:]

usePaths=true
useAttributes=true
useAttributeValues=false

inputFile = new File("/home/mdecorde/xml/xmlw/test.xml")

try {
	usePaths=args["usePaths"]
	useAttributes=args["useAttributes"]
	useAttributeValues=args["useAttributeValues"]
	inputFile=args["inputFile"]
} catch(Exception e) { } // set the "args" map binding to set usePaths, useAttributes,useAttributeValues, inputFile

inputFile = new File("/home/mdecorde/xml/xmlw/test.xml")


inputurl = inputFile.toURI().toURL()
inputData = inputurl.openStream()
factory = XMLInputFactory.newInstance()
parser = factory.createXMLStreamReader(inputData)

try {
	for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
		switch (event) {
			case XMLStreamConstants.NAMESPACE:
				break
			case XMLStreamConstants.START_ELEMENT:
				localname = parser.getLocalName()
				processStartElement()
				break
			case XMLStreamConstants.CHARACTERS:
				break
			case XMLStreamConstants.END_ELEMENT:
				localname = parser.getLocalName()
				processEndElement()
				break
			case XMLStreamConstants.PROCESSING_INSTRUCTION:
				break
			case XMLStreamConstants.DTD:
				break
			case XMLStreamConstants.CDATA:
				break
			case XMLStreamConstants.COMMENT:
				break
			case XMLStreamConstants.END_DOCUMENT:
				break
			case XMLStreamConstants.ENTITY_REFERENCE:
				break
		}
	}
} catch(Exception e) {
	println("Unexpected error while parsing file "+inputurl+" : "+e);
	e.printStackTrace();
	if (parser != null) parser.close();
	if (inputData != null) inputData.close();
	return false;
}

if (parser != null) parser.close();
if (inputData != null) inputData.close();

return paths

public void processEndElement() {
	if (usePaths) {
		path = path.substring(0, path.lastIndexOf("/"))
	}
}

public void processStartElement() {
	String localname = parser.getLocalName();
	if (usePaths) {
		path = path + "/$localname"
	} else {
		path = localname;
	}

	if (useAttributes) {
		if (useAttributeValues) {
			for(int i = 0 ; i < parser.getAttributeCount() ; i++)
				if (!parser.getAttributeLocalName(i).matches(UNICATTRS))
					path += "@"+parser.getAttributeLocalName(i)+"="+parser.getAttributeValue(i)
		} else {
			for(int i = 0 ; i < parser.getAttributeCount() ; i++)
				path += "@"+parser.getAttributeLocalName(i)
		}
	}

	println "++"
	plusOne()
	//super.processStartElement() // don't write :)
}

private void plusOne() {
	if (!paths.containsKey(path)) {
		paths[path] = 1
	} else {
		paths[path] =  paths[path]+1
	}
}


