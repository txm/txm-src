package org.txm.macro.xml;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.ConsoleProgressBar
import org.txm.importer.ApplyXsl2;

// BEGINNING OF PARAMETERS
@Field @Option(name="XSLFile", usage="an example file", widget="File", required=true, def="file.xsl")
def XSLFile = new File(System.getProperty("user.home"),"TXM/xsl/identity.xsl")

@Field @Option(name="intputDirectory", usage="an example folder", widget="Folder", required=true, def="in")
def intputDirectory = new File(System.getProperty("user.home"),"xml/TESTS2/xml")

@Field @Option(name="outputDirectory", usage="an example folder", widget="Folder", required=true, def="out")
def outputDirectory = new File("/tmp/xsltest")

//@Field @Option(name="parameters", usage="an example folder", widget="Text", required=false, def="")
def parameters = ""

@Field @Option(name="debug", usage="Show debug messages, value = true|false", widget="String", required=true, def="false")
def debug

if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS

debug = "true" == debug

outputDirectory.mkdir()

parameters = parameters.trim()
if (parameters.length() == 0) {
	parameters = null
} else {
	def split = parameters.split("\n")
	parameters = []
	for (def str : split) {
		def split2 = str.split("=", 2)
		if (split2.size() == 2) {
			parameters << split2
		}
	}
}

println "Use XSL $XSLFile with parameters $parameters"
println "Processed directory: $intputDirectory"

def files = []
intputDirectory.eachFileMatch(~/.+\.(xml|XML)/) { XMLFile ->
	files << XMLFile
}
def cpb = new ConsoleProgressBar(files.size())
ApplyXsl2 a = new ApplyXsl2(XSLFile.getAbsolutePath());
for (def XMLFile : files) {
	String name = XMLFile.getName()
	try {
		
		def outFile = new File(outputDirectory, name)
		if (parameters != null) {
			for(def param : parameters) {
				a.setParam(param[0], param[1])
			}
		}
		a.process(XMLFile, outFile);
		cpb.tick();
		a.resetParams()
	} catch (Exception e) {
		println "Warning: XSL transformation of '$name' failed with error=$e with "
		if (debug) e.printStackTrace(); 
	}
}

cpb.done();