package org.txm.macro.xml
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.ApplyXsl2

// BEGINNING OF PARAMETERS

@Field @Option(name="inputDirectory", usage="an example folder", widget="Folder", required=true, def="in")
def inputDirectory = new File(System.getProperty("user.home"),"/tmp/xsltest")

@Field @Option(name="outputDirectory", usage="an example folder", widget="Folder", required=true, def="out")
def outputDirectory = new File("/tmp/xsltest/out")

@Field @Option(name="omitXmlDeclaration", usage="omit xml declaration", widget="Boolean", required=false, def="false")
def omitXmlDeclaration

@Field @Option(name="suppressIndentation", usage="space delimited list of elements not to indent", widget="Text", required=false, def="")
def suppressIndentation

@Field @Option(name="doubleSpace", usage="space delimited list of elements with forced line break before", widget="Text", required=false, def="")
def doubleSpace

@Field @Option(name="encoding", usage="output character encoding", widget="String", required=false, def="utf-8")
def encoding

@Field @Option(name="namespacesDeclaration", usage="space delimited list of namespace declarations (eg \"xmlns:dc='http://purl.org/dc/elements/1.1/'\")", widget="Text", required=false, def="")
def namespacesDeclaration

@Field @Option(name="debug", usage="Show debug messages", widget="Boolean", required=true, def="false")
def debug

if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

doubleSpace = doubleSpace.trim()

if (doubleSpace.length() > 0) doubleSpace = " saxon:double-space=\"$doubleSpace\""

suppressIndentation = suppressIndentation.trim()

if (suppressIndentation.length() > 0) suppressIndentation = " saxon:suppress-indentation=\"$suppressIndentation\""

encoding = encoding.trim()

if (encoding.length() > 0) encoding = " encoding=\"$encoding\""

if (omitXmlDeclaration) omitXmlDeclarationP = " omit-xml-declaration=\"yes\"" else omitXmlDeclarationP = " omit-xml-declaration=\"no\""

namespacesDeclaration = namespacesDeclaration.trim()

if (namespacesDeclaration.length() > 0) namespacesDeclaration = " $namespacesDeclaration"

def xslParameters = ""

IndentStyleSheet = """
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:saxon="http://saxon.sf.net/"${namespacesDeclaration}>
  <xsl:output method="xml" indent="yes"${omitXmlDeclarationP}${encoding}${doubleSpace}${suppressIndentation}/>
	<!-- saxon:indent-spaces="6" for Saxon-PE only -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <!-- Including any attributes it has and any child nodes -->
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>"""

def XSLFile = File.createTempFile("IndentStyleSheet",".xsl")
XSLFile.deleteOnExit()

XSLFile << IndentStyleSheet

outputDirectory.mkdir()

xslParameters = xslParameters.trim()
if (xslParameters.length() == 0) {
	xslParameters = null
} else {
	def split = xslParameters.split("\n")
	xslParameters = []
	for (def str : split) {
		def split2 = str.split("=", 2)
		if (split2.size() == 2) {
			xslParameters << split2
		}
	}
}

println "Processed directory: $inputDirectory."

def files = [] 
ApplyXsl2 a = new ApplyXsl2(XSLFile.getAbsolutePath())
inputDirectory.eachFileMatch(~/.+\.(xml|XML)/) { XMLFile ->
	String name = XMLFile.getName()
	try {
		
		def outFile = new File(outputDirectory, name)
		if (xslParameters != null) {
			for(def param : xslParameters) {
				a.setParam(param[0], param[1])
			}
		}
		a.process(XMLFile, outFile)
		a.resetParams()
		files << XMLFile
	} catch (Exception e) {

			msgParse = (e.getMessageAndLocation() =~ /^(.*) lineNumber: ([0-9]+); columnNumber: ([0-9]+); (.*).$/)
			if (msgParse.size() == 1) {

				def lineNumber = msgParse[0][2]
				def colNumber = msgParse[0][3]
				def errMsg = msgParse[0][4]

				println "** $name processing aborted, [line $lineNumber, character $colNumber]: $errMsg."


			} else {
				println e.getMessageAndLocation()
			}

		XSLFile.delete()
		return 0
	}
}

XSLFile.delete()

return 1

