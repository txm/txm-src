package org.txm.macro.xml

// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

import org.txm.scripts.importer.*
import org.txm.utils.FileUtils
import org.txm.metadatas.Metadatas

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="xpathFile", usage="'.properties' file", widget="File", required=true, def="metadata.properties")
def xpathFile

@Field @Option(name="srcDirectory", usage="XML source directory", widget="Folder", required=false, def="srcDirectory")
def srcDirectory

@Field @Option(name="metadataFile", usage="'.properties' file", widget="File", required=false, def="metadata.csv")
def metadataFile

@Field @Option(name="columnSeparator", usage="", widget="String", required=false, def=",")
def columnSeparator

@Field @Option(name="txtSeparator", usage="", widget="String", required=false, def="\"")
def txtSeparator

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

if (!srcDirectory.exists()) { println "$srcDirectory does not exist"; return}
if (!xpathFile.exists()) { println "$xpathFile does not exist"; return}

// get Xpath queries
def xpaths = [:]
for (String line : xpathFile.readLines("UTF-8")) {
	def split = line.split("=", 2);
	if (split.size() == 2) {
		xpaths[split[0]] = split[1]
	}
}
println "XPATH queries: $xpaths"

if (metadataFile == null) {
	metadataFile = new File(srcDirectory, "metadata.csv")
}

def writer = metadataFile.newWriter("UTF-8")
println "Creating file: $metadataFile"
writer.print "id"
for (String p : xpaths.keySet()) {writer.print columnSeparator+p}
writer.println ""

def files = srcDirectory.listFiles()
if (files == null || files.size() == 0) {
	println "No files in $srcDirectory"
	return;
}
files.sort()

for (File xmlFile : files) {
	
	if (!FileUtils.isXMLFile(xmlFile)) continue;
	
	println "Processing file $xmlFile..."
	
	String name = FileUtils.stripExtension(xmlFile);
	
	writer.print "$name"
	
	XPathResult xpathProcessor = new XPathResult(xmlFile)
	for (String p : xpaths.keySet()) {
		def value = xpathProcessor.getXpathResponse(xpaths[p]);
		if (value == null) value = "N/A"
		writer.print columnSeparator+txtSeparator+value.replaceAll(txtSeparator,txtSeparator+txtSeparator)+txtSeparator
	}
	writer.println ""
}

writer.close()