package org.txm.macro.xml;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.ApplyXsl2;

// BEGINNING OF PARAMETERS

@Field @Option(name="inputDirectory", usage="an example folder", widget="Folder", required=true, def="in")
def inputDirectory = new File(System.getProperty("user.home"),"xml/TESTS2/xml")

@Field @Option(name="outputDirectory", usage="an example folder", widget="Folder", required=true, def="out")
def outputDirectory = new File("/tmp/xsltest")

@Field @Option(name="elementName", usage="element to number", widget="String", required=true, def="pb")
def elementName

@Field @Option(name="elementAttribute", usage="element attribute to set", widget="String", required=true, def="n")
def elementAttribute

@Field @Option(name="countStart", usage="count starting number", widget="Integer", required=true, def="1")
def countStart

@Field @Option(name="valuePrefix", usage="attribute value prefix", widget="String", required=true, def="")
def valuePrefix

@Field @Option(name="valueSuffix", usage="attribute value suffix", widget="String", required=true, def="")
def valueSuffix

def parameters = ""

@Field @Option(name="debug", usage="Show debug messages, value = true|false", widget="String", required=true, def="false")
def debug

if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

NumberElementStyleSheet = """<?xml version="1.0"?>
<xsl:stylesheet
  xmlns:xd="http://www.pnp-software.com/XSLTdoc"
  xmlns:edate="http://exslt.org/dates-and-times"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="tei edate xd"
  version="2.0">
  
  <xd:doc type="stylesheet">
    <xd:short>
      Patron de feuille de style de numérotation d'éléments.
    </xd:short>
    <xd:detail>
      This stylesheet is free software; you can redistribute it and/or
      modify it under the terms of the GNU Lesser General Public
      License as published by the Free Software Foundation; either
      version 3 of the License, or (at your option) any later version.
      
      This stylesheet is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
      Lesser General Public License for more details.
      
      You should have received a copy of GNU Lesser Public License with
      this stylesheet. If not, see http://www.gnu.org/licenses/lgpl.html
    </xd:detail>
    <xd:author>Serge Heiden slh@ens-lyon.fr</xd:author>
    <xd:copyright>2018, ENS de Lyon/CNRS UMR 5317 IHRIM</xd:copyright>
  </xd:doc>
  

  <xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>
  
  <!-- On supprime tous les <sp> dont l'attribut @who vaut 'enqueteur' -->
  
  <xsl:template match="//*:$elementName">    
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="$elementAttribute">
	<xsl:value-of select="concat('$valuePrefix', (count(preceding::*:$elementName) + $countStart), '$valueSuffix')"/>
	<!--
	 <xsl:value-of select="string:format(\$arg)" xmlns:string="java:java.lang.String"/>
	 not available in Saxon-HE, use 'Integrated extension functions' instead, see http://saxonica.com/documentation/index.html#!extensibility
	 -->
      </xsl:attribute>
      <xsl:apply-templates select="node()" />
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
"""

debug = "true" == debug

def XSLFile = File.createTempFile("NumberElementStyleSheet",".xsl")
XSLFile.deleteOnExit()

XSLFile << NumberElementStyleSheet

outputDirectory.mkdir()

parameters = parameters.trim()
if (parameters.length() == 0) {
	parameters = null
} else {
	def split = parameters.split("\n")
	parameters = []
	for (def str : split) {
		def split2 = str.split("=", 2)
		if (split2.size() == 2) {
			parameters << split2
		}
	}
}

println "Processed directory: $inputDirectory"

def files = [] 
ApplyXsl2 a = new ApplyXsl2(XSLFile.getAbsolutePath());
inputDirectory.eachFileMatch(~/.+\.(xml|XML)/) { XMLFile ->
	String name = XMLFile.getName()
	try {
		
		def outFile = new File(outputDirectory, name)
		if (parameters != null) {
			for(def param : parameters) {
				a.setParam(param[0], param[1])
			}
		}
		a.process(XMLFile, outFile);
		a.resetParams()
		files << XMLFile
	} catch (Exception e) {
		println "Warning: XSL transformation of '$name' failed with error=$e with "
		if (debug) e.printStackTrace(); 
	}
}
