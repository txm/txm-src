// Copyright © 2019,2021 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License version 3 or any later version (https://www.gnu.org/licenses/gpl.html)
// @author mdecorde
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.xml

import org.txm.utils.FileUtils;
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.importer.StaxIdentityParser
import javax.xml.stream.XMLStreamException

@Field @Option(name="inputDirectory", usage="input directory (.xml files)", widget="Folder", required=true, def="")
def inputDirectory

@Field @Option(name="outputDirectory", usage="output directory", widget="Folder", required=true, def="")
def outputDirectory

@Field @Option(name="wordElement", usage="word element regex", widget="String", required=true, def="word")
def wordElement

@Field @Option(name="foreignIDAttribute", usage="forein id attribute to use if 'id' is already set", widget="String", required=true, def="fn-id")
def foreignIDAttribute

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

if (!inputDirectory.exists()) {
	println "** ForceWordIDDir: no '"+inputDirectory.name+"' directory found. Aborting."
	return false
}

if (!inputDirectory.canRead()) {
	println "** ForceWordIDDir: '"+inputDirectory.name+"' directory not readable. Aborting."
	return false
}

def f = []
inputDirectory.eachFileMatch(~/.*xml/) { f << it }

if (f.size() == 0) {
	println "** ForceWordIDDir: no .xml file found. Aborting."
	return false
}

try {

f.sort{ it.name }.each { inputFile ->

	def outputFile = new File(outputDirectory, inputFile.name)

	res = gse.run(ForceWordIDFileMacro, ["args":[

				"inputFile": inputFile,
				"outputFile": outputFile,
				"wordElement": wordElement,
				"foreignIDAttribute": foreignIDAttribute,

				"selection":selection,
				"selections":selections,
				"corpusViewSelection":corpusViewSelection,
				"corpusViewSelections":corpusViewSelections,
				"monitor":monitor]])
				
			if (!res) println "** problem calling ForceWordIDFileMacro."
}

} catch (Exception e) {
	println "** ForceWordIDDir: unable to read input files. Aborting."
	println e.getLocalizedMessage()
	println e.printStackTrace()
	return false
}

return true

