// Copyright © 2021 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro.xml

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.importer.StaxIdentityParser
import javax.xml.stream.XMLStreamException

class ForceWordIDParser extends StaxIdentityParser {
	public int wordnumber = 1
	String wordElement;
	String foreingIDAttribute;
	String textid
	
	public ForceWordIDParser(File xmlFile, String wordElement, String foreingIDAttribute, String textid) {
		super(xmlFile);
		this.wordElement = wordElement
		this.foreingIDAttribute = foreingIDAttribute
		this.textid = textid
	}
	
	protected void writeAttributes() throws XMLStreamException {
		if (localname ==~ wordElement) { 																								// a word
			boolean idwritten = false
			def attCount = parser.getAttributeCount()
			attCount.times { i ->
				if (parser.getAttributeLocalName(i) == "id") { // update & backup
					writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), "w_"+textid+"_"+(wordnumber++)) 	// force id
					writeAttribute(parser.getAttributePrefix(i), foreingIDAttribute, parser.getAttributeValue(i)) 						// backup
					idwritten = true
				} else if (parser.getAttributeLocalName(i) != foreingIDAttribute) {
					writeAttribute(parser.getAttributePrefix(i), parser.getAttributeLocalName(i), parser.getAttributeValue(i))
				}
			}
			
			if (!idwritten) { 																										// create id
				writeAttribute(null, "id", "w_"+textid+"_"+(wordnumber++))
			}
		} else {
			super.writeAttributes()
		}
	}

}