// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $

// lists all the XML elements matching a given XPath:
// XPath is given by the XPath parameter
// elements are searched in a file (srcFile parameter) or in the files of a directory (srcDirectory).
// files in the directory can be filtered (filterByFileExtension parameter) by a specific file extension (fileExtension parameter)
// results can be localized in the source files by line and column number (lineNumber parameter)
// results can be wrapped to 100 characters max per line (wrapLines parameter)

package org.txm.macro.xml

// STANDARD DECLARATIONS

import javax.xml.xpath.XPathConstants
import javax.xml.namespace.NamespaceContext
import javax.xml.transform.stream.StreamSource
import javax.xml.XMLConstants

import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl
import net.sf.saxon.xpath.XPathFactoryImpl
import net.sf.saxon.s9api.Processor
import net.sf.saxon.s9api.Serializer
import net.sf.saxon.s9api.XdmNode

import org.kohsuke.args4j.Option
import groovy.transform.Field

import org.txm.importer.GetXPath
import org.txm.rcp.swt.widget.parameters.*
import org.txm.scripts.importer.XPathResult

import org.apache.commons.lang3.text.*

// BEGINNING OF PARAMETERS

// Declare each parameter here
// (available widget types: Query, File, Folder, String, Text, Boolean, Integer, Float and Date)

@Field @Option(name="srcFile", usage="XML source file", widget="File", required=false, def="")
		def srcFile

@Field @Option(name="srcDirectory", usage="XML source directory", widget="Folder", required=false, def="")
		def srcDirectory

@Field @Option(name="filterByFileExtension", usage="filter by file extension", widget="Boolean", required=false, def="true")
		def filterByFileExtension

@Field @Option(name="fileExtension", usage="file extension to filter (eg .xml)", widget="String", required=false, def=".xml")
		def fileExtension

@Field @Option(name="XPath", usage="XPath expression", widget="String", required=true, def="//tei:title/text()")
		def XPath

@Field @Option(name="lineNumber", usage="print line number", widget="Boolean", required=false, def="true")
		def lineNumber

@Field @Option(name="wrapLines", usage="wrap output lines longer than 100 characters", widget="Boolean", required=false, def="true")
		def wrapLines

//@Field @Option(name="interactive", usage="open parameters dialog box", widget="Boolean", required=true, def="true")
//def interactive

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) {
	println "** Abandon de la macro."
	return false
}

// END OF PARAMETERS

lineNumberLen = 0
columnNumberLen = 0

if ((srcDirectory==null || srcDirectory.size() == 0) && (srcFile==null || srcFile.size() == 0)) { println "** GetXPathMacro: at least a source file or a source directory must be specified."; return}

if (srcDirectory!=null && srcDirectory.size() > 0 && srcDirectory.exists()) {
	
	def files = srcDirectory.listFiles()
	if (files == null || files.size() == 0) {
		println "** GetXPathMacro: No files in $srcDirectory"
		return
	}
	files.sort()
	
	def noFileSearched = true
	
	xpathProc = GetXPath.build()
	
	println "GetXPath: input directory = '$srcDirectory', XPath = $XPath"
	
	for (def xmlFile : files) {
		
		String name = xmlFile.getName()
		
		if (filterByFileExtension && !name.endsWith(fileExtension)) { continue }
		
		println "\n-- $name"
		
		noFileSearched = false
		
		if (lineNumber) {
			(maxLine, maxColumn) = countLines(xmlFile)
			lineNumberLen = Math.log10(maxLine)+1 as int
			columnNumberLen = Math.log10(maxColumn)+1 as int
		}
		
		//res = xpathProc.evaluateExpression(XPath, new StreamSource(xmlFile), XPathConstants.NODESET);
		res = GetXPath.evaluateExpressionToObject(xpathProc, xmlFile, XPath, XPathConstants.NODESET)
		if (res.getLength() == 0) {
			println "No result."
		} else res.each { printValue(it) }
	}
	
	if (noFileSearched) { println "** GetXPath: no file searched." }
	
} else if (srcFile!=null && srcFile.exists()) {
	
	def xmlFile = srcFile
	String name = xmlFile.getName()
	
	println "GetXPath: file = '$xmlFile', XPath = $XPath"
	
	if (lineNumber) {
		(maxLine, maxColumn) = countLines(xmlFile)
		lineNumberLen = Math.log10(maxLine)+1 as int
		columnNumberLen = Math.log10(maxColumn)+1 as int
	}
	
	res = GetXPath.evaluateExpressionToObject(XPath, srcFile, XPathConstants.NODESET);
	
	if (res.getLength() == 0) {
		println "No result."
	} else res.each { printValue(it) }
}

// FUNCTIONS

def serializeNode(node) {
	processor = new Processor(false)
	serializer = processor.newSerializer()
	// Other properties found here: http://www.saxonica.com/html/documentation/javadoc/net/sf/saxon/s9api/Serializer.Property.html
	serializer.setOutputProperty(Serializer.Property.OMIT_XML_DECLARATION, "yes")
	serializer.setOutputProperty(Serializer.Property.INDENT, "yes")
	xdmNode = new XdmNode(node.getUnderlyingNodeInfo())
	return(serializer.serializeNodeToString(xdmNode))
}

def printValue(node) {
	
	//	println "beginning of printValue"
	//println "result type is of type "+node.getClass()
	switch (node.getNodeType()) {
		
		case 1: // element
		//println "case 1 : "+node.getClass()
		//            			println "beginning of case 1"
			if (lineNumber) {
				print sprintf("%${lineNumberLen}d, %${columnNumberLen}d: ", node.getUnderlyingNodeInfo().getLineNumber(), node.getUnderlyingNodeInfo().getColumnNumber())
			}
			if (wrapLines) {
				println WordUtils.wrap(serializeNode(node).replaceAll(/\r\n+/, " ").replaceAll(/\r+/, " ").replaceAll(/\n+/, " ").replaceAll(/ +/, " "), 100)
			} else {
				println serializeNode(node)
			}
			break
		
		case 2: // attribute
		//println "case 2 : "+node.getClass()
		//            			println "beginning of case 2"
			if (lineNumber) {
				print sprintf("%${lineNumberLen}d, %${columnNumberLen}d: ", node.getUnderlyingNodeInfo().getLineNumber(), node.getUnderlyingNodeInfo().getColumnNumber())
			}
			if (wrapLines) {
				println WordUtils.wrap(serializeNode(node).replaceAll(/\r\n+/, " ").replaceAll(/\r+/, " ").replaceAll(/\n+/, " ").replaceAll(/ +/, " "), 100)
			} else {
				println serializeNode(node)
			}
			break
		
		default:
		//println "other case : "+node.getClass()
		//            			println "beginning of case 3"
			if (lineNumber) {
				print sprintf("%${lineNumberLen}d, %${columnNumberLen}d: ", node.getUnderlyingNodeInfo().getLineNumber(), node.getUnderlyingNodeInfo().getColumnNumber())
			}
			if (wrapLines) {
				println WordUtils.wrap(serializeNode(node).replaceAll(/\r\n+/, " ").replaceAll(/\r+/, " ").replaceAll(/\n+/, " ").replaceAll(/ +/, " "), 100)
			} else {
				println serializeNode(node)
			}
			break
	}
	//		println "end of printValue"
}

// from https://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
def countLines(File file) throws IOException {
	InputStream is = new BufferedInputStream(new FileInputStream(file));
	try {
		byte[] c = new byte[1024];
		int lineCount = 0;
		int maxColumn = 0
		int columnCount = 0;
		int readChars = 0;
		boolean endsWithoutNewLine = false;
		while ((readChars = is.read(c)) != -1) {
			for (int i = 0; i < readChars; ++i) {
				columnCount++
				if (c[i] == '\n') {
					++lineCount
					if (columnCount > maxColumn) { maxColumn = columnCount }
					columnCount=0
				}
			}
			endsWithoutNewLine = (c[readChars - 1] != '\n');
		}
		if(endsWithoutNewLine) {
			++lineCount;
		}
		//println "maxColumn = "+maxColumn
		return [lineCount, maxColumn]
	} finally {
		is.close();
	}
}
