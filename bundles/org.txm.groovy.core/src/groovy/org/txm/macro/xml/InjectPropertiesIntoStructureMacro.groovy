// Copyright © 2022 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

@Field @Option(name="tableFile", usage="Table File (CSV, ODS or XLSX)", widget="FileOpen", required=true, def="table.ods")
		def tableFile

@Field @Option(name="xmlFilesDirectory", usage="directory of the XML files to patch", widget="Folder", required=false, def="xmlfilesdirectory")
		def xmlFilesDirectory

@Field @Option(name="joinElement", usage="element to patch", widget="String", required=false, def="sp")
		def joinElement

@Field @Option(name="joinAttribute", usage="attribute to test", widget="String", required=false, def="id")
		def joinAttribute

@Field @Option(name="debug", usage="an example boolean", widget="Boolean", required=false, def="true")
		def debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def reader = new org.txm.utils.TableReader(tableFile);

if (!reader.readHeaders()) {
	println "No header."
	return false;
}
def headers = reader.getHeaders()
println headers
if (!headers.contains("id")) {
	println "No 'id' column"
	return false
}
//if (!headers.contains("text-id")) {
//	println "No 'text-id' column"
//	return false
//}

def newProperties = []
newProperties.addAll(headers)
newProperties.remove("id")

def replacements = [:]
while (reader.readRecord()) {
	
	def record = reader.getRecord()
	def joinValue = record["id"]
	
	
	if (joinValue.length() > 0) {
		replacements[joinValue] = reader.getRecord()
	}
}
reader.close()

if (debug) println "replacements=$replacements"
if (debug) println "newProperties=$newProperties"

for (def xmlFile : xmlFilesDirectory.listFiles()) {
	
	if (!xmlFile.isFile()) continue;
	
	if (debug) println "text $xmlFile"
	
	def parser = new groovy.xml.XmlParser()
	parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
	parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
	def doc = parser.parse(xmlFile)
	
	doc.'**'.find { element ->
		
		if (element instanceof groovy.util.Node && element.name().matches(joinElement)) {
			
			String joinValue = ""+element["@$joinAttribute"]
			if (joinValue != null) {
				if (debug) println "patch "+element.name()
				def replacement = replacements[joinValue]
				if (debug) println " $joinValue -> replacement $replacement"
				if (replacement != null) {
					
					for (def property : newProperties) {
						if (property.length() > 0) {
							element["@$property"] = replacement[property]
						}
					}
				}
			}
		}
	}
	
	groovy.xml.XmlUtil.serialize(doc, xmlFile.newWriter("UTF-8"))
}

