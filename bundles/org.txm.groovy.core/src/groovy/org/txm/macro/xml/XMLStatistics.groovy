package org.txm.macro.xml

import java.text.Collator;
import java.util.Locale;

import org.txm.importer.StaxIdentityParser;

class XMLStatistics extends StaxIdentityParser {

	static String UNICATTRS = "id|src|name|href"
	String path = "";
	def paths = [:];

	boolean usePaths=false, useAttributes=false, useAttributeValues=false;

	public XMLStatistics(URL url) {
		super(url);
	}

	public void setUsePaths(boolean b) {
		this.usePaths = b
	}

	public void setUseAttributes(boolean b) {
		this.useAttributes = b
	}

	public void setUseAttributeValues(boolean b) {
		this.useAttributeValues = b;
	}

	public void processCharacters() {
		//super.processCharacters()// don't write :)
	}
	
	public void processEndElement() {
		if (usePaths) {
			path = path.substring(0, path.lastIndexOf("/"))
		}
		//super.processEndElement()// don't write :)
	}

	public void processStartElement() {
		String localname = this.parser.getLocalName();
		if (usePaths) {
			path = path + "/$localname"
		} else {
			path = localname;
		}

		if (this.useAttributes) {
			if (this.useAttributeValues) {
				for(int i = 0 ; i < parser.getAttributeCount() ; i++)
					if (!parser.getAttributeLocalName(i).matches(UNICATTRS))
						path += "@"+parser.getAttributeLocalName(i)+"="+parser.getAttributeValue(i)
			} else {
				for(int i = 0 ; i < parser.getAttributeCount() ; i++)
					path += "@"+parser.getAttributeLocalName(i)
			}
		}


		plusOne()
		//super.processStartElement() // don't write :)
	}

	public void process() {
		File tmp = File.createTempFile("sdfs", "sdfsdf")
		process(tmp)
		tmp.delete();
	}

	private void plusOne() {
		if (!paths.containsKey(path)) {
			paths.put(path, 1)
		} else {
			paths.put(path, paths.get(path)+1)
		}
	}

	public void printCounts(boolean sortByPath) {
		def keys = new ArrayList(paths.keySet());
		if (sortByPath) {
			Collator collator = Collator.getInstance(new Locale("en", "US"));
			collator.setStrength(Collator.TERTIARY);
			Collections.sort(keys, collator)
		} else {
			keys.sort() { key -> -paths.get(key) }
		}
		for (String key : keys) {
			println "$key\t"+paths.get(key)
		}
	}

	public static void processDirectory(File srcdir, File outfile, String ext, boolean usePaths, boolean useAttributes, boolean useAttributeValues) {

		def results = [:]
		def allkeys = new HashSet<String>()

		// counts
		for (File infile : srcdir.listFiles().sort{ it.name }) {
			if (!infile.getName().endsWith(ext)) continue

			XMLStatistics diag = new XMLStatistics(infile.toURI().toURL())
			diag.setUsePaths(usePaths)
			diag.setUseAttributes(useAttributes)
			diag.setUseAttributeValues(useAttributeValues)
			diag.process();
			results.put(infile.getName(), diag.getPaths())
			allkeys.addAll(diag.getPaths().keySet())
			println ""
		}

		allkeys = new ArrayList(allkeys)
		def table = [:]
		def files = new ArrayList<String>(results.keySet())

		// create table header
		def header = []
		header.addAll(files)
		header << "TOTAL"
		table["", header]

		// Create empty cells
		for (int i = 0 ; i < allkeys.size() ; i++) {
			table[allkeys.get(i)] = new ArrayList(files.size())
		}

		// fill cells
		for (int i = 0 ; i < allkeys.size() ; i++) {
			String key = allkeys.get(i)
			for (int j = 0 ; j < files.size() ; j++) {
				def c = results.get(files.get(j)).get(key)
				if (c != null) {
					table[key][j] = c
				} else {
					table[key][j] = 0
				}
			}
		}

		// compute column margins
		def colmargins = [];
		allkeys << "TOTAL"
		table["TOTAL"] = colmargins
		for (int i = 0 ; i < files.size() ; i++) {
			colmargins[i] = 0;
			for (String key : allkeys) {
				colmargins[i] += table[key].get(i)
			}
		}

		// compute line margins
		for (int i = 0 ; i < allkeys.size() ; i++) {
			String key = allkeys.get(i)
			int total = 0;
			for (int j = 0 ; j < files.size() ; j++) {
				total += table[key][j]
			}
			table[key][files.size()] = total
		}

		// sort keys
		allkeys.sort() { it -> -table[it][files.size()] }
		allkeys.remove(0)
		allkeys << "TOTAL"

		// print
		outfile.withWriter("UTF-8") { writer ->
			for(def h : header)
				writer.print "\t"+h
			writer.println ""
			for (int i = 0 ; i < allkeys.size() ; i++) {
				writer.print allkeys[i]
				for (def c : table.get(allkeys[i])) {
					if (c == 0) { writer.print "\t " }
					else { writer.print "\t"+c }
				}
				writer.println ""
			}
		}
	}
	
	public static void main(String[] args) {
		File srcdir = new File("/home/mdecorde/TXM/corpora/BFM/txm/BFM");
		File outdir = new File("/home/mdecorde/TXM/corpora/BFM/txm/BFM");

		// Process a directory
		outdir.mkdir()
		processDirectory(srcdir, new File(outdir,"FFF.csv"), ".xml", false, false, false);
//		processDirectory(srcdir, new File(outdir,"FTF.csv"), ".xml", false, true, false);
//		processDirectory(srcdir, new File(outdir,"FTT.csv"), ".xml", false, true, true);
//		processDirectory(srcdir, new File(outdir,"TFF.csv"), ".xml", true, false, false);
//		processDirectory(srcdir, new File(outdir,"TTF.csv"), ".xml", true, true, false);
//		processDirectory(srcdir, new File(outdir,"TTT.csv"), ".xml", true, true, true);

//		// Process one file
//		File xmlfile = new File("/home/mdecorde/Bureau/matrice/témoignages/CONVERSIONS/ratz/odt-ratz.xml");
//		XmlStatistics diag = new XmlStatistics(xmlfile.toURI().toURL());
//		diag.setUsePaths(true)
//		diag.setUseAttributes(false)
//		diag.setUseAttributeValues(false)
//		diag.process();
//		diag.printCounts(true); 
	}
}
