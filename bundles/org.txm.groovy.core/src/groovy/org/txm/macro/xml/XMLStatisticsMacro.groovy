package org.txm.macro.xml

import org.txm.scripts.*
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

@Field @Option(name="inputDirectory", usage="Dossier qui contient les fichiers à parser", widget="Folder", required=true, def="src")
def inputDirectory;

@Field @Option(name="xmlFilesExtension", usage="Extension des fichiers XML à traiter", widget="String", required=true, def=".xml")
def xmlFilesExtension;

@Field @Option(name="csvFile", usage="FichierTSV résultat", widget="File", required=true, def="file.tsv")
def csvFile;

@Field @Option(name="usePaths", usage="FichierTSV résultat", widget="Boolean", required=true, def="false")
def usePaths = true
@Field @Option(name="useAttributes", usage="FichierTSV résultat", widget="Boolean", required=true, def="false")
def useAttributes = true
@Field @Option(name="useAttributeValues", usage="FichierTSV résultat", widget="Boolean", required=true, def="false")
def useAttributeValues = true

if (!ParametersDialog.open(this)) return;

csvFile = csvFile.getAbsoluteFile()
println "Input directory: "+inputDirectory
println "CSV file: "+csvFile
println "use paths $usePaths"
println "use attributes $useAttributes"
println "use attributes values $useAttributeValues"

XMLStatistics.processDirectory(inputDirectory, csvFile, xmlFilesExtension, usePaths, useAttributes, useAttributeValues);