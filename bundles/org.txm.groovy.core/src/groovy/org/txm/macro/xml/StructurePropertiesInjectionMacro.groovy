// Copyright © 2022 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

@Field @Option(name="tableFile", usage="Table File (CSV, ODS or XLSX)", widget="FileOpen", required=true, def="table.ods")
def tableFile

@Field @Option(name="xmlFilesDirectory", usage="directory of the XML files to patch", widget="Folder", required=false, def="xmlfilesdirectory")
def xmlFilesDirectory

@Field @Option(name="debug", usage="an example boolean", widget="Boolean", required=false, def="true")
def debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def reader = new org.txm.utils.TableReader(tableFile);

if (!reader.readHeaders()) {
	println "No header."
	return false;
}
def headers = reader.getHeaders()
println headers
if (!headers.contains("join-element")) {
	println "No 'join-element' column"
	return false
}

if (!headers.contains("join-property")) {
	println "No 'join-property' column"
	return false
}

if (!headers.contains("join-value")) {
	println "No 'join-value' column"
	return false
}

if (!headers.contains("value")) {
	println "No 'value' column"
	return false
}

if (!headers.contains("property")) {
	println "No 'property' column"
	return false
}

def replacements = []
while (reader.readRecord()) {

	def record = reader.getRecord()
	def joinElement = record["join-element"]
	def joinProperty = record["join-property"]
	def joinValue = record["join-value"]
	def property = record["property"]
	def value = record["value"]
	
	if (joinElement.length() > 0 && joinProperty.length() > 0 && joinValue.length() > 0 && property.length() > 0 && value.length() > 0) {
		println "add: "+reader.getRecord()
		replacements << reader.getRecord()
	}
}
reader.close()

for (def xmlFile : xmlFilesDirectory.listFiles()) {

	if (!xmlFile.isFile()) continue;
	
	if (debug) println "text $xmlFile"
	
	def parser = new groovy.xml.XmlParser()
	parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
	parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
	def doc = parser.parse(xmlFile)
	
	for (def replacement : replacements) {
		if (debug) println " replacement $replacement"
		def joinElement = replacement["join-element"]
		def joinProperty = replacement["join-property"]
		def joinValue = replacement["join-value"]
		def property = replacement["property"]
		def value = replacement["value"]
	
		def toPatch = doc.'**'.find { element ->
			
			if (element instanceof groovy.util.Node 
				&& element.name().matches(joinElement) 
				&& element["@$joinProperty"] != null
				&& element["@$joinProperty"].matches(joinValue)) {
				
    				if (debug) println "  patch ${element.name()} with $property=$value"
    				element["@$property"] = value
    		}
		}
	}
	
	groovy.xml.XmlUtil.serialize(doc, xmlFile.newWriter())
}

