// STANDARD DECLARATIONS
package org.txm.macro.xml

import org.txm.annotation.kr.core.conversion.*
import javax.xml.stream.XMLStreamReader
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

@Field @Option(name="xmlFile", usage="fichier XML lu", widget="FileOpen", required=true, def="in.xml") // /home/mdecorde/TEMP/ANTRACT/AF/all.xlsx
File xmlFile

@Field @Option(name="resultFile", usage="fichier XML resultat", widget="FileSave", required=true, def="out.xml") // /home/mdecorde/TEMP/ANTRACT/AF/all.xlsx
File resultFile

@Field @Option(name = "elementName", usage = "oldType", widget = "String", required = true, def = "w")
String elementName

//@Field @Option(name = "oldType", usage = "oldType", widget = "String", required = true, def = "myattr")
def oldType = null // not needed
	
@Field @Option(name = "newType", usage = "newType", widget = "String", required = true, def = "myattr")
String newType

@Field @Option(name = "separator", usage = "newType", widget = "Separator", required = false, def = "Code")
def separator

@Field @Option(name = "getValueCode", usage = "code of the getValue(XMLStreamReader parser, String localname, String attribute, String value) method", widget = "Text", required = true, def = "\"NEWGROOVE\" + parser.getAttributeValue(null, \"myattr\") ")
String getValueCode

@Field @Option(name = "additionalImportsCode", usage = "code of the getValue(XMLStreamReader parser, String localname, String attribute, String value) method", widget = "Text", required = true, def = "import java.io.File")
String additionalImportsCode

@Field @Option(name = "debug", usage = "show debug messages", widget = "Boolean", required = true, def = "false")
boolean debug

if (!ParametersDialog.open(this)) return;


// not needed
LinkedHashMap<Pattern, String> rules = new LinkedHashMap<>();

XMLTXMFileRuledConversion converter = new XMLTXMFileRuledConversion(xmlFile, rules, elementName, oldType, newType, XMLTXMFileRuledConversion.ABANDON);

// initialize a converter using getValueCode value to fill the getValue(parser, localname, attribute, value) method
converter.converter = gse.evaluate("""import org.txm.annotation.kr.core.conversion.*;
import javax.xml.stream.XMLStreamReader
$additionalImportsCode

return new XMLTXMFileRuledConversion.Converter() {
	public String getValue(XMLStreamReader parser, String localname, String attribute, String value) {
		$getValueCode
	}
};
""");
converter.debug = debug
converter.process(resultFile)