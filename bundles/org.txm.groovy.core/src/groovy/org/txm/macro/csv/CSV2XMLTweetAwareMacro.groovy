// Copyright © 2015 - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

package org.txm.macro.csv

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.*
import org.txm.importer.*
import javax.xml.stream.*
import java.net.URL
//import org.apache.xerces.util

@Field @Option(name="inputFile",usage="CSV File", widget="File", required=false, def="/home/sheiden/Corpus/src/charlie/csv/fr_07jan.csv")
File inputFile;

@Field @Option(name="columnSeparator",usage="column columnSeparator", widget="String", required=false, def=",")
def columnSeparator;

@Field @Option(name="characterEncoding",usage="File characterEncoding", widget="String", required=false, def="UTF-8")
def characterEncoding;

@Field @Option(name="rootTag",usage="Root tag name", widget="String", required=false, def="tweets")
def rootTag;

@Field @Option(name="textTag",usage="Text tag name", widget="String", required=false, def="tweet")
def textTag;

@Field @Option(name="metadataColumnList",usage="metadataColumnList column list separated with comma", widget="String", required=false, def="tweet-id-str,job-id,created-at,from-user,from-user-id-str,from-user-name,from-user-fullname,from-user-followers,from-user-following,from-user-favorites,from-user-tweets,from-user-timezone,to-user,to-user-id-str,to-user-name,source,location-geo,location-geo-0,location-geo-1,iso-language,analysis-state")
def metadataColumnList;

@Field @Option(name="textColumnList",usage="textColumnList column list separated with comma", widget="String", required=false, def="text")
def textColumnList;

if (!ParametersDialog.open(this)) return;

metadataColumnList = metadataColumnList.split(",")
textColumnList = textColumnList.split(",")
textTag = textTag.trim()
rootTag = rootTag.trim()

CsvReader reader = new CsvReader(inputFile.getAbsolutePath(), columnSeparator.charAt(0), Charset.forName(characterEncoding));
if (!reader.readHeaders()) {
	println "Error: no header"
	return
}

def headers = Arrays.asList(reader.getHeaders())
headers.eachWithIndex {str, index -> headers[index] = str.replaceAll(/_/, "-") }
reader.setHeaders(headers as String[])

println "Root tag: $rootTag"
println "Text tag: $textTag"
println "Metadata column(s): $metadataColumnList"
println "Text column(s): $textColumnList"
println "Processing..."

def ok = true
for (String m : metadataColumnList) {
	m = m.trim()
	if (!headers.contains(m)) {
		println "Error: missing metadata column named '$m'"
		ok = false
	}
}
for (String t : textColumnList) {
	t = t.trim()
	if (!headers.contains(t)) {
		println "Error: missing text column named '$t'"
		ok = false
	}
}

if (!ok) {
	println "** CSV2XML: Column(s) missing, aborting..."
	return
}

String name = inputFile.getName()
int idx = name.indexOf(".")
if (idx > 0) name = name.substring(0, idx)
File outputfile = new File(inputFile.getParentFile(), name+".xml")

XMLOutputFactory factory = XMLOutputFactory.newInstance()
Writer output = new OutputStreamWriter(new FileOutputStream(outputfile) , "UTF-8")
XMLStreamWriter writer = factory.createXMLStreamWriter(output)

writer.writeStartDocument("UTF-8", "1.0")
writer.writeCharacters("\n")
writer.writeStartElement(rootTag)
writer.writeCharacters("\n")

int pb_n = 1
def tweet = [:]

def isAllValidXmlChars = { s ->
// xml 1.1 spec http://en.wikipedia.org/wiki/Valid_characters_in_XML
if (!s ==~ /[\u0001-\uD7FF\uE000-\uFFFD\x{10000}-\x{10FFFF}]/) {
  // not in valid ranges
  return false
}
if (s ==~ /[\u0001-\u0008\u000b-\u000c\u000E-\u001F\u007F-\u0084\u0086-\u009F]/) {
  // a control character
  return false
}

// "Characters allowed but discouraged"
if (s ==~ /[\uFDD0-\uFDEF\x{1FFFE}-\x{1FFFF}\x{2FFFE}–\x{2FFFF}\x{3FFFE}–\x{3FFFF}\x{4FFFE}–\x{4FFFF}\x{5FFFE}-\x{5FFFF}\x{6FFFE}-\x{6FFFF}\x{7FFFE}-\x{7FFFF}\x{8FFFE}-\x{8FFFF}\x{9FFFE}-\x{9FFFF}\x{AFFFE}-\x{AFFFF}\x{BFFFE}-\x{BFFFF}\x{CFFFE}-\x{CFFFF}\x{DFFFE}-\x{DFFFF}\x{EFFFE}-\x{EFFFF}\x{FFFFE}-\x{FFFFF}\x{10FFFE}-\x{10FFFF}]/) {
  return false
}

return true
}


def stripInvalidXmlCharacters = { input ->
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < input.length(); i++) {
        char c = input.charAt(i);
//        if (XMLChar.isValid(c)) {
        if (isAllValidXmlChars(c)) {
            sb.append(c);
        }
    }

    return sb.toString();
}


def tokenizeAs
tokenizeAs = { source, matchers ->
	if (matchers.size() > 0) {
		def inputPattern = matchers[0]
		def outputElement = matchers[1]
		def outputAttribute = matchers[2]
		def outputAttValue = matchers[3]
		matchers = matchers.drop(4)

		def m = (source =~ inputPattern)
		def lastMatch = 0
		while(m.find()) {
		   tokenizeAs(source.substring(lastMatch, m.start()), matchers)
		   if (outputAttribute == "type" && outputAttValue == "url") {
		   		writer.writeStartElement("a")
		   		writer.writeAttribute("href", m.group())
		   }
		   writer.writeStartElement(outputElement)
		   writer.writeAttribute(outputAttribute, outputAttValue)
		   writer.writeAttribute("frpos", "NAM")
		   writer.writeAttribute("frlemma", m.group())
		   writer.writeCharacters(m.group())
		   writer.writeEndElement()
		   if (outputAttribute == "type" && outputAttValue == "url") {
		   		writer.writeEndElement()
		   }
		   lastMatch = m.end()
		}
		tokenizeAs(source.substring(lastMatch), matchers)
	} else {
		writer.writeCharacters(source)
	}
}

while (reader.readRecord()) {
	
    writer.writeCharacters("  ")
	writer.writeEmptyElement("pb") // <pb/>
	writer.writeAttribute("n", ""+pb_n++)
	writer.writeCharacters("\n")

    writer.writeCharacters("  ")
	writer.writeStartElement(textTag)
	
	for (String m : metadataColumnList) {
		m = m.trim()
		writer.writeAttribute(m, CleanFile.clean(reader.get(m)))
	}

    writer.writeCharacters("\n")

	for (String t : textColumnList) {
		t = t.trim()
		
    	writer.writeCharacters("    ")
		writer.writeStartElement(t)

		// look for re-tweets
		def c = reader.get(t)
		def retweet = false
		def rt_pattern = ~/(?s:^RT @[^:]+: (.*)$)/
		def rt_m = (c =~ rt_pattern)
		if (rt_m) retweet = true 
		if (retweet) { // it is a re-tweet
			// take off and count all re-tweet prefixes
			def nPrefix = 0
			while (retweet) {
				c = rt_m.group(1)
				rt_m = (c =~ rt_pattern)
				if (rt_m) { retweet = true } else { retweet = false }
				nPrefix++
			}
			def crt = sprintf("RT-%d ", nPrefix)+c
			def n = tweet.get(crt)
			if (n) {
				tweet.put(crt, n+1)
				} else {
				tweet.put(crt, 1)
			}
		} else {
			tweet.put(c, 1)
			tokenizeAs(CleanFile.clean(c),\
					   [/@\p{L}+/, "w", "type", "attag",\
						/#\p{L}+/, "w", "type", "hashtag",\
						/((http|ftp|https):\/\/[\p{L}\-_]+(\.[\p{L}\-_]+)+([\p{L}\-\.,@?^=%&amp;:\/~\+#]*[\p{L}\-\@?^=%&amp;\/~\+#])?)/, "w", "type", "url"])
		}
		writer.writeEndElement() // t
		writer.writeCharacters("\n")
	}
	
    writer.writeCharacters("  ")
	writer.writeEndElement() // textTag
	writer.writeCharacters("\n")
}

writer.writeEndElement() // rootTag
writer.writeCharacters("\n")
writer.close()
output.close()
reader.close()

def nt = tweet.size()
println sprintf("\n%d tweets uniques sur %d lus", nt, pb_n-1)

if (nt > 50) {
	nt = 50
	println "Printing first 50 most frequent [re-]tweets:"
}
// take(nt).
tweet.sort { -it.value }.each { entry ->
	def l = entry.key.length()
	if (l > 0) {
//		if (entry.key.find("On n'a pas peur !!")) {
		println sprintf("[%d] %s...", entry.value, entry.key[0..(Math.min(entry.key.length(), 100)-1)])
//		}
	} else {
	println sprintf("Empty tweet content occurring %d times.", entry.value)
	}
}
