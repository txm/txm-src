package org.txm.macro.csv

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL

@Field @Option(name="inputFile", usage="CSV File", widget="File", required=false, def="file.csv")
File inputFile;

@Field @Option(name="columnSeparator", usage="column columnSeparator", widget="String", required=false, def="\t")
def columnSeparator;

@Field @Option(name="characterEncoding", usage="File characterEncoding", widget="String", required=false, def="UTF-8")
def characterEncoding;

@Field @Option(name="rootTag", usage="Root tag name", widget="String", required=false, def="root")
def rootTag;

@Field @Option(name="textTag", usage="Text tag name", widget="String", required=false, def="unit")
def textTag;

@Field @Option(name="metadataColumnList", usage="metadataColumnList column list separated with comma", widget="String", required=false, def="meta1,meta2")
def metadataColumnList;

@Field @Option(name="textColumnList", usage="textColumnList column list separated with comma", widget="String", required=false, def="textColumnList1,textColumnList2")
def textColumnList;

if (!ParametersDialog.open(this)) return;

metadataColumnList = metadataColumnList.split(",")
textColumnList = textColumnList.split(",")
textTag = textTag.trim()
rootTag = rootTag.trim()



CsvReader reader = new CsvReader(inputFile.getAbsolutePath(), columnSeparator.charAt(0), Charset.forName(characterEncoding));
if (!reader.readHeaders()) {
	println "Error: no header"
	return
}
def headers = Arrays.asList(reader.getHeaders()) 

println "root tag: $rootTag"
println "textColumnList tag: $textTag"
println "columns: $headers"
println "metadataColumnList columns: $metadataColumnList"
println "textColumnList columns: $textColumnList"

def ok = true
for (String m : metadataColumnList) {
	m = m.trim()
	if (!headers.contains(m)) {
		println "Error: missing metadataColumnList column: $m"
		ok = false
	}
}
for (String t : textColumnList) {
	t = t.trim()
	if (!headers.contains(t)) {
		println "Error: missing textColumnList column: $t"
	}
}

if (!ok) { return; }

String name = inputFile.getName()
int idx = name.lastIndexOf(".")
if (idx > 0) name = name.substring(0, idx)
File outputfile = new File(inputFile.getParentFile(), name+".xml")
XMLOutputFactory factory = XMLOutputFactory.newInstance()
FileOutputStream output = new FileOutputStream(outputfile)
XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8")

writer.writeStartDocument("UTF-8","1.0")
writer.writeCharacters("\n") // simple XML formating
writer.writeStartElement(rootTag)

int pb_n = 1;
while (reader.readRecord()) {
	
	writer.writeCharacters("\n") // simple XML formating
	
	writer.writeEmptyElement("pb") // <pb/>
	writer.writeAttribute("n", ""+pb_n++)
	
	writer.writeCharacters("\n") // simple XML formating
	
	writer.writeStartElement(textTag)
	
	for (String m : metadataColumnList) {
		m = m.trim()
		writer.writeAttribute(m, reader.get(m))
	}
	
	for (String t : textColumnList) {
		t = t.trim()
		writer.writeStartElement(t)
		writer.writeCharacters(reader.get(t)) // get textColumnList content
		writer.writeEndElement() // t
	}
	
	writer.writeEndElement() // textTag
}

writer.writeEndElement() // rootTag
writer.close()
output.close()
reader.close()