// STANDARD DECLARATIONS
package org.txm.macro

import org.txm.Toolbox
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.rcp.commands.workspace.UpdateCorpus
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.rcp.commands.workspace.RemoveProperties

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Select a subcorpus or a corpus to process."
	return;
}



@Field @Option(name="new_corpus_name", usage="New corpus name", widget="String", required=false, def="")
		def new_corpus_name

@Field @Option(name="properties_to_remove", usage="comma separated list of properties to export, empty if all properties must be exported", widget="String", required=false, def="")
		def properties_to_remove

//@Field @Option(name="create_a_TXM_file_of_the_corpus", usage="an example string", widget="Boolean", required=false, def="false")
		def create_a_TXM_file_of_the_corpus = false
		
// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

	// END OF PARAMETERS

CQPCorpus corpus = corpusViewSelection
corpus.compute(false)
if (corpus.getID().equals(new_corpus_name)) {
	println "Error: cannot use the same corpus name of the selecte corpus ($corpus) and the name $new_corpus_name"
	return
}

println "Get text ids from subcorpus..."

def qr = corpus.query(new CQLQuery("[] expand to text"), "TMP", false);
def starts = qr.getStarts()
def CQI = CQPSearchEngine.getCqiClient()
def structPos = CQI.cpos2Struc(corpus.getTextIdStructuralUnitProperty().getQualifiedName(), starts)
def ids = CQI.struc2Str(corpus.getTextIdStructuralUnitProperty().getQualifiedName(), structPos) as List
if (ids.size() == 0) {
	println "Error: no text found in $corpus"
	return false
}
if (ids.size() == corpus.getProject().getTextsID().size()) {
	println "Error: no text will be removed from the new corpus"
	return false
}

println "IDS (${ids.size()})=$ids"

File outfile = new File(corpus.getProjectDirectory().getParentFile(), new_corpus_name+".txm")
println "Export corpus $outfile..."
corpus.export(outfile, new LogMonitor("Export "+corpus), new HashSet<String>(), "0.8.0", new_corpus_name)

def project = Toolbox.workspace.getProject(new_corpus_name)
if (project != null) {
	println "Delete previous corpus $new_corpus_name"
	project.delete();
}

println "Load corpus..."
project = Toolbox.workspace.installCorpus(outfile)
if (project == null) {
	println "Error: corpus was not loaded. stop."
	return
}

if (ids.size() > 0) {
	println "Delete text files..."
	File txmdir = new File(project.getProjectDirectory(), "txm/"+project.getName())
	println txmdir
	for (File xmltxmFile : txmdir.listFiles() ) {
		String name = FileUtils.stripExtension(xmltxmFile.getName())
		if (!ids.contains(name)) {
//			println "Delete: $xmltxmFile"
			xmltxmFile.delete()
		}
	}
}

corpus = project.getFirstChild(MainCorpus.class)
corpus.compute(false)
if (properties_to_remove.length() > 0) {
	println "Delete properties $properties_to_remove..."
	LinkedHashSet<String> propertiesSet = new LinkedHashSet<String>();
	propertiesSet.addAll(properties_to_remove.split(",") as List)
	
	def job = RemoveProperties.apply(corpus, propertiesSet)
	if (job != null) job.join();
}

println "Update corpus $new_corpus_name..."
project.setDirty(false);// false=dont propagate dirty
project.setNeedToBuild();
project.setDoMultiThread(false); // too soon
project.setDoUpdate(true, true);
String currentModule = project.getImportModuleName();
if (!(currentModule.equals("xtz") || currentModule.equals("transcriber"))) { //$NON-NLS-1$ //$NON-NLS-2$
	project.setImportModuleName("xtz"); //$NON-NLS-1$
}
project.compute(monitor, true)
corpus.compute(false)

if (create_a_TXM_file_of_the_corpus) {
	println "Re-export corpus $outfile..."
	corpus.export(outfile, new LogMonitor("Final export $corpus"), new HashSet<String>(), "0.8.0", null)
} else {
	outfile.delete();
}

monitor.syncExec(new Runnable() {
	public void run() {
		org.txm.rcp.views.corpora.CorporaView.refresh()
		org.txm.rcp.views.corpora.CorporaView.reveal(corpus)
	}
});

println "Done: $outfile"

