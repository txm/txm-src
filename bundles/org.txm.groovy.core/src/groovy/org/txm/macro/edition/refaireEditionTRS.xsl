<?xml version="1.0"?>


<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:atilf="http://www.atilf.fr" version="2.0">

	<xsl:output method="xml" />

	<xsl:param name="soundFile" />




	<xsl:function name="atilf:traduireTemps">
		<xsl:param name="t" />

		<xsl:choose>
			<xsl:when test="count($t) != 0">
				<xsl:analyze-string select="$t"
					regex="([0-9]+):([0-9]+):([0-9]+)">
					<xsl:matching-substring>
						<xsl:value-of
							select="number(regex-group(1))*3600+number(regex-group(2))*60+number(regex-group(3))" />
					</xsl:matching-substring>
					<xsl:non-matching-substring>
						<xsl:text>0</xsl:text>
					</xsl:non-matching-substring>
				</xsl:analyze-string>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>


	<xsl:template match="html">
		<xsl:copy>
			<xsl:apply-templates select="meta" />
			<xsl:apply-templates select="head" />
			<xsl:apply-templates select="link" />
			<xsl:apply-templates
				select="*[not(self::meta) and not(self::head) and
				   not(self::link) and not(self::body) and not(self::tei:body)]" />
			<xsl:apply-templates select="tei:body" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="@* | text() | comment() | processing-instruction()">
		<xsl:copy />
	</xsl:template>



	<xsl:template match="tei:body">
		<xsl:copy>
			<script>
				<!-- Element.prototype.documentOffsetTop = function () { return this.offsetTop 
					+ ( this.offsetParent ? this.offsetParent.documentOffsetTop() : 0 ); }; function 
					myFunc2(event) { document.getElementById('tracktime').innerHTML = Math.floor(event.currentTime) 
					+ ' / ' + Math.floor(event.duration); }; function myFunc3(event) { var x 
					= document.getElementsByTagName("span"); for (index = 0; index &lt; x.length; 
					index++){ if ((x[index].getAttribute("begin") &lt; event.currentTime) &amp;&amp; 
					(x[index].getAttribute("end") &gt; event.currentTime)){ x[index].className 
					= "spk_hi"; /*x[index].scrollIntoView( true );*/ /*var top = x[index].documentOffsetTop() 
					- ( window.innerHeight / 2 ); window.scrollTo(0, top );*/ var topPos = x[index].offsetTop; 
					document.getElementById('scrollDiv').scrollTop = topPos-110; } else{ x[index].className="spk"; 
					} }; }; -->

				function moveInSound(t){
				var player = document.getElementById("audioPlay");

				if (player.paused){
				player.currentTime = t;
				player.play();
				}
				else{
				player.pause();
				}
				};

				function stopSound(t){
				var player = document.getElementById("audioPlay");

				player.pause();
				};
			</script>
			<audio id="audioPlay" controls="controls"
				style="height:100px; display:none">
				<source src="{$soundFile}" />
			</audio>
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>


	<!-- En fait, on veut mettre un bouton play, stop -->
	<xsl:template match="span[@class='spk']">

		<!-- <button type="button" style='background: url("play.png") no-repeat; 
			width:25px;height:25px;background-size:100%;'> <xsl:attribute name="onclick"> 
			<xsl:text>moveInSound(</xsl:text><xsl:value-of select="atilf:traduireTemps(following::span[@class='sync'][1]/text())"/><xsl:text>)</xsl:text> 
			</xsl:attribute> <xsl:text> </xsl:text> </button> -->


		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="onclick">
	<xsl:text>moveInSound(</xsl:text><xsl:value-of
				select="atilf:traduireTemps(following::span[@class='sync'][1]/text())" /><xsl:text>)</xsl:text>
      </xsl:attribute>
			<xsl:apply-templates select="node()" />

		</xsl:copy>

		<!-- <button type="button" style='background: url("pause.png") no-repeat; 
			width:25px;height:25px;background-size:100%;'> <xsl:attribute name="onclick"> 
			<xsl:text>stopSound(</xsl:text><xsl:value-of select="atilf:traduireTemps(following::span[@class='sync'][1]/text())"/><xsl:text>)</xsl:text> 
			</xsl:attribute> <xsl:text> </xsl:text> </button> -->

	</xsl:template>

	<xsl:template match="span[@class='sync']">

		<!-- <button type="button" style='background: url("play.png") no-repeat; 
			width:25px;height:25px;background-size:100%;'> <xsl:attribute name="onclick"> 
			<xsl:text>moveInSound(</xsl:text><xsl:value-of select="atilf:traduireTemps(following::span[@class='sync'][1]/text())"/><xsl:text>)</xsl:text> 
			</xsl:attribute> <xsl:text> </xsl:text> </button> -->


		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="onclick">
	<xsl:text>moveInSound(</xsl:text><xsl:value-of
				select="atilf:traduireTemps(./text())" /><xsl:text>)</xsl:text>
      </xsl:attribute>
			<xsl:apply-templates select="node()" />

		</xsl:copy>

		<!-- <button type="button" style='background: url("pause.png") no-repeat; 
			width:25px;height:25px;background-size:100%;'> <xsl:attribute name="onclick"> 
			<xsl:text>stopSound(</xsl:text><xsl:value-of select="atilf:traduireTemps(following::span[@class='sync'][1]/text())"/><xsl:text>)</xsl:text> 
			</xsl:attribute> <xsl:text> </xsl:text> </button> -->

	</xsl:template>

	<xsl:template match="span[@class='word']">

		<!-- <button type="button" style='background: url("play.png") no-repeat; 
			width:25px;height:25px;background-size:100%;'> <xsl:attribute name="onclick"> 
			<xsl:text>moveInSound(</xsl:text><xsl:value-of select="atilf:traduireTemps(following::span[@class='sync'][1]/text())"/><xsl:text>)</xsl:text> 
			</xsl:attribute> <xsl:text> </xsl:text> </button> -->


		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:attribute name="onclick">
	<xsl:text>moveInSound(</xsl:text><xsl:value-of
				select="atilf:traduireTemps(preceding::span[@class='sync'][1]/text())" /><xsl:text>)</xsl:text>
      </xsl:attribute>
			<xsl:apply-templates select="node()" />

		</xsl:copy>

		<!-- <button type="button" style='background: url("pause.png") no-repeat; 
			width:25px;height:25px;background-size:100%;'> <xsl:attribute name="onclick"> 
			<xsl:text>stopSound(</xsl:text><xsl:value-of select="atilf:traduireTemps(following::span[@class='sync'][1]/text())"/><xsl:text>)</xsl:text> 
			</xsl:attribute> <xsl:text> </xsl:text> </button> -->

	</xsl:template>


</xsl:stylesheet>