// STANDARD DECLARATIONS
package org.txm.macro.edition

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.objects.*
import org.txm.utils.io.FileCopy

@Field @Option(name="source_corpus", usage="the old edition name", widget="String", required=false, def="CORPUSSRC")
def source_corpus

@Field @Option(name="destination_corpus", usage="the old edition name", widget="String", required=false, def="CORPUSDEST")
def destination_corpus

@Field @Option(name="source_edition_name", usage="the old edition name", widget="String", required=false, def="default")
def source_edition_name

@Field @Option(name="destination_edition_name", usage="the old edition name", widget="String", required=false, def="default2")
def destination_edition_name

@Field @Option(name="default_destination_edition_names", usage="the old edition name", widget="String", required=false, def="default,default2")
def default_destination_edition_names

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

if (source_corpus == destination_corpus && source_edition_name == destination_edition_name) {
	println "Error: can't import the same edition in the same corpus"
	return false
}

Project src_project = Workspace.getInstance().getProject(source_corpus)
Project dest_project = Workspace.getInstance().getProject(destination_corpus)

def srcDeclaredEditions = src_project.getEditionNames()
def destDeclaredEditions = dest_project.getEditionNames()
// 0- ensure parameters

if (default_destination_edition_names.length() > 0) {
	def split = default_destination_edition_names.split(",") as List
	split.remove(destination_edition_name)
	if (!srcDeclaredEditions.containsAll(split)) {
		println "Error: the new default editions ($default_destination_edition_names) does not match the source corpus editions: "+srcDeclaredEditions+" and "+destination_edition_name
		return false
	}
}

if (src_project == null) {
	println "Error: could not find source corpus with name=$source_corpus"
	return false
}

if (dest_project == null) {
	println "Error: could not find destination corpus with name=$destination_corpus"
	return false
}

if (!srcDeclaredEditions.contains(source_edition_name)) {
	println "Error: the $source_edition_name edition is not declared in the source corpus: $source_corpus"
	return false
}

if (destDeclaredEditions.contains(destination_edition_name)) {
	println "Error: the $destination_edition_name edition is already declared in the destination corpus: $destination_corpus"
	return false
}

File srcEditionDirectory = new File(src_project.getProjectDirectory(), "HTML/$source_corpus/$source_edition_name")
if (!srcEditionDirectory.exists()) {
	println "Error: the $srcEditionDirectory source edition directory doesn't exist."
	return false
}
File destEditionDirectory = new File(dest_project.getProjectDirectory(), "HTML/$destination_corpus/$destination_edition_name")
if (destEditionDirectory.exists()) {
	println "Error: the $destEditionDirectory destination edition directory already exists."
	return false
}

// 1- copy the HTML directories
if (FileCopy.copydir(srcEditionDirectory, destEditionDirectory)) {
	println "Error: the $srcEditionDirectory could not be copied to the $destEditionDirectory directory"
	return false
}

// 2- copy the edition declaration
EditionDefinition srcEditionDeclaration = src_project.getEditionDefinition(source_edition_name)
EditionDefinition destEditionDeclaration = dest_project.getEditionDefinition(destination_edition_name)

//later use srcEditionDeclaration.copyParametersTo(destEditionDeclaration)
destEditionDeclaration.setPageBreakTag(srcEditionDeclaration.getPageElement())
destEditionDeclaration.setWordsPerPage(srcEditionDeclaration.getWordsPerPage())
destEditionDeclaration.setBuildEdition(srcEditionDeclaration.getBuildEdition())
if (srcEditionDeclaration.getImagesDirectory() != null) destEditionDeclaration.setImagesDirectory(srcEditionDeclaration.getImagesDirectory())

// 3- update the dest Text editions

int n = 0;
for (Text src_text : src_project.getTexts()) {
	Edition src_edition = src_text.getEdition(source_edition_name)
	if (edition == null) continue // nothing to do for this text
	
	Text dest_text = dest_project.getText(src_text.getText())
	if (dest_text == null) {
		println "Error: cannot find the destination text matching: $src_text"
		return false;
	}
	Edition dest_edition = dest_text.getEdition(destination_edition_name)
	if (dest_edition != null) {
		println "Warning: a destination edition already exists for the $dest_text text -> deleted!"
		dest_edition.delete()
	}
	dest_edition = new Edition(dest_text)
	dest_edition.setName(destination_edition_name)
	dest_edition.saveParameter("corpus", destination_corpus.getID())
	dest_edition.pPageNames = src_edition.pPageNames // replace with getter&setter when published in TXM 0.8.x+
	dest_edition.pPageFirstWordIds = src_edition.pPageFirstWordIds
	dest_edition.compute(false)
	
	n++
}

if (n > 0) {
	println "$n text editions have been created"
}

// 4- update default editions if necessary
if (default_destination_edition_names.length() > 0) {
	def split = default_destination_edition_names.split(",") as List
	if (split.size() > 0) {
		dest_project.setDefaultEditionName(defaultEditions.join(","))
		println "Default editions updated to: "+dest_project.getDefaultEditionName()
	}
}
// 5- finally save preferences changes
dest_project.save()
println "Done."
