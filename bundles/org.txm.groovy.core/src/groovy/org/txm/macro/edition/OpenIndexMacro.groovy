// Copyright © 2021 ENS Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.edition

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.Toolbox
import org.txm.macro.cqp.CQPUtils
import org.txm.rcp.commands.*

scriptName = this.class.getSimpleName()

utils = new CQPUtils()

corpora = utils.getCorpora(this)

if ((corpora == null) || corpora.size() > 1) {
	println "** $scriptName: please select a corpus in the Corpus view or provide a corpus name. Aborting."
	return false
}

corpus = corpora[0].getMainCorpus()
corpusName = corpus.getName()

indexPath = new File(Toolbox.getTxmHomePath(), "corpora/$corpusName/HTML/$corpusName/default/index.html")

if (!(indexPath.exists())) {
        println "** $scriptName: cannot find $indexPath. Aborting."
        return false
}

if (!(indexPath.canRead())) {
        println "** $scriptName: cannot open $indexPath. Aborting."
        return false
}

monitor.syncExec(new Runnable() {
	@Override
	public void run() {
		try {
			OpenBrowser.openfile(indexPath.getAbsolutePath())
		} catch (e) {
			println "** $scriptName: error while opening file $indexPath. Aborting."
			println "** "+e.getLocalizedMessage()
		}
	}
})

return true


