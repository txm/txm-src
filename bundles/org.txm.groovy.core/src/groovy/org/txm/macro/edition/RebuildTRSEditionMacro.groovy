//package org.txm.macro.xsl;
// STANDARD DECLARATIONS

//import org.kohsuke.args4j.*
//import groovy.transform.Field
//import org.txm.rcpapplication.swt.widget.parameters.*
//import org.txm.importer.ApplyXsl2;

package org.txm.macro.edition
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.objects.*
import org.txm.searchengine.cqp.corpus.*;
import org.w3c.dom.*
import org.txm.importer.*
import org.txm.Toolbox
import java.io.*
import groovy.xml.XmlSlurper
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.txm.rcpapplication.commands.*
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.txm.utils.io.FileCopy

try { println "ARGS=$args";} catch(Exception e) {args = [:]}

def corpus = null
if (args.size() == 0) {
	if (!(corpusViewSelection instanceof MainCorpus)) {
		println "This marcro works with a MainCorpus selection. Aborting"
		return;
	}
	println "Working on $corpusViewSelection corpus"
	corpus = corpusViewSelection
} else {
	monitor = args["monitor"];
}

// Les paramètres dont on a besoin :
// Nom du Corpus dans TXM on le récupère de l amême façon que EditionUpdaterMacro...
// Nom du dossier dans lequel sont les trs utilisables avec transcriber (i.e. ceux à partir desquels on trouve les fichiers sons).
// Nom de l'édition à modifier.


println "Corpora selection: "+corpusViewSelection
println "corpusName ="+corpus.getName()
def corpusName=corpusViewSelection.getName()

//Je voudrais mettre en dur la feuille xsl ? (ou pas....)
// BEGINNING OF PARAMETERS
//@Field @Option(name="corpusTXM",usage="nom du corpus dans TXM", widget="String", required=true, def="")
def corpusTXM=corpusViewSelection

@Field @Option(name="placeInitCorpus", usage="directory où se trouve/trouvait initialement le corpus : trs et fichiers Sons", widget="Folder", required=true, def="")
def placeInitCorpus

@Field @Option(name="editionName",usage="The edition name to produce", widget="String", required=false, def="default")
String editionName

// BEGINNING OF PARAMETERS
//@Field @Option(name="XSLFile",usage="an example file", widget="File", required=true, def="file.xsl")
//def XSLFile = new File(System.getProperty("user.home"),"TXM/xsl/identity.xsl")
//def XSLFile

//@Field @Option(name="intputDirectory",usage="an example folder", widget="Folder", required=true, def="in")
//def intputDirectory = new File(System.getProperty("user.home"),"xml/TESTS2/xml")
//def intputDirectory

//@Field @Option(name="outputDirectory",usage="an example folder", widget="Folder", required=true, def="out")
////def outputDirectory = new File("/tmp/xsltest")
//def outputDirectory

//@Field @Option(name="parameters",usage="an example folder", widget="Text", required=false, def="")
//def parameters = ""

//@Field @Option(name="debug",usage="Show debug messages, value = true|false", widget="String", required=true, def="false")
//def debug

if (!ParametersDialog.open(this)) return;
// END OF PARAMETERS

binDirectory = corpus.getProjectDirectory()
File HTMLDirectory = new File(binDirectory, "HTML")
File HTMLCorpusDirectory = new File(HTMLDirectory, corpusName)
println HTMLCorpusDirectory
File defaultEditionDirectory = new File(HTMLCorpusDirectory, editionName)
println defaultEditionDirectory

//On cherche récursivement les trs dans placeInitCorpus

println "place init corpus : "+placeInitCorpus.getAbsolutePath()

placeInitCorpus.eachFileRecurse{ AFile ->
	String name = AFile.getName()
    if (name.matches(/.+\.trs/)){
		println name
		println "Path absolu :"+AFile.getAbsolutePath()
		//on le parse pour trouver le audio file name
		parser=new XmlSlurper();
		parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
		parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		list=parser.parse(AFile)
		def transNode=list
		def fichierSon=transNode.@audio_filename
		println "   fichierSon : ->"+fichierSon
		
		File parentFolder = new File(AFile.getParent());
		File soundF = new File(parentFolder, ""+fichierSon);
		String fichierSonAbsolu = soundF.getCanonicalPath(); // may throw IOException
		//String fichierSonAbsolu="toto"
		println "Son absolu :"+fichierSonAbsolu
		
		//maintenant, on veut trouver le fichier son pour de vrai !
		//C'est là qu'il faut voir si on trouve : 
		// fichierSonAbsolu, ou fichierSonAbsolu+.mp3 ou fichierSonAbsolu+.MP3 ou
		// fichierSonAbsolu+.wav ou fichierSonAbsolu+.WAV
		//Deux types de problèmes possibles : n'en trouver aucun
		//                                    en trouver plusieurs !
		File sonFichier=new File(fichierSonAbsolu)
		if (sonFichier.exists()){
			println "   OK, fichier Son trouvé";
			found = true;
		}
		else{
			String[] exts = [".wav", ".WAV", ".mp3", ".MP3"];
			found = false;
			fini = false;
			index=0;
			while (!found && !fini){
				fichierSonAbsoluTest=fichierSonAbsolu+exts[index];
				println("   On teste "+fichierSonAbsoluTest);
				sonFichier=new File(fichierSonAbsoluTest);
				found=sonFichier.exists();
				if (found){
					fichierSonAbsolu = fichierSonAbsoluTest;
				}
				else{
					index = index +1;
					fini = (index >= exts.length);
				}
			}
		}
		if (!found){
			println(" Pas de fichier son trouvé pour : "+name);
		}	
		else{
		
				baseN = name[0..-4]
				htmlRegex = ".*${baseN}.*\\.html"
				println "HtmlRegex = "+htmlRegex
		
				String xslPath = Toolbox.getTxmHomePath()+"/scripts/groovy/user/org/txm/macro/edition/"+"refaireEditionTRS.xsl"
				ApplyXsl2 a = new ApplyXsl2(xslPath)
				outputDirectory = defaultEditionDirectory
				defaultEditionDirectory.eachFileRecurse{ HtmlFile ->
				//println "   ?"+HtmlFile
				if (HtmlFile ==~ /${htmlRegex}/){
					println "   OK "+HtmlFile
					a.SetParam("soundFile", fichierSonAbsolu)
					def outFile = new File(outputDirectory, HtmlFile.getName()+".bis")
					a.process(HtmlFile, outFile);
					// On renome les fichiers :
					println "nouveau nom : "+defaultEditionDirectory.getCanonicalPath()+"/"+HtmlFile.getName()+".orig"
					oldName=HtmlFile.getCanonicalPath()
					//HtmlFile.renameTo (defaultEditionDirectory.getCanonicalPath()+"/"+HtmlFile.getName()+".orig")
					HtmlFile.renameTo (defaultEditionDirectory.getCanonicalPath()+File.separator+HtmlFile.getName()+".orig")
					// Il semble que windows n'aime pas le renomage....
					//outFile.renameTo oldName
					//donc on teste une copie...
					HtmlFile << outFile.text
				
					a.resetParams()
				
				}
			
			}
		}
	}
}
