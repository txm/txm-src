// STANDARD DECLARATIONS
package org.txm.macro.edition

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.objects.*

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Please select a main corpus to update the editions"
	return false
}
@Field @Option(name="oldName", usage="the old edition name", widget="String", required=false, def="default")
def oldName

@Field @Option(name="newName", usage="the old edition name", widget="String", required=false, def="default")
def newName

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

MainCorpus corpus = corpusViewSelection
Project project = corpus.getProject()
String pname = project.getName()

def declaredEditions = project.getEditionNames()

// 0- ensure parameters
if (!declaredEditions.contains(oldName)) {
	println "Error: the $oldName edition is not declared in the corpus: $declaredEditions"
	return false
}
if (declaredEditions.contains(newName)) {
	println "Error: the $newName edition is already declared in the corpus: $declaredEditions"
	return false
}

File oldEditionDirectory = new File(project.getProjectDirectory(), "HTML/$pname/$oldName")
if (!oldEditionDirectory.exists()) {
	println "Error: the $oldEditionDirectory edition directory doesn't exist."
	return false
}
File newEditionDirectory = new File(project.getProjectDirectory(), "HTML/$pname/$newName")
if (newEditionDirectory.exists()) {
	println "Error: the $newEditionDirectory edition directory already exists."
	return false
}

// 1- move the HTML directories
if (!oldEditionDirectory.renameTo(newEditionDirectory)) {
	println "Error: the $oldEditionDirectory could not be moved to the $newEditionDirectory directory"
	return false
}

// 2- rename the edition declaration
EditionDefinition oldEditionDeclaration = project.getEditionDefinition(oldName)
EditionDefinition newEditionDeclaration = project.getEditionDefinition(newName)

//later use oldEditionDeclaration.copyParametersTo(newEditionDeclaration)
newEditionDeclaration.setPageBreakTag(oldEditionDeclaration.getPageElement())
newEditionDeclaration.setWordsPerPage(oldEditionDeclaration.getWordsPerPage())
newEditionDeclaration.setBuildEdition(oldEditionDeclaration.getBuildEdition())
if (oldEditionDeclaration.getImagesDirectory() != null) newEditionDeclaration.setImagesDirectory(oldEditionDeclaration.getImagesDirectory())

oldEditionDeclaration.delete() // later this method will return false if the deletion failed

// 3- update the Text editions

int n = 0;
for (Text text : project.getTexts()) {
	Edition edition = text.getEdition(oldName)
	if (edition == null) continue // nothing to do for this text
	
	edition.setName(newName)
	edition.compute(false)
	n++
}

if (n > 0) {
	println "$n text editions have been updated"
}

// 4- update default editions if necessary
def defaultEditions = project.getDefaultEditionName().split(",") as List
int idx = defaultEditions.indexOf(oldName)
if (idx >= 0) {
	defaultEditions[idx] = newName
	project.setDefaultEditionName(defaultEditions.join(","))
	println "Default editions updated to: "+project.getDefaultEditionName()
}


// 5- finally save preferences changes
project.save()
println "Done."