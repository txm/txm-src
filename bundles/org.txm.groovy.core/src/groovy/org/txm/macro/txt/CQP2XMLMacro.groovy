package org.txm.macro.txt

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL

@Field @Option(name="inputDirectory", usage="CQP directory", widget="Folder", required=false, def="")
		File inputDirectory

@Field @Option(name="encoding", usage="File encoding", widget="String", required=false, def="UTF-8")
		String encoding

@Field @Option(name="separator", usage="Column separator (TAB by default)", widget="String", required=false, def="\\t")
		String separator

@Field @Option(name="columns", usage="Column names separated by comma ('word,pos,lemma' by default)", widget="String", required=true, def="word,pos,lemma")
		String columns

def rootTag = "text"

if (!ParametersDialog.open(this)) return;

encoding = encoding.trim()

if (separator == "\\t") {
	separator = "\t"
} else {
	separator = separator[0]
}

columnNames = columns.trim().split(",")

outputDirectory = new File(inputDirectory, "out")
outputDirectory.mkdir()

println "processing: "+inputDirectory

for (File inputfile : inputDirectory.listFiles()) {
	if (inputfile.isDirectory() || inputfile.isHidden() || !inputfile.getName().endsWith(".cqp")) {
		continue // ignore
	}
	
	println " file: "+inputfile
	
	String name = inputfile.getName()
	int idx = name.lastIndexOf(".")
	
	if (idx > 0) {
		name = name.substring(0, idx)
	}
	
	File outputfile = new File(outputDirectory, name+".xml")
	
	// output = new FileOutputStream(outputfile)
	
	output = new PrintWriter(new BufferedWriter(new FileWriter(outputfile, true)));
	outputStr = new StringWriter()
	factory = XMLOutputFactory.newInstance()
	writer = factory.createXMLStreamWriter(outputStr)
	
	writer.writeStartDocument("UTF-8","1.0")
	writer.writeCharacters("\n") // manage XML reserved characters
	writer.writeStartElement(rootTag)
	writer.writeAttribute("id", name)
	writer.writeCharacters("\n")
	
	inputfile.withReader(encoding) { reader ->
		
		def line
		while ((line = reader.readLine()) != null) {
			if (line.length() > 0) {
				if (line[0] == "<") {
					writer.flush()
					outputStr.write(line)
					outputStr.flush()
				} else {
					writer.writeStartElement("w")
					def word = ""
					println "line = "+line
					println "columnNames = "+columnNames
					line.split(separator).eachWithIndex { columnValue, index ->
						if (columnNames[index] == "word") {
							word = columnValue
						} else {
							writer.writeAttribute(columnNames[index], columnValue)
						}
					}
					writer.writeCharacters(word)
					writer.writeEndElement() // w
				}
				writer.writeCharacters("\n")
			}
		}
	}
	writer.writeEndElement() // rootTag
	writer.close()
	output.println(outputStr.toString())
	output.close()
}
