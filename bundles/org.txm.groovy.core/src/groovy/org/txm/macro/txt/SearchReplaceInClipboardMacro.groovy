package org.txm.macro.txt
// Copyright © 2024 - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// imports

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.rcp.utils.IOClipboard

// parameters

@Field @Option(name="find", usage='''Expression régulière contenant d'éventuels codes d'échappement :
\\t 	Insérer une tabulation dans la chaine à cet endroit.
\\b 	Insérer un 'Retrour Arrière' (backspace) dans la chaine à cet endroit.
\\n 	Insérer un saut de ligne dans la chaine à cet endroit.
\\r 	Insérer un retour chariot dans la chaine à cet endroit.
\\f 	Insérer un saut de page (formfeed) dans la chaine à cet endroit.
\\' 	Insérer un caractère quillemet simple dans la chaine à cet endroit.
\\\" 	Insérer un caractère quillemet double dans la chaine à cet endroit.
\\\\ 	Insérer un antislash (backslash) character dans la chaine à cet endroit.''', widget="String", required=true, def='')
def find

@Field @Option(name="replaceWith", usage="Chaîne de remplacement", widget="String", required=true, def='')
def replaceWith

if (!ParametersDialog.open(this)) return

/* parse Java escape characters in replace string

\t 	Insérer une tabulation dans la chaine à cet endroit.
\b 	Insérer un backspace dans la chaine à cet endroit.
\n 	Insérer un newline dans la chaine à cet endroit.
\r 	Insérer un carriage return dans la chaine à cet endroit.
\f 	Insérer un formfeed dans la chaine à cet endroit.
\' 	Insérer un single quote character dans la chaine à cet endroit.
\" 	Insérer un double quote character dans la chaine à cet endroit.
\\ 	Insérer un backslash character dans la chaine à cet endroit.
*/
replaceWith = org.apache.commons.lang.StringEscapeUtils.unescapeJava(replaceWith)

monitor.syncExec(new Runnable() {
	public void run() {
		try {
			s = IOClipboard.read()
			if (s == null || s.length() == 0) {
				println "No text in clipboard. Aborting."
				return
			}
			
			println "--> Clipboard content on input: \""+s+"\""
			
			def matcher = ( s =~ find )
			
			if (matcher.getCount() > 0) {
				s = matcher.replaceAll(replaceWith)
				IOClipboard.write(s)
				println matcher.getCount()+" replacements."
			} else {
				println "No replacements."
			}
			
			println "<-- Clipboard content on output: \""+s+"\""
			
		} catch (e) {
			e.printStackTrace()
		}
 }
})

return 1
