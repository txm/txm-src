// STANDARD DECLARATIONS
package org.txm.macro.txt

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

// BEGINNING OF PARAMETERS

@Field @Option(name="folder", usage="input directory", widget="Folder", required=false, def="C:/Temp")
def folder

@Field @Option(name="ext", usage="file extension", widget="String", required=false, def=".txt")
def ext

@Field @Option(name="encoding", usage="files encoding", widget="String", required=false, def="UTF-8")
def encoding


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

println "Mergin files in $folder with extension=$ext"

File outdir = new File(folder, "out");
outdir.deleteDir()
outdir.mkdir()
File errordir = new File(folder, "error");
errordir.deleteDir()
errordir.mkdir()

def files = folder.listFiles().findAll{File it-> it.getName().endsWith(ext)};
files.sort()
if (files.size() == 0) return;
def toMerge = []
def prefix = null
def prefix2;

for (File current : files) {
	prefix2 = current.getName();
	int idx = prefix2.indexOf(ext)
	if (idx <= 0) {
		println "error $ext file $current"
		new File(errordir, current.getName()) << current.getText(encoding)
		toMerge << current
		continue;
	}
	prefix2 = prefix2.substring(0, idx)
	int idx2 = prefix2.indexOf("-")
	if (idx2 <= 0) {
		println "error '-' file $current"
		new File(errordir, current.getName()) << current.getText(encoding)
		toMerge << current
		continue;
	}
	String snumber = prefix2.substring(idx2+1);
	prefix2 = prefix2.substring(0, idx2)
	
	if (!prefix2.equals(prefix)) {	
		println "merging $toMerge"
		File mergedFile = new File(outdir, prefix+ext)
		for (def f : toMerge) mergedFile << f.getText(encoding)
		toMerge.clear()
	}
	
	toMerge << current
	prefix = prefix2
}

if (toMerge.size() > 0) {
	File current = toMerge[0]
	prefix2 = current.getName();
	int idx = prefix2.indexOf(ext)
	prefix2 = prefix2.substring(0, idx)
	int idx2 = prefix2.indexOf("-")
	String snumber = prefix2.substring(idx2+1);
	prefix2 = prefix2.substring(0, idx2)
	
		println "merging $toMerge"
		File mergedFile = new File(outdir, prefix+ext)
		for (def f : toMerge) mergedFile << f.getText(encoding)
		toMerge.clear()
		
	println "merging last files: $toMerge"
	println "in $mergedFile"
}
