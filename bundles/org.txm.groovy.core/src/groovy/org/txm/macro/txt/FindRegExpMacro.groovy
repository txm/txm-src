package org.txm.macro

import org.kohsuke.args4j.*

import groovy.transform.Field

import java.nio.charset.Charset

import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.*

import javax.xml.stream.*

import java.net.URL
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Field @Option(name="inputDirectory",usage="TXT directory", widget="Folder", required=false, def="directory path")
File inputDirectory;
@Field @Option(name="regexp",usage="Regular expression to match", widget="String", required=false, def="regular expression")
String regexp;
@Field @Option(name="encoding",usage="File encoding", widget="String", required=false, def="UTF-8")
String encoding;

if (!ParametersDialog.open(this)) return;

def p = /$regexp/

println "processing: "+inputDirectory.listFiles()
for (File inputfile : inputDirectory.listFiles()) {
	if (inputfile.isDirectory()) continue // ignore
	
	def lines = []
	inputfile.eachLine("UTF-8") { line, n ->
		def m = line =~ p
		if ( m.size() > 0) {
			lines << "  line $n: $line"
		}
	}
	
	println inputfile.getName() + " "+lines.size() + " match" +((lines.size() > 1)?"s":"")
	if (lines.size() > 0) {
		for (String s : lines ) println s
	}
}