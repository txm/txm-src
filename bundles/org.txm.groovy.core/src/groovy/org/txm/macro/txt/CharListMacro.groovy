package org.txm.macro.txt
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import java.text.Collator

// BEGINNING OF PARAMETERS

@Field @Option(name="input_file", usage="Input file", widget="File", required=true, def="")
def input_file

@Field @Option(name="language", usage="langage (FR, EN...) for alphabetical sort", widget="String", required=true, def="FR")
def language

@Field @Option(name="character_encoding", usage="Characters encoding (see ListEncodings for a list of available encodings)", widget="String", required=true, def="UTF-8")
def character_encoding

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def collator = Collator.getInstance(new Locale(language))
collator.setStrength(Collator.TERTIARY)

def s = input_file.getText(character_encoding)

def dic = [:]

for (char c : s) {
	if (!dic.containsKey(c)) { dic[c] = 0 }
	dic[c]++
}

def keys = dic.keySet().collect { it.toString() }
Collections.sort(keys, collator)

def specials = [java.lang.Character.UNASSIGNED, java.lang.Character.SPACE_SEPARATOR, java.lang.Character.CONTROL, java.lang.Character.FORMAT, java.lang.Character.PRIVATE_USE, java.lang.Character.SURROGATE]

for (def k : keys) {

	character = k.charAt(0)
	codepoint = k.codePointAt(0)
	type = Character.getType(codepoint)

	if (!specials.contains(type as byte)) println sprintf("%s\t%d", k, dic.get(character))
}

for (def k : keys) {

	character = k.charAt(0)
	codepoint = k.codePointAt(0)
	type = Character.getType(codepoint)

	if (specials.contains(type as byte)) println sprintf("%s\t%d", Character.getName(codepoint), dic.get(character))
}

