// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.txt

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.CharsetDetector

@Field @Option(name="inputDirectory", usage="Dossier qui contient les fichiers à traiter", widget="Folder", required=true, def="")
File inputDirectory

@Field @Option(name="inputEncoding", usage="Input encoding", widget="String", required=false, def="")
String inputEncoding

@Field @Option(name="outputEncoding", usage="Output encoding", widget="String", required=true, def="UTF-8")
String outputEncoding

if (!ParametersDialog.open(this)) return;

File outDir = new File(inputDirectory, "out") // write result in "out" folder
if (outDir.exists()) {
	if (!outDir.deleteDir()) {
		println "Could not delete directory: $outDir"
		return;
	}
}
if (!outDir.mkdir()) {
	println "Could not create directory: $outDir"
	return;
}
println "Writing result in : $outDir"

def processFile(File file, File outfile, String fromEncoding, String toutputEncoding) {
	String text = file.getText(fromEncoding)
	outfile.withWriter(toutputEncoding) {writer -> // read with the "fromEncoding"
		writer.write(text) // write with the "toutputEncoding"
	}
}

try {
inputDirectory.eachFile { file ->
	if (!file.isHidden() && !file.isDirectory()) {
		String e = inputEncoding
		if (inputEncoding == null || inputEncoding.length() == 0) {
			CharsetDetector detector = new CharsetDetector(file)
			e = detector.getEncoding()
		}
		
		File outfile = new File(outDir, file.getName())
		println "$e -> $outputEncoding : "+outfile.getName()
		processFile(file, outfile, e, outputEncoding)
	}
}
} catch (UnsupportedEncodingException e) {
	println "ERROR: Unsupported encoding: "+e
}