package org.txm.macro.line
// Copyright © 2018 - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// imports
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// parameters

// **change this parameter**
@Field @Option(name="inputFile",usage="fichier Taltac", widget="File", required=true, def='')
def inputFile

// **change this parameter**
@Field @Option(name="characterEncoding",usage="Système d'encodage des caractères utilisé pour le fichier", widget="String", required=true, def='UTF-8')
def characterEncoding = "UTF-8"

@Field @Option(name="titleTag", usage="name of title ++++ tag", widget="String", required=false, def="titolo")
def titleTag

@Field @Option(name="addParagraphs", usage="encode each text line as a paragraph", widget="Boolean", required=true, def="true")
def addParagraphs

if (!ParametersDialog.open(this)) return

name = inputFile.getName()
idx = name.lastIndexOf(".")
if (idx > 0) name = name.substring(0, idx)
outputFile = new File(inputFile.getParentFile(), name+".xml")

pageNum = 2
outputFile.write('') // create empty file

outputFile.withWriter() { writer ->

	writer.println '<?xml version="1.0" encoding="UTF-8"?>\n<taltac>'

	def firstDoc = true
	def inTitle = false
	def currentTag = ""
	def metadata = "<hi>no metadata</hi>"
	def metadataEdited = false

	inputFile.eachLine(characterEncoding) { line ->

		line = line.replaceAll("&", "&amp;")
		line = line.replaceAll("<", "&lt;")

		// ****yahoobanque1 *data=31gen *autore=da *rubrica=da *ora=08 *agenzia=reuters *grafici=da
		group = (line =~ /^\*\*\*\*([^ ]+) (.*)/)
		if (group) {

			ident = group[0][1]
			properties = group[0][2]
			metadata=properties.replaceAll(/ ?\*([^=]+)=([^ ]+)/) { all2, propname, propvalue ->
				"<item><hi>$propname</hi>: $propvalue</item>\n"
			}
			metadata="<list type=\"unordered\">\n"+metadata+"</list>"
			metadataEdited = false
			properties=properties.replaceAll(/\*([^=]+)=([^ ]+)/) { all2, propname, propvalue ->
				"$propname=\"$propvalue\""
			}
			if (firstDoc) {
				firstDoc = false
				writer.println "<doc ident=\"$ident\" $properties>"
			}else{
				if (currentTag.size() > 0) {
					if (titleTag.size() > 0) {
						if (inTitle) {
							writer.println sprintf("</%s>", currentTag)
							writer.println "</head>"
							inTitle = false
							currentTag = ""
						}else{
							writer.println sprintf("</%s>", currentTag)
							currentTag = ""
						}
					}else{
						writer.println sprintf("</%s>", currentTag)
						currentTag = ""
					}
/*
					if (!metadataEdited) {
println sprintf ("ident = %s, currentTag = %s (%d), metadata", ident, currentTag, currentTag.size())
						writer.println metadata
						metadataEdited = true
					}
*/
				}
				writer.println "</doc>\n<pb n=\"${pageNum++}\"/>\n<doc ident=\"$ident\" $properties>"
			}
		}else{

			// ++++titolo
			group = (line =~ /^\+\+\+\+(.+)/)
			if (group) {

				tag = group[0][1]

				if (currentTag.size() > 0) {
					if (titleTag.size() > 0) {
						if (inTitle) {
							writer.println sprintf("</%s>", currentTag)
							writer.println "</head>"
							inTitle = false
							currentTag = ""
						}else{
							writer.println sprintf("</%s>", currentTag)
							currentTag = ""
						}
					}else{
						writer.println sprintf("</%s>", currentTag)
						currentTag = ""
					}
					if (!metadataEdited) {
						writer.println metadata
						metadataEdited = true
					}
				}
				if (titleTag.size() > 0) {
					if (tag == titleTag) {
						writer.println "<head>"
						writer.println sprintf("<%s>", tag)
						inTitle = true
						currentTag = tag
					}else{
						writer.println sprintf("<%s>", tag)
						currentTag = tag
					}
				}else{
					writer.println sprintf("<%s>", tag)
					currentTag = tag
				}

			}else{

				// normal line
				if (addParagraphs) {
					writer.println "<p>$line</p>"
				}else{
					writer.println "$line"
				}
			}
		}
	}

	writer.println sprintf("</%s>\n</doc>\n</taltac>\n", currentTag)
	writer.close()
}

