package org.txm.macro.txt
// Copyright © 2024 - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// imports

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.rcp.utils.IOClipboard

// parameters

@Field @Option(name="sep", usage="separator char between list elements (eg ',')", widget="String", required=true, def=',')
def sep

if (!ParametersDialog.open(this)) return

monitor.syncExec(new Runnable() {
	public void run() {
		try {
			s = IOClipboard.read()
			if (s == null || s.length() == 0) {
				println "No text in clipboard. Aborting."
				return
			}
			// a,c,e,b,d
			println "--> Clipboard content on input: \""+s+"\""
			s = s.split(sep).sort().join(sep)
			IOClipboard.write(s)
			println "<-- Clipboard content on output: \""+s+"\""
			
		} catch (e) {
			e.printStackTrace()
		}
 }
})

return 1

