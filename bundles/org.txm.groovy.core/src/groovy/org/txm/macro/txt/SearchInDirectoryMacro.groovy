package org.txm.macro.txt

import org.kohsuke.args4j.*

import groovy.transform.Field

import java.nio.charset.Charset

import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.*

import javax.xml.stream.*

import java.net.URL
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Field @Option(name="inputDirectory", usage="TXT directory", widget="Folder", required=false, def="txt")
File inputDirectory;
@Field @Option(name="regexp", usage="Regular expression to match", widget="String", required=false, def="p")
String regexp;
@Field @Option(name="encoding", usage="File encoding", widget="String", required=false, def="UTF-8")
String encoding;
@Field @Option(name="showLines", usage="File encoding", widget="Boolean", required=false, def="true")
Boolean showLines;

if (!ParametersDialog.open(this)) return;

def p = /$regexp/

println "processing: "+inputDirectory.listFiles()
for (File inputfile : inputDirectory.listFiles()) {
	if (inputfile.isDirectory()) continue // ignore
	
	def lines = []
	inputfile.eachLine("UTF-8") { line, n ->
		def m = line =~ p
		if ( m.size() > 0) {
			lines << "  line $n: $line"
		}
	}
	
	if (lines.size() > 0) {
		println inputfile.getName() + " "+lines.size() + " match" +((lines.size() > 1)?"s":"")
		if (showLines && lines.size() > 0) {
			for (String s : lines ) println s
		}
	}
	
}