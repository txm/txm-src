package org.txm.sw

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// PARAMETERS //

@Field @Option(name="input_file", usage="input file", widget="File", required=true, def="")
File input_file						// the file to process

@Field @Option(name="output_directory", usage="output folder", widget="Folder", required=false, def="")
File output_directory				// the directory which receives result files

@Field @Option(name="separator_line_regex", usage="search pattern", widget="String", required=false, def="(<\\?(.+))")
def separator_line_regex        	// separator line regex

@Field @Option(name="output_filename_prefix", usage="result files prefix", widget="String", required=false, def="")
String output_filename_prefix		// result files name prefix

@Field @Option(name="output_filename_replace", usage='regex replace expression to build result files name (use $1 to use the first parenthesized group in regex (...), $2 to use the second parenthesized group in regex (...), etc.', widget="String", required=false, def="\$2")
String output_filename_replace		// regex replace expression to build result files name (use $1 to use the first parenthesized group in regex (...), $2 to use the second parenthesized group in regex (...), etc.

@Field @Option(name="output_file_extension", usage=" result files extension", widget="String", required=false, def=".xml")
String output_file_extension         // result files extension

@Field @Option(name="includeSeparatorLine", usage="should include matching line in result files", widget="Boolean", required=false, def="true")
boolean includeSeparatorLine		// include separator lines in result files

@Field @Option(name="writeAtStartup", usage="should create files only if a matching line is found ", widget="Boolean", required=false, def="true")
boolean writeAtStartup				// create files only if a matching line is found

@Field @Option(name="encoding", usage="file encoding", widget="String", required=false, def="UTF-8")
String encoding

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

separator_line_regex = /$separator_line_regex/ // create a pattern object

// CALL //
splitByRegExp(input_file, output_directory,
	separator_line_regex, output_filename_prefix, output_filename_replace, output_file_extension,
//	separator_line_regex, output_filename_prefix, null, output_file_extension,
	includeSeparatorLine, writeAtStartup)


// PUBLIC METHODS //
def splitByRegExp(def input_file, def output_directory, def separator_line_regex, def output_filename_prefix, def output_filename_replace, def output_file_extension, def includeSeparatorLine, def writeAtStartup) {
	int count = 1;
	output_directory.deleteDir()
	output_directory.mkdirs()
	if (!output_directory.exists()) return false
	if (!input_file.isFile()) return false

	File outfile = null;
	def writer = null

	if (writeAtStartup) {
			outfile = new File(output_directory, output_filename_prefix+(count++)+output_file_extension)
		writer = outfile.newWriter(encoding)
	}
	input_file.eachLine(encoding) { line ->
		if (line ==~ separator_line_regex) { // new file
			if (output_filename_replace == null || output_filename_replace == "") {
				outfile = new File(output_directory, output_filename_prefix+(count++)+output_file_extension)
			} else {
				String name = output_filename_prefix+line.replaceAll(separator_line_regex, output_filename_replace)+output_file_extension
				outfile = new File(output_directory, name)
			}
			if (writer != null)	writer.close() // close previous writer
			writer = outfile.newWriter(encoding)
			println "new file $outfile"
			if (includeSeparatorLine) {
				writer.println line
			}
		} else {
			if (writer != null)
				writer.println line
		}
	}
	if (writer != null)	writer.close()
}