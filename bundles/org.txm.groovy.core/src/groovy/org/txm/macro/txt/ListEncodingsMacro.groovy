// Copyright © 2022 ENS Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.txt

import java.nio.charset.Charset

def charsets = Charset.availableCharsets()
def encodings = charsets.keySet()

def encodingsN = 0
println "== Character Encodings ==\n\n"+sprintf("%-15s (", "*Name*")+"*Alias names*)\n"
encodings.each { e ->
	charset = charsets.get(e)
	encodingsN++
	if (charset.canEncode()) {
		print sprintf("%-15s (", e)
		first = true
		charset.aliases().each { a ->
			if (!first) print ", "
			first = false
			print a
		}
		println ")"
	}
}
println encodingsN+" encodings."

