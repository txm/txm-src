package org.txm.macro.txt

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL

@Field @Option(name="inputDirectory", usage="TXT directory", widget="Folder", required=false, def="txt")
File inputDirectory;

@Field @Option(name="encoding", usage="File encoding", widget="String", required=false, def="UTF-8")
String encoding;

@Field @Option(name="rootTag", usage="rootTag", widget="String", required=false, def="doc")
String rootTag;

if (!ParametersDialog.open(this)) return;
rootTag = rootTag.trim()
encoding = encoding.trim()

File outputDirectory = new File(inputDirectory, "out")
outputDirectory.mkdir()
println "processing: "+inputDirectory
for (File inputfile : inputDirectory.listFiles()) {
	if (inputfile.isDirectory() || inputfile.isHidden() || !inputfile.getName().endsWith(".txt")) continue // ignore
	println " file: "+inputfile
	
	String name = inputfile.getName()
	int idx = name.lastIndexOf(".")
	if (idx > 0) name = name.substring(0, idx)
	File outputfile = new File(outputDirectory, name+".xml")
	
	XMLOutputFactory factory = XMLOutputFactory.newInstance()
	FileOutputStream output = new FileOutputStream(outputfile)
	XMLStreamWriter writer = factory.createXMLStreamWriter(output)

	writer.writeStartDocument("UTF-8","1.0")
	writer.writeCharacters("\n") // manage XML reserved characters
	writer.writeStartElement(rootTag)
	writer.writeCharacters(inputfile.getText(encoding)) // manage XML reserved characters
	writer.writeEndElement() // rootTag
	writer.close()
	output.close()
}