package org.txm.macro.txt
// Copyright © - ENS de Lyon - http://textometrie.ens-lyon.fr
// @author sheiden
//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// $LastChangedDate: 2013-05-02 11:28:42 +0200 (jeu., 02 mai 2013) $
// $LastChangedRevision: 2378 $
// $LastChangedBy: sheiden $
//

// imports

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*

// parameters

@Field @Option(name="inputDirectory",usage="Dossier qui contient les fichiers à modifier", widget="Folder", required=true, def='')
def inputDirectory

@Field @Option(name="extension",usage="Regexp de l'extension des fichiers à modifier", widget="String", required=true, def='\\.txt')
def extension

@Field @Option(name="find",usage="Expression régulière", widget="String", required=true, def='')
def find

@Field @Option(name="replaceWith",usage="Chaîne de remplacement", widget="String", required=false, def='')
def replaceWith

@Field @Option(name="encoding",usage="Encodage des fichiers", widget="String", required=true, def='UTF-8')
def encoding

//@Field @Option(name="showMatchingFilesOnly",usage="Montrer seulement les matchs", widget="Boolean", required=false, def='false')
def showMatchingFilesOnly = false

if (!ParametersDialog.open(this)) return

println "Working in $inputDirectory on files with extension "+/$extension/

if (showMatchingFilesOnly) {
	println "Searching '$find'"
} else {
	println "Replacing '$find' by '$replaceWith'"
}

/* parse Java escape characters in replace string

\t 	Insert a tab in the text at this point.
\b 	Insert a backspace in the text at this point.
\n 	Insert a newline in the text at this point.
\r 	Insert a carriage return in the text at this point.
\f 	Insert a formfeed in the text at this point.
\' 	Insert a single quote character in the text at this point.
\" 	Insert a double quote character in the text at this point.
\\ 	Insert a backslash character in the text at this point.
*/
replaceWith = org.apache.commons.lang.StringEscapeUtils.unescapeJava(replaceWith)

//find = /date="([0-9]+)-([0-9]+-[0-9]+)"/
// **change this parameter**
//replaceWith = 'date="$1-$2" year="$1"'
// **change this parameter** (warning: '$1', '$2'... can be interpreted by Groovy in "..." strings)
process(inputDirectory, extension, encoding, find, replaceWith, showMatchingFilesOnly)

// main body
def process(File inputDirectory, extension, encoding, find, replaceWith, showMatchingFilesOnly) {
	
	inputDirectory.eachFileMatch(~/.*$extension/) { file ->               // for each file matching extension
		println "Processing: "+file.getName()
//		if (showMatchingFilesOnly) {
//			printMatches(file, encoding, find)
//		} else {
			replaceInFile(file, encoding, find,replaceWith)
//		}
	}
}

//def printMatches(File file, encoding, find) {
//	file.eachLine(encoding) { line ->                  // for each line
//		if (line.matches("$find")) { println " *match* : "+line }
//	}
//}

def replaceInFile(File file, encoding,  find, replaceWith) {
	def tmp = File.createTempFile("SearchReplaceInDirectoryTemp", ".tmp", file.getParentFile()) // create temporary file
	tmp.write('')                                // create empty file
	tmp.withWriter(encoding) { writer ->
		writer.print file.getText(encoding).replaceAll(find, replaceWith)
//		file.eachLine(encoding) { line ->                  // for each line
//			writer.println line.replaceAll(find, replaceWith) // find&replace and print
//		}
		writer.close()
	}
	file.delete()
	tmp.renameTo(file)                           // save results
}

return 1
