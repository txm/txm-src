package org.txm.macro.table

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset

import org.txm.libs.msoffice.ReadExcel
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL
import java.text.SimpleDateFormat

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.ss.util.*

def stringToIndent = { str -> org.txm.utils.AsciiUtils.buildAttributeId(org.txm.utils.AsciiUtils.convertNonAscii(str)).toLowerCase() }

@Field @Option(name="inputFile", usage="fichier Excel à traiter", widget="File", required=true, def="")
		File inputFile

@Field @Option(name="sheetName", usage="sheet name (if no name is given the first sheet will be used)", widget="String", required=false, def="")
		def sheetName

@Field @Option(name="EmbedInTEI", usage="embed xml content in  TEI text element", widget="Boolean", required=false, def="false")
		def EmbedInTEI
		
@Field @Option(name="rootTag", usage="root tag name", widget="String", required=false, def="root")
		def rootTag

@Field @Option(name="textTag", usage="line unit tag name", widget="String", required=false, def="unit")
		def textTag

@Field @Option(name="metadataColumnList", usage="metadata columns list separated by comma", widget="String", required=false, def="meta1,meta2")
		def metadataColumnList

@Field @Option(name="dateColumnList", usage="date columns list separated by comma", widget="String", required=false, def="meta1,meta2")
		def dateColumnList

@Field @Option(name="textColumnList", usage="text columns list separated by comma", widget="String", required=false, def="textColumnList1,textColumnList2")
		def textColumnList
		
@Field @Option(name="prefixContentColumnList", usage="columns which content is a semi-colon separated prefixes list", widget="String", required=false, def="")
		def prefixContentColumnList
		
@Field @Option(name="listContentColumnList", usage="columns which content is a semi-colon separated list", widget="String", required=false, def="")
		def listContentColumnList


if (!ParametersDialog.open(this)) return

	if (!inputFile.exists()) {
		println "** Excel2XML: no '"+inputFile.name+"' file found. Aborting."
		return false
	}

if (!inputFile.canRead()) {
	println "** Excel2XML: '"+inputFile.name+"' file not readable. Aborting."
	return false
}

try {
	metadataColumnList = metadataColumnList.split(",").collect { it.trim() }
	dateColumnList = dateColumnList.split(",").collect { it.trim() }
	textColumnList = textColumnList.split(",").collect { it.trim() }
	prefixContentColumnList = prefixContentColumnList.split(",").collect { it.trim() }
	listContentColumnList = listContentColumnList.split(",").collect { it.trim() }
	textTag = textTag.trim()
	rootTag = rootTag.trim()
	
	ReadExcel excel = new ReadExcel(inputFile, sheetName);
	excel.readHeaders();
	
	headers = excel.getHeaders()
	
	println "Headers: $headers"
	
	ok = true
	metadataColumnList.each { m ->
		if (!headers.contains(m)) {
			println "** Excel2XML: missing metadataColumnList column: $m"
			ok = false
		}
	}
	textColumnList.each { t ->
		if (!headers.contains(t)) {
			println "** Excel2XML: missing textColumnList column: $t"
			ok = false
		}
	}
	
	if (!ok) { return false }
	
	println "metadataColumnList = "+metadataColumnList
	println "dateColumnList = "+dateColumnList
	println "textColumnList = "+textColumnList
	
	name = inputFile.getName()
	idx = name.lastIndexOf(".")
	
	if (idx > 0) name = name.substring(0, idx)
	outputFile = new File(inputFile.getParentFile(), name+".xml")
	
	factory = XMLOutputFactory.newInstance()
	output = new FileOutputStream(outputFile)
	writer = factory.createXMLStreamWriter(output, "UTF-8")
	
	writer.writeStartDocument("UTF-8","1.0")
	writer.writeCharacters("\n") // simple XML formating
	
	if (EmbedInTEI) {
		writer.writeStartElement("TEI")
		writer.writeStartElement("teiHeader")
		writer.writeEndElement() // teiHeader
		writer.writeStartElement("text")
		writer.writeCharacters("\n")
	}
	
	if (rootTag != null && rootTag.length() > 0) {
		writer.writeStartElement(rootTag)
		writer.writeCharacters("\n")
	}
	
	pb_n = 1
	
	//(1..nRows-1).each { rowIndex ->
	def record = null
	while (excel.readRecord()) {
		
		record = excel.getRecord()
		if (record.isEmpty()) continue;
		
		//println "record=$record"
		
		writer.writeCharacters("  ")
		writer.writeEmptyElement("pb") // <pb/> to get one page per input line (don't forget high number of words per page in import module)
		writer.writeAttribute("n", ""+pb_n++)
		writer.writeCharacters("\n") // simple XML formating
		
		writer.writeCharacters("  ")
		if (textTag != null && textTag.length() > 0) {
			writer.writeStartElement(textTag)
			
			//metadataColumnIndex.each { colIndex -> // build an attribute for each metadata
			for (def metadataName : metadataColumnList) {
				String s = record.get(metadataName);
				if (s == null) s = "";
				
				value = s.replaceAll("\n", ";").trim()
				writer.writeAttribute(stringToIndent(metadataName), value)
				
				if (metadataName in dateColumnList) { // also split date attributes in day+month+year attributes
					matches = (value =~ /([0-9]{2})\/([0-9]{2})\/([0-9]{4})/)
					writer.writeAttribute(metadataName+"-jour", matches[0][1])
					writer.writeAttribute(metadataName+"-joursemaine", new java.text.SimpleDateFormat('EEEE').format(Date.parse("dd/MM/yyyy", value)))
					writer.writeAttribute(metadataName+"-mois", matches[0][2])
					writer.writeAttribute(metadataName+"-annee", matches[0][3])
				}
			}
			writer.writeCharacters("\n")
		}
		
		writer.writeCharacters("    ")
		writer.writeStartElement("metadata")
		writer.writeStartElement("list")
		writer.writeAttribute("type", "unordered")
		writer.writeCharacters("\n")
		
		//metadataColumnIndex.each { colIndex ->
		for (def metadataName : metadataColumnList) {
			writer.writeStartElement("item")
			writer.writeCharacters(metadataName+" : "+record.get(metadataName).replaceAll("\n", ";"))
			writer.writeEndElement() // item
			writer.writeCharacters("\n")
		}
		writer.writeCharacters("    ")
		writer.writeEndElement() // list
		writer.writeEndElement() // head
		writer.writeCharacters("\n")
		
		//textColumnIndex.each { colIndex ->
		for (def textColumnName : textColumnList) {
			
			writer.writeCharacters("    ")
			writer.writeStartElement(stringToIndent(textColumnName))
			writer.writeStartElement("p")
			writer.writeStartElement("head")
			writer.writeStartElement("hi")
			writer.writeCharacters(textColumnName+" : ")
			writer.writeEndElement() // hi
			writer.writeEndElement() // head
			
			value = record.get(textColumnName)
			
			if (value ==~ /(?s)^[A-Z]{3}:  [^;\n]+? +[;\n].*/) {
				value.findAll( /(?s)[A-Z]{3}:  ([^;\n]+?) +[;\n]/ ).each { desc ->
					writer.writeStartElement("descripteur")
					matches = (desc =~ /(?s)([A-Z]{3}):  ([^;\n]+?) +[;\n]/)
					writer.writeAttribute("type", matches[0][1])
					writer.writeCharacters(matches[0][2])
					writer.writeEndElement() // descripteur
				}
			} else {
				writer.writeCharacters(value)
			}
			writer.writeEndElement() // p
			writer.writeEndElement() // textColumn
			writer.writeCharacters("\n")
		}
		
		writer.writeCharacters("  ")
		
		if (textTag != null && textTag.length() > 0) {
			writer.writeEndElement() // textTag
			writer.writeCharacters("\n")
		}
	}
	
	if (rootTag != null && rootTag.length() > 0) {
		writer.writeEndElement() // rootTag
		writer.writeCharacters("\n")
	}
	
	if (EmbedInTEI) {
		writer.writeEndElement() // text
		writer.writeCharacters("\n")
		writer.writeEndElement() // TEI
		writer.writeCharacters("\n")
	}
	
	writer.close()
	output.close()
	println "Result file: $outputFile"
	
} catch (Exception e) {
	println "** Excel2XML: unable to read input file. Aborting."
	println e.getLocalizedMessage()
	println e.printStackTrace()
	return false
}

return true
