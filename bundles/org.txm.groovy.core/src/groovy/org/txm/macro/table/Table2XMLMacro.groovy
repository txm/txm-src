package org.txm.macro.table

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL
import org.txm.libs.msoffice.ReadExcel

@Field @Option(name="inputFile", usage="CSV File", widget="File", required=false, def="")
		File inputFile;

@Field @Option(name="outputDirectory", usage="output directory", widget="Folder", required=false, def="")
		File outputDirectory;

@Field @Option(name="textsSeparator", usage="Texts", widget="Separator", required=false, def="Texts")
		def textsSeparator

@Field @Option(name="textIDColumn", usage="<text> id column", widget="String", required=false, def="Identifiant de la notice")
		def textIDColumn;

@Field @Option(name="textSelector", usage="<text> lines selector format is column=value", widget="String", required=false, def="Type de notice=Notice sommaire")
		def textSelector;

@Field @Option(name="textOrderColumn", usage="The column used to fill the 'text-order' text metadata", widget="String", required=false, def="")
		def textOrderColumn

@Field @Option(name="textMetadataColumnList", usage="<text> metadata columns", widget="String", required=false, def="Identifiant de la notice,Nom fichier segmenté (info),Lien notice principale,Date de diffusion,Type de date,Notes du titre ,Genre,Durée,Langue VO / VE ,Nature de production ,Producteurs (Aff.),Thématique")
		def textMetadataColumnList;

@Field @Option(name="textContentColumnList", usage="<text> textual content columns", widget="String", required=false, def="")
		def textContentColumnList;
		
@Field @Option(name="textJoinColumn", usage="jointure column, values should point to the textIDColumn values", widget="String", required=false, def="Lien notice principale")
		def textJoinColumn;

@Field @Option(name="structuresSeparator", usage="Structures", widget="Separator", required=false, def="Structures")
		def structuresSeparator

@Field @Option(name="teiStructures", usage="check to create tei:ab@type elements for each structure", widget="Boolean", required=false, def="false")
		def teiStructures;

@Field @Option(name="structureTag", usage="structure element to create", widget="String", required=false, def="div")
		def structureTag;

@Field @Option(name="structureSelector", usage="column_to_test=regexp", widget="String", required=false, def="Type de notice=Notice sujet")
		def structureSelector;

@Field @Option(name="structureMetadataColumnList", usage="structure metadata columns", widget="String", required=false, def="Identifiant de la notice,Date de diffusion,Durée,Genre,Identifiant Matériels (info.)")
		def structureMetadataColumnList;

@Field @Option(name="structureContentColumnList", usage="structure textual content columns", widget="String", required=false, def="Titre propre,Résumé,Descripteurs (Aff. Lig.),Générique (Aff. Lig.) ,Séquences")
		def structureContentColumnList;

@Field @Option(name="structureTitleColumnList", usage="structure textual content columns", widget="String", required=false, def="Titre propre,Date de diffusion")
		def structureTitleColumnList;
		
@Field @Option(name="structureSortColumnList", usage="structure textual content columns", widget="String", required=false, def="antract_debut,Identifiant de la notice")
		def structureSortColumnList;

@Field @Option(name="typesSeparator", usage="Columns types", widget="Separator", required=false, def="Columns types")
		def typesSeparator

@Field @Option(name="dateColumnTypeList", usage="metadata columns of type=Date", widget="String", required=false, def="Date de diffusion")
		def dateColumnTypeList

@Field @Option(name="prefixesColumnTypeList", usage="metadata columns of type=Prefixes", widget="String", required=false, def="Descripteurs (Aff. Lig.),Générique (Aff. Lig.) ")
		def prefixesColumnTypeList

@Field @Option(name="listColumnTypeList", usage="metadata columns of type=List semi-colon separated", widget="String", required=false, def="Résumé,Séquences")
		def listColumnTypeList

@Field @Option(name="debug", usage="Show debug messages", widget="Boolean", required=false, def="false")
		def debug
//@Field @Option(name="structureOrderColumn", usage="structure column coding structure order", widget="String", required=false, def="Titre propre,Résumé,Descripteurs (Aff. Lig.),Descripteurs (Aff. Col.),Séquences")
//		def structureOrderColumn;

if (!ParametersDialog.open(this)) return false;

textMetadataColumnList = textMetadataColumnList.split(",") as List
textMetadataColumnList.remove("")
textContentColumnList = textContentColumnList.split(",") as List
textContentColumnList.remove("")
structureMetadataColumnList = structureMetadataColumnList.split(",") as List
structureMetadataColumnList.remove("")
structureContentColumnList = structureContentColumnList.split(",") as List
structureContentColumnList.remove("")
structureTitleColumnList = structureTitleColumnList.split(",") as List
structureTitleColumnList.remove("")
structureSortColumnList = structureSortColumnList.split(",") as List
structureSortColumnList.remove("")
dateColumnTypeList = dateColumnTypeList.split(",") as List
dateColumnTypeList.remove("")
prefixesColumnTypeList = prefixesColumnTypeList.split(",") as List
prefixesColumnTypeList.remove("")
listColumnTypeList = listColumnTypeList.split(",") as List
listColumnTypeList.remove("")

if (textSelector != null && textSelector.contains("=")) {
	textSelector = [textSelector.substring(0, textSelector.indexOf("=")), textSelector.substring(textSelector.indexOf("=")+1)]
} else {
	textSelector = [null, null]
}
if (structureSelector != null && structureSelector.contains("=")) {
	structureSelector = [structureSelector.substring(0, structureSelector.indexOf("=")), structureSelector.substring(structureSelector.indexOf("=")+1)]
} else {
	structureSelector = [null, null]
}

println "textIDColumn, textJoinColumn=$textIDColumn, $textJoinColumn"
println "textMetadataColumnList columns: $textMetadataColumnList"
println "textContentColumnList columns: $textContentColumnList"
println "structureTitleColumnList columns: $structureTitleColumnList"
println "structureMetadataColumnList columns: $structureMetadataColumnList"
println "structureContentColumnList columns: $structureContentColumnList"

println "text selector="+textSelector
println "structure selector="+structureSelector
println "structureTag="+structureTag

def reader = null
try {
	reader = new TableReader(inputFile);
	println "Reading $inputFile with TableReader..."
} catch(Exception e) {
	reader = new ReadExcel(inputFile, null);
	println "Reading $inputFile with ReadExcel..."
}

if (!reader.readHeaders()) {
	println "** Error: no header"
	return false
}
def headers = Arrays.asList(reader.getHeaders())
println "$inputFile table column names: $headers"

def ok = true
def hash = ["selection":[textIDColumn, textJoinColumn], "textMetadataColumnList":textMetadataColumnList,
	"textContentColumnList":textContentColumnList, "structureMetadataColumnList":structureMetadataColumnList,
	"structureContentColumnList":structureContentColumnList, "structureTitleColumnList":structureTitleColumnList,
	 "structureSortColumnList":structureSortColumnList]
for (def key : hash.keySet()) {
	for (def m : hash[key]) {
		
		if (!headers.contains(m)) {
			println "** Error: missing $key column: $m"
			ok = false
		}
	}
}

if (!ok) { return false; }

inputName = inputFile.getName()
idx = inputName.lastIndexOf(".")
if (idx > 0) name = inputName.substring(0, idx)

// group records by text
def texts = new LinkedHashMap()
def nRecord = 0
def nRecordToWrite = 0
while (reader.readRecord()) {
	nRecord++
	//	println "record="+reader.getRecord().get(textSelector[0])+" "+reader.getRecord().get(structureSelector[0])
	
	String id = reader.get(textIDColumn)
	String join = reader.get(textJoinColumn)
	String textSelectorValue = reader.get(textSelector[0])
	String structureSelectorValue = reader.get(structureSelector[0])
	
	if (textIDColumn != null && textJoinColumn != null && textIDColumn.length() > 0 && textJoinColumn.length() > 0) {
		if (textSelectorValue != null && structureSelectorValue != null) {
			
			if (textSelectorValue != null && textSelectorValue.matches(textSelector[1])) {
				if (!texts.containsKey(id)) texts[id] = []
				texts[id].add(0, reader.getRecord())
			} else if (structureSelectorValue != null && structureSelectorValue.matches(structureSelector[1])) {
				if (!texts.containsKey(join)) texts[join] = []
				texts[join].add(reader.getRecord())
				nRecordToWrite++
			} else {
				// 	ignore record
			}
		} else { // no text&structure selector set, take the record if 'id' or 'join' are set
			if (id.length() > 0) {
				if (!texts.containsKey(id)) texts[id] = []
				texts[id].add(0, reader.getRecord())
			} else if (join.length() > 0) {
				if (!texts.containsKey(join)) texts[join] = []
				texts[join].add(reader.getRecord())
				nRecordToWrite++
			} else {
				// 	ignore record
			}
		}
	} else { // no id & join parameter set -> regroup all record in one file
		
		if (!texts.containsKey("$inputName.xml")) texts["$inputName.xml"] = []
		texts["$inputName.xml"].add(reader.getRecord())
		nRecordToWrite++
	}
}

println "N records: "+nRecord
println "N records to join: "+nRecordToWrite
println "N texts to build: "+texts.size()
if (texts.size()  == 0) {
	println "No text found. Aborting."
	return false
}
outputDirectory.mkdir()

for (def id : texts.keySet()) {
	def toWrite = texts[id]
	def text = toWrite[0]
	toWrite.remove(0)
	toWrite = toWrite.sort{x,y->
		for (def att : structureSortColumnList) {
			def ret = x.get(att).compareTo(y.get(att))
			if (ret != 0) {
				return ret
			}
		}
		return 0;
	}
	toWrite.add(0, text)
	texts[id] = toWrite
	
}

def metadata = new File(outputDirectory, "metadata.csv")
metadata.delete()
metadata <<  "id	text-order\n"
ConsoleProgressBar cpb = new ConsoleProgressBar(texts.size())
def nText = 0
for (def id : texts.keySet()) {
	nText++
	cpb.tick()
	def toWrite = texts[id]
	def text = toWrite[0]
	String textSelectorValue = text.get(textSelector[0])
	if (textSelectorValue == null || (textSelectorValue != null && textSelectorValue.matches(textSelector[1]))) {
		if (debug) println "Processing text: $id"
		
		if (toWrite.size() <= 1 && textContentColumnList.size() == 0) {
			cpb.setMessage("** Ignoring the empty $id text")
			continue
		}
		
		File outputfile = new File(outputDirectory, id+".xml")
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance()
		FileOutputStream output = new FileOutputStream(outputfile)
		XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8")
		
		writer.writeStartDocument("UTF-8","1.0")
		writer.writeCharacters("\n") // simple XML formating
		writer.writeStartElement("TEI")
		writer.writeCharacters("\n") // simple XML formating
		writer.writeStartElement("teiHeader")
		writer.writeEndElement() // teiHeader
		writer.writeCharacters("\n") // simple XML formating
		writer.writeStartElement("text")
		writer.writeAttribute("id", id)
		
		textOrderWritten = false
		if (textOrderColumn != null && textOrderColumn.length() > 0 && textMetadataColumnList.contains(textOrderColumn)) {
			textOrderWritten = text.get(textOrderColumn)
		}
		
		for (String att : textMetadataColumnList) {
			if (att in dateColumnTypeList) {
				String tri = writeMetadataDate(text, att, writer)
				if (!textOrderWritten && tri != null) {
					textOrderWritten = tri;
				}
			} else {
				writer.writeAttribute(AsciiUtils.buildAttributeId(att), text.get(att)) // struct
			}
		}
		
		if (!textOrderWritten) {
			textOrderWritten = String.sprintf("%08d", nText);
		}
		
		metadata << "$id	$textOrderWritten\n"
		
		writer.writeCharacters("\n") // simple XML formating
		
		for (String att : textContentColumnList) {
			
			if (att in prefixesColumnTypeList) {
				
				writePrefixTextContent(text, att, writer)
			} else if (att in listColumnTypeList) {
				
				writeListTextContent(text, att, writer)
			} else {
				
				writeTextContent(text, att, writer)
			}
		}
		
		int pb_n = 1;
		for (int i = 1 ; i < toWrite.size() ; i++) {
			def record = toWrite[i]
			
			writer.writeEmptyElement("pb") // <pb/>
			writer.writeAttribute("n", ""+pb_n++)
			writer.writeAttribute("type", "record")
			writer.writeCharacters("\n")
			
			writer.writeStartElement(structureTag)
			
			for (String att : structureMetadataColumnList) {
				if (att in dateColumnTypeList) {
					writeMetadataDate(record, att, writer)
				} else {
					writer.writeAttribute(AsciiUtils.buildAttributeId(att), record.get(att)) // struct
				}
			}
			writer.writeCharacters("\n")
			
			for (String att : structureTitleColumnList) {
				writer.writeStartElement("head")
				writer.writeCharacters(record.get(att).trim())
				writer.writeEndElement() // head
				writer.writeCharacters("\n")
			}
			
			writer.writeStartElement("metadata")
			writer.writeStartElement("list")
			writer.writeAttribute("type", "unordered")
			writer.writeCharacters("\n")
			structureMetadataColumnList.each { att ->
				writer.writeStartElement("item")
				writer.writeCharacters(att+" : "+record.get(att).replaceAll("\n", ";"))
				writer.writeEndElement() // item
				writer.writeCharacters("\n")
			}
			writer.writeEndElement() // list
			writer.writeEndElement() // metadata
			writer.writeCharacters("\n")
			
			for (String att : structureContentColumnList) {
				if (att in prefixesColumnTypeList) {
					writePrefixTextContent(record, att, writer)
				} else if (att in listColumnTypeList) {
					writeListTextContent(record, att, writer)
				} else {
					writeTextContent(record, att, writer)
				}
			}
			
			writer.writeEndElement() // struct
			writer.writeCharacters("\n") // simple XML formating
		}
		
		writer.writeEndElement() // text
		writer.writeCharacters("\n") // simple XML formating
		writer.writeEndElement() // TEI
		writer.close()
		output.close()
		reader.close()
	} else {
		// error
		println "ERROR: '$id' text group with  no text line"
	}
}
cpb.done()


def writeListTextContent(def record, def att, def writer) {
	
	if (teiStructures) {
		writer.writeStartElement("ab")
		writer.writeAttribute("type", att)
	} else {
		writer.writeStartElement(AsciiUtils.buildAttributeId(att));
	}
	writer.writeCharacters("\n")
	
	writer.writeStartElement("head")
	writer.writeStartElement("hi")
	writer.writeCharacters(att)
	writer.writeEndElement() // hi
	writer.writeEndElement() // head
	writer.writeCharacters("\n")
	
	def value = record.get(att).replaceAll(" ++", " ")
	if (value.length() > 1) {
		
		writer.writeStartElement("list")
		writer.writeAttribute("type", "ordered")
		writer.writeCharacters("\n")
		
		found = false
		for (String s : value.split("\n")) {
			s = s.trim()
			if (s.length() == 0) continue;
			if (s.startsWith("-")) s = s.substring(1).trim();
			if (s.length() == 0) continue;
			
			found = true
			writer.writeCharacters("\t")
			writer.writeStartElement("item")
			writer.writeStartElement("p")
			writer.writeAttribute("rend", "list")
			writer.writeCharacters(s)
			writer.writeEndElement() // p
			writer.writeEndElement() // item
			writer.writeCharacters("\n")
		}
		
		if (!found) {
			println "Warning: '$att' list not found in '$value'"
			writer.writeCharacters("\t")
			writer.writeStartElement("item")
			writer.writeStartElement("p")
			writer.writeAttribute("type", att)
			writer.writeAttribute("rend", "no-list")
			writer.writeCharacters(value)
			writer.writeEndElement() // p
			writer.writeEndElement() // item
			writer.writeCharacters("\n")
		}
		
		writer.writeEndElement() // list
		writer.writeCharacters("\n")
	}
	writer.writeEndElement() // ab
	writer.writeCharacters("\n")
}

def writeTextContent(def record, def att, def writer) {
	
	if (teiStructures) {
		writer.writeStartElement("ab")
		writer.writeAttribute("type", att)
	} else {
		writer.writeStartElement(AsciiUtils.buildAttributeId(att));
	}
	writer.writeCharacters("\n")
	
	writer.writeStartElement("head")
	writer.writeStartElement("hi")
	writer.writeCharacters(att)
	writer.writeEndElement() // hi
	writer.writeEndElement() // head
	writer.writeCharacters("\n")
	
	def value = record.get(att).replaceAll(" ++", " ")
	if (value.length() > 0) {
		
		for (String s : value.split("\n")) {
			writer.writeStartElement("p")
			writer.writeCharacters(s) // a paragraphe per line
			writer.writeEndElement() // p
		}
	}
	writer.writeEndElement() // ab
	writer.writeCharacters("\n")
}

def writePrefixTextContent(def record, def att, def writer) {
	
	if (teiStructures) {
		writer.writeStartElement("ab")
		writer.writeAttribute("type", att)
	} else {
		writer.writeStartElement(AsciiUtils.buildAttributeId(att));
	}
	writer.writeCharacters("\n")
	
	writer.writeStartElement("head")
	writer.writeStartElement("hi")
	writer.writeCharacters(att)
	writer.writeEndElement() // hi
	writer.writeEndElement() // head
	writer.writeCharacters("\n")
	
	def value = record.get(att).replaceAll(" ++", " ")
	if (value.length() > 1) {
		
		writer.writeCharacters("\n")
		writer.writeStartElement("list")
		writer.writeAttribute("rend", att.trim())
		writer.writeAttribute("type", "unordered")
		writer.writeCharacters("\n")
		found = false
		
		value.findAll( /(?s)[A-Z]{3}:? *([^;\n]+?) +[;\n]/ ).each { desc ->
			found = true
			writer.writeCharacters("\t")
			writer.writeStartElement("item")
			
//			writer.writeStartElement("p")
			matches = (desc =~ /(?s)([A-Z]{3}):? *([^;\n]+?) +[;\n]/)
			writer.writeAttribute("type", matches[0][1])
			
			writer.writeStartElement("span")
			writer.writeCharacters(matches[0][1]+" ")
			writer.writeEndElement() // span
			writer.writeCharacters(matches[0][2])
//			writer.writeEndElement() // p
			writer.writeEndElement() // item
			writer.writeCharacters("\n")
		}
		if (!found) {
			println "Warning: '$att' prefixes not found in: '$value'"
			writer.writeStartElement("p")
			writer.writeCharacters(value)
			writer.writeEndElement() // p
		}
		writer.writeEndElement() // list
		writer.writeCharacters("\n")
	}
	writer.writeEndElement() // ab
	writer.writeCharacters("\n")
}

def writeMetadataDate(def record, def att, def writer) {
	String value = record.get(att)
	if (value.length() == 0) return null
	
	String att_normalized = AsciiUtils.buildAttributeId(att)
	matches = (value =~ /([0-9]{2})\/([0-9]{2})\/([0-9]{4})/)
	writer.writeAttribute(att_normalized+"-jour", matches[0][1])
	writer.writeAttribute(att_normalized+"-joursemaine", new java.text.SimpleDateFormat('EEEE').format(Date.parse("dd/MM/yyyy", value)))
	writer.writeAttribute(att_normalized+"-mois", matches[0][2])
	writer.writeAttribute(att_normalized+"-annee", matches[0][3])
	String tri = matches[0][3]+"-"+matches[0][2]+"-"+matches[0][1]
	writer.writeAttribute(att_normalized+"-tri", tri)
	
	return tri
}

/*
 String name = inputFile.getName()
 int idx = name.lastIndexOf(".")
 if (idx > 0) name = name.substring(0, idx)
 */
