package org.txm.macro.table

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.ss.util.*

@Field @Option(name="inputDirectory", usage="dossier des fichiers Excel à traiter", widget="Folder", required=true, def="")
def inputDirectory

@Field @Option(name="sheetName", usage="sheet name (if no name is given the first sheet will be used)", widget="String", required=false, def="")
def sheetName

@Field @Option(name="rootTag", usage="Root tag name", widget="String", required=false, def="root")
def rootTag

@Field @Option(name="textTag", usage="Text tag name", widget="String", required=false, def="unit")
def textTag

@Field @Option(name="metadataColumnList", usage="metadataColumnList column list separated with comma", widget="String", required=false, def="meta1,meta2")
def metadataColumnList

@Field @Option(name="dateColumnList", usage="date columns list separated by comma", widget="String", required=false, def="meta1,meta2")
def dateColumnList

@Field @Option(name="textColumnList", usage="textColumnList column list separated with comma", widget="String", required=false, def="textColumnList1,textColumnList2")
def textColumnList

@Field @Option(name="EmbedInTEI", usage="text columns list separated by comma", widget="Boolean", required=false, def="false")
def EmbedInTEI

if (!ParametersDialog.open(this)) return

if (!inputDirectory.exists()) {
	println "** ExcelDir2XML: no '"+inputDirectory.name+"' directory found. Aborting."
	return false
}

if (!inputDirectory.canRead()) {
	println "** ExcelDir2XML: '"+inputDirectory.name+"' directory not readable. Aborting."
	return false
}

def f = []
inputDirectory.eachFileMatch(~/.*xlsx/) { f << it }

if (f.size() == 0) {
	println "** ExcelDir2XML: no .xlsx file found. Aborting."
	return false
}

try {

f.sort { it.name }.each { inputFile ->

	res = gse.run(Excel2XMLMacro, ["args":[

"inputFile":inputFile,
"sheetName":sheetName,
"metadataColumnList":metadataColumnList,
"dateColumnList":dateColumnList,
"textColumnList":textColumnList,
"rootTag":rootTag,
"textTag":textTag,
"EmbedInTEI": EmbedInTEI,
			
				"selection":selection,
				"selections":selections,
				"corpusViewSelection":corpusViewSelection,
				"corpusViewSelections":corpusViewSelections,
				"monitor":monitor]])
			if (!res) println "** problem calling Excel2XMLMacro."
}

} catch (Exception e) {
	println "** ExcelDir2XML: unable to read input files. Aborting."
	println e.getLocalizedMessage()
	println e.printStackTrace()
	return false
}

return true
