package org.txm.macro.table

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.ss.util.*

@Field @Option(name="outputDirectory", usage="output directory", widget="Folder", required=false, def="directory")
		File outputDirectory;

@Field @Option(name="textsSeparator", usage="Texts", widget="Separator", required=false, def="Texts")
		def textsSeparator

@Field @Option(name="textIDColumn", usage="<text> id column", widget="String", required=false, def="Identifiant de la notice")
		def textIDColumn;

@Field @Option(name="textSelector", usage="<text> lines selector format is column=value", widget="String", required=false, def="Type de notice=Notice sommaire")
		def textSelector;

@Field @Option(name="textMetadataColumnList", usage="<text> metadata columns", widget="String", required=false, def="Identifiant de la notice,Date de diffusion,Durée,Genre,Identifiant Matériels (info.)")
		def textMetadataColumnList;

@Field @Option(name="textContentColumnList", usage="<text> textual content columns", widget="String", required=false, def="Titre propre,Résumé,Descripteurs (Aff. Lig.),Descripteurs (Aff. Col.),Séquences")
		def textContentColumnList;

@Field @Option(name="structuresSeparator", usage="Structures", widget="Separator", required=false, def="Structures")
		def structuresSeparator

@Field @Option(name="structureTag", usage="structure element to create", widget="String", required=false, def="div")
		def structureTag;

@Field @Option(name="structureSelector", usage="column_to_test=regexp", widget="String", required=false, def="Type de notice=Notice sujet")
		def structureSelector;

@Field @Option(name="textJoinColumn", usage="jointure column, values should point to the textIDColumn values", widget="String", required=false, def="Lien notice principale")
		def textJoinColumn;

@Field @Option(name="structureMetadataColumnList", usage="structure metadata columns", widget="String", required=false, def="Identifiant de la notice,Date de diffusion,Durée,Genre,Identifiant Matériels (info.)")
		def structureMetadataColumnList;

@Field @Option(name="structureContentColumnList", usage="structure textual content columns", widget="String", required=false, def="Titre propre,Résumé,Descripteurs (Aff. Lig.),Descripteurs (Aff. Col.),Séquences")
		def structureContentColumnList;

@Field @Option(name="typesSeparator", usage="Columns types", widget="Separator", required=false, def="Columns types")
		def typesSeparator

@Field @Option(name="dateColumnTypeList", usage="metadata columns of type=Date", widget="String", required=false, def="")
		def dateColumnTypeList

@Field @Option(name="prefixesColumnTypeList", usage="metadata columns of type=Prefixes", widget="String", required=false, def="")
		def prefixesColumnTypeList

@Field @Option(name="listColumnTypeList", usage="metadata columns of type=List semi-colon separated", widget="String", required=false, def="")
		def listColumnTypeList

@Field @Option(name="debug", usage="Show devug messages", widget="Boolean", required=false, def="false")
		def debug

if (!ParametersDialog.open(this)) return

if (!inputDirectory.exists()) {
	println "** TableDir2XML: no '"+inputDirectory.name+"' directory found. Aborting."
	return false
}

if (!inputDirectory.canRead()) {
	println "** TableDir2XML: '"+inputDirectory.name+"' directory not readable. Aborting."
	return false
}

def f = []
inputDirectory.eachFileMatch(~/.*(xlsx|ods|tsv|csv)/) { f << it }

if (f.size() == 0) {
	println "** TableDir2XML: no .xlsx file found. Aborting."
	return false
}

try {

f.sort { it.name }.each { file ->

	res = gse.run(Table2XMLMacro, ["args":[

		"inputFile":file, 
		"outputDirectory":outputDirectory, 
		"textsSeparator":textsSeparator, 
		"textIDColumn":textIDColumn, 
		"textSelector":textSelector, 
		"textMetadataColumnList":textMetadataColumnList, 
		"textContentColumnList":textContentColumnList, 
		"structuresSeparator":structuresSeparator, 
		"structureTag":structureTag, 
		"structureSelector":structureSelector, 
		"textJoinColumn":textJoinColumn, 
		"structureMetadataColumnList":structureMetadataColumnList, 
		"structureContentColumnList":structureContentColumnList, 
		"typesSeparator":typesSeparator, 
		"dateColumnTypeList":dateColumnTypeList, 
		"prefixesColumnTypeList":prefixesColumnTypeList, 
		"listColumnTypeList":listColumnTypeList, 
		"debug":debug
		],
			
		"selection":selection,
		"selections":selections,
		"corpusViewSelection":corpusViewSelection,
		"corpusViewSelections":corpusViewSelections,
		"monitor":monitor])

	if (!res) println "** problem calling Table2XMLMacro."
}

} catch (Exception e) {
	println "** TableDir2XML: unable to read input files. Aborting."
	println e.getLocalizedMessage()
	println e.printStackTrace()
	return false
}

return true
