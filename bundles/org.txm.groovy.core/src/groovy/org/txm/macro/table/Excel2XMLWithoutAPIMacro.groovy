package org.txm.macro.table

import org.kohsuke.args4j.*
import groovy.transform.Field
import java.nio.charset.Charset
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.utils.*
import javax.xml.stream.*
import java.net.URL
import java.text.SimpleDateFormat

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.ss.util.*

def stringToIndent = { str -> org.txm.utils.AsciiUtils.buildAttributeId(org.txm.utils.AsciiUtils.convertNonAscii(str)).toLowerCase() }

// from http://www.java-connect.com/apache-poi-tutorials/read-all-type-of-excel-cell-value-as-string-using-poi
def getCellValueAsString = { cell ->
	strCellValue = null
	if (cell != null) {
		switch (cell.getCellType()) {
			case CellType.STRING:
				strCellValue = cell.toString()
				break
			case CellType.NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy")
					strCellValue = dateFormat.format(cell.getDateCellValue())
				} else {
					value = cell.getNumericCellValue()
					longValue = value.longValue()
					strCellValue = new String(longValue.toString())
				}
				break
			case CellType.BOOLEAN:
				strCellValue = new String(new Boolean(cell.getBooleanCellValue()).toString())
				break
			case CellType.BLANK:
				strCellValue = ""
				break
		}
	}
	if (strCellValue == null) strCellValue = ""
	return strCellValue
}

@Field @Option(name="inputFile", usage="fichier Excel à traiter", widget="File", required=true, def="")
		File inputFile

@Field @Option(name="sheetName", usage="sheet name (if no name is given the first sheet will be used)", widget="String", required=false, def="")
		def sheetName

@Field @Option(name="rootTag", usage="root tag name", widget="String", required=false, def="root")
		def rootTag

@Field @Option(name="textTag", usage="line unit tag name", widget="String", required=false, def="unit")
		def textTag

@Field @Option(name="metadataColumnList", usage="metadata columns list separated by comma", widget="String", required=false, def="meta1,meta2")
		def metadataColumnList

@Field @Option(name="dateColumnList", usage="date columns list separated by comma", widget="String", required=false, def="meta1,meta2")
		def dateColumnList

@Field @Option(name="textColumnList", usage="text columns list separated by comma", widget="String", required=false, def="textColumnList1,textColumnList2")
		def textColumnList

@Field @Option(name="EmbedInTEI", usage="text columns list separated by comma", widget="Boolean", required=false, def="false")
		def EmbedInTEI

if (!ParametersDialog.open(this)) return

	if (!inputFile.exists()) {
		println "** Excel2XML: no '"+inputFile.name+"' file found. Aborting."
		return false
	}

if (!inputFile.canRead()) {
	println "** Excel2XML: '"+inputFile.name+"' file not readable. Aborting."
	return false
}

try {
	
	metadataColumnList = metadataColumnList.split(",").collect { it.trim() }
	dateColumnList = dateColumnList.split(",").collect { it.trim() }
	textColumnList = textColumnList.split(",").collect { it.trim() }
	textTag = textTag.trim()
	rootTag = rootTag.trim()
	
	wb = WorkbookFactory.create(inputFile)
	
	if (sheetName.length() == 0) {
		ws = wb.getSheetAt(0)
	} else {
		ws = wb.getSheet(sheetName)
		if (ws == null) {
			println "** Excel2XML: no '"+sheetName+" found. Aborting."
			return false
		}
	}
	
	if (ws == null) {
		println "** Excel2XML: no sheet found. Aborting."
		return false
	}
	
	nRows = ws.getPhysicalNumberOfRows()
	println nRows+" rows."
	
	firstRow = ws.getRow(0)
	colMax = firstRow.getLastCellNum()
	
	headers = (0..colMax-1).collect { getCellValueAsString(firstRow.getCell(it)) }
	
	println "Headers: $headers"
	
	normalizedHeaders = headers.collect { stringToIndent(it) }
	
	ok = true
	metadataColumnList.each { m ->
		if (!headers.contains(m)) {
			println "** Excel2XML: missing metadataColumnList column: $m"
			ok = false
		}
	}
	textColumnList.each { t ->
		if (!headers.contains(t)) {
			println "** Excel2XML: missing textColumnList column: $t"
			ok = false
		}
	}
	
	if (!ok) { return false }
	
	metadataColumnIndex = metadataColumnList.collect { headers.indexOf(it) }
	dateColumnsIndex = dateColumnList.collect { headers.indexOf(it) }
	textColumnIndex = textColumnList.collect { headers.indexOf(it) }
	
	println "metadataColumnList = "+metadataColumnList
	println "metadataColumnIndex = "+metadataColumnIndex
	
	println "dateColumnList = "+dateColumnList
	println "dateColumnsIndex = "+dateColumnsIndex
	
	println "textColumnList = "+textColumnList
	println "textColumnIndex = "+textColumnIndex
	
	name = inputFile.getName()
	idx = name.lastIndexOf(".")
	
	if (idx > 0) name = name.substring(0, idx)
	outputFile = new File(inputFile.getParentFile(), name+".xml")
	
	factory = XMLOutputFactory.newInstance()
	output = new FileOutputStream(outputFile)
	writer = factory.createXMLStreamWriter(output, "UTF-8")
	
	writer.writeStartDocument("UTF-8","1.0")
	writer.writeCharacters("\n") // simple XML formating
	
	if (EmbedInTEI) {
		writer.writeStartElement("TEI")
		writer.writeStartElement("teiHeader")
		writer.writeEndElement() // teiHeader
		writer.writeStartElement("text")
		writer.writeCharacters("\n")
	}
	
	writer.writeStartElement(rootTag)
	writer.writeCharacters("\n")
	
	pb_n = 1
	
	(1..nRows-1).each { rowIndex ->
		
		writer.writeCharacters("  ")
		writer.writeEmptyElement("pb") // <pb/> to get one page per input line (don't forget high number of words per page in import module)
		writer.writeAttribute("n", ""+pb_n++)
		writer.writeCharacters("\n") // simple XML formating
		
		writer.writeCharacters("  ")
		writer.writeStartElement(textTag)
		metadataColumnIndex.each { colIndex -> // build an attribute for each metadata
			def row = ws.getRow(rowIndex)
			if (row != null) {
				String s = getCellValueAsString(row.getCell(colIndex));
				if (s == null) s ="";
				value = s.replaceAll("\n", ";").trim()
				writer.writeAttribute(normalizedHeaders[colIndex], value)
				if (colIndex in dateColumnsIndex) { // also split date attributes in day+month+year attributes
					matches = (value =~ /([0-9]{2})\/([0-9]{2})\/([0-9]{4})/)
					writer.writeAttribute(normalizedHeaders[colIndex]+"jour", matches[0][1])
					writer.writeAttribute(normalizedHeaders[colIndex]+"joursemaine", new java.text.SimpleDateFormat('EEEE').format(Date.parse("dd/MM/yyyy", value)))
					writer.writeAttribute(normalizedHeaders[colIndex]+"mois", matches[0][2])
					writer.writeAttribute(normalizedHeaders[colIndex]+"annee", matches[0][3])
				}
			}
		}
		writer.writeCharacters("\n")
		
		writer.writeCharacters("    ")
		writer.writeStartElement("metadata")
		writer.writeStartElement("list")
		writer.writeAttribute("type", "unordered")
		writer.writeCharacters("\n")
		
		metadataColumnIndex.each { colIndex ->
			
			def row = ws.getRow(rowIndex)
			if (row != null) {
				writer.writeStartElement("item")
				writer.writeCharacters(headers[colIndex]+" : "+getCellValueAsString(row.getCell(colIndex)).replaceAll("\n", ";"))
				writer.writeEndElement() // item
				writer.writeCharacters("\n")
			}
		}
		writer.writeCharacters("    ")
		writer.writeEndElement() // list
		writer.writeEndElement() // head
		writer.writeCharacters("\n")
		
		textColumnIndex.each { colIndex ->
			
			writer.writeCharacters("    ")
			writer.writeStartElement(normalizedHeaders[colIndex])
			writer.writeStartElement("p")
			writer.writeStartElement("head")
			writer.writeStartElement("hi")
			writer.writeCharacters(headers[colIndex]+" : ")
			writer.writeEndElement() // hi
			writer.writeEndElement() // head
			def row = ws.getRow(rowIndex)
			if (row != null) {
				value = getCellValueAsString(row.getCell(colIndex))
				
				if (value ==~ /(?s)^[A-Z]{3}:  [^;\n]+? +[;\n].*/) {
					value.findAll( /(?s)[A-Z]{3}:  ([^;\n]+?) +[;\n]/ ).each { desc ->
						writer.writeStartElement("descripteur")
						matches = (desc =~ /(?s)([A-Z]{3}):  ([^;\n]+?) +[;\n]/)
						writer.writeAttribute("type", matches[0][1])
						writer.writeCharacters(matches[0][2])
						writer.writeEndElement() // descripteur
					}
				} else {
					writer.writeCharacters(value)
				}
				writer.writeEndElement() // p
				writer.writeEndElement() // textColumn
				writer.writeCharacters("\n")
			}
		}
		
		writer.writeCharacters("  ")
		writer.writeEndElement() // textTag
		writer.writeCharacters("\n")
	}
	
	writer.writeEndElement() // rootTag
	writer.writeCharacters("\n")
	if (EmbedInTEI) {
		writer.writeEndElement() // text
		writer.writeCharacters("\n")
		writer.writeEndElement() // TEI
		writer.writeCharacters("\n")
	}
	
	writer.close()
	output.close()
	println "Result file: $outputFile"
	
} catch (Exception e) {
	println "** Excel2XML: unable to read input file. Aborting."
	println e.getLocalizedMessage()
	println e.printStackTrace()
	return false
}

return true
