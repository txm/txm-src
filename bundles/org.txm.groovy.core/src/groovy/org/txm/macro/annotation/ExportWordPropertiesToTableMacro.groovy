// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
// $LastChangedDate: 2012-10-15 17:35:36 +0200 (lun., 15 oct. 2012) $
// $LastChangedRevision: 2279 $
// $LastChangedBy: mdecorde $
//
package org.txm.macro.annotation

import org.txm.Toolbox
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.concordance.core.functions.Concordance
import org.txm.concordance.core.functions.Line
import org.txm.core.preferences.TBXPreferences
import org.txm.functions.concordances.*
import org.txm.functions.concordances.comparators.*
import org.txm.searchengine.cqp.ReferencePattern

import java.util.List

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Error: you must select a corpus or a subcorpus to export properties"
	return false;
}

CQPCorpus corpus = corpusViewSelection


@Field @Option(name="properties", usage="columns to inject separated by commas", widget="String", required=true, def="p1, p2, ... , pn")
		def properties

@Field @Option(name="query", usage="The query to select words to annotate", widget="Query", required=true, def="[]")
		def query

@Field @Option(name="leftcontextsize", usage="The context sizes", widget="Integer", required=true, def="10")
		def leftcontextsize

@Field @Option(name="rightcontextsize", usage="The context sizes", widget="Integer", required=true, def="10")
		def rightcontextsize

@Field @Option(name="references", usage="references to show", widget="String", required=true, def="text_id")
		def references

@Field @Option(name="tsv_file", usage="The result output file, in .tsv format", widget="FileSave", required=true, def="")
		def tsv_file

if (!ParametersDialog.open(this)) return;

def split = properties.split(",")
if (split.length == 0) {
	println "ERROR: no property given"
	return false
}
properties = []
for (def p : split) properties << p.trim()

tsv_file = tsv_file.getAbsoluteFile()
	
println "Building Annotation Table with the following parameters: "
println "- corpus: $corpus"
println "- query: $query"
println "- annotation properties: $properties"
println "- references: $properties"
println "- CSV file: $tsv_file"


if (!tsv_file.getParentFile().canWrite()) {
	println "Error: "+tsv_file.getParentFile()+" is not writable."
	return;
}

def annots = []
for (def p : properties) {
	def prop = corpus.getProperty(p)
	if (prop == null) {
		println "No such property $p in the $corpus corpus"
		return false;
	}
	annots << prop
}
def id = corpus.getProperty("id")
def word = corpus.getProperty("word")
def text = corpus.getStructuralUnit("text")
def text_id = text.getProperty("id")
def refs = references.split(",")

if (annots == null || annots.size() == 0) {
	println "No such property given"//$annots in the $corpus corpus"
	return false;
}

println "Exporting ..."
def start = System.currentTimeMillis()

// define the references pattern for each concordance line
def referencePattern = new ReferencePattern()
referencePattern.addProperty(text_id)
referencePattern.addProperty(id)
for (def annot : annots) referencePattern.addProperty(annot)

def refProperties = []
for (def ref : refs) {
	ref = ref.trim()
	try {
		if (ref.contains("_")) {
			def split2 = ref.split("_",2)
			def refp = corpus.getStructuralUnit(split2[0].trim()).getProperty(split2[1].trim())
			if (ref != "text_id")
				referencePattern.addProperty(refp)
			refProperties << refp
		} else {
			def p = corpus.getProperty(ref)
			if (p != null) referencePattern.addProperty(p)
		}
	} catch(Exception e) { 
		println "Error while parsing references: "+ref+" "+e
		return false;
	}
}

// compute the concordance with contexts of 15 words on each side of the keyword
//query = new Query(Query.fixQuery(query))
def viewprops = [word]
viewprops.addAll(annots)
Concordance concordance = new Concordance(corpus)
concordance.setParameters(query, [word], [word], [word], viewprops, viewprops, viewprops, referencePattern, referencePattern, leftcontextsize, rightcontextsize)
concordance.compute()
//println "Conc done "+(System.currentTimeMillis()-start)

def writer = tsv_file.newWriter("UTF-8");

//println "Writing lines..."
start = System.currentTimeMillis()
String annotHeader = ""
String separator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
for (def annot : annots) annotHeader += "\t$annot"
writer.write("N"+separator+"Références"+separator+"ContexteGauche"+separator+"Pivot"+annotHeader+separator+"ContexteDroit"+separator+"id"+separator+"text_id\n")

// define which occurrence properties will be displayed
concordance.setViewProperties([word])
int NLines = concordance.getNLines();
int chunk = 1000;
for (int i = 0 ; i < concordance.getNLines() ; i += chunk) {
	List<Line> lines
	if (i+chunk > NLines)
		lines = concordance.getLines(i, concordance.getNLines()-1)
	else
		lines = concordance.getLines(i, i+chunk-1) // e.g. from 0 to 999

	int n = 1
	for (Line l : lines) {
		String ntext = l.getViewRef().getValue(text_id)
		String lcontext = l.leftContextToString()
		String pivot = l.keywordToString()
		String rcontext = l.rightContextToString()
		String ids = l.getViewRef().getValue(id)
		String refValue = "";
		for (def refp : refProperties) refValue += " "+l.getViewRef().getValue(refp)

		String poss = "";
		for (def annot : annots) {
			poss += separator+l.getViewRef().getValue(annot)
		}

		//println l.getKeywordsViewProperties().get(pos)
		writer.write(""+(l.getKeywordPosition())+separator+refValue+separator+lcontext+separator+pivot+poss+separator+rcontext+separator+ids+separator+ntext+"\n")
	}
	writer.flush()
}
println "Saved in "+tsv_file.getAbsolutePath()
writer.close()

concordance.delete()
return true
