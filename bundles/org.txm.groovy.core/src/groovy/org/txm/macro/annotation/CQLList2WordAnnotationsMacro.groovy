
package org.txm.macro.annotation

// STANDARD DECLARATIONS
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.rcp.views.corpora.CorporaView
import org.txm.annotation.kr.core.KRAnnotationEngine
import org.txm.annotation.kr.core.repository.*

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName: please select a Corpus in the Corpus view."
	return 0
}

@Field @Option(name="queries_file", usage="The CQL query to select tokens to annotate", widget="File", required=true, def="queries.tsv")
File queries_file

@Field @Option(name="word_property", usage="The word property to annotate", widget="String", required=true, def="type")
String word_property

@Field @Option(name="annotation_scope", usage="the annotation value", widget="StringArray", metaVar="first|target	first	target	all", required=true, def="first|target")
		String annotation_scope

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

int total = 0;

queries_file.eachLine("UTF-8") { line ->
	String[] split = line.split("\t", 2)
	if (!line.startsWith("#") && split.length == 2 && split[0].length() > 0 && split[1].length() >= 0) {
		
		def hash = ["args":
			 ["query":split[1], "word_property":word_property, "annotation_value":split[0], "annotation_scope":annotation_scope],
			 "corpusViewSelection":corpusViewSelection
		]
		int n = gse.run(CQL2WordAnnotationsMacro, hash)
		if (n == 0) {
			println "## no annotation created with "+line
		} else {
			total += n
		}
	} else {
		println "## ignoring "+line
	}
}

println "$queries_file created $total annotations"

return total