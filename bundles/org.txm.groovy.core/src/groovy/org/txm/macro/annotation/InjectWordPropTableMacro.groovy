package org.txm.macro.annotation;

import org.txm.utils.*
import org.txm.utils.io.*

import java.nio.charset.Charset

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.core.preferences.TBXPreferences
import org.txm.rcp.swt.widget.parameters.*
/*
 * The macro writes the new property values in the XML-TXM files of the inputDirectory. The result is written in the outputDirectory
 * 
 * 
 * 
 */
@Field @Option(name="inputDirectory", usage="The XML-TXM folder", widget="Folder", required=true, def="")
def inputDirectory

@Field @Option(name="outputDirectory", usage="The result folder", widget="Folder", required=true, def="")
def outputDirectory

@Field @Option(name="tsv_File", usage="The annotation input file, in .tsv format", widget="FileOpen", required=true, def="")
def tsv_File

@Field @Option(name="properties", usage="columns to inject separated by commas", widget="String", required=true, def="p1, p2, ... , pn")
def properties

@Field @Option(name="debug", usage="Debug de the macro", widget="Boolean", required=false, def="false")
def debug

if (!ParametersDialog.open(this)) return false;

def split = properties.split(",")
if (split.length == 0) {
	println "** no property given"
	return false
}
properties = []
for (def p : split) properties << p.trim()

println "Injecting annotations with the following parameters:"
println "- inputDirectory: $inputDirectory"
println "- outputDirectory: $outputDirectory"
println "- tsv_File: $tsv_File"
println "- properties: $properties"

outputDirectory.mkdir()
if (!outputDirectory.exists()) {
	println "** Could not create or read $outputDirectory directory"
	return false
}

String separator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
CsvReader records = new CsvReader(tsv_File.getAbsolutePath(), separator.charAt(0), Charset.forName("UTF-8"))
records.setTextQualifier((char)0)
records.readHeaders();

def header = Arrays.asList(records.getHeaders())
if (debug) println "CSV header: $header"
for (def s : properties) if (!header.contains(s)) {
	println "** Missing annotation property: $s"
	return false;
}
for (def s : properties) {
	def s2 = AsciiUtils.buildAttributeId(s);
	 
	if (!s.equals(s2)) {
		println "** Wrong property name format: '$s'. Use '$s2' instead."
		return false;
	}
}
for (def s : ["text_id", "id"]) if (!header.contains(s)) {
	println "** Missing mandatory property: $s"
	return false;
}

def injections = [:]
int no = 1;
while (records.readRecord()) {
	
	String textid = records.get("text_id")
	String wid = records.get("id")
	
	if (wid == "") {
		 println "ERROR in record n="+no+": 'id' is empty"
	} else if (textid == "") {
		 println "ERROR in record n="+no+": no 'text_id' is empty"
	} else {
		if (!injections.containsKey(textid)) injections[textid] = [:]
		
		def props = [:]
		for (def s : properties) {
			props[s] = records.get(s)
		}
		injections[textid][wid] = props
	}
	no++;
}

println "Injecting properties..."
//for (String textid : injections.keySet()) {
def files = inputDirectory.listFiles().sort{ it.name }
def totalInjected = 0
ConsoleProgressBar cpb = new ConsoleProgressBar(files.size())
for (File currentXMLFile : files) {
	cpb.tick()
	if (currentXMLFile.isDirectory()) continue;
	if (currentXMLFile.isHidden()) continue;
	
	String textid = currentXMLFile.getName();
	int idx = textid.lastIndexOf(".")
	if (idx > 0) textid = textid.substring(0, idx);
	
	if (injections.containsKey(textid)) {
		if (debug) println "Injections of "+injections[textid].size()+" lines."
		File tmp = null
		String filename = currentXMLFile.getName()
		if (outputDirectory.equals(inputDirectory)) {
			
			tmp = File.createTempFile("tmp", currentXMLFile.getName())
			FileCopy.copy(currentXMLFile, tmp)
			//println "Patching input: $currentXMLFile with $tmp"
			currentXMLFile = tmp;
		}
		def builder = new AnnotationInjectionFilter(currentXMLFile, injections[textid], properties, debug)
		if (debug) println "injecting in $currentXMLFile $properties"
		builder.process(new File(outputDirectory, filename));
		
		if (tmp != null) tmp.delete(); // tmp != null if inputdir == outputir
		
		if (debug) println ""+builder.n+" lines injected."
		totalInjected += builder.n
	} else {
		if (!outputDirectory.equals(inputDirectory)) {
			println "copying file $currentXMLFile"
			FileCopy.copy(currentXMLFile, new File(outputDirectory, currentXMLFile.getName()))
		}
	}
}

cpb.done()
if (totalInjected ==0) {
	println "No new properties injected.."
} else {
	println "Updated XML-TXM files are saved in ${outputDirectory}."
}
return totalInjected > 0;
