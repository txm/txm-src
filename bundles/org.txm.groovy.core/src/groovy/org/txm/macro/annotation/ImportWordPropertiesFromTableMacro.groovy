package org.txm.macro.annotation;

import org.txm.utils.*
import org.txm.utils.io.*
import java.nio.charset.Charset
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.core.preferences.TBXPreferences
import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.MainCorpus

@Field @Option(name="tsv_File", usage="The annotation input file, in .tsv format", widget="FileOpen", required=true, def="")
def tsv_File

@Field @Option(name="properties", usage="columns to inject separated by commas", widget="String", required=true, def="p1, p2, ... , pn")
def properties

@Field @Option(name="update_edition", usage="Update the edition pages", widget="Boolean", required=true, def="false")
def update_edition

@Field @Option(name="debug", usage="Debug de the macro", widget="Boolean", required=false, def="false")
def debug

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "Error: selection must be a corpus (not: $corpusViewSelection)"
	return;
}

if (!ParametersDialog.open(this)) return false;

println "Importing the $properties properties in the $corpusViewSelection corpus"

File inputDirectory = new File(corpusViewSelection.getParent().getProjectDirectory(), "txm/"+corpusViewSelection.getID())

if (!inputDirectory.exists()) {
	println "Error: corpus XML-TXM files are absents: $inputDirectory"
	return false
}

if (inputDirectory.listFiles().length == 0) {
	println "Error: corpus XML-TXM files are absents: $inputDirectory"
	return false
}

// insert the section in the TRS files
def ret = gse.runMacro(InjectWordPropTableMacro, ["inputDirectory": inputDirectory
	, "outputDirectory": inputDirectory
	, "tsv_File": tsv_File
	, "properties": properties
	, "debug":debug] ,
)

if (ret) {
	
	corpusViewSelection.getProject().appendToLogs("$properties imported from the $tsv_File file")
	
	org.txm.rcp.commands.workspace.UpdateCorpus.update(corpusViewSelection, update_edition)
	return true
} else {
	println "** Aborting. See above for errors."
	return false
}


