package org.txm.macro.annotation

import javax.xml.stream.*

import org.txm.importer.StaxIdentityParser
import org.txm.importer.scripts.filters.*
import org.txm.scripts.importer.graal.PersonalNamespaceContext

/**
 * The Class AnnotationInjectionFilter.
 *
 * @author mdecorde
 *
 * inject annotation from a stand-off file into a xml-tei-txm file
 * 
 * TODO stop using this class and use XMLTXMWordPropertiesInjection
 */

public class AnnotationInjectionFilter extends StaxIdentityParser {

	public static String TXMNS = "http://textometrie.org/1.0"

	/** The xml reader factory. */
	private def factory;

	def xmlFile;
	def records;
	def properties;
	int n = 0;
	boolean debug = false

	public AnnotationInjectionFilter() {
		super(null, null, null);
	}
	
	/**
	 * Instantiates a new annotation injection.
	 *
	 * @param url the xml-tei-txm file
	 * @param anaurl the stand-off file
	 */
	public AnnotationInjectionFilter(File xmlFile, def records, def properties, def debug) {
		super(xmlFile.toURI().toURL()); // init reader and writer
		
		this.debug = debug
		this.xmlFile = xmlFile
		this.records = records
		this.properties = properties
		this.n = 0;
		//println ""+records.size()+" lines to process..."

		try {
			factory = XMLInputFactory.newInstance();
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	def data; // the word id properties to add/replace
	String newform = null
	String wordId;
	String anaType;
	boolean start = false;
	boolean replaceAnnotation = false;
	protected void processStartElement() {
		// this block don't write but only do tests
		switch (localname) {
			case "form":
				if (start) {
					def f = data.get("word")
					if (f != null && f.length() > 0)
						newform = f
				}
			case "w":
				for (int i= 0 ; i < parser.getAttributeCount() ; i++ ) {
					if (parser.getAttributeLocalName(i) == "id") {
						wordId = parser.getAttributeValue(i);
						data = records.get(wordId)
						if (records.containsKey(wordId)) {
							n++;
							start = true; // found a word to update !
							//println "found word $wordId"
						}
						break;
					}
				}
				break;
			case "ana":
				if (start) { // found a word to update
					for (int i= 0 ; i < parser.getAttributeCount() ; i++) { // look for the "type" attribute
						if (parser.getAttributeLocalName(i) == "type") { 
							anaType = parser.getAttributeValue(i);
							anaType = anaType.substring(1)
							//if (debug) println "type: $anaType"
							replaceAnnotation = properties.contains(anaType) // there is a new value for this property
							//if (replaceAnnotation) println " replace annotation $anaType"
							break;
						}
					}
				}
				break;
		}

		super.processStartElement();
	}

	@Override
	protected void processCharacters() {
		if (start && replaceAnnotation) return; // don't write annotation chars if we are currently replacing
		if (newform != null) { // don't rewrite form content
			// we are currently in a <w> tags
		} else {
			super.processCharacters();
		}
	}

	protected void processEndElement() {
		if (start) { // write the new content of the <w> tags
			switch (localname) {
				case "w":
					start = false
					if (data.keySet().size() > 0) writer.writeComment("\n");
					// write remaining properties
					if (debug) if (data.size() > 0) println "new ana: " +data
					for (def prop : data.keySet()) { // (1)
						// write prop
						writer.writeStartElement(TXMNS, "ana")
						writer.writeAttribute("resp", "txm")
						writer.writeAttribute("type", "#$prop")
						writer.writeCharacters(data.get(prop))
						writer.writeEndElement()
						writer.writeComment("\n")
						if (debug) println " create annotation $prop with "+data.get(prop)
					}
					break;
				case "form":
					if (start) newform = null
				case "ana":
					if (replaceAnnotation) {
						writer.writeCharacters(data.get(anaType))
						if (debug) println " replace annotation $anaType content with "+data.get(anaType)
						data.remove(anaType) // the anaType is removed because it is now written. I don't need to write it here (1)
					}
					anaType = ""
					replaceAnnotation = false;
					break;
			}
		}
		super.processEndElement();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		File xmlFile = new File("/home/mdecorde/TEMP/annotation/01_DeGaulle.xml")
		def properties = ["p1", "lemma"]
		def records = [
			"w_1":["p1":"a", "lemma":"b"],
			"w_224":["p1":"c", "lemma":"d"],
			"w_400":["p1":"e", "lemma":"f"],
			"w_750":["p1":"g", "lemma":"h"],
			"w_753":["p1":"i", "lemma":"j"],
			"w_756":["p1":"k", "lemma":"l"]
		]

		def builder = new AnnotationInjectionFilter(xmlFile, records, properties);
		builder.process(new File("/home/mdecorde/TEMP", "rez.xml"));

		return;
	}
}