package org.txm.macro.annotation

// STANDARD DECLARATIONS
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.rcp.utils.JobHandler
import org.txm.rcp.views.corpora.CorporaView
import org.eclipse.core.runtime.Status
import org.txm.annotation.kr.core.KRAnnotationEngine
import org.txm.annotation.kr.core.repository.*
import org.txm.rcp.commands.workspace.UpdateCorpus
import org.txm.annotation.kr.rcp.commands.SaveAnnotations
import org.txm.annotation.kr.rcp.concordance.WordAnnotationToolbar

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName: please select a Corpus in the Corpus view."
	return 0
}

CQPCorpus corpus = corpusViewSelection
MainCorpus mcorpus = corpus.getMainCorpus();

@Field @Option(name="queries_file", usage="The CQL query file to select tokens to annotate", widget="File", required=true, def="queries.tsv")
		File queries_file

@Field @Option(name="word_property", usage="The word property to annotate", widget="String", required=true, def="type")
		String word_property

		@Field @Option(name="scope", usage="the annotation value", widget="StringArray", metaVar="first|target	first	target	all", required=true, def="first|target")
		String scope
		
@Field @Option(name="update_corpus_indexes_and_editions", usage="Unselect to only update the XML-TXM files", widget="Boolean", required=true, def="true")
		def update_corpus_indexes_and_editions

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

	int total = 0;


def hash = ["args":	["queries_file":queries_file, "word_property":word_property, "annotation_scope":scope],
	"corpusViewSelection":corpusViewSelection, "gse":gse
]
int n = gse.run(CQLList2WordAnnotationsMacro, hash)

if (n > 0) {
	println "Saving $n annotations..."
	//	monitor.syncExec(new Runnable() {
	//		public void run() {
	def saveJob = SaveAnnotations.save(mcorpus);
	if (saveJob == null || saveJob.getResult() == Status.CANCEL_STATUS) {
		// update editor corpus
		System.out.println("** Error: failed to save the annotations of the corpus."); //$NON-NLS-1$
		return;
	} else {
		if (update_corpus_indexes_and_editions) {
			println "Updating corpus indexes and editions..."
			UpdateCorpus.update(mcorpus)
		}
		
		return; // something was saved
	}
	//		}
	//	});
} else {
	println "No annotation to save."
	return false
}
