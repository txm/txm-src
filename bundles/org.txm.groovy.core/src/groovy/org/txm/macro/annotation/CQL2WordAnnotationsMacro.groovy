
package org.txm.macro.annotation

// STANDARD DECLARATIONS
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.*
import org.txm.rcp.views.corpora.CorporaView
import org.txm.annotation.kr.core.KRAnnotationEngine
import org.txm.annotation.kr.core.AnnotationScope
import org.txm.annotation.kr.core.DatabasePersistenceManager
import org.txm.annotation.kr.core.repository.*

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName: please select a Corpus in the Corpus view."
	return 0
}

CQPCorpus corpus = corpusViewSelection

MainCorpus mcorpus = corpus.getMainCorpus();

KnowledgeRepository kr = null // the following code should be replaced with KnowledgeRepository.createSimpleKnowledgeRepository(mcorpus.getName())
if (KnowledgeRepositoryManager.getKnowledgeRepository(mcorpus.getName()) != null) {
	kr = KnowledgeRepositoryManager.getKnowledgeRepository(mcorpus.getName());
} else {
	HashMap<String, HashMap<String, ?>> conf = new HashMap<>();
	HashMap<String, String> access = new HashMap<>();
	conf.put(KRAnnotationEngine.KNOWLEDGE_ACCESS, access);
	HashMap<String, HashMap<String, String>> fields = new HashMap<>();
	conf.put(KRAnnotationEngine.KNOWLEDGE_TYPES, fields);
	HashMap<String, HashMap<String, String>> strings = new HashMap<>();
	conf.put(KRAnnotationEngine.KNOWLEDGE_STRINGS, strings);
	access.put("mode", DatabasePersistenceManager.ACCESS_FILE);
	access.put("version", "0");
	kr = KnowledgeRepositoryManager.createKnowledgeRepository(mcorpus.getName(), conf);
	KnowledgeRepositoryManager.registerKnowledgeRepository(kr);
}

@Field @Option(name="query", usage="The CQL query to select tokens to annotate", widget="String", required=true, def="")
		String query

@Field @Option(name="word_property", usage="The word_property to annotate", widget="String", required=true, def="type")
		String word_property

@Field @Option(name="annotation_value", usage="the annotation value", widget="String", required=true, def="annotation_value")
		String annotation_value

@Field @Option(name="annotation_scope", usage="the annotation value", widget="StringArray", metaVar="first|target	first	target	all", required=true, def="first|target")
		String annotation_scope

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

	if (query == null || query.trim().length() == 0) {
		println "Error: no query set."
		return 0
	}

annotManager = KRAnnotationEngine.getAnnotationManager(corpus);

AnnotationType type = kr.getType(word_property);
if (type == null) {
	AnnotationType t = kr.addType(word_property, word_property);
	t.setEffect(AnnotationEffect.TOKEN);
	type = t
} else {
	type.setEffect(AnnotationEffect.TOKEN);
}

value_to_add = kr.getValue(type, annotation_value);
if (value_to_add == null) { // create or not the annotation is simple mode
	value_to_add = kr.addValue(annotation_value, annotation_value, type.getId());
}
try {
	qr = corpus.query(new CQLQuery(query), "TMP", false)
	if (qr.getNMatch() == 0) {
		println "No match to annotate with $query"
		return 0
	}
} catch (Exception ex) {
	println "Query error ($query), no annotation done: "+ex
	return 0
}
annotManager.createAnnotations(type, value_to_add, qr.getMatches(), null, AnnotationScope.fromLabel(annotation_scope));

println query+" created "+qr.getNMatch()+" annotations."

return qr.getNMatch()
