// Copyright © 2021 ENS Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde
// @author sheiden

package org.txm.macro.annotation

// STANDARD DECLARATIONS
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.xml.DOMIdentityHook
import org.txm.xml.LocalNameHookActivator
import org.txm.xml.XMLProcessor
import org.txm.objects.*
import org.txm.rcp.utils.JobHandler
import org.txm.rcp.views.corpora.CorporaView
import org.eclipse.core.runtime.Status
import org.txm.annotation.kr.core.KRAnnotationEngine
import org.txm.annotation.kr.core.repository.*
import org.txm.annotation.kr.rcp.commands.SaveAnnotations
import org.txm.annotation.kr.rcp.concordance.WordAnnotationToolbar
import org.txm.importer.StaxIdentityParser

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof MainCorpus)) {
	println "** $scriptName: please select a Corpus in the Corpus view."
	return 0
}

MainCorpus mcorpus = corpusViewSelection
Project project = mcorpus.getProject()
File txmDir = new File(project.getProjectDirectory(), "txm/"+mcorpus.getName())

@Field @Option(name="outputDirectory", usage="Directory of resulting XML-TXM files", widget="Folder", required=true, def="")
File outputDirectory

@Field @Option(name="word_property", usage="The word property to project", widget="String", required=true, def="type")
String word_property

@Field @Option(name="values_to_ignore_regex", usage="regex of values not to project", widget="String", required=true, def="")
String values_to_ignore_regex

@Field @Option(name="debug", usage="Afficher les messages de debug", widget="Boolean", required=true, def="false")
def debug

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

if (!outputDirectory.exists()) {
	println "Error: output directory not found: "+outputDirectory
	return false
}

values_to_ignore_regex = /$values_to_ignore_regex/

project.compute();

for (Text text : project.getChildren(Text.class)) {
	println "Text: "+text
	
	File orig = text.getXMLTXMFile()
	if ((orig == null) || !orig.exists()) {
		println "** Error: no XML-TXM file found for "+text
		return // next !
	}
	
	File result = new File(outputDirectory, orig.getName())
	
	if (false && result.lastModified() > orig.lastModified()) {
		println "Skipping $text: result file is more recent: $result"
		continue // next !
	}
	
	XMLProcessor xp = new XMLProcessor(orig);
	LocalNameHookActivator activator = new LocalNameHookActivator("w")

	nWords = 0
	new DOMIdentityHook("word", activator, xp) {
		@Override
		public void processDom() {
			//println dom
			use(groovy.xml.dom.DOMCategory) {
				def form = dom.form[0]
				def anatype = dom.ana.findAll(){ a ->
					if (debug && (a['@type'] == "#$word_property")) {
						println 'a["@type"] = '+a["@type"]
						println 'a.text() = '+a.text()
						println 'a["@type"] == "#$word_property"'+" = "+(a['@type'] == "#$word_property")
						println "values_to_ignore_regex = "+values_to_ignore_regex
						println '!(a.text() ==~ values_to_ignore_regex))'+" = "+(!(a.text() ==~ values_to_ignore_regex))
						println '((a["@type"] == "#$word_property") && !(a.text() ==~ values_to_ignore_regex)) = '+((a['@type'] == "#$word_property") && !(a.text() ==~ values_to_ignore_regex))
					}
					return ((a['@type'] == "#$word_property") && !(a.text() ==~ values_to_ignore_regex))
				}
//				println form.text()+" -> $anatype"
				if (anatype.size() > 0) { // keep the word
					anatype = anatype[0]
					def s = form.text()
					form.setTextContent(anatype.text())
					anatype['@type'] = "#form"
					anatype.setTextContent(s)
					nWords++
				} else {
					dom = null
				}
			}
		}
	}
	xp.process(result)

	if (nWords == 0) {
		println "Removing empty <text>: $text: $result"
		result.delete()
	}
}
