// Copyright © 2017 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

package org.txm.macro.commands

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.Toolbox
import org.txm.macro.cqp.CQPUtils
import org.txm.searchengine.cqp.ICqiClient
import org.odftoolkit.odfdom.doc.*
import org.odftoolkit.odfdom.doc.table.*

// BEGINNING OF PARAMETERS

@Field @Option(name="inputFile", usage="input sequence list file (one sequence per line)", widget="File", required=true, def="")
def inputFile

@Field @Option(name="inputIsCQL", usage="input sequences are CQL queries", widget="Boolean", required=true, def="false")
def inputIsCQL

@Field @Option(name="inputWordProperty", usage="implicit word property to use", widget="String", required=true, def="word")
String inputWordProperty

@Field @Option(name="outputFile", usage="results output File (TSV format)", widget="File", required=true, def="")
def outputFile

@Field @Option(name="odsOutputFormat", usage="use ODS as output format", widget="Boolean", required=true, def="false")
def odsOutputFormat

@Field @Option(name="outputWordProperty", usage="output word property to use", widget="String", required=true, def="word")
String outputWordProperty

@Field @Option(name="groupByQuery", usage="provide frequency of each CQL match values or just the total frequency of each CQL match", widget="Boolean", required=true, def="false")
def groupByQuery

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def scriptName = this.class.getSimpleName()

if (!(inputFile && inputFile.isFile() && inputFile.canRead() )) {

	println "** $scriptName: impossible to read input file. Aborting."
	return false
}

corpusEngine = CQPSearchEngine.getCqiClient()

utils = new CQPUtils()

corpora = utils.getCorpora(this)

if ((corpora == null) || corpora.size() == 0) {
	println "** $scriptName: please select a corpus in the Corpus view or provide a corpus name. Aborting."
	return false
}

def getComputerName()
{
	env = System.getenv()
	if (env.containsKey("COMPUTERNAME"))
		return env.get("COMPUTERNAME")
	else if (env.containsKey("HOSTNAME"))
		return env.get("HOSTNAME")
	else
		return InetAddress.getLocalHost().getHostName()
}

def fullName = outputFile.getName()
def i = fullName.lastIndexOf(".")
def name
if (i == -1) {
	name = fullName
} else {
	name = fullName.substring(0, i)
}

if (odsOutputFormat) {

	outputFile = new File(outputFile.getParentFile(), name+".ods")
	ods = OdfSpreadsheetDocument.newSpreadsheetDocument();
	table = ods.getTableByName("Sheet1")

	properties = OdfTable.newTable(ods);
		properties.setTableName("Properties")
	setStringCellBold(properties, 0, 0, "Name")
	setStringCellBold(properties, 0, 1, "Value")
	line = 1
	setStringCell2(properties, line++, 0, "date", new Date().format("dd/MM/yyyy"))
	setStringCell2(properties, line++, 0, "time", new Date().format("HH'h'mm"))
	setStringCell2(properties, line++, 0, "machine", getComputerName())
	setStringCell2(properties, line++, 0, "user", System.getProperty("user.name"))
	setStringCell2(properties, line++, 0, "macro", scriptName)
	setStringCell2(properties, line++, 0, "corpora", corpora.collect { it.getName() }.join(","))
	setStringCell2(properties, line++, 0, "inputFile", inputFile.getAbsolutePath())
	setStringCell2(properties, line++, 0, "inputIsCQL", inputIsCQL.toString())
	setStringCell2(properties, line++, 0, "inputWordProperty", inputWordProperty)
	setStringCell2(properties, line++, 0, "outputFile", outputFile.getAbsolutePath())
	setStringCell2(properties, line++, 0, "odsOutputFormat", odsOutputFormat.toString())
	setStringCell2(properties, line++, 0, "outputWordProperty", outputWordProperty)
	setStringCell2(properties, line++, 0, "groupByQuery", groupByQuery.toString())

} else {
	outputFile = new File(outputFile.getParentFile(), name+".tsv")
	output = new FileWriter(outputFile)
}

def setRow(table, row, corpus, string, frequency) {
	cell=table.getCellByPosition(0, row)
	cell.setStringValue(corpus)
	cell=table.getCellByPosition(1, row)
	cell.setStringValue(string)
	cell=table.getCellByPosition(2, row)
	cell.setDoubleValue(frequency)
}

def setRow2(table, row, corpus, string1, string2, frequency) {
	cell=table.getCellByPosition(0, row)
	cell.setStringValue(corpus)
	cell=table.getCellByPosition(1, row)
	cell.setStringValue(string1)
	cell=table.getCellByPosition(2, row)
	cell.setStringValue((frequency == 0)?'':string2)
	cell=table.getCellByPosition(3, row)
	cell.setDoubleValue(frequency)
}

def setStringCell(table, row, col, string) {
	cell=table.getCellByPosition(col, row)
	cell.setStringValue(string)
	return cell
}

def setStringCellBold(table, row, col, string) {
	cell=table.getCellByPosition(col, row)
	cell.setStringValue(string)
	return cell
}

def setStringCell2(table, row, col, string1, string2) {
	cell=table.getCellByPosition(col, row)
	cell.setStringValue(string1)
	cell=table.getCellByPosition(col+1, row)
	cell.setStringValue(string2)
	return cell
}

if (odsOutputFormat) {
	if (groupByQuery) {
		setStringCellBold(table, 0, 0, "corpus")
		setStringCellBold(table, 0, 1, inputIsCQL?"CQL":outputWordProperty+" query")
		cell = setStringCellBold(table, 0, 2, "f")
	} else {
		setStringCellBold(table, 0, 0, "corpus")
		setStringCellBold(table, 0, 1, inputIsCQL?"CQL":outputWordProperty+" query")
		setStringCellBold(table, 0, 2, outputWordProperty+" values")
		cell = setStringCellBold(table, 0, 3, "f")
	}
} else {
	if (groupByQuery) {
		output.println "corpus\t"+(inputIsCQL?"CQL":outputWordProperty+" query")+"\tf"
	} else {
		output.println "corpus\t"+(inputIsCQL?"CQL":outputWordProperty+" query")+"\t"+(outputWordProperty+" values")+"\tf"
	}
}

row = 1

corpora.each { corpus ->

	corpusName = corpus.getName()
	seqN = 0
	inputFile.eachLine() { line ->

		cql = ""
		subCorpus = "NIndex"+seqN++

		if (line.length() > 0) {
			if (inputIsCQL) {
				cql = line
			} else {
				cql = "[$inputWordProperty=\""+line.replaceAll("\\p{Blank}+", "\"] [$inputWordProperty=\"",)+"\"]"
			}

			corpusEngine.cqpQuery(corpusName, "$subCorpus", cql)
			nmatches = corpusEngine.subCorpusSize("$corpusName:$subCorpus")
			if (nmatches == 0) {
				if (groupByQuery) {
					if (odsOutputFormat) {
						setRow(table, row++, corpusName, line, 0)
					} else {
						output.println corpusName+"\t"+line+"\t0"
					}
				} else {
					if (odsOutputFormat) {
						setRow2(table, row++, corpusName, line, line, 0)
					} else {
						output.println corpusName+"\t"+line+"\t\t0"
					}
				}
			} else {
				if (groupByQuery) {
					if (odsOutputFormat) {
						setRow(table, row++, corpusName, line, nmatches)
					} else {
						output.println corpusName+"\t"+line+"\t$nmatches"
					}
				} else {
					starts = corpusEngine.dumpSubCorpus("$corpusName:$subCorpus", ICqiClient.CQI_CONST_FIELD_MATCH, 0, nmatches-1)
					ends = corpusEngine.dumpSubCorpus("$corpusName:$subCorpus", ICqiClient.CQI_CONST_FIELD_MATCHEND, 0, nmatches-1)
					[starts, ends].transpose().collect {
						corpusEngine.cpos2Str("$corpusName.$outputWordProperty", (it[0]..it[1]) as int[]).join(' ')
					}.countBy { it }.sort { -it.value }.each {
						if (odsOutputFormat) {
							setRow2(table, row++, corpusName, line, it.key, it.value)
						} else {
							output.println corpusName+"\t"+line+"\t"+it.key+"\t"+it.value
						}
					}
				}
			}
		}
	}
}

if (odsOutputFormat) {
	ods.save(outputFile)
} else {
	output.close()
}

println "Done: "+outputFile.getAbsolutePath()
return true
