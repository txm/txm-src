// Copyright © 2018 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde, sheiden

package org.txm.macro.commands

// STANDARD DECLARATIONS
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.rcp.views.corpora.CorporaView

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "** $scriptName: please select a Corpus in the Corpus view."
	return 0
}

CQPCorpus corpus = corpusViewSelection

// PARAMETERS ...................................
// - NAME: name of the partition to build
// - PARTNAMES: list of each partition part name
// - QUERIES: list of each partition part building query (must have the same length as PARTNAMES)

def NAME = "decennies"

def PARTNAMES = [
"60s", 
"70s",
"80s",
"90s",
"2000s",
"2010s",
]

def QUERIES = [
'[_.text_annee="(1959|196.)"] expand to text', 
'[_.text_annee="197."] expand to text', 
'[_.text_annee="198."] expand to text', 
'[_.text_annee="199."] expand to text', 
'[_.text_annee="200."] expand to text', 
'[_.text_annee="201."] expand to text', 
]

// end of PARAMETERS

// BODY

if (QUERIES.size() == PARTNAMES.size()) {

	def partition
	
	println "Building partition on $corpus (size = "+(corpus.getSize())+")."
	
	partition = new Partition(corpus)
	partition.setParameters(NAME, QUERIES, PARTNAMES)
	partition.compute()
	
	def list = Arrays.asList(partition.getPartSizes())

	println "'$NAME' partition created, parts sizes = "+list+", Σ(parts) = "+list.sum()+"."

	// println "Total size: "+list.sum()+" - is equal to (sub)corpus size : "+(list.sum() == partition.getCorpus().getSize())
	
	monitor.syncExec(new Runnable() {
	public void run() {
		CorporaView.refresh()
		CorporaView.expand(partition.getParent())
	}
})
} else {
	println "** $scriptName: PARTNAMES.size() != QUERIES.size(), ("+PARTNAMES.size()+" != "+QUERIES.size()+")."
	return 0
}
