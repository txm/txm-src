package org.txm.macro.commands;

class Struct implements Comparable<Struct> {

	String  name
	Integer start
	Integer end

	Struct(String n, Integer s, Integer e) {
		name  = n
		start = s
		end   = e
	}

	public int compareTo(Struct s) {
		if (start < s.start && end > s.end) { 			// self contains s : [ { } ]
			//println sprintf("%s[%d, %d] ^ %s[%d, %d]", name, start, end, s.name, s.start, s.end)
			return -1
		} else if (start > s.start && end < s.end) { 	// s contains self : { [ ] }
			//println sprintf("%s[%d, %d] v %s[%d, %d]", name, start, end, s.name, s.start, s.end)
			return 1
		} else if (start == s.start && end == s.end) { 	// self and s have the same intervals : [{ }]
			//println sprintf("%s[%d, %d] = %s[%d, %d]", name, start, end, s.name, s.start, s.end)
			return name.compareTo(s.name) // use the lexicographic order of the structure names
		} else if (start < s.start) { 					// interval starting on the left comes first : [ { ...
			return -1
		} else if (start > s.start) { 					// interval starting on the right comes after : { [ ...
			return 1
		} else if (end > s.end) { 						// same start, interval ending on the right comes before : [{ } ]...
			return -1
		} else if (end < s.end) { 						// same start, interval ending on the right comes before : [{ ] }...
			return -1
		} else {										// same start, same end : [{ ]}...
			return name.compareTo(s.name) // use the lexicographic order of the structure names
		}
	}

	public toString(Struct s) {
		sprintf("%s[%d, %d]", s.name, s.start, s.end)
	}

	public print(Struct s) {
		print(s.toString())
	}
}