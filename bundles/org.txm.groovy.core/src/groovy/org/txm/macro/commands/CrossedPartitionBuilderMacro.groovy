package org.txm.macro.commands

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.Partition
import org.txm.utils.logger.Log
import org.txm.rcp.views.corpora.*

//BEGINNING OF PARAMETERS
if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Error: this macro should be run with a Corpus selected"
	return
}

corpus = corpusViewSelection

@Field @Option(name="structuralUnitList", usage="the structural Unit list separated with commas", widget="String", required=true, def="text")
def structuralUnitList

@Field @Option(name="structuralUnitPropertiesList", usage="the structural Unit properties list separated with commas", widget="String", required=true, def="p1,p2")
def structuralUnitPropertiesList

@Field @Option(name="propertySeparator", usage="property name separator", widget="String", required=true, def="_") //  ∩ 
def propertySeparator
		
//@Field @Option(name="expandTarget", usage="Expand structure", widget="String", required=false, def="")
def expandTarget = "";

//@Field @Option(name="structuralUnitToIgnore", usage="the structural Unit properties list to ignore separated with commas", widget="String", required=false, def="")
def structuralUnitToIgnore = "";

//@Field @Option(name="partitionName", usage="The partition name to use", widget="String", required=false, def="")
def partitionName = ""

//@Field @Option(name="partNamePrefix", usage="the part prefix to use", widget="String", required=true, def="")
def partNamePrefix = ""

@Field @Option(name="debug", usage="If debug, then show only queries", widget="Boolean", required=false, def="false")
def debug = true;

//Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

//END OF PARAMETERS

/**
 * Create partition with advanced CQL queries and autoname the parts.
 * Can create partitions by defining a multi-level structural units hierarchy or by defining several properties values for one structural unit.
 * Can define some structural units to remove from the resulting parts subcorpus.
 *
 */

// TODO: add this variables to the macro parameters

def structuralUnits = []
def structuralUnitProperties = []
structuralUnitList = structuralUnitList.trim()
structuralUnitPropertiesList = structuralUnitPropertiesList.trim()

// Build lists
def split = structuralUnitList.split(",")
def split2 = structuralUnitPropertiesList.split(",")
if (split.size() == 1) {
	for (String p : split2) {
		p = p.trim();
		structuralUnits << structuralUnitList
		structuralUnitProperties << p
	}
} else {
	if (split.size() == split2.size()) {
		for (int i = 0 ; i < split.size() ; i++) {
			def su = split[i]
			def p = split2[i];
			structuralUnits << su
			structuralUnitProperties << p
		}
	} else {
		println "Error: structuralUnitList size is different from structuralUnitPropertiesList"
		println "structuralUnitList=structuralUnitList=$structuralUnitList structuralUnitPropertiesList=$structuralUnitPropertiesList"
		return false
	}
}

// check properties existance
try {
	boolean ok = true;
	for (int i = 0 ; i < structuralUnits.size() ; i++) {
		def suName = structuralUnits[i]
		def pName = structuralUnitProperties[i]
		
		if (corpus.getStructuralUnit(suName) == null) {
			println "Error: no '$suName' structure in the '$corpus' corpus"
			ok = false;
		} else {
			def su = corpus.getStructuralUnit(suName)
			for (String p : structuralUnitProperties) {
				if (su.getProperty(pName) == null) {
					println "Error: no '$pName' structure property in the '$corpus' corpus"
					ok = false;
				}
			}
		}
	}
	if (!ok) return;
} catch(Exception e) {
	println "Error: $e"
	Log.printStackTrace(e)
	return
}



//Running
PartitionQueriesGenerator pqg = new PartitionQueriesGenerator();
try {
	def partition = pqg.createPartition(corpus, debug,
			partitionName, partNamePrefix,
			structuralUnits, structuralUnitProperties,
			structuralUnitToIgnore, expandTarget);

	if (partition != null)	{
		monitor.syncExec(new Runnable() {
					public void run() {
						CorporaView.refresh();
						CorporaView.expand(partition.getParent());
					}
				});
	}
} catch(Exception e) {
	println "Exception $e"
	Log.printStackTrace(e);
	return;
}

/**
 * Create a list of queries and part names regarding the structural units, structural units properties, structural units to ignore user defined lists and expand target value specified.
 * @author s
 *
 */
public class PartitionQueriesGenerator	{

	public boolean DEBUG = false;					// si DEBUG, alors les requêtes sont affichées mais la partition n'est pas créée

	public String PARTITION_NAME = "";				// Nom de la partition (optionnel)
	public def STRUCTURAL_UNITS = [];				// Liste des unités structurelles sur lesquelles effectuer la partition, ex: ['text', 'div1']
	public def STRUCTURAL_UNITS_PROPERTIES = [];	// Propriétés des unités structurelles sur lesquelles effectuer la partition, ex : ['id', 'name']
	public def STRUCTURAL_UNITS_TO_IGNORE = [];	// Structure à ignorer, ex. CQL : !speaker (optionnel)
	public String PART_NAMES_PREFIX = '';			// Prefix pour les noms de partie (optionnel)
	public String EXPAND_TARGET = null;			// Expand to target, englobe les empans jusqu'à la balise parente spécifiée.
	// NOTE : Le expand entre en conflit avec les sections à ignorer.
	// Si la target est à un niveau supérieur aux balises à ignorer, il les remet dans liste de résultats CWB et elles ne sont donc pas ignorées

	public def queries = [];
	public def partNames = [];

	/**
	 * Init the generator and process.
	 * @param corpusName
	 */
	public Partition createPartition(CQPCorpus corpus, boolean debug,
			String partitionName, String partNamePrefix,
			def structuralUnits, def structuralUnitProperties,
			def structuralUnitToIgnore, String expandTarget) 	{

		PARTITION_NAME = partitionName
		STRUCTURAL_UNITS = structuralUnits
		STRUCTURAL_UNITS_PROPERTIES = structuralUnitProperties
		STRUCTURAL_UNITS_TO_IGNORE = structuralUnitToIgnore
		PART_NAMES_PREFIX = partNamePrefix
		EXPAND_TARGET = expandTarget
		DEBUG = debug;

		if (DEBUG) {
			println "Arguments: "
			println "PARTITION_NAME = $PARTITION_NAME"
			println "STRUCTURAL_UNITS = $STRUCTURAL_UNITS"
			println "STRUCTURAL_UNITS_PROPERTIES = $STRUCTURAL_UNITS_PROPERTIES"
			println "STRUCTURAL_UNITS_TO_IGNORE = $STRUCTURAL_UNITS_TO_IGNORE"
			println "PART_NAMES_PREFIX = $PART_NAMES_PREFIX"
			println "EXPAND_TARGET = $EXPAND_TARGET"
			println "DEBUG = $DEBUG"
		}

		if (STRUCTURAL_UNITS.size() > 0 && STRUCTURAL_UNITS.size() == STRUCTURAL_UNITS_PROPERTIES.size())	{

			if (DEBUG) println 'Creating the queries on corpus "' + corpus + "'" ;
			if (DEBUG) println 'PARTITION_NAME: ' + PARTITION_NAME;

			//Corpus corpus = CorpusManager.getCorpusManager().getCorpus(corpusName);

			// Recursing through the corpus and subcorpus
			process(corpus, 0, '', '', "_");

			// Displaying the queries
			if (DEBUG) {
				println "Queries processed: "
				for (int i = 0 ; i < queries.size() ; i++) {
					println partNames[i] + " = " + queries[i]
				}
			}

			// Finalizing the queries
			finalizeQueries();

			// Displaying the queries
			if (DEBUG) {
				println "Queries finalized: "
				for (int i = 0 ; i < queries.size() ; i++) {
					println partNames[i] + " = " + queries[i]
				}
			}

			if (DEBUG) println 'Queries created.';

			// Creating the partition
			if (!DEBUG  && queries.size() == partNames.size()) {
				Partition partition = new Partition(corpus)
				partition.setParameters(PARTITION_NAME, queries, partNames);
				partition.compute()
				return partition
			}
		} else {
			println "Error: Structural units count or structural units properties count error.";
			return null
		}
	}

	/**
	 * Recurse through structural units and structural units properties of corpus and create the queries and the part names.
	 * @param corpus the corpus or subcorpus
	 * @param index the index for recursion
	 * @param tmpQuery the temporary query for creating subcorpus part
	 * @param tmpPartName the temporary part name of the subcorpus part
	 */
	protected void process(CQPCorpus corpus, int index, String tmpQuery, String tmpPartName, String propertySeparator)	{
		// End of array
		if (index >= STRUCTURAL_UNITS.size()) {

			queries.add(tmpQuery);
			partNames.add(PART_NAMES_PREFIX + tmpPartName);

			return;
		}

		StructuralUnit su = corpus.getStructuralUnit(STRUCTURAL_UNITS[index]);
		StructuralUnitProperty sup = su.getProperty(STRUCTURAL_UNITS_PROPERTIES[index]);

		if (DEBUG) {
			if (index == 0)	{
				println 'Pocessing Structural Unit Property "' + sup.getFullName() + '" on mother corpus "' + corpus.getID() + '"';
			} else {
				println 'Pocessing Structural Unit Property "' + sup.getFullName() + '" on subcorpus part "' + tmpPartName + '"';
			}
		}


		// Creating the queries parts for each structural units properties values
		// for (supValue in sup.getOrderedValues()) { // TODO : signaler bug Matthieu, on ne devrait pas être obligé de repasser le sous-corpus à la méthode car sup a déjà été créée depuis le sous-corpus ? getValues() bugge aussi
		for (supValue in sup.getOrderedValues(corpus)) {

			// TODO : Log
			if (DEBUG) println 'Value "' + supValue + '"';

			// Getting the subcorpus linked to the structural unit property value
			Subcorpus tmpSubcorpus = corpus.createSubcorpusWithQueryString(su, sup, supValue, "tmp" + UUID.randomUUID());

			// Partition conditions and part name separators
			String and = '';
			String underscore = '';
			if (tmpQuery != '')	{
				underscore = propertySeparator;
				and = ' & ';
			}

			process(tmpSubcorpus, index + 1, (tmpQuery + and + '_.' + sup.getFullName() + '="' + supValue + '"'), tmpPartName + underscore + supValue, propertySeparator);

			// Deleting the temporary subcorpus
			// TODO : bug : cette méthode ne supprime pas le corpus sans doute car il faut que le sous-corpus ne contienne pas d'autres sous-corpus ? le delete() en revanche fonctionne.
			//			corpus.dropSubcorpus(tmpSubcorpus);
			tmpSubcorpus.delete();
		}
	}


	/**
	 * 	Autoname the partition.
	 * @param partitionName
	 */
	protected void autoNamePartition(String partitionName) {

		// Structural units names and properties
		for (int i = 0; i < STRUCTURAL_UNITS.size(); i++) {
			partitionName +=  STRUCTURAL_UNITS[i] + '_' + STRUCTURAL_UNITS_PROPERTIES[i] + ' x ';
		}

		// Structural units to ignore
		for (int i = 0; i < STRUCTURAL_UNITS_TO_IGNORE.size(); i++) {
			partitionName +=  'NOT_' + STRUCTURAL_UNITS_TO_IGNORE[i] + '.';
		}

		// Removing last 'x' in partition name
		PARTITION_NAME = partitionName.substring(0, partitionName.length() - 3);
	}

	/**
	 * Finalize the queries.
	 */
	protected void finalizeQueries() {

		String expandTo = '';
		// Expanding to user defined target
		if (EXPAND_TARGET != null && EXPAND_TARGET.length() > 0) {
			expandTo = ' expand to ' + EXPAND_TARGET;
		}
		// Expanding to last child structural unit in user defined hierarchy
		else if(STRUCTURAL_UNITS_TO_IGNORE.size() == 0)	 {
			expandTo = ' expand to ' + STRUCTURAL_UNITS[STRUCTURAL_UNITS.size() - 1];
		}

		// Autonaming the partition
		if (PARTITION_NAME != null && PARTITION_NAME.length() == 0) {
			autoNamePartition(PARTITION_NAME);
			// Finalizing partition name
			if (EXPAND_TARGET != null && EXPAND_TARGET.length() > 0)
				PARTITION_NAME += expandTo.replace(' expand to', '.EXPAND TO').replace(' ', '_');
		}

		// Finalizing queries
		for (int j = 0; j < queries.size(); j++) {

			String queryEnd = '';

			// Removing some sections
			for (sectionToIgnore in STRUCTURAL_UNITS_TO_IGNORE) {
				queryEnd += ' & !' + sectionToIgnore;
			}

			queryEnd += ']' + expandTo;
			queries.set(j, '[' +  queries.get(j) + queryEnd);
		}
	}
}