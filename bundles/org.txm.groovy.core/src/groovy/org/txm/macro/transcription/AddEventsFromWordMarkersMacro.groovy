package org.txm.macro.transcription

import java.nio.charset.Charset

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.txm.utils.*
import org.txm.utils.logger.*

@Field @Option(name="trsDirectory", usage="Dossier qui contient les fichiers TRS", widget="Folder", required=true, def="")
		File trsDirectory;

@Field @Option(name="resultDirectory", usage="Dossier résultat TRS", widget="Folder", required=true, def="")
		File resultDirectory;
		
@Field @Option(name="wordElement", usage="The marker, spaces included", widget="String", required=true, def="w")
		def wordElement

@Field @Option(name="newEventMarker", usage="The marker, spaces included", widget="String", required=true, def="")
		def newEventMarker
		
@Field @Option(name="eventType", usage="The marker, spaces included", widget="String", required=true, def="")
		def eventType
		
@Field @Option(name="eventDescription", usage="The marker, spaces included", widget="String", required=false, def="")
		def eventDescription


@Field @Option(name="eventExtent", usage="The marker, spaces included", widget="String", required=true, def="instantaneous")
		def eventExtent
		
@Field @Option(name="debug", usage="activate debug messages", widget="Boolean", required=true, def="false")
		boolean debug

if (!ParametersDialog.open(this)) return;

if (!trsDirectory.exists()) {
	println "$trsDirectory not found"
	return
}

println "Writing result to $resultDirectory..."
def newEventMarkerRegex = /$newEventMarker/
try {
	def trsFiles = trsDirectory.listFiles().findAll() { it.getName().toLowerCase().endsWith(".trs") }
	
	if (trsFiles.size() == 0) {
		println "No TRS file to process in $trsDirectory"
		return;
	}
	
	ConsoleProgressBar cpb = new ConsoleProgressBar(trsFiles.size())
	for (File trsFile : trsFiles) {
		if (debug) println "== $trsFile =="
		else cpb.tick()
		
		// Open input file
		def slurper = new groovy.xml.XmlParser(false, true, true);
		//slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false) // allow DTD declaration
		//slurper.setProperty("http://javax.xml.XMLConstants/property/accessExternalDTD", "all"); // allow to read DTD from local file
		def trs = slurper.parse(trsFile.toURI().toString())
		def trsEpisodes = trs.Episode // 1
		if (trsEpisodes.size() > 1) {
			println "Error: multiple Episode node in $trsFile"
			continue
		}
		
		def turns = trs.Episode.Section.Turn

		for (int iTurn = 0 ; iTurn < turns.size() ; iTurn++) {
			def turn = turns[iTurn]
			def children = turn.children()
			for (int i = 0 ; i < children.size() ; i++) {
				def w = children[i]
				if (w instanceof String) {
					println "???? "+w
				}
				if (!(w instanceof String) && wordElement.equals(w.name())) {
					String wContent = w.text()
					if (wContent ==~ newEventMarkerRegex) {
						if (debug) println "Create event $w with $wContent"
						Node replace = new Node(null, "event");
						replace.@type = eventType
						replace.@desc = eventDescription
						replace.@extent = eventExtent
						w.replaceNode(replace)
					}
				}
			}
		}
		
		resultDirectory.mkdir()
		File outfile = new File(resultDirectory, trsFile.getName())
		outfile.withWriter("UTF-8") { writer ->
			writer.write('<?xml version="1.0" encoding="UTF-8"?>\n')
			def printer = new groovy.xml.XmlNodePrinter(new PrintWriter(writer))
			printer.setPreserveWhitespace(true)
			printer.print(trs)
		}
	}
	cpb.done()
	
	println "Done."
	
} catch(Exception e) {
	println "Error: "+e
	Log.printStackTrace(e)
}
