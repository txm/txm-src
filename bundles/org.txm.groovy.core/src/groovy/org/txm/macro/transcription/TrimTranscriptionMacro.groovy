// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.transcription

import java.text.SimpleDateFormat;
import java.util.Date;
import groovy.xml.XmlParser

@Field @Option(name="trsFile", usage="Input TRS file", widget="FileOpen", required=true, def="")
		File trsFile;

@Field @Option(name="outputTrsFile", usage="Output TRS file", widget="FileSave", required=true, def="")
		File outputTrsFile;

@Field @Option(name="startOfRange", usage="Range start time in seconds", widget="String", required=true, def="0.0")
		def startOfRange

@Field @Option(name="endOfRange", usage="Range end time in seconds", widget="String", required=true, def="")
		def endOfRange

if (!ParametersDialog.open(this)) return;

startOfRange = Double.parseDouble(startOfRange)
endOfRange = Double.parseDouble(endOfRange)

def slurper = new XmlParser();
def trs = slurper.parse(trsFile)

// remove Turn not in the range
def sectionsToRemove = []
for (def section : trs.Episode.Section) { // all Section
	def toRemove = []
	def nTurnAndSync = 0
	for (def turn : section.Turn) { // all Section children

		def start = Double.parseDouble(getStartTime(turn))
		def end = Double.parseDouble(getEndTime(turn))

		//println "Removing ${turn.name()} at $start + $end in $startOfRange and $endOfRange ?"
		// cropping
		if (end < startOfRange) {
			toRemove << turn;
			continue // remove the whole Turn
		} else if (start > endOfRange) {
			toRemove << turn;
			continue // remove the whole Turn
		}

		boolean removing = true;
		def children = turn.children()
		for (int i = 0 ; i < children.size() ; i++) { // all Turn children ; using old for+int syntax
			def child = children[i]
			if (!(child instanceof String) && child.name().equals("Sync")) {

				start = Double.parseDouble(getStartTime(child))
				end = Double.parseDouble(getEndTime(child))

				//println "Removing ${child.name()} at $start + $end in $startOfRange and $endOfRange ?"
				// cropping
				if (end < startOfRange) removing = true;
				else if (start > endOfRange) removing = true;
				else removing = false

				// updating start and end times
				if (startOfRange > 0) {
					setStartTime(child, start - startOfRange)
					setEndTime(child, end - startOfRange)
				}
			}

			if (removing) {
				//println "remove $child"
				children.remove(child)
				i--
			}
		}
		turn.value = children
	}
	
	for (def child : toRemove) child.parent().remove(child);
	
	if (section.Turn.size() == 0) { // all Turns and Syncs have been removed -> remove the section
		sectionsToRemove << section
	}
}

for (def section : sectionsToRemove) section.parent().remove(section);

// fixing startTimes and endTimes of Turn using last Sync.@endTime and Sync.@startTime
for (def turn : trs.Episode.Section.Turn) {
	def t = turn.Sync
	if (t.size() == 0) continue;
	turn.@startTime = t[0].@time
	turn.@endTime = t[-1].@time
}

// fixing startTimes and endTimes of Section using last Turn.@endTime and Turn.@startTime
for (def section : trs.Episode.Section) {
	def t = section.Turn
	if (t.size() == 0) continue;
	section.@startTime = t[0].@startTime
	section.@endTime = t[-1].@endTime
}

outputTrsFile.withWriter("UTF-8") { writer ->
	writer.write('<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE Trans SYSTEM "trans-14.dtd">\n')
	new groovy.xml.XmlNodePrinter(new PrintWriter(writer)).print(trs) }

return true;

// UTILS METHODS TO ACCES TIMES

def getStartTime(def node) {
	def ret = null;
	if (node == null) return "0.0"
	if (node.name() == null) return "0.0"
	switch(node.name()) {
		case "Turn":
		case "Section":
		//			println "Turn"
			ret = node.@startTime
			break
		case "Sync":
		//		println "Sync"
			ret = node.@time
			break
		default: break;
	}
	//println "getStartTime "+node.name()+" $ret"
	return ret
}

def setStartTime(def node, def value) {
	switch(node.name()) {
		case "Turn":
		case "Section":
			node.@startTime = String.format("%0,.2f", value)
			break;

		case "Sync":
			return node.@time = String.format("%0,.2f", value)
		default: break;
	}
}

def getEndTime(def node) {
	def ret = null;
	if (node == null) return "0.0"
	if (node.name() == null) return "0.0"
	switch(node.name()) {
		case "Turn":
		case "Section":
		//			println "Turn"
			ret = node.@endTime
			break
		case "Sync":
		//		println "Sync"
			ret = node.@time
			break
		default: break;
	}
	//println "getStartTime "+node.name()+" $ret"
	return ret
}

def setEndTime(def node, def value) {
	switch(node.name()) {
		case "Turn":
		case "Section":
			node.@endTime = String.format("%0,.2f", value)
			break;

		case "Sync":
			return node.@time = String.format("%0,.2f", value)
		default: break;
	}
}

def fixSyncTimes(def list, def start, def end) {
	println "Nfix: "+list.size()+" "+list

	def startf = Double.parseDouble(start)
	def endf = Double.parseDouble(end)
	def delta = (endf-startf)/list.size()
	println "start=$start end=$end delta=$delta"

	float c = startf;
	for (int i = 0 ; i < list.size() ; i++) {
		c += delta;
		list[i].@time = ""+c
	}
}
