package org.txm.macro.transcription

import javax.xml.stream.*

import org.txm.importer.PersonalNamespaceContext

import java.io.BufferedOutputStream
import java.io.FileOutputStream
import java.net.URL
import java.util.regex.Pattern

class Vocapia2Transcriber {
	
	File xmlfile;
	protected BufferedOutputStream output;
	protected XMLStreamWriter writer;
	
	def additionalSpeakers = [:]
	
	public Vocapia2Transcriber(File xmlfile) {
		
		this.xmlfile = xmlfile;
	}
	
	public void setAddtionalSpeakers(def additionalSpeakers) {
		this.additionalSpeakers = additionalSpeakers
	}
	
	public boolean process(File outfile) {
		
		if (!xmlfile.exists()) return false;
		
		output = new BufferedOutputStream(new FileOutputStream(outfile), 16 * 1024);
		writer = XMLOutputFactory.newInstance().createXMLStreamWriter(output, "ISO-8859-1");// create a new file
		writer.setNamespaceContext(new PersonalNamespaceContext());
		
		URL url = xmlfile.toURI().toURL();
		String filename = outfile.getName()
		filename = filename.substring(0, filename.length()-4); // remove ".cqp"
		def inputData = url.openStream();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(inputData);
		
		boolean flagWord = false
		def winfos = new LinkedHashMap()
		def turninfos = new LinkedHashMap()
		boolean other = false;
		String word = ""
		String duration = "0.0"
		try {
			
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				
				switch (event) {
					case XMLStreamConstants.START_ELEMENT:
						String localname = parser.getLocalName()
						switch(localname) {
							case "AudioDoc": // <AudioDoc name="xyz" path="xyz.flac"> -> <Trans scribe="see Proc elements" audio_filename="xyz.flac" version="see Proc elements" version_date="see Proc elements">
							
								writer.writeStartDocument("ISO-8859-1", "1.0")
							
								writer.writeStartElement("Trans")
								writer.writeAttribute("audio_filename", parser.getAttributeValue(null, "path"))
								break
							
							case "Proc": // <Proc name="scribe" version="date" editor="AAA"/>
							//continue writing the "Trans" element
								if ("scribe" == parser.getAttributeValue(null, "name")) {
									writer.writeAttribute("scribe", parser.getAttributeValue(null, "editor"))
									writer.writeAttribute("version", parser.getAttributeValue(null, "version"))
									writer.writeAttribute("version_date", parser.getAttributeValue(null, "version"))
								}
							
								break;
							
							case "SpeakerList": // <SpeakerList> -> <Speakers>
								writer.writeCharacters("\n") // after <Trans>
								writer.writeStartElement("Speakers")
								writer.writeCharacters("\n")
							
								break;
							case "Speaker": // <Speaker ch="1" dur="531.38" gender="X" spkid="Enquêtrice" lang="fre" lconf="1.00" nw="1586" tconf="0.95"/> -> <Speaker id="spk1" name="enq4" check="no" dialect="native" accent="" scope="local"/>
							
								String spkid = parser.getAttributeValue(null, "spkid").trim().replaceAll("[\\uFEFF]", "")
								
								writer.writeStartElement("Speaker")
								writer.writeAttribute("id", spkid)
								writer.writeAttribute("name", spkid)
								writer.writeAttribute("check", "")
								writer.writeAttribute("dialect", parser.getAttributeValue(null, "lang"))
								writer.writeAttribute("accent", parser.getAttributeValue(null, "gender"))
								writer.writeAttribute("scope", "local")
								writer.writeEndElement()
								writer.writeCharacters("\n")
								
								/**
								 * remove the additional speaker if already written
								 */
								if (additionalSpeakers.containsKey(spkid)) {
									additionalSpeakers.remove(spkid)
								}
								break;
								
							case "Channel":
								duration = parser.getAttributeValue(null, "sigdur")
								break;
							
							case "SegmentList":
								writer.writeStartElement("Episode")
							//<Section type="report" startTime="0" endTime="3617.593">
								
								writer.writeStartElement("Section")
								writer.writeAttribute("startTime", "0.0")
								writer.writeAttribute("endTime", duration)
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									writer.writeAttribute(parser.getAttributeLocalName(i), parser.getAttributeValue(i))
								}
								break;
							
							case "SpeechSegment": // <SpeechSegment ch="1" sconf="1.00" stime="9.94" etime="43.81" spkid="Enquêtrice" lang="fre" lconf="1.00" trs="1">
								writer.writeStartElement("Turn")
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									String name = parser.getAttributeLocalName(i)
									String value = parser.getAttributeValue(i)
									
									if (name == "stime") name = "startTime"
									else if (name == "etime") name = "endTime"
									else if (name == "spkid") {
										name = "speaker"
										value = value.trim().replaceAll("[\\uFEFF]", "")
									}
									writer.writeAttribute(name, value)
								}
							
								writer.writeCharacters("\n")
								writer.writeStartElement("Sync")
								writer.writeAttribute("time", parser.getAttributeValue(null, "stime"))
								writer.writeEndElement() // Sync
								writer.writeCharacters("\n")
								break;
							case "Word":
								flagWord = true
								word = ""
							
							// store w infos in case the word must be splited
								def endValue = String.format(Locale.US, "%.2f", (Double.parseDouble(parser.getAttributeValue(null, "stime")) + Double.parseDouble(parser.getAttributeValue(null, "dur"))))
								def startValue = parser.getAttributeValue(null, "stime");
								winfos.clear()
								for (int i = 0 ; i < parser.getAttributeCount() ; i++) {
									String name = parser.getAttributeLocalName(i)
									if (name == "stime") continue
									
									winfos[name] = parser.getAttributeValue(i)
								}
								winfos["time"] = startValue
								winfos["start"] = startValue
								winfos["end"] = endValue
							
							// write <w> at the end of <Word> tag
								break
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						String localname = parser.getLocalName()
						switch(localname) {
							case "AudioDoc": // <AudioDoc name="xyz" path="xyz.flac"> -> <Trans scribe="see Proc elements" audio_filename="xyz.flac" version="see Proc elements" version_date="see Proc elements">
							
								writer.writeEndElement() // Trans
								break
							
							case "Proc": // <Proc name="scribe" version="date" editor="AAA"/>
							
								break
							
							case "SpeakerList": // <SpeakerList> -> <Speakers>
							
								// write additional speakers
								for (String spkid : additionalSpeakers.keySet()) {
									writer.writeStartElement("Speaker")
									writer.writeAttribute("id", spkid)
									writer.writeAttribute("name", spkid)
									writer.writeAttribute("check", "")
									writer.writeAttribute("dialect", "")
									writer.writeAttribute("accent", spkid)
									writer.writeAttribute("scope", spkid)
									writer.writeEndElement()
									writer.writeCharacters("\n")
								}
							
								writer.writeEndElement()
								writer.writeCharacters("\n")
								break
							
							case "Speaker": // <Speaker ch="1" dur="531.38" gender="X" spkid="Enquêtrice" lang="fre" lconf="1.00" nw="1586" tconf="0.95"/> -> <Speaker id="spk1" name="enq4" check="no" dialect="native" accent="" scope="local"/>
							//already closed
								break
							
							case "SegmentList":
								writer.writeEndElement() // Section
								writer.writeEndElement() // Episode
								writer.writeCharacters("\n")
								break
							
							case "SpeechSegment": // <SpeechSegment ch="1" sconf="1.00" stime="9.94" etime="43.81" spkid="Enquêtrice" lang="fre" lconf="1.00" trs="1">
								writer.writeEndElement() // Turn
								writer.writeCharacters("\n")
								break
							
							case "Word":
								flagWord = false
								word = word.trim()
							
							// split before the word
								def puncts = []
															
								for (def punct : puncts) { // pre-retokenize if any
									writer.writeStartElement("w")
									for (String attr : winfos.keySet()) {
										writer.writeAttribute(attr, winfos[attr])
									}
									writer.writeCharacters(punct)
									writer.writeEndElement() // w
									writer.writeCharacters("\n")
								}
														
								if (word.length() > 0) {
									writer.writeStartElement("w") // start the initial word
									for (String attr : winfos.keySet()) {
										writer.writeAttribute(attr, winfos[attr])
									}
									writer.writeCharacters(word)
									writer.writeEndElement() // w
									writer.writeCharacters("\n")
								}

								break
						}
						break
					
					case XMLStreamConstants.CHARACTERS:
						if (flagWord) {
							word += parser.getText()
						}
						break
				}
			}
		} catch (Exception e) {
			println "Error while processing XML File "+xmlfile+": "
			e.printStackTrace();
			println "At: "+parser.getLocation();
			println "See: "+outfile.getAbsolutePath()
		}
		
		output.flush()
		writer.close()
		output.close()
		//writer.close()
		parser.close()
		//println "$xmlfile -> $outfile"
		return true;
	}
	
	public static void main(String[] args) {
		File infile = new File("/home/mdecorde/xml/vocapia/testé input","test.xml")
		File outfile = new File("/home/mdecorde/xml/vocapia/testé oh","testé.trs")
		outfile.getParentFile().mkdir()
		def processor = new Vocapia2Transcriber(infile)
		println processor.process(outfile)
	}
}
