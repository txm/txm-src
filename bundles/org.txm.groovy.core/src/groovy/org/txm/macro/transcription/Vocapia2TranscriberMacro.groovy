package org.txm.macro.transcription

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.txm.utils.*
import org.txm.utils.logger.*

@Field @Option(name="vocapiaFile", usage="A single vocapia XML file", widget="FileOpen", required=true, def="")
		File vocapiaFile;

@Field @Option(name="vocapiaDirectory", usage="A Vocapia XML files directory to process", widget="Folder", required=true, def="")
		File vocapiaDirectory;

@Field @Option(name="resultDirectory", usage="The result directory", widget="Folder", required=true, def="")
		File resultDirectory;
		
@Field @Option(name="additionalSpeakers", usage="The result directory", widget="String", required=true, def="")
		def additionalSpeakers;
		
@Field @Option(name="debug", usage="The result directory", widget="Boolean", required=false, def="false")
		boolean debug;

if (!ParametersDialog.open(this)) return;

resultDirectory.mkdirs();

def xmlFiles = []
if (vocapiaDirectory != null && vocapiaDirectory.exists()) {
	
	println "Processing directory: $vocapiaDirectory"
	for (File file : vocapiaDirectory.listFiles()) {
		if (file.getName().toLowerCase().endsWith(".xml")) {
			xmlFiles << file
		}
	}
} else if (vocapiaFile != null && vocapiaFile.exists()) {
	println "Processing file: $vocapiaFile"
	xmlFiles << vocapiaFile
}

if (xmlFiles.size() == 0) {
	println "No XML file found for parameters vocapiaFile=$vocapiaFile and vocapiaDirectory=$vocapiaDirectory"
	return false
}

ConsoleProgressBar cpb = new ConsoleProgressBar(xmlFiles.size())
for (File xmlFile : xmlFiles) {
	
	if (debug) println "== $xmlFile =="
	else cpb.tick()
	
		
	def map = [:]
	for (String spk : additionalSpeakers.split("\t")) {
		def split = spk.split(":", 2)
		map[split[0]] = split[1]
	}
	Vocapia2Transcriber v2t = new Vocapia2Transcriber(xmlFile)
	v2t.setAddtionalSpeakers(map)
	String name = FileUtils.stripExtension(xmlFile)
	File outFile = new File(resultDirectory, name+".trs")
	
	if (!v2t.process(outFile)) {
		println "WARNING: ERROR WHILE PROCESSING: "+xmlFile
		return false
	}
}
cpb.done()

println "Done: "+xmlFiles.size()+" files processed. Result files in $resultDirectory"
