// Copyright © 2022 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author bpincemin mdecorde

package org.txm.macro.transcription

import java.nio.charset.Charset

import java.time.LocalTime
import java.time.format.DateTimeFormatter

import org.eclipse.core.internal.localstore.IsSynchronizedVisitor
import org.txm.utils.*
import org.txm.utils.logger.*

@Field @Option(name="metadataFile", usage="Tableau des metadonnées de sections", widget="FileOpen", required=true, def="")
		File metadataFile;

@Field @Option(name="trsDirectory", usage="Dossier qui contient les fichiers TRS", widget="Folder", required=true, def="")
		File trsDirectory;

@Field @Option(name="joinTRSColumn", usage="Colonne de jointure de transcription", widget="String", required=true, def="Lien notice principale")
		def joinTRSColumn

@Field @Option(name="startTimeColumn", usage="Colonne de timing de début de section", widget="String", required=true, def="antract_debut")
		def startTimeColumn = "antract_debut"

@Field @Option(name="endTimeColumn", usage="Colonne de timing de fin de section", widget="String", required=true, def="antract_fin")
		def endTimeColumn = "antract_fin"

@Field @Option(name="typeColumns", usage="Colonnes des métadonnées de type de section", widget="String", required=true, def="Titre propre")
		def typeColumns

@Field @Option(name="topicColumns", usage="Colonnes des métadonnées de topic de section", widget="String", required=true, def="Date de diffusion")
		def topicColumns

@Field @Option(name="metadataColumns", usage="Colonnes de metadonnées de section", widget="String", required=true, def="Titre propre;Date de diffusion;Identifiant de la notice;Notes du titre;Type de date;Durée;Genre;Langue VO / VE;Nature de production;Producteurs (Aff.);Thématique;Nom fichier segmenté (info);antract_video;antract_debut;antract_fin;antract_duree;antract_tc_type;antract_tc_date;Résumé;Séquences;Descripteurs (Aff. Lig.);Générique (Aff. Lig.)")
		def metadataColumns

@Field @Option(name="metadataColumnsGroups", usage="Colonnes des gruopes de metadonnées de section", widget="String", required=true, def="metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;metadata;secondary;secondary;secondary;secondary;secondary;secondary;secondary;text;text;text;text")
		def metadataColumnsGroups

@Field @Option(name="fixSectionsLimits", usage="Correction des limites de sections du tableau de metadonnees", widget="Boolean", required=true, def="true")
		def fixSectionsLimits

@Field @Option(name="sectionsMergeActivationThreashold", usage="marge d'erreur de corrections des limites de sections", widget="Float", required=true, def="1.0")
		def sectionsMergeActivationThreashold

@Field @Option(name="fixTurnsLimits", usage="Découpage des tours à cheval sur plusieurs sujets", widget="Boolean", required=true, def="true") // description màj
		def fixTurnsLimits

@Field @Option(name="turnsCutActivationThreashold", usage="marge d'erreur de corrections des limites de tours", widget="Float", required=true, def="0.1")
		def turnsCutActivationThreashold

@Field @Option(name="debug", usage="show debug messages", widget="Boolean", required=true, def="false")
		def debug

if (!ParametersDialog.open(this)) return;

typeColumns = typeColumns.split(";")
topicColumns = topicColumns.split(";")
metadataColumns = metadataColumns.split(";")
metadataColumnsGroups = metadataColumnsGroups.split(";")

if (metadataColumns.size() != metadataColumnsGroups.size()) {
	println "ERROR in metadata declarations&groups:"
	println "COLUMNS: "+metadataColumns
	println "GROUPS : "+metadataColumnsGroups
	return
}

if (!trsDirectory.exists()) {
	println "$trsDirectory not found"
	return
}

println "Loading data from $metadataFile..."
TableReader reader = new TableReader(metadataFile)//, "\t".charAt(0), Charset.forName("UTF-8")
reader.readHeaders()
def header = reader.getHeaders()
if (!header.contains(joinTRSColumn)) {
	println "No TRS ID $joinTRSColumn column found"
	return
}
if (!header.contains(startTimeColumn)) {
	println "No start time $startTimeColumn column found"
	return
}
if (!header.contains(endTimeColumn)) {
	println "No end time $endTimeColumn column found"
	return
}
for (def col : metadataColumns) {
	if (!header.contains(endTimeColumn)) {
		println "No $col column found"
		return
	}
}
for (def col : typeColumns) {
	if (!header.contains(endTimeColumn)) {
		println "No type $col column found"
		return
	}
}
for (def col : topicColumns) {
	if (!header.contains(endTimeColumn)) {
		println "No topic $col column found"
		return
	}
}

File outputDirectory = new File(trsDirectory, "out")
println "Writing result to $outputDirectory..."

dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_TIME
def strTotime(def str) {
	if (str.lastIndexOf(":") == -1) {
		return null
	}
	
	bonusFrame = Integer.parseInt(str.substring(str.lastIndexOf(":")+1))
	//if (str.contains("135475")) println "ERROR $str in $infos -> $bonusFrame"
	if (bonusFrame > 25) {
		bonusFrame=0;
	}
	totalFrame = str.substring(0, str.lastIndexOf(":"))
	
	LocalTime time1 = LocalTime.parse(totalFrame, dateTimeFormatter)
	totalFrame = (time1.getHour()*60*60) + (time1.getMinute()*60) + time1.getSecond()
	
	def ret = totalFrame + (bonusFrame/25)
	return ret
}

try {
	debugMore=false
	def sectionGroupsToInsert = [:]
	println "Reading data..."
	//BP boucle de préparation qui constitue les listes de sections, avec sectionGroupsToInsert[identifiant-émission][section][start,end,contenu]
	//BP l'indice de section commence à zéro dans chaque émission
	while (reader.readRecord()) { // loading & sorting sections
		String id = reader.get(joinTRSColumn).trim()
		if (id.endsWith(".mp4")) id = id.substring(0, id.length()-4)
		if (id.length() == 0) continue;
		
		if (!sectionGroupsToInsert.containsKey(id)) { //BP initialisation du contenu pour chaque émission
			sectionGroupsToInsert[id] = []
		}
//		def section = sectionGroupsToInsert[id] // plus homogène si sections  ; utilisée une seule fois (30 lignes après)
		
		if (reader.get(startTimeColumn) != null && reader.get(startTimeColumn).length() > 0) { // ignore non timed sections
			
			def m = [:]
			
			for (def todo : ["topic":topicColumns, "type":typeColumns]) {
				def data = []
				for (def col : todo.value) {
					if (reader.get(col).trim().length() > 0) {
						data << reader.get(col).trim().replace("\n", "")
					}
				}
				m[todo.key] = data.join("\t")
			}
			def metadataList = []
			def metadataGroupList = []
			for (int i = 0 ;  i < metadataColumns.size() ; i++) {
				def col = metadataColumns[i]
				String c = AsciiUtils.buildAttributeId(col)
				m[c] = reader.get(col)
				metadataList << c
				metadataGroupList << metadataColumnsGroups[i]
			}
			m["metadata"] = metadataList.join("|")
			m["metadata_groups"] = metadataGroupList.join("|")
			
			m["startTime"] = strTotime(reader.get(startTimeColumn))
			m["endTime"] = strTotime(reader.get(endTimeColumn))
			m["synchronized"] = "true"
			
//			section << [m["startTime"], m["endTime"], m]
			sectionGroupsToInsert[id] << [m["startTime"], m["endTime"], m]
		}
	}
	
	println "Inserting sections... "+sectionGroupsToInsert.size()
	
	ConsoleProgressBar cpb = new ConsoleProgressBar(sectionGroupsToInsert.keySet().size())
	//BP boucle générale sur les émissions, avec id=identifiant de l'émission
	for (String id : sectionGroupsToInsert.keySet()) {
		
		//BP on crée le fichier .trs
		File trsFile = new File(trsDirectory, id+".trs")
		if (!trsFile.exists()) {
			cpb.tick()
			continue
		}
		
		if (debug) println "== $id =="
		else cpb.tick()
		
		//println "Processing $id..."
		
		//BP liste des sections de l'émission : sections[indice][start,end,contenu]
		sections = sectionGroupsToInsert[id]
		
		//BP on résoud les faibles chevauchements
		if (fixSectionsLimits) {
			if (debug && debugMore) println "Fixing sections of $id"
			//BP on unifie les start entre eux
			sections = sections.sort() { a, b -> a[0] <=> b[0] }
			for (int iSection = 1 ; iSection < sections.size() ; iSection++) {
				//println sections[iSection]
				if ((sections[iSection][0] - sections[iSection - 1][0] < sectionsMergeActivationThreashold) && (sections[iSection][0] != sections[iSection - 1][0])) {
					if (debug && debugMore) println "s=$iSection start <- start : "+sections[iSection][0]+ " <- "+sections[iSection - 1][0]
					sections[iSection][0] = sections[iSection - 1][0] // fix the start time with a close preceeding start time
				}
			}
			//BP on unifie les end entre eux
			sections = sections.sort() { a, b -> a[1] <=> b[1] }
			for (int iSection = 1 ; iSection < sections.size() ; iSection++) {
				//println sections[iSection]
				if ((sections[iSection][1] - sections[iSection - 1][1] < sectionsMergeActivationThreashold) && (sections[iSection][1] != sections[iSection - 1][1])) {
					if (debug && debugMore) println "s=$iSection end <- end : "+sections[iSection][1]+ " <- "+sections[iSection - 1][1]
					sections[iSection][1] = sections[iSection - 1][1] // fix the end time with a close preceeding end time
				}
			}
			//BP on unifie les start avec les end
//			sections = sections.sort() { a, b -> a[0] <=> b[0] }
			for (int iSection = 1 ; iSection < sections.size() ; iSection++) { // pour chaque start...
				//println sections[iSection]
				int j = 1
				boolean continuer = true
				while (continuer && (j < (iSection + 1))) { // ...on regarde les end avant son propre end
					if ((Math.abs(sections[iSection][0] - sections[iSection - j][1]) < sectionsMergeActivationThreashold) && (sections[iSection][0] != sections[iSection - j][1])) { // on a trouvé des valeurs à unifier
						if (sections[iSection][0] < sections[iSection - j][1]) { // on cherche lequel est avant pour unifier vers sa valeur
							if (debug && debugMore) println "s=$iSection end <- start : "+sections[iSection - j][1]+ " <- "+sections[iSection][0]
							sections[iSection - j][1] = sections[iSection][0]
						} else {
							if (debug && debugMore) println "s=$iSection start <- end : "+sections[iSection][0]+ " <- "+sections[iSection - j][1]
							sections[iSection][0] = sections[iSection - j][1]
						}
						continuer = false
					} else {
						if (sections[iSection][0] < sections[iSection - j][1]) { // l'écart ne peut plus que grandir
							continuer = false
						} else { // le end est au-dessus, le prochain sera plus bas, il peut se rapprocher
							j++
						}
					}
				}
			}	
		}

		//BP les sections sont triées
		sections = sections.sort() { a, b -> a[0] <=> b[0] ?: a[1] <=> -b[1] } // negative second test for sections inclusion
		
		// Open input file
		slurper = new groovy.xml.XmlParser(false, true, true);
		slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false) // allow DTD declaration
		slurper.setProperty("http://javax.xml.XMLConstants/property/accessExternalDTD", "all"); // allow to read DTD from local file
		trs = slurper.parse(trsFile.toURI().toString())
		trsEpisodes = trs.Episode // 1
		if (trsEpisodes.size() > 1) {
			println "Error: multiple Episode node in $trsFile"
			continue
		}
		def trsEpisode = trsEpisodes[0]
		def trsSections =  trs.Episode.Section // 1
		if (trsSections.size() > 1) {
			println "Error: multiple Section node in $trsFile"
			continue
		}
		
		trsSection = trsSections[0]
		
		turns = trsSection.Turn
		iSection = 0;
		currentSection = null
		currentNode = null
		
		def startSection = 0;
		def endSection = 0;
		def endPreviousSection = 0;
		
//		foundSection=null
//		isTurnSynchronized=false
//		cutCheck=false
//		=> ces initialisations ont été déplacées dans la boucle des tours (la 1ère y était déjà)
		
		// boucle sur les tours dans l'ordre
//		for (iTurn = 0 ; iTurn < turns.size() ; iTurn++) {
		for (iTurn = 0 ; iTurn < turns.size() ; iTurn++) {
			if (debug && debugMore) println "iTurn=$iTurn turn="+turns[iTurn].attributes()+" iSection=$iSection ("+sections[iSection][0]+", "+sections[iSection][1]+")"
			//if (debug && iTurn == 199) println "WARNING: boucle probable sur iTurn dans cette transcription (ou bien plus de 200 tours dans la transcription)"
			turn = turns[iTurn]
			start = Float.parseFloat(turn.@startTime) // Float sera utile pour les tests ?
			end = Float.parseFloat(turn.@endTime)
			//println "Turn: $iTurn ($start, $end)"

			// Etape 1 : dans quelle section est le (début du) tour et doit-il être coupé

			foundSection = null;
			isTurnSynchronized=false
			cutCheck=false

			for (int i = iSection ; i < sections.size() ; i++) {
				// if section_end < turn_start OU (|turn_start - section_end| < turn_threshold ET (turn_end - section_end) >= turn_threshold)
				if ((sections[i][1] < start) || ((Math.abs(start - sections[i][1]) < turnsCutActivationThreashold) && (end - sections[i][1] >= turnsCutActivationThreashold))) { // Section is before Turn
				// Cas 1 : la section est complètement avant (modulo la marge)
				} else {
				// Cas 2 : on est arrivés à la section à considérer
					iSection = i
					// if turn_end < section_start OU (|section_start - turn_end| < turn_threshold ET (section_start - turn_start) >= turn_threshold)
					if ((end < sections[i][0]) || ((Math.abs(sections[i][0] - end) < turnsCutActivationThreashold) && (sections[i][0] - start >= turnsCutActivationThreashold))) { // Section is after Turn
					// Cas 2.1 : la section est complètement après (modulo la marge) (et les suivantes le seront aussi)
						if (debug && debugMore) println "Etape 1, Cas 2.1"
						foundSection = sections[i] // (c'est la prochaine section, qui mettra fin à la section non-synchronisée)
						isTurnSynchronized = false
						cutCheck = false
					} else {
						// if (section_start - turn_start) >= turn_threshold
						if (sections[i][0] - start >= turnsCutActivationThreashold) { // Turn begins before section does
						// Cas 2.2 : la section commence significativement après le début du tour (le début est non synchronisé)
							if (debug && debugMore) println "Etape 1, Cas 2.2"
							foundSection = sections[i] // (c'est la première section rencontrée, mais elle sera pour le tour suivant, elle va servir à savoir où couper)
							isTurnSynchronized = false
							cutCheck = true
						} else {
							// if turn_end < section_end OU |section_end - turn_end| < turn_threshold
							if (end < sections[i][1] || Math.abs(sections[i][1] - end) < turnsCutActivationThreashold) {
							// Cas 2.3 : le début du tour est dans la section et la fin aussi.
								if (debug && debugMore) println "Etape 1, Cas 2.3"
								foundSection = sections[i]  // (c'est la section qui contient le tour)
								isTurnSynchronized = true
								cutCheck = false
							} else {
							// Cas 2.4 : le début du tour est dans la section mais il dépasse.
								if (debug && debugMore) println "Etape 1, Cas 2.4"
								foundSection = sections[i]  // (c'est la section qui contient le début du tour)
								isTurnSynchronized = true
								cutCheck = true							
							}
						}
					}
					break; // stop searching and set iSection to accelerate next search
				}
			}

			
			// Etape 2 : on met à jour currentSection et currentNode,
			// ainsi que des variables startSection, endSection, endPreviousSection
			// (on ne gère qu'un seul tour et un seul noeud à chaque itération de la boucle tour,
			// puisqu'on retaille le tour pour qu'il ne concerne pas plusieurs noeuds)
			// currentSection == null au début et quand c'est une section non synchronisée
			// currentNode == null au début
			
			if (isTurnSynchronized) { // on sait que foundSection != null
				if (debug && debugMore) println "iSection=$iSection ("+sections[iSection][0]+", "+sections[iSection][1]+") will be used"
				if (foundSection != currentSection) { // le tour ouvre une nouvelle section (au lieu de compléter une section existante)
					if (currentSection != null) {
						endPreviousSection = endSection // faut-il préciser float ?
					}
					startSection = sections[iSection][0] // currentNode.@startTime
					if (startSection instanceof String) startSection = Float.parseFloat(sections[iSection][0]) //currentNode.@startTime)
					endSection = sections[iSection][1] // currentNode.@endTime
					if (endSection instanceof String) endSection = Float.parseFloat(sections[iSection][1]) //currentNode.@endTime)

					currentSection = foundSection
					currentNode = new Node(trsEpisode, "Section", currentSection[2])
					if (debug && debugMore) println " create synchronized turn at start="+foundSection[0]
				} 
			} else {
				if (debug && debugMore) println "not synchronized with current iSection=$iSection ("+sections[iSection][0]+", "+sections[iSection][1]+")"
				if (currentNode == null) { // il n'y a pas de section avant, et il faut créer un noeud puisqu'il n'y en a pas
					if (foundSection == null) { // il n'y a pas de section après
						currentNode = new Node(trsEpisode, "Section", ["type":"Sujet non synchronisé", "startTime":trsSection.@startTime, "endTime":trsSection.@endTime, "synchronized":"false"] )
						//BP pas besoin de Float ici ? Dans le code originel on avait un ex. de "startTime":turn.@startTime
					} else {
//						if (currentSection != null) { // a priori ce cas n'est jamais réalisé : quand currentNode == null, currentSection aussi
//							endPreviousSection = endSection // faut-il préciser float ?
//						}
						startSection = sections[iSection][0] // currentNode.@startTime
						if (startSection instanceof String) startSection = Float.parseFloat(sections[iSection][0]) //currentNode.@startTime)
						endSection = sections[iSection][1] // currentNode.@endTime
						if (endSection instanceof String) endSection = Float.parseFloat(sections[iSection][1]) //currentNode.@endTime)

						currentNode = new Node(trsEpisode, "Section", ["type":"Sujet non synchronisé", "startTime":trsSection.@startTime, "endTime":startSection, "synchronized":"false"] )					
					}	
//					currentSection = null;
					if (debug && debugMore) println " create un-synchronized turn at "+trsSection.@startTime
                } else {
                	if (currentSection != null) { // il y a un noeud et il correspond à une section synchronisée, donc il faut créer un noeud non-synchronisé
                		if (foundSection == null) { // il n'y a pas de section après
                			currentNode = new Node(trsEpisode, "Section", ["type":"Sujet non synchronisé", "startTime":endSection, "endTime":trsSection.@endTime, "synchronized":"false"] )					
                		} else {
                			endPreviousSection = endSection // faut-il préciser float ?
                			startSection = sections[iSection][0] // currentNode.@startTime
                			if (startSection instanceof String) startSection = Float.parseFloat(sections[iSection][0]) //currentNode.@startTime)
							endSection = sections[iSection][1] // currentNode.@endTime
							if (endSection instanceof String) endSection = Float.parseFloat(sections[iSection][1]) //currentNode.@endTime)
							
							currentNode = new Node(trsEpisode, "Section", ["type":"Sujet non synchronisé", "startTime":endPreviousSection, "endTime":startSection, "synchronized":"false"] )					
                		}
                		currentSection = null;                			
						if (debug && debugMore) println " create un-synchronized turn at $endPreviousSection"
                	}
                }
			}
			
			// Etape 3 : on coupe le tour s'il y a besoin
			if (cutCheck && fixTurnsLimits) {
				if (debug && debugMore) println " fixing current turn iTurn=$iTurn ($start, $end) )in turns ("+turns.size()+") section ("+trsSection.children().size()+")"
				
				def children = turn.children()
				Node newTurnKaNode = null;//new Node(trsEpisode, "Turn", currentSection[2])
	
				//println "Cut the last turn if necessary"
		
				if (debug && debugMore) println " cut turn and test with end ? $isTurnSynchronized of iSection=$iSection ($startSection, $endSection) at iTurn=$iTurn (${turn.@startTime}, ${turn.@endTime}) children="+turn.children().size()
				for (int iChildren = 0 ; iChildren < children.size() ; iChildren++) {
				//BP boucle gérée comme un genre de pile. iChildren vaut 0 ou est incrémenté à 0
				//BP (modulo des string qui vont rester dans le tour initial, à la queue leu leu pour la partie coupée :
				//BP je propose de déplacer la ligne pour que les chaînes suivent le reste,
				//BP sous réserves que remove et append peuvent s'appliquer aux enfants chaîne.
		
					def c = children[iChildren]
//					if (c instanceof String) continue; // a Turn contains Sync or w tags
		
					if (newTurnKaNode != null) { // append the remaining children to the new turn
						turn.remove(c)
						newTurnKaNode.append(c)
						if (debug && debugMore) c.@moved="yes"
						iChildren--
					} else {
						if (c instanceof String) continue; // a Turn contains Sync or w tags
						if ("w".equals(c.name())) {
							def start2 = Float.parseFloat(c.@startTime)
							def end2 = Float.parseFloat(c.@endTime)
							
							boolean test = null
							if (isTurnSynchronized) {
								test = start2 > endSection
							} else {
								test = start2 >= startSection
							}
				
							if (test) { // && Math.abs(start2 - endSection) > turnsCutActivationThreashold // no more needed
								if (debug && debugMore) println " cut with a w at ($start2, $end2) for section ("+startSection+", "+endSection+")"
								newTurnKaNode = new Node(trsSection, "Turn", ["startTime":""+start2, "endTime":""+turn.@endTime, "speaker":turn.@speaker])
								new Node(newTurnKaNode, "Sync", ["time":""+start2]) // TRS
								turns.add(iTurn+1, newTurnKaNode) // set as next turn to process
//								iTurn-- //BP doute sur pertinence -> test sans
								if (debug && debugMore) newTurnKaNode.@created = "yes"
								turn.@endTime = ""+start2;
								turn.remove(c)
								newTurnKaNode.append(c)
								if (debug && debugMore) c.@moved="yes"
								iChildren--
							}
						} else if ("Sync".equals(c.name())) {
							def start2 = c.@time
							if (start2 instanceof String) start2 = Float.parseFloat(c.@time)
							def end2 = start2
							
							boolean test = null
							if (isTurnSynchronized) {
								test = start2 > endSection
							} else {
								test = start2 >= startSection
							}
				
							if (test) { //  && Math.abs(start2 - endSection) > turnsCutActivationThreashold
								if (debug && debugMore) println " cut with a Sync at ($start2, $end2) for section ("+startSection+", "+endSection+")"
								newTurnKaNode = new Node(trsSection, "Turn", ["startTime":""+start2, "endTime":""+turn.@endTime, "speaker":turn.@speaker])
								turns.add(iTurn+1, newTurnKaNode)
//								iTurn-- //BP doute sur pertinence -> test sans
								if (debug && debugMore) newTurnKaNode.@created = "yes"
								turn.@endTime = ""+start2;
								turn.remove(c)
								newTurnKaNode.append(c)
								if (debug && debugMore) c.@moved="yes"
								iChildren--
							}
						} else {
				// no time to check
						}
					}
				}
			} // fin étape 3
			
			if (debug && debugMore) println " remove turn in initial section ("+trsSection.children().size()+" remaining turns before removing this one)"
			//turns.remove(turn)
			trsSection.remove(turn)
			currentNode.append(turn)
			//if (debug && debugMore) println " removed turn in turns ("+turns.size()+") section ("+trsSection.children().size()+")"
		}
		
		trsEpisode.remove(trsSection) //BP on pourrait éventuellement vérifier que trsSection est bien vide.
		
		outputDirectory.mkdir()
		File outfile = new File(outputDirectory, trsFile.getName())
		outfile.withWriter("UTF-8") { writer ->
			writer.write('<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE Trans SYSTEM "trans-14.dtd">\n')
			def printer = new groovy.xml.XmlNodePrinter(new PrintWriter(writer))
			printer.setPreserveWhitespace(true)
			printer.print(trs)
		}
	}
	cpb.done()
	reader.close()
	println "Done."
	
} catch(Exception e) {
	println "Error: "+e
	Log.printStackTrace(e)
}

