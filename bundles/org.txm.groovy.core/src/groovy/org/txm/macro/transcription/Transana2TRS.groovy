// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.transcription

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.xml.DomUtils;
import org.w3c.tidy.Tidy
import org.txm.doc.*;

import javax.xml.stream.*;

import java.util.HashMap;
import java.util.List;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;
import org.txm.macro.transcription.FixTransanaTimings;
import org.txm.macro.transcription.HTML2TRS;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import java.net.URL;

import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.txm.libs.jodconverter.ConvertDocument

class Transana2TRS {
	
	boolean debug = false;
	boolean isDirectory = false;
	File outDir;
	File dtd;
	def monitor

	Transana2TRS(File outDir, File dtd, boolean debug, def monitor) {
		this.dtd = dtd;
		this.outDir = outDir
		this.monitor = monitor
		this.debug = debug
	}

	boolean process(File file) {
		try {
			isDirectory = file.isDirectory();
			if (isDirectory) {
				return processDir(file)
			} else {
				return processFile(file, 100)
			}
		} catch(Exception e) {
			println "Error while processing: "+e
			if (debug) e.printStackTrace()
		}
	}

	boolean processDir(File dir) {
		boolean ret = true
		try {
			println "* Processing $dir directory"
			def files = dir.listFiles()
			def okfiles = [];
			if (files != null)
				for (File file : files) {
					String fname = file.getName().toLowerCase()
					if ( file.isFile() &&
					(fname.endsWith(".odt") || fname.endsWith(".doc") || fname.endsWith(".rtf"))) {
						okfiles << file
					}

				}

			if (okfiles.size() == 0) {
				println "No ODT/DOC/RTF file found in $dir"
				return false
			}

			int delta = 100 / okfiles.size()
			for (File file : okfiles) {
				monitor.worked(0, "Processing file: $file")
				if (!processFile(file, delta)) {
					ret = false;
					println "ERROR: Failed to process $file"
				}

				if (monitor.isCanceled()) {
					break; // stop looping
				}
			}
		} catch(Exception e) {
			println "Error while processing directory: "+e;
			if (debug) e.printStackTrace();
		}
		return ret;
	}

	boolean processFile(File docFile, int work) {
		if (docFile == null) return false;
		int delta = work / 5
		println "** Processing $docFile file"
		String filename = docFile.getName()
		int idx = filename.lastIndexOf(".")
		if (idx < 0) return false;
		String ext = filename.substring(idx)
		filename = filename.substring(0, idx)

		//File docFile = new File(srcDir, "${filename}$ext")
		File htmlFile = new File(outDir, "${filename}.html")
		File htmlFile2 = new File(outDir, "${filename}_temp.html")
		File xhtmlFile = new File(outDir, "${filename}.xhtml")
		File xmlFile = new File(outDir, "${filename}.xml")
		File trsFile = new File(outDir, "${filename}-tmp.trs")
		File finalTrsFile = new File(outDir, "${filename}.trs")

		monitor.worked(0, "DOCtoHTML: "+docFile.getName())

		boolean ret = DOCtoHTML(docFile, htmlFile)
		monitor.worked(delta, "HTMLtoHTMLforTidy: "+docFile.getName())
		if (monitor.isCanceled()) {
			println "Process interrupted by user"
			return false;
		}
		if (!ret) return ret;

		ret = ret && HTMLtoHTMLforTidy(htmlFile, htmlFile2)
		monitor.worked(delta, "HTMLtoXHTML: "+docFile.getName())
		if (monitor.isCanceled()) {
			println "Process interrupted by user"
			return false;
		}
		if (!ret) return ret;

		ret = ret && HTMLtoXHTML(htmlFile2, xhtmlFile)
		monitor.worked(delta, "XHTMLtoTRS: "+docFile.getName())
		if (monitor.isCanceled()) {
			println "Process interrupted by user"
			return false;
		}
		if (!ret) return ret;

		ret = ret && XHTMLtoTRS(xhtmlFile, trsFile)
		monitor.worked(delta, "TRStoFixedTRS: "+docFile.getName())
		if (monitor.isCanceled()) {
			println "Process interrupted by user"
			return false;
		}
		if (!ret) return ret;

		ret = ret && TRStoFixedTRS(trsFile, finalTrsFile);
		monitor.worked(delta, "Done: "+docFile.getName())
		if (monitor.isCanceled()) {
			println "Process interrupted by user"
			return false;
		}
		if (!ret) return ret;

		// cleaning
		System.gc()
		if (!debug) {
			htmlFile.delete();htmlFile2.delete();xhtmlFile.delete();xmlFile.delete();trsFile.delete()
		}

		return ret
	}

	boolean DOCtoHTML(File docFile, File htmlFile) {
		println "*** ODT -> HTML"
		try {
			ConvertDocument.convert(docFile, htmlFile)
		} catch(Exception e) {
			println "Error while converting $docFile : $e"
			if (debug) e.printStackTrace()
		} finally {
		}
		return htmlFile.exists() && htmlFile.length() > 0
	}

	boolean HTMLtoHTMLforTidy(File htmlFile, File htmlFile2) {
		println "*** HTML -> HTML for tidy"
		try {
			println "replace TABS with 4 spaces"
			String text2 = htmlFile.getText("UTF-8")
			text2 = text2.replaceAll("&nbsp;", " ")
			text2 = text2.replaceAll("’", "'")
			text2 = text2.replaceAll("&rsquo;", "'")
			text2 = text2.replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;")
			text2 = text2.replaceAll("\n", '&nbsp;')
			text2 = text2.replaceAll("\r\n", '&nbsp;')
			htmlFile2.withWriter("UTF-8") { writer ->
				writer.write(text2);
			}
		} catch(Exception e) {
			println "Error while preparing HTML of $htmlFile : $e"
			if (debug) e.printStackTrace()
		}
		return htmlFile2.exists() && htmlFile2.length() > 0
	}

	boolean HTMLtoXHTML(File htmlFile2, File xhtmlFile) {
		println "*** HTML for tidy -> XHTML"
		try {
			Tidy tidy = new Tidy(); // obtain a new Tidy instance
			tidy.setXHTML(true); // set desired config options using tidy setters
			tidy.setInputEncoding("UTF-8")
			tidy.setOutputEncoding("UTF-8")
			tidy.setShowErrors(100)
			tidy.setShowWarnings(debug)
			tidy.setTabsize(10)
			tidy.setWraplen(9999)
			tidy.setForceOutput(true) // Tidy won't stop if error are found
			xhtmlFile.withWriter("UTF-8") { out ->
				def input = new InputStreamReader(htmlFile2.toURI().toURL().newInputStream(), "UTF-8")
				tidy.parse(input, out); // run tidy, providing an input and output stream
			}
			if (xhtmlFile.exists()) {
				// JTidy produced a "0x0" char. removing them
				// fix separated < and / ???
				def c = Character.toChars(0)[0]
				String txttmp = xhtmlFile.getText("UTF-8");
				xhtmlFile.withWriter("UTF-8") { out ->
					out.write(txttmp.replace("<\n/", "</").replace("<\r\n/", "</"))
				}
			}
		} catch(Exception e) {
			println "Error while applying JTidy: "+e
			if (debug) e.printStackTrace()
		}
		return xhtmlFile.exists() && xhtmlFile.length() > 0
	}

	boolean XHTMLtoTRS(File xhtmlFile, File trsFile) {
		println "*** XHTML -> TRS"
		try {
			HTML2TRS ht = new HTML2TRS();
			ht.process(xhtmlFile, trsFile);
		} catch(Exception e) {
			println "Error while creating TRS file: "+e
			if (debug) e.printStackTrace()
		}
		return trsFile.exists() && trsFile.length() > 0
	}

	boolean TRStoFixedTRS(File trsFile, File finalTrsFile) {
		println "*** TRS -> FIXED TRS"
		try {
			def timingfixer = new FixTransanaTimings(trsFile, finalTrsFile);
			if (!timingfixer.process()) {
				println "Error while fixing timing"
			}
		} catch(Exception e) {
			println "Error while fixing TRS file: "+e
			if (debug) e.printStackTrace()
		}
		return finalTrsFile.exists() && finalTrsFile.length() > 0
	}
}

