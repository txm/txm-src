// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.transcription

import org.txm.utils.xml.DomUtils;
import org.w3c.tidy.Tidy
import org.txm.doc.*
import javax.xml.stream.*
import java.net.URL
import java.io.File
import java.net.URL
import java.util.HashMap
import java.util.List
import org.txm.scripts.importer.*
import org.xml.sax.Attributes
import org.txm.importer.scripts.filters.*
import java.io.File
import java.io.IOException
import java.util.ArrayList
import javax.xml.parsers.SAXParserFactory
import javax.xml.parsers.ParserConfigurationException
import javax.xml.parsers.SAXParser
import java.net.URL
import org.xml.sax.InputSource
import org.xml.sax.helpers.DefaultHandler

import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date;

class HTML2TRS {

	boolean inTurn = false
	def locs = new HashSet<String>()
	def lastTiming = "none"
	String localname = null
	String allTxt
	String header = ""
	String type = ""
	boolean startOfP = false
	boolean firstTurn = true
	boolean firstSync = true
	def loc = null
	int time = 0;
	boolean start= false;
	boolean startTranscription=false
	boolean isThemeOpened = false

	def inputData
	def factory
	def parser
	def formater
	def formater2
	StaxStackWriter pagedWriter
	public HTML2TRS() { }
	
	public process(File xhtmlFile, File trsFile) {
	
		println "remove entities and doctype"
		String text = xhtmlFile.getText("UTF-8")
		text = text.replaceAll("&nbsp;", " ")
		text = text.replaceAll("""<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">""", "")
		xhtmlFile.withWriter("UTF-8") { writer ->
			writer.write(text);
		}
		
		inputData = xhtmlFile.toURI().toURL().openStream();
		factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
		parser = factory.createXMLStreamReader(inputData);
		
		formater = new SimpleDateFormat("h:mm:ss.S");
		formater.setTimeZone(TimeZone.getTimeZone("GMT"));
		formater2 = new DecimalFormat("#######.0")
		formater2.setMaximumFractionDigits(2)
	
		println "start writing TRS file"
		pagedWriter = new StaxStackWriter(trsFile, "UTF-8");
		pagedWriter.writeStartDocument("UTF-8", "1.0")
		//pagedWriter.writeDTD("<!DOCTYPE Trans SYSTEM \"trans-14.dtd\">") // removed because parser has the "no DTD" option set
		pagedWriter.writeStartElement("Trans");
		pagedWriter.writeAttribute("scribe", "SCRIBE");
		pagedWriter.writeAttribute("audio_filename", "");
		pagedWriter.writeAttribute("version", "1.0");
		pagedWriter.writeAttribute("version_date", "110110");
		pagedWriter.writeStartElement("Speakers");
		pagedWriter.writeEndElement(); // Speakers

		for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
			switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					localname = parser.getLocalName()
					//println "start elem: "+localname
					switch (localname) {
						case "body": println "START OF BODY";start = true;
							break;
						case "p":
							loc = null
							allTxt = ""
							type = "";
							startOfP = true;
							break
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					localname = parser.getLocalName()
					switch (localname) {
						case "body": start = false;
							break
						case "p":
							//if (allTxt.length() > 0) allTxt = allTxt.substring(1)
							//println "PTEXT '$allTxt'"
							if (allTxt.indexOf("####") > 0) {
								//println "SAY $txt"
								if (allTxt.startsWith("\"")) allTxt = allTxt.substring(1)
								loc = allTxt.substring(0, allTxt.indexOf("####")).trim()
								locs.add(loc)
								allTxt = allTxt.substring(allTxt.indexOf("####")+4) // remove " [A-Z]"
								//println "LOC: '$loc'"
								//println "LOC: '$allTxt'"
							}
							String trimedTxt = allTxt.trim()
							if (startTranscription)
							if (loc) {
								inTurn = true;
								pagedWriter.writeStartElement("Turn");
								pagedWriter.writeAttribute("speaker", loc);
								pagedWriter.writeAttribute("startTime", "");
								pagedWriter.writeAttribute("endTime", "");
								
								// write Sync
								if (firstTurn) {
									writeTimeCode("(0:00:00.0)")
									firstTurn = false;
								} else {
									pagedWriter.writeEmptyElement("Sync")
									pagedWriter.writeAttribute("time", "");
								}
								
								if (!processCommentAndSync(allTxt)) {
									if (allTxt.trim().length() > 0)
										println "ERROR $lastTiming 'contains ([]) but malformed' error line "+parser.getLocation().getLineNumber()+" : $allTxt"
								}
								//pagedWriter.writeCharacters(allTxt.replaceAll("\\(.+:..:..\\..+\\)", ""))
								pagedWriter.writeEndElement(); // endTurn
								
								inTurn = false
								time++;
							} else { // Sync or Comment or Section : out of Turn 
								if (allTxt.length() > 0)
								if (trimedTxt.startsWith("(") && trimedTxt.endsWith(")")) {
									if (!processCommentAndSync(allTxt)) {
										if (allTxt.trim().length() > 0)
											println "ERROR $lastTiming 'contains ([]) but malformed' line "+parser.getLocation().getLineNumber()+" : $allTxt"
									}
								} else if (trimedTxt.startsWith("[") && trimedTxt.endsWith("]")) {
									println "SECTION: "+trimedTxt
									def section_regex = /([a-zA-Z]+)="([^"]+)"/
									def m = (trimedTxt =~ section_regex) // time
									if (m.size() > 0) {
										if (isThemeOpened)
											pagedWriter.writeEndElement(); // previous Section
										isThemeOpened = true;
										pagedWriter.writeStartElement("Section");
										pagedWriter.writeAttribute("type", "report");
										pagedWriter.writeAttribute("topic", "");
										pagedWriter.writeAttribute("startTime", "");
										pagedWriter.writeAttribute("endTime", "");
										for (def match : m) {
											if (match.size() == 3) {
												pagedWriter.writeAttribute(match[1], match[2]);
											} else {
												println "ERROR $lastTiming malformed section attribute "+trimedTxt
											}
										}
									} else {
										println "ERROR $lastTiming malformed section "+trimedTxt
									}
								} else if (allTxt.trim().length() > 0) {
									println "ERROR $lastTiming 'not in Turn and not a comment' line "+parser.getLocation().getLineNumber()+" : $allTxt"
								}
							}
		
							allTxt = ""
							break
					}
					break;
					
				case XMLStreamConstants.CHARACTERS:
					String txt = parser.getText().replaceAll("\n", " ")
					allTxt += txt.replaceAll("    ", "####")
					//println allTxt+"\n\n"
					if (!startTranscription && start && txt.matches("\\(.+:..:..\\..+\\)")) {
						println "START with $txt"
						startTranscription = true
						
						pagedWriter.writeStartElement("Episode");
						pagedWriter.writeAttribute("program", "NA");
						pagedWriter.writeAttribute("air_date", "NA");
						isThemeOpened = true
						pagedWriter.writeStartElement("Section");
						pagedWriter.writeAttribute("type", "report");
						pagedWriter.writeAttribute("topic", "");
						pagedWriter.writeAttribute("startTime", "0.0");
						pagedWriter.writeAttribute("endTime", "");
					}
					//println "$start $startTranscription "+txt.length()+" $startOfP"
					if (start && startTranscription && txt.length() > 0 && startOfP) {
						//println "first P: $txt $loc"
						startOfP = false
					}
					break;
				default: break;
			}
		}
		boolean ret = true
		if (startTranscription) {
			if (isThemeOpened) { // close previous theme
				pagedWriter.writeEndElement(); // Section
			}
			
			//pagedWriter.writeEndElement(); // Section
			pagedWriter.writeEndElement(); // Episode
			pagedWriter.writeEndElement(); // Trans

			ret = true
		} else {
			println "ERROR6: header not found"
			ret = false;
		}
		pagedWriter.close()
		if (parser != null) parser.close();
		if (inputData != null) inputData.close();
		return ret
	}

	def writeTimeCode(String txt) {
		//println "write time code $txt"
		String str = txt.substring(1, txt.length()-1)
		Date date = formater.parse(str);
		lastTiming = str;
		if (inTurn || firstSync) {
			pagedWriter.writeEmptyElement("Sync");
			pagedWriter.writeAttribute("time", ""+(date.getTime()/1000.0f));
		} else {
			pagedWriter.writeStartElement("Turn");
			pagedWriter.writeAttribute("speaker", "none");
			pagedWriter.writeAttribute("startTime", "");
			pagedWriter.writeAttribute("endTime", "");
			inTurn = true
			writeTimeCode(txt);
			inTurn = false	
			pagedWriter.writeEndElement(); // endTurn
			//println "ERROR: can't write Sync out of Turn $txt at "+parser.getLocation().getLineNumber()
		}
		firstSync = false
	}
	
	//                 /\([0-9]+:[0-9][0-9]?:[0-9][0-9]?(\.[0-9])?\)/
	def bullet_regex = /\([0-9]+:[0-9][0-9]?:[0-9][0-9]?(\.[0-9])?\)/
	def processCommentAndSync(String txt) {
		if (txt.length() == 0) return true;
		//println "process sync text: "+txt
		boolean ret = true;
		//find timing
		//find comments
		def allBySync = []
		def all = []
		def splits = []
		def lastMatch = 0
		
		// split bullet from txt
		def m = (txt =~ bullet_regex) // time
		while (m.find()) {
			//println "found: "+m.group()
	   		// grab the string in between the end of the last match
	   		// and the start of the current one (empty string if none)
	   		allBySync << txt.substring(lastMatch, m.start())
	   		// grab the delimiter
	   		allBySync << m.group()
	   		// keep looking from the end of the current match
	   		lastMatch = m.end()
		}
		all = allBySync
		// grab everything after the end of the last match
		all << txt.substring(lastMatch)
		//println "** $txt >> $all"
		for (String str : all) {
			if (str.trim().length() == 0) continue
			if (str.matches(bullet_regex)) {
				//println "TIMING: $str"
				writeTimeCode(str)
			} else {
				int idxOpen = str.indexOf("(");
				int idxClose = str.indexOf(")")
				
				while (idxOpen >=0 && idxClose > idxOpen) {
					if (str.substring(0, idxOpen).length() > 0) {
						if (loc != null) {
							pagedWriter.writeCharacters(str.substring(0, idxOpen))
							//println "SAY: "+str.substring(0, idxOpen)
						} else {
							if (str.trim().length() > 0)
								println "WARNING $lastTiming no locutor at line "+parser.getLocation().getLineNumber()+" : $str"
						}
					}
					pagedWriter.writeEmptyElement("Comment")
					pagedWriter.writeAttribute("desc", str.substring(idxOpen+1, idxClose))
					//println "COMMENT: "+str.substring(idxOpen+1, idxClose)
					str = str.substring(idxClose+1)
					idxOpen = str.indexOf("(");
					idxClose = str.indexOf(")")
				}
				if (str.length() > 0) {
					if (loc != null) {
						pagedWriter.writeCharacters(str)
					//	println "SAY: "+str
					} else {
						if (str.trim().length() > 0)
							println "ERROR $lastTiming 'not in turn and not a comment' line "+parser.getLocation().getLineNumber()+" : $str"
					}
				}
				if (str.contains("(") || str.contains("[") || str.contains(")") || str.contains("]")) {
					ret = false;
				}
		  }
		}
		return ret;
	}
}