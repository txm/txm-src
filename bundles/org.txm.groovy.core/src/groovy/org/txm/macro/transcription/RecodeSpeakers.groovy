package org.txm.macro.transcription

import org.txm.scripts.importer.*;
import org.txm.utils.*;
import org.txm.metadatas.*;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/** 
 * Renames speakers given an id or name regex
 **/
public class RecodeSpeakers {
	File outfile, transcriptionfile
	
	/** The doc. */
	Document doc
	
	def idRegex, nameRegex
	String newId, newName
	boolean debug
	
	/**
	 * Instantiates a new change speaker.
	 *
	 * @param transcriptionfile the transcriptionfile
	 * @param outfile the outfile
	 * @param id the id
	 * @param newid the newid
	 */
	public RecodeSpeakers(File transcriptionfile, File outfile, String idRegexString, String nameRegexString, String newId, String newName) {
		
		this.transcriptionfile = transcriptionfile
		this.outfile = outfile;
		
		if (idRegexString != null && idRegexString.length() > 0) {
			this.idRegex = /$idRegexString/
		}
		if (nameRegexString != null && nameRegexString.length() > 0) {
			this.nameRegex = /$nameRegexString/
		}
		this.newId = newId
		this.newName = newName
	}
	
	/**
	 * Save.
	 *
	 * @return true, if successful
	 */
	public boolean process() {
		try {
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true); // never forget this!
			domFactory.setXIncludeAware(true);
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			doc = builder.parse(transcriptionfile);
			
			def expr2 = XPathFactory.newInstance().newXPath().compile("//Speaker");
			def nodes2 = expr2.evaluate(doc, XPathConstants.NODESET);
			
			// fix speaker declarations
			def replacedIds = [] // list of IDs replaced, the list is used later to update the Turn locutors ids
			
			for (def node : nodes2) {
				if (node == null) continue;
				Element elem = (Element)node;
				
				String id = elem.getAttribute("id")
				String name = elem.getAttribute("name")
				
				if (idRegex != null && id =~ idRegex) { // patch Speaker@id
					if (newId != null) {
						elem.setAttribute("id", newId);
						replacedIds << id
					}
					if (newName != null) {
						elem.setAttribute("name", newName);
					}
					if (debug) println "Recoding id $id -> $newId and name $name -> newName"
				}
				
				if (nameRegex != null && id =~ nameRegex) { // patch Speaker@name
					if (newName != null) {
						elem.setAttribute("name", newName);
					}
				}
			}
			
			def expr = XPathFactory.newInstance().newXPath().compile("//Turn");
			def nodes = expr.evaluate(doc, XPathConstants.NODESET);
			
			// fix speaker turns
			
			int nReplace = 0
			for (def node : nodes) {
				if (node == null) continue;
				
				Element elem = (Element)node;
				String id = elem.getAttribute("speaker");
				
				if (replacedIds.contains(id)) {
					elem.setAttribute("speaker", newId);
					nReplace++
				}
			}
			
			if (debug) println "id=$idRegex or name=$nameRegex -> $nReplace replacements"
			if (nReplace == 0) println "Warning: didn't found any occurrence of id=$idRegex or name=$nameRegex"
			
			// Création de la source DOM
			Source source = new DOMSource(doc);
			
			// Création du fichier de sortie
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), "UTF-8"));
			Result resultat = new StreamResult(writer);
			
			// Configuration du transformer
			TransformerFactory fabrique = new net.sf.saxon.TransformerFactoryImpl();
			Transformer transformer = fabrique.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			
			// Transformation
			transformer.transform(source, resultat);
			writer.close();
			return true;
		} catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		String homedir = System.getProperty("user.home")
		File trs1 = new File(homedir, "xml/concattrs/int18_1.trs")
		File trs2 = new File(homedir, "xml/concattrs/int18_1-renamed.trs")
		
		new RecodeSpeakers(trs1, trs2, "spk2", null, "spk4", null).process();
	}
}
