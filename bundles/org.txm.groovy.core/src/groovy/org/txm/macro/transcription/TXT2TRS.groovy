// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
package org.txm.macro.transcription

import org.txm.utils.xml.DomUtils;
import org.txm.doc.*

import javax.xml.stream.*

import java.util.HashMap
import java.util.List

import org.txm.scripts.importer.*
import org.xml.sax.Attributes
import org.txm.importer.scripts.filters.*
import org.txm.utils.io.IOUtils;
import org.txm.utils.i18n.DetectBOM

import java.io.File
import java.io.IOException
import java.util.ArrayList

import javax.xml.parsers.SAXParserFactory
import javax.xml.parsers.ParserConfigurationException
import javax.xml.parsers.SAXParser

import java.net.URL

import org.xml.sax.InputSource
import org.xml.sax.helpers.DefaultHandler

import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date;

/**
 * Parse a formatted a TXT file to create a TRS file.
 * 
 * Manage turns, syncs, locutors, section.
 * 
 * The parsing of the TXT file starts at the first time code
 *  
 * @author mdecorde, sheiden
 *
 */
class TXT2TRS {

	boolean inTurn = false
	def locs = new HashSet<String>()
	def lastTiming = "none"
	String localname = null
	String allTxt
	String header = ""
	String type = ""
	boolean startOfP = false
	boolean firstTurn = true
	boolean firstSync = true
	def loc = null
	int time = 0;
	boolean start= false;
	boolean startTranscription=false
	boolean isThemeOpened = false

	def formater
	def formater_without_ms
	def formater2
	StaxStackWriter pagedWriter
	
	
	//                 /\([0-9]+:[0-9][0-9]?:[0-9][0-9]?(\.[0-9][0-9]?)?\)?|(¤<[0-9]+>)/
	def bullet_regex = /\([0-9]+:[0-9][0-9]?:[0-9][0-9]?(\.[0-9][0-9]?)?\)?|(¤<[0-9]+>)/
	def turn_with_bullet = /.+(\([0-9]+:[0-9][0-9]?:[0-9][0-9]?(\.[0-9])?\)?|(¤<[0-9]+>))/
	def section_regex = /([a-zA-Z]+)="([^"]+)"/
	
	public TXT2TRS() { }

	public boolean process(File txtFile, File trsFile) {
//
		FileInputStream input = new FileInputStream(txtFile);
		Reader reader = new InputStreamReader(input , "UTF-8");
		
		for (int i = 0 ; i < new DetectBOM(txtFile).getBOMSize() ; i++) input.read();
		def lines = reader.readLines();

		formater = new SimpleDateFormat("h:mm:ss.S");
		formater_without_ms = new SimpleDateFormat("h:mm:ss");
		formater.setTimeZone(TimeZone.getTimeZone("GMT"));
		formater_without_ms.setTimeZone(TimeZone.getTimeZone("GMT"));
		formater2 = new DecimalFormat("#######.0")
		formater2.setMaximumFractionDigits(2)

		//println "Start writing TRS file..."
		pagedWriter = new StaxStackWriter(trsFile, "UTF-8");
		pagedWriter.writeStartDocument("UTF-8", "1.0")
		//pagedWriter.writeDTD("<!DOCTYPE Trans SYSTEM \"trans-14.dtd\">") // removed because parser has the "no DTD" option set
		pagedWriter.writeStartElement("Trans");
		pagedWriter.writeAttribute("scribe", "SCRIBE");
		pagedWriter.writeAttribute("audio_filename", "");
		pagedWriter.writeAttribute("version", "1.0");
		pagedWriter.writeAttribute("version_date", "110110");
		pagedWriter.writeStartElement("Speakers");
		pagedWriter.writeEndElement(); // Speakers
		
		pagedWriter.writeStartElement("Episode");
		pagedWriter.writeAttribute("program", "NA");
		pagedWriter.writeAttribute("air_date", "NA");
		isThemeOpened = false
		def firstTurn = true

		// test if the transcription structure is : comment, start line, end line
		def trslines = []
		boolean started = false
		for (String line : lines) {
			line = line.trim()
			if (line.matches(bullet_regex)) {
				started = true;
			}

			if (started) { // get ttranscription lines
				if (line.length() > 0) trslines << line
			}
		}

		if (!started) {
			println "The $txtFile file does not contain the first time bullet"
			return false;
		}
		String lastLine = trslines[-1]
		if (!lastLine.matches(bullet_regex)) {
			// check if last turn has a bullet at the end
			boolean bulletIsMissing = true;
			for (int i = trslines.size() - 1 ; i > 0 ; i--) {
				String line = trslines[i].trim()
				def split = line.split("\t", 2)
				//println "$line "+split.length+ " VS "+turn_with_bullet
				if (split.length == 2) {
				 	if (line.matches(turn_with_bullet)) {
						bulletIsMissing = false;
					} 
					println "END $bulletIsMissing"
					i=-1
				}
			}
			if (bulletIsMissing) {
				println "Error: The $txtFile file last line or last turn does not contain the end time bullet: $bullet_regex"
				return false;
			}
		}

		lines = trslines

		def currentTime = lines[0] // get starting time

		// group lines per Turn, Event, Comment...
		def groups = [["sync", lines[0]]]
		lines.remove(0)
		def currentGroup = []

		for (String line : lines) {

			if (line.indexOf("\t") > 0) { // start of Turn
				def split = line.split("\t", 2)
				def loc = split[0].replace(" ", "_")
				def content = split[1]

				if (currentGroup.size() > 0) {groups << currentGroup} // store previous group
				currentGroup = ["turn", loc, content.trim()];

			} else if (line.matches("(\\([^)]+\\) *)+")) {//("(") && line.endsWith(")")) { // comment or timing
				if (line.matches(bullet_regex)) { // Sync in a Turn
					if (currentGroup.size() > 0) {
						currentGroup << line
					} else {
						if (currentGroup.size() > 0) {groups << currentGroup} // store previous group
						groups << ["sync", line.trim()]
					}
				} else { // comment -> ignored
					if (currentGroup.size() > 0) {groups << currentGroup} // store previous group
					currentGroup = ["comment", line.trim()];
				}
			} else if (line.startsWith("[") && line.endsWith("]")) { // section, end of Turn
				if (currentGroup.size() > 0) groups << currentGroup
				groups << ["section", line.trim()]
			} else { // something that happens during a Turn
				currentGroup << line.trim()
				
			}
		}

	println "PROCESS GROUPS..."
		for (def g : groups) {
			// println g
			if (g[0] == "turn") {
				String loc = g[1]
				//println "process turn: "+g
				def tokens = []
				for (int i = 2 ; i < g.size() ; i++) {// speach starts for position 2
					//println " process speach: "+g[i]
					// 	tokenize time bullets, comment and speach

					def turntokens = []
					def toProcess = [g[i]]
					def lastMatch = 0
					// find Sync
					while (toProcess.size() > 0) {
						String txt = toProcess.pop()
						boolean nomatch = true
						def m = (txt =~ bullet_regex) // time
						//if (m.size() > 0) {
						while (m.find()) {
							nomatch = false
							//println "found: "+m.group()
							// grab the string in between the end of the last match
							// and the start of the current one (empty string if none)
							String before = txt.substring(lastMatch, m.start())
							if (before.length() > 0) turntokens << before
							// grab the delimiter
							turntokens << m.group()
							// keep looking from the end of the current match
							lastMatch = m.end()
						}
						if (nomatch) turntokens << txt
						if (lastMatch > 0 && lastMatch < txt.length()) turntokens << txt.substring(lastMatch)
					}
					//println "  speach+syncs="+turntokens

					// find Comments (....)
					toProcess = turntokens
					turntokens = []
					
					while (toProcess.size() > 0) {
						String txt = toProcess.remove(0)
						
						boolean nomatch = true
						def m = (txt =~ /(\([^)]+\))/) // time
						//if (m.size() > 0) {
						lastMatch = 0
						while (m.find()) {
							nomatch = false
							//println "found: "+m.group()
							// grab the string in between the end of the last match
							// and the start of the current one (empty string if none)
							String before = txt.substring(lastMatch, m.start())
							if (before.length() > 0) turntokens << before
							// grab the delimiter
							turntokens << m.group()
							// keep looking from the end of the current match
							lastMatch = m.end()
						}
						if (nomatch) turntokens << txt
						if (lastMatch > 0 && lastMatch < txt.length()) turntokens << txt.substring(lastMatch)
					}
					//println "  speach+syncs+comments="+turntokens
					//					if (!processCommentAndSync(g[i])) {
					//						println "ERROR $lastTiming 'contains ([]) but malformed' error line "+parser.getLocation().getLineNumber()+" : $allTxt"
					//					}
					tokens.addAll(turntokens)
				}

				if (firstTurn && !isThemeOpened) {
					isThemeOpened = true;
					
					pagedWriter.writeStartElement("Section");
					pagedWriter.writeAttribute("type", "report");
					pagedWriter.writeAttribute("topic", "");
					pagedWriter.writeAttribute("startTime", "0.0");
					pagedWriter.writeAttribute("endTime", "");
				}
				firstTurn = false
				pagedWriter.writeCharacters("\n")
				pagedWriter.writeStartElement("Turn");
				pagedWriter.writeAttribute("speaker", loc);
				pagedWriter.writeAttribute("startTime", "");
				pagedWriter.writeAttribute("endTime", "");
				pagedWriter.writeCharacters("\n")
				
				if (tokens.size() == 0 || !tokens[0].matches(bullet_regex)) { // add the bullet only if the first token is not a bullet
					writeSync(currentTime)
				}

				String previousToken = ""
				//println "tokens=$tokens"
				for (def t : tokens) {
					if (t.matches(bullet_regex)) {
						writeSync(t)
						currentTime = t
					} else if (t.startsWith("(") && t.endsWith(")") ){
						pagedWriter.writeCharacters("\n")
						pagedWriter.writeEmptyElement("Comment")
						pagedWriter.writeAttribute("desc", t.substring(1, t.length() -1))
					} else {
						pagedWriter.writeCharacters(" "+t)
					}
				}

				pagedWriter.writeCharacters("\n")
				pagedWriter.writeEndElement(); // Turn
			} else if (g[0] == "sync") {
				currentTime = g[1]
				writeSync(currentTime)
			} else if (g[0] == "comment") {
//				println "process comment: "+g[1]
				pagedWriter.writeCharacters("\n")
				pagedWriter.writeEmptyElement("Comment")
				pagedWriter.writeAttribute("desc", g[1].substring(1, g[1].length() -1))
			} else if (g[0] == "section") {
//				println "process section: "+g[1]
				
				def m = (g[1] =~ section_regex) // time
				if (m.size() > 0) {
					if (isThemeOpened) {
						pagedWriter.writeCharacters("\n")
						pagedWriter.writeEndElement(); // previous Section
					}
					isThemeOpened = true;
					pagedWriter.writeCharacters("\n")
					pagedWriter.writeStartElement("Section");
					def attr = ["type":"report", "topic":"", "startTime":"", "endTime":""]
					
					for (def match : m) {
						if (match.size() == 3) {
							attr[match[1]]=match[2];
						} else {
							println "ERROR $lastTiming malformed section attribute "+g[1]+ "near $currentTime"
						}
					}
					
					// write attributes
					for (def k : attr.keySet())	pagedWriter.writeAttribute(k, attr[k]);
				} else {
					println "ERROR $lastTiming malformed section "+g[1]
				}
			} else {
				println "Error: found malformed line: "+g
			}
		}

		boolean ret = true
		if (isThemeOpened) { // close previous theme
			pagedWriter.writeCharacters("\n")
			pagedWriter.writeEndElement(); // Section
		}

		//pagedWriter.writeEndElement(); // Section
		pagedWriter.writeCharacters("\n")
		pagedWriter.writeEndElement(); // Episode
		pagedWriter.writeCharacters("\n")
		pagedWriter.writeEndElement(); // Trans

		ret = true

		pagedWriter.close()
		return ret
	}

	def writeSync(String txt) {
		//println "write time code $txt"
		pagedWriter.writeEmptyElement("Sync");
		if (txt.startsWith("¤")) { // ¤<5648>
			String time = txt.substring(2, txt.length()-1)
			pagedWriter.writeAttribute("time", time.substring(0,time.length()-3)+"."+time.substring(time.length()-3));
		} else {
			String str = txt.substring(1, txt.length()-1)
			Date date = null;
			try {date = formater.parse(str);} catch(Exception e1){}
			try {date = formater_without_ms.parse(str);} catch(Exception e2){println "Failed to parse time: "+str; return;}
			pagedWriter.writeAttribute("time", ""+(date.getTime()/1000.0f));
		}
		pagedWriter.writeCharacters("\n")
	}
	
	public static void main(String[] args) {}
}