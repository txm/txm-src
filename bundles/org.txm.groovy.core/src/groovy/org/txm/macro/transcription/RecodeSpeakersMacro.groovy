package org.txm.macro.transcription

import java.nio.charset.Charset

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.txm.utils.*
import org.txm.utils.logger.*

@Field @Option(name="trsFile", usage="Dossier qui contient les fichiers TRS", widget="FileOpen", required=true, def="")
File trsFile;

@Field @Option(name="outputTrsFile", usage="Dossier qui contient les fichiers TRS", widget="FileSave", required=true, def="")
File outputTrsFile;

@Field @Option(name="idRegex", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def idRegex

@Field @Option(name="nameRegex", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def nameRegex

@Field @Option(name="idRegex", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def newID

@Field @Option(name="nameRegex", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def newName

if (!ParametersDialog.open(this)) return;

def cs = new RecodeSpeakers(trsFile, outputTrsFile, idRegex, nameRegex, newID, newName);

return cs.process();