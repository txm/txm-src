package org.txm.macro.transcription

import java.nio.charset.Charset

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.txm.utils.*
import org.txm.utils.logger.*

@Field @Option(name="trsDirectory", usage="Dossier qui contient les fichiers TRS", widget="Folder", required=true, def="")
		File trsDirectory;

@Field @Option(name="resultDirectory", usage="Dossier résultat TRS", widget="Folder", required=true, def="")
		File resultDirectory;

@Field @Option(name="newSectionMarker", usage="The marker, spaces included", widget="String", required=true, def=" *#")
		def newSectionMarker

@Field @Option(name="debug", usage="activate debug messages", widget="Boolean", required=true, def="false")
		boolean debug

if (!ParametersDialog.open(this)) return;

if (!trsDirectory.exists()) {
	println "$trsDirectory not found"
	return
}

println "Writing result to $resultDirectory..."

dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_TIME
def strTotime(def str) {
	if (str.lastIndexOf(":") == -1) {
		return null
	}
	
	bonusFrame = Integer.parseInt(str.substring(str.lastIndexOf(":")+1))
	//if (str.contains("135475")) println "ERROR $str in $infos -> $bonusFrame"
	if (bonusFrame > 25) {
		bonusFrame=0;
	}
	totalFrame = str.substring(0, str.lastIndexOf(":"))
	
	LocalTime time1 = LocalTime.parse(totalFrame, dateTimeFormatter)
	totalFrame = (time1.getHour()*60*60) + (time1.getMinute()*60) + time1.getSecond()
	
	def ret = totalFrame + (bonusFrame/25)
	return ret
}

try {
	def trsFiles = trsDirectory.listFiles().findAll() { it.getName().toLowerCase().endsWith(".trs") }
	
	if (trsFiles.size() == 0) {
		println "No TRS file to process in $trsDirectory"
		return;
	}
	
	def errors = []
	ConsoleProgressBar cpb = new ConsoleProgressBar(trsFiles.size())
	for (File trsFile : trsFiles) {
		
		if (debug) println "== $trsFile =="
		else cpb.tick()
		
		// Open input file
		def slurper = new groovy.xml.XmlParser(false, true, true);
		slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false) // allow DTD declaration
		slurper.setProperty("http://javax.xml.XMLConstants/property/accessExternalDTD", "all"); // allow to read DTD from local file
		def trs = slurper.parse(trsFile.toURI().toString())
		def trsEpisodes = trs.Episode // 1
		if (trsEpisodes.size() > 1) {
			println "Error: multiple Episode node in $trsFile"
			continue
		}
		def trsEpisode = trsEpisodes[0]
		def trsSections =  trs.Episode.Section // 1
		if (trsSections.size() > 1) {
			println "Error: multiple Section node in $trsFile"
			continue
		}
		def trsSection = trsSections[0]
		
		def turns = trsSection.Turn
		def newSections = []
		def iSection = 0;
		def previousSection = trsSection
		def currentSection = trsSection
		
		def nFound = 0
		
		for (int iTurn = 0 ; iTurn < turns.size() ; iTurn++) {
			def turn = turns[iTurn]
			def previousTurn = turn
			def newTurn = null
			def start = Float.parseFloat(turn.@startTime)
			def end = Float.parseFloat(turn.@endTime)
			
			//if (debug) println "TURN: "
			
			def children = turn.children()
			for (int i = 0 ; i < children.size() ; i++) {
				def node = children[i]
				
				String content = null;
				if (node instanceof String) {
					content = node
				} else if (node instanceof groovy.util.Node && node.name() == "w") {
					content = node.text().trim()
					start = Float.parseFloat(node.@time)
				}
				
				if (content != null && (content.equals(newSectionMarker) || content.startsWith(newSectionMarker) || content.endsWith(newSectionMarker))) {
					
					if (debug) println "New section at $turn with $node child node"
					previousSection = currentSection
					currentSection = new Node(trsEpisode, "Section", new LinkedHashMap(["type":newSectionMarker, "startTime":turn.@startTime, "endTime":previousSection.attributes()["endTime"]]))
					
					previousSection.attributes()["endTime"] = start
					previousTurn.attributes()["endTime"] = turn.@startTime
					
					if (i > 0) { // The mark is not at the begining of the turn
						
						newTurn = new Node(currentSection, "Turn", new LinkedHashMap())
						for (def att : turn.attributes()) {
							newTurn.attributes()[att.getKey()] = att.getValue()
						}
						newTurn.attributes()["type"] = newSectionMarker
						newTurn.attributes()["startTime"] = start
						turn.attributes()["endTime"] = start
						
						def syncNode = new Node(newTurn, "Sync", new LinkedHashMap())
						syncNode.attributes()["time"] = start
						nFound++
						
					}
					
					children.remove(i) // remove the mark
					i--
					if (content.startsWith(newSectionMarker)) { // remove the marker and keep the tail content
						node.value = node.text().substring(newSectionMarker.length())
						newTurn.children().add(node)
					} else if (content.endsWith(newSectionMarker)) { // remove the marker and keep the head content
						node.value = node.text().substring(0, node.text().length() - newSectionMarker.length())
						newTurn.children().add(node)
					}
					
					
				} else if (newTurn != null) {
					turn.children().remove(i)
					i--
					newTurn.children().add(node)
				}
				previousTurn = turn
			}
			
			trsSection.remove(turn)
			currentSection.append(turn)
		}
		
		if (nFound == 0) {
			errors << trsFile.getName()
		}
		
		resultDirectory.mkdir()
		File outfile = new File(resultDirectory, trsFile.getName())
		outfile.withWriter("UTF-8") { writer ->
			writer.write('<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE Trans SYSTEM "trans-14.dtd">\n')
			def printer = new groovy.xml.XmlNodePrinter(new PrintWriter(writer))
			printer.setPreserveWhitespace(true)
			printer.print(trs)
		}
	}
	cpb.done()
	
	if (errors.size() > 0) {
		println "Error, no marker $newSectionMarker found in files: $errors"
	}
	
	println "Done."
	
} catch(Exception e) {
	println "Error: "+e
	Log.printStackTrace(e)
}
