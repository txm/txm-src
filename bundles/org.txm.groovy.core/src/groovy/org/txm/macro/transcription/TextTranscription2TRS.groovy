// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.transcription

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*
import org.txm.utils.xml.DomUtils;
import org.txm.doc.*;

import javax.xml.stream.*;

import java.util.HashMap;
import java.util.List;

import org.txm.scripts.importer.*;
import org.xml.sax.Attributes;
import org.txm.importer.scripts.filters.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import java.net.URL;

import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;
import org.txm.utils.io.*
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.txm.libs.jodconverter.ConvertDocument

/**
 * BUGS:
 * - n'a pas repéré les timings autour des commentaires
 * - a loupé du coup le timing du premier tour du prof : startTime="35.333332" au lieu de (0:00:48.5)
 * - a ajouté des sauts de ligne dans les contenus de tour, dommage.
 * @author mdecorde
 *
 */
class TextTranscription2TRS {
	boolean debug = false;
	boolean isDirectory = false;
	File outDir;
	def monitor

	TextTranscription2TRS(File outDir, boolean debug, def monitor) {
		this.outDir = outDir
		this.monitor = monitor
		this.debug = debug
	}

	boolean process(File file) {
		try {
			isDirectory = file.isDirectory();
			if (isDirectory) {
				return processDir(file)
			} else {
				return processFile(file, 100)
			}
		} catch(Exception e) {
			println "Error while processing: "+e
			if (debug) e.printStackTrace()
		}
	}

	boolean processDir(File dir) {
		boolean ret = true
		try {
			println "* Processing $dir directory"
			def files = dir.listFiles()
			def okfiles = [];
			if (files != null)
				for (File file : files) {
					String fname = file.getName().toLowerCase()
					if ( file.isFile() &&
					(fname.endsWith(".odt") || fname.endsWith(".doc") || fname.endsWith(".rtf") || fname.endsWith(".txt"))) {
						okfiles << file
					}

				}

			if (okfiles.size() == 0) {
				println "No ODT/DOC/RTF file found in $dir"
				return false
			}

			int delta = 100 / okfiles.size()
			for (File file : okfiles) {
				if (monitor != null) monitor.worked(0, "Processing file: $file")
				if (!processFile(file, delta)) {
					ret = false;
					println "ERROR: Failed to process $file"
				}

				if (monitor != null && monitor.isCanceled()) {
					break; // stop looping
				}
			}
		} catch(Exception e) {
			println "Error while processing directory: "+e;
			if (debug) e.printStackTrace();
		}
		return ret;
	}

	boolean processFile(File docFile, int work) {
		if (docFile == null) return false;
		int delta = work / 5
		println "** Processing $docFile file"
		String filename = docFile.getName()
		int idx = filename.lastIndexOf(".")
		if (idx < 0) return false;
		String ext = filename.substring(idx)
		filename = filename.substring(0, idx)

		//File docFile = new File(srcDir, "${filename}$ext")
		//		File htmlFile = new File(outDir, "${filename}.html")
		//		File htmlFile2 = new File(outDir, "${filename}_temp.html")
		//		File xhtmlFile = new File(outDir, "${filename}.xhtml")
		//		File xmlFile = new File(outDir, "${filename}.xml")
		File txtFile = new File(outDir, "${filename}.txt")
		File trsFile = new File(outDir, "${filename}-tmp.trs")
		File finalTrsFile = new File(outDir, "${filename}.trs")
		boolean ret = false;
		
		if (!docFile.getName().toLowerCase().endsWith("txt")) {
			if (monitor != null) monitor.worked(0, "DOCtoTXT: "+docFile.getName())

			ret = DOCtoTXT(docFile, txtFile)
			if (monitor != null) monitor.worked(delta, "DOCtoTXT: "+docFile.getName())
			if (monitor != null && monitor.isCanceled()) {
				println "Process interrupted by user"
				return false;
			}
			if (!ret) return ret;
		} else { // the document is already a TXT file
			org.txm.utils.io.FileCopy.copy(docFile, txtFile)
			ret = true
		}

		ret = ret && TXTtoTRS(txtFile, trsFile)
		if (monitor != null) monitor.worked(delta, "TXTtoTRS: "+docFile.getName())
		if (monitor != null && monitor.isCanceled()) {
			println "Process interrupted by user"
			return false;
		}
		if (!ret) return ret;

		ret = ret && TRStoFixedTRS(trsFile, finalTrsFile);
		if (monitor != null) monitor.worked(delta, "Done: "+docFile.getName())
		if (monitor != null && monitor.isCanceled()) {
			println "Process interrupted by user"
			return false;
		}
		if (!ret) return ret;

		// cleaning
		System.gc()
		if (!debug) {
			//htmlFile.delete();htmlFile2.delete();xhtmlFile.delete();xmlFile.delete();
			txtFile.delete()
			trsFile.delete()
		}

		return ret
	}

	boolean DOCtoTXT(File docFile, File txtFile) {
		println "*** ODT -> TXT"
		try {
			ConvertDocument.convert(docFile, txtFile)
		} catch(Exception e) {
			println "Error while converting $docFile : $e"
			if (debug) e.printStackTrace()
		}
		return txtFile.exists() && txtFile.length() > 0
	}

	boolean TXTtoTRS(File txtFile, File trsFile) {
		println "*** TXT -> TRS"
		try {
			TXT2TRS ht = new TXT2TRS();
			ht.process(txtFile, trsFile);
		} catch(Exception e) {
			println "Error while creating TRS file: "+e
			if (debug) e.printStackTrace()
		}
		return trsFile.exists() && trsFile.length() > 0
	}

	boolean TRStoFixedTRS(File trsFile, File finalTrsFile) {
		println "*** TRS -> FIXED TRS"
		try {
			def timingfixer = new SetTRSTurnTimingsAndSpeakers(trsFile, finalTrsFile);
			if (!timingfixer.process()) {
				println "Error while fixing timing"
			}
		} catch(Exception e) {
			println "Error while fixing TRS file: "+e
			if (debug) e.printStackTrace()
		}
		return finalTrsFile.exists() && finalTrsFile.length() > 0
	}
}
