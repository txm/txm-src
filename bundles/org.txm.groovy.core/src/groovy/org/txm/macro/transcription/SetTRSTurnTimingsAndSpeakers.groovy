// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.transcription

import java.text.SimpleDateFormat;
import java.util.Date;

class SetTRSTurnTimingsAndSpeakers {

	File trsInFile = new File("/home/mdecorde/xml/transana/out.xml")
	File trsOutFile = new File("/home/mdecorde/xml/transana/TrP1S8_08102010.trs")

	public SetTRSTurnTimingsAndSpeakers(File trsInFile, File trsOutFile) {
		this.trsInFile = trsInFile;
		this.trsOutFile = trsOutFile;
	}

	public boolean process() {
		def slurper = new groovy.xml.XmlParser();
		def trs = slurper.parse(trsInFile)

		// collect speakers from Turn@speaker values
		def speakers = new HashSet<String>();
		for (def turn : trs.Episode.Section.Turn) {
			String tmp = turn.@speaker.toUpperCase();
			speakers << tmp
			turn.@speaker = tmp
		}
		for (def speakersNode : trs.Speakers) {
			for (String spk : speakers) {
				def speakerNode = new Node(speakersNode, 'Speaker',
				[id:spk.replace(" ", "_"), name:spk, type:'unknown', check:'yes', dialect:'native', accent:'no', scope:'local'])
			}
			def speakerNode = new Node(speakersNode, 'Speaker',	[id:"none", name:"none", type:'unknown', check:'yes', dialect:'native', accent:'no', scope:'local'])
		}
				
		String currentEnd = null;
		for (def turn : trs.Episode.Section.Turn) {
			def children = turn.children()
	
			if (children.size() > 0) {
				//println "fixing turn start="+turn.@startTime+" end="+turn.@endTime+" previous end="+currentEnd
				if (currentEnd != null && currentEnd.length() > 0 && turn.@startTime.length() == 0) { // using last Sync
					//println "fixing with previous Turn $currentEnd"
					turn.@startTime = currentEnd
					setStartTime(children[0], currentEnd)
				} else if (!(children[0] instanceof String)) { // no previous Sync, using next Sync
					def start = getStartTime(children[0])
					if (start != null && start.length() > 0)	turn.@startTime = start
				}
				currentEnd = null
	
				if (!(children[children.size() - 1] instanceof String)) { // this is the last Sync
					def end = getStartTime(children[children.size() - 1])
					if (end != null && end.length() > 0)  {
						turn.@endTime = end
						currentEnd = end;
					}
				}
			}
		}
	
		def nodes = []
		// get Sync nodes to fix
		for (def section : trs.Episode.Section) { // all Section
			for (def child : section.children()) { // all Section children
				if (!(child instanceof String)) { // is a Tag
					switch(child.name()) {
						case "Turn": // get Sync in Turn
							//nodes << child;
							for (def sync : child.Sync) {
								nodes << sync
							}
							break;
						case "Sync":
							nodes << child;
							break;
						default: break;
					}
				}
			}
		}

		//Fixing Sync@time
		String previousValue = "0.0"
		for (int i = 0 ; i < nodes.size() ; i++) {
			def node = nodes[i]
			String time = getStartTime(node)	
			
			if (time.length() == 0 || time == previousValue) {				
				def list = [];
				String previous;
				if (i > 0) { previous = getStartTime(nodes[i-1]);
				} else { previous = "0.0" }
				
				String next = null;
				while ((next == null || next.length() == 0 || next == previousValue) && i <= nodes.size()) {
					list << node
					node = nodes[i]
					next = getStartTime(node)
					i++
				}
				if (next != null) {
					if (node != null) list << node
					fixSyncTimes(list, previous, next)
					previousValue = next;
				} else {
					println "ERROR: no end time in the transcription"
				}
			}
		}
	
		// fixing startTime of Turn using next Sync.@time
		def turns = trs.Episode.Section.Turn
		for (int i = 0 ; i < turns.size() ; i++) {
			def turn = turns[i]
			def syncs = turn.Sync
			if (syncs.size() > 0) {
				turn.@startTime = syncs[0].@time
			} else {
				println "Error: Turn with no Sync milestone"
			}
		}
	
		// fixing endTimes of Turn using next Turn.@startTime
		for (int i = 0 ; i < turns.size() ; i++) {
			def turn = turns[i]
			//println "turn start="+turn.@startTime+" end="+turn.@endTime
			if (i < turns.size() - 1) {
				if (turn.@endTime.length() == 0 && turns[i+1].@startTime.length() > 0) {
					//println "fixing turn.@endTime "+turn.@endTime+" with turns[i+1].@startTime "+(turns[i+1]);
					turn.@endTime = turns[i+1].@startTime
				}
			}
		}
		
		// fixing endTimes of Section using last Turn.@endTime
		// fixing startTimes of Section using first Turn.@startTime
		def sections = trs.Episode.Section
		for (int i = 0 ; i < sections.size() ; i++) {
			def t = sections[i].Turn
			if (t.size() == 0) continue;
			sections[i].@startTime = t[0].@startTime
			sections[i].@endTime = t[-1].@endTime
		}
		
		trsOutFile.withWriter("UTF-8") { writer ->
			writer.write('<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE Trans SYSTEM "trans-14.dtd">\n')
			new groovy.xml.XmlNodePrinter(new PrintWriter(writer)).print(trs) }
		
		return true;
	}
	
	def getStartTime(def node) {
		def ret = null;
		if (node == null) return "0.0"
		if (node.name() == null) return "0.0"
		switch(node.name()) {
			case "Turn":
			case "Section":
			//			println "Turn"
				ret = node.@startTime
				break
			case "Sync":
			//		println "Sync"
				ret = node.@time
				break
			default: break;
		}
		//println "getStartTime "+node.name()+" $ret"
		return ret
	}

	def setStartTime(def node, def value) {
		switch(node.name()) {
			case "Turn":
			case "Section":
				node.@startTime = ""+value
				break;
			
			case "Sync":
				return node.@time = ""+value
			default: break;
		}
	}

	def fixSyncTimes(def list, def start, def end) {
		println "Nfix: "+list.size()+" "+list
		
		def startf = Double.parseDouble(start)
		def endf = Double.parseDouble(end)
		def delta = (endf-startf)/list.size()
		println "start=$start end=$end delta=$delta"

		float c = startf;
		for (int i = 0 ; i < list.size() ; i++) {
			c += delta;
			list[i].@time = ""+c
		}
	}
}