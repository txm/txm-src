package org.txm.macro.transcription

import java.nio.charset.Charset

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.txm.utils.*
import org.txm.utils.logger.*

@Field @Option(name="trsDirectory", usage="Dossier qui contient les fichiers TRS", widget="Folder", required=true, def="")
File trsDirectory;

@Field @Option(name="outputTrsDirectory", usage="Dossier résultat qui contient les fichiers TRS", widget="Folder", required=true, def="")
File outputTrsDirectory;

@Field @Option(name="idRegex", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def idRegex

@Field @Option(name="nameRegex", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def nameRegex

@Field @Option(name="newID", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def newID

@Field @Option(name="newName", usage="Colonne de jointure de transcription", widget="String", required=true, def="")
def newName

def files = trsDirectory.listFiles()
if (files == null) {
	println "Error: no files in $trsDirectory"
	return false
}

outputTrsDirectory.mkdirs()
if (!outputTrsDirectory.exists()) {
	println "Error: can't create $outputTrsDirectory"
	return false;
}

boolean ok = true
for (File trsFile : files) {
	
	if (!trsFile.getName().toLowerCase().endsWith(".trs")) continue;
	
	File outputTrsFile = new File(outputTrsDirectory, trsFile.getName());
	
	ok = ok && gse.runMacro(RecodeSpeakersMacro, ["trsFile":trsFile, "outputTrsFile":outputTrsFile, "idRegex":idRegex, "nameRegex":nameRegex, "newID":newID, "newName":newName])
}

return ok