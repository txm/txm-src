// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.macro.transcription

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.rcp.swt.widget.parameters.*


@Field @Option(name="odtDir", usage="Dossier qui contient les fichiers ODT", widget="Folder", required=true, def="")
File odtDir;

@Field @Option(name="debug", usage="Don't clean files, 'y' to enable debug", widget="String", required=false, def="n")
def debug = "n";

if (!ParametersDialog.open(this)) return;

File outDir = new File(odtDir, "out") // write result in "out" folder

outDir.deleteDir()
outDir.mkdir()

println "out: $outDir"
println "odtDir: $odtDir"

println "DEBUG: "+("y" == debug || "yes" == debug)

if (!odtDir.exists()) {
	println "$odtDir does not exists. Aborting"
}
def monitor = null
TextTranscription2TRS trs = new TextTranscription2TRS(outDir, "y" == debug, monitor)
trs.process(odtDir)
