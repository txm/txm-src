// STANDARD DECLARATIONS
package org.txm.macroproto.export

import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.*
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.Toolbox

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "Please select a corpus first"
	return
}

// PARAMETERS

@Field @Option(name="csvFile", usage="CSV file path", widget="File", required=true, def="/tmp/metadata.csv")
def File csvFile

@Field @Option(name="columnSeparator", usage="column columnSeparator", widget="String", required=false, def="\t")
def columnSeparator

if (!ParametersDialog.open(this)) return

// BEGINNING
CQPCorpus corpus = corpusViewSelection
def CQI = CQPSearchEngine.getCqiClient()
def writer = csvFile.newWriter("UTF-8")
def internalTextProperties = ["project", "base", "path"]

println "Exporting $corpus metadata to $csvFile file."

def text = corpus.getStructuralUnit("text")
def properties = text.getProperties()

// putting the "id" property in the first position
def idi = properties.findIndexOf { prop -> prop.toString()=="id" }
if (idi != 0 && idi > 0) {
	def tmp = properties[0]
	properties[0] = properties[idi]
	properties[idi] = tmp
} else if (idi == -1) {
	println sprintf("** Warning: incoherent metadata content found for %s corpus - no 'id' metadata found", corpus)
	println "** Aborting"
	return
}

// getting values for all texts and all text metadata
def propertyValues = [:]
def numberOfTexts = CQI.attributeSize(properties[0].getQualifiedName())
def int[] struct_pos = new int[numberOfTexts]

struct_pos.eachWithIndex { c, i -> struct_pos[i] = i }

properties.each { property ->
	def values = CQI.struc2Str(property.getQualifiedName(), struct_pos)
	propertyValues[property] = values
}

// writing result to csvFile
def c = 0
properties.each { property ->
	if (!internalTextProperties.contains(property.getName())) {
		if (c++ > 0) writer.print columnSeparator
		writer.print "$property"
	}
}
writer.println ""

numberOfTexts.times {
	c = 0
	properties.each { property ->
		if (!internalTextProperties.contains(property.getName())) {
			if (c++ > 0) writer.print columnSeparator
			writer.print propertyValues[property][it]
		}
	}
	writer.println ""
}

writer.close()
println sprintf("Wrote %d different metadata for %d texts.", c, numberOfTexts)

