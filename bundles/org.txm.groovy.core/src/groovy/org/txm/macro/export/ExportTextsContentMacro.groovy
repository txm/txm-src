// Copyright © 2020-2022 ENS de Lyon, CNRS, University of Franche-Comté
// @author mdecorde
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro.export

import org.txm.searchengine.cqp.CQPSearchEngine
import org.txm.searchengine.cqp.corpus.*
import org.txm.searchengine.cqp.corpus.query.CQLQuery
import org.txm.Toolbox
import org.txm.utils.i18n.LangFormater;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*

if (!(corpusViewSelection instanceof CQPCorpus)) {
	monitorShowError("Erreur : la sélection dans la vue Corpus n'est pas un corpus ou un sous-corpus ($corpusViewSelection).")
	return false
} 

// PARAMETERS

@Field @Option(name="outputDirectory", usage="results output directory", widget="Folder", required=true, def="")
File outputDirectory

@Field @Option(name="wordProperty", usage="word property to export", widget="String", required=true, def="word")
def wordProperty

@Field @Option(name="oneWordPerLine", usage="output one word per line", widget="Boolean", required=false, def="false")
def oneWordPerLine

@Field @Option(name="oneSentencePerLine", usage="output one sentence per line", widget="Boolean", required=false, def="false")
def oneSentencePerLine

@Field @Option(name="sentenceStructureName", usage="name of the structure encoding sentences", widget="String", required=false, def="")
def sentenceStructureName

if (!ParametersDialog.open(this)) return

// BEGINNING

if (!outputDirectory.exists()) outputDirectory.mkdirs()

def corpus = corpusViewSelection
corpus.compute()
def mainCorpus = corpus.getMainCorpus()
def corpusName = corpus.getName()
def CQI = CQPSearchEngine.getCqiClient()

if (wordProperty == null || !(wordProperty.length() > 0)) {
	println "** Please provide a word property name in parameter 'wordProperty', for example 'word'. Aborting..."
	return 1
}

if (oneSentencePerLine && (sentenceStructureName == null || !(sentenceStructureName.length() > 0))) {
	println "** Please provide a name for the structure encoding sentences in parameter 'sentenceStructureName', or uncheck parameter 'oneSentencePerLine'. Aborting..."
	return 1
}

if (oneSentencePerLine) {

	lineSeparatorStructure = corpus.getStructuralUnit(sentenceStructureName)

	if (lineSeparatorStructure == null) {
		println "** No $sentenceStructureName structure in the $corpus corpus. Aborting..."
		return 1
	}
	
	breaks_pos = Arrays.asList(corpus.query(new CQLQuery("[]</"+sentenceStructureName+">"),"test", false).getEnds())
}

println "Exporting $corpus text content to $outputDirectory..."

def wordPropertyI = corpus.getProperty(wordProperty)

if (wordPropertyI == null) {
	println "** No '$wordProperty' word property in the $corpus corpus. Aborting..."
	return 1
}

def textidProperty = mainCorpus.getStructuralUnit("text").getProperty("id")
def textStartBoundaries = mainCorpus.getTextStartLimits()
def textEndBoundaries = mainCorpus.getTextEndLimits()

int[] struct_pos = CQI.cpos2Struc(textidProperty.getQualifiedName(), textStartBoundaries)
String[] allTextIds =  CQI.struc2Str(textidProperty.getQualifiedName(), struct_pos)

corpusTextIds = new HashSet<String>(corpus.getStructuralUnit("text").getProperty("id").getValues(corpus))

// https://stackoverflow.com/questions/150750/hashset-vs-list-performance

if (corpusTextIds.size() == 1) {
	println "1 text ("+corpusTextIds+")"
	} else {
	println ""+corpusTextIds.size()+" texts ("+corpusTextIds+")"
}

for (int i = 0; i < textStartBoundaries.size(); i++) {

    if (corpusTextIds.contains(allTextIds[i])) {
        
        int start = textStartBoundaries[i]
        int end = textEndBoundaries[i]
        File txtFile = new File(outputDirectory, allTextIds[i] + ".txt")

        def writer = txtFile.newWriter("UTF-8")
        int [] positions = new int [end - start + 1]
        int c = 0
        for (int p: start..end) {
            positions[c++] = p
        }
        int [] idx = CQI.cpos2Id(wordPropertyI.getQualifiedName(), positions)
        def words = CQI.id2Str(wordPropertyI.getQualifiedName(), idx)
        def tmp = []
        for (int j = 0; j < positions.length; j++) {
            int p = positions[j]
            tmp << words[j]
            if (oneSentencePerLine && breaks_pos.contains(p)) {
                if (oneWordPerLine) {
                    tmp.each {
                        word -> writer.println word
                    }
                } else {
                    writer.println LangFormater.format(tmp.join(" "), corpus.getLang())
                }
                tmp = []
            }
        }
        if (tmp.size() > 0) {
            if (oneWordPerLine) {
                tmp.each {
                    word -> writer.println word
                }
            } else {
                writer.println LangFormater.format(tmp.join(" "), corpus.getLang())
            }
        }
        writer.close()
    }
}  

println "Done, result saved in "+outputDirectory.getAbsolutePath()

def monitorShowError(String message) {
	monitor.syncExec(new Runnable() {
		public void run() {
			org.eclipse.jface.dialogs.MessageDialog.openError(org.eclipse.swt.widgets.Display.getCurrent().getActiveShell(), "Erreur", message)
		}
	})
}
