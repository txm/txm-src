// Copyright © 2022 ENS LYON
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro.export

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.ca.core.functions.CA
import org.txm.libs.office.WriteODS

@Field @Option(name="outputODSFile", usage="ODS file with 2 sheets", widget="FileSave", required=true, def="")
def outputODSFile

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END OF PARAMETERS

def scriptName = this.class.getSimpleName()

if (!(corpusViewSelection instanceof CA)) {
	println "** $scriptName: please select a CA in the Corpus view."
	return 0
}

CA ca = corpusViewSelection
outputODSFile = outputODSFile.getAbsoluteFile()
println "Exporting CA '$ca' to '$outputODSFile'..."

WriteODS writer = new WriteODS(outputODSFile)
def header = ["label", "Q12", "Q13", "Q23", "Masse", "Dist", "Cont1", "Cos²1", "Cont2", "Cos²2", "Cont3", "Cos²3", "c1", "c2", "c3"]

println "Writing the rows infos table..."

writer.table.setTableName("rows")
i = 0
writer.writeLine(header, i++)
for (def line : ca.getRowInfos()) {
line = [line[0],line[2], line[3], line[4], line[5], line[6], line[8], line[9], line[11], line[12], line[14], line[15], line[17], line[18], line[19]] 
	writer.writeLine(line, i++)
}

println "Writing the column infos table..."
table = writer.newTable("cols")
i = 0
writer.writeLine(header, i++)
for (def line : ca.getColInfos()) {
	line = [line[0],line[2], line[3], line[4], line[5], line[6], line[8], line[9], line[11], line[12], line[14], line[15], line[17], line[18], line[19]] 
	writer.writeLine(line, i++)
}

writer.save()