package org.txm.macro.r;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.Toolbox
import org.txm.statsengine.r.core.RWorkspace
import org.txm.statsengine.r.core.RResult
import org.txm.core.results.TXMResult

if (editor != null) {
	println "editor: $editor"
}

def selection = corpusViewSelection
if (!(selection instanceof TXMResult)) {
	println "Selection is not a result. Aborting"
	return
}
def symbol = null;

if (selection instanceof RResult) {
	symbol = ((RResult)selection).getRSymbol();
//} else if (selection.getStringParameterValue("rsymbol").length() > 0) {
//	symbol = selection.getStringParameterValue("rsymbol")
} else {
	try {
		symbol = selection.getClass().getDeclaredMethod("getSymbol").invoke(selection)
	} catch(Exception e) {}
}

if (symbol != null) {
	println "R Name of $selection is: $symbol"
} else {
	println "$selection has no R representation"
}
