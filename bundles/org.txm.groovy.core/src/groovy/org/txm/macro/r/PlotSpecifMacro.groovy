package org.txm.macro.r;
// Copyright © 2013-2016 ENS de Lyon, University of Franche-Comté.
// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.commands.*
import org.txm.Toolbox
import org.txm.statsengine.r.core.RWorkspace

/**
 * 
 * @authors Matthieu Decorde, Serge Heiden, Achille Falaise.
 * 
 * Sample default values from:
 *  Lafon, P. “Sur la variabilité de la fréquence des formes dans un corpus.” Mots, no. 1 (1980):
 *  Robespierre 'peuple' word in D9 part example (see Figure 1), pp 140-141.
 *  <http://www.persee.fr/web/revues/home/prescript/article/mots_0243-6450_1980_num_1_1_1008>.
 *  
 **/

@Field @Option(name="f", usage="La fréquence de la forme dans la partie", widget="Integer", required=true, def="11")
		def f;

@Field @Option(name="F", usage="La fréquence totale de la forme dans le corpus", widget="Integer", required=true, def="296")
		def F;

@Field @Option(name="t", usage="Le nombre d'occurrences de la partie", widget="Integer", required=true, def="1084")
		def t;

@Field @Option(name="T", usage="Le nombre total d'occurrences du corpus", widget="Integer", required=true, def="61449")
		def T;

@Field @Option(name="display_chart", usage="Display the SVG file in a new Window.", widget="Boolean", required=true, def="true")
		def display_chart;

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

// END OF PARAMETERS

def r = RWorkspace.getRWorkspaceInstance()
def resultsDir = new File(Toolbox.getTxmHomePath(), "results")
resultsDir.mkdirs()
def file = File.createTempFile("txm", ".svg", resultsDir)

/// BEGIN SCRIPTS
def script ="""
library(textometry)
res <- specificities.distribution.plot($f, $F, $t, $T)
"""
/// END SCRIPTS
r.plot(file, script)

def px = r.eval('res$px').asDouble()
def mode = r.eval('res$mode').asInteger()
def pfsum = r.eval('res$pfsum'+"[$f+1]").asDouble()
def isPositiveSpecificity = (f > mode.abs())  // True if S+
def proba = pfsum.abs()  // Compute actual probability (should be a positive number, even for negative specificities, where pfsum = -probability)
def specifSign = (isPositiveSpecificity ? "+" : "-")
def specif = Math.log10(proba).abs()  // Compute actual specificity
def operatorForDisplay = isPositiveSpecificity ? '>=' : '<='

println """
P(f'=f) et P(f' $operatorForDisplay f)
f	F	t	T
$f	$F	$t $T
P(f' = $f) = $px
mode = $mode
P(f' $operatorForDisplay $f) = $proba
Specificity S$specifSign = $specif
"""
println "Result saved in: "+file.getAbsolutePath()

//display the graphic
if (display_chart) {
	monitor.syncExec(new Runnable() {
		@Override
		public void run() {	OpenBrowser.openfile(file.getAbsolutePath()) }
	});
}