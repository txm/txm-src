package org.txm.macro.r

// Classes utilisées
import org.txm.Toolbox
import org.txm.statsengine.r.core.RWorkspace
import org.txm.index.core.functions.Index
import org.txm.rcp.commands.OpenBrowser

// Variables
symbol = null
prop = "none"

// Récupération de la sélection dans la vue Corpus
sel = corpusViewSelection

// Envoi de l'Index dans R
if (sel instanceof Index) {
	sel.compute(false)
	println "Sending Index data to R..."
	symbol = sel.asRMatrix()+'$data'
	prop = sel.getProperties().toString()
} else {
	println "Selection is not an Index. Aborting."
	return
}

// Création du fichier de sortie .svg
def file = File.createTempFile("IndexHist", ".svg", new File(Toolbox.getTxmHomePath(), "results"))

// Récupération de R
def r = RWorkspace.getRWorkspaceInstance()

// Définition du script R
def script ="""
par(las=2)
barplot(t($symbol), xlab="$prop", ylab="f")
"""

// Appel du script en écrivant dans le fichier de sortie
r.plot(file, script)

println "Result saved in: "+file.getAbsolutePath()

// Affichage du fichier de sortie .svg dans une nouvelle fenêtre de TXM
monitor.syncExec(new Runnable() {
	@Override
	public void run() {	OpenBrowser.openfile(file.getAbsolutePath()) }
})
