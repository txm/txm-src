package org.txm.macro.r;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*

import groovy.transform.Field

import org.txm.index.core.functions.Index
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.commands.*
import org.txm.Toolbox
import org.txm.statsengine.r.core.RWorkspace

def sel = corpusViewSelection

String symbol = null;
String prop = "none"
if (sel instanceof Index) {
	sel.compute(false)
	println "Sending Index data to R..."
	symbol = ((Index)sel).asRMatrix()
	symbol = "t("+symbol+'$data)'
	prop = ((Index)sel).getProperties().toString()
} else {
	println "Selection is not an Index. Aborting."
	return
}

def r = RWorkspace.getRWorkspaceInstance()
new File(Toolbox.getTxmHomePath(), "results").mkdirs()
def file = File.createTempFile("IndexHist", ".svg", new File(Toolbox.getTxmHomePath(), "results"))

/// BEGIN SCRIPTS
def script ="""
par(las=2)
barplot($symbol, xlab="$prop", ylab="f")
"""
/// END SCRIPTS
r.plot(file, script)
println "Result saved in: "+file.getAbsolutePath()

//display the graphic
monitor.syncExec(new Runnable() {
	@Override
	public void run() {	OpenBrowser.openfile(file.getAbsolutePath()) }
});
