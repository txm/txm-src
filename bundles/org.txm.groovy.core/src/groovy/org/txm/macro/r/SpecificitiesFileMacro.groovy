// Copyright © 2021 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden

package org.txm.macro.r

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.rcp.commands.*
import org.txm.Toolbox
import org.txm.statsengine.r.core.RWorkspace
import java.text.DecimalFormat
import java.util.Locale

// BEGIN PARAMETERS

@Field @Option(name="inputFile", usage="(f, F, t, T) values TSV table, one line per example", widget="FileOpen", required=true, def="")
def inputFile

@Field @Option(name="outputLanguage", usage="output langage (FR, EN...) for decimal point format", widget="String", required=false, def="FR")
def outputLanguage


// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return

// END PARAMETERS

// Prepare R output environment
def r = RWorkspace.getRWorkspaceInstance()
def resultsDir = new File(Toolbox.getTxmHomePath(), "results")
resultsDir.mkdirs()
def file = File.createTempFile("txm", ".svg", resultsDir)

// Output header
println """f	F	t	T	mode	comp	sign	P(f')	P(f'<>mode)	specificity	specif"""

// For each line of input
inputFile.eachLine { line ->

  if (line == "") return

  def (sf, sF, st, sT) = line.split("\t")
  def f = sf.toInteger()
  def F = sF.toInteger()
  def t = st.toInteger()
  def T = sT.toInteger()

  /// BEGIN R SCRIPTS
  def script ="""
  library(textometry)
  res <- specificities.distribution.plot($f, $F, $t, $T)
  """
  /// END R SCRIPTS

  // Call R script
  r.plot(file, script)

  // Compute auxiliary results
  def px = r.eval('res$px').asDouble()
  def mode = r.eval('res$mode').asInteger()
  def pfsum = r.eval('res$pfsum'+"[$f+1]").asDouble()
  def isPositiveSpecificity = (f > mode.abs())  // True if S+
  def proba = pfsum.abs()  // Compute actual probability (should be a positive number, even for negative specificities, where pfsum = -probability)
  def specifSign = (isPositiveSpecificity ? "+1" : "-1")
  def specifSign2 = (isPositiveSpecificity ? "" : "-")
  def operatorForDisplay = isPositiveSpecificity ? '>=' : '<='
  def specif = Math.log10(proba).abs()  // Compute actual specificity
  def specifTruncPlusOne = specif.trunc().toInteger()+1

  def locale = new Locale(outputLanguage)
  def pxStr = String.format(locale, "%.16e", px)
  def probaStr = String.format(locale, "%.16e", proba)
  def specifStr = String.format(locale, "%.16f", specif)

  // Output results
  println """$f	$F	$t	$T	$mode	$operatorForDisplay	$specifSign	$pxStr	$probaStr	$specifSign2$specifStr	$specifSign2$specifTruncPlusOne"""

}

// Delete unused graphic
if (!file.delete()) {
  println "** problem deleting temporary file "+file+"."
}

/* TEST: generate content of PlotSpecifFile-test-file.tsv

51.times {
  println "$it	50	4588	41027"
}

*/
