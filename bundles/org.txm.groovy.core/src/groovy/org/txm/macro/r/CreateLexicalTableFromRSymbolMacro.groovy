package org.txm.macro.r;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.Toolbox
import org.txm.statsengine.r.core.RWorkspace
import org.txm.statsengine.r.core.RResult
import org.txm.core.results.TXMResult
import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.lexicaltable.core.statsengine.r.data.LexicalTableImpl
import org.txm.lexicaltable.core.functions.LexicalTable
import org.txm.searchengine.cqp.corpus.MainCorpus

def selection = corpusViewSelection
if (!(selection instanceof TXMResult)) {
	println "Selection is not a result. Aborting"
	return
}

@Field @Option(name="r_table_symbol", usage="R symbol of the R table to import", widget="String", required=false, def="")
def r_table_symbol

if (!ParametersDialog.open(this)) return

if (!RWorkspace.getRWorkspaceInstance().containsVariable(r_table_symbol)) {
	println "Error: no R object found for symbol '$r_table_symbol'"
	return
}
def corpus = corpusViewSelection
if (!(corpus instanceof MainCorpus)) {
	corpus = corpus.getFirstParent(MainCorpus.class)
}

def lt = new LexicalTable(corpus);
lt.setData(new LexicalTableImpl(r_table_symbol))
lt.compute()

monitor.syncExec(new Runnable() {
	public void run() {
		org.txm.rcp.views.corpora.CorporaView.reload()
	}
});