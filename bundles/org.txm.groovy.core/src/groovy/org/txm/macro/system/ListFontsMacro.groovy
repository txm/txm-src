// Copyright © 2019 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
// @author sheiden
// STANDARD DECLARATIONS
package org.txm.macro.utils

def e = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment()
def fonts = e.getAllFonts()

println fonts.size()+" fonts on this computer:"
println ""

fonts.each { f ->
	println f.getFontName()
}
