// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
package org.txm.macro.debug
System.properties.each { key, value ->
	if (key ==~ /^os\..*/) {
		println sprintf("%10s = %s", key, value)
	}
}
