// Copyright © 2020 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
package org.txm.macro.debug

println "Windows: "

println "Mac OS X: "

println "Ubuntu: "
println "	GTK version: "+ System.properties.'org.eclipse.swt.internal.gtk.version'
println "	WEBKITGTK version: "+ System.properties.'org.eclipse.swt.internal.webkitgtk.version'

//def loader = ClassLoader.getSystemClassLoader()
//LIBRARIES = ClassLoader.class.getDeclaredField("loadedLibraryNames")
//LIBRARIES.setAccessible(true)
//println "Loaded libraries: "
//println "\t"+LIBRARIES.get(loader).join("\n\t")