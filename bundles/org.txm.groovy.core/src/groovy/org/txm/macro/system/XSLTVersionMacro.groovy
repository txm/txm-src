// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
package org.txm.macro.debug
import net.sf.saxon.Version



println "XSLT "+Version.getSoftwarePlatform()+" ("+Version.getProductTitle()+" - "+Version.getReleaseDate()+")"

Class c = net.sf.saxon.Version.class
println c.getProtectionDomain().getCodeSource().getLocation().getFile()