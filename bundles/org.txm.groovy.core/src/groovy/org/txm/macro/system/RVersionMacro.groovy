// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
//
// @author sheiden

package org.txm.macro.debug

import org.txm.statsengine.r.core.RWorkspace

def r = RWorkspace.getRWorkspaceInstance()

println "VERSION="+r.eval("R.version.string").asString()

println ".libPaths="+r.eval(".libPaths()").asString()
println ".Library="+r.eval(".Library").asString()
//println ".Library.site="+r.eval(".Library.site").asString() // no more available ?

println "R.home()="+r.eval("R.home()").asString()
rez = r.eval("options()")
println rez

def printList(def list) {
	printList(list, "");
}
def printList(def list, def tabs) {
	int n = 0;
	def names =  list.names
	for (def o : list) {
		if (o != null) {
			if (o.isList()) {
				println tabs+"\t"+names[n]+"="
				printList(o.asList(), tabs+"\t")
			} else {
				println tabs+"\t"+names[n]+"="+o.asString()
			}
		}
		n++
	}
}

options=rez.asList()
println "OPTIONS: "
printList(options)

println "\nLIBRARIES: "
for (def l : ["Rserve", "textometry", "FactoMineR", "wordcloud"]) {
	try {
		def res = r.safeEval("packageVersion(\"$l\")")
		
		println "\t"+l+": "+res.asList()[0].asIntegers().join(".");
	} catch(Exception ex) {
		println "\tNo package $l"
	}
}
