// Copyright © 2023 MY_INSTITUTION
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author mdecorde

// STANDARD DECLARATIONS
package org.txm.macro.debug

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.eclipse.core.runtime.RegistryFactory;
import org.txm.core.results.TXMResult;


@Field @Option(name="extension_point_filter", usage="Positive filter by name", widget="String", required=false, def=".+")
def extension_point_filter

@Field @Option(name="extension_attribute_filter", usage="Positive filter by name", widget="String", required=false, def="")
def extension_attribute_filter

@Field @Option(name="extension_attribute_value_filter", usage="Positive filter by name", widget="String", required=false, def=".+")
def extension_attribute_value_filter

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

extension_point_filter = /$extension_point_filter/
extension_attribute_value_filter = /$extension_attribute_value_filter/

println "Extensions points: "
def eps = RegistryFactory.getRegistry().getExtensionPoints()
eps.sort()
for (def ep : eps) {
	
	if (!(ep.getUniqueIdentifier() =~ extension_point_filter)) continue;
	
	
	System.out.println(ep.getUniqueIdentifier());
	for (def c : RegistryFactory.getRegistry().getConfigurationElementsFor(ep.getUniqueIdentifier())) {
		if (extension_attribute_filter.length() > 0) {
			if (extension_attribute_value_filter.length() == 0) {
				if (c.getAttribute(extension_attribute_filter) == null) continue;
			} else {
				if (!(c.getAttribute(extension_attribute_filter) =~ extension_attribute_value_filter)) continue;
			}
		}
		
		println "\t"+c+": "+c.getAttributeNames().collect() { it-> it + "="+c.getAttribute(it) }.join(", ")
	}
}

