// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
package org.txm.macro.debug
import javax.script.*

manager = new ScriptEngineManager()
for (def factory : manager.getEngineFactories()) {
	println factory.getLanguageName()+" "+factory.getLanguageVersion()+" ("+factory.getEngineName()+" "+factory.getEngineVersion()+")"
}
