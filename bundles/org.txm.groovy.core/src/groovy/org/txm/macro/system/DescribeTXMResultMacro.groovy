// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
package org.txm.macro.debug

import org.txm.objects.*

def printResult(def obj, String prof) {
	println (prof+obj.getClass().getSimpleName()+"="+obj)
	for (def c : obj.getChildren()) {
		printResult(c, prof+ " ")
	}
}

def projet = corpusViewSelection.getProject()
if (projet == null && projet instanceof Project) projet = corpusViewSelection
if (projet == null) {
	println "no selection usable $corpusViewSelection"
	return;
}

printResult(projet, "")

println " object creation: "+corpusViewSelection.getCreationDate()
println " object update  : "+corpusViewSelection.getLastComputingDate()	
println "project creation: "+projet.getCreationDate()
println "project update  : "+projet.getLastComputingDate()
println "project 'default' edition : "+projet.getEditionDefinition("default")
println "project 'default' edition : "+projet.getEditionDefinition("facs")

println "Parameter keys:"
println "\t"+org.txm.core.results.BeanParameters.getFieldsPerName(corpusViewSelection).keySet().join("\n\t")

println "Java description:"
org.txm.utils.DescribeJavaObject.print(corpusViewSelection.getClass())