// STANDARD DECLARATIONS
package org.txm.macro.debug

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.core.preferences.TXMPreferences
import org.txm.searchengine.cqp.CQPPreferences
import org.txm.utils.io.*

@Field @Option(name="outputFile", usage="set outputFile to '' to write in console", widget="CreateFile", required=false, def="")
File outputFile;
if (!ParametersDialog.open(this)) {
	return
}

if (outputFile == null) {
	org.txm.core.preferences.TXMPreferences.dump();
} else {
	def out = new PrintStream(outputFile);
	org.txm.core.preferences.TXMPreferences.dump(out);
	out.close()
	println "Result in: $outputFile"
}

//println CQPPreferences.getInstance().getProperties()