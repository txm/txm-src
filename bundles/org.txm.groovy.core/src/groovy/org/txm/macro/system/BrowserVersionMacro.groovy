// Copyright © 2016 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author sheiden
package org.txm.macro.debug

import org.eclipse.ui.*
import org.eclipse.ui.browser.*
import org.eclipse.ui.internal.browser.*
import org.eclipse.ui.internal.browser.browsers.MozillaBrowser.*
import org.txm.rcp.commands.*

b = PlatformUI.getWorkbench().getBrowserSupport().createBrowser(IWorkbenchBrowserSupport.LOCATION_BAR | IWorkbenchBrowserSupport.NAVIGATION_BAR | IWorkbenchBrowserSupport.STATUS, 'iname', 'name', 'longName')

println b.getClass()

monitor.syncExec(new Runnable() {
			public void run() {

				def editor = OpenBrowser.openfile("file:hello", "Hello");

				def script = """
return ""+navigator.appName+" "+navigator.appVersion
"""
				println editor.webBrowser.getBrowser().evaluate(script);
				
				editor.close()
			}});