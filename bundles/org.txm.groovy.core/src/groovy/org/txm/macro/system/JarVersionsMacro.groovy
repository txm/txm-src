// Copyright © 2023 ENS Lyon
// Licensed under the terms of the GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.html)
// @author sheiden

// STANDARD DECLARATIONS
package org.txm.macro

def printClassPath(classLoader) {
  classLoader.getURLs().each {url->
    path = url.getPath()
    if (!(path ==~ /.*\/$/)) println path
  }
}
printClassPath this.class.classLoader
