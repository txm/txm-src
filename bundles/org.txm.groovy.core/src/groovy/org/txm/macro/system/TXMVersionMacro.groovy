// Copyright © 2021 ENS de Lyon, CNRS, University of Franche-Comté
// Licensed under the terms of the GNU General Public License (http://www.gnu.org/licenses)
// @author mdecorde
package org.txm.macro.debug

println "TXM version: "+ org.txm.rcp.ApplicationWorkbenchAdvisor.getTXMVersionPrettyString()

println "TXM working directory: "+org.txm.Toolbox.getTxmHomePath()

println "TXM installation directory: "+org.txm.Toolbox.getInstallDirectory()