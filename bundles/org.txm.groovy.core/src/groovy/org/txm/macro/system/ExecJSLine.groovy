import org.txm.edition.rcp.editors.*

@Field @Option(name="script", usage="an example string", widget="Text", required=false, def="alert('hello world!');")
script


if (!(editor.getClass().getName().equals("org.txm.edition.rcp.editors.SynopticEditionEditor"))) {
	println "Editor is not an edition: "+editor+". Use the F12 key binding to run the macro."
	return
}
monitor.syncExec(new Runnable() {
	public void run() {
		panel = editor.editionPanels["default"]
		
		println "Execute result: "+panel.execute(script)
}});

