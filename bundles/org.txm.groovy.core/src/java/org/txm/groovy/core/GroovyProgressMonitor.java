package org.txm.groovy.core;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * IProgressMonitor wrapper
 * 
 * adds some methods for Groovy scripts
 * 
 * @author mdecorde
 *
 */
public class GroovyProgressMonitor implements IProgressMonitor {

	private IProgressMonitor monitor;

	public GroovyProgressMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
	}

	@Override
	public void beginTask(String name, int totalWork) {
		if (monitor == null) return;

		monitor.beginTask(name, totalWork);
	}

	@Override
	public void done() {
		if (monitor == null) return;
		monitor.done();
	}

	@Override
	public void internalWorked(double work) {
		if (monitor == null) return;
		monitor.internalWorked(work);
	}

	@Override
	public boolean isCanceled() {
		if (monitor == null) return false;

		return monitor.isCanceled();
	}

	@Override
	public void setCanceled(boolean value) {
		if (monitor == null) return;

		monitor.setCanceled(value);
	}

	@Override
	public void setTaskName(String name) {
		if (monitor == null) return;

		monitor.setTaskName(name);
	}

	@Override
	public void subTask(String name) {
		if (monitor == null) return;

		monitor.subTask(name);
	}

	@Override
	public void worked(int work) {
		if (monitor == null) return;
		monitor.worked(work);
	}

	public void worked(int work, String message) {
		if (monitor == null) return;
		monitor.worked(work);
		monitor.setTaskName(message);
	}
}
