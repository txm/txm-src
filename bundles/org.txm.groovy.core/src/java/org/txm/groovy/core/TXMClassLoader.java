package org.txm.groovy.core;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;

import org.eclipse.osgi.internal.loader.EquinoxClassLoader;
import org.txm.utils.logger.Log;

/**
 * Class loader that looking for a class in all installed RCP Bundle then load it.
 * 
 * @author mdecorde
 *
 */
public class TXMClassLoader extends ClassLoader {

	/**
	 * Index of package names => loaders.
	 */
	private HashMap<String, EquinoxClassLoader> loaders;

	/**
	 * Index of bundle names => loaders.
	 */
	private HashMap<String, HashSet<EquinoxClassLoader>> packageToLoaders;

	/**
	 * Default loader to first look in.
	 */
	private ClassLoader defaultLoader;

	/**
	 * 
	 * @param defaultLoader
	 * @param loaders2
	 */
	public TXMClassLoader(ClassLoader defaultLoader, HashSet<EquinoxClassLoader> loaders2) {

		this.defaultLoader = defaultLoader;

		boolean useFastIndex = GroovyPreferences.getInstance().getBoolean(GroovyPreferences.FAST_PACKAGE_INDEX);

		if (useFastIndex) {
			Log.finest("Fast package index option is activated."); //$NON-NLS-1$
			this.packageToLoaders = new HashMap<String, HashSet<EquinoxClassLoader>>();
		}
		else {
			this.loaders = new HashMap<>();
		}

		// build an index of package names => loaders
		for (EquinoxClassLoader loader : loaders2) {
			if (useFastIndex) {
				Dictionary<String, String> d = loader.getBundle().getHeaders();
				String packagesString = d.get("Export-Package"); //$NON-NLS-1$
				if (packagesString != null) {
					packagesString = packagesString.replace("\n", ""); //$NON-NLS-1$ //$NON-NLS-2$
					for (String p : packagesString.split(",")) { //$NON-NLS-1$
						p = p.trim();
						if (p.contains(";")) p = p.substring(p.indexOf(";")); //$NON-NLS-1$ //$NON-NLS-2$
						if (!packageToLoaders.containsKey(p)) {
							HashSet<EquinoxClassLoader> tmp = new HashSet<EquinoxClassLoader>();
							packageToLoaders.put(p, tmp);
						}
						packageToLoaders.get(p).add(loader);
					}
				}
			}
			// build an index of bundle names => loaders 
			else {
				loaders.put(loader.getBundle().getSymbolicName(), loader);
			}
		}
		//		for(String p : packageToLoaders.keySet()) System.out.println(p);
	}

	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {

		// try with the System class loader
		try {
			Class<?> c = defaultLoader.loadClass(name);

			//System.out.println("+TXMClassLoader.findClass(): class loaded from default loader: " + name + " class:" + c.getCanonicalName());

			return c;
		}
		catch (Exception e) {
		}

		if (packageToLoaders != null) { // fast index enable
			//get the package name from the canonical class name
			String p = null;
			int idx = name.lastIndexOf(".");
			if (idx > 0) {
				p = name.substring(0, idx);
			}
			else {
				p = "";
			}

			if (packageToLoaders.containsKey(p)) {
				for (EquinoxClassLoader loader : packageToLoaders.get(p)) {
					if (loader != null) {
						Class<?> c = loader.loadClass(name);
						//System.out.println("load from " + loader.getName() + ": " + name);
						//System.out.println("+TXMClassLoader.findClass(): class loaded from package loaders index: " + name + " class:" + c.getCanonicalName());
						return c;
					}
				}
			}
		}
		else { // no fast index, try with all loaders until one works
			for (EquinoxClassLoader cl : loaders.values()) {

				// FIXME: SJ: tests to speed up this very long process
				// if(cl instanceof EquinoxClassLoader) {
				// EquinoxClassLoader ecl = (EquinoxClassLoader) cl;
				//
				// //Log.finest("TXMClassLoader.findClass(): looking for class " + name + " in bundle " + ecl.getBundle().getSymbolicName());
				//
				// // FIXME: trying to skip Bundle where Bundle id not matching the class package
				// String pack = name.split("\\$")[0];
				// if(!ecl.getBundle().getSymbolicName().startsWith(pack)) {
				// // FIXME: SJ: for debugging to speed up the process
				// //Log.finest("TXMClassLoader.findClass(): skipping bundle " + ecl.getBundle().getSymbolicName() + " for class starting with " + pack);
				// continue;
				// }
				// }
				//

				try {
					return cl.loadClass(name);
				}
				catch (Exception e) { // an exception is raised if class is not found, ignore it and try with the other loaders
				}
			}
		}



		//System.out.println("-------TXMClassLoader.findClass(): class not found in default loader or package index: " + name);


		//TODO first tests did not boost the class loading
		//		// try using the class package name
		//		if (name.startsWith("org.txm.")) {
		//			int idx = name.indexOf(".rcp.");
		//			if (idx > 0) {
		//				String loaderName = name.substring(0, idx+4);
		//				EquinoxClassLoader loader = loaders.get(loaderName);
		//				try {
		//					if (loader != null) {
		//						Class<?> c = loader.loadClass(name);
		//						//System.out.println("load from "+loaderName+": "+name);
		//						return c;
		//					}
		//				}
		//				catch (Exception e) {
		//					//System.out.println("can't load from "+loaderName+": "+name+" "+e);
		//					if (e instanceof NullPointerException) {
		//						e.printStackTrace();
		//					}
		//				}
		//			}
		//			idx = name.indexOf(".core.");
		//			if (idx > 0) {
		//				String loaderName =name.substring(0, idx)+".rcp";
		//				EquinoxClassLoader loader = loaders.get(name.substring(0, idx)+".rcp");
		//				try {
		//					if (loader != null) {
		//						Class<?> c = loader.loadClass(name);
		//						//System.out.println("load from "+loaderName+": "+name);
		//						return c;
		//					}
		//				}
		//				catch (Exception e) {
		//					//System.out.println("can't load from "+loaderName+": "+name+" "+e);
		//					if (e instanceof NullPointerException) {
		//						e.printStackTrace();
		//					}
		//				}
		//				
		//				loaderName = name.substring(0, idx+5);
		//				loader = loaders.get(loaderName);
		//				try {
		//					if (loader != null) {
		//						Class<?> c = loader.loadClass(name);
		//						//System.out.println("load from "+loaderName+": "+name);
		//						return c;
		//					}
		//				}
		//				catch (Exception e) {
		//					//System.out.println("can't load from "+loaderName+": "+name+" "+e);
		//					if (e instanceof NullPointerException) {
		//						e.printStackTrace();
		//					}
		//				}
		//			}
		//		}


		throw new ClassNotFoundException(name);
	}
}
