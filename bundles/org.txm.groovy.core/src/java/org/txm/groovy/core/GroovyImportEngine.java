package org.txm.groovy.core;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.txm.Toolbox;
import org.txm.core.engines.ImportEngine;
import org.txm.core.engines.ImportEngines;
import org.txm.core.engines.ScriptedImportEngine;
import org.txm.objects.Project;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class GroovyImportEngine extends ImportEngine {

	@Override
	public boolean isRunning() {
		return false;
	}

	@Override
	public boolean initialize() throws Exception {

		ImportEngines engines = Toolbox.getImportEngines();
		File importsPackageDirectory = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/scripts/importer"); //$NON-NLS-1$

		importsPackageDirectory.mkdirs();

		if (!importsPackageDirectory.exists()) {
			Log.warning("No script directory found: " + importsPackageDirectory.getAbsolutePath());
			return false;
		}
		for (File dir : importsPackageDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)) {
			if (!dir.isDirectory()) continue;

			String name = dir.getName();
			File loader = new File(dir, name + "Loader.groovy"); //$NON-NLS-1$
			if (loader.exists()) {
				ScriptedImportEngine sengine = new GroovyScriptedImportEngine(dir);
				if (engines.getEngine(name) == null) {
					engines.put(name, sengine);
				}
				else {
					System.out.println("Scripted import module in " + dir + " not registered since one with the same name is registered.");
				}
			}
		}
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getName() {
		return "groovy scripted imports installers - not meant to be used"; //$NON-NLS-1$
	}

	@Override
	public IStatus build(Project project, IProgressMonitor monitor) {
		return Status.CANCEL_STATUS;
	}
}
