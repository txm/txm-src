package org.txm.groovy.core.messages;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;

public class GroovyMessages extends TXMCoreMessages {

	private static final String BUNDLE_NAME = "org.txm.groovy.core.messages.messages"; //$NON-NLS-1$

	public static String restartingToolboxSearchengines;


	public static String errorImportNotCorrectlyEndedSeeConsoleMessages;

	public static String errorTreeTaggerAnnotationEngineIsNotReady;

	public static String startingTheP0importModule;

	public static String importDidNotFinishedItEncounteredTheFollowingErrorP0;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, GroovyMessages.class);
	}
}
