package org.txm.groovy.core;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.txm.core.engines.ScriptEngine;

public class TXMGroovyScriptEngine extends ScriptEngine {

	public TXMGroovyScriptEngine() {
		super("groovy", "groovy"); //$NON-NLS-1$
	}

	@Override
	public boolean isRunning() {
		return GSERunner.defaultGSE != null;
	}

	@Override
	public boolean initialize() throws Exception {
		return GSERunner.buildDefaultGSE() != null;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		GSERunner.clearDefaultGSE();
		return true;
	}

	@Override
	public IStatus executeScript(File script, HashMap<String, Object> env, List<String> stringArgs) {
		// move Job code in the ExecuteGroovyScript here
		return Status.CANCEL_STATUS;
	}

	@Override
	public IStatus executeText(String text, HashMap<String, Object> env) {
		return Status.CANCEL_STATUS;
	}
}
