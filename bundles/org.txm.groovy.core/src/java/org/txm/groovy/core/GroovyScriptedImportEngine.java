package org.txm.groovy.core;

import java.io.File;
import java.util.logging.Level;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.txm.Toolbox;
import org.txm.core.engines.Engine;
import org.txm.core.engines.EngineType;
import org.txm.core.engines.ScriptedImportEngine;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.groovy.core.messages.GroovyMessages;
import org.txm.nlp.core.NLPEngine;
import org.txm.objects.Project;
import org.txm.objects.Workspace;
import org.txm.tokenizer.TokenizerClasses;
import org.txm.utils.DeleteDir;
import org.txm.utils.ExecTimer;
import org.txm.utils.logger.Log;

import groovy.lang.Binding;
import groovy.lang.Script;

public class GroovyScriptedImportEngine extends ScriptedImportEngine {

	File importer, compiler, pager, annotate;

	public GroovyScriptedImportEngine(File directory) {
		super(directory.getName(), new File(directory, directory.getName() + "Loader.groovy"));
		importer = new File(directory, "importer.groovy");
		compiler = new File(directory, "compiler.groovy");
		pager = new File(directory, "pager.groovy");
		annotate = new File(directory, "annotate.groovy");
	}

	@Override
	public IStatus _build(Project project, IProgressMonitor monitor) {

		try {
			// check if TreeTagger is ready
			Workspace w = Toolbox.workspace;
			boolean annotate = project.getAnnotate();
			if (annotate && !project.getDoUpdate()) { // test TreeTagger only if not updating the corpus
				Engine e = Toolbox.getEngineManager(EngineType.NLP).getEngine("TreeTagger");
				if (e == null) {
					Log.warning("Error: no TreeTagger annotation engine found to annotate the corpus. Aborting");
					return Status.CANCEL_STATUS;
				}
				NLPEngine engine = (NLPEngine) e;
				if (!engine.isRunning()) {
					Log.warning(GroovyMessages.errorTreeTaggerAnnotationEngineIsNotReady);
					return Status.CANCEL_STATUS;
				}
			}

			if (project.getDoUpdate()) {
				// drop CorpusBuild and Editions
				// List<? extends CorpusBuild> corpora = project.getCorpora();
				// while (project.getCorpora().size() > 0) {
				// CorpusBuild c = project.getCorpora().get(0);
				// c.delete();
				// }
				// List<Text> texts = project.getTexts();
				// for (Text t : texts) {
				// while (t.getEditions().size() > 0) {
				// Edition ed = t.getEditions().get(0);
				// ed.delete();
				// }
				// }
			}
			else {
				// clear all children
				while (project.getChildren().size() > 0) {
					TXMResult child = project.getChildren().get(0);
					child.delete();
				}

				// clear files
				if (project.getProjectDirectory().exists()) {
					for (File f : project.getProjectDirectory().listFiles()) {
						if (".settings".equals(f.getName())) continue;
						if (".project".equals(f.getName())) continue;
						if (f.isDirectory()) {
							DeleteDir.deleteDirectory(f);
						}
						else {
							f.delete();
						}
					}
				}
				// System.out.println("TXM FILES: " + Arrays.asList(new File(project.getProjectDirectory(), "txm/" + project.getName()).list()));
			}

			// the binary directory which is going to be created

			// final File basedir = project.getProjectDirectory();
			// final File tempBinDirectory = new File(basedir.getAbsolutePath()+"_temp"); //$NON-NLS-1$
			// final File errorBinDirectory = new File(basedir.getAbsolutePath()+"_error"); //$NON-NLS-1$
			// if (tempBinDirectory.exists()) DeleteDir.deleteDirectory(tempBinDirectory);
			// if (errorBinDirectory.exists()) DeleteDir.deleteDirectory(errorBinDirectory);

			// this.setCurrentMonitor(monitor);
			monitor.beginTask("Importing sources " + project.getSrcdir(), 100); //$NON-NLS-1$

			// org.txm.Toolbox.shutdownSearchEngine();
			monitor.worked(5);

			GSERunner gse = GSERunner.buildDefaultGSE();

			Binding binding = new Binding();
			binding.setProperty("projectBinding", project); //$NON-NLS-1$
			binding.setProperty("monitor", new GroovyProgressMonitor(monitor)); //$NON-NLS-1$

			boolean logLevel = Log.getLevel().intValue() < Level.INFO.intValue();
			binding.setProperty("debug", logLevel); //$NON-NLS-1$
			binding.setProperty("readyToLoad", Boolean.FALSE); //$NON-NLS-1$
			// System.out.println("script="+script);

			if (!new TokenizerClasses().loadFromProject(project.getPreferencesScope())) {
				Log.severe("Error: failed to load tokenizer parameters");
				monitor.done();
				return Status.CANCEL_STATUS;
			}

			ExecTimer.start();
			Log.info(NLS.bind("Compiling the \"{0}\" import module scripts...", project.getImportModuleName()));
			Script s = gse.createScript(mainScript.toURI().toURL().toString(), binding);
			Log.info(NLS.bind(GroovyMessages.startingTheP0importModule, project.getImportModuleName()));
			s.run(); // run the groovy import script
			monitor.worked(90);

			Object ready = binding.getVariable("readyToLoad"); //$NON-NLS-1$
			if (ready != null && ready instanceof Boolean) {
				Boolean readyToLoad = (Boolean) ready;
				// System.out.println("TRY TO LOAD THE BINARY CORPUS CREATED: "+basedir);
				if (readyToLoad) {

					project.saveParameters(true);

					if (monitor != null) monitor.setTaskName(TXMCoreMessages.common_done);

					// Log.info(GroovyMessages.restartingToolboxSearchengines);
					// Toolbox.getEngineManager(EngineType.SEARCH).restartEngines();
					//					if (project.getDoUpdate()) {
					//						System.out.println(NLS.bind(TXMCoreMessages.corpusUpdateDoneInP0, ExecTimer.stop()));
					//					}
					//					else {
					//						System.out.println(NLS.bind(TXMCoreMessages.corpusImportDoneInP0, ExecTimer.stop()));
					//					}

				}
				else {
					Log.fine(GroovyMessages.errorImportNotCorrectlyEndedSeeConsoleMessages);

					if (!project.getDoUpdate()) { // clear all
						project.deleteChildren();
					}

					// if (basedir.exists()) basedir.renameTo(errorBinDirectory);
					// if (tempBinDirectory.exists()) tempBinDirectory.renameTo(basedir);
					return new Status(Status.ERROR, "org.txm.groovy.core", GroovyMessages.errorImportNotCorrectlyEndedSeeConsoleMessages);
				}
			}
			else {
				Log.fine(GroovyMessages.errorImportNotCorrectlyEndedSeeConsoleMessages);
				return new Status(Status.ERROR, "org.txm.groovy.core", GroovyMessages.errorImportNotCorrectlyEndedSeeConsoleMessages);
			}
		}
		catch (ThreadDeath td) {
			Log.warning("Import aborted.");
			return Status.CANCEL_STATUS;
		}
		catch (Exception e) {
			// System.out.println(NLS.bind(TXMUIMessages.errorWhileRunningScriptColonP0, e));
			Log.printStackTrace(e);
			Log.warning(GroovyMessages.bind(GroovyMessages.importDidNotFinishedItEncounteredTheFollowingErrorP0, e.getMessage()));

			return new Status(Status.ERROR, e.getClass().getCanonicalName(), e.getMessage());
		}
		finally {
			// if (!CQPSearchEngine.isInitialized()) { // the import failed
			// monitor.setTaskName(GroovyMessages.restartingToolboxSearchengines); //$NON-NLS-1$
			// Toolbox.getEngineManager(EngineType.SEARCH).restartEngines();
			// }
		}
		System.gc();
		return Status.OK_STATUS;
	}
}
