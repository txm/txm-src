package org.txm.groovy.core;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.util.NLS;
import org.txm.PostTXMHOMEInstallationStep;
import org.txm.Toolbox;
import org.txm.objects.Workspace;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.FileCopy;
import org.txm.utils.logger.Log;

/**
 * Copy all Groovy scripts in TXMHome directory
 * 
 * @author mdecorde
 *
 */
public class InstallGroovyFiles extends PostTXMHOMEInstallationStep {

	final static String createfolders[] = { "scripts/groovy/lib", "scripts/groovy/user", "scripts/groovy/system" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	@Override
	public boolean do_install(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());

		// String version = GroovyPreferences.getInstance().getString(GroovyPreferences.VERSION);
		//
		// boolean doInstall = false;
		// Version installedVersion = BundleUtils.getBundleVersion("org.txm.groovy.core");
		// if (version != null && version.length() > 0) {
		// Version v = new Version(version);
		//
		// doInstall = v.compareTo(installedVersion) > 0;
		// Log.fine("Groovy installed version: "+installedVersion+" preferenced version: "+v+" -> "+doInstall);
		// } else {
		// doInstall = true;
		// GroovyPreferences.getInstance().put(GroovyPreferences.VERSION, installedVersion.toString());
		// }
		//
		// if (!doInstall) return true; // nothing to do

		for (String folder : createfolders) {
			new File(txmhomedir, folder).mkdirs();
		}
		File scriptsDirectory = new File(txmhomedir, "scripts/groovy"); //$NON-NLS-1$
		File userDirectory = new File(scriptsDirectory, "user"); //$NON-NLS-1$
		File systemDirectory = new File(scriptsDirectory, "system"); //$NON-NLS-1$
		Log.fine("Installing Groovy files to " + userDirectory); //$NON-NLS-1$

		// HashMap<String, String> publishedImports = new HashMap<String, String>();
		// String IMPORTERPACKAGE="org/txm/scriptsimporter/";
		// publishedImports.put(IMPORTERPACKAGE+"alceste","alcesteLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"discours","discoursLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"bfm","bfmLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"frantext","frantextLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"hyperbase","hyperbaseLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"txt","txtLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"xml","xmlLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"xtz","xtzLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"factiva","factivaLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"factiva","factivamailLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"xmltxm","xmltxmLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"transcriber","transcriberLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"tmx","tmxLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"doc","docLoader.groovy");
		// publishedImports.put(IMPORTERPACKAGE+"cqp","cqpLoader.groovy");
		//
		// for (String p : publishedImports.keySet()) {
		// BundleUtils.copyFiles(bundle_id, "src/", "groovy/"+p, publishedImports.get(p), importDirectory);
		// }
		//
		// // copy published macros
		// macroDirectory.mkdirs();
		// BundleUtils.copyFiles(bundle_id, "src/", "groovy/org/txm", "macro/", macroDirectory);
		//
		// // copy sample Groovy scripts
		// BundleUtils.copyFiles(bundle_id, "src/", "groovy/org/txm", "export/", samplesDirectory);
		// BundleUtils.copyFiles(bundle_id, "src/", "groovy/org/txm", "functions/", samplesDirectory);
		// BundleUtils.copyFiles(bundle_id, "src/", "groovy/org/txm", "importer/", samplesDirectory);
		// BundleUtils.copyFiles(bundle_id, "src/", "groovy/org/txm", "sw/", samplesDirectory);
		// BundleUtils.copyFiles(bundle_id, "src/", "groovy/org/txm", "tal/", samplesDirectory);


		String bundle_id = "org.txm.groovy.core"; //$NON-NLS-1$

		// copy modified files in the backup directory
		backupFiles(userDirectory, systemDirectory, userDirectory.getParentFile());

		BundleUtils.copyFiles(bundle_id, "", "src/groovy", "", userDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		try {
			Thread.sleep(1000);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BundleUtils.copyFiles(bundle_id, "", "src/groovy", "", systemDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		bundle_id = "org.txm.core"; //$NON-NLS-1$

		// copy additional Groovy files from the src/java directory of 
		File scriptsPackageDirectory = new File(systemDirectory, "org/txm/scripts/importer"); //$NON-NLS-1$
		scriptsPackageDirectory.mkdirs();
		BundleUtils.copyFiles(bundle_id, "src/java/", "org/txm/scripts/importer", "", scriptsPackageDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		try {
			Thread.sleep(1000);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		scriptsPackageDirectory = new File(userDirectory, "org/txm/scripts/importer"); //$NON-NLS-1$
		scriptsPackageDirectory.mkdirs();
		BundleUtils.copyFiles(bundle_id, "src/java/", "org/txm/scripts/importer", "", scriptsPackageDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		return scriptsDirectory.exists();
	}

	public static void backupFiles(File directorytoBackUp, File referenceDirectory, File parentFiletoUse) {

		if (Platform.inDevelopmentMode()) return; // don't recopy the groovy files in dev mode

		Log.fine("Backing up modified Groovy files of " + directorytoBackUp + " VS " + referenceDirectory + " to " + parentFiletoUse);
		File backUserDirectory = new File(parentFiletoUse, directorytoBackUp.getName() + "-" + Toolbox.dateformat.format(new Date()));
		int nBackupedFiles = 0;

		//		System.out.println("update time: "+updateTime+" of "+bundle_id);
		for (File f : DeleteDir.scanDirectory(directorytoBackUp, true, true)) {

			if (f.isFile() && f.getName().endsWith(".groovy")) { // backup the file if it was modified by the user

				String path = f.getAbsolutePath();
				String subpath = path.substring(directorytoBackUp.getAbsolutePath().length());
				File backGroovy = new File(backUserDirectory, subpath);
				File refGroovy = new File(referenceDirectory, subpath);

				if (!refGroovy.exists() || refGroovy.lastModified() < f.lastModified()) {
					Log.fine(NLS.bind("Backing {0} -> {1}", f, backGroovy));

					try {
						backGroovy.getParentFile().mkdirs();
						FileCopy.copy(f, backGroovy);
						nBackupedFiles++;
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


				}
			}
		}
		if (nBackupedFiles > 0) {
			Log.info(NLS.bind("Some modified Groovy files have been saved in the {0} directory ({1})", backUserDirectory, nBackupedFiles));
		}

	}

	@Override
	public String getName() {
		return "Groovy (org.txm.groovy.core)"; //$NON-NLS-1$
	}

	@Override
	public boolean needsReinstall(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());
		for (String folder : createfolders) {
			if (!new File(txmhomedir, folder).exists()) {
				return true;
			}
		}
		return false;
	}
}
