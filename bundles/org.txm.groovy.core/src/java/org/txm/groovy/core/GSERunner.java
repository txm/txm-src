package org.txm.groovy.core;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.eclipse.core.internal.runtime.InternalPlatform;
import org.eclipse.osgi.internal.loader.EquinoxClassLoader;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.wiring.BundleWiring;
import org.txm.Toolbox;
import org.txm.utils.BundleUtils;
import org.txm.utils.DeleteDir;
import org.txm.utils.logger.Log;

import cern.colt.Arrays;
import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

/**
 * GroovyScriptEngine wrapper for TXM scripts and macros
 * 
 * @author mdecorde
 *
 */
public class GSERunner extends GroovyScriptEngine {

	protected GSERunner(String[] urls) throws IOException {
		super(urls, getTXMClassLoader());
	}

	protected static GSERunner defaultGSE;

	protected static File defaultScriptRootDir;

	public static void clearDefaultGSE() {
		defaultGSE = null;
		defaultScriptRootDir = null;
	}

	/**
	 * Build a GSERunner with default paths : user, macro, import and library path
	 * 
	 * @return
	 */
	public static GSERunner buildDefaultGSE() {
		File rootDir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/"); //$NON-NLS-1$
		return buildDefaultGSE(rootDir);
	}

	/**
	 * Build a GSERunner with default paths : user, macro, import and library path
	 * 
	 * @param rootDir the root directory of the Groovy files
	 * @return
	 */
	public static GSERunner buildDefaultGSE(File rootDir) {

		// check if script root dir was changed
		if (defaultScriptRootDir != null && defaultScriptRootDir.equals(rootDir) && defaultGSE != null) {
			// try {
			// defaultGSE.getGroovyClassLoader().addURL(rootDir.toURI().toURL());
			// } catch (MalformedURLException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			return defaultGSE;
		}

		String[] roots = new String[] {
				rootDir.getAbsolutePath() + "/user/", //$NON-NLS-1$
				// rootDir.getAbsolutePath()+"/system/", //$NON-NLS-1$
		};

		try {
			defaultScriptRootDir = rootDir;
			Log.fine("GSE roots: " + Arrays.toString(roots)); //$NON-NLS-1$
			defaultGSE = new GSERunner(roots);
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		ImportCustomizer imports = new ImportCustomizer();
		imports.addStarImports("org.txm.rcp.utils", //$NON-NLS-1$
				"org.txm.utils", //$NON-NLS-1$
				"org.txm", //$NON-NLS-1$
				"org.kohsuke.args4j", //$NON-NLS-1$
				"org.txm.rcp.swt.widget.parameters"); //$NON-NLS-1$
		imports.addImports("groovy.transform.Field"); //$NON-NLS-1$
		CompilerConfiguration configuration = new CompilerConfiguration();
		configuration.addCompilationCustomizers(imports);

		defaultGSE.setConfig(configuration);

		// FIXME MD classes file are created but the GSE don't read from it and rewrite the class files
		//		File classesDir = new File(Toolbox.getTxmHomePath()+"/scripts/groovy/classes");
		//		classesDir.mkdirs();
		//		configuration.setTargetDirectory(classesDir);
		//		configuration.setRecompileGroovySource(false);
		//		defaultGSE.reloadCaches();	

		reloadLibraries();

		// for the .class files if any
		defaultGSE.getGroovyClassLoader().addClasspath(rootDir.getAbsolutePath());

		return defaultGSE;
	}

	public static void reloadLibraries() {

		if (defaultGSE == null) {
			Log.severe("GSERunner not initialized");
			return;
		}

		File rootDir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/"); //$NON-NLS-1$

		File jardir = new File(rootDir, "lib"); //$NON-NLS-1$
		try {
			defaultGSE.getGroovyClassLoader().addClasspath(jardir.getAbsolutePath());
			defaultGSE.getGroovyClassLoader().addURL(jardir.toURI().toURL());
		}
		catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (jardir.exists() && jardir.isDirectory()) {
			for (File f : jardir.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".jar") || name.endsWith(".so") || name.endsWith(".dylib") || name.endsWith(".dll"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				}
			})) {
				try {
					defaultGSE.getGroovyClassLoader().addURL(f.toURI().toURL());
				}
				catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * reload classes from the target directory in the sourceCache
	 */
	public void reloadCaches() {
		GroovyClassLoader gcl = this.getGroovyClassLoader();
		// get field sourceCache
		Field field;
		try {
			field = gcl.getClass().getSuperclass().getDeclaredField("sourceCache"); //$NON-NLS-1$
			field.setAccessible(true);
			Map<String, Class<?>> sourceCache = (Map<String, Class<?>>) field.get(gcl);

			File target = this.getConfig().getTargetDirectory();
			URLClassLoader classLoader = new URLClassLoader(new URL[] { target.toURI().toURL() }, Thread.currentThread().getContextClassLoader());

			for (File clazzFile : DeleteDir.scanDirectory(target, true, true)) {
				if (!clazzFile.getName().endsWith(".class")) continue; //$NON-NLS-1$
				String fullclassname = clazzFile.getAbsolutePath().substring(target.getAbsolutePath().length() + 1);
				fullclassname = fullclassname.replace("/", "."); //$NON-NLS-1$ //$NON-NLS-2$
				fullclassname = fullclassname.replace("\\", "."); //$NON-NLS-1$ //$NON-NLS-2$
				fullclassname = fullclassname.substring(0, fullclassname.length() - 6);
				Class<?> clazz = Class.forName(fullclassname, true, classLoader);
				sourceCache.put(clazzFile.getAbsolutePath(), clazz);
			}

			//this.getConfig().setTargetDirectory((File)null);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Object evaluate(String code) {
		return new GroovyShell(this.getGroovyClassLoader(), new Binding()).evaluate(code);
	}

	public static void getDependancies(String bundle, HashSet<String> defaultPlugins) {
		// if (defaultPlugins.contains(bundle)) return;

		Bundle rcpBundle = BundleUtils.getBundle(bundle);
		if (rcpBundle == null) return;
		String dependanciesString = rcpBundle.getHeaders().get("Require-Bundle"); //$NON-NLS-1$
		if (dependanciesString == null) return;
		// System.out.println("Require-Bundle: "+dependanciesString);
		for (String dependancy : dependanciesString.split(",")) {
			String name = dependancy.split(";", 2)[0]; //$NON-NLS-1$
			defaultPlugins.add(name);
			getDependancies(name, defaultPlugins);
		}
	}

	/**
	 * 
	 * @return a class loader containing all bundles activated in TXM
	 */
	protected static ClassLoader getTXMClassLoader() {
		HashSet<EquinoxClassLoader> loaders = new HashSet<>();
		BundleContext bundleContext = InternalPlatform.getDefault().getBundleContext();
		Bundle[] bundles = bundleContext.getBundles();
		// java.util.Arrays.sort(bundles, new Comparator<Bundle>() {
		//
		// @Override
		// public int compare(Bundle o1, Bundle o2) {
		// if (o1.getSymbolicName().startsWith("org.txm")) return 1;
		// return -1;
		// }
		// });
		//HashSet<String> defaultPlugins = new HashSet<>();
		// getDependancies("org.txm.rcp", defaultPlugins);

		ClassLoader defaultLoader = EquinoxClassLoader.getSystemClassLoader();

		for (Bundle b : bundles) {
			if (b == null) continue;
			String name = b.getSymbolicName();
			//if (!name.startsWith("org.txm")) continue; // only keep txm plugins (they contains the necessary plugins deps)
			//if (name.endsWith(".core")) continue; // usually TXM *.rcp plugins depends on the *.core plugins
			//if (defaultPlugins.contains(name)) continue;

			BundleWiring bundleWiring = b.adapt(BundleWiring.class);
			if (bundleWiring == null) {
				// System.out.println("GSERunner.createGSERunner(): NO wiring: " + b.getSymbolicName());
			}
			else if (bundleWiring.getClassLoader() != null && bundleWiring.getClassLoader() instanceof EquinoxClassLoader) {
				EquinoxClassLoader loader = (EquinoxClassLoader) bundleWiring.getClassLoader();

				loaders.add(loader);
				if (loaders.contains(loader.getParent())) {
					loaders.remove(loader.getParent());
				}

				if ("org.txm.rcp".equals(name)) { //$NON-NLS-1$
					defaultLoader = loader;
				}
				// FIXME: debug
				// System.out.println("GSERunner.createGSERunner(): ADD class loader: " + b.getSymbolicName());
			}
			else {
				// System.out.println("GSERunner.createGSERunner(): NO class loader: " + b.getSymbolicName());
			}
		}

		Log.fine("Initializing TXMClassLoader with " + loaders.size() + " bundles.");
		// for (ClassLoader l : loaders) System.out.println(l);
		return new TXMClassLoader(defaultLoader, loaders);
	}

	/**
	 * 
	 * Simplify the call a Groovy script within a Groovy script and allow to send named and typed arguments
	 * 
	 * @param script
	 * @param binding
	 * @return
	 * @throws MalformedURLException
	 * @throws ResourceException
	 * @throws ScriptException
	 */
	@SuppressWarnings("rawtypes")
	public Object run(File script, Binding binding) throws MalformedURLException, ResourceException, ScriptException {
		String scriptName = script.toURI().toURL().toString();
		// Class clazz = this.loadScriptByName(scriptName);
		//
		// Script sc = InvokerHelper.createScript(clazz, binding);
		return super.run(scriptName, binding);
	}

	/**
	 * 
	 * Simplify the call a Groovy script within a Groovy script and allow to send named and typed arguments
	 * 
	 * @param clazz
	 * @param bindingArgs
	 * @return
	 * @throws ResourceException
	 * @throws ScriptException
	 */
	@SuppressWarnings("rawtypes")
	public Object run(Class clazz, Map bindingArgs) throws ResourceException, ScriptException {
		return run(clazz.getCanonicalName().replace(".", "/") + ".groovy", new Binding(bindingArgs));
	}

	/**
	 * Convenience method exactly the same as calling
	 * run(clazz, ["args":args])
	 * 
	 * @param clazz
	 * @param args
	 * @return
	 * @throws ResourceException
	 * @throws ScriptException
	 */
	@SuppressWarnings("rawtypes")
	public Object runMacro(Class clazz, Map args, Map defaultArgs) throws ResourceException, ScriptException {
		HashMap<String, Object> arrangedMap = new HashMap<>();
		if (args != null) {
			arrangedMap.put("args", args);
		}
		if (defaultArgs != null) {
			arrangedMap.put("defaultArgs", defaultArgs);
		}


		// Find the Groovy Script
		/*
		 * Binding binding = null;
		 * if (bean instanceof groovy.lang.Script) {
		 * groovy.lang.Script script = (groovy.lang.Script) bean;
		 * // if the "args" variable exists then use its values. Can be used to set a default value by another Macro
		 * if (script.getBinding().hasVariable("args")) {
		 * oArgs = script.getBinding().getVariable("args"); //$NON-NLS-1$
		 * }
		 * }
		 */

		return run(clazz.getCanonicalName().replace(".", "/") + ".groovy", new Binding(arrangedMap));
	}

	/**
	 * Convenience method exactly the same as calling
	 * run(clazz, ["args":args])
	 * 
	 * @param clazz
	 * @param args
	 * @return
	 * @throws ResourceException
	 * @throws ScriptException
	 */
	@SuppressWarnings("rawtypes")
	public Object runMacro(Class clazz, Map args) throws ResourceException, ScriptException {
		return runMacro(clazz, args, null);
	}

	/**
	 * Convenience method exactly the same as calling
	 * run(clazz, ["args":[:]])
	 * 
	 * @param clazz
	 * @return
	 * @throws ResourceException
	 * @throws ScriptException
	 */
	@SuppressWarnings("rawtypes")
	public Object runMacro(Class clazz) throws ResourceException, ScriptException {
		//		HashMap<String, Object> arrangedMap = new HashMap<>();
		//		arrangedMap.put("args", new HashMap<String, Object>());
		//		return run(clazz.getCanonicalName().replace(".", "/") + ".groovy", new Binding(arrangedMap));
		return runMacro(clazz, null, null);
	}

	//// TODO ScriptCacheEntry is a private class...
	// public boolean needToRecompile(String scriptName) throws ResourceException, ScriptException {
	//
	// URLConnection conn = this.getResourceConnection(scriptName);
	// String path = conn.getURL().toExternalForm();
	// ScriptCacheEntry entry = null;
	// Class clazz = null;
	// if (entry != null) clazz = entry.scriptClass;
	// try {
	// return isSourceNewer(entry);
	//
	// }
	// catch(Exception e) {
	//
	// }
	//
	// }
	// }
}
