package org.txm.groovy.core;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class GroovyPreferences extends TXMPreferences {

	//FIXME: SJ: useless?
	public static final String PREFERENCES_NODE = GroovyPreferences.class.getName();// FrameworkUtil.getBundle(RPreferences.class).getSymbolicName();

	public static final String VERSION = "version"; //$NON-NLS-1$

	public static final String FAST_PACKAGE_INDEX = "fast_package_index";

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(GroovyPreferences.class)) {
			new GroovyPreferences();
		}
		return TXMPreferences.instances.get(GroovyPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putBoolean(FAST_PACKAGE_INDEX, true);
	}
}
