/**
 * 
 */
package org.txm.chartsengine.graphstream.rcp.swt;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.DefaultView;
import org.graphstream.ui.view.Viewer;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.graphstream.rcp.swing.GSChartComponent;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.swt.SwingChartComposite;

/**
 * GraphStream chart composite.
 * 
 * @author sjacquot
 *
 */
public class GSComposite extends SwingChartComposite {

	/**
	 * @param chartEditor
	 * @param parent
	 */
	public GSComposite(ChartEditor<ChartResult> chartEditor, Composite parent) {
		super(chartEditor, parent, SWT.EMBEDDED);
		// TODO Auto-generated constructor stub



		// JPanel pane1 = new JPanel();
		// Viewer v = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
		// v.a

		// v.enableAutoLayout();
		// ViewPanel view = v.addDefaultView(false);
		// pane1.setLayout(new BorderLayout());
		// pane1.add(view,BorderLayout.CENTER);
	}

	@Override
	public void loadChart(Object data) {
		Graph graph = (Graph) data;

		// System.err.println("GSComposite.loadChart(): " + graph.getId());
		//
		// if(graph.getId().equals("dummy")) {

		// Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
		Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
		viewer.enableAutoLayout();
		//// //v.enableXYZfeedback(true);
		// viewer.removeView(Viewer.DEFAULT_VIEW_ID);
		viewer.addView(((DefaultView) this.chartComponent));

		((GSChartComponent) this.chartComponent).reinit(viewer);

		// }
		// FIXME: tests (opening a Frame with the graph but be careful, closing the Frame closes the entire RCP app)
		//graph.display();
	}

	@Override
	public void clearChartItemsSelection() {
		// TODO Auto-generated method stub

	}

	@Override
	protected IChartComponent _createChartComponent() {

		// System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer"); // Pour les fonctionnalités avancées des CSS

		// creating a dummy graph (the viewer can not have a null graph)
		Graph graph = new SingleGraph("dummy"); //$NON-NLS-1$

		// FIXME: tests
		// Generator gen = new DorogovtsevMendesGenerator();
		// gen.addSink(graph);
		// gen.begin();
		// for(int i=0; i<500; i++) {
		// gen.nextEvents();
		// }
		// gen.end();
		//
		// for (Node node : graph) {
		// node.addAttribute("ui.label", node.getId());
		// //node.setAttribute("ui.size", 5);
		//// node.addAttribute("ui.style", "fill-color: 0.5; size: 5px;");
		// }
		//
		// //graph.display();

		// Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
		Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
		viewer.enableAutoLayout();
		// graph.display();
		GSChartComponent chartComponent = new GSChartComponent(viewer);
		viewer.addView(chartComponent);

		return chartComponent;
	}

	@Override
	public File exportView(File file, String fileFormat) {
		System.out.println("GSComposite.exportView(): not yet implemented."); //$NON-NLS-1$
		return null;
	}

	@Override
	public ArrayList<String> getEditorSupportedExportFileFormats() {
		return this.getChartsEngine().getSupportedOutputFileFormats();
	}

	@Override
	public void initEventsListeners() {
		// TODO Auto-generated method stub

	}



}
