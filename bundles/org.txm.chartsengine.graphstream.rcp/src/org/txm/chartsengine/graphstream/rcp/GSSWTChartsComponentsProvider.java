/**
 * 
 */
package org.txm.chartsengine.graphstream.rcp;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.txm.chartsengine.graphstream.rcp.swt.GSComposite;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.swt.ChartComposite;

/**
 * Graphstream library charts components provider.
 * 
 * @author sjacquot
 *
 */
public class GSSWTChartsComponentsProvider extends SWTChartsComponentsProvider {

	/**
	 * Default constructor.
	 */
	public GSSWTChartsComponentsProvider() {
		super();
	}

	@Override
	public ChartComposite createComposite(ChartEditor chartEditor, Composite parent) {
		return new GSComposite(chartEditor, parent);
	}

	@Override
	public ArrayList<String> getChartsEngineSupportedExportFileFormats() {
		return this.chartsEngine.getSupportedOutputFileFormats();
	}


}
