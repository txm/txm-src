package org.txm.chartsengine.graphstream.rcp.handler;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.txm.Toolbox;
import org.txm.chartsengine.graphstream.rcp.editor.GSEditor;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.SWTEditorsUtils;

public class OpenGraphFile extends BaseAbstractHandler {

	public static String ID = OpenGraphFile.class.getName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		String txmhome = Toolbox.getTxmHomePath();

		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		if (LastOpened.getFile(ID) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ID));
			dialog.setFileName(LastOpened.getFile(ID));
		}
		else {
			dialog.setFilterPath(txmhome);
		}
		if (dialog.open() == null) return null;
		String filepath = dialog.getFilterPath() + "/" + dialog.getFileName(); //$NON-NLS-1$
		LastOpened.set(ID, dialog.getFilterPath(), dialog.getFileName());

		try {
			return open(new File(filepath));
		}
		catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static GSEditor open(File graphFile) throws PartInitException {
		IWorkbenchWindow window = TXMWindows.getActiveWindow();
		IWorkbenchPage page = window.getActivePage();

		IFileStore fileOnLocalDisk = EFS.getLocalFileSystem().getStore(graphFile.toURI());

		FileStoreEditorInput editorInput = new FileStoreEditorInput(fileOnLocalDisk);

		IEditorPart editor = SWTEditorsUtils.openEditor(editorInput, GSEditor.class.getName(), true, IWorkbenchPage.MATCH_INPUT | IWorkbenchPage.MATCH_ID);
		if (editor != null && editor instanceof GSEditor) {
			return (GSEditor) editor;
		}
		else {
			return null;
		}
	}
}
