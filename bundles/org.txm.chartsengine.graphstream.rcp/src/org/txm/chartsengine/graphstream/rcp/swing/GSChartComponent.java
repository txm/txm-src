/**
 * 
 */
package org.txm.chartsengine.graphstream.rcp.swing;

import java.awt.event.MouseEvent;

import org.graphstream.ui.swingViewer.DefaultView;
import org.graphstream.ui.view.Viewer;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;

/**
 * GraphStream Chart Component.
 * 
 * @author sjacquot
 *
 */
public class GSChartComponent extends DefaultView implements IChartComponent {

	private static final long serialVersionUID = -7185771795063103912L;

	protected double zoomFactor = 0.1;


	/**
	 * 
	 * @param viewer
	 */
	public GSChartComponent(Viewer viewer) {
		super(viewer, Viewer.DEFAULT_VIEW_ID, Viewer.newGraphRenderer());
	}

	/**
	 * 
	 * @param viewer
	 */
	public void reinit(Viewer viewer) {
		if (this.viewer != null) {
			this.viewer.close();
		}
		this.viewer = viewer;
		this.graph = viewer.getGraphicGraph();
		this.renderer = Viewer.newGraphRenderer();

		setOpaque(false);
		setDoubleBuffered(true);
		setMouseManager(null);
		setShortcutManager(null);
		this.renderer.open(this.graph, this);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.rcp.IChartComponent#zoom(double, double, boolean)
	 */
	@Override
	public void zoom(double x, double y, boolean zoomIn, boolean range, boolean domain) {
		
		// TODO range and domain are not implemented yet
		double percent = zoomFactor;
		if (zoomIn) {
			percent = -percent;
		}
		this.getCamera().setViewPercent(this.getCamera().getViewPercent() + percent);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.rcp.IChartComponent#resetView()
	 */
	@Override
	public void resetView() {
		this.getCamera().setViewPercent(1);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.rcp.IChartComponent#pan(double, double, double, double, double)
	 */
	@Override
	public void pan(double srcX, double srcY, double dstX, double dstY, double panFactor) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.rcp.IChartComponent#getChartEditor()
	 */
	@Override
	public ChartEditor<? extends ChartResult> getChartEditor() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.rcp.IChartComponent#setChartEditor(org.txm.chartsengine.rcp.editors.ChartEditor)
	 */
	@Override
	public void setChartEditor(ChartEditor editor) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.rcp.IChartComponent#updateMouseOverItem(java.awt.event.MouseEvent)
	 */
	@Override
	public void updateMouseOverItem(MouseEvent event) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.chartsengine.rcp.IChartComponent#squareOff()
	 */
	@Override
	public void squareOff() {
		// TODO Auto-generated method stub

	}

	// /* (non-Javadoc)
	// * @see org.txm.chartsengine.rcp.IChartComponent#setSquareOffEnabled(boolean)
	// */
	// @Override
	// public void setSquareOffEnabled(boolean enabled) {
	// // TODO Auto-generated method stub
	//
	// }


	public Viewer getViewer() {
		return this.viewer;
	}


}
