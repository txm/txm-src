package org.txm.chartsengine.graphstream.rcp.editor;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceFactory;
import org.graphstream.stream.file.FileSourceGraphML;
import org.txm.chartsengine.graphstream.rcp.swt.GSComposite;
import org.txm.utils.logger.Log;

public class GSEditor extends EditorPart {

	private GSComposite main;

	private Button loadButton;

	public GSEditor() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		this.setSite(site);
		this.setInput(input);
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(1, true));

		loadButton = new Button(parent, SWT.PUSH);
		loadButton.setText("Load Graph file...");
		loadButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				FileDialog dialog = new FileDialog(e.display.getActiveShell(), SWT.OPEN);
				dialog.setFilterExtensions(new String[] { "*.*", "*.dot", "*.gml", "*.gexf", "*.graphml" }); //$NON-NLS-1$
				String path = dialog.open();
				if (path == null) {
					return;
				}

				Graph g = new DefaultGraph("g"); //$NON-NLS-1$ //$NON-NLS-1$

				try {
					FileSource fs = FileSourceFactory.sourceFor(path);
					if (path.endsWith(".graphml")) fs = new FileSourceGraphML(); //$NON-NLS-1$

					if (fs != null) {
						fs.addSink(g);
						try {
							fs.readAll(path);
							main.loadChart(g);
							main.redraw();
						}
						catch (Throwable e1) {
							e1.printStackTrace();
							return;
						}
						finally {
							fs.removeSink(g);
						}
					}
					else {
						Log.warning("File format not supported: " + path);
					}
				}
				catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		main = new GSComposite(null, parent);
		main.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
	}

	@Override
	public void setFocus() {
		loadButton.setFocus();
	}
}
