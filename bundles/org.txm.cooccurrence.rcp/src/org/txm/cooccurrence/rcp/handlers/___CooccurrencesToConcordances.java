// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.cooccurrence.rcp.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.concordance.core.functions.Concordance;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.cooccurrence.core.functions.Cooccurrence.CLine;
import org.txm.cooccurrence.rcp.editors.CooccurrencesEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

// TODO: Auto-generated Javadoc
/**
 * use the lines of a cooc result to build a concordance @ author mdecorde.
 */
public class ___CooccurrencesToConcordances extends AbstractHandler {

	/** The selection. */
	private IStructuredSelection selection;
	
	/** The editor input. */
	private static TXMResultEditorInput editorInput;

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);

		CooccurrencesEditor ceditor = null;
		IEditorPart editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActiveEditor();
		ceditor = (CooccurrencesEditor) editor;
		
		link(ceditor, selection);
		return null;
	}
	
	/**
	 * Link.
	 *
	 * @param editor the editor
	 * @param selection the selection
	 */
	public static void link(CooccurrencesEditor editor, IStructuredSelection selection)
	{

		assert (selection.getFirstElement() instanceof CLine);

		Cooccurrence cooc = ((CLine) selection.getFirstElement()).getCooc();
		
		int nbProps = cooc.getProperties().size();
		List<WordProperty> props = cooc.getProperties();
		List<CLine> list = selection.toList();
		//System.out.println("Cooc lines "+list);

		String query = "["; //$NON-NLS-1$
		for (int p = 0; p < nbProps; p++) {
			query += props.get(p) + "=\""; //$NON-NLS-1$
			for (int l = 0; l < list.size(); l++) {
				CLine line = list.get(l);
				String s = line.props.get(p);
				s = CQLQuery.addBackSlash(s);
				query += s + "|"; //$NON-NLS-1$
			}
			query = query.substring(0, query.length() - 1);
			query += "\" & "; //$NON-NLS-1$
		}
		query = query.substring(0, query.length() - 3);
		query += "] "; //$NON-NLS-1$
		
		int maxempan = Math.max(cooc.getMaxLeft(), cooc.getMaxRight()); 
		if (cooc.getIncludeXPivot() && maxempan == 0) maxempan = 1;  
		
		String maxempanstr = "within " + maxempan + " "; //$NON-NLS-1$ //$NON-NLS-2$
		if (cooc.getStructuralUnitLimit() != null) maxempanstr += cooc.getStructuralUnitLimit().getName(); 
		
		query = "("+cooc.getQuery()+" []* "+query+") | ("+query+" []* "+cooc.getQuery()+") "+maxempanstr; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		
		CQPCorpus corpus = cooc.getCorpus();
		Concordance concordance = new Concordance(corpus);
		concordance.setParameters(new CQLQuery(query), null, null, null, null, null, null, null, null, null, null);
		editorInput = new TXMResultEditorInput(concordance);

		IWorkbenchPage page = editor.getEditorSite().getWorkbenchWindow().getActivePage();
		
//		try {
//			ConcordanceEditor conceditor = (ConcordanceEditor) page
//					.openEditor(editorInput, ConcordanceEditor.ID); //$NON-NLS-1$
//		} catch (PartInitException e) {
//			System.err.println("Error: "+e.getLocalizedMessage());
//		}
	}
}
