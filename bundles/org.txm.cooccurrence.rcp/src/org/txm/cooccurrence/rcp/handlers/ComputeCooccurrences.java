// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.cooccurrence.rcp.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.concordance.core.functions.Concordance;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.cooccurrence.core.preferences.CooccurrencePreferences;
import org.txm.cooccurrence.rcp.editors.CooccurrencesEditor;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.logger.Log;

/**
 * Opens a Cooccurrence editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeCooccurrences extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		if (!this.checkCorpusEngine()) {
			return null;
		}

		Cooccurrence cooc = null;

		// From link: creating from parameters node
		String parametersNodePath = event.getParameter(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
		if (parametersNodePath != null && !parametersNodePath.isEmpty()) {
			cooc = new Cooccurrence(parametersNodePath);
		}
		// From view result node
		else {

			Object selection = this.getCorporaViewSelectedObject(event);

			// Creating from Concordance
			if (selection instanceof Concordance) {

				int minleft = 1;
				int maxleft = 50;
				int minright = 1;
				int maxright = 50;

				int minf = CooccurrencePreferences.getInstance().getInt(TXMPreferences.F_MIN);
				int mincof = CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MIN_COUNT);
				float minscore = CooccurrencePreferences.getInstance().getFloat(CooccurrencePreferences.MIN_SCORE);
				boolean buildLexicalTableWithCooccurrents = CooccurrencePreferences.getInstance().getBoolean(CooccurrencePreferences.PARTIAL_LEXICAL_TABLE);

				StructuralUnit limit = null;
				CQLQuery query = null;
				List<WordProperty> properties = null;

				Concordance conc = (Concordance) selection;
				CQPCorpus corpus = conc.getCorpus();
				maxleft = conc.getLeftContextSize();
				maxright = conc.getRightContextSize();
				properties = new ArrayList<WordProperty>();
				properties.addAll(conc.getAnalysisProperty());
				query = (CQLQuery) conc.getQuery(); //TODO manage all kind of Query

				try {
					cooc = new Cooccurrence(corpus);
					cooc.setParameters(query, properties, limit, maxleft, minleft, minright, maxright, minf, minscore, mincof, false, buildLexicalTableWithCooccurrents);
				}
				catch (Exception e) {
					Log.printStackTrace(e);
					return null;
				}

			}
			// Creating from Corpus
			else if (selection instanceof CQPCorpus) {
				cooc = new Cooccurrence((CQPCorpus) selection);
			}
			// Reopening an existing result
			else if (selection instanceof Cooccurrence) {
				cooc = (Cooccurrence) selection;
			}
			// Error
			else {
				return this.logCanNotExecuteCommand(selection);
			}
		}

		StatusLine.setMessage(TXMUIMessages.openingCooccurrentsTable);

		TXMEditor.openEditor(cooc, CooccurrencesEditor.ID);

		return null;
	}
}