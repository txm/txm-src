// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.cooccurrence.rcp.editors;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.txm.cooccurrence.core.preferences.CooccurrencePreferences;
import org.txm.cooccurrence.rcp.messages.CooccurrenceUIMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;

// TODO: Auto-generated Javadoc
/**
 * Used by the Cooc Editor Allow to choose a structural unit and the size @
 * author mdecorde.
 */
public class EmpantWidget extends Composite {
	
	/** The struct type. */
	Button structType;
	
	/** The word type. */
	Button wordType;
	
	/** The structs. */
	Combo structs;
	
	/** The max left. */
	Spinner maxLeft;
	
	/** The max right. */
	Spinner maxRight;
	
	/** The min right. */
	Spinner minRight;
	
	/** The min left. */
	Spinner minLeft;
	
	/** The check xword. */
	Button checkXword;
	
	/** The check xword. */
	Button checkLeft;
	
	/** The check xword. */
	Button checkRight;
	
	// Label infoLabel;
	
	/** The corpus. */
	CQPCorpus corpus;
	
	/**
	 * Instantiates a new empant widget.
	 *
	 * @param parent the parent
	 * @param style the style
	 * @param corpus the corpus
	 */
	public EmpantWidget(Composite parent, int style, CQPCorpus corpus) {
		super(parent, style);
		this.corpus = corpus;
		GridLayout mainLayout = new GridLayout(1, true);
		// mainLayout.verticalSpacing = 0;
		mainLayout.marginWidth = 0;
		this.setLayout(mainLayout);
		
		Composite line1 = new Composite(this, style);
		Composite line2 = new Composite(this, style);
		line1.setLayout(new GridLayout(7, false));
		line2.setLayout(new GridLayout(9, false));
		line1.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		line2.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		
		
		Label l = new Label(line1, SWT.NONE);
		l.setText(TXMUIMessages.contextColon);
		
		wordType = new Button(line1, SWT.RADIO);
		wordType.setText(CooccurrenceUIMessages.wordWindow);
		wordType.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				structs.setEnabled(false);
				checkLeft.setSelection(true);
				checkRight.setSelection(true);
				maxLeft.setEnabled(true);
				minLeft.setEnabled(true);
				minRight.setEnabled(true);
				maxRight.setEnabled(true);
				
				checkXword.setEnabled(false);
				checkXword.setSelection(false);
				
				maxLeft.setSelection(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MAX_LEFT)-1);
				minLeft.setSelection(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MIN_LEFT)-1);
				minRight.setSelection(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MIN_RIGHT)-1);
				maxRight.setSelection(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MAX_RIGHT)-1);
				
				maxLeft.setMinimum(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MIN_LEFT)-1);
				minLeft.setMaximum(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MAX_LEFT)-1);
				minRight.setMaximum(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MAX_RIGHT)-1);
				maxRight.setMinimum(CooccurrencePreferences.getInstance().getInt(CooccurrencePreferences.MIN_RIGHT)-1);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		wordType.setSelection(true);
		
		structType = new Button(line1, SWT.RADIO);
		structType.setText(CooccurrenceUIMessages.structureWindow);
		structType.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				structs.setEnabled(true);
				checkLeft.setSelection(false);
				checkRight.setSelection(false);
				
				checkXword.setEnabled(true);
				checkXword.setSelection(true);
				
				maxLeft.setSelection(0);
				minLeft.setSelection(0);
				minRight.setSelection(0);
				maxRight.setSelection(0);
				
				maxLeft.setMinimum(0);
				minLeft.setMaximum(0);
				minRight.setMaximum(0);
				maxRight.setMinimum(0);
				
				maxLeft.setEnabled(false);
				minLeft.setEnabled(false);
				minRight.setEnabled(false);
				maxRight.setEnabled(false);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		// [info]
		structs = new Combo(line1, SWT.READ_ONLY | SWT.SINGLE | SWT.BORDER);
		GridData layoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		structs.setLayoutData(layoutData);
		
		checkLeft = new Button(line1, SWT.CHECK);
		checkLeft.setSelection(true);
		checkLeft.setText(TXMUIMessages.useLeftWindow);
		checkLeft.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean sel = checkLeft.getSelection();
				maxLeft.setEnabled(sel);
				minLeft.setEnabled(sel);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		checkRight = new Button(line1, SWT.CHECK);
		checkRight.setSelection(true);
		checkRight.setText(TXMUIMessages.useRightWindow);
		checkRight.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean sel = checkRight.getSelection();
				maxRight.setEnabled(sel);
				minRight.setEnabled(sel);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		checkXword = new Button(line1, SWT.CHECK);
		checkXword.setText(TXMUIMessages.includeTheKeywordStructureInTheCount);
		
		
		// LINE 2
		l = new Label(line2, SWT.NONE);
		l.setText(TXMUIMessages.from);
		
		// | [ ] [ ] [ |v] [ ] [ ] |
		maxLeft = new Spinner(line2, SWT.BORDER);
		maxLeft.setMinimum(0);
		maxLeft.setMaximum(9999);
		maxLeft.setIncrement(1);
		maxLeft.setPageIncrement(10);
		maxLeft.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				minLeft.setMaximum(maxLeft.getSelection());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		l = new Label(line2, SWT.NONE);
		l.setText(TXMUIMessages.to_2);
		
		minLeft = new Spinner(line2, SWT.BORDER);
		minLeft.setMinimum(0);
		minLeft.setMaximum(9999);
		minLeft.setIncrement(1);
		minLeft.setPageIncrement(10);
		minLeft.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				maxLeft.setMinimum(minLeft.getSelection());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		l = new Label(line2, SWT.NONE);
		l.setText(TXMUIMessages.andFrom);
		
		// [>]
		minRight = new Spinner(line2, SWT.BORDER);
		minRight.setMinimum(0);
		minRight.setMaximum(9999);
		minRight.setIncrement(1);
		minRight.setPageIncrement(10);
		minRight.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				maxRight.setMinimum(minRight.getSelection());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		l = new Label(line2, SWT.NONE);
		l.setText(TXMUIMessages.to_3);
		
		// [>|]
		maxRight = new Spinner(line2, SWT.BORDER);
		maxRight.setMinimum(0);
		maxRight.setMaximum(9999);
		maxRight.setIncrement(1);
		maxRight.setPageIncrement(10);
		maxRight.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				minRight.setMaximum(maxRight.getSelection());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		// infoLabel = new Label(line2, SWT.NONE);
		// GridData infoLabelData = new GridData(GridData.END, GridData.CENTER, true, false);
		// infoLabel.setLayoutData(infoLabelData);
		
		loadStructs();
		structs.setEnabled(false);
		checkXword.setEnabled(false);
	}
	//
	// public void setInfo(String txt, String tooltip) {
	// infoLabel.setText(txt);
	// infoLabel.setToolTipText(tooltip);
	// }
	
	/**
	 * Load structs.
	 */
	private void loadStructs() {
		try {
			java.util.List<StructuralUnit> corpusstructuralunits = corpus.getOrderedStructuralUnits();
			HashSet<String> suToIgnore = new HashSet<>(Arrays.asList(CooccurrencePreferences.getInstance().getString(CooccurrencePreferences.STRUCTURES_TO_IGNORE).split(","))); //$NON-NLS-1$
			for (StructuralUnit su : corpusstructuralunits) {
				if (!suToIgnore.contains(su.getName())) { //$NON-NLS-1$ $NON-NLS-2$
					structs.add(su.getName());
				}
			}
			if (corpusstructuralunits.size() > 0) {
				int pi = Arrays.binarySearch(structs.getItems(), "p"); //$NON-NLS-1$
				int si = Arrays.binarySearch(structs.getItems(), "s"); //$NON-NLS-1$
				if (pi > 0) {
					structs.select(pi);
				} else if (si > 0) {
					structs.select(si);
				} else {
					structs.select(0);
				}
			}
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
	
	/**
	 * Adds the first listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addFirstListener(SelectionListener selectionListener) {
		maxLeft.addSelectionListener(selectionListener);
	}
	
	/**
	 * Adds the next listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addNextListener(SelectionListener selectionListener) {
		minRight.addSelectionListener(selectionListener);
	}
	
	/**
	 * Adds the previous listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addPreviousListener(SelectionListener selectionListener) {
		minLeft.addSelectionListener(selectionListener);
	}
	
	/**
	 * Adds the last listener.
	 *
	 * @param selectionListener the selection listener
	 */
	public void addLastListener(SelectionListener selectionListener) {
		maxRight.addSelectionListener(selectionListener);
	}
	
	/**
	 * Sets the max left.
	 *
	 * @param maxleft the new max left
	 */
	public void setMaxLeft(int maxleft) {
		this.maxLeft.setSelection(maxleft);
	}
	
	/**
	 * Sets the min left.
	 *
	 * @param minleft the new min left
	 */
	public void setMinLeft(int minleft) {
		this.minLeft.setSelection(minleft);
	}
	
	/**
	 * Sets the max right.
	 *
	 * @param maxright the new max right
	 */
	public void setMaxRight(int maxright) {
		this.maxRight.setSelection(maxright);
	}
	
	/**
	 * Sets the min right.
	 *
	 * @param minright the new min right
	 */
	public void setMinRight(int minright) {
		this.minRight.setSelection(minright);
	}
	
	/**
	 * Gets the max left.
	 *
	 * @return the max left
	 */
	public int getMaxLeft() {
		if (maxLeft.isEnabled())
			return maxLeft.getSelection();
		return -1;
	}
	
	/**
	 * Gets the min left.
	 *
	 * @return the min left
	 */
	public int getMinLeft() {
		if (minLeft.isEnabled())
			return minLeft.getSelection();
		return -1;
	}
	
	/**
	 * Gets the max right.
	 *
	 * @return the max right
	 */
	public int getMaxRight() {
		if (maxRight.isEnabled())
			return maxRight.getSelection();
		return -1;
	}
	
	/**
	 * Gets the min right.
	 *
	 * @return the min right
	 */
	public int getMinRight() {
		if (minRight.isEnabled())
			return minRight.getSelection();
		return -1;
	}
	
	/**
	 * Sets the structure. If null then swap to word mode.
	 *
	 * @param su the new structure
	 */
	public void setStructure(StructuralUnit su) {
		if (su != null) {
			structType.setSelection(true);
			wordType.setSelection(false);
			structs.setText(su.getName());
			structs.setEnabled(true);
			checkXword.setEnabled(true);
		}
		else {
			structType.setSelection(false);
			wordType.setSelection(true);
			structs.setText(""); //$NON-NLS-1$
			structs.setEnabled(false);
			checkXword.setEnabled(false);
		}
	}
	
	/**
	 * Gets the struct.
	 *
	 * @return the struct
	 */
	public StructuralUnit getStruct() {
		if (this.wordType.getSelection()) {
			return null;
		}
		try {
			if (structs.getText().length() == 0) {
				return null;
			}
			return this.corpus.getStructuralUnit(structs.getText());
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}
	
	/**
	 * Gets the x pivot.
	 *
	 * @return the x pivot
	 */
	public boolean getXPivot() {
		return this.checkXword.getSelection();
	}
	
	/**
	 * Gets the x pivot.
	 *
	 */
	public void setXPivot(boolean sel) {
		this.checkXword.setSelection(sel);
	}
	
	public void addSelectionListener(SelectionListener listener) {
		structType.addSelectionListener(listener);
		wordType.addSelectionListener(listener);
		structs.addSelectionListener(listener);
		maxLeft.addSelectionListener(listener);
		maxRight.addSelectionListener(listener);
		minRight.addSelectionListener(listener);
		minLeft.addSelectionListener(listener);
		checkXword.addSelectionListener(listener);
		checkLeft.addSelectionListener(listener);
		checkRight.addSelectionListener(listener);
	}
	
	public void removeSelectionListener(SelectionListener listener) {
		structType.removeSelectionListener(listener);
		wordType.removeSelectionListener(listener);
		structs.removeSelectionListener(listener);
		maxLeft.removeSelectionListener(listener);
		maxRight.removeSelectionListener(listener);
		minRight.removeSelectionListener(listener);
		minLeft.removeSelectionListener(listener);
		checkXword.removeSelectionListener(listener);
		checkLeft.removeSelectionListener(listener);
		checkRight.removeSelectionListener(listener);
	}
	
	public void addMinMaxKeyListener(KeyListener listener) {
		maxLeft.addKeyListener(listener);
		maxRight.addKeyListener(listener);
		minRight.addKeyListener(listener);
		minLeft.addKeyListener(listener);
	}
	
}
