// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.cooccurrence.rcp.editors;

import java.text.DecimalFormat;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.cooccurrence.core.preferences.CooccurrencePreferences;

// TODO: Auto-generated Javadoc
/**
 * The Class LineLabelProvider.
 *
 * @author mdecorde
 */
public class LineLabelProvider implements ITableLabelProvider {

	/** The format. */
	String format = CooccurrencePreferences.getInstance().getString(CooccurrencePreferences.SCORE_FORMAT);

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	@Override
	public String getColumnText(Object element, int columnIndex) {
		// System.out.println("getColumnText "+columnIndex+" >>> "+((Cooccurence.CLine)
		// element).resume());
		Cooccurrence.CLine line = (Cooccurrence.CLine) element;
		switch (columnIndex) {
		case 0:
			return ""; //$NON-NLS-1$
		case 1:
			return line.occ;
		case 2:
			return "" + line.freq; //$NON-NLS-1$
		case 3:
			return "" + line.nbocc; //$NON-NLS-1$
		case 4:
			//String signe = "+"; //$NON-NLS-1$
			/*
			 * DecimalFormat f = new DecimalFormat("0.0000E00"); //$NON-NLS-1$
			 * try { //if (line.mode > line.nbocc) // signe="-"; //$NON-NLS-1$
			 * 
			 * f = new DecimalFormat(format); return f.format(line.score);
			 * }catch(Exception e){ } String d = f.format(line.score);
			 * 
			 * return d.substring(d.length()-2, d.length());
			 */
			return String.valueOf((int) line.score);
			//return ""+(line.score*100.0)+"%"; //$NON-NLS-1$
		case 5:
			DecimalFormat f = new DecimalFormat("#.0"); //$NON-NLS-1$
			return f.format(line.distmoyenne);
			//return ""+line.distmoyenne; //$NON-NLS-1$
		case 6:
			return ""; //$NON-NLS-1$
		}
		return "null" + columnIndex; //$NON-NLS-1$
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
	}
}
