// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.cooccurrence.rcp.editors;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.cooccurrence.core.functions.Cooccurrence.CLine;
import org.txm.cooccurrence.core.functions.comparators.CLineComparator;
import org.txm.cooccurrence.core.functions.comparators.DistComparator;
import org.txm.cooccurrence.core.functions.comparators.FreqComparator;
import org.txm.cooccurrence.core.functions.comparators.NbOccComparator;
import org.txm.cooccurrence.core.functions.comparators.OccComparator;
import org.txm.cooccurrence.core.functions.comparators.ScoreComparator;
import org.txm.cooccurrence.core.preferences.CooccurrencePreferences;
import org.txm.cooccurrence.rcp.messages.CooccurrenceUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMParameters;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.rcp.swt.jface.TableViewerSeparatorColumn;
import org.txm.rcp.swt.widget.AssistedQueryWidget;
import org.txm.rcp.swt.widget.FloatSpinner;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.rcp.utils.IOClipboard;
import org.txm.rcp.views.QueriesView;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.statsengine.r.rcp.views.RVariablesView;
import org.txm.utils.logger.Log;

/**
 * display the cooc parameters and result
 * 
 * @author mdecorde
 */
public class CooccurrencesEditor extends TXMEditor<Cooccurrence> implements TableResultEditor {
	
	/** The Constant ID. */
	public static final String ID = CooccurrencesEditor.class.getName();
	
	/** The cooc. */
	Cooccurrence cooc;
	
	/** The empant panel. */
	EmpantWidget empantPanel;
	
	/** The line table viewer. */
	TableViewer viewer;
	
	/** The occ column. */
	TableColumn occColumn;
	
	/** The freq column. */
	TableColumn freqColumn;
	
	/** The nbocc column. */
	TableColumn nboccColumn;
	
	/** The score column. */
	TableColumn scoreColumn;
	
	/** The dist column. */
	TableColumn distColumn;
	
	/** The current comparator. */
	CLineComparator currentComparator;
	
	
	
	// params
	/** The query widget. */
	@Parameter(key = CooccurrencePreferences.QUERY)
	AssistedQueryWidget queryWidget;
	
	// /** The query widget. */
	// @Parameter(key=CooccurrencePreferences.QUERY_FILTER)
	// AssistedQueryWidget coocQueryWidget;
	
	/** The props area. */
	@Parameter(key = TXMPreferences.UNIT_PROPERTIES)
	PropertiesSelector<WordProperty> propertiesSelector;
	
	/** The T freq. */
	@Parameter(key = TXMPreferences.F_MIN)
	Spinner fMin;
	
	/** The T count. */
	@Parameter(key = CooccurrencePreferences.MIN_COUNT)
	Spinner cMin;
	
	/** The T score. */
	@Parameter(key = CooccurrencePreferences.MIN_SCORE)
	FloatSpinner minScore;
	
	private Button infoLine;

	private TableKeyListener tableKeyListener;
	
	
	
	/**
	 * Initialize the editor with the cooccurrences TXMResult
	 *
	 * @param site
	 *            the site
	 * @param input
	 *            the input
	 * @throws PartInitException
	 *             the part init exception
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite,
	 *      org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void _init() throws PartInitException {
		this.cooc = (Cooccurrence) ((TXMResultEditorInput<?>) getEditorInput()).getResult();
	}
	
	/**
	 * Creates the part control.
	 *
	 * @see "org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)"
	 */
	@Override
	public void _createPartControl() {
		
		final Composite paramArea = this.getExtendedParametersGroup();
		FormLayout paramLayout = new FormLayout();
		paramArea.setLayout(paramLayout);
		
		// Computing listeners
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this);
		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this);
		
		
		
		// Main parameters area
		Composite queryArea = new Composite(paramArea, SWT.NONE);
		
		FormData queryLayoutData = new FormData();
		queryLayoutData.top = new FormAttachment(0);
		queryLayoutData.left = new FormAttachment(0);
		queryLayoutData.right = new FormAttachment(100);
		queryArea.setLayoutData(queryLayoutData);
		
		queryArea.setLayout(new GridLayout(3, false));
		
		this.getMainParametersComposite().getLayout().numColumns = 3;
		this.getMainParametersComposite().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		
		Label queryLabel = new Label(getMainParametersComposite(), SWT.NONE);
		queryLabel.setText(TXMCoreMessages.common_query);
		queryLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true));
		
		// [ (v)]
		queryWidget = new AssistedQueryWidget(getMainParametersComposite(), SWT.DROP_DOWN, this.cooc.getCorpus());
		GridData layoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		layoutData.horizontalAlignment = GridData.FILL;
		layoutData.grabExcessHorizontalSpace = true;
		queryWidget.setLayoutData(layoutData);
		queryWidget.addKeyListener(computeKeyListener);
		queryWidget.getQueryWidget().addModifyListener(computeKeyListener);
		
		
		// TODO add new parameter to select the cooccurrents with a CQL
		// queryLabel = new Label(getMainParametersComposite(), SWT.NONE);
		// queryLabel.setText("Cooc");
		// queryLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true));
		// // [ (v)]
		// coocQueryWidget = new AssistedQueryWidget(getMainParametersComposite(), SWT.DROP_DOWN, this.cooc.getCorpus());
		// layoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		// layoutData.horizontalAlignment = GridData.FILL;
		// layoutData.grabExcessHorizontalSpace = true;
		// coocQueryWidget.setLayoutData(layoutData);
		// coocQueryWidget.addKeyListener(computeKeyListener);
		
		// Extended parameters
		
		// Filters
		Composite filtercontrols = new Composite(paramArea, SWT.NONE);
		FormData filtersLayoutData = new FormData();
		filtersLayoutData.top = new FormAttachment(queryArea, 0);
		// filtersLayoutData.bottom = new FormAttachment (100);
		filtersLayoutData.left = new FormAttachment(0);
		filtersLayoutData.right = new FormAttachment(100);
		filtercontrols.setLayoutData(filtersLayoutData);
		RowLayout layout = new RowLayout();
		layout.wrap = true;
		layout.center = true;
		filtercontrols.setLayout(layout);
		
		// | Properties: word_pos [Edit] |
		propertiesSelector = new PropertiesSelector<>(filtercontrols, SWT.NONE);
		propertiesSelector.setLayout(new GridLayout(4, false));
		propertiesSelector.setCorpus(this.getCorpus());
		propertiesSelector.setTitle(CooccurrenceUIMessages.cooccurrentsPropertiesColon);
		propertiesSelector.addSelectionListener(computeSelectionListener);
		
		// Thresholds
		// Fmin
		TXMParameterSpinner fMin = new TXMParameterSpinner(filtercontrols, this, CooccurrenceUIMessages.thresholdsColonFmin);
		this.fMin = fMin.getControl();
		
		// Cmin
		TXMParameterSpinner cMin = new TXMParameterSpinner(filtercontrols, this, CooccurrenceUIMessages.cminEquals);
		this.cMin = cMin.getControl();
		
		// Minimum score
		TXMParameterSpinner minimumScore = new TXMParameterSpinner(filtercontrols, this, new FloatSpinner(filtercontrols, SWT.BORDER), CooccurrenceUIMessages.score_2);
		this.minScore = (FloatSpinner) minimumScore.getControl();
		
		// empant
		empantPanel = new EmpantWidget(paramArea, SWT.NONE, this.getCorpus());
		
		FormData empantLayoutData = new FormData();
		empantLayoutData.top = new FormAttachment(filtercontrols, 0);
		empantLayoutData.bottom = new FormAttachment(100);
		empantLayoutData.left = new FormAttachment(0);
		empantLayoutData.right = new FormAttachment(100);
		empantPanel.setLayoutData(empantLayoutData);
		empantPanel.addSelectionListener(computeSelectionListener);
		empantPanel.addMinMaxKeyListener(computeKeyListener);
		
		getBottomToolbar().getSubWidgetComposite().getLayout().numColumns++;
		infoLine = new Button(getBottomToolbar().getSubWidgetComposite(), SWT.PUSH);
		infoLine.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IOClipboard.write(infoLine.getText());
				Log.info("Copied text: "+infoLine.getText());
			}
		});
//		infoLine.setLayoutData(new RowData(600, SWT.DEFAULT));
		
		
		// result area
		Composite resultArea = this.getResultArea();
		
		viewer = new TableViewer(resultArea, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER | SWT.VIRTUAL);
		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer, true, false);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);
		
		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setHeaderVisible(true);
		
		String fontName = cooc.getCorpus().getFont();
		if (fontName != null && fontName.length() > 0) {
			Font old = viewer.getTable().getFont();
			FontData fD = old.getFontData()[0];
			// Font f = new Font(old.getDevice(), corpus.getFont(),
			// fD.getHeight(), fD.getStyle());
			Font font = TXMFontRegistry.getFont(Display.getCurrent(), fontName, fD.getHeight(), fD.getStyle());
			viewer.getTable().setFont(font);
		}
		
		viewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
		
		viewer.setLabelProvider(new LineLabelProvider());
		viewer.setContentProvider(new LineContentProvider());
		viewer.getTable().setToolTipText(""); //$NON-NLS-1$
		viewer.addPostSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				
				viewer.getTable().setToolTipText(viewer.getStructuredSelection().toList().toString());
			}
		});
		//ColumnViewerToolTipSupport.enableFor(viewer);
		
		TableViewerSeparatorColumn.newSeparator(viewer);
		
		occColumn = new TableColumn(viewer.getTable(), SWT.LEFT);
		occColumn.setText(CooccurrenceUIMessages.cooccurrent);
		occColumn.setToolTipText(CooccurrenceUIMessages.cooccurrent);
		occColumn.setWidth(200);
		occColumn.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				setColComparator(new OccComparator(), occColumn);
				sort();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		freqColumn = new TableColumn(viewer.getTable(), SWT.RIGHT);
		freqColumn.setText(TXMCoreMessages.common_frequency);
		freqColumn.setToolTipText(TXMCoreMessages.common_frequency);
		freqColumn.setWidth(100);
		freqColumn.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				setColComparator(new FreqComparator(), freqColumn);
				sort();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		nboccColumn = new TableColumn(viewer.getTable(), SWT.RIGHT);
		nboccColumn.setText(CooccurrenceUIMessages.cofrequency);
		nboccColumn.setToolTipText(CooccurrenceUIMessages.cofrequency);
		nboccColumn.setWidth(130);
		nboccColumn.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				setColComparator(new NbOccComparator(), nboccColumn);
				sort();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		scoreColumn = new TableColumn(viewer.getTable(), SWT.RIGHT);
		scoreColumn.setText(CooccurrenceUIMessages.score);
		scoreColumn.setToolTipText(CooccurrenceUIMessages.score);
		scoreColumn.setWidth(100);
		scoreColumn.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				setColComparator(new ScoreComparator(), scoreColumn);
				sort();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		distColumn = new TableColumn(viewer.getTable(), SWT.RIGHT);
		distColumn.setText(CooccurrenceUIMessages.meanDistance);
		distColumn.setToolTipText(CooccurrenceUIMessages.meanDistance);
		distColumn.setWidth(130);
		distColumn.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				setColComparator(new DistComparator(), distColumn);
				sort();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		TableViewerSeparatorColumn.newSeparator(viewer);
		
		// Add double click, "Send to" command
		TableUtils.addDoubleClickCommandListener(viewer.getTable(), "org.txm.concordance.rcp.handlers.ComputeConcordance"); //$NON-NLS-1$
		
		// Register the context menu
		TXMEditor.initContextMenu(this.viewer.getTable(), this.getSite(), this.viewer);
		
		// descending initial sorting on score column
		currentComparator = new ScoreComparator();
		setColComparator(new ScoreComparator(), scoreColumn);
		setColComparator(new ScoreComparator(), scoreColumn); // second time is descending mode
		sort();
		
	}
	
	/**
	 * Sets the col comparator.
	 *
	 * @param comp the comp
	 * @param col the col
	 */
	private void setColComparator(CLineComparator comp, TableColumn col) {
		currentComparator = comp;
		if (viewer.getTable().getSortColumn() != col) {
			viewer.getTable().setSortColumn(col);
			viewer.getTable().setSortDirection(SWT.UP);
			currentComparator.setAscending(true);
		}
		else if (viewer.getTable().getSortDirection() == SWT.UP) {
			viewer.getTable().setSortDirection(SWT.DOWN);
			currentComparator.setAscending(false);
		}
		else {
			viewer.getTable().setSortDirection(SWT.UP);
			currentComparator.setAscending(true);
		}
		if (cooc != null) {
			currentComparator.initialize(cooc.getCorpus());
		}
	}
	
	/**
	 * Creates some parameters from the current widget values.
	 * 
	 * @return
	 */
	public TXMParameters getWidgetsParameters() {
		
		TXMParameters parameters = new TXMParameters();
		parameters.put(CooccurrencePreferences.UNIT_PROPERTIES, new ArrayList<Property>(this.propertiesSelector.getSelectedProperties()));
		parameters.put(CooccurrencePreferences.STRUCTURAL_UNIT_LIMIT, this.empantPanel.getStruct());
		parameters.put(CooccurrencePreferences.MAX_LEFT, this.empantPanel.getMaxLeft() + 1);
		parameters.put(CooccurrencePreferences.MIN_LEFT, this.empantPanel.getMinLeft() + 1);
		parameters.put(CooccurrencePreferences.MIN_RIGHT, this.empantPanel.getMinRight() + 1);
		parameters.put(CooccurrencePreferences.MAX_RIGHT, this.empantPanel.getMaxRight() + 1);
		parameters.put(TXMPreferences.F_MIN, this.fMin.getSelection());
		parameters.put(CooccurrencePreferences.MIN_COUNT, this.cMin.getSelection());
		parameters.put(CooccurrencePreferences.INCLUDE_X_PIVOT, this.empantPanel.getXPivot());
		parameters.put(CooccurrencePreferences.PARTIAL_LEXICAL_TABLE, this.getBooleanParameterValue(CooccurrencePreferences.PARTIAL_LEXICAL_TABLE));
		
		Double minScore = 0.0;
		try {
			Double d = Double.parseDouble(this.minScore.getText().replace(",", ".")); //$NON-NLS-1$ //$NON-NLS-2$
			minScore = d;
		}
		catch (Exception e) {
			Log.severe(NLS.bind(CooccurrenceUIMessages.theScoreThresholdMustBeARealNumberCurrentValueEqualsP0, this.minScore.getText()));
		}
		
		parameters.put(CooccurrencePreferences.MIN_SCORE, minScore);
		
		return parameters;
	}
	
	
	
	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return cooc.getCorpus();
	}
	
	/**
	 * Gets the cooc.
	 *
	 * @return the cooc
	 */
	public Cooccurrence getCooc() {
		return this.cooc;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		
		if (cooc != null && !cooc.isDirty() && currentComparator != null) {
			
			try {
				List<CLine> sel = viewer.getStructuredSelection().toList();
				HashSet<String> lineNames = new HashSet<String>();
				for (CLine l : sel) {
					lineNames.add(l.occ);
				}
				
				Log.fine(NLS.bind(CooccurrenceUIMessages.sortByColonP0, currentComparator.getName()));
				
				PlatformUI.getWorkbench().getProgressService().busyCursorWhile(new IRunnableWithProgress() {
					
					@Override
					public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
						try {
							cooc.sort(currentComparator);
						}
						catch (CqiClientException e) {
							Log.severe(TXMCoreMessages.error_errorWhileSortingResult);
							org.txm.utils.logger.Log.printStackTrace(e);
						}
					}
				});
				
				TableUtils.refreshColumns(this.viewer);
				
				viewer.getTable().deselectAll();
				if (lineNames.size() > 0) {
					List<CLine> newLines = (List<CLine>) viewer.getInput();
					for (int i = 0 ; i < newLines.size() ; i++) {
						
						if (lineNames.remove(newLines.get(i).occ)) {
							viewer.getTable().select(i);
						}
					}
					// FIXME setSelection dont work, is it because the table is virtual ?
					//viewer.setSelection(new StructuredSelection(new ArrayList()));
					
				}
			}
			catch (Exception e) {
				Log.severe(TXMCoreMessages.error_errorWhileSortingResult);
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
	}
	
	
	@Override
	public void updateEditorFromResult(boolean update) {
		
		if (this.cooc.getProperties() != null) {
			List<WordProperty> available;
			try {
				available = new ArrayList<>(this.cooc.getCorpus().getProperties());
				available.addAll(this.cooc.getCorpus().getVirtualProperties());
				this.propertiesSelector.setProperties(available, cooc.getProperties());
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
		
		if (!queryWidget.isDisposed()) {
			queryWidget.memorize();
		}
		
		// Structural unit limit
		empantPanel.setStructure(this.cooc.getStructuralUnitLimit());
		
		// Contexts
		empantPanel.setMinLeft(this.getIntParameterValue(CooccurrencePreferences.MIN_LEFT) - 1);
		empantPanel.setMaxLeft(this.getIntParameterValue(CooccurrencePreferences.MAX_LEFT) - 1);
		empantPanel.setMinRight(this.getIntParameterValue(CooccurrencePreferences.MIN_RIGHT) - 1);
		empantPanel.setMaxRight(this.getIntParameterValue(CooccurrencePreferences.MAX_RIGHT) - 1);
		empantPanel.setXPivot(this.getBooleanParameterValue(CooccurrencePreferences.INCLUDE_X_PIVOT));
		
		String txt = "t pivot " + cooc.getNumberOfKeyword() + ", v cooc " + cooc.getNumberOfDifferentCooccurrents() + ", t cooc " + cooc.getNumberOfCooccurrents(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		try {
			txt += ", T corpus " + cooc.getCorpus().getSize();  //$NON-NLS-1$
		}
		catch (Exception e) {
			Log.severe(CooccurrenceUIMessages.errorWhileReadingCorpusSize);
		}
		String tooltip = "- " + txt.replaceAll(", ", "\n- "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		
		// empantPanel.setInfo(txt, tooltip);
		infoLine.setText(txt);
		infoLine.setToolTipText(tooltip);
		
		viewer.setInput(cooc.getLines());
		sort();
		
		// Pack the columns
		TableUtils.packColumns(viewer);
		
		// FIXME: need to be done in another way after the plugin split
		QueriesView.refresh();
		RVariablesView.refresh();
		
		viewer.getTable().setFocus();
	}
	
	
	@Override
	public void updateResultFromEditor() {
		TXMParameters parameters = getWidgetsParameters();
		cooc.setParametersFromWidgets(parameters);
	}

	@Override
	public TableViewer[] getTableViewers() {
		
		return new TableViewer[] {viewer};
	}

	@Override
	public TableKeyListener getTableKeyListener() {
		
		return tableKeyListener;
	}
}
