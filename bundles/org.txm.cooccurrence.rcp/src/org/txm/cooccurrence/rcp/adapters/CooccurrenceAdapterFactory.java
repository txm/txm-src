// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.cooccurrence.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.cooccurrence.core.functions.Cooccurrence;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class CooccurrenceAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(CooccurrenceAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(CooccurrenceAdapterFactory.class).getSymbolicName() + "/icons/cooccurrences.png"); //$NON-NLS-1$ //$NON-NLS-2$


	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof Cooccurrence) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			});
		}
		return null;
	}


}
