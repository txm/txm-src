package org.txm.cooccurrence.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Cooccurrence UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CooccurrenceUIMessages extends NLS {
	
	private static final String BUNDLE_NAME = "org.txm.cooccurrence.rcp.messages.messages"; //$NON-NLS-1$
	
	public static String minimumFrequencyForAWordToBeAbleToParticipate;
	public static String limitingMarginsToCooccurrenceContexts;
	
	public static String scoreFormat;
	public static String scoreFormatDefault00000E00; 
	public static String minimumFrequencyThresholdOfTheCooccurrent; 
	public static String minimumRight; 
	public static String maximumRight; 
	public static String minimumCooccurrencyCountThreshold; 
	public static String minimumCooccurrencyScoreThreshold; 
	public static String maximumLeft; 
	public static String cooccurrences; 
	public static String minimumLeft; 
	public static String usePartialLexicalTable; 
	public static String sortByColonP0; 

	public static String cofrequency; 
	public static String score; 
	public static String theScoreThresholdMustBeARealNumberCurrentValueEqualsP0; 

	public static String errorWhileReadingCorpusSize; 
	public static String cooccurrentsPropertiesColon; 
	public static String thresholdsColonFmin; 
	public static String cmin;
	public static String cminEquals;
	public static String score_2; 
	public static String cooccurrent; 
	public static String meanDistance;

	public static String structuresListCommaSeparated;
	public static String structuresListToIgnore;
	
	public static String wordWindow; 
	public static String structureWindow; 

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, CooccurrenceUIMessages.class);
	}

	private CooccurrenceUIMessages() {
	}
}
