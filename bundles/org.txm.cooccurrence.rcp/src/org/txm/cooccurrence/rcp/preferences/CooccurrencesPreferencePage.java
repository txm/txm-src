// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.cooccurrence.rcp.preferences;

import org.eclipse.ui.IWorkbench;
import org.txm.cooccurrence.core.preferences.CooccurrencePreferences;
import org.txm.cooccurrence.rcp.adapters.CooccurrenceAdapterFactory;
import org.txm.cooccurrence.rcp.messages.CooccurrenceUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.rcp.jface.DoubleFieldEditor;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.IntegerFieldEditor;
import org.txm.rcp.swt.widget.preferences.StringFieldEditor;

/**
 * Cooccurrence preference page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class CooccurrencesPreferencePage extends TXMPreferencePage {
	
	
	@Override
	public void createFieldEditors() {
		
		/** The scoreformatfield. */
		StringFieldEditor scoreformatfield = new StringFieldEditor(CooccurrencePreferences.SCORE_FORMAT, CooccurrenceUIMessages.scoreFormat, getFieldEditorParent());
		scoreformatfield.setToolTipText(CooccurrenceUIMessages.scoreFormatDefault00000E00);
		scoreformatfield.setTextLimit(15);
		
		/** The minfreqfield. */
		IntegerFieldEditor minfreqfield = new IntegerFieldEditor(TXMPreferences.F_MIN, TXMCoreMessages.common_fMin, getFieldEditorParent());
		minfreqfield.setToolTipText(CooccurrenceUIMessages.minimumFrequencyThresholdOfTheCooccurrent);
		minfreqfield.setValidRange(0, Integer.MAX_VALUE);
		
		/** The mincountfield. */
		IntegerFieldEditor mincountfield = new IntegerFieldEditor(CooccurrencePreferences.MIN_COUNT, CooccurrenceUIMessages.cmin, getFieldEditorParent());
		mincountfield.setToolTipText(CooccurrenceUIMessages.minimumCooccurrencyCountThreshold);
		mincountfield.setValidRange(0, Integer.MAX_VALUE);
		
		/** The minscorefield. */
		DoubleFieldEditor minscorefield = new DoubleFieldEditor(CooccurrencePreferences.MIN_SCORE, CooccurrenceUIMessages.minimumCooccurrencyScoreThreshold, getFieldEditorParent());
		minscorefield.setTextLimit(15);
		
		IntegerFieldEditor maxleftfield = new IntegerFieldEditor(CooccurrencePreferences.MAX_LEFT, CooccurrenceUIMessages.maximumLeft, getFieldEditorParent());
		maxleftfield.setValidRange(0, 99999);
		
		IntegerFieldEditor minleftfield = new IntegerFieldEditor(CooccurrencePreferences.MIN_LEFT, CooccurrenceUIMessages.minimumLeft, getFieldEditorParent());
		minleftfield.setValidRange(0, 99999);
		
		IntegerFieldEditor minrightfield = new IntegerFieldEditor(CooccurrencePreferences.MIN_RIGHT, CooccurrenceUIMessages.minimumRight, getFieldEditorParent());
		minrightfield.setValidRange(0, 99999);
		
		IntegerFieldEditor maxrightfield = new IntegerFieldEditor(CooccurrencePreferences.MAX_RIGHT, CooccurrenceUIMessages.maximumRight, getFieldEditorParent());
		maxrightfield.setValidRange(0, 99999);
		
		StringFieldEditor milestonesfield = new StringFieldEditor(CooccurrencePreferences.STRUCTURES_TO_IGNORE, CooccurrenceUIMessages.structuresListToIgnore, getFieldEditorParent());
		milestonesfield.setToolTipText(CooccurrenceUIMessages.structuresListCommaSeparated);
		
		BooleanFieldEditor partiallexicaltablefield = new BooleanFieldEditor(CooccurrencePreferences.PARTIAL_LEXICAL_TABLE, CooccurrenceUIMessages.limitingMarginsToCooccurrenceContexts, getFieldEditorParent());
		partiallexicaltablefield.setToolTipText(CooccurrenceUIMessages.usePartialLexicalTable);
		addField(scoreformatfield);
		addField(minfreqfield);
		addField(mincountfield);
		addField(minscorefield);
		addField(maxleftfield);
		addField(minleftfield);
		addField(minrightfield);
		addField(maxrightfield);
		addField(milestonesfield);
		addField(partiallexicaltablefield);
	}
	
	
	
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(CooccurrencePreferences.getInstance().getPreferencesNodeQualifier()));
		// setDescription("Cooccurrences");
		this.setTitle(CooccurrenceUIMessages.cooccurrences);
		this.setImageDescriptor(CooccurrenceAdapterFactory.ICON);
	}
	
	
}
