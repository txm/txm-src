package org.txm.libs.msoffice;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcel {
	
	Workbook wb = null;
	
	Sheet ws = null;
	
	List<String> header = null;
	
	LinkedHashMap<String, String> record = null;
	
	int iRow = 0;
	
	int nRows;
	
	private File tableFile;
	
	public WriteExcel(File tableFile) throws EncryptedDocumentException, InvalidFormatException, IOException {
		this(tableFile, null, true);
	}
	
	public WriteExcel(File tableFile, String sheetName) throws EncryptedDocumentException, InvalidFormatException, IOException {
		this(tableFile, sheetName, true);
	}
	
	public WriteExcel(File tableFile, String sheetName, boolean readonly) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		this.tableFile = tableFile;
		
		if (tableFile.exists() && tableFile.length() > 0) {
			wb = WorkbookFactory.create(tableFile, null, readonly);
			if (sheetName == null || sheetName.length() == 0) {
				ws = wb.getSheetAt(0);
			}
			else {
				ws = wb.getSheet(sheetName);
				if (ws == null) {
					ws = wb.getSheetAt(0);
				}
			}
			
			nRows = ws.getPhysicalNumberOfRows();
		}
		else {
			wb = new XSSFWorkbook();
			ws = wb.createSheet();
		}
	}
	
	public void writeHeader(List<String> header) {
		
		this.header = new ArrayList<String>(header);
		
		Row row = ws.createRow(0);
		
		for (int i = 0 ; i < header.size() ; i++) {
			Cell cell = row.createCell(i, CellType.STRING);
			cell.setCellValue(header.get(i));
		}
	}
	
	public void writeHeader(String[] header) {
		
		this.header = Arrays.asList(header);
		
		Row row = ws.createRow(0);
		
		for (int i = 0 ; i < header.length ; i++) {
			Cell cell = row.createCell(i, CellType.STRING);
			cell.setCellValue(header[i]);
		}
	}
	
	public void writeLine(Object[] line) {
		
		Row row = ws.createRow(ws.getLastRowNum() + 1);
		
		for (int i = 0 ; i < line.length ; i++) {
			if (line[i] == null) {
				row.createCell(i, CellType.BLANK);
			} else if (line[i] instanceof String s) {
				Cell cell = row.createCell(i, CellType.STRING);
				cell.setCellValue(s);
			} else if (line[i] instanceof Boolean b) {
				Cell cell = row.createCell(i, CellType.BOOLEAN);
				cell.setCellValue(b);
			} else if (line[i] instanceof Integer in) {
				Cell cell = row.createCell(i, CellType.NUMERIC);
				cell.setCellValue(in);
			} else if (line[i] instanceof Float f) {
				Cell cell = row.createCell(i, CellType.NUMERIC);
				cell.setCellValue(f);
			} else if (line[i] instanceof Double d) {
				Cell cell = row.createCell(i, CellType.NUMERIC);
				cell.setCellValue(d);
			} else {
				Cell cell = row.createCell(i, CellType.STRING);
				cell.setCellValue(line[i].toString());
			}
		}
	}
	
	public void writeLine(double[] line) {
		
		Row row = ws.createRow(ws.getLastRowNum() + 1);
		
		for (int i = 0 ; i < line.length ; i++) {
			Cell cell = row.createCell(i, CellType.NUMERIC);
			cell.setCellValue(line[i]);
		}
	}
	
	public void writeLine(int[] line) {
		
		Row row = ws.createRow(ws.getLastRowNum() + 1);
		
		for (int i = 0 ; i < line.length ; i++) {
			Cell cell = row.createCell(i, CellType.NUMERIC);
			cell.setCellValue(line[i]);
		}
	}
	
	public void writeNamedLine(String name, double[] line) {
		
		Row row = ws.createRow(ws.getLastRowNum() + 1);
		
		Cell namecell = row.createCell(0, CellType.STRING);
		namecell.setCellValue(name);
		
		for (int i = 0 ; i < line.length ; i++) {
			Cell cell = row.createCell(i + 1, CellType.NUMERIC);
			cell.setCellValue(line[i]);
		}
	}
	
	public void writeNamedLine(String name, int[] line) {
		
		Row row = ws.createRow(ws.getLastRowNum() + 1);
		
		Cell namecell = row.createCell(0, CellType.STRING);
		namecell.setCellValue(name);
		
		for (int i = 0 ; i < line.length ; i++) {
			Cell cell = row.createCell(i + 1, CellType.NUMERIC);
			cell.setCellValue(line[i]);
		}
	}
	
	public boolean save() throws IOException {
		return saveAs(tableFile);
	}
	
	public boolean saveAs(File newTableFile) throws IOException {
		BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(newTableFile, false));
		wb.write(writer);
		writer.close();
		return true;
	}
		
	public void close() throws IOException {
		if (wb != null) wb.close();
	}
		
	public static void main(String[] args) throws Exception {
		File tableFile = new File("/home/mdecorde/Téléchargements/txm-ressources-master-supports-corpus/supports/corpus/voeux-metadata/metadata.xlsx");
		// ReadExcel excel = new ReadExcel(tableFile, null);
//		ArrayList<ArrayList<String>> table = ReadExcel.toTable(tableFile, null);
//		for (ArrayList<String> line : table) {
//			System.out.println(line);
//		}
//
//		XSSFWorkbook wb2 = new XSSFWorkbook(tableFile);
//		XSSFSheet sheet = wb2.getSheetAt(0);
//		for (int i = sheet.getFirstRowNum() ; i <= sheet.getLastRowNum() ; i++) {
//			System.out.println(sheet.getRow(i));
//		}
//		wb2.close();
		Workbook wb = WorkbookFactory.create(tableFile, null, true);
//		Workbook wb = WorkbookFactory.create();
		wb.close();
	}
	
}
