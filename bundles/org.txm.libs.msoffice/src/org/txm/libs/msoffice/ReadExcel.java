package org.txm.libs.msoffice;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	Workbook wb = null;
	
	Sheet ws = null;
	
	String[] header = null;
	
	LinkedHashMap<String, String> record = null;
	
	int iRow = 0;
	
	int nRows;
	
	private File tableFile;
	
	public ReadExcel(File tableFile) throws EncryptedDocumentException, InvalidFormatException, IOException {
		this(tableFile, null, true);
	}
	
	public ReadExcel(File tableFile, String sheetName) throws EncryptedDocumentException, InvalidFormatException, IOException {
		this(tableFile, sheetName, true);
	}
	
	public ReadExcel(File tableFile, String sheetName, boolean readonly) throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		this.tableFile = tableFile;
		
		if (tableFile.exists() && tableFile.length() > 0) {
			wb = WorkbookFactory.create(tableFile, null, readonly);
			if (sheetName == null || sheetName.length() == 0) {
				ws = wb.getSheetAt(0);
			}
			else {
				ws = wb.getSheet(sheetName);
				if (ws == null) {
					ws = wb.getSheetAt(0);
				}
			}
			
			nRows = ws.getPhysicalNumberOfRows();
		}
		else {
			wb = new XSSFWorkbook();
			ws = wb.createSheet();
		}
	}
	
	/**
	 * one rule (regex test) per column
	 * all lines are processed
	 * 
	 * @param rules String: column name to test -> regex to match
	 */
	public boolean removeLines(HashMap<String, String> rules) {
		Row headers = ws.getRow(0);
		if (headers == null) return false;
		int colMax = headers.getLastCellNum();
		
		ArrayList<Integer> columnIdxToTest = new ArrayList<>();
		ArrayList<String> columnsTest = new ArrayList<>();
		for (int colIndex = 0; colIndex < colMax; colIndex++) {
			Cell cell = headers.getCell(colIndex);
			if (cell != null) {
				String value = cellToString(cell).trim();
				if (rules.containsKey(value)) {
					columnIdxToTest.add(colIndex);
					columnsTest.add(rules.get(value));
				}
			}
		}
		
		if (columnIdxToTest.size() == 0) {
			System.out.println("Error: no column found with name=" + rules.keySet());
			return false;
		}
		
		ArrayList<Integer> removed = new ArrayList<>();
		for (int rowIndex = 0; rowIndex < nRows; rowIndex++) {
			Row row = ws.getRow(rowIndex);
			if (row == null) continue;
			
			for (int i = 0; i < columnIdxToTest.size(); i++) {
				int colIndex = columnIdxToTest.get(i);
				
				Cell cell = row.getCell(colIndex);
				if (cell != null) {
					String value = cellToString(cell).trim();
					if (value.matches(columnsTest.get(i))) {
						removed.add(0, rowIndex);
						ws.removeRow(row);
						break; // next row
					}
				}
			}
		}
		
		// TODO very slow when there is a lot of lines
		for (int rowIndex : removed) {
			ws.shiftRows(rowIndex + 1, nRows, -1);
			nRows--;
		}
		
		return true;
	}
	
	/**
	 * search and replace lines utils
	 * 
	 * @param excel2
	 * @param lineRules
	 * @param columnsSelection
	 * @return
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public boolean extractTo(ReadExcel excel2, HashMap<String, String> lineRules, List<String> columnsSelection) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Sheet ws2 = excel2.ws;
		if (!extractTo(ws2, lineRules, columnsSelection)) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * extract lines and columns following rules.
	 * 
	 * don't forget to save the ReadeExcel object
	 * 
	 * @param columnsToCopy
	 * @return
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public boolean copyColumns(HashMap<String, String[]> columnsToCopy) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Row headers = ws.getRow(0);
		if (headers == null) return false;
		
		int colMax = headers.getLastCellNum();
		int nRows = ws.getPhysicalNumberOfRows();
		
		HashMap<String, Integer> searchColumns = new HashMap<>();
		for (int colIndex = 0; colIndex < colMax; colIndex++) {
			Cell cell = headers.getCell(colIndex);
			if (cell != null) {
				String value = cellToString(cell).trim();
				if (columnsToCopy.containsKey(value)) {
					searchColumns.put(value, colIndex);
				}
			}
		}
		
		if (searchColumns.keySet().size() != columnsToCopy.keySet().size()) {
			System.out.println("Error: not all columns found=" + searchColumns.keySet() + " of " + columnsToCopy.keySet());
			ArrayList<String> notFound = new ArrayList<>(columnsToCopy.keySet());
			notFound.removeAll(searchColumns.keySet());
			System.out.println("NOT FOUND: " + notFound);
			return false;
		}
		
		System.out.println("N Rows to update: " + nRows);
		int nRowWritten = 0;
		
		for (int rowIndex = 0; rowIndex < nRows; rowIndex++) { // update all rows, starting from the second row (first row is the header)
			Row row = ws.getRow(rowIndex);
			if (row == null) continue;
			
			int icol = 0;
			for (String col : columnsToCopy.keySet()) {
				for (String newcol : columnsToCopy.get(col)) {
					int colIndex = searchColumns.get(col);
					
					Cell cell = row.getCell(colIndex);
					if (cell != null) {
						
						Cell newCell = row.createCell(colMax + icol);
						if (rowIndex == 0) {
							newCell.setCellValue(newcol); // first column must be renamed
						}
						else {
							newCell.setCellValue(cellToString(cell).trim());
						}
					}
					icol++;
				}
			}
			
			nRowWritten++;
		}
		
		System.out.println("" + nRowWritten + " rows updated.");
		
		return nRowWritten > 0;
	}
	
	public boolean formatDateColumns(List<String> columns, String dateformat) {
		try {
			java.text.SimpleDateFormat parser = new java.text.SimpleDateFormat(dateformat);
			
			Row headers = ws.getRow(0);
			if (headers == null) return false;
			int colMax = headers.getLastCellNum(); // current number of columns
			int nRows = ws.getPhysicalNumberOfRows();
			
			HashMap<String, Integer> searchColumns = new HashMap<>();
			for (int colIndex = 0; colIndex < colMax; colIndex++) {
				Cell cell = headers.getCell(colIndex);
				if (cell != null) {
					String value = cellToString(cell).trim();
					if (columns.contains(value)) {
						searchColumns.put(value, colIndex);
					}
				}
			}
			
			if (searchColumns.keySet().size() != columns.size()) {
				System.out.println("Error: not all columns found=" + searchColumns.keySet() + " of " + columns);
				ArrayList<String> notFound = new ArrayList<>(columns);
				notFound.removeAll(searchColumns.keySet());
				System.out.println("NOT FOUND: " + notFound);
				return false;
			}
			
			for (String column : columns) {
				
				int colIndex = searchColumns.get(column);
				for (int rowIndex = 0; rowIndex < nRows; rowIndex++) { // update all rows, starting from the second row (first row is the header)
					Row row = ws.getRow(rowIndex);
					if (row == null) continue;
					
					Cell cell = row.getCell(colIndex);
					int icol = 0; // number of columns written for this row
					
					if (cell != null) {
						
						Cell newCell = null;
						if (rowIndex == 0) { // header line
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(column + "-jour-semaine"); // first row must be renamed
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(column + "-jour"); // first row must be renamed
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(column + "-mois"); // first row must be renamed
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(column + "-annee"); // first row must be renamed
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(column + "-tri"); // first row must be renamed
						}
						else {
							
							String rawValue = cellToString(cell).trim();
							Date date = parser.parse(rawValue);
							String joursemaine = new java.text.SimpleDateFormat("EEEE").format(date);
							String jour = new java.text.SimpleDateFormat("dd").format(date);
							String moi = new java.text.SimpleDateFormat("MM").format(date);
							String annee = new java.text.SimpleDateFormat("yyyy").format(date);
							String tri = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date);
							
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(joursemaine);
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(jour);
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(moi);
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(annee);
							newCell = row.createCell(colMax + icol++);
							newCell.setCellValue(tri);
						}
					}
				}
				
				colMax += 5; // 5 new columns written
			}
		}
		catch (ParseException e) {
			System.out.println("Error: while formating date columns: " + e);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * extract lines and columns following rules.
	 * 
	 * don't forget to save the ReadeExcel object
	 * 
	 * @param searchAndReplaceRules
	 * @return
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public boolean searchAndReplaceInLines(HashMap<String, String[]> searchAndReplaceRules) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Row headers = ws.getRow(0);
		if (headers == null) return false;
		
		int colMax = headers.getLastCellNum();
		int nRows = ws.getPhysicalNumberOfRows();
		
		HashMap<String, Integer> searchColumns = new HashMap<>();
		for (int colIndex = 0; colIndex < colMax; colIndex++) {
			Cell cell = headers.getCell(colIndex);
			if (cell != null) {
				String value = cellToString(cell).trim();
				if (searchAndReplaceRules.containsKey(value)) {
					searchColumns.put(value, colIndex);
				}
			}
		}
		
		if (searchColumns.keySet().size() != searchAndReplaceRules.keySet().size()) {
			System.out.println("Error: not all columns found=" + searchColumns.keySet() + " of " + searchAndReplaceRules.keySet());
			ArrayList<String> notFound = new ArrayList<>(searchAndReplaceRules.keySet());
			notFound.removeAll(searchColumns.keySet());
			System.out.println("NOT FOUND: " + notFound);
			return false;
		}
		
		System.out.println("N Rows to update: " + nRows);
		int nRowWritten = 0;
		
		for (int rowIndex = 0; rowIndex < nRows; rowIndex++) { // update all rows, starting from the second row (first row is the header)
			Row row = ws.getRow(rowIndex);
			if (row == null) continue;
			
			boolean change = false;
			for (String col : searchAndReplaceRules.keySet()) {
				int colIndex = searchColumns.get(col);
				
				Cell cell = row.getCell(colIndex);
				if (cell != null) {
					String value = cellToString(cell).trim();
					String[] replace = searchAndReplaceRules.get(col);
					String newValue = value.replaceAll(replace[0], replace[1]);
					if (!value.equals(newValue)) {
						cell.setCellValue(newValue);
						change = true;
					}
				}
			}
			
			if (change) {
				nRowWritten++;
			}
		}
		
		System.out.println("" + nRowWritten + " rows updated.");
		
		return true;
	}
	
	/**
	 * one rule (regex test) per column
	 * all lines are processed
	 * 
	 * @param lineRules String: column name to test -> regex to match
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public boolean extractTo(Sheet ws2, HashMap<String, String> lineRules, List<String> columnsSelection) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Row headers = ws.getRow(0);
		if (headers == null) return false;
		
		int colMax = headers.getLastCellNum();
		
		ArrayList<Integer> columnIdxToTest = new ArrayList<>();
		ArrayList<Integer> columnIdxToWrite = new ArrayList<>();
		ArrayList<String> columns = new ArrayList<>();
		ArrayList<String> columnsTest = new ArrayList<>();
		for (int colIndex = 0; colIndex < colMax; colIndex++) {
			Cell cell = headers.getCell(colIndex);
			if (cell != null) {
				String value = cellToString(cell).trim();
				columns.add(value);
				if (lineRules.containsKey(value)) {
					columnIdxToTest.add(colIndex);
					columnsTest.add(lineRules.get(value));
				}
				
				if (columnsSelection.contains(value)) {
					columnIdxToWrite.add(colIndex);
				}
			}
		}
		
		if (columnIdxToTest.size() == 0) {
			System.out.println("Error: no column found with name=" + lineRules.keySet());
			return false;
		}
		
		if (columnIdxToWrite.size() != columnsSelection.size()) {
			System.out.println("Error: not all columns found=" + columnIdxToWrite + " of " + columnsSelection);
			ArrayList<String> notFound = new ArrayList<>(columnsSelection);
			notFound.removeAll(columns);
			System.out.println("NOT FOUND: " + notFound);
			return false;
		}
		
		Row row2 = ws2.createRow(0);
		int nCell = 0;
		for (int iCol : columnIdxToWrite) {
			Cell cell2 = row2.createCell(nCell);
			cell2.setCellValue(cellToString(headers.getCell(iCol)).trim());
			nCell++;
		}
		
		int nRowWritten = 1;
		for (int rowIndex = 0; rowIndex < nRows; rowIndex++) {
			Row row = ws.getRow(rowIndex);
			if (row == null) continue;
			
			for (int i = 0; i < columnIdxToTest.size(); i++) {
				int colIndex = columnIdxToTest.get(i);
				
				Cell cell = row.getCell(colIndex);
				if (cell != null) {
					String value = cellToString(cell).trim();
					if (value.matches(columnsTest.get(i))) {
						
						// write new line in Sheet ws2
						row2 = ws2.createRow(nRowWritten);
						nCell = 0;
						for (int iCol : columnIdxToWrite) {
							Cell cell2 = row2.createCell(nCell);
							cell2.setCellValue(cellToString(row.getCell(iCol)));
							nCell++;
						}
						nRowWritten++;
						
						break; // next row
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param rules String:old column name -> String:new column name
	 */
	private boolean renameColumns(HashMap<String, String> rules) {
		Row headers = ws.getRow(0);
		if (headers == null) return false;
		
		int colMax = headers.getLastCellNum();
		for (int colIndex = 0; colIndex < colMax; colIndex++) {
			Cell cell = headers.getCell(colIndex);
			if (cell != null) {
				String value = cellToString(cell).trim();
				if (rules.containsKey(value)) {
					String s = rules.get(value);
					cell.setCellValue(s);
				}
			}
		}
		return true;
	}
	
	public boolean save() throws IOException {
		return saveAs(tableFile);
	}
	
	public boolean saveAs(File newTableFile) throws IOException {
		BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(newTableFile, false));
		wb.write(writer);
		writer.close();
		return true;
	}
	
	public boolean readHeaders() {
		
		if (nRows == 0) {
			header = new String[0];
			return header != null;
		}
		
		Row firstRow = ws.getRow(0);
		int colMax = firstRow.getLastCellNum();
		
		header = new String[colMax];
		for (int it = 0; it < colMax; it++) {
			Cell c = firstRow.getCell(it);
			if (c == null) { // virtual cell ?
				header[it] = "";
			} else {
				header[it] = c.getStringCellValue();
			}
		}
		int it = header.length - 1;
		for ( ; it >= 0 ; it--) {
			if (header[it].length() > 0) break;
		}
		if (it != header.length - 1) { // cut tail
			System.out.println("Warning: cut tail of empty values in header at:" +(it+1)+" (length was: "+header.length+")");
			header = Arrays.copyOfRange(header, 0, it+1);
		}
		if (iRow == 0) iRow = 1; // skip first line
		return true;
	}
	
	public boolean readRecord() {
		if (iRow < nRows) {
			_getRecord();
			iRow++; // for next
			return true;
		}
		else { // end of file
			record = null;
			return false;
		}
	}
	
	public String[] getHeaders() {
		return header;
	}
	
	public LinkedHashMap<String, String> getRecord() {
		return record;
	}
	
	public String get(String h) {
		if (record == null) return null;
		
		return record.get(h);
	}
	
	protected void _getRecord() {
		record = new LinkedHashMap<>();
		Row row = ws.getRow(iRow);
		if (row == null) return;
		
		for (int colIndex = 0; colIndex < header.length; colIndex++) {
			String col = header[colIndex];
			Cell cell = row.getCell(colIndex);
			if (cell != null) {
				String value = cellToString(cell).trim();
				if (value == null) {
					record.put(col, "");
				}
				else {
					record.put(col, value);
				}
			}
			else {
				record.put(col, "");
			}
		}
		
		for (String k : record.keySet())
			if (record.get(k) == null) System.out.println("ERROR null value with " + k);
	}
	
	public void close() throws IOException {
		if (wb != null) wb.close();
	}
	
	/**
	 * 
	 * @param inputFile
	 * @param sheetName
	 * @return list of lines (line = list of cells) with values converted to String - the file is completely read
	 */
	public static ArrayList<ArrayList<String>> toTable(File inputFile, String sheetName) {
		
		ArrayList<ArrayList<String>> data = new ArrayList<>();
		
		if (!inputFile.canRead()) {
			System.out.println("** Excel2XML: '" + inputFile.getName() + "' file not readable. Aborting.");
			return data;
		}
		
		try {
			Workbook wb = WorkbookFactory.create(inputFile, null, true);
			Sheet ws;
			if (sheetName == null || sheetName.length() == 0) {
				ws = wb.getSheetAt(0);
			}
			else {
				ws = wb.getSheet(sheetName);
				if (ws == null) {
					ws = wb.getSheetAt(0);
				}
			}
			
			if (ws == null) {
				System.out.println("** Excel2XML: no sheet found. Aborting.");
				return data;
			}
			
			int nRows = ws.getPhysicalNumberOfRows();
			if (nRows == 0) {
				System.out.println("** Excel2XML: no row to process. Aborting.");
				return null;
			}
			
			Row firstRow = ws.getRow(0);
			int colMax = firstRow.getLastCellNum();
			
			ArrayList<String> headers = new ArrayList<>();
			for (int it = 0; it < colMax; it++) {
				headers.add(firstRow.getCell(it).getStringCellValue());
			}
			
			for (int rowIndex = 0; rowIndex < nRows; rowIndex++) {
				Row row = ws.getRow(rowIndex);
				if (row == null) continue;
				
				ArrayList<String> dataLine = new ArrayList<>();
				data.add(dataLine);
				for (int colIndex = 0; colIndex < colMax; colIndex++) {
					Cell cell = row.getCell(colIndex);
					if (cell != null) {
						String value = cellToString(cell).trim();
						dataLine.add(value);
					}
					else {
						dataLine.add("");
					}
				}
			}
			wb.close();
		}
		catch (Exception e) {
			System.out.println("** Excel2XML: unable to read input file. Aborting.");
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
			return null;
		}
		
		return data;
	}
	
	/**
	 * 
	 * @param cell If null returns ""
	 * @return always a String starting with '#' if an error occurs
	 */
	public static String cellToString(Cell cell) {
		if (cell == null) return "";
		
		switch (cell.getCellType()) {
			case FORMULA:
				return cell.getCellFormula();
			case NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-dd-MM");
					return dateFormat.format(cell.getDateCellValue());
				}
				else {
					if (cell instanceof XSSFCell xssfCell) {
						
						String raw = xssfCell.getRawValue();
						Double value = cell.getNumericCellValue();
						Integer i = null;
						try {i = Integer.parseInt(raw);} catch(Exception e) {}
						
						if (i == null) {
							return value.toString();
						} else {
							return i.toString();
						}
					} else {
//						if (cell.toString())
						return cell.toString();
					}
				}
			case STRING:
				return cell.getRichStringCellValue().toString();
			case BLANK:
				return "";
			case BOOLEAN:
				return cell.getBooleanCellValue() ? "TRUE" : "FALSE";
			case ERROR:
				return "#error";
			default:
				return "#typeerror: "+cell.getCellType();
		}
	}
	
	
	public static void main(String[] args) throws Exception {
		File tableFile = new File("/home/mdecorde/Téléchargements/txm-ressources-master-supports-corpus/supports/corpus/voeux-metadata/metadata.xlsx");
		// ReadExcel excel = new ReadExcel(tableFile, null);
//		ArrayList<ArrayList<String>> table = ReadExcel.toTable(tableFile, null);
//		for (ArrayList<String> line : table) {
//			System.out.println(line);
//		}
//
//		XSSFWorkbook wb2 = new XSSFWorkbook(tableFile);
//		XSSFSheet sheet = wb2.getSheetAt(0);
//		for (int i = sheet.getFirstRowNum() ; i <= sheet.getLastRowNum() ; i++) {
//			System.out.println(sheet.getRow(i));
//		}
//		wb2.close();
		Workbook wb = WorkbookFactory.create(tableFile, null, true);
//		Workbook wb = WorkbookFactory.create();
		wb.close();
	}
	
	public static void mainAF(String[] args) throws Exception {
		// ArrayList<ArrayList<String>> data = toTable(new File("/home/mdecorde/xml/ruscorpora1m-test/metadata.xlsx"), null);
		// if (data.size() == 0) {
		// System.out.println("no data.");
		// } else {
		// System.out.println(data);
		// }
		
		File tableFile = new File("/home/mdecorde/TEMP/ANTRACT/AF/all.xlsx");
		File table2File = null;
		
		System.out.println("open...");
		ReadExcel excel = new ReadExcel(tableFile, null);
		
		HashMap<String, String> lineRules = new HashMap<>(); // line tests to select line to keep
		List<String> columnsSelection; // list of columns to keep
		HashMap<String, String> columnsToCopy = new HashMap<>();
		HashMap<String, String> columnsToRenameRules = new HashMap<>();
		HashMap<String, String[]> searchAndReplaceRules = new HashMap<>();
		// //emissions
		// table2File = new File("/home/mdecorde/TEMP/ANTRACT/AF/emissions.xlsx");
		// columnsSelection = Arrays.asList(
		// "Identifiant de la notice", "Titre propre", "Notes du titre", "Date de diffusion", "Durée", "Nom fichier segmenté (info)", "antract_video",
		// "antract_debut","antract_fin","antract_duree","antract_tc_type","antract_tc_date");
		// lineRules.put("Type de notice", "Notice sommaire");
		// columnsToRenameRules.put("Identifiant de la notice", "id");
		//
		// columnsToCopy.put("Notes du titre", "subtitle"); // not working yet
		// columnsToCopy.put("Titre propre", "title"); // not working yet
		// columnsToCopy.put("Date de diffusion", "text-order"); // not working yet
		// searchAndReplaceRules.put("text-order", new String[] {"../../....", "$3$2$1"}); // not working yet
		
		// sujets
		table2File = new File("/home/mdecorde/TEMP/ANTRACT/AF/sujets.xlsx");
		columnsSelection = Arrays.asList(
				"Identifiant de la notice", "Titre propre", "Notes du titre", "Lien notice principale",
				"Date de diffusion", "Type de date", "Durée", "Genre", "Langue VO / VE", "Nature de production", "Producteurs (Aff.)", "Thématique",
				"Nom fichier segmenté (info)", "antract_video", "antract_debut", "antract_fin", "antract_duree", "antract_tc_type", "antract_tc_date",
				"Résumé", "Séquences", "Descripteurs (Aff. Lig.)", "Générique (Aff. Lig.)");
		lineRules.put("Type de notice", "Notice sujet");
		
		table2File.delete();
		ReadExcel excel2 = new ReadExcel(table2File, null);
		if (!excel.extractTo(excel2, lineRules, columnsSelection)) {
			System.out.println("FAIL");
			return;
		}
		
		System.out.println("copying column: " + columnsToCopy.size());
		// excel2.copyColumns(columnsToCopy);
		
		System.out.println("search&replace column: " + searchAndReplaceRules.size());
		// excel2.searchAndReplaceInLines(searchAndReplaceRules);
		
		System.out.println("renaming column: " + columnsToRenameRules.size());
		excel2.renameColumns(columnsToRenameRules);
		
		System.out.println("saving&closing...");
		excel2.save();
		excel2.close();
		excel.close();
		System.out.println("done.");
	}
}
