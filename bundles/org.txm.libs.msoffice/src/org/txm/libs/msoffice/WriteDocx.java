package org.txm.libs.msoffice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 * Simple API to write DOCX documents
 * 
 * Usages : 1) import org.txm.libs.msoffice.WriteDocx
 * 2a) WriteDocx.write(docFile, "LLALALALALA");
 * 2b) WriteDocx.writeParagraphs(docFile, "LLALA\nLALALA"); // 2 paragraphs
 * 2c) WriteDocx.writeParagraphs(docFile, new String[]{"LLALA","LALALA"}); // 2 paragraphs
 *  
 * @author mdecorde
 *
 */
public class WriteDocx {

	/**
	 * Create a DOCX document. end the writing with the doc.close() method
	 * 
	 * @param docfile
	 * @param content
	 * @throws IOException
	 */
	public static XWPFDocument newDocument(File docfile) throws IOException {
		
		return new XWPFDocument();
	}
	
	/**
	 * Write a text into ONE paragraph
	 * 
	 * @param docfile
	 * @param content
	 * @throws IOException
	 */
	public static void write(File docfile, String content) throws IOException {
		
		XWPFDocument document = new XWPFDocument();
		XWPFParagraph tmpParagraph = document.createParagraph();
		XWPFRun tmpRun = tmpParagraph.createRun();
		tmpRun.setText(content);
		tmpRun.setFontSize(18);
		document.write(new FileOutputStream(docfile));
		document.close() ;
	}

	/**
	 * Write a text into SEVERAL paragraphs, a paragraph is created when a newline is encountered
	 * 
	 * @param docfile
	 * @param content
	 * @throws IOException
	 */
	public static void writeParagraphs(File docfile, String content) throws IOException {
		
		writeParagraphs(docfile, content.split("\n"));
	}

	/**
	 * Write a text into SEVERAL paragraphs
	 * 
	 * @param docfile
	 * @param content
	 * @throws IOException
	 */
	public static void writeParagraphs(File docfile, String[] paragraphs) throws IOException {
		
		XWPFDocument document = new XWPFDocument();

		for (String p : paragraphs) {
			XWPFParagraph tmpParagraph = document.createParagraph();
			XWPFRun tmpRun = tmpParagraph.createRun();
			tmpRun.setText(p);
			tmpRun.setFontSize(18);
		}

		document.write(new FileOutputStream(docfile));
		document.close() ;
	}
}
