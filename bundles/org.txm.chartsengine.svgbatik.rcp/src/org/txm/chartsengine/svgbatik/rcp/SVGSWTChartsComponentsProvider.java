package org.txm.chartsengine.svgbatik.rcp;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.swt.ChartComposite;
import org.txm.chartsengine.svgbatik.rcp.swt.SVGChartComposite;

/**
 * Batik SVG library charts component provider.
 * 
 * @author sjacquot
 *
 */
public class SVGSWTChartsComponentsProvider extends SWTChartsComponentsProvider {


	/**
	 * 
	 */
	public SVGSWTChartsComponentsProvider() {
		super();
	}


	@Override
	public ChartComposite createComposite(ChartEditor chartEditor, Composite parent) {
		return new SVGChartComposite(chartEditor, parent);
	}


	@Override
	public ArrayList<String> getChartsEngineSupportedExportFileFormats() {
		return this.chartsEngine.getSupportedOutputFileFormats();
	}

}
