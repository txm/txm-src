// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.chartsengine.svgbatik.rcp.swing;


import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Dimension2D;
import java.util.List;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.gvt.AbstractImageZoomInteractor;
import org.apache.batik.swing.gvt.AbstractPanInteractor;
import org.apache.batik.swing.gvt.AbstractResetTransformInteractor;
import org.apache.batik.swing.gvt.AbstractZoomInteractor;
import org.apache.batik.swing.gvt.Interactor;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.events.ChartKeyListener;

/**
 * Batik SVG canvas swing component.
 * 
 * @author sjacquot
 * 
 */
public class SVGCanvas extends JSVGCanvas implements IChartComponent {

	/**
	 * The linked editor.
	 */
	protected ChartEditor editor;

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -4381954167495751486L;

	/**
	 * Current zoom value.
	 */
	int zoom = 0;

	/**
	 * Key pressed pan factor.
	 */
	protected double keyPressedPanFactor = 0.5;


	/**
	 * Instantiates a new SVG panel.
	 *
	 */
	public SVGCanvas() {
		// super(new BorderLayout());
		super(null, true, false);

		// this.svgCanvas = new JSVGCanvas(null, true, false);
		// this.add("Center", svgCanvas); //$NON-NLS-1$

		// TODO: needed for automatic refresh after dynamically modifying the SVG DOM
		// svgCanvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);

		List<Interactor> list = this.getInteractors();
		list.clear();


		// FIXME : test for constant text label font size (only work with the SVG produced by the SVGGraphicsDevice of R, not with CairoSVG because it doesn't produces text tag, only glyph paths)
		// svgCanvas.addMouseWheelListener(new ___AbstractImageZoomInteractor());

		list.add(zoomInteractor);
		list.add(imageZoomInteractor);
		list.add(panInteractor);
		list.add(resetTransformInteractor);

		this.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {


				// FIXME : test zoom by viewbox attribute
				// final MouseWheelEvent mwe = e;
				// svgCanvas.getUpdateManager().getUpdateRunnableQueue().invokeLater(new Runnable() {
				//
				// @Override
				// public void run() {
				// // TODO Auto-generated method stub
				//
				// //System.out.println("SVGPanel.SVGPanel(...).new MouseWheelListener() {...}.mouseWheelMoved(): " + ((Element)getSVGDocument().getFirstChild()).getAttribute("viewBox"));
				//
				// String[] viewBox = ((Element)getSVGDocument().getFirstChild()).getAttribute("viewBox").split(" ");
				//
				// int x = Integer.valueOf(viewBox[0]);
				// int y = Integer.valueOf(viewBox[1]);
				// int width = Integer.valueOf(viewBox[2]);
				// int height = Integer.valueOf(viewBox[3]);
				//
				// if (mwe.getUnitsToScroll() < 0) {
				//
				// x += 10;
				// y += 10;
				// width -= 10;
				// height -= 10;
				//
				//
				//
				// }
				// else {
				//
				// x -= 10;
				// y -= 10;
				// width += 10;
				// height += 10;
				//
				// }
				// ((Element)getSVGDocument().getFirstChild()).setAttribute("viewBox", x + " " + y + " " + width + " " + height);
				// }
				// });



				// FIXME : tests for constant text font size
				// FIXME : only work with SVG generated with text tag
				// FIXME : do not work properly
				// NodeList elements = svgCanvas.getSVGDocument().getElementsByTagName("text");
				// Element element;
				// for (int i = 0; i < elements.getLength(); i++) {
				// element = (Element) elements.item(i);
				//
				// // TODO : Debug : Affichage des attributs de l'�l�ment
				//// NamedNodeMap attributes = element.getAttributes();
				//// int numAttrs = attributes.getLength();
				//// System.out.println("");
				//// for (int j = 0; j < numAttrs; j++) {
				////
				//// Attr attr = (Attr) attributes.item(j);
				////
				//// String attrName = attr.getNodeName();
				//// String attrValue = attr.getNodeValue();
				//// System.out.print(" " + attrName + "=" + attrValue);
				//// }
				//
				// // TODO : test de modif de l'attribut font-size du texte
				// // TODO : Fonctionne mais prend beaucoup plus de ressources que la modification directe de la shape GVT
				// element.removeAttribute("style");
				// //element.setAttribute("style", "font-size:" + (8 * 1d / zoom));
				//
				// if(zoom != 0) {
				// double fontSize = (8 * 1d / zoom);
				// element.setAttribute("font-size", fontSize + "px");
				// }
				//
				// }


				// FIXME : old version by matrix transformation
				if (e.getUnitsToScroll() < 0) {
					zoom(e.getX(), e.getY(), true, true, true);
				}
				else {
					zoom(e.getX(), e.getY(), false, true, true);
				}


			}
		});

		// add default chart key listener
		this.addKeyListener(new ChartKeyListener(this, 10.5));

		// this.setChartEditor(editor);


	}

	// ----------------------------------------------------------------------
	// Interactors
	// ----------------------------------------------------------------------

	/**
	 * An interactor to perform a zoom.
	 * <p>
	 * Binding: BUTTON1 + CTRL Key
	 * </p>
	 */
	protected Interactor zoomInteractor = new AbstractZoomInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_PRESSED &&
					(mods & InputEvent.BUTTON1_MASK) != 0 &&
					(mods & InputEvent.CTRL_MASK) != 0;
		}
	};

	/**
	 * An interactor to perform a realtime zoom.
	 * <p>
	 * Binding: BUTTON3 + SHif T Key
	 * </p>
	 */
	protected Interactor imageZoomInteractor = new AbstractImageZoomInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_PRESSED &&
					(mods & InputEvent.BUTTON3_MASK) != 0 &&
					(mods & InputEvent.SHIFT_MASK) != 0;
		}
	};

	/**
	 * An interactor to perform a translation.
	 * <p>
	 * Binding: BUTTON1 + SHif T Key
	 * </p>
	 */
	protected Interactor panInteractor = new AbstractPanInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_PRESSED &&
					(mods & InputEvent.BUTTON1_MASK) != 0 &&
					(mods & InputEvent.SHIFT_MASK) != 1;
		}
	};

	/**
	 * An interactor to reset the rendering transform.
	 * <p>
	 * Binding: CTRL+SHif T+BUTTON3
	 * </p>
	 */
	protected Interactor resetTransformInteractor = new AbstractResetTransformInteractor() {

		@Override
		public boolean startInteraction(InputEvent ie) {
			int mods = ie.getModifiers();
			return ie.getID() == MouseEvent.MOUSE_CLICKED &&
					(mods & InputEvent.BUTTON2_MASK) != 0 &&
					(mods & InputEvent.SHIFT_MASK) != 1 &&
					(mods & InputEvent.CTRL_MASK) != 1;
		}
	};

	@Override
	public void resetView() {
		this.zoom = 0;
		// old method
		// AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
		// trans.scale(1, 1);
		// this.setRenderingTransform(trans);

		// new method
		this.resetRenderingTransform();

	}



	@Override
	public void zoom(double x, double y, boolean zoomIn, boolean range, boolean domain) {

		// FIXME: zoom tests. reset the pan

		// TODO Auto-generated method stub
		// x += this.svgCanvas.getRenderingTransform().getTranslateX();
		// y += this.svgCanvas.getRenderingTransform().getTranslateY();


		AffineTransform trans = AffineTransform.getTranslateInstance(x, y);

		double scaleX = this.getRenderingTransform().getScaleX();
		double scaleY = this.getRenderingTransform().getScaleY();
		if (zoomIn) {
			trans.scale(this.getRenderingTransform().getScaleX() * 1.1, this.getRenderingTransform().getScaleY() * 1.1);
			if (range) scaleX *= 1.1;
			if (domain) scaleY *= 1.1;
		}
		else {
			if (range) scaleX *= 0.9;
			if (domain) scaleY *= 0.9;
		}
		trans.scale(scaleX, scaleY);
		// trans.translate(-x + this.svgCanvas.getRenderingTransform().getTranslateX(), -y + this.svgCanvas.getRenderingTransform().getTranslateY());

		// System.out.println("SVGPanel.zoom2(): " + this.svgCanvas.getRenderingTransform().getTranslateX());

		trans.translate(-x, -y);


		if (trans.getDeterminant() != 0) {
			this.setRenderingTransform(trans);
		}
	}

	// FIXME: tests zoom
	/**
	 * Zoom in or zoom out the panel at the specified origin point coordinates.
	 * 
	 * @param x
	 * @param y
	 * @param zoom
	 */
	public void zoom(double x, double y, int zoom) {

		// FIXME: SJ: the zoom does nothing when zoom is equal to 0 and 1.

		AffineTransform trans = AffineTransform.getTranslateInstance(x, y);

		int dy = zoom;
		double s;
		if (dy < 0) {
			s = -1f / dy;
		}
		else {
			s = dy;
		}

		trans.scale(s, s);

		trans.translate(-x, -y);

		if (trans.getDeterminant() != 0) {
			this.setRenderingTransform(trans);
		}
	}


	@Override
	public void pan(double srcX, double srcY, double dstX, double dstY, double panFactor) {

		AffineTransform trans = this.getRenderingTransform();
		Dimension2D size = this.getSVGDocumentSize(); // adapt the pan to the SVG size
		trans.translate(-dstX * panFactor * size.getWidth(), dstY * panFactor * size.getHeight());
		this.setRenderingTransform(trans);

	}

	@Override
	public ChartEditor getChartEditor() {
		return this.editor;
	}

	@Override
	public void setChartEditor(ChartEditor editor) {
		this.editor = editor;
	}

	@Override
	public void updateMouseOverItem(MouseEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void squareOff() {
		// not needed in this implementation
	}

}
