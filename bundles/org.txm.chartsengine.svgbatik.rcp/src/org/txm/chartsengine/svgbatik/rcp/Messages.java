package org.txm.chartsengine.svgbatik.rcp;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$

	public static String CantLoadSVGDocumentFromFileP0;

	public static String SVGFileDoesntExist;

	public static String SVGFileP0IsEmpty;
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
