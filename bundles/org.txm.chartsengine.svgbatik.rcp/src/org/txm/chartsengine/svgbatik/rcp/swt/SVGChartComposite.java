package org.txm.chartsengine.svgbatik.rcp.swt;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import org.apache.batik.transcoder.AbstractTranscoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.svg2svg.SVGTranscoder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.swt.SwingChartComposite;
import org.txm.chartsengine.svgbatik.rcp.Messages;
import org.txm.chartsengine.svgbatik.rcp.swing.SVGCanvas;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.logger.Log;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

import com.lowagie.text.Document;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;


/**
 * Batik SVG SWT Chart Composite.
 * 
 * @author sjacquot
 *
 */
public class SVGChartComposite extends SwingChartComposite {


	/**
	 * The displayed chart SVG file. Null while loadChart(chart) is not called.
	 */
	protected File file;


	/**
	 * Creates a Batik SVG composite.
	 * 
	 * @param parent
	 */
	public SVGChartComposite(ChartEditor<? extends ChartResult> chartEditor, Composite parent) {
		super(chartEditor, parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		this.file = null;
	}


	@Override
	public void loadChart(Object chart) {

		ChartsEngine engine = chartEditor.getChartsEngine();
		ChartResult result = chartEditor.getResult();
		int w = this.getSize().x;
		int h = this.getSize().y;

		// an alternative way is to always call this in a Display.asyncExec() to ensure the widget size is computed -> slow the display
		if (w == 0 || h == 0) { // if the composite is not drawn (editor is opening), the chart file size is set to 600*400 by default, TODO resetView() should show-zoom the chart
			w = 600;
			h = 400;
		}
		File file = engine.exportChartResultToFile(result, w, h, ChartsEngine.OUTPUT_FORMAT_SVG);

		this.loadSVGDocument(file);
	}



	/**
	 * Loads a SVG document from the specified file.
	 * 
	 * @param file the file to load
	 */
	// FIXME: SJ: we should use Batik listener system to check if the file has been well loaded
	public void loadSVGDocument(final File file) {
		if (file == null) {
			Log.severe(Messages.SVGFileDoesntExist);
			return;
		}
		if (file.length() == 0) {
			Log.severe(TXMCoreMessages.bind(Messages.SVGFileP0IsEmpty, file));
			return;
		}

		this.file = file;


		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				try {
					Log.fine(TXMCoreMessages.bind("Loading SVG document from file: {0}...", file.getAbsolutePath())); //$NON-NLS-1$

					getSVGCanvas().loadSVGDocument(file.toURL().toExternalForm());

					Log.fine(TXMCoreMessages.bind("File {0} loaded.", file.getAbsolutePath())); //$NON-NLS-1$

				}
				catch (Exception e) {
					Log.severe(TXMCoreMessages.bind(Messages.CantLoadSVGDocumentFromFileP0, file));
					Log.printStackTrace(e);
				}

			}
		});
	}


	/**
	 * Gets the SVG document.
	 * 
	 * @return
	 */
	public SVGDocument getSVGDocument() {
		return this.getSVGCanvas().getSVGDocument();
	}

	/**
	 * Gets the SVG canvas chart component.
	 * Convenience method for getChartComponent().
	 * 
	 * @return the SVG canvas
	 */
	public SVGCanvas getSVGCanvas() {
		return (SVGCanvas) this.chartComponent;
	}



	@Override
	public void clearChartItemsSelection() {
		Log.severe("SVGChartComposite.clearChartItemsSelection(): not implemented."); //$NON-NLS-1$
	}


	@Override
	protected IChartComponent _createChartComponent() {
		return new SVGCanvas();
	}


	@Override
	public File exportView(File file, String fileFormat) {
		
		
		// Export the chart, keeping aspect ratio and according to the width export preference
		int outputImageWidth = ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS);
		int outputImageHeight = this.computeOutputImageHeight();
		

		
		// SVG
		if (fileFormat.equals(ChartsEngine.OUTPUT_FORMAT_SVG)) {



			// FIXME : Batik transcoder version
			// FIXME : the cropping to match view in SVG doesn't work
			try {
				// AbstractTranscoder imageTranscoder = this.getImageTranscoder(fileFormat);
				AbstractTranscoder imageTranscoder = new SVGTranscoder();


				// Transcoder configuration
				// imageTranscoder.addTranscodingHint(ImageTranscoder.KEY_WIDTH, new Integer(300));


				// SVG Cropping tests
				// Rectangle area = new Rectangle(200, 200, 300, 500); // FIXME : how to get the correct area after the charts has been zoomed or moved ?
				//// imageTranscoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, new Float(this.composite.getPanel().getWidth()));
				//// imageTranscoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, new Float(this.composite.getPanel().getHeight()));
				// imageTranscoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, new Float(20));
				// imageTranscoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, new Float(20));
				// imageTranscoder.addTranscodingHint(JPEGTranscoder.KEY_AOI, area);


				// Add a viewbox attribute
				// ((Element)this.composite.getSVGDocument().getFirstChild()).setAttribute("viewBox", "20 20 400 350"); // FIXME : how to get the correct area after the charts has been zoomed or moved
				// ?

				// Adjust dimensions from component
				((Element) this.getSVGDocument().getFirstChild()).setAttribute("width", String.valueOf(outputImageWidth)); //$NON-NLS-1$
				((Element) this.getSVGDocument().getFirstChild()).setAttribute("height", String.valueOf(outputImageHeight)); //$NON-NLS-1$


				TranscoderInput input = new TranscoderInput(this.getSVGDocument());

				// SVGTranscoder needs an OutputStreamWriter, not an OutputStream
				if (imageTranscoder instanceof SVGTranscoder) {

					FileWriter outputStreamWriter = new FileWriter(file);
					imageTranscoder.transcode(input, new TranscoderOutput(outputStreamWriter));

					outputStreamWriter.flush();
					outputStreamWriter.close();
				}
				// else {
				// OutputStream outputStream = new FileOutputStream(file);
				// TranscoderOutput output = new TranscoderOutput(outputStream);
				//
				// imageTranscoder.transcode(input, output);
				//
				// outputStream.flush();
				// outputStream.close();
				// }
			}
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (TranscoderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}



			// FIXME : SVG via Batik
			// FIXME : it saves the JPanel content as image raster tag in the SVG instead of creating real SVG shapes tag

			// // Get a DOMImplementation
			// DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
			//
			// String svgNS = "http://www.w3.org/2000/svg";
			// Document document = domImpl.createDocument(svgNS, "svg", null);
			//
			// SVGGraphics2D svgGenerator = new SVGGraphics2D(document);
			//
			// // Paint in the SVGGraphics2D implementation
			// //this.composite.getPanel().getSVGCanvas().paint(svgGenerator);
			// this.composite.getPanel().paint(svgGenerator);
			//
			// boolean useCSS = true;
			//
			// try {
			// Writer out = new FileWriter(file);
			// svgGenerator.stream(out, useCSS);
			// }
			// catch(UnsupportedEncodingException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// catch(SVGGraphics2DIOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// catch(IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//



			// FIXME : SVG via JFreeSVG : doesn't work
			// SVGGraphics2D g2 = new SVGGraphics2D(this.composite.getPanel().getWidth(), this.composite.getPanel().getHeight());
			//
			// // suppress shadow generation, because SVG is a vector format and
			// // the shadow effect is applied via bitmap effects...
			// g2.setRenderingHint(JFreeChart.KEY_SUPPRESS_SHADOW_GENERATION, true);
			// String svg = null;
			// this.composite.
			// getPanel().
			// paintAll(g2);
			// svg = g2.getSVGElement();
			//
			// if (file != null) {
			// BufferedWriter writer = null;
			// try {
			// writer = new BufferedWriter(new FileWriter(file));
			// writer.write("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
			// writer.write(svg + "\n");
			// writer.flush();
			// }
			// catch(IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// finally {
			// try {
			// if (writer != null) {
			// writer.close();
			// }
			// }
			// catch (IOException ex) {
			// throw new RuntimeException(ex);
			// }
			// }
			//
			// }



		}
		// PDF (using Lowagie iTtext lib)
		else if (fileFormat.equals(ChartsEngine.OUTPUT_FORMAT_PDF)) {

			Document document = new Document(new Rectangle(0, 0, outputImageWidth, outputImageWidth));
			try {

				PdfWriter writer = PdfWriter.getInstance(document, new BufferedOutputStream(new FileOutputStream(file)));
				document.open();
				PdfContentByte cb = writer.getDirectContent();

				Graphics2D g = cb.createGraphics(this.getSVGCanvas().getWidth(), this.getSVGCanvas().getHeight());
				this.getSVGCanvas().printAll(g);
				g.dispose();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			document.close();
		}
		// Raster parent export (using AWT/Swing methods)
		else {
			super.exportView(file, fileFormat);
		}

		return file;
	}


	@Override
	public ArrayList<String> getEditorSupportedExportFileFormats() {

		ArrayList<String> supportedExportFileFormats = new ArrayList<>(5);

		supportedExportFileFormats.add(ChartsEngine.OUTPUT_FORMAT_SVG); // Default format
		supportedExportFileFormats.add(ChartsEngine.OUTPUT_FORMAT_PDF);

		// Auto-populate supported output file raster formats from available image writers
		ChartsEngine.populateSupportedOutputRasterFileFormats(supportedExportFileFormats);


		return supportedExportFileFormats;

	}

}
