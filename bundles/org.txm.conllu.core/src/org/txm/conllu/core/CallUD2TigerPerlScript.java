package org.txm.conllu.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.runtime.IStatus;
import org.txm.core.engines.ScriptEngine;
import org.txm.core.engines.ScriptEnginesManager;
import org.txm.tigersearch.preferences.TigerSearchPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class CallUD2TigerPerlScript {

	public boolean canBuildTSFiles() {
		ScriptEngine engine = ScriptEnginesManager.getPerlEngine();
		if (engine == null) {
			Log.warning("No Perl script engine installed in TXM");
			return false;
		}
		if (!engine.isRunning()) {
			Log.warning("No Perl script engine ready in TXM. See Perl preferences.");
			return false;
		}
		return true;
	}

	public boolean convertCoNLLUFiles(File[] conlluFiles, String output_directory) throws IOException {

		if (conlluFiles.length == 0) return false;

		File tigerXMLDirectory = new File(output_directory, "tiger-xml"); //$NON-NLS-1$
		File conversionFile = new File(tigerXMLDirectory, "conversion.log"); //$NON-NLS-1$

		String driverFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DRIVER_FILENAME);
		File mainFullFile = new File(tigerXMLDirectory, driverFilename);

		File mainFile = new File(tigerXMLDirectory, "main.xml"); //$NON-NLS-1$
		File perlScript = BundleUtils.getFile("org.txm.conllu.core", "groovy", "/org/txm/scripts/importer/conllu/", "conll2tiger-ud.pl"); //$NON-NLS-1$
		System.out.println("PERL: " + perlScript); //$NON-NLS-1$
		DeleteDir.deleteDirectory(tigerXMLDirectory);
		//println "clean&mkdir $tigerXMLDirectory"
		tigerXMLDirectory.mkdir();

		//println "Converting..."

		String subcorpusList = "";

		ConsoleProgressBar cpb = new ConsoleProgressBar(conlluFiles.length);
		for (File f : conlluFiles) {
			cpb.tick();
			if (!f.getName().endsWith(".conllu")) { //$NON-NLS-1$
				continue;
			}

			try {
				//System.out.println("$f... "+"perl '$perlScript' -s 10000 -o '$tigerXMLDirectory' '$f'");
				ScriptEngine engine = ScriptEnginesManager.getPerlEngine();
				if (!engine.canExecute(perlScript)) {
					System.out.println("Error: Perl engine not correctly set to run: " + perlScript);
					return false;
				}
				ArrayList<String> cmd = new ArrayList<String>();
				cmd.addAll(Arrays.asList("-s", "10000", "-o", tigerXMLDirectory.getAbsolutePath(), f.getAbsolutePath())); //$NON-NLS-1$
				//System.out.println("Calling perl with: "+perlScript+" and "+cmd);
				IStatus status = engine.executeScript(perlScript, cmd);
				for (String line : IOUtils.getLines(mainFile, "UTF-8")) { //$NON-NLS-1$
					if (line.contains("<subcorpus ")) { //$NON-NLS-1$
						subcorpusList += line + "\n"; //$NON-NLS-1$
					}
				}
			}
			catch (Throwable t) {
				System.out.println("** Error: " + t + " at " + t.getStackTrace()[0]);
			}
		}
		cpb.done();

		//	println "subcorpus-list: "+subcorpusList
		String content = IOUtils.getText(mainFile, "UTF-8"); //$NON-NLS-1$
		content = content.replaceAll("<subcorpus .+\n", ""); //$NON-NLS-1$
		content = content.replaceAll("<body>", "<body>\n" + subcorpusList + "\n"); // get the last main.xml content and patch it with the subcorpus tags //$NON-NLS-1$
		IOUtils.setText(mainFullFile, content, "UTF-8"); //$NON-NLS-1$
		//mainFullFile.setText(mainFile.getText().replaceAll("<body/>", subcorpusList+"\n<body/>"));

		conversionFile.delete();
		mainFile.delete();

		//println "Done."
		return true;
	}
}
