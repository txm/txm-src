package org.txm.conllu.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class CoNLLUReader {

	public static final String[] UD_PROPERTY_NAMES = { "id", "form", "lemma", "upos", "xpos", "feats", "head", "deprel", "deps", "misc" }; //$NON-NLS-1$

	File coonluFile;

	private String textId;

	BufferedReader reader;

	int nLine = 0;

	private String line;

	public CoNLLUReader(File coonluFile, String textId) {
		this.coonluFile = coonluFile;
		this.textId = textId;

		if (textId == null || textId.length() == 0) { // no text id provided, using the conllu file name
			textId = coonluFile.getName().substring(0, coonluFile.getName().length() - 7);
		}
	}

	public Sentence readNext() throws IOException {
		if (reader == null) {
			reader = IOUtils.getReader(coonluFile);
			nLine = 0;
			line = null;
		}
		else if (!reader.ready()) {
			return null; // the reader is closed
		}

		Sentence sentence = new Sentence();
		sentence.newdoc_id = textId;

		if (line == null) {
			line = reader.readLine();
		}

		while (line != null) {

			nLine++;
			if (line.length() == 0) {
				line = reader.readLine();
				continue; // comment
			}

			if (line.startsWith("#")) { //$NON-NLS-1$
				if (line.startsWith("# sent_id = ")) { //$NON-NLS-1$

					// write previous sentence word properties
					if (sentence.nWords2 > 0) {
						return sentence; // the line field will be used as it is
					}

					sentence.sent_id = line.substring(12).trim();
				}
				else if (line.startsWith("# newdoc id = ")) { //$NON-NLS-1$
					sentence.newdoc_id = line.substring(14).trim();
				}
				else if (line.startsWith("# newpar id = ")) { //$NON-NLS-1$
					sentence.newpar_id = line.substring(14).trim();
				}
				else {
					// nothing for now
				}

				line = reader.readLine();
				continue; // comment
			}

			String[] split = line.split("\t", 10); //$NON-NLS-1$
			if (split.length < 10) {
				Log.warning("Error: line " + nLine + " : " + line + " -> " + Arrays.toString(split) + " len=" + split.length);
				line = reader.readLine();
				continue; // comment
			}

			LinkedHashMap<String, String> properties = new LinkedHashMap<>();
			for (int i = 0; i < split.length; i++) {
				properties.put(UD_PROPERTY_NAMES[i], split[i]); // add the property name using the prefix ; XML-TXM types are prefixed with '#'
			}

			if ("1".equals(properties.get(UD_PROPERTY_NAMES[0])) && sentence.nWords2 > 0) { //$NON-NLS-1$
				return sentence; // this is a new sentence !!, return the current one
			}

			String misc = split[9];
			String[] miscValues = misc.split("\\|"); //$NON-NLS-1$
			for (String miscEntry : miscValues) {
				if (miscEntry.contains("=")) { //$NON-NLS-1$
					String[] s = miscEntry.split("=", 2); //$NON-NLS-1$
					properties.put(s[0], s[1]);
				}
			}

			sentence.put(properties.get(UD_PROPERTY_NAMES[0]), properties);
			sentence.nWords2++;

			line = reader.readLine();
		}
		return sentence;
	}

	/**
	 * 
	 * @return the sentences as an ArrayList
	 * 
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	public Sentences readAll() throws IOException, XMLStreamException {

		// reset the reader
		if (reader != null) {
			reader.close();
		}
		reader = IOUtils.getReader(coonluFile);
		nLine = 0;
		line = null;

		Sentences sentences = new Sentences();

		Sentence sentence = readNext();
		while (sentence != null) {
			sentences.add(sentence);
			sentence = readNext();
		}
		reader.close();

		return sentences;
	}

	public static void buildPropertiesProjections(Sentence sentence, Set<String> headPropertiesToProject, Set<String> depsPropertiesToProject) throws IOException, XMLStreamException {

		//System.out.println("Building "+headPropertiesToProject+" and "+depsPropertiesToProject+" in "+sentenceProperties);
		if (headPropertiesToProject.size() > 0) {

			for (String wId : sentence.keySet()) {

				HashMap<String, String> properties = sentence.get(wId);

				for (String wheadid : sentence.keySet()) { // find the word head properties

					HashMap<String, String> headProperties = sentence.get(wheadid);

					if (headProperties.get("id").equals(properties.get("head"))) { // a word which head is the current word id //$NON-NLS-1$
						// { "form", "lemma", "upos", "xpos", "feats", "deprel", "deps", "misc" };
						if (headPropertiesToProject.contains("form")) properties.put("head-form", headProperties.get("form")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("lemma")) properties.put("head-lemma", headProperties.get("lemma")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("upos")) properties.put("head-upos", headProperties.get("upos")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("xpos")) properties.put("head-xpos", headProperties.get("xpos")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("feats")) properties.put("head-feats", headProperties.get("feats")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("deprel")) properties.put("head-deprel", headProperties.get("deprel")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("deps")) properties.put("head-deps", headProperties.get("deps")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("misc")) properties.put("head-misc", headProperties.get("misc")); //$NON-NLS-1$
						break; // the is only one head per word
					}
				}
			}
		}

		if (depsPropertiesToProject.size() > 0) {

			for (String wId : sentence.keySet()) {

				HashMap<String, String> properties = sentence.get(wId);
				HashMap<String, String> newProperties = new HashMap<String, String>();
				if (depsPropertiesToProject.contains("id")) newProperties.put("dep-id", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("form")) newProperties.put("dep-form", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("lemma")) newProperties.put("dep-lemma", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("upos")) newProperties.put("dep-upos", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("xpos")) newProperties.put("dep-xpos", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("deprel")) newProperties.put("dep-deprel", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("deps")) newProperties.put("dep-deps", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("misc")) newProperties.put("dep-misc", "|"); //$NON-NLS-1$

				// browse each dep properties to build the depXYZ properties
				for (String wdepid : sentence.keySet()) {

					HashMap<String, String> depProperties = sentence.get(wdepid);
					if (depProperties.get("head").equals(properties.get("id"))) { // a word which head is the current word id //$NON-NLS-1$

						if (depsPropertiesToProject.contains("id")) newProperties.put("dep-id", newProperties.get("dep-id") + depProperties.get("id") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("form")) newProperties.put("dep-form", newProperties.get("dep-form") + depProperties.get("form") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("lemma")) newProperties.put("dep-lemma", newProperties.get("dep-lemma") + depProperties.get("lemma") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("upos")) newProperties.put("dep-upos", newProperties.get("dep-upos") + depProperties.get("upos") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("xpos")) newProperties.put("dep-xpos", newProperties.get("dep-xpos") + depProperties.get("xpos") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("deprel")) newProperties.put("dep-deprel", newProperties.get("dep-deprel") + depProperties.get("deprel") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("deps")) newProperties.put("dep-deps", newProperties.get("dep-deps") + depProperties.get("deps") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("misc")) properties.put("dep-misc", newProperties.get("dep-misc") + depProperties.get("misc") + "|"); //$NON-NLS-1$
					}
				}

				for (String newProp : newProperties.keySet()) {
					properties.put(newProp, newProperties.get(newProp));
				}
			}
		}
		//System.out.println("Result "+sentenceProperties);
	}

	@Override
	public void finalize() {
		if (reader != null) try {
			reader.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String args[]) throws IOException, XMLStreamException {

		File file = new File(System.getProperty("user.home"), "TEMP/test2.conllu"); //$NON-NLS-1$
		Set<String> headPropertiesToProject = new HashSet<String>(Arrays.asList(UD_PROPERTY_NAMES));
		CoNLLUReader reader = new CoNLLUReader(file, "test"); //$NON-NLS-1$
		Sentences sentences = reader.readAll();
		sentences.replaceIdwithXmlId();
		for (Sentence s : sentences) {
			buildPropertiesProjections(s, headPropertiesToProject, headPropertiesToProject);
			for (String w : s.keySet()) {

				System.out.println(w + "\t" + s.get(w)); //$NON-NLS-1$
			}
			System.out.println();
		}
	}

	public void close() {

		finalize();
	}
}
