package org.txm.conllu.core;

import java.util.ArrayList;

public class Sentences extends ArrayList<Sentence> {

	private static final long serialVersionUID = 5706212463880734916L;

	/**
	 * replace the ud id with the misc XmlId value
	 * A word with no misc XmlId is removed
	 */
	public void replaceIdwithXmlId() {

		for (Sentence s : this) {

			s.replaceIdwithXmlId();
		}
	}

	/**
	 * merge the part word lines in the n-m word lines (the part are removed
	 */
	public void removePartTokensWords() {

		for (Sentence s : this) {

			s.removePartTokensWords();
		}
	}

	/**
	 * removes the n-m word lines
	 */
	public void removeMultiTokensWords() {

		for (Sentence s : this) {

			s.removeMultiTokensWords();
		}
	}

}
