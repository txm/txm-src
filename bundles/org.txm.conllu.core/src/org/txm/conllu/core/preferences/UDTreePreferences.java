package org.txm.conllu.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.treesearch.preferences.TreeSearchPreferences;

/**
 * UD tree Preferences initializer and manager.
 * 
 * @author mdecorde
 *
 */
public class UDTreePreferences extends TreeSearchPreferences {

	public static final String DEPTREEVIZ = "DepTreeViz";

	public static final String BRAT = "Brat";

	public static final String ENGINE = "visu_engine";

	public static final String PRINTCONLLUSENTENCES = "print_ud_seentence";

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(UDTreePreferences.class)) {
			new UDTreePreferences();
		}
		return TXMPreferences.instances.get(UDTreePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(VISUALISATION, DEPTREEVIZ);
		preferences.putBoolean(PRINTCONLLUSENTENCES, false);
		preferences.put(TFEATURE, "");
		preferences.put(NTFEATURE, "feats");
	}
}
