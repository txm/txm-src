package org.txm.conllu.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 *
 */
public class UDPreferences extends TXMPreferences {


	public static String VERSION = "version";

	public static String UDPREFIX = "ud_prefix";

	public static String CONTRACTIONS_MANAGEMENT = "import_contractions_MANAGEMENT";

	public static String SURFACE = "surface";

	public static String SYNTAX = "syntax";

	public static String ALL = "all";

	public static String IMPORT_USE_NEW_DOC_ID = "import_use_new_doc_id";

	public static String IMPORT_HEAD_TO_PROJECT = "import_head_to_project";

	public static String IMPORT_DEPS_TO_PROJECT = "import_deps_to_project";

	public static String IMPORT_BUILD_TIGERSEARCH_INDEXES = "import_build_tiger_indexes";

	public static String IMPORT_PRINT_NEWLINES_AFTER_SENTENCES = "import_print_newlines_in_editions";

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(UDPreferences.class)) {
			new UDPreferences();
		}
		return TXMPreferences.instances.get(UDPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(UDPREFIX, "ud-");
		preferences.put(IMPORT_HEAD_TO_PROJECT, "upos,deprel");
		preferences.put(IMPORT_DEPS_TO_PROJECT, "upos,deprel");
		preferences.put(CONTRACTIONS_MANAGEMENT, SURFACE); // surface, syntax, all
		preferences.putBoolean(IMPORT_USE_NEW_DOC_ID, true);
		preferences.putBoolean(IMPORT_BUILD_TIGERSEARCH_INDEXES, true);
		preferences.putBoolean(IMPORT_PRINT_NEWLINES_AFTER_SENTENCES, true);
	}
}
