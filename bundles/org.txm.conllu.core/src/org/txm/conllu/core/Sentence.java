package org.txm.conllu.core;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.TreeMap;

public class Sentence extends TreeMap<String, LinkedHashMap<String, String>> {

	private static final long serialVersionUID = -1585503070690160508L;

	public int nWords2 = 0;

	public String sent_id = null;

	public String newpar_id = null;

	public String newdoc_id = null;

	@Override
	public Comparator<String> comparator() {

		return new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				int idx1 = o1.indexOf("-"); //$NON-NLS-1$
				int idx2 = o2.indexOf("-"); //$NON-NLS-1$
				if (idx1 >= 0) {
					if (idx2 >= 0) {
						return Integer.parseInt(o1.substring(0, idx1)) - Integer.parseInt(o2.substring(0, idx2));
					}
					else {
						return Integer.parseInt(o1.substring(0, idx1)) - Integer.parseInt(o2);
					}
				}
				else if (idx2 >= 0) {
					if (idx1 >= 0) {
						return Integer.parseInt(o1.substring(0, idx1)) - Integer.parseInt(o2.substring(0, idx2));
					}
					else {
						return Integer.parseInt(o1) - Integer.parseInt(o2.substring(0, idx2));
					}
				}
				else {
					return Integer.parseInt(o1) - Integer.parseInt(o2);
				}
			}
		};
	}

	/**
	 * merge the part word lines in the n-m word lines (the part are removed
	 */
	public void removePartTokensWords() {

		for (String wordid : this.keySet()) {

			LinkedHashMap<String, String> word = get(wordid);
			if (word == null) continue;

			if (wordid.contains("-")) { // multi-word line  //$NON-NLS-1$
				String[] parts = wordid.split("-"); //$NON-NLS-1$

				for (String p : CoNLLUReader.UD_PROPERTY_NAMES) {
					if (p == "id") continue; // don't merge the form property //$NON-NLS-1$

					String v = ""; //$NON-NLS-1$
					if (p == "form") continue; // don't merge the form property //$NON-NLS-1$

					for (String part : parts) {
						if (v.length() > 0) v += "+"; //$NON-NLS-1$
						v += this.get(part).get(p);
					}
					word.put(p, v); // replace value with the concat values
				}

				for (String part : parts) {
					remove(part); // remove the part token
				}
			}
		}

		//		Vector<String> keys = new Vector<String>(this.keySet());
		//		keys.sort(new Comparator<String>() {
		//
		//			@Override
		//			public int compare(String o1, String o2) {
		//				
		//				return Integer.parseInt(o1) - Integer.parseInt(o2);
		//			}
		//		});
		//		for (String w : keys) {
		//			
		//		}
	}

	/**
	 * removes the n-m word lines
	 */
	public void removeMultiTokensWords() {

		Set<String> keys = new HashSet<String>(this.keySet());
		for (String w : keys) {
			if (w.contains("-")) { //$NON-NLS-1$
				this.remove(w);
			}
		}
	}

	/**
	 * replace the ud id with the misc XmlId value
	 * A word with no misc XmlId is removed
	 */
	public void replaceIdwithXmlId() {

		Set<String> keys = new HashSet<String>(this.keySet());
		for (String w : keys) {

			if (this.get(w).get("XmlId") != null) this.put(this.get(w).get("XmlId"), this.get(w)); //$NON-NLS-1$

			this.remove(w);
		}
	}
}
