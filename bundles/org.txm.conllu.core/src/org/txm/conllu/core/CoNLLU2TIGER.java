package org.txm.conllu.core;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.LinkedHashMap;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.txm.utils.FileUtils;
import org.txm.utils.logger.Log;

/**
 * Create a TIGER XML corpus from conllu files
 * 
 * @author mdecorde
 *
 */
public class CoNLLU2TIGER {

	File conlluDirectory;

	File tigerDirectory;

	String corpusName;


	/** The output. */
	protected XMLOutputFactory outfactory = XMLOutputFactory.newInstance();

	protected BufferedOutputStream tigerOutput;

	protected XMLStreamWriter tigerWriter;

	protected BufferedOutputStream mainOutput;

	protected XMLStreamWriter mainWriter;

	public CoNLLU2TIGER(File conlluDirectory, File tigerDirectory, String corpusName) {

		this.conlluDirectory = conlluDirectory;
		this.tigerDirectory = tigerDirectory;
		this.corpusName = corpusName;
	}

	public boolean process() throws IOException, XMLStreamException {
		File[] conlluFiles = conlluDirectory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {

				return name.endsWith(".conllu"); //$NON-NLS-1$
			}
		});

		if (conlluFiles == null || conlluFiles.length == 0) {
			Log.warning("** No CoNLLU file found in " + conlluDirectory);
			return false;
		}

		File mainFile = new File(tigerDirectory, "main.xml"); //$NON-NLS-1$
		mainOutput = new BufferedOutputStream(new FileOutputStream(mainFile), 16 * 1024);
		mainWriter = outfactory.createXMLStreamWriter(mainOutput, "UTF-8"); // create a new file  //$NON-NLS-1$

		//		printf MASTER "<corpus id=\"$corpus\">
		mainWriter.writeStartElement("corpus"); //$NON-NLS-1$
		tigerWriter.writeAttribute("id", corpusName); //$NON-NLS-1$

		for (File conlluFile : conlluFiles) {

			String filename = FileUtils.stripExtension(conlluFile);
			File tigerXMLFile = new File(tigerDirectory, filename + ".xml"); //$NON-NLS-1$
			tigerOutput = new BufferedOutputStream(new FileOutputStream(tigerXMLFile), 16 * 1024);
			tigerWriter = outfactory.createXMLStreamWriter(tigerOutput, "UTF-8"); // create a new file //$NON-NLS-1$

			CoNLLUReader reader = new CoNLLUReader(conlluFile, null);
			Sentence s = reader.readNext();

			// "<subcorpus name='$infilename-$suffix' external='file:$infilename-$suffix.xml'/>\n";
			tigerWriter.writeEmptyElement("subcorpus"); //$NON-NLS-1$
			tigerWriter.writeAttribute("name", tigerXMLFile.getName()); //$NON-NLS-1$
			tigerWriter.writeAttribute("external", "file:" + tigerXMLFile.getName()); //$NON-NLS-1$

			while (s != null) { // write <s> <t> <nt> and <edge> elements for each sentence

				//				printf XML "<s id=\"s%s%s\" textid=\"$text_id\" sentid=\"$sent_id\">\n", $., $add_to_sentcode;
				tigerWriter.writeStartElement("s"); //$NON-NLS-1$
				tigerWriter.writeAttribute("id", s.newdoc_id + "_" + s.sent_id); //$NON-NLS-1$
				tigerWriter.writeAttribute("textid", s.newdoc_id); //$NON-NLS-1$
				tigerWriter.writeAttribute("sentid", s.sent_id); //$NON-NLS-1$
				tigerWriter.writeAttribute("pid", s.newpar_id); //$NON-NLS-1$

				//				print XML "  <graph root=\"n$._$rootnode\">\n";
				tigerWriter.writeStartElement("graph"); //$NON-NLS-1$
				tigerWriter.writeAttribute("root", "._rootnode"); //$NON-NLS-1$

				// write terminals
				//				print XML "    <terminals>\n";
				tigerWriter.writeStartElement("terminals"); //$NON-NLS-1$
				for (String wordid : s.keySet()) {
					LinkedHashMap<String, String> word = s.get(wordid);
					//					push(@terminals, sprintf("      <t id=\"s%d_%d\" word=\"%s\" pos=\"%s\" mor=\"%s\" lemma=\"%s\" textid=\"%s\" editionId=\"%s\"/>\n", $., $wnr, $word, $pos, $mor, $lemma, $text_id, $edition_id));
					//					push(@terminals, sprintf("      <t id=\"s%d_%d_dupl\" word=\"%s\" pos=\"%s\" mor=\"%s\" lemma=\"%s\" textid=\"%s\" editionId=\"%s\"/>\n", $., $wnr, "*", "_", "_", "_", $text_id, $edition_id));
					tigerWriter.writeStartElement("t"); //$NON-NLS-1$
					for (String attr : CoNLLUReader.UD_PROPERTY_NAMES) {
						tigerWriter.writeAttribute(attr, word.get(attr));
					}
					if (s.newdoc_id != null && s.newdoc_id.length() > 0) {
						tigerWriter.writeAttribute("textid", s.newdoc_id); //$NON-NLS-1$
					}
					if (s.newpar_id != null && s.newpar_id.length() > 0) {
						tigerWriter.writeAttribute("pid", s.newpar_id); //$NON-NLS-1$
					}
					if (word.get("XmlId") != null) { //$NON-NLS-1$
						tigerWriter.writeAttribute("editionId", word.get("XmlId")); //$NON-NLS-1$
					}
				}
				tigerWriter.writeEndElement(); // terminals

				// write non-terminals
				tigerWriter.writeStartElement("nonterminals"); //$NON-NLS-1$
				//				for (String nt : s.keySet()) {
				//					/*
				//					 * <nt id="n1_1" cat="advmod" coord="--" dom="--" type="nV" vform="--" vlemma="--" note="--" snr="1">
				//					 *   <edge idref="s1_1" label="L"/>
				//					 * </nt>
				//					 */
				//					tigerWriter.writeStartElement("nt");
				//					tigerWriter.writeAttribute("name", nt);
				//					tigerWriter.writeEndElement(); // nt
				//				}
				tigerWriter.writeEndElement(); // nonterminals

				tigerWriter.writeEndElement(); // graph

				tigerWriter.writeEndElement(); // s

				s = reader.readNext();
			}
			tigerWriter.writeEndElement(); // subcorpus
			reader.close();
		}

		return true;
	}
}
