package org.txm.conllu.core.search;

import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.Toolbox;
import org.txm.conllu.core.Messages;
import org.txm.conllu.core.function.UDTreeSearch;
import org.txm.conllu.core.preferences.UDPreferences;
import org.txm.core.engines.EngineType;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.libs.cqp.CQPLibPreferences;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Match;
import org.txm.searchengine.core.EmptySelection;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEngineProperty;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

/**
 * Encasulate CQP Search engine to execute queries on "ud-s" subcorpus
 * 
 * @author mdecorde
 *
 */
public class UDSearchEngine extends SearchEngine {

	public static final String NAME = "UD"; //$NON-NLS-1$

	CQPSearchEngine CQI;

	private CQPSearchEngine getCQI() {

		if (CQI == null) {
			CQI = CQPSearchEngine.getEngine();
		}
		return CQI;
	}

	public HashMap<String, List<String>> getAvailableQueryOptions() {
		return getCQI().getAvailableQueryOptions();
	}

	public String getOptionForSearchengine() {
		return "CQP"; //$NON-NLS-1$
	}

	public String getOptionNameForSearchengine() {
		return "ud-s"; //$NON-NLS-1$
	}

	public String getOptionDescriptionForSearchengine() {
		return Messages.UDSearchEngine_0;
	}

	@Override
	public boolean isRunning() {

		return getCQI().isRunning();
	}

	@Override
	public boolean initialize() throws Exception {

		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public void notify(TXMResult r, String state) {

	}

	@Override
	public String getName() {
		return "UD"; //$NON-NLS-1$
	}

	@Override
	public Selection query(CorpusBuild corpus, IQuery query, String name, boolean saveQuery) throws Exception {

		if (corpus instanceof CQPCorpus) {
			CQPCorpus cqpCorpus = (CQPCorpus) corpus;
			String prefix = UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));
			Subcorpus udsentences = getSentenceSubcorpus(cqpCorpus, prefix);

			return getCQI().query(udsentences, query, name, saveQuery);
		}
		return new EmptySelection(query);
	}

	@Override
	public Query newQuery() {

		return new UDQuery();
	}

	@Override
	public boolean hasIndexes(CorpusBuild corpus) {
		return corpus instanceof CQPCorpus // HAS CQP INDEXES 
				&& UDTreeSearch.hasUDProperties((CQPCorpus) corpus); // AND THE ud-id property
	}

	@Override
	public String getValueForProperty(Match match, SearchEngineProperty property) {

		return getCQI().getValueForProperty(match, property);
	}

	@Override
	public List<String> getValuesForProperty(Match match, SearchEngineProperty property) {

		return getCQI().getValuesForProperty(match, property);
	}

	public static SearchEngine getEngine() {

		return (UDSearchEngine) Toolbox.getEngineManager(EngineType.SEARCH).getEngine(UDSearchEngine.NAME);
	}

	public static Subcorpus getSentenceSubcorpus(CQPCorpus corpus, String prefix) throws CqiClientException, InterruptedException {

		Subcorpus udSentences = (Subcorpus) corpus.getSubcorpusBySimpleName("ud-s"); //$NON-NLS-1$
		CQLQuery sentencesQuery = new CQLQuery(getUDSCQPQueryString(prefix));

		if (udSentences != null && !udSentences.getQuery().equals(sentencesQuery)) { // this one is not ok, delete it
			udSentences.delete();
			udSentences = null;
		}

		if (udSentences == null) { // create it
			udSentences = new Subcorpus(corpus);
			sentencesQuery.setOption(CQPLibPreferences.MATCHINGSTRATEGY, "longest");
			udSentences.setQuery(sentencesQuery);
			udSentences.setName("ud-s");
			udSentences.setVisible(false);
		}

		udSentences.compute(); // this on is OK, just prepare it to be used

		return udSentences;
	}

	/**
	 * 
	 * @param prefix the UD prefix of the CQP properties
	 * @return A CQL query to build a Subcorpus of UD sentences
	 */
	public static String getUDSCQPQueryString(String prefix) {
		String emptyvalue = TBXPreferences.getInstance().getString(TBXPreferences.EMPTY_PROPERTY_VALUE_CODE);
		//              [ud-id="1(-2)?"]          [ud-id!="1(-2)?|__UNDEF__"]* + longest
		return "[" + prefix + "id=\"1(-2)?\"] [" + prefix + "id!=\"1(-2)?|" + emptyvalue + "\"]*";  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	public String hasAdditionalDetailsForResult(TXMResult result) {
		if (result instanceof CQPCorpus) {
			CQPCorpus corpus = (CQPCorpus) result;
			return UDTreeSearch.hasUDProperties(corpus) ? "Syntax" : null; //$NON-NLS-1$
		}
		return null;
	}

	public String getAdditionalDetailsForResult(TXMResult result) {
		if (result instanceof CQPCorpus) {
			CQPCorpus corpus = (CQPCorpus) result;
			if (UDTreeSearch.hasUDProperties(corpus)) {
				StringBuilder buffer = new StringBuilder();

				buffer.append("<h3>CoNLLU</h3>\n"); //$NON-NLS-1$

				String prefix = UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));
				buffer.append("<p>UD prefix: '" + prefix + "'</p>\n"); //$NON-NLS-2$

				try {
					Subcorpus udSentences = getSentenceSubcorpus(corpus, prefix);
					buffer.append(Messages.UDSearchEngine_9 + udSentences.getNMatch() + "</p>\n"); //$NON-NLS-2$
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return buffer.toString();
			}
		}
		return null;
	}
}
