package org.txm.conllu.core.search;

import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;

public class UDQuery extends CQLQuery {

	public UDQuery() {

	}

	public UDQuery(String query) {
		super(query);
	}

	@Override
	public SearchEngine getSearchEngine() {
		return UDSearchEngine.getEngine();
	}
}
