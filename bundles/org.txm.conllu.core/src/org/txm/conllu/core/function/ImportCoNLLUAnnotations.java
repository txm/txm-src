package org.txm.conllu.core.function;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang.StringUtils;
import org.txm.objects.Text;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.AsciiUtils;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;
import org.txm.xml.xmltxm.XMLTXMWordPropertiesInjection;

public class ImportCoNLLUAnnotations {

	public static final String[] UD_PROPERTY_NAMES = { "id", "form", "lemma", "upos", "xpos", "feats", "head", "deprel", "deps", "misc" }; //$NON-NLS-1$

	public static int _importAnnotations(File coonluFile, MainCorpus mainCorpus,
			String propertiesPrefix, String textId,
			Boolean normalize_word_ids,
			Boolean importCommentProperties,
			Set<String> headPropertiesToProject,
			Set<String> depsPropertiesToProject,
			Set<String> udPropertiesToImport) throws IOException, XMLStreamException {

		if (textId == null || textId.length() == 0) { // no text id provided, using the conllu file name
			textId = coonluFile.getName().substring(0, coonluFile.getName().length() - 7);
		}

		for (String p : headPropertiesToProject) {
			udPropertiesToImport.add("head-" + p); //$NON-NLS-1$
		}

		for (String p : depsPropertiesToProject) {
			udPropertiesToImport.add("dep-" + p); //$NON-NLS-1$
		}

		if (!udPropertiesToImport.contains("id")) {
			Log.warning("The UD based commands need at least the 'id' property to properly work.");
		}

		Log.info("** processing text: " + textId);
		Text text = mainCorpus.getProject().getText(textId);
		if (text == null) {
			Log.warning("No text found with ID=" + textId);
			return 0;
		}
		File xmltxmFile = mainCorpus.getProject().getText(textId).getXMLTXMFile();
		File xmltxmUpdatedFile = new File(System.getProperty("java.io.tmpdir"), xmltxmFile.getName()); //$NON-NLS-1$

		XMLTXMWordPropertiesInjection processor = new XMLTXMWordPropertiesInjection(xmltxmFile);
		HashMap<String, HashMap<String, String>> rules = new HashMap<>();
		processor.setProperties(rules);

		BufferedReader reader = IOUtils.getReader(coonluFile);
		String line = reader.readLine();

		int nWords2 = 0;
		int nLine = 0;
		String sent_id = null;
		String newpar_id = null;
		String newdoc_id = null;
		LinkedHashMap<String, HashMap<String, String>> sentenceProperties = new LinkedHashMap<String, HashMap<String, String>>();
		while (line != null) {
			nLine++;
			if (line.length() == 0) {
				line = reader.readLine();
				continue; // comment
			}

			if (line.startsWith("#")) { //$NON-NLS-1$
				if (line.startsWith("# sent_id = ")) { //$NON-NLS-1$
					sent_id = line.substring(12).trim();
				}
				else if (line.startsWith("# newdoc id = ")) { //$NON-NLS-1$
					newdoc_id = line.substring(14).trim();
				}
				else if (line.startsWith("# newpar id = ")) { //$NON-NLS-1$
					newpar_id = line.substring(14).trim();
				}
				else {
					// nothing for now
				}

				line = reader.readLine();
				continue; // comment
			}

			String[] split = line.split("\t", 10); //$NON-NLS-1$
			if (split.length < 10) {
				Log.warning("Error: line " + nLine + " : " + line + " -> " + Arrays.toString(split) + " len=" + split.length);
				line = reader.readLine();
				continue; // comment
			}

			if (split[0].equals("1")) { // a new sentence begins, write the previous if any //$NON-NLS-1$
				// write previous sentence word properties
				if (sentenceProperties.size() > 0) {
					storeSentenceProperties(sentenceProperties, processor, propertiesPrefix, headPropertiesToProject, depsPropertiesToProject, udPropertiesToImport);
				}
			}

			String misc = split[9];
			String[] miscValues = misc.split("\\|"); //$NON-NLS-1$
			String wId = null;
			for (String miscValue : miscValues) {
				if (miscValue.startsWith("XmlId=")) { //$NON-NLS-1$
					wId = miscValue.substring(6);
				}
			}

			HashMap<String, String> properties = new HashMap<>();
			for (int i = 0; i < split.length; i++) {
				properties.put(UD_PROPERTY_NAMES[i], split[i]); // add the property name using the prefix ; XML-TXM types are prefixed with '#'
			}

			if (sent_id != null) {
				if (importCommentProperties) properties.put("#" + propertiesPrefix + "sentid", sent_id); //$NON-NLS-1$
				sent_id = ""; // reset value for next sentence
			}
			else {
				if (importCommentProperties) properties.put("#" + propertiesPrefix + "sentid", ""); //$NON-NLS-1$
			}

			if (newdoc_id != null) {
				if (importCommentProperties) properties.put("#" + propertiesPrefix + "newdocid", newdoc_id); //$NON-NLS-1$
				newdoc_id = null; // reset value for next sentence
			}
			else {
				if (importCommentProperties) properties.put("#" + propertiesPrefix + "newdocid", ""); //$NON-NLS-1$
			}

			if (newpar_id != null) {
				if (importCommentProperties) properties.put("#" + propertiesPrefix + "newparid", newpar_id); //$NON-NLS-1$
				newpar_id = null; // reset value for next sentence
			}
			else {
				if (importCommentProperties) properties.put("#" + propertiesPrefix + "newparid", ""); //$NON-NLS-1$
			}

			if (wId == null) {
				Log.warning("No 'XmlId=' found for UD line: " + line);
			}
			else {

				if (normalize_word_ids) {
					if (!wId.startsWith("w_")) { //$NON-NLS-1$
						wId = "w_" + wId.substring(1); //$NON-NLS-1$
					}
					wId = AsciiUtils.buildWordId(wId);
				}

				sentenceProperties.put(wId, properties);
				nWords2++;
			}
			line = reader.readLine();
		}
		reader.close();

		// IF ANY write previous sentence word properties 
		if (sentenceProperties.size() > 0) {
			storeSentenceProperties(sentenceProperties, processor, propertiesPrefix, headPropertiesToProject, depsPropertiesToProject, udPropertiesToImport);
		}

		if (nWords2 == 0) {
			Log.warning("** No annotation to import in " + coonluFile);
			return 0;
		}

		Log.info("** Loading annotations from : " + coonluFile + " " + processor.getRules().size() + " injections to do.");
		if (processor.process(xmltxmUpdatedFile)) {
			if (xmltxmFile.delete() && FileCopy.copy(xmltxmUpdatedFile, xmltxmFile)) {
				if (processor.getNonActivatedRules().size() > 0) {
					Log.warning("Warning: some words were not imported: " + StringUtils.join(processor.getNonActivatedRules(), ", "));
				}
			}
			else {
				Log.warning("** Warning: annotation import failed for replace the corpus XML-TXM file: " + xmltxmFile + ". TEMP file: " + xmltxmUpdatedFile);
				return 0;
			}
		}
		else {
			Log.warning("** Warning: annotation import failed for text: " + textId);
			return 0;
		}

		if (processor.getNInsertions() == 0) {
			Log.warning("** No annotation imported in " + textId);
		}

		return processor.getNInsertions();
	}

	public static void buildPropertiesProjections(LinkedHashMap<String, HashMap<String, String>> sentenceProperties, Set<String> headPropertiesToProject, Set<String> depsPropertiesToProject)
			throws IOException, XMLStreamException {

		//System.out.println("Building "+headPropertiesToProject+" and "+depsPropertiesToProject+" in "+sentenceProperties);
		if (headPropertiesToProject.size() > 0) {

			for (String wId : sentenceProperties.keySet()) {

				HashMap<String, String> properties = sentenceProperties.get(wId);

				for (String wheadid : sentenceProperties.keySet()) { // find the word head properties

					HashMap<String, String> headProperties = sentenceProperties.get(wheadid);

					if (headProperties.get("id").equals(properties.get("head"))) { // a word which head is the current word id //$NON-NLS-1$
						// { "form", "lemma", "upos", "xpos", "feats", "deprel", "deps", "misc" };
						if (headPropertiesToProject.contains("form")) properties.put("head-form", headProperties.get("form")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("lemma")) properties.put("head-lemma", headProperties.get("lemma")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("upos")) properties.put("head-upos", headProperties.get("upos")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("xpos")) properties.put("head-xpos", headProperties.get("xpos")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("feats")) properties.put("head-feats", headProperties.get("feats")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("deprel")) properties.put("head-deprel", headProperties.get("deprel")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("deps")) properties.put("head-deps", headProperties.get("deps")); //$NON-NLS-1$
						if (headPropertiesToProject.contains("misc")) properties.put("head-misc", headProperties.get("misc")); //$NON-NLS-1$
						break; // the is only one head per word
					}
				}
			}
		}

		if (depsPropertiesToProject.size() > 0) {

			for (String wId : sentenceProperties.keySet()) {

				HashMap<String, String> properties = sentenceProperties.get(wId);
				HashMap<String, String> newProperties = new HashMap<String, String>();
				if (depsPropertiesToProject.contains("id")) newProperties.put("dep-id", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("form")) newProperties.put("dep-form", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("lemma")) newProperties.put("dep-lemma", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("upos")) newProperties.put("dep-upos", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("xpos")) newProperties.put("dep-xpos", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("deprel")) newProperties.put("dep-deprel", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("deps")) newProperties.put("dep-deps", "|"); //$NON-NLS-1$
				if (depsPropertiesToProject.contains("misc")) newProperties.put("dep-misc", "|"); //$NON-NLS-1$

				// browse each dep properties to build the depXYZ properties
				for (String wdepid : sentenceProperties.keySet()) {

					HashMap<String, String> depProperties = sentenceProperties.get(wdepid);
					if (depProperties.get("head").equals(properties.get("id"))) { // a word which head is the current word id //$NON-NLS-1$

						if (depsPropertiesToProject.contains("id")) newProperties.put("dep-id", newProperties.get("dep-id") + depProperties.get("id") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("form")) newProperties.put("dep-form", newProperties.get("dep-form") + depProperties.get("form") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("lemma")) newProperties.put("dep-lemma", newProperties.get("dep-lemma") + depProperties.get("lemma") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("upos")) newProperties.put("dep-upos", newProperties.get("dep-upos") + depProperties.get("upos") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("xpos")) newProperties.put("dep-xpos", newProperties.get("dep-xpos") + depProperties.get("xpos") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("deprel")) newProperties.put("dep-deprel", newProperties.get("dep-deprel") + depProperties.get("deprel") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("deps")) newProperties.put("dep-deps", newProperties.get("dep-deps") + depProperties.get("deps") + "|"); //$NON-NLS-1$
						if (depsPropertiesToProject.contains("misc")) properties.put("dep-misc", newProperties.get("dep-misc") + depProperties.get("misc") + "|"); //$NON-NLS-1$
					}
				}

				for (String newProp : newProperties.keySet()) {
					properties.put(newProp, newProperties.get(newProp));
				}
			}
		}
		//System.out.println("Result "+sentenceProperties);
	}

	private static void storeSentenceProperties(LinkedHashMap<String, HashMap<String, String>> sentenceProperties, XMLTXMWordPropertiesInjection processor, String propertiesPrefix,
			Set<String> headPropertiesToProject, Set<String> depsPropertiesToProject, Set<String> udPropertiesToImport) throws IOException, XMLStreamException {

		buildPropertiesProjections(sentenceProperties, headPropertiesToProject, depsPropertiesToProject);

		for (String id : sentenceProperties.keySet()) {
			HashMap<String, String> properties = sentenceProperties.get(id);
			ArrayList<String> propertyNames = new ArrayList<String>(properties.keySet());

			for (String p : propertyNames) { // add # to properties to inject, and remove properties not to inject

				if (p.startsWith("#")) { //$NON-NLS-1$
					continue; //property already prefixed with #, do not add the prefix
				}
				else if (udPropertiesToImport != null && udPropertiesToImport.size() > 0 && !udPropertiesToImport.contains(p)) {
					; // keep only the sentence properties to import
					properties.remove(p);
					continue; //property already prefixed with #, do not add the prefix
				}
				String value = properties.get(p);
				properties.remove(p);
				properties.put("#" + propertiesPrefix + p, value); //$NON-NLS-1$
			}

			processor.addProperty(id, properties);
		}

		sentenceProperties.clear();
	}
}
