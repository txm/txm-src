package org.txm.conllu.core.function;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.txm.conllu.core.preferences.UDPreferences;
import org.txm.conllu.core.preferences.UDTreePreferences;
import org.txm.conllu.core.search.UDSearchEngine;
import org.txm.libs.deptreeviz.UDDepTreeVizPrintTree;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.core.SimpleSelection;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.treesearch.function.TreeSearch;
import org.txm.treesearch.function.TreeSearchSelector;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

public class UDTreeSearch extends TreeSearch {

	Subcorpus udSentences;

	private QueryResult rawMatches;

	private LinkedHashMap<org.txm.objects.Match, List<Integer>> matchingSentencesToMatches;

	private ArrayList<org.txm.objects.Match> matchingSentencesKeys;

	private int nTotalMatches;

	public UDTreeSearch(CQPCorpus corpus) {
		this(null, corpus);
	}

	public UDTreeSearch(String parentNodePath, CQPCorpus corpus) {
		super(parentNodePath, corpus);

		this.corpus = getParent();
	}

	public UDTreeSearch(String parentNodePath) {
		this(parentNodePath, null);
	}

	public boolean isComputed() {
		return matchingSentencesToMatches != null;
	}

	public boolean canBeDrawn() {
		return matchingSentencesToMatches != null;
	}

	public Selection getSelection() {
		return new SimpleSelection<org.txm.objects.Match>(pQuery, matchingSentencesKeys);
	}

	@Override
	public boolean toSVG(File svgFile, int sent, int sub, List<String> T, List<String> NT) {

		if (!canBeDrawn()) return false; // no result
		try {

			org.txm.objects.Match matchingSentence = this.matchingSentencesKeys.get(sent);
			Match submatch = rawMatches.getMatch(this.matchingSentencesToMatches.get(matchingSentence).get(sub)); // use the set highlight in the Graph

			int[] positions = new int[matchingSentence.size()];
			for (int i = 0; i < matchingSentence.size(); i++)
				positions[i] = matchingSentence.getStart() + i;

			String prefix = UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));

			String[] Tvalues = new String[positions.length];


			if (T != null && T.size() > 0) {
				for (int i = 0; i < Tvalues.length; i++) {
					Tvalues[i] = "";
				}
				for (int iT = 0; iT < T.size(); iT++) {
					String t = T.get(iT);
					if (corpus.getProperty(t) != null) {
						String[] tvalues = corpus.getProperty(t).cpos2Str(positions);
						for (int i = 0; i < tvalues.length; i++) {
							if (iT > 0) Tvalues[i] += " / ";
							Tvalues[i] += tvalues[i];
						}
					}
				}
			}

			String[] NTvalues = new String[positions.length];
			if (NT != null && NT.size() > 0) {
				for (int i = 0; i < NTvalues.length; i++) {
					NTvalues[i] = "";
				}
				for (int iNT = 0; iNT < NT.size(); iNT++) {
					String nt = NT.get(iNT);
					if (corpus.getProperty(nt) != null) {
						String[] ntvalues = corpus.getProperty(nt).cpos2Str(positions);
						for (int i = 0; i < ntvalues.length; i++) {
							if (iNT > 0) NTvalues[i] += " / ";
							NTvalues[i] += ntvalues[i];
						}
					}
				}
			}

			HashMap<String, String[]> values = new HashMap<>(); // prepare an array of empty values in case of missing properties
			String[] emptyvalues = new String[positions.length];
			for (int i = 0; i < emptyvalues.length; i++) {
				emptyvalues[i] = "_";
			}

			for (String p : ImportCoNLLUAnnotations.UD_PROPERTY_NAMES) {

				if (corpus.getProperty(prefix + p) != null) {
					values.put(prefix + p, corpus.getProperty(prefix + p).cpos2Str(positions));
				}
				else if (p.equals("form")) { // in case the prefix-form properties was not found use the "word" property
					values.put(prefix + p, corpus.getWordProperty().cpos2Str(positions));
				}
				else { // in case the CQP property does not exists, use empty values
					values.put(prefix + p, emptyvalues);
				}
			}

			ArrayList<String> conll = new ArrayList<String>();

			for (int p = 0; p < positions.length; p++) {

				boolean startOfSubMatch = submatch.getStart() == positions[p];
				boolean endOfSubMatch = submatch.getEnd() == positions[p];

				StringBuilder buffer = new StringBuilder();
				for (String prop : ImportCoNLLUAnnotations.UD_PROPERTY_NAMES) {

					if (buffer.length() > 0) buffer.append("\t");
					if (prop.equals("form") && startOfSubMatch) buffer.append("[");
					String v = values.get(prefix + prop)[p];
					if (v.startsWith("|") && v.endsWith("|")) v = v.substring(1, v.length() - 1); // remove the '|' of CQP multi values field
					buffer.append(v);
					if (prop.equals("form") && endOfSubMatch) buffer.append("]");
				}

				conll.add(buffer.toString());
			}

			if (UDTreePreferences.getInstance().getBoolean(UDTreePreferences.PRINTCONLLUSENTENCES)) {
				Log.info("CoNLL-U:");
				for (String l : conll) {
					Log.info(l);
				}
			}

			if (UDTreePreferences.BRAT.equals(pVisualisation)) {
				return BratPrintTree.print(svgFile, conll, Tvalues, NTvalues) != null;
			}
			else { // if (engine == null || UDTreePreferences.DEPTREEVIZ.equals(engine)) {
				return UDDepTreeVizPrintTree.print(svgFile, conll, Tvalues, NTvalues) != null;
			}
		}
		catch (Throwable e) {
			Log.warning("Error while drawing SVG: " + e + " at " + e.getStackTrace()[0]);
			Log.printStackTrace(e);
			return false;
		}
	}

	protected String[] getAvailableProperties() throws CqiClientException {
		List<WordProperty> props = corpus.getProperties();
		String[] sprops = new String[props.size()];

		for (int i = 0; i < props.size(); i++) {
			sprops[i] = props.get(i).getName();
		}

		Arrays.sort(sprops);
		return sprops;
	}

	public String[] getAvailableTProperties() {

		//		String prefix = UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));
		//		
		//		String[] properties = new String[ImportCoNLLUAnnotations.UD_PROPERTY_NAMES.length];
		//		for (int p = 0 ; p < ImportCoNLLUAnnotations.UD_PROPERTY_NAMES.length ; p++) {
		//			properties[p] = prefix+ImportCoNLLUAnnotations.UD_PROPERTY_NAMES[p];
		//		}
		try {
			return getAvailableProperties();
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new String[0];
		}
	}

	/**
	 * UD words stores the T and NT values
	 */
	public final String[] getAvailableNTProperties() {
		return getAvailableTProperties();
	}

	@Override
	public CQPCorpus getParent() {
		return (CQPCorpus) super.getParent();
	}

	public boolean isReady() {
		return hasUDProperties(corpus,
				UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX)));
	}

	public int getNMatchingSentences() {

		if (matchingSentencesToMatches == null) return 0;

		return matchingSentencesToMatches.size();
	}

	public int getNSubMatch() {

		if (matchingSentencesKeys == null) return 0;

		org.txm.objects.Match currentSent = matchingSentencesKeys.get(pIndex);

		List<Integer> submatches = matchingSentencesToMatches.get(currentSent);

		return submatches.size();
	}

	public int getSub(int matchingSent) {

		return pSubIndex;//matchingSentencesToMatches.get(matchingSentencesKeys.get(matchingSent)).size();
	}

	/**
	 * @return the query or the generic one
	 */
	public String getQuery() {

		String prefix = UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));

		if (pQuery == null || pQuery.getQueryString().length() == 0) {
			return "[" + prefix + "id=\"1(-2)?\"]";
		}
		else {
			return pQuery.getQueryString();
		}
	}

	@Override
	public String toString() {
		if (pQuery != null) {
			return pQuery.getQueryString().substring(0, Math.min(20, pQuery.getQueryString().length()));
		}
		return "" + corpus;
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return false;
	}

	@Override
	public void clean() {

	}

	/**
	 * 
	 * @return the array of extensions to show in the FileDialog SWT widget
	 */
	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.xml" };
	}

	@Override
	public boolean canCompute() {
		return corpus != null; // && pQuery != null && pQuery.length() > 0; // if pQuery is empty/null set a default query
	}

	@Override
	public boolean _loadParameters() throws Exception {
		return true;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		String prefix = UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));

		if (pQuery == null || pQuery.getQueryString().length() == 0) {
			pQuery = new CQLQuery("[" + prefix + "id=\"1\"]"); // MD or use [ud-deprel="root"] ? but don't work if the corpus is not parsed
		}

		udSentences = UDSearchEngine.getSentenceSubcorpus(corpus, prefix);
		rawMatches = udSentences.query((CQLQuery) pQuery, "matches", false);
		nTotalMatches = 0; // counted below

		matchingSentencesToMatches = new LinkedHashMap<org.txm.objects.Match, List<Integer>>();
		;
		int nSent = udSentences.getSize();
		int imatch = 0;
		int nMatch = rawMatches.getNMatch();
		List<? extends org.txm.objects.Match> sentMatches = udSentences.getMatches();
		for (int iSent = 0; iSent < nSent; iSent++) {
			if (imatch >= nMatch) { // all matches processed
				break;
			}
			org.txm.objects.Match sentMatch = sentMatches.get(iSent);
			for (; imatch < nMatch; imatch++) {
				Match currentMatch = rawMatches.getMatch(imatch);
				if (currentMatch.getStart() > sentMatch.getEnd()) { // the match is after the sentence, process the next sentence
					break;
				}
				else if (sentMatch.getStart() > currentMatch.getEnd()) { // the sentence is after the match -> should not happen
					Log.warning("Warning: should not happen: the sentence is after the match: iSent=" + iSent + " " + sentMatch + " imatch=" + imatch + " " + currentMatch);
				}
				else {
					if (!matchingSentencesToMatches.containsKey(sentMatch)) {
						matchingSentencesToMatches.put(sentMatch, new ArrayList<Integer>());
					}
					matchingSentencesToMatches.get(sentMatch).add(imatch);
					nTotalMatches++;
				}
			}
		}

		if (imatch < nMatch) {
			Log.warning("Warning: should not happen: the matches should all be in the 'ud-s' subcorpus");
		}

		matchingSentencesKeys = new ArrayList<org.txm.objects.Match>(matchingSentencesToMatches.keySet());

		pIndex = 0;
		pSubIndex = 0;

		return true;
	}

	@Override
	public String getResultType() {
		return "UD search";
	}

	public void setIndexAndSubIndex(int sent, int sub) {
		this.pIndex = sent;
		this.pSubIndex = sub;
	}

	@Override
	public boolean hasSubMatchesStrategy() {

		return true;
	}

	public static boolean hasUDProperties(CQPCorpus corpus) {
		return hasUDProperties(corpus,
				UDPreferences.getInstance().getProjectPreferenceValue(corpus.getProject(), UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX)));
	}

	public static boolean hasUDProperties(CQPCorpus corpus, String prefix) {

		//		for (String prop : ImportCoNLLUAnnotations.UD_PROPERTY_NAMES) {
		//			try {
		//				if (corpus.getProperty(prefix+prop) == null) return false;
		//			}
		//			catch (CqiClientException e) {
		//				return false;
		//			}
		//		}
		//		return true;

		try {
			return corpus.getProperty(prefix + "id") != null;
		}
		catch (CqiClientException e) {
			return false;
		}
	}

	@Override
	public String getEngine() {

		return UDTreeSearchSelector.UD;
	}

	@Override
	public TreeSearchSelector getSelector() {

		return new UDTreeSearchSelector();
	}

	@Override
	public String[] getTextAndWordIDSOfCurrentSentence() throws Exception {

		String[] textAndWord = new String[2];

		WordProperty pid = corpus.getProperty("id");
		StructuralUnitProperty ptextid = corpus.getStructuralUnitProperty("text_id");

		org.txm.objects.Match match = this.udSentences.getMatches().get(this.pIndex);
		int[] positions = new int[match.size()];
		for (int i = 0; i < match.size(); i++) {
			positions[i] = match.getStart() + i;
		}

		List<String> idvalues = CQPSearchEngine.getEngine().getValuesForProperty(positions, pid);

		int[] structid = CQPSearchEngine.getCqiClient().cpos2Struc(ptextid.getQualifiedName(), positions);
		String[] values = CQPSearchEngine.getCqiClient().struc2Str(ptextid.getQualifiedName(), structid);

		textAndWord[0] = values[0];
		textAndWord[1] = StringUtils.join(idvalues, ",");

		return textAndWord;
	}

	@Override
	public String getIconPath() {

		return "platform:/plugin/org.txm.conllu.core/icons/functions/UD.png";
	}

	@Override
	public File createTemporaryFile(File resultDir) {

		try {
			String engine = this.pVisualisation;//UDTreePreferences.getInstance().getString(UDTreePreferences.VISUALISATION);
			if (engine.equals(UDTreePreferences.BRAT)) {
				return File.createTempFile("UDTreeSearch", ".html", resultDir);
			}
			else {
				return File.createTempFile("UDTreeSearch", ".svg", resultDir);
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} //$NON-NLS-1$ //$NON-NLS-2$ull;
	}

	@Override
	public String[] getAvailableVisualisations() {

		return new String[] { UDTreePreferences.DEPTREEVIZ, UDTreePreferences.BRAT };
	}

	@Override
	public int getTotalSubMatch() {
		return nTotalMatches;
	}
}
