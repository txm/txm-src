package org.txm.conllu.core.function;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.txm.utils.BundleUtils;
import org.txm.utils.io.IOUtils;

public class BratPrintTree {

	public static File print(File file, List<String> conll, String[] Tvalues, String[] NTvalues) {

		try {
			ArrayList<String[]> splittedLines = new ArrayList<>();
			for (int i = 0; i < conll.size(); i++) {
				String l = conll.get(i);
				String split[] = l.split("\t"); //$NON-NLS-1$
				splittedLines.add(split);
			}

			ArrayList<String> conll2 = new ArrayList<>();
			for (int i = 0; i < splittedLines.size(); i++) {
				String split[] = splittedLines.get(i);

				if (split[0].contains("-")) { //$NON-NLS-1$
					int n1 = Integer.parseInt(split[0].substring(0, split[0].indexOf("-"))); //$NON-NLS-1$
					int n2 = Integer.parseInt(split[0].substring(1 + split[0].indexOf("-"))); //$NON-NLS-1$
					int n = n2 - n1;

					if (!(splittedLines.get(i + 1)[0].equals("" + n1)) || !(splittedLines.get(i + n + 1)[0].equals("" + n2))) { //$NON-NLS-1$

						ArrayList<String[]> newlines = new ArrayList<>();
						for (int j = 0; j <= n; j++) {
							newlines.add(new String[split.length]);

							newlines.get(j)[0] = "" + (n1 + j); //$NON-NLS-1$

							for (int p = 1; p < split.length; p++) {
								newlines.get(j)[p] = "_"; //$NON-NLS-1$
							}
						}

						//System.out.println("FIXING "+split);
						for (int p = 1; p < split.length - 1; p++) {
							String v = split[p];
							String[] splittedValues = v.split("\\."); //$NON-NLS-1$
							if (splittedValues.length == newlines.size()) {
								for (int j = 0; j <= n; j++) {
									newlines.get(j)[p] = splittedValues[j];
								}
							}
							else if ((splittedValues.length - 1) == newlines.size()) {
								for (int j = 0; j <= n; j++) {
									newlines.get(j)[p] = splittedValues[j + 1];
								}
							}
							else {
								for (int j = 0; j <= n; j++) {
									newlines.get(j)[p] = split[p];
								}
							}
						}

						for (int j = 0; j <= n; j++) {
							splittedLines.add(i + j + 1, newlines.get(j));

						}
						i = i + newlines.size();

					}
					else {
						//System.out.println("NOT FIXING "+conll.get(i));
					}
				}
			}

			for (int i = 0; i < splittedLines.size(); i++) {
				String split[] = splittedLines.get(i);
				conll2.add(StringUtils.join(split, "\t")); //$NON-NLS-1$
			}

			for (String l : conll2)
				System.out.println(l);

			String bundle_id = "org.txm.conllu.core"; //$NON-NLS-1$
			File HTMLTEMPATE = BundleUtils.getFile(bundle_id, "template", "/", "index.html"); //$NON-NLS-1$
			File root = HTMLTEMPATE.getParentFile();

			String content = IOUtils.getText(HTMLTEMPATE);
			content = content.replace("HTMLROOTDIRECTORY", root.getAbsolutePath()); //$NON-NLS-1$
			content = content.replace("CONLLUSENTENCE", StringUtils.join(conll2, "\n")); //$NON-NLS-1$

			IOUtils.write(file, content);
			//BundleUtils.copyFiles(bundle_id, "groovy", "org/txm/scripts/importer", "", scriptsPackageDirectory, true);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return file;
	}
}
