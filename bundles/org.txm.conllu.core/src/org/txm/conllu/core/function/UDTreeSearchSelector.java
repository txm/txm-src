package org.txm.conllu.core.function;

import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.treesearch.function.TreeSearch;
import org.txm.treesearch.function.TreeSearchSelector;


public class UDTreeSearchSelector extends TreeSearchSelector {

	public static String UD = "UD";

	@Override
	public String getEngine() {

		return UD;
	}

	@Override
	public boolean canComputeWith(CQPCorpus corpus) {

		return UDTreeSearch.hasUDProperties(corpus);
	}

	@Override
	public TreeSearch getTreeSearch(CQPCorpus corpus) {

		return new UDTreeSearch(corpus);
	}

	@Override
	public TreeSearch getTreeSearch(String resultnodepath, CQPCorpus corpus) {

		return new UDTreeSearch(resultnodepath, corpus);
	}

	@Override
	public Class<? extends Query> getQueryClass() {

		return CQLQuery.class;
	}
}
