package org.txm.conllu.core;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$

	public static String UDSearchEngine_0;

	public static String UDSearchEngine_9;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
