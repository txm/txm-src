package org.txm.conllu.core;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * 
 * @author mdecorde
 *
 */
public class FixDriverFile {

	public static boolean fixFeatureValues(File driverFile, List<File> xmlFiles) throws ParserConfigurationException, SAXException, IOException {

		LinkedHashMap<String, LinkedHashSet<String>> declaredFeatures = new LinkedHashMap<String, LinkedHashSet<String>>();
		LinkedHashMap<String, LinkedHashSet<String>> missingFeatures = new LinkedHashMap<String, LinkedHashSet<String>>();
		LinkedHashMap<String, Element> featuresElements = new LinkedHashMap<String, Element>();

		Document doc = DomUtils.load(driverFile);
		NodeList featuresList = doc.getElementsByTagName("feature"); //$NON-NLS-1$
		for (int i = 0; i < featuresList.getLength(); i++) {
			Element f = (Element) featuresList.item(i);

			featuresElements.put(f.getAttribute("name") + "\t" + f.getAttribute("domain"), f); //$NON-NLS-1$
			LinkedHashSet<String> values = new LinkedHashSet<String>();
			declaredFeatures.put(f.getAttribute("name") + "\t" + f.getAttribute("domain"), values); //$NON-NLS-1$
			missingFeatures.put(f.getAttribute("name") + "\t" + f.getAttribute("domain"), new LinkedHashSet<String>()); //$NON-NLS-1$

			NodeList featureValuesList = f.getElementsByTagName("value"); //$NON-NLS-1$
			for (int j = 0; j < featureValuesList.getLength(); j++) {
				Element v = (Element) featureValuesList.item(j);
				values.add(v.getAttribute("name")); //$NON-NLS-1$
			}
		}
		//System.out.println("Declared: "+declaredFeatures.keySet());

		for (File xmlFile : xmlFiles) {
			Document doc2 = DomUtils.load(xmlFile);
			NodeList tList = doc2.getElementsByTagName("t"); //$NON-NLS-1$
			for (int i = 0; i < tList.getLength(); i++) {
				Element e = (Element) tList.item(i);
				//System.out.println("T="+e.getAttributes());
				for (int j = 0; j < e.getAttributes().getLength(); j++) {

					String name = e.getAttributes().item(j).getLocalName() + "\tT"; //$NON-NLS-1$
					String value = e.getAttributes().item(j).getNodeValue();

					if (declaredFeatures.containsKey(name) && declaredFeatures.get(name).size() > 0) {
						HashSet<String> existingValues = declaredFeatures.get(name);
						if (existingValues.contains(value)) {
							// ok
						}
						else {
							missingFeatures.get(name).add(value);
						}
					}
				}

			}
			tList = doc2.getElementsByTagName("nt"); //$NON-NLS-1$
			for (int i = 0; i < tList.getLength(); i++) {
				Element e = (Element) tList.item(i);
				//System.out.println("NT="+e.getAttributes());
				for (int j = 0; j < e.getAttributes().getLength(); j++) {

					String name = e.getAttributes().item(j).getLocalName() + "\tNT"; //$NON-NLS-1$
					String value = e.getAttributes().item(j).getNodeValue();

					if (declaredFeatures.containsKey(name) && declaredFeatures.get(name).size() > 0) {
						HashSet<String> existingValues = declaredFeatures.get(name);
						if (existingValues.contains(value)) {
							// ok
						}
						else {
							missingFeatures.get(name).add(value);
						}
					}
				}
			}
		}

		//System.out.println("Missing values:");
		for (String missingFeatureNamedomain : missingFeatures.keySet()) {
			if (missingFeatures.get(missingFeatureNamedomain).size() == 0) continue;
			//System.out.println("\t"+missingFeatureNamedomain);

			Element f = featuresElements.get(missingFeatureNamedomain);
			//missingFeatures.get(missingFeatureNamedomain).add("");

			for (String v : missingFeatures.get(missingFeatureNamedomain)) {
				//System.out.println("\t\t"+v);
				Element missingFeatureElement = f.getOwnerDocument().createElement("value"); //$NON-NLS-1$
				missingFeatureElement.setAttribute("name", v); //$NON-NLS-1$
				missingFeatureElement.setTextContent(v);
				f.appendChild(missingFeatureElement);
			}
		}

		return DomUtils.save(doc, driverFile);
	}

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File[] files = { new File("runtime-rcpapplication.product/corpora/VOEUX-CONLLU/tiger-xml/1959.xml"), //$NON-NLS-1$
				new File("runtime-rcpapplication.product/corpora/VOEUX-CONLLU/tiger-xml/1960.xml"), //$NON-NLS-1$
				new File("runtime-rcpapplication.product/corpora/VOEUX-CONLLU/tiger-xml/1961.xml") }; //$NON-NLS-1$

		FixDriverFile.fixFeatureValues(new File("runtime-rcpapplication.product/corpora/VOEUX-CONLLU/tiger-xml/corpus.xml"), Arrays.asList(files)); //$NON-NLS-1$
	}
}
