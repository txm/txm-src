package org.txm.conllu.core;

import java.io.File;

import org.txm.PostTXMHOMEInstallationStep;
import org.txm.Toolbox;
import org.txm.objects.Workspace;
import org.txm.utils.BundleUtils;

public class InstallGroovyCoNLLUFiles extends PostTXMHOMEInstallationStep {

	final static String createfolders[] = { "scripts/groovy/lib", "scripts/groovy/user", "scripts/groovy/system" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

	@Override
	public boolean do_install(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());

		for (String folder : createfolders) {
			new File(txmhomedir, folder).mkdirs();
		}
		File scriptsDirectory = new File(txmhomedir, "scripts/groovy"); //$NON-NLS-1$
		File userDirectory = new File(scriptsDirectory, "user"); //$NON-NLS-1$
		File systemDirectory = new File(scriptsDirectory, "system"); //$NON-NLS-1$

		String bundle_id = "org.txm.conllu.core"; //$NON-NLS-1$
		File scriptsPackageDirectory = new File(userDirectory, "org/txm/scripts/importer"); //$NON-NLS-1$
		File scriptsPackageDirectory2 = new File(systemDirectory, "org/txm/scripts/importer"); //$NON-NLS-1$
		scriptsPackageDirectory.mkdirs();
		scriptsPackageDirectory2.mkdirs();
		BundleUtils.copyFiles(bundle_id, "groovy", "org/txm/scripts/importer", "", scriptsPackageDirectory, true); //$NON-NLS-1$
		BundleUtils.copyFiles(bundle_id, "groovy", "org/txm/scripts/importer", "", scriptsPackageDirectory2, true); //$NON-NLS-1$

		return scriptsDirectory.exists();
	}

	@Override
	public String getName() {
		return "UD (org.txm.conllu.core)"; //$NON-NLS-1$
	}

	@Override
	public boolean needsReinstall(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());
		for (String folder : createfolders) {
			if (!new File(txmhomedir, folder).exists()) {
				return true;
			}
		}


		return false;
	}
}
