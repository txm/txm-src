package org.txm.scripts.importer.conllu

import org.txm.Toolbox
import org.txm.importer.xtz.ImportModule;
import org.txm.metadatas.Metadata
import org.txm.metadatas.Metadatas
import org.txm.utils.io.FileCopy
import org.txm.utils.io.IOUtils
import org.txm.importer.xtz.*
import org.txm.scripts.importer.xtz.*
import org.txm.conllu.core.function.ImportCoNLLUAnnotations
import org.txm.conllu.core.preferences.UDPreferences
import org.txm.importer.ApplyXsl2;
import javax.xml.stream.*
import org.txm.utils.AsciiUtils
import org.txm.utils.ConsoleProgressBar
import org.txm.utils.FileUtils
import org.txm.conllu.core.preferences.UDPreferences
/**
 * Only build the Metadatas object since all XML-TXM files already exists.
 * Metadatas is used to build text order.
 * 
 * 
 * @author mdecorde
 *
 */
class CoNLLUImporter extends XTZImporter {

	public CoNLLUImporter(ImportModule module) {
		super(module)
	}

	public final String merge(String orig, def sss) {

		if (orig.equals("") || orig.equals("_")) {

		} else {
			sss.add(0, orig)
		}

		def ssset = new LinkedHashSet(sss)

		return ssset.join(".")
	}

	@Override
	public void process() {

		File conlluSrcDirectory = inputDirectory

		boolean usenewdocid =  "true".equals(UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.IMPORT_USE_NEW_DOC_ID)); // THE conllu -> Tiger XSL MUST HAVE THE SAME BEHAVIOR BEFORE //

		if (usenewdocid) {
			conlluSrcDirectory = new File(outputDirectory.getParentFile().getParentFile(), "conllu")
			conlluSrcDirectory.deleteDir()
			conlluSrcDirectory.mkdirs()

			if (!splitCoNLLUFiles(inputDirectory, conlluSrcDirectory, project)) {
				return
			}
		}

		def files = conlluSrcDirectory.listFiles()
		files.sort()

		println "Add XmlId if necessary & remove empty nodes"

		String contractionsManagement =  UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.CONTRACTIONS_MANAGEMENT, UDPreferences.getInstance().getString(UDPreferences.CONTRACTIONS_MANAGEMENT));

		ConsoleProgressBar cpb_texts = new ConsoleProgressBar(files.size())
		for (File conlluFile : files) {
			cpb_texts.tick()
			if (conlluFile.getName().endsWith(".conllu")) {
				String textid = FileUtils.stripExtension(conlluFile)
				int wcounter = 1;

				ArrayList<String> lines = IOUtils.getLines(conlluFile, "UTF-8");
				for (int i = 0 ; i < lines.size() ; i++) {
					String line = lines[i]

					if (line.length() == 0 || line.startsWith("#") || !line.contains("\t")) continue;

					def split = line.split("\t", ImportCoNLLUAnnotations.UD_PROPERTY_NAMES.length);
					if (split[0].contains(".")) {
						//println "REMOVE EMPTY NODE: $split : "+
						lines.remove(i)
						i--
						continue; // next !
					}
				}
				
				def temp_multiwords = [:]
				for (int i = 0 ; i < lines.size() ; i++) {
					String line = lines[i]
					if (line.length() == 0 || line.startsWith("#") || !line.contains("\t")) continue;

					def split = line.split("\t", ImportCoNLLUAnnotations.UD_PROPERTY_NAMES.length);

					if (temp_multiwords.containsKey(split[0])) { // this word XMLid must be the same as its multiword id, see below
						String id = temp_multiwords.remove(split[0]);
						if (split[-1] == "_") {
							split[-1] = "XmlId="+id
						} else {
							split[-1] += "|XmlId="+id
						}
					} else {

						if (split[-1] != null && !split[-1].contains("XmlId=")) { // There is no XmlID -> create one and manage subwords
							String id = "w_"+textid+"_"+(wcounter++);
							if (split[-1] == "_") {
								split[-1] = "XmlId="+id
							} else {
								split[-1] += "|XmlId="+id
							}
							
							if (split[0].contains("-") && contractionsManagement == "surface") {
								temp_multiwords = [:] // reset to avoid using another multiwords 
								String[] fromstart= split[0].split("-", 2)
								int pfrom = Integer.parseInt(fromstart[0])
								int pend = Integer.parseInt(fromstart[1])
								for (int p = pfrom ; p <= pend ; p++) {
									temp_multiwords.put(""+p, id)
								}
								println temp_multiwords
							}
						}
					}

					lines[i] = split.join("\t") // rebuild the line
				}
				IOUtils.write(conlluFile, lines.join("\n") + "\n") // CoNLLU needs the last line
			}
		}
		cpb_texts.done()


		// Keep or not contractions
		File conlluSrcForTXMDirectory = new File(outputDirectory.getParentFile().getParentFile(), "conllu-fortxm")

		if (contractionsManagement == UDPreferences.ALL) {
			conlluSrcForTXMDirectory = conlluSrcDirectory; // use the same directory as TIGER since no word modifications have been done
		} else {

			conlluSrcForTXMDirectory.deleteDir()
			conlluSrcForTXMDirectory.mkdirs()

			println "Contractions management mode is '$contractionsManagement'"
			cpb_texts = new ConsoleProgressBar(files.size())
			for (File conlluFile : files) {
				cpb_texts.tick()

				if (conlluFile.getName().endsWith(".conllu")) {

					File conlluFile2 = new File(conlluSrcForTXMDirectory, conlluFile.getName())

					String textid = FileUtils.stripExtension(conlluFile)
					int wcounter = 1;

					ArrayList<String> lines = IOUtils.getLines(conlluFile, "UTF-8");

					def temp_multiwords = [:]

					for (int i = 0 ; i < lines.size() ; i++) {
						String line = lines[i]
						if (line.length() == 0 || line.startsWith("#") || !line.contains("\t")) continue;

						def split = line.split("\t", ImportCoNLLUAnnotations.UD_PROPERTY_NAMES.length);
						
						if (contractionsManagement == UDPreferences.SYNTAX) {
							if (split[0].contains("-")) {

								// stores the syntatic word id and the ortographic word properties
								temp_multiwords = [:] // reset to avoid using another multiwords 
								int n1 = Integer.parseInt(split[0].substring(0,  split[0].indexOf("-")));
								int n2 = Integer.parseInt(split[0].substring(1 + split[0].indexOf("-")));
								for (int ii = n1 ; ii <= n2 ; ii++) {
									temp_multiwords[""+ii] = split;
								}

								//println "REMOVE - $split"
								lines.remove(i)
								i--
								continue; /// next !
							} else if (temp_multiwords.containsKey(split[0])) { // it's a syntactic word of an orthographic word
								def split_ortho = temp_multiwords.remove(split[0])

								if (split[9].length() > 0) split[9] += "|"
								split[9] += "multiword="+split_ortho[1] // the orthographic form
							}
						} else if (contractionsManagement == UDPreferences.SURFACE) {
							if (split[0].contains("-")) {
								int n1 = Integer.parseInt(split[0].substring(0,  split[0].indexOf("-")));
								int n2 = Integer.parseInt(split[0].substring(1 + split[0].indexOf("-")));
								int n =  n2 - n1

								//split[0] = ""+n1

								// before merging and deleting words, check if they are the right ones
								if (lines[i+1].startsWith(""+n1+"\t") && lines[i+n+1].startsWith(""+n2+"\t")) {
									def splits = []
									for (int j = 0 ; j <= n ;j++) {
										def tmp = lines[i+j+1].split("\t", ImportCoNLLUAnnotations.UD_PROPERTY_NAMES.length);
										splits << tmp
									}

									for (int j = 1 ; j < 8 ; j++) {
										split[j] = merge(split[j], splits.collect(){it[j]})
									}

									//println "REMOVE non- $split"
									for (int j = 0 ; j <= n ;j++) {
										lines.remove(i+1)
									}
								}
								//println "splits=$splits"
							}
						}

						lines[i] = split.join("\t") // rebuild the line
					}
					IOUtils.write(conlluFile2, lines.join("\n") + "\n") // CoNLLU needs the last line
				}
			}
			cpb_texts.done()
		}
		File metadataFile = Metadatas.findMetadataFile(module.sourceDirectory)
		File srcDirectory = new File(outputDirectory.getParentFile().getParentFile(), "conllu2tei")
		srcDirectory.deleteDir()
		srcDirectory.mkdirs()

		if (metadataFile != null && metadataFile.exists()) {
			File metadataFile2 = new File(srcDirectory, metadataFile.getName())
			FileCopy.copy(metadataFile, metadataFile2)
		}

		println "Convert CoNLL-U to XML-TEI..."
		convertCoNLLU2TEI(conlluSrcForTXMDirectory, srcDirectory, project)

		inputDirectory = srcDirectory // switch files source directory

		super.process()
	}

	public static def splitCoNLLUFiles(File inputDirectory, File srcDirectory, def project) {
		def files = inputDirectory.listFiles(new FilenameFilter() {
					boolean accept(File dir, String name) {
						return name.toLowerCase().endsWith(".conllu")
					}
				});

		if (files == null) {
			println "Aborting. No CONLL file found in $inputDirectory."
			return false
		}
		files.sort()
		ConsoleProgressBar cpb_texts = new ConsoleProgressBar(files.size())

		println "Splitting CoNLL-U files..."
		for (File master : files) {

			cpb_texts.tick()

			if (!master.getName().endsWith(".conllu")) {
				continue
			}

			String orig_text_id = FileUtils.stripExtension(master)
			String current_text_id = FileUtils.stripExtension(master)
			File conlluFile = new File(srcDirectory, current_text_id+".conllu")
			def writer = conlluFile.newWriter("UTF-8", true)

			master.eachLine("UTF-8") { line ->
				if (line.startsWith("# newdoc id = ")) {

					String text_id = line.substring("# newdoc id = ".length())
					if (!text_id.equals(current_text_id)) {
						writer.close()
						current_text_id = text_id
						conlluFile = new File(srcDirectory, current_text_id+".conllu")
						writer = conlluFile.newWriter("UTF-8", true)
					}
				}

				writer.println(line)
			}
			writer.close()
		}
		cpb_texts.done()
		return true
	}

	public static def convertCoNLLU2TEI(File inputDirectory, File srcDirectory, def project) {

		def files = inputDirectory.listFiles()

		if (files == null) {
			println "Aborting. No CONLL file found in $inputDirectory."
			return false
		}
		files.sort()
		def properties = Arrays.asList(ImportCoNLLUAnnotations.UD_PROPERTY_NAMES)

		String prefix = UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.UDPREFIX, UDPreferences.getInstance().getString(UDPreferences.UDPREFIX));

		UDPreferences.getInstance().setProjectPreferenceValue(project, UDPreferences.UDPREFIX, prefix); // copy the current preference into the corpus preference

		def headPropertiesToProject = UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.IMPORT_HEAD_TO_PROJECT, UDPreferences.getInstance().getString(UDPreferences.IMPORT_HEAD_TO_PROJECT)).split(",") as Set

		def depsPropertiesToProject = UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.IMPORT_DEPS_TO_PROJECT, UDPreferences.getInstance().getString(UDPreferences.IMPORT_DEPS_TO_PROJECT)).split(",") as Set

		def formatSentences = "true" == UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.IMPORT_PRINT_NEWLINES_AFTER_SENTENCES, ""+UDPreferences.getInstance().getString(UDPreferences.IMPORT_PRINT_NEWLINES_AFTER_SENTENCES))

		String contractionsManagement =  UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.CONTRACTIONS_MANAGEMENT, UDPreferences.getInstance().getString(UDPreferences.CONTRACTIONS_MANAGEMENT));

		ConsoleProgressBar cpb_texts = new ConsoleProgressBar(files.size())

		println "Parsing CoNLL-U files..."
		for (File master : files) {

			cpb_texts.tick()

			if (!master.getName().endsWith(".conllu")) {
				continue
			}

			def content = [] // list of sentence

			String text_id = FileUtils.stripExtension(master)
			String sent_id = ""
			String par_id = "1"
			def comments = [] // /text/par/sent
			def words = []

			master.eachLine("UTF-8") { line ->

				if (line.startsWith("# newdoc id = ")) {
					// already set or ignored
				} else if (line.startsWith("# sent_id = ")) {
					sent_id = line.substring("# sent_id = ".length())
				} else if (line.startsWith("# newpar id = ")) {
					par_id = line.substring("# newpar id = ".length())
				} else if (line.startsWith("#")) {
					comments << line
				} else if (line.trim().isEmpty()) {
					if (words.size() > 0) {
						def sentence = [par_id, sent_id, words, comments]
						content.add(sentence)

						sent_id = ""
						par_id = "1"
						comments = []
						words = []
					}

				} else {
					LinkedHashMap<String, String> wProperties = new LinkedHashMap<String, String>()

					def split = line.split("\t", ImportCoNLLUAnnotations.UD_PROPERTY_NAMES.length)
					if (split.size() == properties.size()) {
						String id = split[0]
						for (int i = 0 ; i < split.size() ; i++) {
							wProperties[properties[i]] = split[i]
						}

						//						if (wProperties.get("id").equals("1") || wProperties.get("id").startsWith("1-")) { // it's a new sentence, store the current if any and starts a new sentence
						//							if (words.size() > 0) {
						//								def sentence = [par_id, sent_id, words, comments]
						//								content.add(sentence)
						//
						//								sent_id = ""
						//								par_id = "1"
						//								comments = []
						//								words = []
						//							}
						//						}
						if (wProperties[properties[0]].contains(".")) { // id
							// empty node
						} else {
							words << wProperties
						}
					} else {
						//println "Warning: not a line: "+line
					}
				}
			}

			if (words.size() > 0) { // last sentence ?
				def sentence = [par_id, sent_id, words, comments]
				content.add(sentence)
			}

			if (content.size() == 0) {
				continue;
			}

			//println "${content.size()} sentences found."

			File xmlFile = new File(srcDirectory, text_id+".xml")
			// println "xmlFile=$xmlFile"
			BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(xmlFile))
			XMLOutputFactory factory = XMLOutputFactory.newInstance()
			XMLStreamWriter writer = factory.createXMLStreamWriter(output, "UTF-8")

			writer.writeStartDocument("UTF-8","1.0")
			writer.writeStartElement ("TEI")
			writer.writeDefaultNamespace("http://www.tei-c.org/ns/1.0")
			writer.writeNamespace("txm", "http://textometrie.org/1.0")
			writer.writeCharacters("\n")
			writer.writeStartElement ("teiHeader")
			writer.writeEndElement()
			writer.writeCharacters("\n")
			writer.writeStartElement ("text")

			writer.writeCharacters("\n")

			String current_par_id = null
			int wordCounter = 0
			for (def sentence : content) { // for all paragraph of the current text

				par_id = sentence[0]
				sent_id = sentence[1]
				words = sentence[2]
				comments = sentence[3]

				if (current_par_id == null || par_id != current_par_id) {
					if (current_par_id != null) {
						writer.writeEndElement() // p
					}
					writer.writeStartElement ("p")
					writer.writeAttribute("id", par_id)
					writer.writeCharacters("\n")

					current_par_id = par_id
				}

				writer.writeStartElement ("s")
				writer.writeAttribute("id", sent_id)
				writer.writeCharacters("\n")

				for (def comment : comments) {
					writer.writeComment(comment.replace("--", "&#x2212;&#x2212;"))
					writer.writeCharacters("\n")
				}

				if (headPropertiesToProject.size() > 0 || depsPropertiesToProject.size() > 0) {
					LinkedHashMap sentencehash = new LinkedHashMap()
					//println "WORDS="+words
					for (def word : words) {
						sentencehash[word["id"]] = word
					}
					//println "SENTENCE="+sentencehash
					ImportCoNLLUAnnotations.buildPropertiesProjections(sentencehash, headPropertiesToProject, depsPropertiesToProject)
				}

				if (formatSentences) {
					writer.writeStartElement("p")
					writer.writeAttribute("type", "sentence")
					writer.writeAttribute("style", "--before-content:'$sent_id';")
				}

				for (def word : words) {

					String id = null
					String cqpWordValue = word["form"]
					int idx = word["form"].indexOf(".", 1);
					if (word["id"].contains("-") && idx > 0) {
						cqpWordValue = word["form"].substring(0, idx)
						word["form"] = word["form"].substring(idx+1)
					}
					wordCounter++
					writer.writeStartElement ("w")
					for (String p : word.keySet()) {
						if (p == "feats") word[p] = "|"+word[p]+"|"
						if (p == "misc" && word[p].contains("XmlId="))  {
							id =  word[p].substring(word[p].indexOf("XmlId=") + 6)
							if (id.contains("|")) { // more misc values after XmlId=
								id = id.substring(0, id.indexOf("|"));
							}
						}
						//println "WORD="+word
						writer.writeAttribute(prefix+p, word[p])
					}

					if (id != null) {
						writer.writeAttribute("id", id)
					} else {
						writer.writeAttribute("id", "w_"+text_id+"_"+wordCounter)
					}

					writer.writeCharacters(cqpWordValue)
					writer.writeEndElement() // w
					writer.writeCharacters(" ")
				}

				if (formatSentences) writer.writeEndElement()

				writer.writeCharacters("\n")
				writer.writeEndElement() // s
			}

			if (current_par_id != null) {
				writer.writeEndElement() // p
				writer.writeCharacters("\n")
			}

			writer.writeEndElement() // text
			writer.writeCharacters("\n")
			writer.writeEndElement() // TEI
			writer.close()
		}

		cpb_texts.done()

		return true
	}

}
