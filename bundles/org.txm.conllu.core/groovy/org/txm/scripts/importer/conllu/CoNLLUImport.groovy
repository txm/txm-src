package org.txm.scripts.importer.conllu;

import java.io.File;
import java.util.ArrayList;

import ims.tiger.index.writer.*
import ims.tiger.system.*

import org.txm.Toolbox;
import org.txm.importer.ApplyXsl2;
import org.txm.importer.xtz.*
import org.txm.objects.Project
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils
import org.apache.log4j.BasicConfigurator;
import org.txm.importer.xtz.*
import org.txm.scripts.importer.xtz.*
import org.txm.scripts.importer.tigersearch.TSImport
import org.txm.utils.*
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.searchengine.ts.TIGERSearchEngine
import org.txm.conllu.core.preferences.UDPreferences
import org.txm.tigersearch.preferences.TigerSearchPreferences
import org.txm.conllu.core.CallUD2TigerPerlScript

import org.txm.conllu.core.FixDriverFile

class CoNLLUImport extends XTZImport {
	
	public CoNLLUImport(Project params) {
		super(params);
	}
	
	@Override
	public void init(Project p) {
		super.init(p);
		
		importer = new CoNLLUImporter(this);
//		compiler = new XTZCompiler(this)
//		annotater = null; // no annotater step to do
//		pager = new XTZPager(this)
	}
	
	/**
	 * Do a XTZ Import then build the TIGERSearch indexes in the binary corpus "tiger" directory
	 */
	@Override
	public void start() throws InterruptedException {
		
		this.project.setDoTokenizerStep(false)
		
		super.start(); // call the usual XTZ import
		
		// build the TIGER-XML file
		CallUD2TigerPerlScript cutps = new CallUD2TigerPerlScript();
		
		boolean buildtigerindexes = "true".equals(UDPreferences.getInstance().getProjectPreferenceValue(this.project, UDPreferences.IMPORT_BUILD_TIGERSEARCH_INDEXES))
		if (!buildtigerindexes) {
			println "Skipping TIGER conversion step."
			return;
		}
		
		def formatSentences = "true" == UDPreferences.getInstance().getProjectPreferenceValue(project, UDPreferences.IMPORT_PRINT_NEWLINES_AFTER_SENTENCES, ""+UDPreferences.getInstance().getString(UDPreferences.IMPORT_PRINT_NEWLINES_AFTER_SENTENCES))
		println "FORMATTING SENTENCES: "+formatSentences
		if (formatSentences) {
			File conlluCorpusCSS = new File(this.binaryDirectory, "HTML/"+this.project.getName()+"/default/css/"+this.project.getName()+".css")
			conlluCorpusCSS.getParentFile().mkdirs()
			conlluCorpusCSS << """p[type="sentence"]::before {
	content:var(--before-content);
	color:grey;
}
"""
			println "conlluCorpusCSS=$conlluCorpusCSS"
		}
		
		if (cutps.canBuildTSFiles()) {
			
			println "Converting CoNLL-U files to TIGER-XML files..."
			
			File conlluDirectory = new File(this.binaryDirectory, "conllu")
			cutps.convertCoNLLUFiles(conlluDirectory.listFiles(), this.binaryDirectory.getAbsolutePath())
			
			File tigerXMLDirectory = new File(this.binaryDirectory, "tiger-xml")
			String driverFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DRIVER_FILENAME);
			File xslfile1 = BundleUtils.getFile("org.txm.conllu.core", "groovy", "/org/txm/scripts/importer/conllu/", "tigerXml-commentOutLongSentences.xsl")
			//File xslfile2 = BundleUtils.getFile("org.txm.conllu.core", "groovy", "/org/txm/scripts/importer/conllu/", "tigerXml-sortBfmByDate.xsl")
			
			println "Post-processing TIGER-XML files..."
			ApplyXsl2 a1 = new ApplyXsl2(xslfile1);
			//ApplyXsl2 a2 = new ApplyXsl2(xslfile2);
			for (File xmlFile : tigerXMLDirectory.listFiles()) {
				
				if (!xmlFile.getName().endsWith(".xml")) continue;
				if (xmlFile.getName().equals(driverFilename)) continue; // don't process the driver
				
				File xmlFileTmp = new File(xmlFile.getAbsolutePath()+".tmp")
				if (!(a1.process(xmlFile, xmlFileTmp) && xmlFile.delete() && xmlFileTmp.renameTo(xmlFile))) {
					println "Error while applying $xslfile1 to $xmlFile"
				}
				
//				if (!(a2.process(xmlFile, xmlFileTmp) && xmlFile.delete() && xmlFileTmp.renameTo(xmlFile))) {
//					println "Error while applying $xslfile2 to $xmlFile"
//				}
			}
			
			println "Patching TIGER-XML driver file..."
			def tigerxmlFiles = []
			def xmltxmFilesNames = this.getTXMFilesOrder();
			//println "xml-txm files: "+xmltxmFilesNames
			for (String name : xmltxmFilesNames) {
				name = FileUtils.stripExtension(name);
				
				File conlluFile = new File(this.binaryDirectory, "tiger-xml/"+name+".xml")
				//println " test "+conlluFile
				if (conlluFile.exists()) {
					tigerxmlFiles << conlluFile
				}
			}
			
			// patch the subcorpus tags in the driver XML file with the right corpus order : 1) the text order 2) the properties values
			File driver = new File(this.binaryDirectory, "tiger-xml/"+driverFilename)
			String content = IOUtils.getText(driver, "UTF-8");
			content = content.replaceAll("<subcorpus .+\n", "");
			String subcorpusList = "";
			for (String name : xmltxmFilesNames) {
				name = FileUtils.stripExtension(name);
				subcorpusList += "<subcorpus name=\"$name\" external=\"file:${name}.xml\"/>\n"
			}
			content = content.replaceAll("<body>", "<body>\n"+subcorpusList+"\n"); // get the last main.xml content and patch it with the subcorpus tags
			
			// write the modified driver file
			IOUtils.setText(driver, content, "UTF-8");
			
			FixDriverFile.fixFeatureValues(driver, tigerxmlFiles)
			
			// build TIGER indexes
			if (isSuccessful) {
				// read from the 'tiger-xml' and write to the 'tiger' directory
				TIGERSearchEngine.buildTIGERCorpus(tigerXMLDirectory, this.binaryDirectory, corpusName);
				
				// re-align TIGER word indexes with the CQP word indexes using the TS@editionId and CQP@id properties
				File tigerDirectory = new File(this.binaryDirectory, "tiger");
				File tigerCorpusExistingDirectory = new File(this.binaryDirectory, "tiger/"+corpusName);
				MainCorpus corpus = this.project.getFirstChild(MainCorpus.class);
				corpus.compute(false); // load  the corpus in CQP
				TIGERSearchEngine.writeOffsetDataFiles(corpus, "editionId", tigerCorpusExistingDirectory, tigerDirectory, tigerCorpusExistingDirectory)
			}
		} else {
			println "Can not do the TIGER indexes step."
		}
	}
}