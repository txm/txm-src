package org.txm.rcp.chartsengine.r.preferences;

// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté

// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//


import java.util.ArrayList;
import java.util.TreeSet;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbench;
import org.txm.Toolbox;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.r.core.RChartsEngine;
import org.txm.chartsengine.r.core.preferences.RChartsEnginePreferences;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.messages.ChartsEngineUIMessages;
import org.txm.core.engines.EngineType;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * R charts engine preference page.
 * 
 * @author sjacquot
 *
 */
public class RChartsEnginePreferencePage extends TXMPreferencePage {



	@Override
	public void createFieldEditors() {

		// TODO : add R plot themes selection?

		ArrayList<String> supportedOutputFormats = new ArrayList<String>();


		supportedOutputFormats.addAll(((ChartsEngine) Toolbox.getEngineManager(EngineType.CHARTS).getEngine(RChartsEngine.class)).getSupportedOutputDisplayFormats());
		supportedOutputFormats.addAll(((ChartsEngine) Toolbox.getEngineManager(EngineType.CHARTS).getEngine(RChartsEngine.class)).getSupportedOutputFileFormats());

		// Sort the set
		TreeSet<String> formatsSet = new TreeSet<String>(supportedOutputFormats);
		String[] formats = formatsSet.toArray(new String[formatsSet.size()]);

		String[][] displayFormats = new String[formats.length][2];
		for (int i = 0; i < formats.length; i++) {
			displayFormats[i][0] = formats[i];
			displayFormats[i][1] = formats[i];
		}

		addField(new ComboFieldEditor(RChartsEnginePreferences.OUTPUT_FORMAT, ChartsEngineUIMessages.displayFormat, displayFormats, getFieldEditorParent()));

	}



	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(RChartsEnginePreferences.getInstance().getPreferencesNodeQualifier()));
	}

	@Override
	public boolean performOk() {
		try {
			super.performOk();


			// Change the R Charts engine output format
			((ChartsEngine) Toolbox.getEngineManager(EngineType.CHARTS).getEngine(RChartsEngine.class)).setOutputFormat(this.getPreferenceStore().getString(RChartsEnginePreferences.OUTPUT_FORMAT));


			// Sets the new current charts SWT component provider according to the output format if the charts engine is the current one
			if (ChartsEngine.getCurrent() instanceof RChartsEngine) {
				SWTChartsComponentsProvider.setCurrrentComponentsProvider(ChartsEngine.getCurrent());

				//FIXME: old code
				// Set the first charts engine supported export file format as default charts export file format

				//				ChartsEnginePreferences.getInstance().put(ChartsEnginePreferences.RDEVICE, ChartsEngine.getCurrent().getSupportedOutputFileFormats().get(0));
				//				
				//				// Refresh the User\Export preference page to update "Default charts export file format" combo box
				//				PlatformUI.getWorkbench().getPreferenceManager().
				//					find("org.txm.rcp.preferences.TXMMainPreferencePage/" + UserPreferencePage.ID + "/" + ExportPreferencePage.ID).createPage(); //$NON-NLS-1$ //$NON-NLS-2$
			}


		}
		catch (Exception e) {
			System.err.println(NLS.bind(TXMUIMessages.failedToSavePreferencesColonP0, e));
		}
		return true;
	}
}
