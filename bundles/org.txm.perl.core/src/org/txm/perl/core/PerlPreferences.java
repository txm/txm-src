package org.txm.perl.core;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * 
 * @author mdecorde
 *
 */
public class PerlPreferences extends TXMPreferences {

	public static final String EXECUTABLE_PATH = "exec";

	public static final String ADDITIONAL_PARAMETERS = "exec_parameters";

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		// TODO Auto-generated method stub

		Preferences preferences = getDefaultPreferencesNode();

		preferences.put(EXECUTABLE_PATH, "perl");
		preferences.put(ADDITIONAL_PARAMETERS, "");
	}

	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(PerlPreferences.class)) {
			new PerlPreferences();
		}
		return TXMPreferences.instances.get(PerlPreferences.class);
	}
}
