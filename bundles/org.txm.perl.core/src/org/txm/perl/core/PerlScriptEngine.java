package org.txm.perl.core;


import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.txm.core.engines.ScriptEngine;
import org.txm.utils.logger.Log;

/**
 * 
 * @author mdecorde
 *
 */
public class PerlScriptEngine extends ScriptEngine {

	public PerlScriptEngine() {
		super("perl", "pl");
	}

	@Override
	public boolean isRunning() {

		String path = PerlPreferences.getInstance().getString(PerlPreferences.EXECUTABLE_PATH);

		if (path.isEmpty()) return false;

		//		if (false && (OSDetector.isFamilyUnix() || OSDetector.isFamilyMac())) {
		//			if (path != null && path.length() > 0) {
		//				cmd.add(0, new File(path, perlRunner).getAbsolutePath());
		//			} else {
		//				cmd.add(0, perlRunner);
		//			}
		//			
		//			String code = "'"+StringUtils.join(cmd, "' '")+"'";
		//			cmd = new ArrayList(Arrays.asList("bash", "-c", code));
		//		} else {
		if (path != null && path.length() > 0) {
			return new File(path).canExecute();
		}
		else {
			//return new File(perlRunner).canExecute();
			return !path.isEmpty();
		}
		//		}
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public IStatus executeScript(File script, HashMap<String, Object> env, List<String> args) {
		ArrayList<String> cmd = new ArrayList<String>();
		cmd.add(script.getAbsolutePath());
		cmd.addAll(args);
		//System.out.println("Running perl script...");
		return runPerl(cmd, env);
	}

	@Override
	public IStatus executeText(String str, HashMap<String, Object> env) {
		ArrayList<String> cmd = new ArrayList<String>();
		cmd.add("-c");
		cmd.add(str);
		//System.out.println("Running Perl code...");
		return runPerl(cmd, env);
	}

	public boolean canExecute(File script) {

		return script.isFile()
				&& script.canRead();
	}

	private static IStatus runPerl(ArrayList<String> cmd, HashMap<String, Object> env) {

		try {
			cmd = new ArrayList<String>(cmd);

			String additionalParametersString = PerlPreferences.getInstance().getString(PerlPreferences.ADDITIONAL_PARAMETERS);
			String[] additionalParameters = additionalParametersString.split("\t");
			if (additionalParametersString.length() > 0 && additionalParameters.length > 0) {
				cmd.addAll(0, Arrays.asList(additionalParameters));
			}

			String path = PerlPreferences.getInstance().getString(PerlPreferences.EXECUTABLE_PATH);


			//			if (false && (OSDetector.isFamilyUnix() || OSDetector.isFamilyMac())) {
			//				if (path != null && path.length() > 0) {
			//					cmd.add(0, new File(path, perlRunner).getAbsolutePath());
			//				} else {
			//					cmd.add(0, perlRunner);
			//				}
			//				
			//				String code = "'"+StringUtils.join(cmd, "' '")+"'";
			//				cmd = new ArrayList(Arrays.asList("bash", "-c", code));
			//			} else {
			if (path != null && path.length() > 0) {
				cmd.add(0, new File(path).toString());
			}
			else {
				cmd.add(0, path);
			}
			//			}

			ProcessBuilder pb = new ProcessBuilder(cmd);
			pb.redirectErrorStream(true);

			Process process = pb.start();
			if (process != null) {
				BufferedReader bfr = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = "";
				while ((line = bfr.readLine()) != null) {
					// display each output line form perl script
					Log.info(line);
				}
				int e = process.waitFor();
				if (e != 0) {
					System.err.println("Process exited abnormally with code=" + e);
					return Status.CANCEL_STATUS;
				}
			}
			else {
				System.out.println("Error: perl process not created");
				return Status.CANCEL_STATUS;
			}
			//System.out.println("Done.");
		}
		catch (Exception e) {
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}
		return Status.OK_STATUS;
	}

	public static void main(String args[]) {
		ArrayList<String> cmd = new ArrayList<String>();
		//		cmd.add("-c");
		//		cmd.add("print('hi')");
		cmd.add("/home/mdecorde/TEMP/test.pl");
		runPerl(cmd, null);
	}
}
