package org.txm.annotation.rcp.editor;

import java.util.Locale;

import org.eclipse.swt.widgets.Composite;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;

/**
 * A widget to show in a to annotate a Result within a TXMEditor
 * 
 * @author mdecorde
 * 
 */
public abstract class AnnotationArea {

	protected TXMEditor<? extends TXMResult> editor;

	protected GLComposite annotationArea;

	protected AnnotationExtension extension;

	public AnnotationArea() {
	}

	/**
	 * TODO replace with a more generic method canAnnotateResult(TXMResult result) true/false
	 * 
	 * @return
	 */
	public abstract boolean canAnnotateResult(TXMResult result);

	/**
	 * TODO not done, should ask the
	 * 
	 * @return
	 */
	public String getLocale() {
		return Locale.getDefault().getLanguage();
	}

	/**
	 * use this if you need something to be done when the extension is discovered
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean install(TXMEditor<? extends TXMResult> editor, AnnotationExtension extension, Composite parent, int position) throws Exception;

	/**
	 * extra close steps
	 */
	protected abstract void _close();

	/**
	 * use this to save the annotations. Save must end when the files are updated
	 * 
	 * @return true if something was saved
	 */
	public abstract boolean save();

	public abstract String getName();

	public GLComposite getAnnotationArea() {
		return annotationArea;
	}

	public TXMEditor<? extends TXMResult> getEditor() {
		return editor;
	}

	public boolean allowMultipleAnnotations() {
		return false;
	}

	/**
	 * Called BEFORE the TXMResult is computed
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean notifyStartOfCompute() throws Exception;

	/**
	 * Called AFTER the TXMResult is computed
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean notifyEndOfCompute() throws Exception;


	public final boolean isAnnotationEnable() {
		return annotationArea != null;
	}

	/**
	 * Called after the editor has been refreshed (after the result has been computed)
	 * 
	 * @throws Exception
	 */
	public abstract void notifyEndOfRefresh() throws Exception;

	public abstract void notifyStartOfCreatePartControl() throws Exception;

	public abstract void notifyStartOfRefresh() throws Exception;

	public abstract boolean isDirty();

	public abstract boolean discardChanges();

	/**
	 * 
	 * @return true if the corpus indexes needs to be updated **after saving annotation**
	 */
	public abstract boolean needToUpdateIndexes();

	/**
	 * 
	 * @return true if the corpus editions needs to be updated **after saving updating indexes**
	 */
	public abstract boolean needToUpdateEditions();

	public abstract boolean hasChanges();
}
