package org.txm.annotation.rcp.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.txm.annotation.rcp.messages.AnnotationUIMessages;
import org.txm.concordance.core.functions.Concordance;
import org.txm.core.results.TXMResult;
import org.txm.objects.Text;
import org.txm.rcp.editors.TXMEditorExtension;
import org.txm.utils.logger.Log;

public class AnnotationExtension extends TXMEditorExtension<TXMResult> {

	/** The annotation area. */
	protected ArrayList<AnnotationArea> annotationAreas = new ArrayList<>();

	private Group controlArea;

	private SelectionListener defaultListener;

	//private ToolItem saveButton;

	public static final String GROUP_NAME = AnnotationUIMessages.Annotation;

	@Override
	public String getName() {
		return GROUP_NAME;
	}

	//	/**
	//	 * use this to enable/disable the button
	//	 * 
	//	 * @return
	//	 */
	//	public ToolItem getSaveButton() {
	//		return saveButton;
	//	}

	/**
	 * install the annotation start button and dropdown list
	 */
	@Override
	public void notifyEndOfCreatePartControl() throws Exception {

		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(AnnotationArea.class.getName());
		Arrays.sort(config, new Comparator<IConfigurationElement>() {

			@Override
			public int compare(IConfigurationElement o1, IConfigurationElement o2) {
				return o1.getAttribute("position").compareTo(o2.getAttribute("position")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		});

		ArrayList<String> modes = new ArrayList<>();
		ArrayList<SelectionListener> listeners = new ArrayList<>();
		for (final IConfigurationElement celement : config) {
			try {
				Object o = celement.createExecutableExtension("class"); //$NON-NLS-1$
				AnnotationArea aa = (AnnotationArea) o;

				if (!aa.canAnnotateResult(this.editor.getResult())) {
					continue;
				}
				Log.finest("Installing AnnotationExtension: " + aa.getName()); //$NON-NLS-1$
				modes.add(aa.getName()); // get the annotation name

				listeners.add(new SelectionListener() { // create a listener to instantiate a new AnnotationArea

					@Override
					public void widgetSelected(SelectionEvent e) {
						try {
							Object o = celement.createExecutableExtension("class"); //$NON-NLS-1$
							openAnnotationMode((AnnotationArea) o, -1);
						}
						catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
					}
				});
			}
			catch (Exception exx) {
				Log.printStackTrace(exx);
			}
		}

		if (modes.size() == 0) {
			return; // no need to show the buttons
		}

		SelectionListener openCloseSelectionListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// if (editor.getTopToolbar().isVisible(GROUP_NAME)) {
				// if (annotationAreas.size() == 0) {
				// openDefaultAnnotationMode(e);
				// }
				// }
				// else {
				// annotationAreas.clear();
				// }

				if (annotationAreas.size() == 0) { // if no annotation mode is opened, opens the default mode
					openDefaultAnnotationMode(e);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		defaultListener = listeners.get(0);
		String defaultMode = modes.get(0);
		controlArea = editor.getTopToolbar().installAlternativesGroup(GROUP_NAME, AnnotationUIMessages.annotateWordProperty,
				"platform:/plugin/org.txm.rcp/icons/functions/pencil_open.png", //$NON-NLS-1$
				"platform:/plugin/org.txm.rcp/icons/functions/pencil_close.png", false, //$NON-NLS-1$
				openCloseSelectionListener, modes, listeners);
		controlArea.setLayout(new GridLayout(1, true));

		//		saveButton = new ToolItem(editor.getTopToolbar(), SWT.PUSH);
		//		saveButton.setText("Save annotations");
		//		//saveButton.setImage(IImageKeys.getImage(IImageKeys.PENCIL_SAVE));
		//		saveButton.addSelectionListener(new SelectionListener() {
		//			
		//			@Override
		//			public void widgetSelected(SelectionEvent e) {
		//				try {
		//					notifyDoSave();
		//				}
		//				catch (Exception e1) {
		//					Log.printStackTrace(e1);
		//				}
		//			}
		//			
		//			@Override
		//			public void widgetDefaultSelected(SelectionEvent e) {}
		//		});
		//		saveButton.setEnabled(false);
	}

	public void closeAreasPanel() {
		editor.getTopToolbar().setVisible(GROUP_NAME, false);
		editor.getTopToolbar().getOpenCloseButton().setSelection(false);
		//		saveButton.setEnabled(false);
	}

	public void openAnnotationMode(AnnotationArea aa, int position) throws Exception {
		// AnnotationArea aa = (AnnotationArea)o;
		if (!aa.allowMultipleAnnotations()) {
			closeAllAreas(null);
			annotationAreas.clear();
		}
		else { // close the other annotation modes
			ArrayList<AnnotationArea> removed = new ArrayList<>();
			removed.addAll(closeAllAreas(aa.getClass()));
			annotationAreas.removeAll(removed);
		}

		// System.out.println("Starting "+aa.getName());

		aa.install(editor, AnnotationExtension.this, controlArea, position);

		editor.getTopToolbar().setVisible(GROUP_NAME, true);
		annotationAreas.add(aa);

		aa.notifyStartOfCompute();
		aa.notifyStartOfRefresh();

		//editor.refresh(false); // refresh the concordance display
	}

	/**
	 * 
	 * @param areaToAvoid
	 * @return the closed areas
	 */
	private ArrayList<AnnotationArea> closeAllAreas(Class<? extends AnnotationArea> areaToAvoid) {
		ArrayList<AnnotationArea> removed = new ArrayList<>();
		for (int i = 0; i < annotationAreas.size(); i++) {
			AnnotationArea a = annotationAreas.get(i);
			if (!a.getClass().equals(areaToAvoid)) {
				closeArea(a, false);
				removed.add(a);
				i--; // since annotationAreas was reduced
			}
		}
		return removed;
	}

	public void closeArea(AnnotationArea area, boolean closeExtensionIfEmpty) {
		if (area != null && !area.getAnnotationArea().isDisposed()) {
			area.getAnnotationArea().dispose();
		}
		area._close();

		this.annotationAreas.remove(area);
		if (this.annotationAreas.size() == 0 && closeExtensionIfEmpty) {
			this.closeAreasPanel();
		}
	}

	protected void openDefaultAnnotationMode(SelectionEvent e) {
		defaultListener.widgetSelected(e);
	}

	@Override
	public void notifyStartOfCompute() throws Exception {
		for (AnnotationArea aa : annotationAreas) {
			aa.notifyStartOfCompute();
		}
	}

	@Override
	public void notifyEndOfCompute() throws Exception {
		for (AnnotationArea aa : annotationAreas) {
			aa.notifyEndOfCompute();
		}
	}


	public boolean isAnnotationEnable() {
		return annotationAreas != null;
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		for (AnnotationArea aa : annotationAreas) {
			aa.notifyEndOfRefresh();
		}
	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		for (AnnotationArea aa : annotationAreas) {
			aa.notifyStartOfCreatePartControl();
		}
	}

	@Override
	public void notifyStartOfRefresh() throws Exception {
		for (AnnotationArea aa : annotationAreas) {
			aa.notifyStartOfRefresh();
		}
	}

	@Override
	public boolean isDirty() throws Exception {
		return false;

		//		boolean dirty = false;
		//		for (AnnotationArea aa : annotationAreas) {
		//			dirty = dirty || aa.isDirty();
		//		}
		//		return dirty;
	}

	@Override
	public boolean isSaveOnCloseNeeded() throws Exception {
		return true;
	}

	@Override
	public void notifyDoSave() throws Exception {

		//		if (needToUpdateIndexes) {
		//		JobHandler job = new JobHandler("Updating corpus indexes and editions", true) {
		//			
		//			@Override
		//			protected IStatus run(IProgressMonitor monitor) {
		//				
		//				this.runInit(monitor);
		//				
		//				try {
		//					monitor.setTaskName("Updating corpus XML-TXM files");
		//					
		//					Log.info("Saving annotations...");
		//					boolean needToUpdateIndexes = false;
		//					boolean needToUpdateEditions = false;
		//					// System.out.println("Saving annotations...");
		//					if (annotationAreas != null && annotationAreas.size() > 0) {
		//						for (AnnotationArea aa : annotationAreas) {
		//							needToUpdateIndexes = needToUpdateIndexes 
		//									|| (aa.isDirty() 
		//											&& aa.save()
		//											&& aa.needToUpdateIndexes());
		//							needToUpdateEditions = needToUpdateEditions || aa.needToUpdateEditions();
		//						}
		//					}
		//					
		//					if (needToUpdateIndexes) {
		//						final MainCorpus corpus = editor.getResult().getFirstParent(MainCorpus.class);
		//						monitor.setTaskName("Updating corpus indexes and editions");
		//						if (corpus != null && UpdateCorpus.update(corpus, needToUpdateEditions) != null) {
		//							monitor.worked(50);
		//							this.syncExec(new Runnable() {
		//								
		//								@Override
		//								public void run() {
		//									Log.info("Done.");
		//									CorporaView.refreshObject(corpus);
		//									if (!saveButton.isDisposed()) {
		//										saveButton.setEnabled(false);
		//									}
		//								}
		//							});
		//							return Status.OK_STATUS;
		//						}
		//						else {
		//							monitor.worked(50);
		//							Log.warning("Fail to update corpus. Aborting");
		//							Log.warning("Fix XML-TXM files and call command 'UpdateCorpus'");
		//							return Status.CANCEL_STATUS;
		//						}
		//					} else {
		//						this.syncExec(new Runnable() {
		//							
		//							@Override
		//							public void run() {
		//								Log.info("Done.");
		//								if (!saveButton.isDisposed()) {
		//									saveButton.setEnabled(false);
		//								}
		//							}
		//						});
		//						return Status.OK_STATUS;
		//					}
		//				} catch(ThreadDeath ex) {
		//					Log.warning(TXMUIMessages.executionCanceled);
		//					return Status.CANCEL_STATUS;
		//				}
		//			}
		//		};
		//		job.schedule();
		//		}
	}

	@Override
	public void notifyDispose() throws Exception {

		//FIXME MD: I have to do this because Eclipse is not asking for editor saves before exiting the app - BUG
		//		try {
		//			boolean annotationsDirty = false;
		//			for (AnnotationArea aa : annotationAreas) {
		//				annotationsDirty = annotationsDirty || aa.isDirty();
		//			}
		//			if (annotationsDirty) {
		//				boolean doSave = MessageDialog.openQuestion(this.editor.getShell(), "Annotations not saved", "Do you want to save the annotations before closing the editor?");
		//				if (doSave) {
		//					notifyDoSave();
		//				}
		//			}
		//		} catch(Exception e) {
		//			Log.printStackTrace(e);
		//		}

		for (AnnotationArea aa : annotationAreas) {
			aa._close();
		}
	}

	public void layout() {
		if (!controlArea.isDisposed()) {
			editor.layout(true);
		}
	}

	public int getNumberOfAnnotationArea() {
		return annotationAreas.size();
	}

	@Override
	public Set<Class<? extends TXMResult>> getTXMResultValidClasses() {
		HashSet<Class<? extends TXMResult>> h = new HashSet<>();
		h.add(Concordance.class);
		h.add(Text.class);
		return h;
	}


}
