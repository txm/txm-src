package org.txm.annotation.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;


/**
 * Annotation UI messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class AnnotationUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.annotation.rcp.messages.messages"; //$NON-NLS-1$

	public static String annotateWordProperty;

	public static String Annotation;

	public static String KnowledgeRepositoryName;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, AnnotationUIMessages.class);
	}

	private AnnotationUIMessages() {
	}
}
