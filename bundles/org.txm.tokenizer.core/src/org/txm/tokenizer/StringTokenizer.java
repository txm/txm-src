package org.txm.tokenizer;

import java.util.List;

/**
 * Abstract class of a String tokenizer
 * 
 * @author mdecorde
 *
 */
public interface StringTokenizer {

	/**
	 * 
	 * @param text
	 * @return the tokenized string : a list of sentences containing a list of tokens
	 */
	List<List<String>> processText(String text);

	/**
	 * 
	 * @return true if the String tokenizer detects sentences
	 */
	boolean doSentences();
}
