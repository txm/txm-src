// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-04-06 09:11:32 +0200 (jeu. 06 avril 2017) $
// $LastChangedRevision: 3425 $
// $LastChangedBy: mdecorde $
//
package org.txm.tokenizer;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.osgi.service.prefs.BackingStoreException;
import org.txm.utils.logger.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cern.colt.Arrays;

/**
 * Holder for tokenizer rules regexp
 */
public class TokenizerClasses {

	public String lang;

	public TokenizerClasses(String lang) {
		if (lang != null) {
			this.lang = lang;
		}
		else {
			this.lang = Locale.getDefault().getCountry();
		}
		reset();
	}

	public TokenizerClasses() {
		this(Locale.getDefault().getCountry());
	}

	/**
	 * Helper
	 * 
	 * @param projectScope
	 * @return
	 */
	public static TokenizerClasses newTokenizerClasses(ProjectScope projectScope, String lang) {
		TokenizerClasses tc = new TokenizerClasses();
		tc.loadFromProject(projectScope);
		tc.lang = lang;
		return tc;
	}

	//// ROOTS ? ////
	public boolean debug = false;

	/** The tag_all. */
	public String tag_all = null;

	/** The Weblex enclitics. */
	public String enclitics = null;

	public String encliticsFR = null;

	/** The TT enclitics. */
	public String FClitic_en = null;

	public String PClitic_fr = null;

	public String FClitic_fr = null;

	public String PClitic_it = null;

	public String FClitic_gl = null;

	/** The div_tags. */
	public String div_tags = null;

	/** The q_tags. */
	public String q_tags = null;

	/** The extraword_tags. */
	public String extraword1_tags = null;

	/** The corr_tags_no_seg. */
	public String corr_tags_no_seg = null;

	/** The word_tags. */
	public String word_tags = null;

	/** the element to create when a word is created */
	public String word_element_to_create = null;

	/** The intraword_tags. */
	public String intraword_tags = null;

	/** The punct_quotes. */
	public String punct_quotes = null;

	/** The punct_strong1. */
	public String punct_strong1 = null;

	/** The punct_strong2. */
	public String punct_strong2 = null;

	/** The punct_paren_open1. */
	public String punct_paren_open1 = null;

	/** The punct_paren_open2. */
	public String punct_paren_open2 = null;

	/** The punct_paren_close1. */
	public String punct_paren_close1 = null;

	/** The punct_paren_close2. */
	public String punct_paren_close2 = null;

	/** The punct_weak. */
	public String punct_weak = null;

	public String entity = null;

	public String seg_tags = null;

	//// COMBINAISONS ////

	/** The corr_tags. */
	public String corr_tags = null;

	/** The extraword_tags. */
	public String extraword_tags = null;

	/** The punct_strong. */
	public String punct_strong = null;

	/** The punct_paren_open. */
	public String punct_paren_open = null;

	/** The punct_paren_close. */
	public String punct_paren_close = null;

	/** The punct_paren. */
	public String punct_paren = null;

	/** The punct_all. */
	public String punct_all = null;

	/** The word_chars. */
	public String word_chars = null;

	public TTest[] tests = {
			// new TTest(/\A(.*-)(je|m[eo]i|tu|t[eo]i|lui|luy|ilz|ils?|no?u?s|vo?u?s|on|leurs|ce|ci|là|elles?)(.*)\Z/),
			// new TTest(/\A(.*)(-)()\Z/, "pon"),
			// new TTest(/\A(.*)(Bande dessinée|eau de vie|machine à écrire|Moyen Âge|petit pois|poule d'eau|Avoir l'air|se rendre compte|faire semblant|prendre froid|s'en aller|Comme il faut|bon
			// marché|bon enfant|en retard|en colère|à la mode|de bonne humeur|hors de propos|de travers|en vacances|de standing|à l'abandon|sans défense|pieds nus|Tout à fait|d'ores et déjà|de temps
			// en temps|tout de suite|de gré ou de force|ne pas|ne jamais|ne plus|sans doute|on ne peut plus|Quelque chose|ce dernier|n'importe quoi|N'importe quel|beaucoup de|plus d'un|peu de|un tas
			// de|un groupe de|je ne sais quel|De sorte que|bien que|parce que|c'est pourquoi|de même que|depuis que|Quant à|au-dessus de|par rapport à|à côté de|grâce à|hors de|le long de|Par ma
			// barbe !|Mon dieu !|Eh bien !|Bonne nuit !|Pas de panique !)(.*)\Z/),
			// new TTest(/\A([Aa]ujourd'hui)(.*)/,0,1,2),

			// BFM ONLY
			// new TTest(/\A()([.·][^ .·]+[.·])(.*)\Z/, "num"),

			// TXM REFMAN ONLY
			// new TTest(/\A(.*)($tag_all)(.*)\Z/, "tag"),
			// new TTest(/\A(.*)([0-9][0-9][\/][0-9][0-9][\/][0-9][0-9])(.*)\Z/, "date", 1,2,3),
			// new TTest("^(.*)([0-9][0-9]:[0-9][0-9]:[0-9][0-9])(.*)$", "time", 1, 2, 3),
			// new TTest("^([^0-9]*)([0-9]+(\\.[0-9]+)?(\\.[0-9]+[a-z]*))(.*)$", "version", 1, 2, 5),
			// new TTest(/\A([^0-9]*)([0-9]+(\.[0-9]+)(\.[0-9]+)+)(\.[0-9]+[a-z]+)?(.*)\Z/, "version", 1,2,6),
			// new TTest("^(.*)(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)(.*)$", "url", 1, 2, 5),
			// new TTest(/\A()([0-9]+([.,][0-9]+)?)()\Z/, "num", 1,2,4),
			// new TTest(/\A()(([A-Z]:)?([\/\\][^\/\\]+)+)()\Z/, "path", 1,2,5),

			// new TTest(/\A([\p{Ps}|\p{Pe}|\p{Pi}|\p{Pf}|\p{Po}|\p{S}]*)(\p{L}++['‘’])(.*)\Z/, "w", 1, 2, 3),
			// new TTest(/\A(.*)(\.\.\.)(.*)\Z/, "pon"),
			// new TTest(/\A(.*)(\p{Ps}|\p{Pe}|\p{Pi}|\p{Pf}|\p{Po}|\p{S})(.*)\Z/, "pon")
	};

	public String whitespaces = null;

	public String regElision = null;

	public String regPunct = null;

	// punct_strong1 = ".!?";
	// punct_strong2 = "\\.\\.|\\.\\.\\.|…|\\|";
	// punct_strong = "[$punct_strong1]|$punct_strong2";
	// punct_paren_open1 = "«";
	// punct_paren_open2 = "<<|``|\\(|\\[|\\{";
	// punct_paren_open = "$punct_paren_open1|$punct_paren_open2";
	// punct_paren_close1 = "»";
	// punct_paren_close2 = ">>|''|‘‘|’’|\\)|\\]|\\}";
	// punct_paren_close = "$punct_paren_close1|$punct_paren_close2";
	// punct_paren = "$punct_paren_open|$punct_paren_close";
	// punct_weak = ",;∼ˆ·*:\"`'“”\\+±=/\\-≤≥<>\\—_ν√μ•@≈→αγ∞≡σ&¼½Θĩ†φθΓ§ẽə∈";
	// punct_all = "$punct_strong|$punct_paren|[$punct_weak]";
	// word_chars = "[^ $punct_strong1$punct_paren_open1$punct_paren_close1$punct_weak]|&[^;]+;";

	public void reset() {

		if (debug) System.out.println("Reset TC"); //$NON-NLS-1$
		tag_all = "<[A-Za-z][^>]+>"; //$NON-NLS-1$

		div_tags = "TEI|text|front|body|div|div1|div2|div3|div4|div5|div6|back|head|trailer|p|ab|sp|speaker|list|notice|bibl|opener|dateline"; //$NON-NLS-1$
		q_tags = "q|quote|said|item|stage|cit|label|heraldry"; //$NON-NLS-1$
		extraword1_tags = "expan|pb|lb|milestone|gap|note|s|locus|title|ref|hi|witDetail"; //$NON-NLS-1$
		corr_tags_no_seg = "expan|unclear|choice|corr|sic|reg|orig|foreign|hi|title|name|supplied|subst|add|del|damage|date|idno|surplus"; //$NON-NLS-1$
		word_tags = "w"; //$NON-NLS-1$
		word_element_to_create = "w"; //$NON-NLS-1$
		intraword_tags = "c|ex|caesura"; //$NON-NLS-1$
		punct_quotes = "'‘’’"; //$NON-NLS-1$
		punct_strong1 = ".!?"; //$NON-NLS-1$
		punct_strong2 = "\\.\\.|\\.\\.\\.|…|\\|"; //$NON-NLS-1$
		punct_paren_open1 = "«"; //$NON-NLS-1$
		punct_paren_open2 = "``|\\(|\\[|\\{"; //$NON-NLS-1$
		punct_paren_close1 = "»"; //$NON-NLS-1$
		punct_paren_close2 = "''|‘‘|’’|\\)|\\]|\\}"; //$NON-NLS-1$
		punct_weak = "\\-,;∼ˆ·*:\"“”\\+±=/\\≤≥<>\\—ν√μ•@≈→αγ∞≡σ&%|#¼½Θĩ†φθΓ§ẽə∈"; //$NON-NLS-1$
		entity = "&[^;]+;"; //$NON-NLS-1$
		seg_tags = "seg"; //$NON-NLS-1$

		enclitics = "je|m[eo]i|tu|t[eo]i|il|lui|luy|ilz|ils|no?u?s|vo?u?s|on|ce|ci|là|elles?"; // FRO //$NON-NLS-1$
		encliticsFR = "je|tu|il|elle|on|nous|vous|ils|elles|toi|moi|en|y|t|leur|lui|le|la|les"; // FR //$NON-NLS-1$

		/** The TT enclitics. */
		FClitic_en = "['‘’](s|re|ve|d|m|em|ll)|n['‘’]t"; //$NON-NLS-1$
		PClitic_fr = "[dcjlmnstyDCJLNMSTY]['‘’]|[Qq]u['‘’]|[Jj]usqu['‘’]|[Ll]orsqu['‘’]|[Pp]uisqu['‘’]|[Qq]uoiqu['‘’]"; //$NON-NLS-1$
		FClitic_fr = "-t-elles?|-t-ils?|-t-on|-ce|-elles?|-ils?|-je|-la|-les?|-leur|-lui|-mêmes?|-m['‘’]|-moi|-nous|-on|-toi|-tu|-t['‘’]|-vous|-en|-y|-ci|-là"; //$NON-NLS-1$
		PClitic_it = "[dD][ae]ll['‘’]|[nN]ell['‘’]|[Aa]ll['‘’]|[lLDd]['‘’]|[Ss]ull['‘’]|[Qq]uest['‘’]|[Uu]n['‘’]|[Ss]enz['‘’]|[Tt]utt['‘’]"; //$NON-NLS-1$
		FClitic_gl = "-la|-las|-lo|-los|-nos"; //$NON-NLS-1$

		whitespaces = "[\\p{Z}\\p{C}]+"; //$NON-NLS-1$
		regElision = "['‘’]"; //$NON-NLS-1$
		regPunct = "[\\p{Ps}\\p{Pe}\\p{Pi}\\p{Pf}\\p{Po}\\p{S}]"; //$NON-NLS-1$

		recombine();
	}

	public void recombine() {

		if (corr_tags == null) {
			corr_tags = "" + corr_tags_no_seg + "|" + seg_tags + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (extraword_tags == null) {
			extraword_tags = "" + div_tags + "|" + q_tags + "|" + extraword1_tags + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		if (punct_strong == null) {
			punct_strong = "[" + punct_strong1 + "]|" + punct_strong2 + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (punct_paren_open == null) {
			punct_paren_open = "" + punct_paren_open1 + "|" + punct_paren_open2 + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (punct_paren_close == null) {
			punct_paren_close = "" + punct_paren_close1 + "|" + punct_paren_close2 + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (punct_paren == null) {
			punct_paren = "" + punct_paren_open + "|" + punct_paren_close + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (punct_all == null) {
			punct_all = "" + punct_strong + "|" + punct_paren + "|[" + punct_weak + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		if (word_chars == null) {
			word_chars = "[^ " + punct_quotes + "" + punct_strong1 + "" + punct_paren_open1 + "" + punct_paren_close1 + "" + punct_weak + "]+|" + entity + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
		}
	}

	/**
	 * To dom.
	 *
	 * @param doc the doc
	 * @param parent the parent
	 */
	@Deprecated
	public void toDom(Document doc, Node parent) {

		Element tokenizer = doc.createElement("tokenizer"); //$NON-NLS-1$
		tokenizer.setAttribute("onlyThoseTests", "false"); //$NON-NLS-1$ //$NON-NLS-2$
		parent.appendChild(tokenizer);

		// String tag_all = "<[^>]+>";
		Element p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "tag_all"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(tag_all);
		tokenizer.appendChild(p);
		// String enclitics = "je|m[eo]i|tu|t[eo]i|il|lui|luy|ilz|ils|no?u?s|vo?u?s|on|ce|ci|là|elles?"; // FRO
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "enclitics"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(enclitics);
		tokenizer.appendChild(p);
		// String encliticsFR = "je|tu|il|elle|on|nous|vous|ils|elles|toi|moi|en|y|t|leur|lui|le|la|les"; // FR
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "encliticsFR"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(encliticsFR);
		tokenizer.appendChild(p);
		// String div_tags = "TEI|text|front|body|div|div1|div2|div3|div4|div5|div6|back|head|trailer|p|ab|sp|speaker|list|notice|bibl";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "div_tags"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(div_tags);
		tokenizer.appendChild(p);
		// String q_tags = "q|quote|item|stage";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "q_tags"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(q_tags);
		tokenizer.appendChild(p);
		// String extraword_tags = "$div_tags|$q_tags|expan|pb|lb|milestone|gap|note|s|locus|title|ref|hi|witDetail";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "extraword_tags"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(extraword_tags);
		tokenizer.appendChild(p);
		// String corr_tags_no_seg = "expan|unclear|choice|corr|sic|reg|orig|foreign|hi|title|name|supplied|subst|add|del|damage|date|idno";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "enclitics"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(enclitics);
		tokenizer.appendChild(p);
		// String corr_tags = "$corr_tags_no_seg|seg";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "corr_tags"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(corr_tags);
		tokenizer.appendChild(p);
		// String word_tags = "w|abbr|num";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "word_tags"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(word_tags);
		tokenizer.appendChild(p);
		// String intraword_tags = "c|ex";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "intraword_tags"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(intraword_tags);
		tokenizer.appendChild(p);

		// String punct_strong1 = ".!?";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_strong1"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_strong1);
		tokenizer.appendChild(p);
		// String punct_strong2 = "\\.\\.|\\.\\.\\.|…|\\|";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_strong2"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_strong2);
		tokenizer.appendChild(p);
		// String punct_strong = "[$punct_strong1]|$punct_strong2";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_strong"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_strong);
		tokenizer.appendChild(p);
		// String punct_paren_open1 = "«";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_paren_open1"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_paren_open1);
		tokenizer.appendChild(p);
		// String punct_paren_open2 = "<<|``|\\(|\\[|\\{";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_paren_open2"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_paren_open2);
		tokenizer.appendChild(p);
		// String punct_paren_open = "$punct_paren_open1|$punct_paren_open2";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_paren_open"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_paren_open);
		tokenizer.appendChild(p);
		// String punct_paren_close1 = "»";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_paren_close1"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_paren_close1);
		tokenizer.appendChild(p);
		// String punct_paren_close2 = ">>|''|‘‘|’’|\\)|\\]|\\}";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_paren_close2"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_paren_close2);
		tokenizer.appendChild(p);
		// String punct_paren_close = "$punct_paren_close1|$punct_paren_close2";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_paren_close"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_paren_close);
		tokenizer.appendChild(p);
		// String punct_paren = "$punct_paren_open|$punct_paren_close";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_paren"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_paren);
		tokenizer.appendChild(p);
		// String punct_weak = ",;∼ˆ·*:\"`'“”\\+±=/\\-≤≥<>\\—_ν√μ•@≈→αγ∞≡σ&¼½Θĩ†φθΓ§ẽə∈";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_weak"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_weak);
		tokenizer.appendChild(p);
		// String punct_all = "$punct_strong|$punct_paren|[$punct_weak]";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "punct_all"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(punct_all);
		tokenizer.appendChild(p);
		// String word_chars = "[^ $punct_strong1$punct_paren_open1$punct_paren_close1$punct_weak]|&[^;]+;";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "word_chars"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(word_chars);
		tokenizer.appendChild(p);
		// whitespaces = "[\\p{Z}\\p{C}]+";
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "whitespaces"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(whitespaces);
		tokenizer.appendChild(p);
		// regElision = "['‘’]"
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "regElision"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(regElision);
		tokenizer.appendChild(p);
		// regPunct = "[\\p{Ps}\\p{Pe}\\p{Pi}\\p{Pf}\\p{Po}\\p{S}]"
		p = doc.createElement("param"); //$NON-NLS-1$
		p.setAttribute("key", "regPunct"); //$NON-NLS-1$ //$NON-NLS-2$
		p.setTextContent(regPunct);
		tokenizer.appendChild(p);
	}

	/**
	 * Dump.
	 */
	public void dump() {
		System.out.println("BRUT"); //$NON-NLS-1$
		System.out.println("tag_all = " + tag_all + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("enclitics = " + enclitics + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("encliticsFR = " + encliticsFR + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("div_tags = " + div_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("q_tags = " + q_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("extraword1_tags = " + extraword1_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("corr_tags_no_seg = " + corr_tags_no_seg + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("word_tags = " + word_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("intraword_tags = " + intraword_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_quotes = " + punct_quotes + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_strong1 = " + punct_strong1 + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_strong2 = " + punct_strong2 + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_paren_open1 = " + punct_paren_open1 + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_paren_open2 = " + punct_paren_open2 + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_paren_close1 = " + punct_paren_close1 + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_paren_close2 = " + punct_paren_close2 + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_weak = " + punct_weak + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("entity = " + entity + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("seg_tags = " + seg_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("COMBINED"); //$NON-NLS-1$
		System.out.println("corr_tags = " + corr_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("extraword_tags = " + extraword_tags + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_strong = " + punct_strong + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_paren_open = " + punct_paren_open + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_paren_close = " + punct_paren_close + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_paren = " + punct_paren + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("punct_all = " + punct_all + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("word_chars = " + word_chars + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("whitespaces = " + whitespaces + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("regElision = " + regElision + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("regPunct = " + regPunct + ""); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("TESTS:"); //$NON-NLS-1$

		for (TTest test : tests) {
			System.out.println(" " + test + ""); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	public boolean isValid() {
		try {
			Pattern.compile(tag_all);
		}
		catch (Exception e) {
			System.out.println("tag_all=" + tag_all + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(enclitics);
		}
		catch (Exception e) {
			System.out.println("enclitics=" + enclitics + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(encliticsFR);
		}
		catch (Exception e) {
			System.out.println("encliticsFR=" + encliticsFR + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(div_tags);
		}
		catch (Exception e) {
			System.out.println("div_tags=" + div_tags + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(q_tags);
		}
		catch (Exception e) {
			System.out.println("q_tags=" + q_tags + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(extraword1_tags);
		}
		catch (Exception e) {
			System.out.println("extraword1_tags=" + extraword1_tags + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(corr_tags_no_seg);
		}
		catch (Exception e) {
			System.out.println("corr_tags_no_seg=" + corr_tags_no_seg + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(word_tags);
		}
		catch (Exception e) {
			System.out.println("word_tags=" + word_tags + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(intraword_tags);
		}
		catch (Exception e) {
			System.out.println("intraword_tags=" + intraword_tags + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_quotes);
		}
		catch (Exception e) {
			System.out.println("punct_quotes=" + punct_quotes + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_strong1);
		}
		catch (Exception e) {
			System.out.println("punct_strong1=" + punct_strong1 + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_strong2);
		}
		catch (Exception e) {
			System.out.println("punct_strong2=" + punct_strong2 + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_paren_open1);
		}
		catch (Exception e) {
			System.out.println("punct_paren_open1=" + punct_paren_open1 + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_paren_open2);
		}
		catch (Exception e) {
			System.out.println("punct_paren_open2=" + punct_paren_open2 + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_paren_close1);
		}
		catch (Exception e) {
			System.out.println("punct_paren_close1=" + punct_paren_close1 + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_paren_close2);
		}
		catch (Exception e) {
			System.out.println("punct_paren_close2=" + punct_paren_close2 + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(punct_weak);
		}
		catch (Exception e) {
			System.out.println("punct_weak=" + punct_weak + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(entity);
		}
		catch (Exception e) {
			System.out.println("entity=" + entity + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(seg_tags);
		}
		catch (Exception e) {
			System.out.println("seg_tags=" + seg_tags + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
			return false;
		}

		try {
			Pattern.compile(corr_tags);
		}
		catch (Exception e) {
			System.out.println("corr_tags: " + e); //$NON-NLS-1$
			return false;
		}

		try {
			Pattern.compile(extraword_tags);
		}
		catch (Exception e) {
			System.out.println("extraword_tags: " + e); //$NON-NLS-1$
			return false;
		}

		if (punct_strong != null)
			try {
				Pattern.compile(punct_strong);
			}
			catch (Exception e) {
				System.out.println("punct_strong: " + e); //$NON-NLS-1$
				return false;
			}

		try {
			Pattern.compile(punct_paren_open);
		}
		catch (Exception e) {
			System.out.println("punct_paren_open: " + e); //$NON-NLS-1$
			return false;
		}

		try {
			Pattern.compile(punct_paren_close);
		}
		catch (Exception e) {
			System.out.println("punct_paren_close: " + e); //$NON-NLS-1$
			return false;
		}

		try {
			Pattern.compile(punct_paren);
		}
		catch (Exception e) {
			System.out.println("punct_paren: " + e); //$NON-NLS-1$
			return false;
		}

		try {
			Pattern.compile(punct_all);
		}
		catch (Exception e) {
			System.out.println("punct_all: " + e); //$NON-NLS-1$
			return false;
		}

		try {
			Pattern.compile(word_chars);
		}
		catch (Exception e) {
			System.out.println("word_chars: " + e); //$NON-NLS-1$
			return false;
		}

		if (regPunct != null)
			try {
				Pattern.compile(regPunct);
			}
			catch (Exception e) {
				System.out.println("regPunct=" + regPunct + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
				return false;
			}

		if (regElision != null)
			try {
				Pattern.compile(regElision);
			}
			catch (Exception e) {
				System.out.println("regElision=" + regElision + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
				return false;
			}

		if (whitespaces != null)
			try {
				Pattern.compile(whitespaces);
			}
			catch (Exception e) {
				System.out.println("whitespaces=" + whitespaces + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
				return false;
			}

		for (TTest test : tests) {
			try {
				Pattern.compile(test.getRegex());
			}
			catch (Exception e) {
				System.out.println("" + test.getRegex() + ": " + e); //$NON-NLS-1$ //$NON-NLS-2$
				return false;
			}
		}
		return true;
	}

	/**
	 * old way used in TXM 0.7.9 AND in corpus 0.7.9 -> 0.8.0 restoration
	 * 
	 * @param tokenizerElement
	 * @return
	 */
	@Deprecated
	public boolean loadFromNode(Element tokenizerElement) {
		// load params
		String tmp_strong_punct = null;
		NodeList params = tokenizerElement.getElementsByTagName("param"); //$NON-NLS-1$
		for (int i = 0; i < params.getLength(); i++) {
			Element param = (Element) params.item(i);
			String key = param.getAttribute("name"); //$NON-NLS-1$
			String value = param.getAttribute("value"); //$NON-NLS-1$
			if (value == null || value.length() == 0) value = param.getTextContent();
			if (value.length() == 0) value = null;

			if (debug) System.out.println(" Tokenizer parametrized with " + key + "=" + value + ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			if (key.equals("tag_all")) //$NON-NLS-1$
				tag_all = value;
			else if (key.equals("enclitics")) //$NON-NLS-1$
				enclitics = value;
			else if (key.equals("encliticsFR")) //$NON-NLS-1$
				encliticsFR = value;
			else if (key.equals("div_tags")) //$NON-NLS-1$
				div_tags = value;
			else if (key.equals("q_tags")) //$NON-NLS-1$
				q_tags = value;
			else if (key.equals("extraword1_tags")) //$NON-NLS-1$
				extraword1_tags = value;
			else if (key.equals("corr_tags_no_seg")) //$NON-NLS-1$
				corr_tags_no_seg = value;
			else if (key.equals("word_tags")) //$NON-NLS-1$
				word_tags = value;
			else if (key.equals("intraword_tags")) //$NON-NLS-1$
				intraword_tags = value;
			else if (key.equals("punct_quotes")) //$NON-NLS-1$
				punct_quotes = value;
			else if (key.equals("punct_strong1")) //$NON-NLS-1$
				punct_strong1 = value;
			else if (key.equals("punct_strong2")) //$NON-NLS-1$
				punct_strong2 = value;
			else if (key.equals("punct_paren_open1")) //$NON-NLS-1$
				punct_paren_open1 = value;
			else if (key.equals("punct_paren_open2")) //$NON-NLS-1$
				punct_paren_open2 = value;
			else if (key.equals("punct_paren_close1")) //$NON-NLS-1$
				punct_paren_close1 = value;
			else if (key.equals("punct_paren_close2")) //$NON-NLS-1$
				punct_paren_close2 = value;
			else if (key.equals("punct_weak")) //$NON-NLS-1$
				punct_weak = value;
			else if (key.equals("entity")) //$NON-NLS-1$
				entity = value;
			else if (key.equals("seg_tags")) //$NON-NLS-1$
				seg_tags = value;
			else if (key.equals("regPunct")) //$NON-NLS-1$
				regPunct = value;
			else if (key.equals("regElision")) //$NON-NLS-1$
				regElision = value;
			else if (key.equals("whitespaces")) //$NON-NLS-1$
				whitespaces = value;
			else if (key.equals("punct_strong")) // this is temporary //$NON-NLS-1$
				tmp_strong_punct = value; // this is temporary
			else
				System.out.println("MISSING TOKENIZER KEY: " + key); //$NON-NLS-1$
		}
		// recombine
		recombine();
		if (tmp_strong_punct != null) punct_strong = tmp_strong_punct;  // this is temporary

		String shouldResetTests = tokenizerElement.getAttribute("onlyThoseTests"); //$NON-NLS-1$
		if ("true".equals(shouldResetTests)) { //$NON-NLS-1$
			System.out.println("Warning: tokenizer only using import parameters tests"); //$NON-NLS-1$
			// tests = new ArrayList<>();
		}

		NodeList testsList = tokenizerElement.getElementsByTagName("test"); //$NON-NLS-1$
		if (testsList.getLength() > 0) {
			// System.out.println("Add "+testsList.getLength()+" tests to the tokenizer"
			for (int i = 0; i < testsList.getLength(); i++) {
				Element test = (Element) testsList.item(i);
				// tests.add(i, new TTest(test.getTextContent(), test.getAttribute("type"),
				// Integer.parseInt(test.getAttribute("before")),
				// Integer.parseInt(test.getAttribute("hit")),
				// Integer.parseInt(test.getAttribute("after")))
				// );
			}
			// System.out.println("Tests: "+tests);
		}

		if (isValid())
			return true;
		else {
			reset();
			recombine();
			return false;
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		// Document doc = DomUtils.load(new File("/home/mdecorde/xml/discours/import.xml"));
		// Node corpora = doc.getDocumentElement().getElementsByTagName("corpora").item(0);
		// Node corpus = ((Element)corpora).getElementsByTagName("corpus").item(0);
		// Node tokenizer = ((Element)corpus).getElementsByTagName("tokenizer").item(0);
		// System.out.println("OK? "+ TokenizerClasses.loadFromNode((Element) tokenizer));
	}

	public boolean loadFromProject(ProjectScope projectScope) {
		// load params
		IEclipsePreferences params = projectScope.getNode("Tokenizer"); //$NON-NLS-1$
		try {
			if (debug) System.out.println(Arrays.toString(params.keys()));
			if (debug) System.out.println(Arrays.toString(params.childrenNames()));
		}
		catch (BackingStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

			Field[] fields = TokenizerClasses.class.getFields();
			for (Field field : fields) {
				int m = field.getModifiers();
				if (!Modifier.isStatic(m) && Modifier.isPublic(m) && field.getType().equals(String.class)) {
					String name = field.getName();
					String defaultValue = null;
					try {
						if (field.get(this) != null) {
							defaultValue = field.get(this).toString();
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					String value = params.get(name, defaultValue);
					if (value != null) {
						try {
							if (debug) System.out.println(" Tokenizer parametrized with " + name + "=" + value); //$NON-NLS-1$ //$NON-NLS-2$
							field.set(this, value);
						}
						catch (Exception e) {
							Log.printStackTrace(e);
						}
					}
				}
			}

			// recombine
			recombine();

			if (params.getBoolean("onlyThoseTests", false)) { //$NON-NLS-1$
				System.out.println("Warning: tokenizer only using import parameters tests"); //$NON-NLS-1$
				// tests = new ArrayList<>();
			}

			org.osgi.service.prefs.Preferences testsList = params.node("tests"); //$NON-NLS-1$
			String[] tests;
			tests = testsList.childrenNames();
			// System.out.println("Add "+testsList.getLength()+" tests to the tokenizer"
			for (String testname : tests) {
				org.osgi.service.prefs.Preferences testdef = testsList.node(testname);

				// Element test = (Element) testsList.item(i);
				TTest t = new TTest(testdef.get("content", null), testdef.get("type", null), //$NON-NLS-1$ //$NON-NLS-2$
						testdef.getInt("before", 0), //$NON-NLS-1$
						testdef.getInt("hit", 0), //$NON-NLS-1$
						testdef.getInt("after", 0)); //$NON-NLS-1$
			}
			// System.out.println("Tests: "+tests);

		}
		catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (isValid()) {
			return true;
		}
		else {
			reset();
			recombine();
			return false;
		}
	}
}
