// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

package org.txm.tokenizer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tokenizes a String using a bunch of regular expressions
 * 
 * String tokenisation depends on the language
 */
public class SimpleStringTokenizer implements StringTokenizer {

	/** The punct_strong. */
	String punct_strong;

	/** The punct_all. */
	String punct_all;

	/** The word_chars. */
	String word_chars;

	String fclitics = null; // default behavior don't manage clitics

	String pclitics = null; // default behavior don't manage clitics

	Pattern reg_punct_other = Pattern.compile("\\p{P}"); //$NON-NLS-1$

	/** The DEBUG. */
	public boolean DEBUG = false;

	String lang;

	Pattern regElision = null;

	Pattern reg3pts = null;

	Pattern regPunct;

	Pattern regFClitics = null;

	Pattern regPClitics = null;

	String whitespaces;

	Pattern regWhitespaces;

	Pattern regSplitWhiteSpaces;

	TokenizerClasses tc;

	/**
	 * Instantiates a new simple string tokenizer.
	 *
	 * @param lang
	 */
	public SimpleStringTokenizer(String lang) {
		this(new TokenizerClasses(lang));
	}

	public boolean doSentences() {
		return false;
	}

	/**
	 * Instantiates a new simple string tokenizer.
	 *
	 * @param lang
	 */
	public SimpleStringTokenizer(TokenizerClasses tc) {
		this.tc = tc;
		this.lang = tc.lang;
		if (lang != null)
			if (lang.startsWith("en")) { //$NON-NLS-1$
				fclitics = tc.FClitic_en;
			}
			else if (lang.startsWith("fr")) { //$NON-NLS-1$
				fclitics = tc.FClitic_fr;
				pclitics = tc.PClitic_fr;
			}
			else if (lang.startsWith("gl")) { //$NON-NLS-1$
				fclitics = tc.FClitic_gl;
			}
			else if (lang.startsWith("it")) { //$NON-NLS-1$
				pclitics = tc.PClitic_it;
			}

		punct_strong = tc.punct_strong;
		punct_all = tc.punct_all;
		word_chars = tc.word_chars;

		String strRegElision = tc.regElision;
		if (strRegElision != null && strRegElision.length() > 0) {
			regElision = Pattern.compile("^([\\p{L}-]++" + strRegElision + "[\\p{L}-]++)(.*)$");
		}
		reg3pts = Pattern.compile("^(.*?)(\\.\\.\\.)(.*)$");

		String strRegPunct = tc.regPunct;
		if (strRegPunct != null && strRegPunct.length() > 0) {
			regPunct = Pattern.compile("^(.*?)(" + strRegPunct + ")(.*)$");
		}
		if (fclitics != null && fclitics.length() > 0) {
			regFClitics = Pattern.compile("(.+)(" + fclitics + ")$"); // the test must end with the end of string "$" FIXME or with "(.*)$" ??
		}
		if (pclitics != null && pclitics.length() > 0)
			regPClitics = Pattern.compile("^(" + pclitics + ")(.*)"); // the test must start with the start of string  ^

		if (tc.whitespaces != null && tc.whitespaces.length() > 0) {
			regWhitespaces = Pattern.compile(tc.whitespaces); // MD ???????? whats the diff with regSplitWhiteSpaces ?
		}

		if (tc.whitespaces != null && tc.whitespaces.length() > 0) {
			regSplitWhiteSpaces = Pattern.compile(tc.whitespaces);
		}
	}

	public final static String WHITESPACE = " "; //$NON-NLS-1$

	public final static String EMPTY = ""; //$NON-NLS-1$

	/**
	 * Process word.
	 */
	public List<List<String>> processText(String text) {

		List<List<String>> result = new ArrayList<List<String>>();
		ArrayList<String> sresult = new ArrayList<String>();
		if (regSplitWhiteSpaces != null) {
			for (String s : regSplitWhiteSpaces.split(text)) {		// separate with unicode white spaces
				if (DEBUG) {
					System.out.println("process $s"); //$NON-NLS-1$
				}
				sresult.addAll(iterate(s));
			}
		}
		else {
			sresult.addAll(iterate(text));
		}
		result.add(sresult);
		return result;
	}

	/**
	 * Iterate. a String, should be called when a word is found in a String
	 *
	 * @param s the s
	 * @return the java.lang. object
	 */
	protected ArrayList<String> iterate(String s) {
		ArrayList<String> result = new ArrayList<String>();
		while (s != null && s.length() > 0) {
			if (DEBUG) {
				System.out.println("  > $s"); //$NON-NLS-1$
			}
			s = standardChecks(result, s);
		}
		return result;
	}

	/**
	 * Standard checks.
	 *
	 * @param s the s
	 * @return the java.lang. object
	 */
	//	@CompileStatic(SKIP)
	public String standardChecks(ArrayList<String> result, String s) {
		Matcher m;

		for (TTest test : tc.tests) {

			m = Pattern.compile(test.regex).matcher(s);

			if (m.find()) {
				if (DEBUG) {
					System.out.println("test : " + test.regex); //$NON-NLS-1$
				}

				if (test.before > 0) {
					result.addAll(iterate(m.group(test.before)));
				}
				if (test.hit > 0) {
					result.add(m.group(test.hit));
				}
				if (test.after > 0) {
					return m.group(test.after);
				}
			}
		}
		;
		if (fclitics != null && (m = regFClitics.matcher(s)).find()) {
			if (DEBUG) System.out.println("CLITIC found: $s ->" + m); //$NON-NLS-1$
			result.addAll(iterate(m.group(1)));

			result.add(m.group(2));

			return ""; //$NON-NLS-1$
		}
		else if (pclitics != null && (m = regPClitics.matcher(s)).find()) {
			if (DEBUG) System.out.println("PCLITIC found: $s ->" + m); //$NON-NLS-1$

			result.add(m.group(1));

			result.addAll(iterate(m.group(2)));

			return ""; //$NON-NLS-1$
		}
		else if (regElision != null && (m = regElision.matcher(s)).find()) {
			if (DEBUG) System.out.println("Elision found: $s ->" + m.group(1) + " + " + m.group(2) + " + " + m.group(3)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			//iterate(m.group(1))

			//			int sep = s.indexOf("'");
			//			if (sep < 0)
			//				sep = s.indexOf("’");
			//			if (sep < 0)
			//				sep = s.indexOf("‘");

			result.add(m.group(1));

			result.addAll(iterate(m.group(2)));

			return ""; //$NON-NLS-1$
		}
		else if (reg3pts != null && (m = reg3pts.matcher(s)).find()) {
			if (DEBUG) {
				System.out.println("REG '...' found: $s -> " + m.group(1) + " + " + m.group(2) + " + " + m.group(3)); //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
			}   
			result.addAll(iterate(m.group(1)));

			result.add("..."); //$NON-NLS-1$

			return m.group(3);
		}
		else if (regPunct != null && (m = regPunct.matcher(s)).find()) {
			if (DEBUG) {
				System.out.println("PUNCT '$regPunct' found: $s ->" + m.group(1) + " + " + m.group(2) + " + " + m.group(3)); //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
			}   
			result.addAll(iterate(m.group(1)));

			result.add(m.group(2));

			return m.group(3);
		}
		else {
			//		if(DEBUG){System.out.println("Other found: "+s}
			result.add(s);

			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		ArrayList<List<String>> tests = new ArrayList<List<String>>();
		tests.add(Arrays.asList("fr", "c'est un test.")); //$NON-NLS-1$ //$NON-NLS-2$
		tests.add(Arrays.asList("fr", "C'est un autre test.")); //$NON-NLS-1$ //$NON-NLS-2$
		tests.add(Arrays.asList("fr", "C'est une version 1.2.3 un 01:12:12 test vers http://un.site.web.fr, fin.")); //$NON-NLS-1$
		tests.add(Arrays.asList("en", "This is a test.")); //$NON-NLS-1$ //$NON-NLS-2$
		tests.add(Arrays.asList("en", "It's a test.")); //$NON-NLS-1$ //$NON-NLS-2$

		for (List<String> d : tests) {
			String lang = d.get(0);
			String text = d.get(1);
			SimpleStringTokenizer tokenizer = new SimpleStringTokenizer(lang);
			System.out.println("Process: $text"); //$NON-NLS-1$
			System.out.println("Result : " + tokenizer.processText(text)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
	}
}
