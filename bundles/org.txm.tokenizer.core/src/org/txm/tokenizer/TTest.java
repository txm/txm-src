package org.txm.tokenizer;

public class TTest {

	String regex;

	String type = "w"; //$NON-NLS-1$

	int before = 1, hit = 2, after = 3;

	public TTest(String regex, String type, int before, int hit, int after) {
		this.regex = regex;
		this.type = type;
		this.before = before;
		this.hit = hit;
		this.after = after;
	}

	public String getRegex() {
		return regex;
	}

	public TTest(String regex, int before, int hit, int after) {
		this.regex = regex;
		this.before = before;
		this.hit = hit;
		this.after = after;
	}

	public TTest(String regex, String type) {
		this.regex = regex;
		this.type = type;
	}

	public TTest(String regex) {
		this.regex = regex;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[regex=" + regex + ", type=" + type + ", before=" + before + ", hit=" + hit + ", after=" + after + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
	}
}

