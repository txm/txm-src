package org.txm.libs.cqp;

import org.txm.PostInstallationStep;

public class ___DoInstallStep extends PostInstallationStep {
	
	protected String name = "corg.txm.libs.cqp.DoInstallStep"; //$NON-NLS-1$
	
	public ___DoInstallStep() {
		name = "CQP";
	}
	
	@Override
	public void install() {
		// nothing to do
	}
	
	@Override
	public void preInstall() {
		
		// String CQPFRAGMENT = "org.txm.libs.cqp."+System.getProperty("osgi.os");
		// //TODO: fix preferneces logic between org.txm.cqp.lib et org.txm.searchengine.cqp
		// Log.info("CQP.DoInstallStep.preInstall()");
		// String saved = CQPLibPreferences.getInstance().getString(CQPLibPreferences.VERSION);
		// Version currentVersion = BundleUtils.getBundleVersion(CQPFRAGMENT); // the CQPFRAGMENT plugin contains the right version
		//
		// if (saved != null && saved.length() > 0) {
		// Version savedVersion = new Version(saved);
		// if (currentVersion.compareTo(savedVersion) <= 0) {
		// Log.info("No post-installation of CQP to do");
		// return; // nothing to do
		// }
		// }
		// Log.info("Updating CWB preferences for CQP version="+currentVersion);
		//
		// String os = "win";
		// String ext = "";
		// if (Util.isWindows()) { //$NON-NLS-1$
		// if (System.getProperty("os.arch").contains("64")) { os = "win64"; }
		// else { os = "win32"; }
		// ext = ".exe";
		// } else if (Util.isMac()) { //$NON-NLS-1$
		// os = "macosx";
		// } else {
		// os = "linux32";
		// if (System.getProperty("os.arch").contains("64"))
		// os = "linux64";
		// }
		//
		// File bundleDir = BundleUtils.getBundleFile(CQPFRAGMENT);
		// if (bundleDir == null) {
		// System.out.println("Error while retrieving TreeTaggerInstaller bundle directory.");
		// return;
		// }
		// File cwbDir = new File(bundleDir, "res");
		// File initFile = new File(cwbDir, "cqpserver.init");
		// File OSDir = new File(cwbDir, os);
		//
		// int n;
		// try { // extract all gz files
		// n = Utils.unGzipFilesInDirectory(OSDir);
		//
		// if (n == 0) {
		// System.out.println("Error while extracting CQP files: no file extracted.");
		// return;
		// }
		// } catch (IOException e1) {
		// System.out.println("Error while extracting CQP files: "+e1);
		// return;
		// }
		//
		// File execFile = new File(OSDir, "cqpserver"+ext);
		//
		// CQPLibPreferences.getInstance().put(CQPLibPreferences.CQI_SERVER_PATH_TO_CQPLIB, OSDir.getAbsolutePath());
		// CQPLibPreferences.getInstance().put(CQPLibPreferences.CQI_SERVER_PATH_TO_INIT_FILE, initFile.getAbsolutePath());
		// CQPLibPreferences.getInstance().put(CQPLibPreferences.CQI_SERVER_PATH_TO_EXECUTABLE, execFile.getAbsolutePath());
		// if (!Util.isWindows()) {
		// try {
		// Log.info("Setting execution file rights to: "+OSDir.getAbsolutePath());
		// Runtime.getRuntime().exec("chmod -R +x "+OSDir.getAbsolutePath()).waitFor();
		// } catch (Exception e) {
		// System.out.println("Error while setting execution file rights to: "+OSDir.getAbsolutePath());
		// e.printStackTrace();
		// return;
		// }
		// }
		//
		// if (!execFile.exists()) {
		// System.out.println("Cannot find CQP executable file: "+execFile);
		// return;
		// }
		//
		// if (!execFile.canExecute()) {
		// System.out.println("File right setting error, cannot execute: "+execFile);
		// return;
		// }
		//
		// Log.warning("SearchEngine preferences set with: "+OSDir.getAbsolutePath()+ ", "+initFile.getAbsolutePath()+ " and "+execFile.getAbsolutePath());
		// CQPLibPreferences.getInstance().put(CQPLibPreferences.VERSION, currentVersion.toString());
	}
}
