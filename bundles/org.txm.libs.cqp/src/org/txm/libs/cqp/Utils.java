package org.txm.libs.cqp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;

public class Utils {
	
	/**
	 * Ungzip an input file into an output file.
	 * <p>
	 * The output file is created in the output folder, having the same name
	 * as the input file, minus the '.gz' extension.
	 * 
	 * @param inputFile the input .gz file
	 * @param outputDir the output directory file.
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 * @return The @see java.io.File with the ungzipped content.
	 */
	public static File unGzip(final File inputFile, final File outputDir) throws FileNotFoundException, IOException {
		
		final File outputFile = new File(outputDir, inputFile.getName().substring(0, inputFile.getName().length() - 3));
		
		final GZIPInputStream in = new GZIPInputStream(new FileInputStream(inputFile));
		final FileOutputStream out = new FileOutputStream(outputFile);
		
		IOUtils.copy(in, out);
		
		in.close();
		out.close();
		
		return outputFile;
	}
	
	/**
	 * Extract GZ files stored in a directory
	 * 
	 * @param inputDir
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @return the number of extracted files
	 */
	public static int unGzipFilesInDirectory(final File inputDir) throws FileNotFoundException, IOException {
		File[] gzFiles = inputDir.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".gz");
			}
		});
		if (gzFiles == null) return 0;
		if (gzFiles.length == 0) return 0;
		
		int n = 0;
		for (File f : gzFiles) {
			File resultFile = unGzip(f, inputDir);
			if (resultFile != null && resultFile.exists()) n++;
		}
		return n;
	}
	
	public static void main(String[] args) {
		try {
			System.out.println("n=" + unGzipFilesInDirectory(new File("/usr/lib/TXM/TXM/plugins/CQP.linux_1.1.0.201702231552/res/linux64")));
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

