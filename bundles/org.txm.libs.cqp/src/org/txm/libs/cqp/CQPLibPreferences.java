package org.txm.libs.cqp;

import java.io.File;
import java.io.IOException;

import org.osgi.framework.Version;
import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.logger.Log;

public class CQPLibPreferences extends TXMPreferences {
	
	
	/** The Constant CQI_SERVER_PATH_TO_EXECUTABLE. */
	public static final String VERSION = "version"; //$NON-NLS-1$
	
	
	/** The Constant CQI_SERVER_PATH_TO_EXECUTABLE. */
	public static final String CQI_SERVER_PATH_TO_EXECUTABLE = "cqi_server_path_to_executable"; //$NON-NLS-1$
	
	public static final String CQI_SERVER_PATH_TO_CQPLIB = "cqi_server_path_to_cqplib"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_PATH_TO_INIT_FILE. */
	public static final String CQI_SERVER_PATH_TO_INIT_FILE = "cqi_server_path_to_init_file"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_HOST. */
	public static final String CQI_SERVER_HOST = "cqi_server_host"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_PORT. */
	public static final String CQI_SERVER_PORT = "cqi_server_port"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_LOGIN. */
	public static final String CQI_SERVER_LOGIN = "cqi_server_login"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_PASSWORD. */
	public static final String CQI_SERVER_PASSWORD = "cqi_server_password"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_ADDITIONAL_OPTIONS. */
	public static final String CQI_SERVER_ADDITIONAL_OPTIONS = "cqi_server_additional_options"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_MONITOR_OUTPUT. */
	public static final String CQI_SERVER_MONITOR_OUTPUT = "cqi_server_monitor_output"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_IS_REMOTE. */
	public static final String CQI_SERVER_IS_REMOTE = "cqi_server_is_remote"; //$NON-NLS-1$
	
	/** The Constant CQI_SERVER_MODE. */
	public static final String CQI_NETWORK_MODE = "cqi_server_mode"; //$NON-NLS-1$
	
	public static final String QUERY_FIX_TOKENIZE = "query_fix_tokenize_string"; //$NON-NLS-1$



	/**
	 * The global CQP matching strategy or the local query matching strategy
	 */
	//TODO: SJ: move this key to searchengine core level
	public static final String MATCHINGSTRATEGY = "MatchingStrategy";
	
	/**
	 * The global CQP option to set the property which don't needs brackets
	 */
	public static final String DEFAULTNONBRACKATTR = "DefaultNonbrackAttr";
	
	/**
	 * The global CQP option to set strict regions mode
	 */
	public static final String STRICTREGIONS = "StrictRegions";
	
	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		
		// FIXME: SJ: some code in this method should be done only at TXM first run
		// Extra note: and be better to use P2.inf and install/uninstall Touchpoints
		
		Preferences preferences = this.getDefaultPreferencesNode();
		
		String CQPFRAGMENT = "org.txm.libs.cqp." + System.getProperty("osgi.os");
		// TODO: fix preferences logic between org.txm.cqp.lib and org.txm.searchengine.cqp
		Log.fine("CQPLibPreferences.initializeDefaultPreferences()");
		String saved = getString(CQPLibPreferences.VERSION);
		Version currentVersion = BundleUtils.getBundleVersion(CQPFRAGMENT); // the CQPFRAGMENT plugin contains the right version
		
		String os = "win";
		String ext = "";
		String osname = System.getProperty("os.name").toLowerCase();
		if (osname.contains("windows")) { //$NON-NLS-1$
			if (System.getProperty("os.arch").contains("64")) {
				os = "win64"; //$NON-NLS-1$
			}
			else {
				os = "win32"; //$NON-NLS-1$
			}
			ext = ".exe"; //$NON-NLS-1$
		}
		else if (osname.contains("mac")) { //$NON-NLS-1$
			os = "macosx"; //$NON-NLS-1$
		}
		else {
			os = "linux32"; //$NON-NLS-1$
			if (System.getProperty("os.arch").contains("64"))
				os = "linux64"; //$NON-NLS-1$
		}
		
		File bundleDir = BundleUtils.getBundleFile(CQPFRAGMENT);
		if (bundleDir == null) {
			Log.severe("Error while retrieving " + CQPFRAGMENT + " bundle directory.");
			return;
		}
		File cwbDir = new File(bundleDir, "res"); //$NON-NLS-1$
		File initFile = new File(cwbDir, "cqpserver.init"); //$NON-NLS-1$
		File OSDir = new File(cwbDir, os);
		try {
			OSDir = OSDir.getCanonicalFile(); // get the resolved path
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		String[] filenames = { "cqp", "cqpserver", "cwb-align", "cwb-align-encode", "cwb-decode", "cwb-encode", "cwb-makeall", "libcqpjni.so" };
		File execFile = new File(OSDir, "cqpserver" + ext);
		if (!execFile.canExecute()) { // file rights need to be set
			Log.fine("Setting execution file rights to: " + OSDir.getAbsolutePath() + " files...");
			// int ret = Runtime.getRuntime().exec("chmod -R +x '"+OSDir.getAbsolutePath()+"'").waitFor();
			for (String filename : filenames) {
				new File(OSDir, filename).setExecutable(true);
			}
		}
		
		if (!execFile.exists()) {
			System.out.println("Cannot find CQP executable file: " + execFile);
			return;
		}
		
		if (!execFile.canExecute()) {
			System.out.println("File right setting error, cannot execute: " + execFile);
			return;
		}
		
		if (!osname.contains("windows")) {
			try {
				Log.fine("Setting execution file rights to: " + OSDir.getAbsolutePath());
				Runtime.getRuntime().exec("chmod -R +x " + OSDir.getAbsolutePath()).waitFor();
			}
			catch (Exception e) {
				System.out.println("Error while setting execution file rights to: " + OSDir.getAbsolutePath());
				e.printStackTrace();
				return;
			}
		}
		
		preferences.put(CQI_SERVER_PATH_TO_CQPLIB, OSDir.getAbsolutePath());
		preferences.put(CQI_SERVER_PATH_TO_INIT_FILE, initFile.getAbsolutePath());
		preferences.put(CQI_SERVER_PATH_TO_EXECUTABLE, execFile.getAbsolutePath());
		preferences.put(CQI_SERVER_ADDITIONAL_OPTIONS, "-b 1000000"); //$NON-NLS-1$
		preferences.put(CQI_SERVER_PORT, "4877"); //$NON-NLS-1$
		preferences.put(CQI_SERVER_HOST, "localhost"); //$NON-NLS-1$
		preferences.put(CQI_SERVER_LOGIN, "anonymous"); //$NON-NLS-1$
		preferences.put(CQI_SERVER_PASSWORD, ""); //$NON-NLS-1$
		preferences.putBoolean(CQI_SERVER_IS_REMOTE, true);
		preferences.putBoolean(CQI_NETWORK_MODE, false);
		preferences.putBoolean(QUERY_FIX_TOKENIZE, true);
		
		preferences.put(MATCHINGSTRATEGY, "standard");
//		preferences.put(DEFAULTNONBRACKATTR, "word");
//		preferences.put(STRICTREGIONS, "yes");
		
		if (saved != null && saved.length() > 0) {
			Version savedVersion = new Version(saved);
			if (currentVersion.compareTo(savedVersion) <= 0) {
				Log.fine("No post-installation of CQP to do");
				return; // nothing to do
			}
		}
		Log.fine("Updating CWB preferences for CQP version=" + currentVersion);
		
		Log.fine("SearchEngine preferences set with: " + OSDir.getAbsolutePath() + ", " + initFile.getAbsolutePath() + " and " + execFile.getAbsolutePath());
		put(CQPLibPreferences.VERSION, currentVersion.toString());
		
		flush();
	}
	
	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(CQPLibPreferences.class)) {
			new CQPLibPreferences();
		}
		return TXMPreferences.instances.get(CQPLibPreferences.class);
	}
}
