package org.txm.tigersearch.rcp.preferences;

import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * TIGERSearch preference page
 * @author mdecorde
 *
 */
public class TreeSearchPreferencePage extends TXMPreferencePage {

	@Override
	public void createFieldEditors() {
//		this.addField(new ComboFieldEditor(TIGERSearchPreferences.METHOD, "Method", methods, chartsTab));
//		this.addField(new BooleanFieldEditor(TIGERSearchPreferences.GROUP_BY_TEXTS, "Group by words", chartsTab));
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(TreeSearchPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setDescription("Syntax Tree");
		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/functions/Tree.png"));
	}
}