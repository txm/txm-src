package org.txm.tigersearch.rcp.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 *
 */
public class TreeSearchPreferences extends TXMPreferences {
	
	
	public static String VERSION = "version";
	
	public static String USESUBMATCHES = "use_sub_matches";
	
	public static String UDPREFIX = "ud_prefix";
	
	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(TreeSearchPreferences.class)) {
			new TreeSearchPreferences();
		}
		return TXMPreferences.instances.get(TreeSearchPreferences.class);
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putBoolean(USESUBMATCHES, true);
		preferences.put(UDPREFIX, "ud-");
		// preferences.putBoolean(GROUP_BY_TEXTS, true);
		// preferences.putInt(METHOD, 2);
	}
}


