package org.txm.treesearch.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 *
 */
public class TreeSearchPreferences extends TXMPreferences {

	public static final String VERSION = "version"; //$NON-NLS-1$

	public static final String DEFAULT_VISUALISATION = "default_representation"; //$NON-NLS-1$

	public static final String TFEATURE = "t"; //$NON-NLS-1$

	public static final String NTFEATURE = "nt"; //$NON-NLS-1$

	public static final String VISUALISATION = "visualisation"; //$NON-NLS-1$

	public static String BACKTOTREE_POSITION = "linked_editor_position"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(TreeSearchPreferences.class)) {
			new TreeSearchPreferences();
		}
		return TXMPreferences.instances.get(TreeSearchPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(DEFAULT_VISUALISATION, "TIGER"); //$NON-NLS-1$
		preferences.put(TFEATURE, "word"); //$NON-NLS-1$
		preferences.put(NTFEATURE, "cat"); //$NON-NLS-1$
		preferences.put(VISUALISATION, ""); // no supplementary visualisation //$NON-NLS-1$
		preferences.putInt(BACKTOTREE_POSITION, 0);
	}
}
