package org.txm.treesearch.function;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.treesearch.preferences.TreeSearchPreferences;

public abstract class TreeSearch extends TXMResult {

	protected CQPCorpus corpus;

	/**
	 * from 0 to N-1 sentences
	 */
	@Parameter(key = TXMPreferences.INDEX, type = Parameter.RENDERING)
	protected int pIndex;

	/**
	 * from 0 to N-1 sentence subgraphs
	 */
	@Parameter(key = TXMPreferences.SUB_INDEX, type = Parameter.RENDERING)
	protected int pSubIndex;

	@Parameter(key = TXMPreferences.QUERY)
	protected IQuery pQuery;

	@Parameter(key = TreeSearchPreferences.TFEATURE)
	protected List<String> T;

	@Parameter(key = TreeSearchPreferences.NTFEATURE)
	protected List<String> NT;

	@Parameter(key = TreeSearchPreferences.VISUALISATION)
	protected String pVisualisation;


	public TreeSearch(CQPCorpus corpus) {

		this(null, corpus);
	}

	public TreeSearch(String parentNodePath) {

		this(parentNodePath, null);
	}

	public TreeSearch(String parentNodePath, CQPCorpus corpus) {

		super(parentNodePath, corpus);
	}

	@Override
	public boolean canCompute() {

		return corpus != null; // && pQuery != null && pQuery.length() > 0; // if pQuery is empty/null set a default query
	}

	public abstract String[] getAvailableNTProperties();

	public abstract String[] getAvailableTProperties();

	public CQPCorpus getCorpus() {

		return corpus;
	}

	@Override
	public String getDetails() {

		if (pQuery != null) {
			return pQuery.toString();
		}
		return Messages.noQuery;
	}

	@Override
	public String getName() {

		if (pQuery != null) {
			return pQuery.toString();
		}
		return Messages.noQuery;
	}

	public abstract int getNMatchingSentences();

	public abstract int getNSubMatch();

	public abstract int getTotalSubMatch();

	public final List<String> getNT() {
		return NT;
	}

	public abstract String getQuery();

	@Override
	public String getSimpleName() {

		if (pQuery != null) {
			return pQuery.toString();
		}
		return Messages.noQuery;
	}

	public String getComputingStartMessage() {

		return NLS.bind(Messages.renderingP0SyntacticTree, this.getSelector().getEngine());
	}

	public String getComputingDoneMessage() {

		int n = this.getNMatchingSentences();

		if (n > 0) {
			return NLS.bind(Messages.P0matchingSentenceP1, n, n > 1 ? "s" : ""); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			return Messages.noMatchingSentence;
		}
	}



	public final List<String> getT() {
		return T;
	}

	@Override
	public final boolean loadParameters() throws Exception {

		this.T = new ArrayList<>();
		String values = getStringParameterValue(TreeSearchPreferences.TFEATURE);

		if (values != null && !values.isEmpty() && values.length() > 0) {
			this.T.addAll(Arrays.asList(values.split(","))); //$NON-NLS-1$
		}

		this.NT = new ArrayList<>();
		values = getStringParameterValue(TreeSearchPreferences.TFEATURE);

		if (values != null && !values.isEmpty() && values.length() > 0) {
			this.NT.addAll(Arrays.asList(values.split(","))); //$NON-NLS-1$
		}

		String squery = getStringParameterValue(TreeSearchPreferences.QUERY);
		if (squery != null && !squery.isEmpty() && squery.length() > 0) {
			this.pQuery = Query.stringToQuery(this.getStringParameterValue(TXMPreferences.QUERY));
		}

		return _loadParameters();
	}

	@Override
	public boolean saveParameters() throws Exception {

		if (pQuery != null) {
			this.saveParameter(TreeSearchPreferences.QUERY, Query.queryToString(pQuery));
		}

		this.saveParameter(TreeSearchPreferences.TFEATURE, StringUtils.join(this.T, ",")); //$NON-NLS-1$

		this.saveParameter(TreeSearchPreferences.NTFEATURE, StringUtils.join(this.NT, ",")); //$NON-NLS-1$

		return true;
	}

	public abstract boolean _loadParameters() throws Exception;

	public abstract boolean hasSubMatchesStrategy();

	public abstract boolean toSVG(File svgFile, int sent, int sub, List<String> T, List<String> nT2);

	public abstract boolean isReady();

	public abstract void setIndexAndSubIndex(int sent, int sub);

	public abstract boolean isComputed();

	public abstract boolean canBeDrawn();

	public abstract String getEngine();

	public abstract TreeSearchSelector getSelector();

	public abstract String[] getTextAndWordIDSOfCurrentSentence() throws Exception;

	public abstract String getIconPath();

	public final int getCurrentMatchIndex() {

		return pIndex;
	}

	public final int getCurrentSubMatchIndex() {

		return this.pSubIndex;
	}

	public void setQuery(IQuery query) {
		this.pQuery = query;
	}

	public abstract Selection getSelection();

	public abstract File createTemporaryFile(File resultDir);

	public abstract String[] getAvailableVisualisations();

	public String getVisualisation() {
		return pVisualisation;
	}

}
