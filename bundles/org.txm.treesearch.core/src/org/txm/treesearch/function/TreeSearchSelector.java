package org.txm.treesearch.function;

import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.corpus.CQPCorpus;

public abstract class TreeSearchSelector {

	public TreeSearchSelector() {

	}

	public abstract String getEngine();

	public abstract boolean canComputeWith(CQPCorpus corpus);

	public abstract TreeSearch getTreeSearch(CQPCorpus corpus);

	public abstract TreeSearch getTreeSearch(String resultnodepath, CQPCorpus corpus);

	public abstract Class<? extends Query> getQueryClass();
}
