package org.txm.treesearch.function;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = Messages.class.getPackage().getName() + ".messages"; //$NON-NLS-1$

	public static String noMatchingSentence;

	public static String noQuery;

	public static String P0matchingSentenceP1;

	public static String renderingP0SyntacticTree;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
