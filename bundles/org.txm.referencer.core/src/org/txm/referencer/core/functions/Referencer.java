// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.

//
// This file is part of the TXM platform.
//
// The TXM platform is free software: you can redistribute it and/or modif y
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The TXM platform is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
// 
// 
// 
// $LastChangedDate: 2016-11-29 16:47:07 +0100 (Tue, 29 Nov 2016) $
// $LastChangedRevision: 3349 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.referencer.core.functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.objects.Match;
import org.txm.referencer.core.messages.ReferencerCoreMessages;
import org.txm.referencer.core.preferences.ReferencerPreferences;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
//import org.txm.searchengine.cqp.corpus.query.Match;
//import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

/**
 * The Class Referencer computes the references of a query.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class Referencer extends TXMResult {

	/** The noref. */
	protected static int noref = 1;

	/** The prefix r. */
	protected static String prefixR = "Referencer"; //$NON-NLS-1$

	/** The indexes. */
	int[] indexes;

	/** The lines. */
	List<Line> lines = new ArrayList<Line>();

	/** The matches. */
	List<? extends Match> matches;

	/** The positions. */
	int[] positions;

	/** The nlines. */
	int nlines;

	/** The processed idx. */
	HashMap<Integer, Line> processedIdx;

	/** The result. */
	Selection result;

	/** The symbol. */
	private String symbol;

	/** The writer. */
	private BufferedWriter writer;

	/** The hierarchic sort. */
	@Parameter(key = ReferencerPreferences.SORT_BY_FREQUENCIES)
	protected boolean pHierarchicSort;

	@Parameter(key = ReferencerPreferences.PATTERN)
	protected List<StructuralUnitProperty> pPattern;

	/** The prop. */
	@Parameter(key = ReferencerPreferences.UNIT_PROPERTY)
	protected Property pProperty;

	/** The query. */
	@Parameter(key = ReferencerPreferences.QUERY)
	protected IQuery pQuery;

	/** The show counts boolean. */
	@Parameter(key = ReferencerPreferences.SHOW_COUNTS)
	protected Boolean pShowCounts;

	/** The show counts boolean. */
	@Parameter(key = ReferencerPreferences.HIERARCHIC_MODE)
	private boolean pHierarchicMode;

	/**
	 * Creates a not computed referencer.
	 *
	 * @param parent the parent corpus
	 */
	public Referencer(CQPCorpus parent) {
		super(parent);
	}


	/**
	 * Creates a not computed referencer.
	 * 
	 * @param parametersNodePath
	 */
	public Referencer(String parametersNodePath) {
		super(parametersNodePath);
	}


	/**
	 * As r matrix.
	 *
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 */
	public String asRMatrix() throws RWorkspaceException {

		String keywords[] = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++)
			keywords[i] = lines.get(i).getPropValue();
		// System.out.println(Arrays.toString(keywords));

		String symbol = prefixR + noref;

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();
		rw.eval(symbol + " <- list()"); //$NON-NLS-1$
		rw.addVectorToWorkspace("refkeywords", keywords); //$NON-NLS-1$
		rw.eval(symbol + "$keywords <- refkeywords"); //$NON-NLS-1$
		// rw.eval(symbol+"$query <- \""+this.query.getQueryString()+"\")");
		rw.eval(symbol + "$refs <- list()"); //$NON-NLS-1$
		int i = 1;
		for (Line line : this.lines) {
			String refs[] = line.getReferences().toArray(new String[line.getReferences().size()]);
			// System.out.println(line.getPropValue()+": "+Arrays.toString(refs));
			rw.addVectorToWorkspace("reflinerefs", refs); //$NON-NLS-1$
			rw.eval(symbol + "$refs[[" + i + "]] <- reflinerefs"); //$NON-NLS-1$ //$NON-NLS-2$
			i++;
		}

		noref++;
		this.symbol = symbol;
		return symbol;
	}

	@Override
	public void clean() {
		try {
			if (this.writer != null) {
				this.writer.flush();
				this.writer.close();
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Clears all lines.
	 */
	public void clearAllLines() {
		for (Line line : lines) { // we need to recompute lines references
			line.clear();
		}
	}

	/**
	 * Calls in order all stages to compute the referencer.
	 *
	 * @return true, if successful
	 */
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) {
		try {
			if (!getQueryMatches()) {
				return false;
			}

			if (!getQueryindexes()) {
				return false;
			}

			if (!groupPositionsbyId()) {
				return false;
			}
		}
		catch (Exception e) {
			Log.severe(NLS.bind(ReferencerCoreMessages.ErrorP0, e));
			Log.printStackTrace(e);
			return false;
		}

		return true;
	}

	/**
	 * Gets the parent corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return (CQPCorpus) this.parent;
	}

	@Override
	public String getDetails() {
		return getCorpus().getName();
	}

	/**
	 * Gets the lines.
	 *
	 * @return the lines
	 * @throws Exception
	 */
	public List<Line> getLines() throws Exception {
		return getLines(0, lines.size());
	}

	/**
	 * Gets the lines.
	 *
	 * @param from the from
	 * @param to the to
	 * @return the lines
	 * @throws Exception
	 */
	public List<Line> getLines(int from, int to) throws Exception {
		if (from < 0) {
			from = 0;
		}
		if (to > lines.size()) {
			to = lines.size();
		}

		HashMap<Integer, Line> lineswithoutpropvalue = new HashMap<>();

		AbstractCqiClient cqiClient = CorpusManager.getCorpusManager().getCqiClient();
		// build Missing lines
		for (int i = from; i < to; i++) { // for each position group
			Line l = lines.get(i);
			if (l.notcomputed) {
				lineswithoutpropvalue.put(l.id, l);

				// get all reference values of all position of the line
				Map<Property, List<List<String>>> refValues = new HashMap<>();
				for (Property property : pPattern) {
					refValues.put(property, cqiClient.getData(property,
							l.positions, Collections.nCopies(l.positions.size(), 1)));
				}
				// build the Reference object
				for (int j = 0; j < l.positions.size(); j++) {
					String propvalue = ""; //$NON-NLS-1$
					for (Property property : pPattern) {
						propvalue += refValues.get(property).get(j).get(0) + ", "; //$NON-NLS-1$
					}

					l.addReference(propvalue.substring(0, propvalue.length() - 2));
				}
				if (pHierarchicSort) {
					l.sortRef(true);
				}
				else {
					l.sortAlpha();
				}
				l.notcomputed = false;
			}
		}

		// get idx String value
		int[] idxwithoutvalues = new int[lineswithoutpropvalue.keySet().size()];
		int count = 0;
		for (int i : lineswithoutpropvalue.keySet()) {
			idxwithoutvalues[count++] = i;
		}

		String[] values = pProperty.id2Str(idxwithoutvalues);
		for (int i = 0; i < values.length; i++) {
			int id = idxwithoutvalues[i];
			lineswithoutpropvalue.get(id).linepropvalue = values[i];
		}
		return lines.subList(from, to);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Override
	public String getName() {
		try {
			return getCorpus().getName() + TXMPreferences.PARENT_NAME_SEPARATOR + this.getSimpleName();
		}
		catch (Exception e) {
			return this.getEmptyName();
		}
	}


	@Override
	public String getSimpleName() {
		if (this.pQuery != null && !this.pQuery.isEmpty()) {
			return this.pQuery.asString() + ", " + StructuralUnitProperty.asFullNameString(this.pPattern); //$NON-NLS-1$
		}
		else {
			return this.getEmptyName();
		}
	}


	@Override
	public String getComputingStartMessage() {
		if (pQuery != null) {
			return TXMCoreMessages.bind(ReferencerCoreMessages.referencesOfP0InTheP1Corpus, this.pQuery.asString(), this.getCorpus().getName());
		}
		return super.getComputingStartMessage();
	}


	@Override
	public String getComputingDoneMessage() {
		if (this.lines == null || this.lines.isEmpty()) {
			return ""; //$NON-NLS-1$
		}
		else {
			return TXMCoreMessages.bind("", TXMCoreMessages.formatNumber(this.getNLines()), TXMCoreMessages.formatNumber(this.getV())); //$NON-NLS-1$
		}
	}


	/**
	 * Gets the n lines.
	 *
	 * @return the n lines
	 */
	public int getNLines() {
		return nlines;
	}

	/**
	 * Gets the pattern.
	 *
	 * @return the pattern
	 */
	public List<StructuralUnitProperty> getPattern() {
		return pPattern;
	}

	/**
	 * Gets the property.
	 *
	 * @return the property
	 */
	public Property getProperty() {
		return pProperty;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query
	 */
	public IQuery getQuery() {
		return pQuery;
	}

	/**
	 * 2nd step.
	 *
	 * @return the queryindexes
	 */
	public boolean getQueryindexes() {
		try {
			positions = new int[matches.size()];
			int i = 0;
			for (Match m : matches) {
				positions[i++] = m.getStart();
			}

			indexes = pProperty.cpos2Id(positions);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * 1st step.
	 *
	 * @return the query matches
	 * @throws Exception
	 */
	public boolean getQueryMatches() throws Exception {

		result = pQuery.getSearchEngine().query(this.getCorpus(), pQuery, "TMP", false); //$NON-NLS-1$

		//		result = this.getCorpus().query(pQuery, ReferencerCoreMessages.RESULT_TYPE, false);
		if (result.getNMatch() == 0) {
			return false;
		}
		matches = result.getMatches();
		result.drop();
		return true;
	}

	/**
	 * Gets the symbol.
	 *
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	public int getV() {
		if (matches == null) {
			return 0;
		}
		return matches.size();
	}

	/**
	 * 3rd stage.
	 *
	 * @return true, if successful
	 */
	public boolean groupPositionsbyId() {
		processedIdx = new HashMap<>();

		lines = new ArrayList<>();
		// System.out.println("indexes ");
		for (int i = 0; i < indexes.length; i++) {
			int id = indexes[i];
			// System.out.print(" "+id);
			if (!processedIdx.containsKey(id)) {
				// System.out.println("add idx "+id);
				Line l = new Line(id);
				processedIdx.put(id, l);

				lines.add(l);
			}
			processedIdx.get(id).addPosition(positions[i]);
		}

		nlines = processedIdx.keySet().size();
		return true;
	}

	public void setParameters(IQuery query, WordProperty prop, List<StructuralUnitProperty> pattern, Boolean hierarchicSort) {
		if (query != null) this.pQuery = query;
		if (pattern != null) this.pPattern = pattern;
		if (prop != null) this.pProperty = prop;
		if (hierarchicSort != null) this.pHierarchicSort = hierarchicSort;
		this.setDirty();
	}

	@Override
	@Deprecated
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) {
		try {
			// NK: writer declared as class attribute to perform a clean if the operation is interrupted
			this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outfile), encoding));
			// if ("UTF-8".equals(encoding)) writer.write('\ufeff'); // UTF-8 BOM
			toTxt(writer, 0, lines.size() - 1, colseparator, txtseparator);
		}
		catch (Exception e) {
			Log.severe(TXMCoreMessages.bind(ReferencerCoreMessages.referencerExportFailedP0, Log.toString(e)));
			return false;
		}
		return true;
	}

	@Deprecated
	public void toTxt(Writer writer, int from, int to, String colseparator, String txtseparator)
			throws Exception {
		try {
			getLines(0, Integer.MAX_VALUE);
			writer.write(pProperty.getName() + colseparator + this.pPattern + "\n"); //$NON-NLS-1$
			for (Line line : lines) {
				writer.write(txtseparator + line.getPropValue().replace(txtseparator, txtseparator + txtseparator) + txtseparator + colseparator + txtseparator + line.getReferences().toString()
						.replace(txtseparator, txtseparator + txtseparator) + txtseparator + "\n"); //$NON-NLS-1$
				writer.flush();
			}

			writer.close();
		}
		catch (CqiServerError e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	@Override
	public boolean canCompute() {

		if (pQuery == null || pQuery.isEmpty()) {
			Log.fine("No query set."); //$NON-NLS-1$
			return false;
		}
		if (pPattern == null || pPattern.size() == 0) {
			Log.fine("No reference pattern set."); //$NON-NLS-1$
			return false;
		}
		if (pProperty == null) {
			Log.fine("No property set."); //$NON-NLS-1$
			return false;
		}

		return true;
	}

	@Override
	public boolean loadParameters() throws CqiClientException {

		this.pQuery = Query.stringToQuery(this.getStringParameterValue(TXMPreferences.QUERY));
		this.pPattern = StructuralUnitProperty.stringToProperties(this.getCorpus(), this.getStringParameterValue(ReferencerPreferences.PATTERN));
		this.pProperty = this.getCorpus().getProperty(this.getStringParameterValue(ReferencerPreferences.UNIT_PROPERTY));
		return true;
	}

	@Override
	public boolean saveParameters() {

		if (this.pQuery != null) {
			this.saveParameter(ReferencerPreferences.QUERY, Query.queryToString(pQuery));
		}

		if (this.pPattern != null) {
			this.saveParameter(ReferencerPreferences.PATTERN, StructuralUnitProperty.propertiesToString(this.pPattern));
		}

		if (this.pProperty != null) {
			this.saveParameter(ReferencerPreferences.UNIT_PROPERTY, this.pProperty.getName());
		}
		return true;
	}

	/**
	 * The Class Line.
	 */
	public class Line {

		/** The id. */
		int id;

		/** The propvalue. */
		String linepropvalue;

		/** The notcomputed. */
		boolean notcomputed = true;

		/** The positions. */
		ArrayList<Integer> positions;

		/** The references. */
		ArrayList<String> references;

		/** The references counts. */
		HashMap<String, Integer> referencesCounts = new HashMap<>();

		/**
		 * Instantiates a new line.
		 *
		 * @param i the i
		 */
		public Line(int i) {
			id = i;
			positions = new ArrayList<>();
			references = new ArrayList<>();
		}

		/**
		 * Adds the position.
		 *
		 * @param i the i
		 */
		public void addPosition(int i) {
			positions.add(i);
		}

		/**
		 * Adds the reference.
		 *
		 * @param string the string
		 */
		public void addReference(String string) {
			if (!referencesCounts.containsKey(string)) {
				references.add(string);
				referencesCounts.put(string, 1);
			}
			else {
				referencesCounts.put(string, referencesCounts.get(string) + 1);
			}
		}

		/**
		 * Clear.
		 */
		public void clear() {
			notcomputed = false;
			references = new ArrayList<>();
		}

		/**
		 * Gets the count.
		 *
		 * @param ref the ref
		 * @return the count
		 */
		public int getCount(String ref) {
			return referencesCounts.get(ref);
		}

		public HashMap<String, Integer> getCounts() {
			return referencesCounts;
		}

		/**
		 * Gets the prop value.
		 *
		 * @return the prop value
		 */
		public String getPropValue() {
			return linepropvalue;
		}

		/**
		 * Gets the references.
		 *
		 * @return the references
		 */
		public List<String> getReferences() {
			return references;
		}

		/**
		 * Sort alpha.
		 */
		public void sortAlpha() {
			Collections.sort(references);
		}

		/**
		 * Sort ref.
		 *
		 * @param reverse the reverse
		 */
		public void sortRef(final boolean reverse) {
			Collections.sort(references, new Comparator<String>() {

				@Override
				public int compare(String arg0, String arg1) {
					if (reverse)
						return getCount(arg1) - getCount(arg0);
					else
						return getCount(arg0) - getCount(arg1);
				}
			});
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return linepropvalue + " : <" + id + "," + pProperty + "> : " + references.toString(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
	}

	public void setQuery(Query query) {
		this.pQuery = query;
	}

	public void setProperty(WordProperty property) {
		this.pProperty = property;
	}

	public void setPattern(List<StructuralUnitProperty> properties) {
		this.pPattern = properties;
	}


	/**
	 * Creates a CQL query string from the specified Referencer lines.
	 *
	 * @param lines
	 * @return the query
	 */
	public Query createQuery(List<Line> lines) {

		Property prop = this.getProperty();
		String query = "["; //$NON-NLS-1$

		ArrayList<String> values = new ArrayList<>();
		for (Line line : lines) {
			String s = line.getPropValue();
			values.add(s);
		}
		String test = ((WordProperty) prop).getCQLTest(values);
		if (test != null) {
			query += test;
		}

		query += "]"; //$NON-NLS-1$

		return new CQLQuery(query);

	}

	@Override
	public String getResultType() {
		return ReferencerCoreMessages.RESULT_TYPE;
	}

	public void setShowCounts(boolean showCounts) {

		pShowCounts = showCounts;
	}

	public boolean getShowCounts() {

		return pShowCounts;
	}


	public boolean getHierarchicMode() {

		return pHierarchicMode;
	}
}
