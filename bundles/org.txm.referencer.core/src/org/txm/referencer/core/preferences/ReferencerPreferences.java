package org.txm.referencer.core.preferences;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Default preferences.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ReferencerPreferences extends TXMPreferences {

	public static final String SORT_BY_FREQUENCIES = "sort_by_frequencies"; //$NON-NLS-1$

	public static final String PATTERN = "pattern"; //$NON-NLS-1$

	public static final String SHOW_COUNTS = "show_counts"; //$NON-NLS-1$

	public static final String HIERARCHIC_MODE = "hierarchic_mode"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(ReferencerPreferences.class)) {
			new ReferencerPreferences();
		}
		return TXMPreferences.instances.get(ReferencerPreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putBoolean(SORT_BY_FREQUENCIES, false);

		preferences.put(UNIT_PROPERTY, TXMPreferences.DEFAULT_UNIT_PROPERTY); //$NON-NLS-1$
		preferences.put(PATTERN, "text_id"); //$NON-NLS-1$
		preferences.putBoolean(SHOW_COUNTS, true); //$NON-NLS-1$
	}
}
