package org.txm.referencer.core.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Referencer core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class ReferencerCoreMessages extends NLS {


	private static final String BUNDLE_NAME = "org.txm.referencer.core.messages.messages"; //$NON-NLS-1$

	public static String ErrorP0;

	public static String RESULT_TYPE;

	public static String referencesOfP0InTheP1Corpus;

	public static String referencerExportFailedP0;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, ReferencerCoreMessages.class);
	}



}
