package org.txm.rcp.testers;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.core.results.TXMResult;
import org.txm.searchengine.cqp.corpus.MainCorpus;


public class IsCorpusDirty extends PropertyTester {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {

		if (receiver instanceof List) {
			List list = (List) receiver;
			if (list.size() == 0) return true;
			receiver = list.get(0);
		}

		if (receiver instanceof TXMResult) {
			receiver = ((TXMResult) receiver).getFirstParent(MainCorpus.class);
		}

		if (receiver instanceof Line) {
			receiver = ((Line) receiver).getConcordance().getFirstParent(MainCorpus.class);
		}

		if (receiver instanceof MainCorpus) {
			return KRAnnotationEngine.needToSaveAnnotations(((MainCorpus) receiver));
		}

		IEditorPart e = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (e instanceof ConcordanceEditor) {
			KRAnnotationEngine.needToSaveAnnotations(((ConcordanceEditor) e).getCorpus().getMainCorpus());
		}

		return false;
	}
}
