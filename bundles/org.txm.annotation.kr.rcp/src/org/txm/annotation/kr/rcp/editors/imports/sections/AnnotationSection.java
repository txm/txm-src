package org.txm.annotation.kr.rcp.editors.imports.sections;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.objects.Project;
import org.txm.rcp.editors.imports.ImportFormEditor;
import org.txm.rcp.editors.imports.sections.ImportEditorSection;

public class AnnotationSection extends ImportEditorSection {

	private static final int SECTION_SIZE = 1;

	public static final String DEFAULTNAMESUFFIX = "KR"; //$NON-NLS-1$

	Text nameText;

	public AnnotationSection(ImportFormEditor editor, FormToolkit toolkit2, ScrolledForm form2, Composite parent, int style) {
		super(editor, toolkit2, form2, parent, style, "annotation"); //$NON-NLS-1$

		this.section.setText(Messages.Annotation);
		TableWrapLayout layout = new TableWrapLayout();
		layout.makeColumnsEqualWidth = true;
		this.section.setLayout(layout);
		this.section.setLayoutData(getSectionGridData(SECTION_SIZE));
		this.section.setEnabled(false);

		this.section.addExpansionListener(new ExpansionAdapter() {

			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.layout(true);
			}
		});

		// filesection.setDescription("Select how to find source files");
		Composite sectionClient = toolkit.createComposite(this.section);
		TableWrapLayout slayout = new TableWrapLayout();
		slayout.makeColumnsEqualWidth = false;
		slayout.numColumns = 2;
		sectionClient.setLayout(slayout);
		this.section.setClient(sectionClient);

		Label l = toolkit.createLabel(sectionClient, Messages.KnowledgeRepositoryName, SWT.WRAP);
		l.setLayoutData(new TableWrapData(TableWrapData.LEFT, TableWrapData.MIDDLE));

		nameText = new Text(sectionClient, SWT.BORDER);
	}

	@Override
	public void updateFields(Project project) {
		if (this.section.isDisposed()) return;
		if (project == null) return;

		// List<String> values = KRAnnotationEngine.getKnowledgeRepositoryNames(project);
		//
		// String value = project.getName()+DEFAULTNAMESUFFIX;
		// if (values.size() > 0) {
		// value = values.get(0);
		// }
		// nameText.setText(value);
	}

	@Override
	public boolean saveFields(Project project) {
		if (!this.section.isDisposed()) {
			String value = nameText.getText();

			return true;
		}
		return false;
	}

	@Override
	public boolean checkFields() {
		return true;
	}

	@Override
	public int getSectionSize() {
		return SECTION_SIZE;
	}
}
