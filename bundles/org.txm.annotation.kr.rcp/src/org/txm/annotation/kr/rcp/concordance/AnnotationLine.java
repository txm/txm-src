package org.txm.annotation.kr.rcp.concordance;

import org.txm.annotation.kr.core.Annotation;
import org.txm.annotation.kr.core.repository.TypedValue;

/**
 * stores an annotation associated to a concordance line
 * 
 * @author mdecorde
 *
 */
public class AnnotationLine {

	private TypedValue annotationValue;

	private Annotation annotation;

	public AnnotationLine(Annotation annotation, TypedValue annotationValue) {
		this.annotation = annotation;
		this.annotationValue = annotationValue;
	}


	public Annotation getAnnotation() {
		return annotation;
	}


	public TypedValue getAnnotationValue() {
		return annotationValue;
	}
}
