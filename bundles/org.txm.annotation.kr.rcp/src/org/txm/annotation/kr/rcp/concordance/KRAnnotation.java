package org.txm.annotation.kr.rcp.concordance;

import java.io.File;
import java.util.List;
import java.util.Vector;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.ListDialog;
import org.txm.annotation.kr.core.AnnotationScope;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.repository.AnnotationEffect;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.SQLKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.commands.InitializeKnowledgeRepository;
import org.txm.annotation.kr.rcp.commands.krview.OpenKRView;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.annotation.kr.rcp.views.knowledgerepositories.KRView;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.results.TXMResult;
import org.txm.objects.Match;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.AsciiUtils;
import org.txm.utils.logger.Log;

/**
 * Type+Value annotation toolbar for the Concordance Editor
 * 
 * TODO: merge code with SimpleKRAnnotation and WordAnnotationToolbar
 * 
 * @author mdecorde
 *
 */
public class KRAnnotation extends ConcordanceBasedAnnotationArea {

	/**
	 * the limit number of annotation when a confirm dialog box is shown
	 */
	protected static final int NALERTAFFECTANNOTATIONS = 100;

	public static final String EMPTYTEXT = ""; //$NON-NLS-1$


	protected Point annotationSize;

	/** The annot btn : starts the annotation */
	private Button addAnnotationTypeLink;

	protected Text annotationValuesText;

	protected ComboViewer annotationTypesCombo;

	protected List<TypedValue> typeValuesList;

	protected Combo addRemoveCombo;

	protected Combo affectCombo;

	protected Button affectAnnotationButton;

	protected Button infosAnnotTypeSelectedLink;

	protected Vector<AnnotationType> typesList = new Vector<>();

	protected Button addTypedValueLink;

	protected Label withLabel;

	protected Label equalLabel;

	//protected TypedValue value_to_add;

	public KRAnnotation() {

	}

	@Override
	public String getName() {
		return Messages.squencesDeMotsCatgorievaleur;
	}

	@Override
	public boolean __install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception {

		// RESULT
		if (position < 0) {
			position = 3 + extension.getNumberOfAnnotationArea(); // after keyword column and previous annotation columns
		}

		annotationColumn.setText(Messages.category);

		editor.layout(true);

		return composeAnnotationArea(parent);
	}

	public boolean initialiseInput(ConcordanceEditor editor) throws Exception {

		this.editor = editor;
		concordance = editor.getResult();
		annotations = new ConcordanceAnnotations(concordance);
		annotations.computeLines(this.editor.getResult().getLines()); // pre-compute the annotation lines ot avoid breaking the sort and co.
		corpus = concordance.getCorpus();

		CQPCorpus corpus = editor.getCorpus();

		this.annotManager = KRAnnotationEngine.getAnnotationManager(corpus);

		return true;
	}

	private boolean composeAnnotationArea(final Composite parent) {

		List<KnowledgeRepository> krs = InitializeKnowledgeRepository.get(corpus.getMainCorpus());
		typesList.clear();
		Log.fine("Corpus KRs: " + krs); //$NON-NLS-1$

		for (KnowledgeRepository kr : krs) {
			if (kr == null) continue;

			currentKnowledgeRepository = kr;
			Log.fine(" KR: " + kr); //$NON-NLS-1$
			List<AnnotationType> krtypes = kr.getAllAnnotationTypes();
			for (AnnotationType type : krtypes) {
				if (type.getEffect().equals(AnnotationEffect.SEGMENT)) {
					typesList.add(type);
				}
			}

			Log.fine(NLS.bind(Messages.availableAnnotationTypesColonP0, typesList));
			break;
		}


		if (currentKnowledgeRepository == null) {
			Log.warning(Messages.errorColonNoSuitableKnowledgeRepositoryFound);
			return false;
		}

//		annotationArea = new GLComposite(parent, SWT.NONE, Messages.concordanceAnnotationArea);
		annotationArea.getLayout().numColumns = 12;
		annotationArea.getLayout().horizontalSpacing = 2;
		annotationArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		addRemoveCombo = new Combo(annotationArea, SWT.READ_ONLY);
		String affectLabel = currentKnowledgeRepository.getString(editor.getLocale(), "ConcordancesEditor_22"); //$NON-NLS-1$
		if (affectLabel == null) affectLabel = Messages.affect;
		String removeLabel = currentKnowledgeRepository.getString(editor.getLocale(), "ConcordancesEditor_24"); //$NON-NLS-1$
		if (removeLabel == null) removeLabel = Messages.delete;
		String items[] = { affectLabel, removeLabel };
		addRemoveCombo.setItems(items);
		addRemoveCombo.select(0);
		addRemoveCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (addRemoveCombo.getSelectionIndex() == 0) { // add
					annotationValuesText.setEnabled(true);
					String withLabelText = currentKnowledgeRepository.getString(editor.getLocale(), "ConcordancesEditor_83"); //$NON-NLS-1$
					if (withLabelText == null) withLabelText = Messages.withTheCategory;
					withLabel.setText(withLabelText);
					if (equalLabel != null) equalLabel.setText("="); //$NON-NLS-1$
				}
				else { // remove
					annotationValuesText.setEnabled(false);
					withLabel.setText(""); //$NON-NLS-1$
					if (equalLabel != null) equalLabel.setText(""); //$NON-NLS-1$
				}
				withLabel.redraw();
				annotationArea.layout();
				updateAnnotationWidgetStates();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		GridData gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		//gdata.widthHint = 90;
		addRemoveCombo.setLayoutData(gdata);

		withLabel = new Label(annotationArea, SWT.NONE);
		String withLabelText = currentKnowledgeRepository.getString(editor.getLocale(), "ConcordancesEditor_83"); //$NON-NLS-1$
		if (withLabelText == null) withLabelText = Messages.withTheCategory;
		withLabel.setText(withLabelText);
		withLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		equalLabel = new Label(annotationArea, SWT.NONE);
		equalLabel.setText("="); //$NON-NLS-1$
		equalLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		// Display combo with list of Annotation Type
		annotationTypesCombo = new ComboViewer(annotationArea, SWT.READ_ONLY);
		annotationTypesCombo.setContentProvider(new ArrayContentProvider());
		annotationTypesCombo.setLabelProvider(new SimpleLabelProvider() {

			@Override
			public String getColumnText(Object element, int columnIndex) {
				return ((AnnotationType) element).getName();
			}

			@Override
			public String getText(Object element) {
				return ((AnnotationType) element).getName();
			}
		});
		gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 200;

		annotationTypesCombo.getCombo().setLayoutData(gdata);
		annotationTypesCombo.setInput(typesList);
		if (typesList.size() > 0) {
			annotationTypesCombo.getCombo().select(0);
			annotations.setViewAnnotation(typesList.get(0));
			annotationColumn.setText(typesList.get(0).getId());
		}

		annotationTypesCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				AnnotationType type = getSelectedAnnotationType();
				if (type == null) return;
				if (type.getSize() != AnnotationType.ValuesSize.LARGE) {
					try {
						KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(type.getKnowledgeRepository());
						typeValuesList = kr.getValues(type);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				annotationColumn.setText(type.getName());
				if (concordance != null) {
					annotations.setViewAnnotation(type);
					annotations.setAnnotationOverlap(true);
					try {
						notifyStartOfRefresh();
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				updateAnnotationWidgetStates();
			}
		});

		if (currentKnowledgeRepository instanceof SQLKnowledgeRepository) {
			infosAnnotTypeSelectedLink = new Button(annotationArea, SWT.PUSH);
			infosAnnotTypeSelectedLink.setImage(IImageKeys.getImage(IImageKeys.ACTION_INFO));
			infosAnnotTypeSelectedLink.setEnabled(true);
			infosAnnotTypeSelectedLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			infosAnnotTypeSelectedLink.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					AnnotationType type = getSelectedAnnotationType();
					if (type == null) return;

					String typedValueURL = type.getURL(); // may be null/empty
					if (typedValueURL != null && typedValueURL.length() > 0) {
						KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(type.getKnowledgeRepository());
						kr.getTypeURL(type);
					}
					if (typedValueURL != null && typedValueURL.length() > 0) {
						OpenBrowser.openfile(typedValueURL, new File(typedValueURL).getName());
					}
					else {
						IWorkbenchWindow window = editor.getSite().getWorkbenchWindow();
						try {
							OpenKRView.openView(window, type);
						}
						catch (PartInitException e1) {
							e1.printStackTrace();
						}
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		if (currentKnowledgeRepository instanceof LocalKnowledgeRepository) {
			addAnnotationTypeLink = new Button(annotationArea, SWT.PUSH);
			addAnnotationTypeLink.setToolTipText(Messages.addANewCategory);
			addAnnotationTypeLink.setImage(IImageKeys.getImage(IImageKeys.ACTION_ADD));
			addAnnotationTypeLink.setEnabled(true);
			addAnnotationTypeLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			addAnnotationTypeLink.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (currentKnowledgeRepository == null) return;
					if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

					LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;

					InputDialog dialog = new InputDialog(e.widget.getDisplay().getActiveShell(), Messages.newCategory, Messages.pleaseEnterTheNewTypeName, "", null); //$NON-NLS-1$
					if (dialog.open() == InputDialog.OK) {
						String name = dialog.getValue();
						if (name.trim().length() == 0) return;
						String id = AsciiUtils.buildId(name);
						AnnotationType existingType = kr.getType(id);
						if (existingType != null) {
							Log.warning(NLS.bind(Messages.theP0KRAnnotationAlreadyExistingPleaseChooseAnotherP1, id, name)); 
							annotationTypesCombo.setSelection(new StructuredSelection(existingType));
							return;
						}
						AnnotationType type = kr.addType(name, id, ""); //$NON-NLS-1$
						type.setEffect(AnnotationEffect.SEGMENT);
						typesList.add(type);

						if (annotationTypesCombo != null) annotationTypesCombo.refresh();

						// if (typesList.size() == 1) {
						// if (annotationTypesCombo != null) annotationTypesCombo.getCombo().select(0);
						// if (concordance != null) {
						// annotations.setAnnotationOverlap(true);
						// try {
						// annotationColumn.setText(type.getId());
						// editor.refresh(false);
						// }
						// catch (Exception e1) {
						// // TODO Auto-generated catch block
						// e1.printStackTrace();
						// }
						// }
						// annotationArea.layout(true);
						// }
						// else {
						// if (typesList.size() > 1) {
						// if (annotationTypesCombo != null) {
						// annotationTypesCombo.getCombo().select(typesList.size() - 1);
						// }
						// }
						// }

						annotations.setViewAnnotation(type);
						annotationTypesCombo.setSelection(new StructuredSelection(typesList.get(typesList.size() - 1))); // select the new annotation
						annotationArea.layout(true);

						KRView.refresh();
						updateAnnotationWidgetStates();
					}
					else {
						Log.warning(Messages.annotationAbortedByUser);
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		Label valueLabel = new Label(annotationArea, SWT.NONE);
		String valueLabelText = currentKnowledgeRepository.getString(editor.getLocale(), "ConcordancesEditor_80"); //$NON-NLS-1$
		if (valueLabelText == null) {
			valueLabelText = Messages.valueEquals;
		}
		valueLabel.setText(valueLabelText);
		valueLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		annotationValuesText = new Text(annotationArea, SWT.BORDER);
		annotationValuesText.setToolTipText(Messages.enterAValueForAnId);
		GridData gdata2 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata2.widthHint = 200;
		annotationValuesText.setLayoutData(gdata2);
		annotationValuesText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					affectAnnotationToSelection(editor.getTableViewer().getSelection());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		if (currentKnowledgeRepository instanceof LocalKnowledgeRepository) {

			addTypedValueLink = new Button(annotationArea, SWT.PUSH);
			addTypedValueLink.setText("..."); //$NON-NLS-1$
			addTypedValueLink.setToolTipText(Messages.selectAValueAmongTheList);
			addTypedValueLink.setEnabled(true);
			addTypedValueLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			addTypedValueLink.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (currentKnowledgeRepository == null) return;
					if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

					AnnotationType type = getSelectedAnnotationType();
					if (type == null) return;

					LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;

					ListDialog dialog = new ListDialog(e.widget.getDisplay().getActiveShell());
					String title = currentKnowledgeRepository.getString(editor.getLocale(), "ConcordancesEditor_100"); //$NON-NLS-1$
					if (title == null) title = Messages.availableValuesForP0;
					dialog.setTitle(ConcordanceUIMessages.bind(title, type.getName()));
					dialog.setContentProvider(new ArrayContentProvider());
					dialog.setLabelProvider(new SimpleLabelProvider() {

						@Override
						public String getColumnText(Object element, int columnIndex) {
							if (element instanceof TypedValue)
								return ((TypedValue) element).getId();
							return element.toString();
						}
					});
					List<TypedValue> values;
					try {
						values = kr.getValues(type);
					}
					catch (Exception e1) {
						e1.printStackTrace();
						return;
					}
					dialog.setInput(values);
					if (dialog.open() == InputDialog.OK) {
						TypedValue value = (TypedValue) dialog.getResult()[0];
						String name = value.getId();
						if (name.trim().length() == 0) return;

						annotationValuesText.setText(name);
					}
					else {

					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		affectCombo = new Combo(annotationArea, SWT.READ_ONLY);
		affectCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		String items2[] = { Messages.selectedLines, Messages.allLines };
		affectCombo.setItems(items2);
		affectCombo.select(0);
		gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		//gdata.widthHint = 140;
		affectCombo.setLayoutData(gdata);

		affectAnnotationButton = new Button(annotationArea, SWT.PUSH);
		affectAnnotationButton.setText(Messages.oK);
		affectAnnotationButton.setToolTipText(Messages.proceedToAnnotation);
		affectAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		affectAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int isel = affectCombo.getSelectionIndex();
				final boolean doAffect = addRemoveCombo.getSelectionIndex() == 0; // add is default
				
				if (isel == 0) { // selected line
					if (doAffect) {
						affectAnnotationToSelection(editor.getTableViewer().getSelection());
					}
					else {
						deleteAnnotationValues(editor.getTableViewer().getSelection());
					}
				}
				else { // all
					try {
						List<? extends Match> matches = concordance.getMatches();
						if (doAffect) {
							affectAnnotationsToMatches(matches);
						}
						else {
							deleteAnnotationValues(matches);
						}
					}
					catch (Exception e1) {
						Log.severe(NLS.bind(Messages.errorWhileAnnotatingConcordanceColonP0, e1.getLocalizedMessage()));
						Log.printStackTrace(e1);
						return;
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button closeButton = new Button(annotationArea, SWT.PUSH);
		closeButton.setToolTipText(Messages.closeTheToolbarWithoutSaving);
		closeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
		closeButton.setLayoutData(new GridData(GridData.END, GridData.CENTER, true, false));
		closeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				extension.closeArea(KRAnnotation.this, true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		updateAnnotationWidgetStates();
		editor.layout(true);

		return true;
	}

	@Override
	protected AnnotationType getSelectedAnnotationType() {
		if (annotationTypesCombo.getCombo().isDisposed()) return null;

		IStructuredSelection isel = (IStructuredSelection) annotationTypesCombo.getSelection();
		if (isel.isEmpty()) {
			return null;
		}
		return (AnnotationType) isel.getFirstElement();
	}

	public void updateAnnotationWidgetStates() {

		if (annotationArea == null) return; // :)
		if (annotationArea.isDisposed()) return;

		annotationArea.getDisplay().syncExec(new Runnable() {

			@Override
			public void run() {
				if (addRemoveCombo.getSelectionIndex() == 0) { // add is default

					if (getSelectedAnnotationType() != null) {

						annotationValuesText.setEnabled(true);
						affectAnnotationButton.setEnabled(true);
						affectCombo.setEnabled(true);
					}
					else {

						annotationValuesText.setEnabled(false);
						affectAnnotationButton.setEnabled(false);
						affectCombo.setEnabled(false);
					}
				}
				else {
					annotationValuesText.setEnabled(false);

					if (getSelectedAnnotationType() != null) {
						affectAnnotationButton.setEnabled(true);
						affectCombo.setEnabled(true);
					}
					else {
						affectAnnotationButton.setEnabled(false);
						affectCombo.setEnabled(false);
					}
				}

				if (infosAnnotTypeSelectedLink != null) {
					infosAnnotTypeSelectedLink.setEnabled(getSelectedAnnotationType() != null);
				}
				if (addTypedValueLink != null) {
					addTypedValueLink.setEnabled(getSelectedAnnotationType() != null);
				}

				if (concordance == null) {
					affectAnnotationButton.setEnabled(false);
					affectCombo.setEnabled(false);
				}
			}
		});
	}

	@Override
	public boolean notifyStartOfCompute() throws Exception {
		if (annotationArea == null) return false;
		if (annotationArea.isDisposed()) return false;

		annotationArea.getDisplay().syncExec(new Runnable() {

			@Override
			public void run() {
				AnnotationType type = getSelectedAnnotationType();
				if (type != null) {
					annotations.setViewAnnotation(type);
					annotations.setAnnotationOverlap(true);
				}
			}
		});

		return true;
	}

	@Override
	public boolean notifyEndOfCompute() throws Exception {
		updateAnnotationWidgetStates();
		if (annotationArea == null) return false;
		if (annotationArea.isDisposed()) return false;

		// update annotation column width
		if (annotationArea != null) {
			annotationArea.getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					annotationColumn.pack();
					annotationColumn.setResizable(true);
				}
			});
		}
		else {
			annotationColumn.setWidth(0);
		}

		return true;
	}


	@Override
	protected void _close() {
		if (!annotationColumn.isDisposed()) annotationColumn.dispose();
		if (!annotationArea.isDisposed()) {
			annotationArea.dispose();
		}
		extension.layout();
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyStartOfRefresh() throws Exception {

		List<Line> lines = concordance.getLines(concordance.getTopIndex(), concordance.getTopIndex() + concordance.getNLinePerPage());
		annotations.computeLines(lines);

		updateAnnotationWidgetStates();

		// update annotation column width
		if (annotationArea != null) {
			//annotationColumn.pack();

			annotationColumn.setResizable(true);
		}
		else {
			annotationColumn.setWidth(0);
		}

		annotationColumnViewer.getViewer().refresh();
	}


	@Override
	public boolean discardChanges() {
		return false;
	}

	@Override
	protected String getSelectedAnnotationValue() {

		return annotationValuesText.getText();
	}

	@Override
	protected AnnotationScope getSelectedAnnotationScope() {

		return AnnotationScope.FIRSTTARGET;
	}


}
