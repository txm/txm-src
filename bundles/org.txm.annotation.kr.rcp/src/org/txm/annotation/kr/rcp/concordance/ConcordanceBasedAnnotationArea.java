package org.txm.annotation.kr.rcp.concordance;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.annotation.kr.core.Annotation;
import org.txm.annotation.kr.core.AnnotationManager;
import org.txm.annotation.kr.core.AnnotationScope;
import org.txm.annotation.kr.core.CQPAnnotation;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.KRBasedAnnotationArea;
import org.txm.annotation.kr.rcp.commands.SaveAnnotations;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.annotation.kr.rcp.views.knowledgerepositories.KRView;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.concordance.rcp.editors.ConcordanceEditor.ConcordanceColumnSizeControlListener;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.objects.Match;
import org.txm.objects.Project;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.TXMFontRegistry;
import org.txm.rcp.swt.dialog.ConfirmDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.utils.logger.Log;

public abstract class ConcordanceBasedAnnotationArea extends KRBasedAnnotationArea {

	protected Concordance concordance;

	protected ConcordanceEditor editor;

	/**
	 * display the annotation values of the current shown concordance lines
	 */
	protected TableColumn annotationColumn;

	protected TableViewerColumn annotationColumnViewer;

	/**
	 * The concordance annotations to show in the concordance column table
	 */
	protected ConcordanceAnnotations annotations = null;

	public ConcordanceBasedAnnotationArea() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @return the type of the annotation to create
	 */
	protected abstract AnnotationType getSelectedAnnotationType();

	/**
	 * 
	 * @return the value of the annotation to create
	 */
	protected abstract String getSelectedAnnotationValue();

	protected abstract AnnotationScope getSelectedAnnotationScope();

	private Font annotationColummnFont;

	private Font defaultColummnFont;

	@Override
	public synchronized boolean _install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception {

		this.extension = ext;
		this.editor = (ConcordanceEditor) txmeditor;
		this.concordance = this.editor.getResult();

		// RESULT
		if (position < 0) {
			position = 3 + extension.getNumberOfAnnotationArea(); // after keyword column and previous annotation columns
		}

		this.annotations = new ConcordanceAnnotations(this.editor.getResult());
		//annotations.computeLines(this.editor.getConcordance().getLines()); // pre-compute the annotation lines ot avoid breaking the sort and co.

		// create the annotation viewer column
		TableViewer viewer = this.editor.getTableViewer();

		this.annotationColumnViewer = new TableViewerColumn(viewer, SWT.CENTER, position);
		if (annotationColumn != null && !annotationColumn.isDisposed()) {
			annotationColumn.dispose();
		}
		annotationColumn = annotationColumnViewer.getColumn();
		annotationColumn.setText(Messages.category);
		annotationColumn.setToolTipText(Messages.keywordsAnnotation);
		annotationColumn.pack();
		annotationColumn.setResizable(true);
		annotationColumn.setAlignment(SWT.CENTER);

		defaultColummnFont = editor.getContainer().getFont();
		FontData fdata = defaultColummnFont.getFontData()[0];
		annotationColummnFont = TXMFontRegistry.getFont(Display.getCurrent(), fdata.getName(), fdata.getHeight(), SWT.ITALIC);

		annotationColumnViewer.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				AnnotationLine annotationLine = annotations.getAnnotationLine(line);
				if (annotationLine == null) return EMPTYTEXT;
				if (annotationLine.getAnnotation() != null) {
					String value = annotationLine.getAnnotationValue().getStandardName();
					Annotation a = annotationLine.getAnnotation();
					if (value == null) value = a.getValue();

					if (a.getStart() < line.matchGetStart()) {
						value = "… " + value; //$NON-NLS-1$
					}
					else if (a.getStart() > line.matchGetStart()) {
						//value = "> " + value; //$NON-NLS-1$
					}

					if (a.getEnd() > line.matchGetEnd()) {
						value = value + " …"; //$NON-NLS-1$
					}
					else if (a.getEnd() < line.matchGetEnd()) {
						//value = value + " <"; //$NON-NLS-1$
					}
					return value;
				}
				else {
					return EMPTYTEXT;
				}
			}

			@Override
			public Font getFont(Object element) {
				Line line = (Line) element;

				AnnotationLine annotationLine = annotations.getAnnotationLine(line);
				if (annotationLine == null) return null;

				Annotation a = annotationLine.getAnnotation();
				if (a == null) return null;

				String value = annotationLine.getAnnotationValue().getStandardName();
				if (a instanceof CQPAnnotation) return null;

				return annotationColummnFont;
			}
		});
		
		annotationColumn.addControlListener(new ConcordanceColumnSizeControlListener(annotationColumn));

		annotationArea = new GLComposite(parent, SWT.NONE, Messages.concordanceAnnotationArea);
		annotationArea.getLayout().numColumns = 12;
		annotationArea.getLayout().horizontalSpacing = 2;
		annotationArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		return __install(txmeditor, ext, parent, position);
	}

	/**
	 * finalize installation UI
	 * 
	 * @param txmeditor
	 * @param ext
	 * @param parent
	 * @param position
	 * @return
	 * @throws Exception
	 */
	public abstract boolean __install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception;


	@Override
	public final boolean save() {
		try {
			AnnotationManager am = KRAnnotationEngine.getAnnotationManager(corpus);
			if (am != null && am.hasChanges()) {
				// && MessageDialog.openConfirm(e.display.getActiveShell(), "Confirm annotation save", "Saving annotation will close this editor. Are you sure you want to save annotations right now
				// ?")
				JobHandler saveJob = SaveAnnotations.save(corpus.getMainCorpus());
				if (saveJob == null || saveJob.getResult() == Status.CANCEL_STATUS) {
					// update editor corpus
					Log.warning(Messages.errorTheAnnotationsCouldNotBeSaved);
					return false;
				}
				else {
					if (annotationColumn != null && !annotationColumn.isDisposed()) {
						annotationColumn.setText(annotations.getViewAnnotation().getName());
					}
					return true; // something was saved
				}
			}
			else {
				return false;
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public final boolean canAnnotateResult(TXMResult r) {
		if (r == null) return false;
		Project p = r.getProject();
		String importName = p.getImportModuleName();
		if (r instanceof Concordance
				&& !importName.equals("cqp") //$NON-NLS-1$
				&& new File(p.getProjectDirectory(), "txm/" + p.getName()).exists()) { //$NON-NLS-1$
			return true;
		}
		return false;
	}

	protected final void affectAnnotationToSelection(ISelection selection) {

		final IStructuredSelection lineSelection = (IStructuredSelection) selection;
		List<Line> lines = lineSelection.toList();
		ArrayList<Match> matches = new ArrayList<>();
		for (Line l : lines) {
			matches.add(l.getMatch());
		}

		affectAnnotationsToMatches(matches);
	}

	protected final void affectAnnotationValues(final List<? extends Match> matches, final AnnotationType type, final String svalue, AnnotationScope scope, JobHandler job) {

		TypedValue value_to_add = null; // reset
		if (concordance == null) {
			Log.severe("NULL concordance. Aborting."); //$NON-NLS-1$
			return;
		}
		if (matches == null || matches.size() == 0) {
			Log.warning(Messages.noLineSelectedAborting);
			return;
		}
		if (type == null) return; // no type, no annotation

		job.syncExec(new Runnable() {

			@Override
			public void run() {
				if (matches.size() > NALERTAFFECTANNOTATIONS) {
					ConfirmDialog dialog = new ConfirmDialog(Display.getCurrent().getActiveShell(),
							"confirm_annotate",  //$NON-NLS-1$
							Messages.confirmAnnotationAffectation,
							NLS.bind(Messages.youAreAboutToAnnotateP0ElementsContinue, matches.size()));

					if (dialog.open() == ConfirmDialog.CANCEL) {
						Log.info(Messages.annotationAbortedByUser);
						matches.clear();
					}
				}
			}
		});

		// get value from combo text value
		Log.fine(NLS.bind(Messages.lookingForTypedValueWithIdEqualsP0, svalue));
		final KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(type.getKnowledgeRepository());
		value_to_add = kr.getValue(type, svalue);

		if (value_to_add == null && kr instanceof LocalKnowledgeRepository) { // create or not the annotation is simple mode
			job.syncExec(new Runnable() {

				@Override
				public void run() {
					String title = kr.getString(editor.getLocale(), "ConcordancesEditor_110"); //$NON-NLS-1$
					String content = kr.getString(editor.getLocale(), "ConcordancesEditor_112"); //$NON-NLS-1$
					if (title == null) title = Messages.newValueP0ForCategoryP1;
					if (content == null) content = Messages.theValueP0IsNotAssociatedWithTheCategoryP1AssociateIt;
					ConfirmDialog dialog = new ConfirmDialog(Display.getCurrent().getActiveShell(),
							"create_value", //$NON-NLS-1$
							ConcordanceUIMessages.bind(title, svalue, type.getName()),
							ConcordanceUIMessages.bind(content, svalue, type.getName()));
					if (dialog.open() == ConfirmDialog.CANCEL) {
						Log.info(Messages.annotationAbortedByUser);
						return;
					}
					else {
						kr.addValue(svalue, svalue, type.getId());
						KRView.refresh();
					}
				}
			});
		}

		value_to_add = kr.getValue(type, svalue); // re-initialize value_to_add if the value was created

		if (value_to_add == null && kr instanceof LocalKnowledgeRepository) return; // canceled, there was a problem ?

		if (value_to_add == null) {
			job.syncExec(new Runnable() {

				@Override
				public void run() {
					String mess = Messages.bind(Messages.noValueFoundWithTheP0Id, svalue);
					Log.warning(mess);
					MessageDialog.openError(Display.getCurrent().getActiveShell(), Messages.annotationAbortedByUser, mess);
				}
			});
			return;
		}

		Log.info(TXMCoreMessages.bind(Messages.AffectP0ToP1SelectionEqualsP2, value_to_add.getId(), value_to_add.getTypeID(), matches.size()));

		// finally we can 'try' to create the annotations \o/
		try {
			HashMap<Match, List<Annotation>> existingAnnots = annotManager.createAnnotations(type, value_to_add, matches, job, scope);

			// did we had problems ?
			if (existingAnnots != null && existingAnnots.size() > 0) {
				String message = NLS.bind(Messages.couldNotAnnotateTheValueP0OnCertainSequences, value_to_add.getStandardName());
				for (Match m : existingAnnots.keySet()) {
					message += NLS.bind(Messages.theSequenceP0IsOverlappingWith, m);

					for (Annotation existingAnnot : existingAnnots.get(m)) {
						if (existingAnnot.getStart() < m.getStart()) {
							message += TXMCoreMessages.bind(Messages.theEndOfAStructureP0AtP1P2, existingAnnot.getType(), existingAnnot.getStart(), existingAnnot.getEnd());
						}
						else {
							message += TXMCoreMessages.bind(Messages.theStartOfAStructureP0AtP1P2, existingAnnot.getType(), existingAnnot.getStart(), existingAnnot.getEnd());
						}
					}
				}
				final String final_message = message;
				job.syncExec(new Runnable() {

					@Override
					public void run() {
						MessageDialog.openInformation(editor.getSite().getShell(), Messages.aboutAnnotations, final_message);
					}
				});
			}

			if (Log.getLevel().intValue() < Level.INFO.intValue() && annotManager != null) {
				annotManager.checkData();
			}
		}
		catch (Exception e1) {
			Log.warning(NLS.bind(Messages.errorWhileAffectionAnnotationColonP0, e1));
			Log.printStackTrace(e1);
			return;
		}

		job.syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					annotationColumn.setText("*" + annotations.getViewAnnotation().getName()); //$NON-NLS-1$
					notifyStartOfRefresh();
					annotationColumnViewer.getViewer().refresh();
					CorporaView.refreshObject(corpus);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected final void deleteAnnotationValues(ISelection selection) {

		final IStructuredSelection lineSelection = (IStructuredSelection) selection;
		List<Line> lines = lineSelection.toList();
		ArrayList<Match> matches = new ArrayList<>();
		for (Line l : lines) {
			matches.add(l.getMatch());
		}

		deleteAnnotationValues(matches);
	}

	protected final void deleteAnnotationValues(final List<? extends Match> matches) {
		final AnnotationType type = getSelectedAnnotationType();

		JobHandler job = new JobHandler(Messages.annotatingConcordanceSelection, true) {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				try {
					deleteAnnotationValues(matches, type, this);
				}
				catch (Exception e) {
					Log.severe(NLS.bind(Messages.errorWhileAnnotatingConcordanceSelectionColonP0, e));
					Log.printStackTrace(e);
					throw e;
				}
				return Status.OK_STATUS;
			}
		};
		job.startJob(true);
	}

	protected final void deleteAnnotationValues(List<? extends Match> matches, AnnotationType type, JobHandler job) {
		if (concordance == null) {
			return;
		}

		if (matches.size() == 0) {
			Log.warning(Messages.noLineSelectedAborting);
			return;
		}

		if (type == null) {
			return;
		}

		try {
			if (annotManager != null && annotManager.deleteAnnotations(type, matches, job)) {
				if (Log.getLevel().intValue() < Level.INFO.intValue()) annotManager.checkData();

			}
			else {
				return;
			}
		}
		catch (Exception e1) {
			Log.warning(NLS.bind(Messages.errorWhileDeletingAnnotationColonP0, e1));
			Log.printStackTrace(e1);
			return;
		}

		job.syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					annotationColumn.setText("*" + annotations.getViewAnnotation().getName()); //$NON-NLS-1$
					notifyStartOfRefresh();
					annotationColumnViewer.getViewer().refresh();
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				CorporaView.refreshObject(corpus);
			}
		});
	}

	protected final void affectAnnotationsToMatches(final List<? extends Match> matches) {
		AnnotationType type = getSelectedAnnotationType();
		String svalue = getSelectedAnnotationValue();//annotationValuesText.getText();
		AnnotationScope scope = getSelectedAnnotationScope();//AnnotationScope.fromLabel(scopeCombo.getText());
		JobHandler job = new JobHandler(Messages.annotatingConcordanceSelection, true) {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				try {
					affectAnnotationValues(matches, type, svalue, scope, this);
				}
				catch (Exception e) {
					Log.severe(NLS.bind(Messages.errorWhileAnnotatingConcordanceSelectionColonP0, e));
					Log.printStackTrace(e);
					return Status.CANCEL_STATUS;
				}

				return Status.OK_STATUS;
			}
		};
		job.startJob(true);
	}



}
