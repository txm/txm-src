package org.txm.annotation.kr.rcp.edition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TypedEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ListDialog;
import org.txm.annotation.kr.core.Annotation;
import org.txm.annotation.kr.core.AnnotationManager;
import org.txm.annotation.kr.core.DatabasePersistenceManager;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.repository.AnnotationEffect;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.KRBasedAnnotationArea;
import org.txm.annotation.kr.rcp.commands.InitializeKnowledgeRepository;
import org.txm.annotation.kr.rcp.commands.SaveAnnotations;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.annotation.kr.rcp.views.knowledgerepositories.KRView;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.RGBA;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.objects.Match;
import org.txm.objects.Match2P;
import org.txm.objects.Page;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.dialog.ConfirmDialog;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.utils.AsciiUtils;
import org.txm.utils.logger.Log;

public class WordAnnotationToolbar extends KRBasedAnnotationArea {

	/**
	 * the limit number of annotation when a confirm dialog box is shown
	 */
	protected static final int NALERTAFFECTANNOTATIONS = 100;

	public static final String EMPTYTEXT = ""; //$NON-NLS-1$

	protected SynopticEditionEditor editor;

	/**
	 * The concordance annotations to show in the concordance column table
	 */
	private EditionAnnotations annotations = null;

	/**
	 * All available annotation types
	 */
	protected List<TypedValue> typeValuesList;

	protected ComboViewer annotationTypesCombo;

	protected Label equalLabel;

	protected Text annotationValuesText;

	protected Button affectAnnotationButton;

	protected Button deleteAnnotationButton;

	protected KnowledgeRepository currentKnowledgeRepository = null;

	protected Vector<AnnotationType> typesList = new Vector<>();

	protected AnnotationType tagAnnotationType; // the tag annotation type if annotation mode is simple

	protected TypedValue value_to_add;

	//protected Concordance concordance;

	private TableViewerColumn annotationColumnViewer;

	private Font annotationColummnFont;

	private Font defaultColummnFont;

	private Button addAnnotationTypeLink;

	private Button addTypedValueLink;

	protected Label colorsLabel;

	protected Text colorsValuesText;

	@Override
	public String getName() {
		return Messages.motsPropritsInfDfaut;
	}

	@Override
	public boolean allowMultipleAnnotations() {
		return true;
	}

	public WordAnnotationToolbar() {
	}

	protected AnnotationType getSelectedAnnotationType() {
		return annotations.getViewAnnotation();
	}

	public void updateAnnotationWidgetStates() {
		if (annotationArea == null) return; // :)

		if (getSelectedAnnotationType() != null) {
			annotationValuesText.setEnabled(true);
			affectAnnotationButton.setEnabled(true);
			deleteAnnotationButton.setEnabled(true);
		}
		else {
			annotationValuesText.setEnabled(false);
			affectAnnotationButton.setEnabled(false);
			deleteAnnotationButton.setEnabled(false);
		}
	}

	protected void affectAnnotationToSelection(String[] word_selection) {

		if (word_selection == null) return;

		ArrayList<Match> matches = new ArrayList<>();

		if (word_selection[1] == null) word_selection[1] = word_selection[0];

		try {

			int[] indexes = CQPSearchEngine.getCqiClient().str2Id(corpus.getProperty("id").getQualifiedName(), word_selection); //$NON-NLS-1$
			int[] allPositions = CQPSearchEngine.getCqiClient().idList2Cpos(corpus.getProperty("id").getQualifiedName(), indexes); //$NON-NLS-1$
			for (int p = allPositions[0]; p <= allPositions[1]; p++) {
				matches.add(new Match2P(p));
			}
			affectAnnotationsToMatches(matches);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void affectAnnotationValues(final List<? extends Match> matches, final AnnotationType type, final String svalue, JobHandler job) {
		value_to_add = null; // reset
		if (matches == null || matches.size() == 0) {
			Log.warning(Messages.noLineSelectedAborting);
			return;
		}
		if (type == null) return; // no type, no annotation

		job.syncExec(new Runnable() {

			@Override
			public void run() {
				if (matches.size() > NALERTAFFECTANNOTATIONS) {
					ConfirmDialog dialog = new ConfirmDialog(Display.getCurrent().getActiveShell(),
							Messages.confirmAnnotationAffectation,
							Messages.confirmAnnotationAffectation,
							NLS.bind(Messages.youAreAboutToAnnotateP0ElementsContinue, matches.size()));

					if (dialog.open() == ConfirmDialog.CANCEL) {
						Log.info(Messages.annotationAbortedByUser);
						matches.clear();
					}
				}
			}
		});

		// get value from combo text value
		Log.fine(NLS.bind(Messages.lookingForTypedValueWithIdEqualsP0, svalue));
		final KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(type.getKnowledgeRepository());
		value_to_add = kr.getValue(type, svalue);


		if (value_to_add == null && kr instanceof LocalKnowledgeRepository) { // create or not the annotation is simple mode
			value_to_add = kr.addValue(svalue, svalue, type.getId());
		}

		if (value_to_add == null) {
			job.syncExec(new Runnable() {

				@Override
				public void run() {
					String mess = Messages.bind(Messages.noValueFoundWithTheP0Id, svalue);
					Log.warning(mess);
					MessageDialog.openError(Display.getCurrent().getActiveShell(), Messages.creationAborted, mess);
				}
			});
			return;
		}

		Log.info(TXMCoreMessages.bind(Messages.AffectP0ToP1SelectionEqualsP2, value_to_add.getId(), value_to_add.getTypeID(), matches.size()));

		// finally we can 'try' to create the annotations \o/
		try {
			HashMap<Match, List<Annotation>> existingAnnots = annotManager.createAnnotations(type, value_to_add, matches, job);

			// did we had problems ?
			if (existingAnnots != null && existingAnnots.size() > 0) {
				String message = NLS.bind(Messages.couldNotAnnotateTheValueP0OnCertainSequences, value_to_add.getStandardName());
				for (Match m : existingAnnots.keySet()) {
					message += NLS.bind(Messages.theSequenceP0IsOverlappingWith, m);

					for (Annotation existingAnnot : existingAnnots.get(m)) {
						if (existingAnnot.getStart() < m.getStart()) {
							message += TXMCoreMessages.bind(Messages.theEndOfAStructureP0AtP1P2, existingAnnot.getType(), existingAnnot.getStart(), existingAnnot.getEnd());
						}
						else {
							message += TXMCoreMessages.bind(Messages.theStartOfAStructureP0AtP1P2, existingAnnot.getType(), existingAnnot.getStart(), existingAnnot.getEnd());
						}
					}
				}
				final String final_message = message;
				job.syncExec(new Runnable() {

					@Override
					public void run() {
						MessageDialog.openInformation(editor.getSite().getShell(), Messages.aboutAnnotations, final_message);
					}
				});
			}
			// if (history != null && !history.get(type).contains(value_to_add))
			// history.get(type).add(value_to_add);

			if (Log.getLevel().intValue() < Level.INFO.intValue() && annotManager != null) {
				annotManager.checkData();
			}
			// concordance.reloadCurrentLines();
		}
		catch (Exception e1) {
			Log.warning(NLS.bind(Messages.errorWhileAffectionAnnotationColonP0, e1));
			Log.printStackTrace(e1);
			return;
		}

		job.syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					editor.refresh(false);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private HashMap<RGBA, HashSet<String>> previousSelectedTypeUnitIDS;

	public void clearHighlight() {

		if (previousSelectedTypeUnitIDS != null) {
			if (!editor.isDisposed()) {
				for (RGBA color : previousSelectedTypeUnitIDS.keySet()) {
					editor.removeHighlightWordsById(color, previousSelectedTypeUnitIDS.get(color));
				}
			}
			previousSelectedTypeUnitIDS = null; // release memory
		}
		else {
			previousSelectedTypeUnitIDS = new HashMap<RGBA, HashSet<String>>();
		}
	}

	protected void deleteAnnotationValues(String[] word_selection) {

		if (word_selection == null) return;

		ArrayList<Match> matches = new ArrayList<>();
		try {
			int[] indexes = CQPSearchEngine.getCqiClient().str2Id(corpus.getProperty("id").getQualifiedName(), word_selection); //$NON-NLS-1$
			int[] allPositions = CQPSearchEngine.getCqiClient().idList2Cpos(corpus.getProperty("id").getQualifiedName(), indexes); //$NON-NLS-1$
			for (int p : allPositions) {
				matches.add(new Match2P(p));
			}
			deleteAnnotationValues(matches);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void deleteAnnotationValues(final List<Match> matches) {
		final AnnotationType type = getSelectedAnnotationType();

		JobHandler job = new JobHandler(Messages.annotatingConcordanceSelection, true) {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				try {
					deleteAnnotationValues(matches, type, this);

					this.syncExec(new Runnable() {

						@Override
						public void run() {
							updateHighlights(null);
							editor.updateWordStyles();
						}
					});

				}
				catch (Exception e) {
					Log.severe(NLS.bind(Messages.errorWhileAnnotatingConcordanceSelectionColonP0, e));
					Log.printStackTrace(e);
					return Status.CANCEL_STATUS;
				}

				return Status.OK_STATUS;
			}
		};
		job.startJob(true);
	}

	protected void deleteAnnotationValues(List<Match> matches, AnnotationType type, JobHandler job) {

		if (matches.size() == 0) {
			Log.warning(Messages.noLineSelectedAborting);
			return;
		}

		if (type == null) {
			return;
		}

		try {
			if (annotManager != null && annotManager.deleteAnnotations(type, matches, job)) {
				if (Log.getLevel().intValue() < Level.INFO.intValue()) annotManager.checkData();
			}
			else {
				return;
			}
		}
		catch (Exception e1) {
			Log.warning(NLS.bind(Messages.errorWhileDeletingAnnotationColonP0, e1));
			Log.printStackTrace(e1);
			return;
		}

		job.syncExec(new Runnable() {

			@Override
			public void run() {
				try {
					updateHighlights(null);
					editor.updateWordStyles();
				}
				catch (Exception e) {
					Log.printStackTrace(e);
				}
			}
		});
	}

	protected void affectAnnotationsToMatches(final List<? extends Match> matches) {
		final AnnotationType type = getSelectedAnnotationType();
		final String svalue = annotationValuesText.getText();

		JobHandler job = new JobHandler(Messages.annotatingConcordanceSelection, true) {

			@Override
			protected IStatus _run(SubMonitor monitor) {

				try {
					affectAnnotationValues(matches, type, svalue, this);
					updateHighlights(null);

					this.syncExec(new Runnable() {

						@Override
						public void run() {
							editor.updateWordStyles();
						}
					});

				}
				catch (Exception e) {
					Log.severe(NLS.bind(Messages.errorWhileAnnotatingConcordanceSelectionColonP0, e));
					throw e;
				}

				return Status.OK_STATUS;
			}
		};
		job.startJob(true);
	}

	@Override
	public boolean _install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception {

		this.extension = ext;
		this.editor = (SynopticEditionEditor) txmeditor;
		this.corpus = editor.getCorpus();
		this.annotManager = KRAnnotationEngine.getAnnotationManager(corpus);
		this.annotations = new EditionAnnotations(this.editor);

		// TODO install browser listeners here

		List<KnowledgeRepository> krs = InitializeKnowledgeRepository.get(corpus.getMainCorpus());
		typesList.clear();
		Log.fine("Corpus KRs: " + krs); //$NON-NLS-1$

		currentKnowledgeRepository = KnowledgeRepositoryManager.getKnowledgeRepository(corpus.getMainCorpus().getID());
		if (currentKnowledgeRepository == null) {
			HashMap<String, HashMap<String, ?>> conf = new HashMap<>();
			HashMap<String, String> access = new HashMap<>();
			conf.put(KRAnnotationEngine.KNOWLEDGE_ACCESS, access);
			HashMap<String, HashMap<String, String>> fields = new HashMap<>();
			conf.put(KRAnnotationEngine.KNOWLEDGE_TYPES, fields);
			HashMap<String, HashMap<String, String>> strings = new HashMap<>();
			conf.put(KRAnnotationEngine.KNOWLEDGE_STRINGS, strings);

			access.put("mode", DatabasePersistenceManager.ACCESS_FILE); //$NON-NLS-1$
			access.put("version", "0"); //$NON-NLS-1$ //$NON-NLS-2$
			currentKnowledgeRepository = KnowledgeRepositoryManager.createKnowledgeRepository(corpus.getMainCorpus().getID(), conf);
			KnowledgeRepositoryManager.registerKnowledgeRepository(currentKnowledgeRepository);
			// KRAnnotationEngine.registerKnowledgeRepositoryName(corpus, corpus.getMainCorpus().getID());
		}

		List<WordProperty> wordProperties = corpus.getProperties();
		for (WordProperty p : wordProperties) {
			if (p.getName().equals("id")) continue; //$NON-NLS-1$
			AnnotationType type = currentKnowledgeRepository.getType(p.getName());
			if (type == null) {
				AnnotationType t = currentKnowledgeRepository.addType(p.getName(), p.getName());
				t.setEffect(AnnotationEffect.TOKEN);
			}
			else if (type != null) {
				type.setEffect(AnnotationEffect.TOKEN);
			}
		}

		Log.fine("KR: " + currentKnowledgeRepository); //$NON-NLS-1$
		List<AnnotationType> krtypes = currentKnowledgeRepository.getAllAnnotationTypes();
		for (AnnotationType type : krtypes) {
			if (type.getEffect().equals(AnnotationEffect.TOKEN)) {
				typesList.add(type);
			}
		}

		Log.fine(NLS.bind(Messages.availableAnnotationTypesColonP0, typesList));

		annotationArea = new GLComposite(parent, SWT.NONE, Messages.concordanceAnnotationArea);
		annotationArea.getLayout().numColumns = 12;
		annotationArea.getLayout().horizontalSpacing = 2;
		annotationArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		GridData gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 90;

		new Label(annotationArea, SWT.NONE).setText(TXMCoreMessages.common_property);

		// Display combo with list of Annotation Type
		annotationTypesCombo = new ComboViewer(annotationArea, SWT.READ_ONLY);
		annotationTypesCombo.setContentProvider(new ArrayContentProvider());
		annotationTypesCombo.setLabelProvider(new SimpleLabelProvider() {

			@Override
			public String getColumnText(Object element, int columnIndex) {
				return ((AnnotationType) element).getName();
			}

			@Override
			public String getText(Object element) {
				return ((AnnotationType) element).getName();
			}
		});
		// annotationTypesCombo.getCombo().addKeyListener(new KeyListener() {
		//
		// @Override
		// public void keyReleased(KeyEvent e) {
		// if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
		// String name = annotationTypesCombo.getCombo().getText();
		// if (currentKnowledgeRepository.getType(name) == null) {
		// createNewType(e, name);
		// } else {
		// for (AnnotationType type : typesList) {
		// if (name.equals(type.getId())) {
		// StructuredSelection selection = new StructuredSelection(type);
		// annotationTypesCombo.setSelection(selection, true);
		// break;
		// }
		// }
		// }
		// }
		// }
		//
		// @Override
		// public void keyPressed(KeyEvent e) { }
		// });

		gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 200;

		annotationTypesCombo.getCombo().setLayoutData(gdata);
		annotationTypesCombo.setInput(typesList);
		annotationTypesCombo.getCombo().select(0);
		annotations.setViewAnnotation(typesList.get(0));
		// for (int i = 0 ; i < typesList.size(); i++) {
		// String name = typesList.get(i).getName();
		// if ("word".equals(name)) {
		// annotations.setViewAnnotation(typesList.get(i));
		// annotationColumn.setText(name);
		// }
		// }

		annotationTypesCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				onAnnotationTypeSelected(event);
			}
		});

		addAnnotationTypeLink = new Button(annotationArea, SWT.PUSH);
		addAnnotationTypeLink.setToolTipText(Messages.addANewCategory);
		addAnnotationTypeLink.setImage(IImageKeys.getImage(IImageKeys.ACTION_ADD));
		addAnnotationTypeLink.setEnabled(true);
		addAnnotationTypeLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addAnnotationTypeLink.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (currentKnowledgeRepository == null) return;
				if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

				LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;

				InputDialog dialog = new InputDialog(e.widget.getDisplay().getActiveShell(), Messages.newProperty, Messages.enterTheNewPropertyName, "", null);
				if (dialog.open() == InputDialog.OK) {
					String name = dialog.getValue();
					if (name.trim().length() == 0) return;
					String id = AsciiUtils.buildId(name);
					AnnotationType existingType = kr.getType(id);
					if (existingType != null) {
						Log.warning(NLS.bind(Messages.theP0AnnotationTypeAlreadyExistsPleaseUseAnotherOneP1, id, name));
						annotationTypesCombo.setSelection(new StructuredSelection(existingType));
						return;
					}
					AnnotationType type = kr.addType(name, id, "", AnnotationEffect.TOKEN); //$NON-NLS-1$
					typesList.add(type);

					if (annotationTypesCombo != null) annotationTypesCombo.refresh();
					if (typesList.size() == 1) {
						if (annotationTypesCombo != null) annotationTypesCombo.getCombo().select(typesList.size() - 1);

						annotations.setViewAnnotation(type);
						annotations.setAnnotationOverlap(true);
						try {
							editor.refresh(false);
						}
						catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						annotationArea.layout(true);

					}
					else {
						if (typesList.size() > 1) {
							if (annotationTypesCombo != null) {
								annotationTypesCombo.setSelection(new StructuredSelection(typesList.get(typesList.size() - 1)));
							}
						}
					}
					annotations.setViewAnnotation(type);
					annotationTypesCombo.setSelection(new StructuredSelection(typesList.get(typesList.size() - 1)));
					annotationArea.layout(true);
					KRView.refresh();
					updateAnnotationWidgetStates();
				}
				else {
					Log.info(Messages.creationAborted);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		equalLabel = new Label(annotationArea, SWT.NONE);
		equalLabel.setText("="); //$NON-NLS-1$
		equalLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		annotationValuesText = new Text(annotationArea, SWT.BORDER);
		annotationValuesText.setToolTipText(Messages.enterAValueForAnId);
		GridData gdata2 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata2.widthHint = 200;
		annotationValuesText.setLayoutData(gdata2);
		annotationValuesText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					affectAnnotationToSelection(editor.getEditionPanel(0).getWordSelection());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		addTypedValueLink = new Button(annotationArea, SWT.PUSH);
		addTypedValueLink.setText("..."); //$NON-NLS-1$
		addTypedValueLink.setToolTipText(Messages.selectAValueAmongTheList);
		addTypedValueLink.setEnabled(true);
		addTypedValueLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addTypedValueLink.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (currentKnowledgeRepository == null) return;
				if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

				AnnotationType type = getSelectedAnnotationType();
				if (type == null) return;

				LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;

				ListDialog dialog = new ListDialog(e.widget.getDisplay().getActiveShell());
				String title = currentKnowledgeRepository.getString(editor.getCorpus().getLang(), "ConcordancesEditor_100"); //$NON-NLS-1$
				if (title == null) title = Messages.availableValuesForP0;
				dialog.setTitle(ConcordanceUIMessages.bind(title, type.getName()));
				dialog.setContentProvider(new ArrayContentProvider());
				dialog.setLabelProvider(new SimpleLabelProvider() {

					@Override
					public String getColumnText(Object element, int columnIndex) {
						if (element instanceof TypedValue) {
							return ((TypedValue) element).getId();
						}
						return element.toString();
					}
				});
				List<TypedValue> values;
				try {
					values = kr.getValues(type);
				}
				catch (Exception e1) {
					e1.printStackTrace();
					return;
				}
				dialog.setInput(values);
				if (dialog.open() == InputDialog.OK) {
					TypedValue value = (TypedValue) dialog.getResult()[0];
					String name = value.getId();
					if (name.trim().length() == 0) return;

					annotationValuesText.setText(name);
				}
				else {

				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		new Label(annotationArea, SWT.NONE).setText(" "); //$NON-NLS-1$

		affectAnnotationButton = new Button(annotationArea, SWT.PUSH);
		affectAnnotationButton.setText(Messages.oK);
		affectAnnotationButton.setToolTipText(Messages.proceedToAnnotation);
		affectAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		affectAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				affectAnnotationToSelection(editor.getEditionPanel(0).getWordSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		deleteAnnotationButton = new Button(annotationArea, SWT.PUSH);
		deleteAnnotationButton.setText(Messages.delete);
		deleteAnnotationButton.setToolTipText(Messages.delete);
		deleteAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		deleteAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteAnnotationValues(editor.getEditionPanel(0).getWordSelection());

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button closeButton = new Button(annotationArea, SWT.PUSH);
		closeButton.setToolTipText(Messages.closeTheToolbarWithoutSaving);
		closeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));

		closeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				extension.closeArea(WordAnnotationToolbar.this, true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});


		colorsLabel = new Label(annotationArea, SWT.NONE);
		colorsLabel.setText("Colors"); //$NON-NLS-1$
		colorsLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		colorsValuesText = new Text(annotationArea, SWT.BORDER);
		colorsValuesText.setToolTipText(Messages.enterAValueForAnId);
		gdata2 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata2.widthHint = 200;
		colorsValuesText.setLayoutData(gdata2);
		colorsValuesText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {

					updateHighlights(colorsValuesText.getText());

					editor.updateWordStyles();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});


		updateAnnotationWidgetStates();
		editor.layout(true);
		//		ext.getSaveButton().setEnabled(true);
		return true;
	}

	HashMap<RGBA, String> colorsRules;

	public void updateHighlights(String rules) {

		if (rules != null) {
			String[] couples = rules.split(","); //$NON-NLS-1$

			colorsRules = new HashMap<RGBA, String>();
			if (couples.length > 0) {
				for (String couple : couples) {
					String[] colorValue = couple.split("=", 2); //$NON-NLS-1$

					if (colorValue[0].startsWith("#")) { //$NON-NLS-1$
						colorsRules.put(new RGBA(colorValue[0]), colorValue[1]);
					}
					else {
						colorsRules.put(new RGBA(RGBA.nameToHex(colorValue[0])), colorValue[1]);
					}
				}
			}
		}

		if (colorsRules == null) return; // nothing to do

		clearHighlight();

		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();

		try {
			WordProperty property = corpus.getProperty(getSelectedAnnotationType().getId());
			WordProperty idproperty = corpus.getProperty("id"); //$NON-NLS-1$

			//			// Page word positions range (start, end)
			//			String idstart = editor.getEditionPanel(0).getCurrentPage().getWordId();
			//			String idend = editor.getEditionPanel(0).getCurrentPage().getLastWordID();
			//			
			//			int pStart = CQI.str2Id(idproperty.getQualifiedName(), new String[] {idstart})[0];
			//			int pEnd = corpus.getSize()-1;
			//			if (!idstart.equals(idend)) {
			//				pEnd = CQI.str2Id(idproperty.getQualifiedName(), new String[] {idend})[0];
			//			} else {
			//				String text = editor.getEditionPanel(0).getCurrentText().getName();
			//				org.txm.objects.Text nextText = editor.getEditionPanel(0).getNextText();
			//				String nextTextName = nextText.getName();
			//				if (!text.equals(nextTextName)) {
			//					idend = nextText.getEdition(editor.getEditionPanel(0).getEdition().getName()).getFirstPage().getWordId();
			//					pEnd = CQI.str2Id(idproperty.getQualifiedName(), new String[] {idend})[0];
			//				}
			//			}
			//			//System.out.println("start="+pStart+" end="+pEnd);
			//			
			//			List<Match> matches = new ArrayList<Match>();
			//			for (int p = pStart ; p <=pEnd ; p++) {
			//				matches.add(new Match2P(p));
			//			}

			List<Annotation> annotations = annotManager.getAnnotations(getSelectedAnnotationType(), false);

			//System.out.println("annotations="+annotations);
			previousSelectedTypeUnitIDS = new HashMap<RGBA, HashSet<String>>();
			for (RGBA color : colorsRules.keySet()) {

				Pattern regex = Pattern.compile(colorsRules.get(color));
				// get words per value
				//				System.out.println("FIND WORDS FOR COLOR AND VALUE "+color+"="+colorsRules.get(color));
				//				int[] ids = CQI.regex2Id(property.getQualifiedName(), colorsRules.get(color));
				//				System.out.println("ids="+Arrays.toString(ids));
				//				int[] positions = CQI.idList2Cpos(property.getQualifiedName(), ids);
				//				System.out.println("positions="+Arrays.toString(positions));

				// get annotations for the current page range and select those who matches the
				List<Integer> positionsList = new ArrayList<Integer>();

				for (Annotation annot : annotations) {
					if (regex.matcher(annot.getValue()).matches()) {
						positionsList.add(annot.getStart());
					}
				}

				int[] positions = new int[positionsList.size()];
				for (int i = 0; i < positionsList.size(); i++) {
					positions[i] = positionsList.get(i);
				}
				String[] wordIds = CQI.cpos2Str(idproperty.getQualifiedName(), positions);
				HashSet<String> h = new HashSet<String>();
				for (String a : wordIds) {
					h.add(a);
				}
				//System.out.println("h="+h);

				previousSelectedTypeUnitIDS.put(color, h);
				editor.setHighlightWordsById(color, h);
				//System.out.println("SET "+h);
			}
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	protected void onAnnotationTypeSelected(SelectionChangedEvent event) {

		IStructuredSelection sel = (IStructuredSelection) annotationTypesCombo.getSelection();
		AnnotationType type = (AnnotationType) sel.getFirstElement();
		if (type == null) return;

		if (type.getSize() != AnnotationType.ValuesSize.LARGE) {
			try {
				KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(type.getKnowledgeRepository());
				typeValuesList = kr.getValues(type);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		annotations.setViewAnnotation(type);
		annotations.setAnnotationOverlap(true);
		try {
			editor.refresh(false);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		updateAnnotationWidgetStates();
	}

	protected void createNewType(TypedEvent e, String typeName) {

		if (currentKnowledgeRepository == null) return;
		if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

		LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;
		if (typeName == null) typeName = ""; //$NON-NLS-1$
		InputDialog dialog = new InputDialog(e.widget.getDisplay().getActiveShell(), Messages.nouvelleProprit, Messages.nomDeLaPropritPatternEqualsazaz09Plus, typeName,
				null);
		if (dialog.open() == InputDialog.OK) {
			String name = dialog.getValue();
			if (name.trim().length() == 0) return;
			String baseid = AsciiUtils.buildId(name);
			String id = AsciiUtils.buildId(name);
			int c = 1;
			while (kr.getType(id) != null) {
				id = baseid + "_" + (c++); //$NON-NLS-1$
			}
			AnnotationType type = kr.addType(name, id, "", AnnotationEffect.TOKEN); //$NON-NLS-1$
			typesList.add(type);
			annotationTypesCombo.refresh();
			StructuredSelection selection = new StructuredSelection(type);
			annotationTypesCombo.setSelection(selection, true);

			// if (annotationTypesCombo != null) annotationTypesCombo.refresh();
			// if (typesList.size() == 1) {
			// if (annotationTypesCombo != null) annotationTypesCombo.getCombo().select(0);
			// annotations.setViewAnnotation(type);
			// annotations.setAnnotationOverlap(true);
			// try {
			// editor.refresh(false);
			// } catch (Exception e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }
			// annotationArea.layout(true);
			// } else {
			// if (typesList.size() > 1) {
			// if (annotationTypesCombo != null) {
			// annotationTypesCombo.getCombo().select(typesList.size()-1);
			// }
			// }
			// }
			// KRView.refresh();
			// updateAnnotationWidgetStates();

			// onAnnotationTypeSelected(null);
		}
		else {
			Log.info(Messages.creationAborted);
		}
	}

	@Override
	public boolean save() {
		try {
			AnnotationManager am = KRAnnotationEngine.getAnnotationManager(WordAnnotationToolbar.this.corpus);
			if (am != null && am.hasChanges()) {
				// && MessageDialog.openConfirm(e.display.getActiveShell(), "Confirm annotation save", "Saving annotation will close this editor. Are you sure you want to save annotations right now
				// ?")
				JobHandler saveJob = SaveAnnotations.save(WordAnnotationToolbar.this.corpus.getMainCorpus());
				if (saveJob == null || saveJob.getResult() == Status.CANCEL_STATUS) {
					Log.warning(Messages.errorTheAnnotationsCouldNotBeSaved);
					return false;
				}
				else {
					return true; // something was saved
				}
			}
			else {
				return false;
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean notifyStartOfCompute() throws Exception {
		if (annotationArea == null) return false;
		if (annotationArea.isDisposed()) return false;

		AnnotationType type = getSelectedAnnotationType();
		if (type != null) {
			annotations.setViewAnnotation(type);
			annotations.setAnnotationOverlap(true);
		}

		return true;
	}

	@Override
	public boolean notifyEndOfCompute() throws Exception {
		return true;
	}

	@Override
	public void notifyStartOfRefresh() throws Exception {
		if (annotationArea == null) return;
		if (annotationArea.isDisposed()) return;

		Page currentPage = editor.getEditionPanel(0).getCurrentPage();
		if (currentPage == null) return;

		String startID = currentPage.getWordId();
		Page nextPage = currentPage.getEdition().getNextPage(currentPage);
		String endID = nextPage.getWordId();

		annotations.computeStrings(startID, endID);

		updateAnnotationWidgetStates();

	}

	@Override
	protected void _close() {
		if (!annotationArea.isDisposed()) annotationArea.dispose();
		extension.layout();
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean canAnnotateResult(TXMResult r) {
		if (r instanceof org.txm.objects.Text && r != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean discardChanges() {
		return false;
	}
}
