package org.txm.annotation.kr.rcp.concordance;

import java.util.List;
import java.util.Vector;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ListDialog;
import org.txm.annotation.kr.core.AnnotationScope;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.concordance.core.functions.Line;
import org.txm.core.results.TXMResult;
import org.txm.objects.Match;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.utils.logger.Log;

/**
 * TODO merge some methods with the KRannotationToolbar
 * 
 * @author mdecorde
 *
 */
public class SimpleKRAnnotation extends ConcordanceBasedAnnotationArea {

	public SimpleKRAnnotation() {

	}

	protected Point annotationSize;

	protected Text annotationValuesText;

	protected ComboViewer annotationTypesCombo;

	protected Combo addRemoveCombo;

	protected Combo affectCombo;

	protected Button affectAnnotationButton;

	protected Vector<AnnotationType> typesList = new Vector<>();

	protected Label withLabel;

	protected AnnotationType tagAnnotationType; // the tag annotation type if annotation mode is simple

	private Button addTypedValueLink;

	@Override
	public String getName() {
		return Messages.squencesDeMotsCatgorie;
	}

	@Override
	public boolean __install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception {

		// RESULT

		if (position < 0) {
			position = 3 + extension.getNumberOfAnnotationArea(); // after keyword column and previous annotation columns
		}

		typesList.clear();

		annotationColumn.setText(Messages.category);

		List<AnnotationType> krtypes = currentKnowledgeRepository.getAllAnnotationTypes();

		boolean createTagAnnotationType = true;
		for (AnnotationType type : krtypes) {
			if (type.getName().equals("span")) { //$NON-NLS-1$
				createTagAnnotationType = false;
			}
		}
		if (createTagAnnotationType) {
			tagAnnotationType = currentKnowledgeRepository.addType("span", "span");// corresponds to an "undef" type //$NON-NLS-1$ //$NON-NLS-2$
			typesList.add(tagAnnotationType);
		}
		else {
			tagAnnotationType = currentKnowledgeRepository.getType("span"); //$NON-NLS-1$
		}
		annotations.setViewAnnotation(tagAnnotationType);



		addRemoveCombo = new Combo(annotationArea, SWT.READ_ONLY);
		String affectLabel = currentKnowledgeRepository.getString(getLocale(), "ConcordancesEditor_22"); //$NON-NLS-1$
		if (affectLabel == null) affectLabel = Messages.affect;
		String removeLabel = currentKnowledgeRepository.getString(getLocale(), "ConcordancesEditor_24"); //$NON-NLS-1$
		if (removeLabel == null) removeLabel = Messages.delete;
		String items[] = { affectLabel, removeLabel };
		addRemoveCombo.setItems(items);
		addRemoveCombo.select(0);
		addRemoveCombo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (addRemoveCombo.getSelectionIndex() == 0) { // add
					annotationValuesText.setEnabled(true);
					String withLabelText = currentKnowledgeRepository.getString(getLocale(), "ConcordancesEditor_83"); //$NON-NLS-1$
					if (withLabelText == null) withLabelText = Messages.withTheCategory;
					withLabel.setText(withLabelText);
				}
				else { // remove
					annotationValuesText.setEnabled(false);
					withLabel.setText(""); //$NON-NLS-1$
				}
				withLabel.redraw();
				annotationArea.layout();
				updateAnnotationWidgetStates();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		GridData gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		//gdata.widthHint = 100;
		addRemoveCombo.setLayoutData(gdata);

		withLabel = new Label(annotationArea, SWT.NONE);
		String withLabelText = currentKnowledgeRepository.getString(getLocale(), "ConcordancesEditor_83"); //$NON-NLS-1$
		if (withLabelText == null) withLabelText = Messages.withTheCategory;
		withLabel.setText(withLabelText);
		withLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		annotationValuesText = new Text(annotationArea, SWT.BORDER);
		annotationValuesText.setToolTipText(Messages.enterAValueForAnId);
		GridData gdata2 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata2.widthHint = 200;
		annotationValuesText.setLayoutData(gdata2);
		annotationValuesText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					affectAnnotationToSelection(editor.getTableViewer().getSelection());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		if (currentKnowledgeRepository instanceof LocalKnowledgeRepository) {
			addTypedValueLink = new Button(annotationArea, SWT.PUSH);
			addTypedValueLink.setText("..."); //$NON-NLS-1$
			addTypedValueLink.setToolTipText(Messages.openTheListOfCategories);
			addTypedValueLink.setEnabled(true);
			addTypedValueLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			addTypedValueLink.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					if (currentKnowledgeRepository == null) return;
					if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

					AnnotationType type = getSelectedAnnotationType();
					if (type == null) return;

					LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;

					ListDialog dialog = new ListDialog(e.widget.getDisplay().getActiveShell());
					dialog.setTitle(Messages.listOfCategories);
					dialog.setContentProvider(new ArrayContentProvider());
					dialog.setLabelProvider(new SimpleLabelProvider() {

						@Override
						public String getColumnText(Object element, int columnIndex) {
							if (element instanceof TypedValue)
								return ((TypedValue) element).getId();
							return element.toString();
						}
					});
					List<TypedValue> values;
					try {
						values = kr.getValues(type);
					}
					catch (Exception e1) {
						e1.printStackTrace();
						return;
					}
					dialog.setInput(values);
					if (dialog.open() == InputDialog.OK) {
						TypedValue value = (TypedValue) dialog.getResult()[0];
						String name = value.getId();
						if (name.trim().length() == 0) return;

						annotationValuesText.setText(name);
					}
					else {

					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
		}

		affectCombo = new Combo(annotationArea, SWT.READ_ONLY);
		affectCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		String items2[] = { Messages.selectedLines, Messages.allLines };
		affectCombo.setItems(items2);
		affectCombo.select(0);
		gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		//gdata.widthHint = 200;
		affectCombo.setLayoutData(gdata);

		affectAnnotationButton = new Button(annotationArea, SWT.PUSH);
		affectAnnotationButton.setText(Messages.oK);
		affectAnnotationButton.setToolTipText(Messages.proceedToAnnotation);
		affectAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		affectAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				int isel = affectCombo.getSelectionIndex();
				final boolean doAffect = addRemoveCombo.getSelectionIndex() == 0; // add is default
				
				if (isel == 0) { // selected line
					if (doAffect) {
						affectAnnotationToSelection(editor.getTableViewer().getSelection());
					}
					else {
						deleteAnnotationValues(editor.getTableViewer().getSelection());
					}
				}
				else { // all
					try {

						

						List<? extends Match> matches = concordance.getMatches();
						if (doAffect) {
							affectAnnotationsToMatches(matches);
						}
						else {
							deleteAnnotationValues(matches);
						}

					}
					catch (Exception e1) {
						Log.severe(NLS.bind(Messages.errorWhileAnnotatingConcordanceColonP0, e1.getLocalizedMessage()));
						Log.printStackTrace(e1);
						return;
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button closeButton = new Button(annotationArea, SWT.PUSH);
		closeButton.setToolTipText(Messages.closeTheToolbarWithoutSaving);
		closeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
		closeButton.setLayoutData(new GridData(GridData.END, GridData.CENTER, true, false));
		closeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				extension.closeArea(SimpleKRAnnotation.this, true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		updateAnnotationWidgetStates();
		editor.layout(true);

		//		ext.getSaveButton().setEnabled(true);
		return true;
	}

	@Override
	protected AnnotationType getSelectedAnnotationType() {
		if (annotationTypesCombo != null) {
			IStructuredSelection isel = (IStructuredSelection) annotationTypesCombo.getSelection();
			if (isel.isEmpty()) {
				return null;
			}
			return (AnnotationType) isel.getFirstElement();
		}
		else { // simple mode
			return tagAnnotationType;
		}
	}

	public void updateAnnotationWidgetStates() {
		if (annotationArea == null) return; // :)
		annotationArea.getDisplay().syncExec(new Runnable() {

			@Override
			public void run() {
				if (addRemoveCombo.getSelectionIndex() == 0) { // add is default

					if (getSelectedAnnotationType() != null) {
						annotationValuesText.setEnabled(true);

						affectAnnotationButton.setEnabled(true);
						affectCombo.setEnabled(true);
					}
					else {
						annotationValuesText.setEnabled(false);

						affectAnnotationButton.setEnabled(false);
						affectCombo.setEnabled(false);
					}
				}
				else {
					annotationValuesText.setEnabled(false);

					if (getSelectedAnnotationType() != null) {
						affectAnnotationButton.setEnabled(true);
						affectCombo.setEnabled(true);
					}
					else {
						affectAnnotationButton.setEnabled(false);
						affectCombo.setEnabled(false);
					}
				}


				if (addTypedValueLink != null) {
					addTypedValueLink.setEnabled(getSelectedAnnotationType() != null);
				}
			}
		});
	}

	@Override
	public boolean notifyStartOfCompute() throws Exception {
		if (annotationArea != null) {
			annotationArea.getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					AnnotationType type = getSelectedAnnotationType();
					if (type != null) {
						annotations.setViewAnnotation(type);
						annotations.setAnnotationOverlap(true);
					}
				}
			});

		}
		return true;
	}

	@Override
	public boolean notifyEndOfCompute() throws Exception {
		updateAnnotationWidgetStates();

		// update annotation column width
		if (annotationArea != null) {
			annotationArea.getDisplay().syncExec(new Runnable() {

				@Override
				public void run() {
					annotationColumn.pack();
					annotationColumn.setResizable(true);
				}
			});
		}
		else {
			annotationColumn.setWidth(0);
		}

		return true;
	}


	@Override
	public void notifyStartOfRefresh() throws Exception {
		List<Line> lines = concordance.getLines(concordance.getTopIndex(), concordance.getTopIndex() + concordance.getNLinePerPage());
		annotations.computeLines(lines);

		updateAnnotationWidgetStates();

		// update annotation column width
		if (annotationArea != null) {
			//annotationColumn.pack();

			annotationColumn.setResizable(true);
		}
		else {
			annotationColumn.setWidth(0);
		}

		annotationColumnViewer.getViewer().refresh();
	}

	@Override
	protected void _close() {
		if (!annotationColumn.isDisposed()) annotationColumn.dispose();
		if (!annotationArea.isDisposed()) annotationArea.dispose();
		extension.layout();
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean discardChanges() {
		return false;
	}

	@Override
	protected String getSelectedAnnotationValue() {

		return annotationValuesText.getText();
	}

	@Override
	protected AnnotationScope getSelectedAnnotationScope() {

		return AnnotationScope.FIRSTTARGET;
	}
}
