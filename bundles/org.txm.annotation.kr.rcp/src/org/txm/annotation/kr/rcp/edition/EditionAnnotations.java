package org.txm.annotation.kr.rcp.edition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.annotation.kr.core.Annotation;
import org.txm.annotation.kr.core.AnnotationManager;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.objects.Match;
import org.txm.objects.Match2P;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.MatchUtils;
import org.txm.utils.logger.Log;

/**
 * Annotation values to display in the current concordance shown lines
 * 
 * Call getStrings(conclines, from, to)
 * 
 * @author mdecorde
 *
 */
public class EditionAnnotations {

	SynopticEditionEditor editor;

	private AnnotationType viewAnnotation;

	public EditionAnnotations(SynopticEditionEditor editor) {
		this.editor = editor;
	}

	private boolean overlapAnnotation;

	private HashMap<String, HashMap<String, String>> lines;

	public void setAnnotationOverlap(boolean overlap) {
		overlapAnnotation = overlap;
	}

	public boolean getAnnotationOverlap() {
		return overlapAnnotation;
	}

	public HashMap<String, HashMap<String, String>> computeStrings(String startID, String endID) throws Exception {

		CQPCorpus corpus = this.editor.getCorpus();

		int[] indexes = CQPSearchEngine.getCqiClient().str2Id(corpus.getProperty("id").getQualifiedName(), new String[] { startID, endID }); //$NON-NLS-1$
		int[] startEndPositions = CQPSearchEngine.getCqiClient().idList2Cpos(corpus.getProperty("id").getQualifiedName(), indexes); //$NON-NLS-1$
		int[] allPositions = MatchUtils.toPositions(startEndPositions[1], startEndPositions[0]);
		String[] allWordIDS = CQPSearchEngine.getCqiClient().cpos2Str(corpus.getProperty("id").getQualifiedName(), allPositions); //$NON-NLS-1$

		HashMap<Match, String> matches = new HashMap<Match, String>();
		for (int i = 0; i < allWordIDS.length; i++) {
			matches.put(new Match2P(allPositions[i]), allWordIDS[i]);
		}
		ArrayList<Match> sorted_matches = new ArrayList<Match>(matches.keySet());
		Collections.sort(sorted_matches);

		List<Annotation> ann = getAnnotationForMatches(sorted_matches, overlapAnnotation);
		lines = new HashMap<String, HashMap<String, String>>();

		for (int i = 0; i < ann.size(); i++) {
			Annotation a = ann.get(i);
			HashMap<String, String> aline = new HashMap<String, String>();
			TypedValue annotation = getAnnotationTypedValue(a);
			aline.put(annotation.getTypeID(), annotation.getId());
			lines.put(matches.get(sorted_matches.get(i)), aline);
		}

		return lines; // TODO find a way to add with lazy method
	}

	private String findAnnotationForString(Match match, List<Annotation> annots, boolean overlap) {
		String annotationValue = ""; //$NON-NLS-1$
		if (annots != null) {
			if (!annots.isEmpty()) {
				for (Annotation annot : annots) {
					if (overlap) {
						if ((annot.getStart() <= match.getStart() && annot.getEnd() >= match.getEnd()) || (annot.getStart() > match.getStart() && annot.getEnd() < match.getEnd())) {
							annotationValue = annot.getValue();
							// use the first Annotation found, the one stored in the temporary HSQL database
							//should be modified to display a list of annotation values corresponding to the match positions (or wrapped if overlap is admitted)
						}
					}
					else {
						if (annot.getStart() == match.getStart() && annot.getEnd() == match.getEnd()) {
							annotationValue = annot.getValue();
							// use the first Annotation found, the one stored in the temporary HSQL database
						}
					}
				}
			}
		}
		return annotationValue;
	}

	public TypedValue getAnnotationTypedValue(Annotation annotation) {

		TypedValue annotationValue = null;
		if (annotation != null) {
			annotationValue = viewAnnotation.getTypedValue(annotation.getValue());
			if (annotationValue == null) { // the annotation is not stored in the KR type -> the annotation is surely built with the CQPAnnotationManager
				//System.out.println("WARNING: no annotation value found for value id="+annotation.getValue()+" - will be created.");
				annotationValue = new TypedValue(annotation.getValue(), annotation.getValue(), annotation.getType());
			}
		}

		return annotationValue;
	}

	/**
	 * 
	 * @return the current annotation type
	 */
	public AnnotationType getViewAnnotation() {
		return viewAnnotation;
	}


	private List<Annotation> prepareAnnotationsList(List<Match> matches, List<Annotation> annotations, boolean overlap) {
		List<Annotation> annotsList = new ArrayList<Annotation>();

		int iMatch = 0;
		int iAnnotation = 0;
		int nMatch = matches.size();
		int nAnnotations = annotations.size();

		while (iMatch < nMatch && iAnnotation < nAnnotations) {
			if (overlap) {
				if (matches.get(iMatch).getEnd() < annotations.get(iAnnotation).getStart()) {
					iMatch++;
					annotsList.add(null); // nothing for this match
				}
				else if (annotations.get(iAnnotation).getEnd() < matches.get(iMatch).getStart()) {
					iAnnotation++;
				}
				else {
					annotsList.add(annotations.get(iAnnotation));
					iMatch++;
				}
			}
			else {
				if (matches.get(iMatch).getEnd() < annotations.get(iAnnotation).getStart()) {
					iMatch++;
					annotsList.add(null); // nothing for this match
				}
				else if (annotations.get(iAnnotation).getEnd() < matches.get(iMatch).getStart()) {
					iAnnotation++;
				}
				else if (annotations.get(iAnnotation).getStart() == matches.get(iMatch).getStart() &&
						annotations.get(iAnnotation).getEnd() == matches.get(iMatch).getEnd()) {
							annotsList.add(annotations.get(iAnnotation));
							iMatch++;
							iAnnotation++;
						}
				else {
					iAnnotation++;
				}
			}
		}

		while (annotsList.size() < matches.size())
			annotsList.add(null); // fill matches that were not process due to no more annotations

		return annotsList;
	}

	private HashMap<Match, Annotation> prepareAnnotationsMap(List<Match> matches, List<Annotation> annots, boolean overlap) {
		HashMap<Match, Annotation> annotsList = new HashMap<Match, Annotation>();
		for (Match m : matches) {
			boolean hasAnnotations = false;
			if (annots != null) {
				if (!annots.isEmpty()) {
					for (Annotation annot : annots) {
						if (overlap) {
							if ((annot.getStart() <= m.getStart() && m.getStart() <= annot.getEnd() && m.getEnd() >= annot.getEnd()) ||
									(annot.getStart() <= m.getEnd() && m.getEnd() <= annot.getEnd() && m.getStart() <= annot.getStart())
									|| !(annot.getEnd() < m.getStart() || annot.getStart() > m.getEnd())) {
								annotsList.put(m, annot);
								hasAnnotations = true;
							}
						}
						else {
							if (annot.getStart() == m.getStart() && annot.getEnd() == m.getEnd()) {
								annotsList.put(m, annot);
								hasAnnotations = true;
							}
						}
					}
				}
			}
			if (!hasAnnotations) {
				annotsList.put(m, null);
			}
		}
		return annotsList;
	}

	public List<Annotation> getAnnotationForMatches(List<Match> subsetMatch, boolean overlapAnnotation) throws Exception {

		AnnotationManager am = KRAnnotationEngine.getAnnotationManager(editor.getCorpus());

		//List<Annotation> allAnnotations = null;
		List<Annotation> annotationsPerMatch = new ArrayList<Annotation>();
		if (viewAnnotation != null && am != null) {
			try {
				annotationsPerMatch = am.getAnnotationsForMatches(viewAnnotation, subsetMatch, overlapAnnotation);
				//System.out.println("annotationsPerMatch: "+annotationsPerMatch);
			}
			catch (Exception e) {
				Log.warning(NLS.bind(Messages.errorWhileReadingAnnotationP0AndWithP1Matches, viewAnnotation, subsetMatch.size()));
				Log.printStackTrace(e);
			}
		}
		return annotationsPerMatch;
	}

	public void setViewAnnotation(AnnotationType type) {
		viewAnnotation = type;
	}

	/**
	 * 
	 * @param line the relative position of the HashMap<String, String> in the lines list
	 * 
	 * @return the HashMap<String, String> pointed by 'i'. May return null if i is wrong.
	 */
	public HashMap<String, String> getAnnotationsString(String line) {
		if (lines == null) return null;
		if (!lines.containsKey(line)) return null;
		return lines.get(line);
	}
}
