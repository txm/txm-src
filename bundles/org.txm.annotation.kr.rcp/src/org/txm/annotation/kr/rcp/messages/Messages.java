package org.txm.annotation.kr.rcp.messages;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends TXMCoreMessages {

	private static final String BUNDLE_NAME = "org.txm.annotation.kr.rcp.messages.messages"; //$NON-NLS-1$

	public static String DeleteALocalKR;

	public static String DeleteAnAnnotationType;

	public static String Done;

	public static String ErrorWhileExportingAnnotations;

	public static String ErrorWhileImportingAnnotations;

	public static String ErrorWhileImportingAnnotationsP0;

	public static String ErrorWhileSavingAnnotationsP0;

	public static String ErrorWhileSavingStandOffAnnotations;

	public static String exportingAnnotations;

	public static String ExportingAnnotationsAsStandoff;

	public static String ExportingAnnotationsAsStandOff;

	public static String SelectionIsNotACorpusP0Aborting;

	public static String WritingAnnotationsInTheTableFileP0;

	public static String WritingAnnotationsInXMLTXMStandOffFiles;

	public static String BeforeSavingAnnotations;

	public static String CancelingLoginForP0KR;

	public static String CQPAnnotationsImportedFromP0ByP1;

	public static String CQPIndexesNotUpdatedOnlyTheXMLTXMFilesHaveBeenUpdated;

	public static String executionCanceled;

	public static String FailToUpdateTheCorpusAborting;

	public static String FixXMLTXMFilesAndCallCommandUpdateCorpus;

	public static String NoCQPAnnotationsFoundToSaveInTheCorporaP0;

	public static String SaveAnnotationsAndUpdateTheCorpus;

	public static String SaveP0Annotations;

	public static String UpdateCorpusAfterWritingTheAnnotations;
	public static String UpdateCorpusAfterWritingTheAnnotationsEtc;

	public static String UpdateEdition;

	public static String UpdateOptions;

	public static String UpdatingCorpus;

	public static String UpdatingCorpusEditionsAndIndexes;

	public static String ErrorDuringConnectionToTheP0KRPleaseCheckLogin;

	public static String ErrorWhileReadingAnnotationP0AndWithP1Matches;

	public static String ErrorWhileReadingTheP0KRValuesForTheP1Type;

	public static String availableValuesForP0;

	public static String newValueP0ForCategoryP1;

	public static String theValueP0IsNotAssociatedWithTheCategoryP1AssociateIt;

	public static String addANewCategory;

	public static String proceedToAnnotation;

	public static String category;

	public static String keywordsAnnotation;

	public static String affect;

	public static String delete;

	public static String enterAValueForAnId;

	public static String selectedLines;

	public static String annotatingConcordanceSelection;


	public static String errorWhileAnnotatingConcordanceSelectionColonP0;


	public static String allLines;

	public static String errorWhileAnnotatingConcordanceColonP0;

	public static String errorWhileDeletingAnnotationColonP0;


	public static String lookingForTypedValueWithIdEqualsP0;


	public static String noValueFoundWithTheP0Id;


	public static String AffectP0ToP1SelectionEqualsP2;


	public static String Annotation;

	public static String AskingCredentialForP0KRLogin;

	public static String couldNotAnnotateTheValueP0OnCertainSequences;


	public static String theSequenceP0IsOverlappingWith;


	public static String theEndOfAStructureP0AtP1P2;


	public static String theStartOfAStructureP0AtP1P2;


	public static String typeName;

	public static String aboutAnnotations;


	public static String errorWhileAffectionAnnotationColonP0;


	public static String errorWhileReadingAnnotationP0AndWithP1Matches;

	public static String errorWhileSavingAnnotations;

	public static String errorWhileSavingAnnotationsP0;

	public static String saveTheAnnotations;

	public static String savingAnnotationsOfP0;

	public static String openTheListOfCategories;

	public static String selectAValueAmongTheList;

	public static String valueEquals;

	public static String valueName;

	public static String pleaseEnterTheNewTypeName;

	public static String pleaseEnterTheNewValueName;

	public static String newCategory;

	public static String withTheCategory;

	public static String oK;

	public static String confirmAnnotationAffectation;

	public static String youAreAboutToAnnotateP0ElementsContinue;

	public static String listOfCategories;

	public static String squencesDeMotsCatgorievaleur;

	public static String closeTheToolbarWithoutSaving;

	public static String annotationAbortedByUser;

	public static String annotationCanceledByUser;

	public static String annotationsAreSavedInXMLTXMFiles;

	public static String availableAnnotationTypesColonP0;

	public static String errorColonNoSuitableKnowledgeRepositoryFound;

	public static String concordanceAnnotationArea;

	public static String creationAborted;

	public static String squencesDeMotsCatgorie;

	public static String motsPropritsInfDfaut;

	public static String all;

	public static String ImportingAnnotationsAsStandOff;

	public static String KnowledgeRepositoryName;

	public static String nouvelleProprit;

	public static String nomDeLaPropritPatternEqualsazaz09Plus;

	public static String PreserveAnnotationBetweenTXMSessions;

	public static String property;

	public static String ReadingAnnotationsInTheTableFileP0;

	public static String updatingTheP0Corpus;

	public static String WarningP0KRIsNullPleaseCheckKRConfiguration;

	public static String importingAnnotationsFromP0ToP1;

	public static String recodingCorpusP0;

	public static String failToRecodeTheP0CorpusProperties;

	public static String errorTheAnnotationsCouldNotBeSaved;

	public static String noLineSelectedAborting;

	public static String theP0KRAnnotationAlreadyExistingPleaseChooseAnotherP1;

	public static String noCorpusSelectedAborting;

	public static String newProperty;

	public static String enterTheNewPropertyName;

	public static String theP0AnnotationTypeAlreadyExistsPleaseUseAnotherOneP1;

	public static String scope;

	public static String scopeLayus;

	public static String defaultScope;

	public static String errorTheCorpusWasNotImportedWithTheXTZImportModuleP0;

	public static String theP0CorpusHasNoP1Property;

	public static String recodingTheP0CorpusPropertiesFromP1ToP2;

	public static String processingTheXMLTXMFiles;

	public static String failToEditorTheXMLTXMFilesOfTheP0Corpus;

	public static String creatingTheConversionRulesFromTheP0ConversionFile;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
