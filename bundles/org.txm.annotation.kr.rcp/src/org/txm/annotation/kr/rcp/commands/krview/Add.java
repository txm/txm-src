// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.annotation.kr.rcp.commands.krview;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.SQLKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.annotation.kr.rcp.views.knowledgerepositories.KRView;
import org.txm.utils.AsciiUtils;
import org.txm.utils.logger.Log;

/**
 * Copy KnowledgeRepository or AnnotationType toString() in clipboard
 */
public class Add extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		//		System.out.println("Add: "+selection);
		add(selection);

		return null;
	}

	/**
	 * Add a new annotation type or value.
	 *
	 * @param selection the selection
	 */
	public static void add(IStructuredSelection selection) {
		Object selectedItem = selection.getFirstElement();
		if (selectedItem instanceof SQLKnowledgeRepository) {

		}
		else if (selectedItem instanceof LocalKnowledgeRepository) {
			LocalKnowledgeRepository kr = ((LocalKnowledgeRepository) selectedItem);
			Shell shell = Display.getDefault().getActiveShell();
			InputDialog dialog = new InputDialog(shell, Messages.typeName, Messages.pleaseEnterTheNewTypeName, "", null); //$NON-NLS-3$
			if (dialog.open() == InputDialog.OK) {
				String name = dialog.getValue();
				String id = AsciiUtils.buildId(name);

				//Generate an URL page 
				//String pathFile = KnowledgeRepositoryManager.generateLocalURLPage(kr.getName(), name, id);
				String pathFile = null; //TODO build page for type or KR will be done in another version (after 0.7.9)
				//System.out.println("URL for new type : "+pathFile);
				AnnotationType at = kr.addType(name, id, pathFile);
				KRView.refresh();
				//kr.add(at);
			}
			else {
				Log.info(Messages.creationAborted);
			}
		}
		else if (selectedItem instanceof AnnotationType) {
			AnnotationType t = ((AnnotationType) selectedItem);

			String krname = t.getKnowledgeRepository();

			KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(krname);
			if (kr instanceof LocalKnowledgeRepository) {

				LocalKnowledgeRepository lkr = ((LocalKnowledgeRepository) kr);

				Shell shell = Display.getDefault().getActiveShell();
				InputDialog dialog = new InputDialog(shell, Messages.valueName, Messages.pleaseEnterTheNewValueName, "", null); //$NON-NLS-3$
				if (dialog.open() == InputDialog.OK) {
					String name = dialog.getValue();
					String id = name;
					/*
					 * String baseid = AsciiUtils.buildId(name);
					 * String id = AsciiUtils.buildId(name);
					 * int c = 1;
					 * while (kr.getValue(t, id) != null) {
					 * id = baseid+"_"+(c++);
					 * }
					 */
					TypedValue val = lkr.addValue(name, id, t.getId());
					//t.addTypedValue(val);
					KnowledgeRepositoryManager.generateLocalURLPage(lkr.getName(), t.getName(), t.getId());
					KRView.refresh();
				}
			}
		}
		else if (selectedItem instanceof TypedValue) {


		}
	}
}
