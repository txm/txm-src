package org.txm.annotation.kr.rcp.commands;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.kohsuke.args4j.Option;
import org.txm.annotation.kr.core.conversion.CorpusRuledConvertion;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.objects.Project;
import org.txm.rcp.commands.CloseEditorsUsing;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.swt.widget.parameters.ParametersDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.utils.logger.Log;

public class RecodeCorpus extends AbstractHandler {

	public static final String ID = RecodeCorpus.class.getCanonicalName();

	@Option(name = "conversionFile", usage = "conversionFile", widget = "File", required = true, def = "conv.tsv")
	protected File conversionFile;

	@Option(name = "oldType", usage = "oldType", widget = "String", required = true, def = "pos")
	protected String oldType;

	@Option(name = "newType", usage = "newType", widget = "String", required = true, def = "pos2")
	protected String newType;

	@Option(name = "gestionInconnus", usage = "how to manage failed conversion", widget = "String", required = true, def = "abandon")
	protected String gestionInconnus;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection isel = HandlerUtil.getCurrentSelection(event);
		if (isel == null) return null;
		if (!(isel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) isel;
		Object s = selection.getFirstElement();
		if (!(s instanceof MainCorpus)) return null;

		final MainCorpus corpus = (MainCorpus) s;

		try {
			if (ParametersDialog.open(this)) {

				JobHandler job = new JobHandler(Messages.bind(Messages.recodingCorpusP0, corpus)) {

					@Override
					protected IStatus _run(SubMonitor monitor) throws Exception {

						try {
							recode(corpus, conversionFile, oldType, newType, monitor);
						}
						catch (Exception e) {
							Log.warning(NLS.bind(Messages.failToRecodeTheP0CorpusProperties, corpus));
							throw e;
						}
						return Status.OK_STATUS;
					}
				};
				job.startJob();
				return corpus;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return corpus;
	}

	public static JobHandler recode(final MainCorpus corpus, CorpusRuledConvertion crc, String oldType, String newType, IProgressMonitor monitor) throws IOException, CqiClientException,
			XMLStreamException {

		Project project = corpus.getProject();
		if (!"xtz".equals(project.getImportModuleName())) { //$NON-NLS-1$
			Log.warning(NLS.bind(Messages.errorTheCorpusWasNotImportedWithTheXTZImportModuleP0, corpus));
			return null;
		}

		Property p1 = corpus.getProperty(oldType);
		if (p1 == null) {
			Log.warning(NLS.bind(Messages.theP0CorpusHasNoP1Property, corpus, oldType));
			return null;
		}

		Log.info(Messages.bind(Messages.recodingTheP0CorpusPropertiesFromP1ToP2, corpus, oldType, newType));

		monitor.beginTask(Messages.bind(Messages.recodingTheP0CorpusPropertiesFromP1ToP2, corpus, oldType, newType), 2);
		monitor.setTaskName(Messages.processingTheXMLTXMFiles);
		// apply conversion file
		if (!crc.process(corpus)) {
			Log.warning(Messages.bind(Messages.failToEditorTheXMLTXMFilesOfTheP0Corpus, corpus));
			return null;
		}
		monitor.worked(1);

		// update corpus indexes and edition
		// String txmhome = Toolbox.getTxmHomePath();


		project.setDoMultiThread(false); // too soon
		project.setDoUpdate(true, false);

		// monitor.setTaskName("Updating corpus");
		// File scriptDir = new File(txmhome, "scripts/groovy/user/org/txm/scripts/importer/xtz");
		// File script = new File(scriptDir, "xtzLoader.groovy");
		Log.info(NLS.bind(Messages.updatingTheP0Corpus, corpus));
		JobHandler ret = ExecuteImportScript.executeScript(project);
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				CloseEditorsUsing.corpus(corpus);
			}
		});
		monitor.worked(1);
		return ret;
	}

	public static JobHandler recode(final MainCorpus corpus, LinkedHashMap<Pattern, String> rules, String oldType, String newType, IProgressMonitor monitor) throws IOException, CqiClientException,
			XMLStreamException {

		// apply conversion file
		CorpusRuledConvertion crc = new CorpusRuledConvertion(rules, newType, oldType);
		return recode(corpus, crc, oldType, newType, monitor);
	}

	public static JobHandler recode(final MainCorpus corpus, File conversionFile, String oldType, String newType, IProgressMonitor monitor) throws IOException, CqiClientException, XMLStreamException {
		Log.info(Messages.bind(Messages.creatingTheConversionRulesFromTheP0ConversionFile, conversionFile));
		// apply conversion file
		CorpusRuledConvertion crc = new CorpusRuledConvertion(conversionFile, oldType, newType);

		return recode(corpus, crc, oldType, newType, monitor);
	}
}
