package org.txm.annotation.kr.rcp.commands.krview;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.rcp.views.knowledgerepositories.KRView;

// extends org.eclipse.ui.views.showView
public class OpenKRView extends AbstractHandler {

	public static final String ID = OpenKRView.class.getCanonicalName(); // org.txm.rcp.commands.krview.OpenKRView

	@Override
	public final Object execute(final ExecutionEvent event)
			throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);

		try {
			openView(window);
		}
		catch (PartInitException e) {
			throw new ExecutionException("Part could not be initialized", e); //$NON-NLS-1$
		}

		return null;
	}

	/**
	 * Opens the view with the given identifier.
	 * 
	 * @throws PartInitException
	 *             If the part could not be initialized.
	 */
	public static void openView(final IWorkbenchWindow activeWorkbenchWindow, AnnotationType type)
			throws PartInitException {

		final IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
		if (activePage == null) {
			return;
		}

		activePage.showView(KRView.ID);

		if (type != null) {
			KRView.showType(type);
		}
	}

	/**
	 * Opens the view with the given identifier.
	 * 
	 * @throws PartInitException
	 *             If the part could not be initialized.
	 */
	public static void openView(final IWorkbenchWindow activeWorkbenchWindow)
			throws PartInitException {
		openView(activeWorkbenchWindow, null);
	}
}
