// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.annotation.kr.rcp.commands.krview;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.SQLKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.annotation.kr.rcp.views.knowledgerepositories.KRView;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Copy KnowledgeRepository or AnnotationType toString() in clipboard
 */
public class Delete extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		//		System.out.println("Delete: "+selection);
		delete(selection);

		return null;
	}

	/**
	 * Copy.
	 *
	 * @param selection the selection
	 */
	public static void delete(IStructuredSelection selection) {
		Object selectedItem = selection.getFirstElement();

		if (selectedItem instanceof SQLKnowledgeRepository) {

		}
		else if (selectedItem instanceof LocalKnowledgeRepository) {
			LocalKnowledgeRepository kr = ((LocalKnowledgeRepository) selectedItem);
			Log.info(Messages.DeleteALocalKR);

			//TODO: confirm message dialog to generate
			KnowledgeRepositoryManager.deleteKnowledgeRepository(kr);
			KRView.refresh();

		}
		else if (selectedItem instanceof AnnotationType) {
			final AnnotationType t = ((AnnotationType) selectedItem);
			KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(t.getKnowledgeRepository());

			if (kr instanceof LocalKnowledgeRepository) {
				final LocalKnowledgeRepository lkr = ((LocalKnowledgeRepository) kr);
				Log.info(Messages.DeleteAnAnnotationType);

				/*
				 * JobHandler job = new JobHandler("Deleting all annotations for type : "+t) {
				 * @Override
				 * protected IStatus run(IProgressMonitor monitor) {
				 * this.setCurrentMonitor(monitor);
				 * try {
				 */
				lkr.deleteType(t);
				//TODO: For each corpus, get AnnotationManager and deleteAnnotations
				//annotManager.deleteAnnotations(t, job);
				/*
				 * } catch(Exception e) {
				 * Log.severe("Error while deleting annotations "+e);
				 * Log.printStackTrace(e);
				 * return Status.CANCEL_STATUS;
				 * }
				 * return Status.OK_STATUS;
				 * }
				 * };
				 * job.schedule();
				 */


				//TODO: must delete corpus possible annotations
				KRView.refresh();
			}
		}
		else if (selectedItem instanceof TypedValue) {
			TypedValue v = (TypedValue) selectedItem;
			TreeViewer tree = KRView.getTree();

			if (tree != null) {
				TreeItem[] selItems = tree.getTree().getSelection();
				if (selItems.length == 0) return;
				TreeItem first = selItems[0];
				Object o = first.getData();
				if (o instanceof TypedValue) {
					TypedValue selectedTypedValue = (TypedValue) o;
					if (selectedTypedValue.getId().equals(v.getId())) {
						TreeItem parentTreeItem = first.getParentItem();
						Object o2 = parentTreeItem.getData();
						if (o2 instanceof AnnotationType) {
							AnnotationType t = (AnnotationType) o2;
							KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(t.getKnowledgeRepository());
							kr.deleteValue(v);
							tree.refresh();
						}
					}
				}
			}
		}
	}
}
