package org.txm.annotation.kr.rcp.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.annotation.kr.core.AnnotationManager;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

public class SaveAnnotations extends AbstractHandler {

	public static final String ID = SaveAnnotations.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = HandlerUtil.getCurrentSelection(event);
		if (!(sel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) sel;

		Object s = selection.getFirstElement();
		if (!(s instanceof MainCorpus))
			return null;
		MainCorpus corpus = (MainCorpus) s;
		try {
			JobHandler job = save(corpus);
			if (job == null) {
				return null;
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		CorporaView.refresh();

		return corpus;
	}

	public static JobHandler save(final MainCorpus corpus) throws Exception {
		final AnnotationManager am = KRAnnotationEngine.getAnnotationManager(corpus);
		if (am == null) return null; // nothing to do

		if (!KRAnnotationEngine.needToSaveAnnotations(corpus)) {
			return null;
		}

		JobHandler jobhandler = new JobHandler(NLS.bind(Messages.savingAnnotationsOfP0, corpus)) {

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				try {
					boolean rez = am.saveAnnotations(monitor);
					monitor.worked(30);

					if (rez) {
						Log.fine(Messages.annotationsAreSavedInXMLTXMFiles);
						// close all opened editors of all children corpus
						SWTEditorsUtils.closeEditors(corpus.getProject(), true);
					}
					else {
						Log.warning(Messages.errorWhileSavingAnnotations);
					}
				}
				catch (Exception e) {
					Log.warning(NLS.bind(Messages.errorWhileSavingAnnotationsP0, e));
					throw e;
				}
				return Status.OK_STATUS;
			}
		};

		jobhandler.startJob(true);
		jobhandler.join();
		return jobhandler;
	}
}
