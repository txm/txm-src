package org.txm.annotation.kr.rcp.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.preferences.KRAnnotationPreferences;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.objects.Workspace;
import org.txm.rcp.commands.workspace.UpdateCorpus;
import org.txm.rcp.swt.dialog.UpdateCorpusDialog;
import org.txm.rcp.utils.JobHandler;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

public class SaveAnnotationsAndUpdateCorpus extends AbstractHandler {

	public static final String ID = SaveAnnotationsAndUpdateCorpus.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IStructuredSelection selection =  HandlerUtil.getCurrentStructuredSelection(event);

		List<MainCorpus> corpora = Workspace.getInstance().getDeepChildren(MainCorpus.class);
		Collections.sort(corpora);
		ArrayList<MainCorpus> corpusWithAnnotationsToSave = new ArrayList<>();
		for (MainCorpus corpus : corpora) {
			if (KRAnnotationEngine.needToSaveAnnotations(corpus)) {
				corpusWithAnnotationsToSave.add(corpus);
			}
		}

		//		if (HandlerUtil.getActiveEditor(event) instanceof ConcordanceEditor) {
		//			corpus = ((ConcordanceEditor) HandlerUtil.getActiveEditor(event)).getCorpus().getMainCorpus();
		//		} else {
		//			Object s = selection.getFirstElement();
		//			if (s == null) {
		//				Log.info("No available selection to target the corpus annotations to save");
		//				return null;
		//			} else if ((s instanceof MainCorpus)) {
		//				corpus = (MainCorpus) s;
		//			} else if (s instanceof TXMResult) {
		//				corpus = ((TXMResult) s).getFirstParent(MainCorpus.class);
		//			} else {
		//				Log.info("No available selection to target the corpus annotations to save");
		//				return null;
		//			}
		//		}

		if (corpusWithAnnotationsToSave.size() == 0) {
			Log.info(NLS.bind(Messages.NoCQPAnnotationsFoundToSaveInTheCorporaP0, corpora));
			return null;
		}

		try {
			// System.out.println("DISPLAY="+Display.getDefault());
			final Boolean[] doit = new Boolean[1];
			doit[0] = true;
			final MainCorpus[] corpus = new MainCorpus[1];

			final Boolean[] doupdateedition = new Boolean[1];
			doupdateedition[0] = KRAnnotationPreferences.getInstance().getBoolean(KRAnnotationPreferences.UPDATE_EDITION);

			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					UpdateCorpusDialog dialog = new UpdateCorpusDialog(Display.getDefault().getActiveShell());
					dialog.setTitle(Messages.BeforeSavingAnnotations);
					dialog.setMessages(Messages.SaveAnnotationsAndUpdateTheCorpus, dialog.getBottomMessage());
					dialog.setDoForceUpdate(false);
					dialog.setDoUpdateEdition(KRAnnotationPreferences.getInstance().getBoolean(KRAnnotationPreferences.UPDATE_EDITION));
					if (selection !=null && selection.getFirstElement() != null && selection.getFirstElement() instanceof MainCorpus) {
						dialog.setCorpusToUpdate((MainCorpus) selection.getFirstElement());
					}
					dialog.setCorporaToUpdate(corpusWithAnnotationsToSave);

					doit[0] = dialog.open() == Window.OK;
					doupdateedition[0] = dialog.getDoUpdateEdition();
					corpus[0] = dialog.getCorpustoUpdate();
				}
			});
			if (doit[0] == false) {
				return null;
			}

			final MainCorpus fcorpus = corpus[0];

			if (fcorpus == null) {
				Log.warning(Messages.noCorpusSelectedAborting);
				return null;
			}

			JobHandler saveAndUpdateJob = new JobHandler(NLS.bind(Messages.SaveP0Annotations, fcorpus.getID())) {

				@Override
				protected IStatus _run(SubMonitor monitor) throws Exception {

					Log.info(NLS.bind(Messages.SaveP0Annotations, fcorpus.getID()));
					JobHandler job = SaveAnnotations.save(fcorpus);
					if (job == null) {
						Log.warning(Messages.errorWhileSavingAnnotations);
						return Status.CANCEL_STATUS;
					}
					if (job.getResult() != Status.OK_STATUS) {
						Log.warning(Messages.errorWhileSavingAnnotations);
						return Status.CANCEL_STATUS;
					}
					if (!KRAnnotationPreferences.getInstance().getBoolean(KRAnnotationPreferences.UPDATE)) {
						Log.warning(Messages.CQPIndexesNotUpdatedOnlyTheXMLTXMFilesHaveBeenUpdated);
						return Status.CANCEL_STATUS;
					}

					JobHandler job2 = new JobHandler(Messages.UpdatingCorpusEditionsAndIndexes, true) {

						@Override
						protected IStatus _run(SubMonitor monitor) {

							monitor.setTaskName(Messages.UpdatingCorpus);
							if (fcorpus != null && UpdateCorpus.update(fcorpus, false, doupdateedition[0]) != null) {
								monitor.worked(50);
								return Status.OK_STATUS;
							}
							else {
								monitor.worked(50);
								Log.warning(Messages.FailToUpdateTheCorpusAborting);
								Log.warning(Messages.FixXMLTXMFilesAndCallCommandUpdateCorpus);
								return Status.CANCEL_STATUS;
							}
						}
					};
					job2.schedule();
					job2.join();

					if (job2.getResult() == Status.OK_STATUS) {
						this.syncExec(new Runnable() {

							@Override
							public void run() {
								CorporaView.refreshObject(fcorpus);
							}
						});

						return Status.OK_STATUS;
					}
					else {

						return Status.CANCEL_STATUS;
					}
				}
			};

			saveAndUpdateJob.schedule();
			return saveAndUpdateJob;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}
