// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.annotation.kr.rcp.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.annotation.kr.core.AnnotationManager;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.rcp.StatusLine;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

/**
 * Open a file in the TxtEditor
 * 
 * @author mdecorde.
 */
public class ExportTable extends AbstractHandler {

	public static final String ID = ExportTable.class.getCanonicalName();


	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (!(sel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) sel;

		Object s = selection.getFirstElement();
		if (!(s instanceof MainCorpus)) {
			Log.warning(NLS.bind(Messages.SelectionIsNotACorpusP0Aborting, s));
			return null;
		}

		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		//FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		MainCorpus corpus = (MainCorpus) s;
		String path = Toolbox.getTxmHomePath();
		dialog.setFilterPath(path); //To set a specific path
		dialog.setFileName(corpus.getID() + "_annotations.csv"); //$NON-NLS-1$
		dialog.setFilterExtensions(new String[] { "*.csv", "*.tsv" }); //$NON-NLS-1$ //$NON-NLS-2$
		if (dialog.open() != null) {
			StatusLine.setMessage(Messages.exportingAnnotations);
			String filepath = dialog.getFilterPath();
			String filename = dialog.getFileName();

			final File resultFile = new File(filepath, filename);
			LastOpened.set(ID, resultFile.getParent(), resultFile.getName());

			try {
				exportAnnotations(corpus, resultFile);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
		return corpus;
	}

	public static boolean exportAnnotations(final MainCorpus corpus, final File resultFile) throws Exception {
		final AnnotationManager am = KRAnnotationEngine.getAnnotationManager(corpus);
		if (am == null) return true; // nothing to do

		JobHandler jobhandler = new JobHandler("Exporting annotations in table for " + corpus) { //$NON-NLS-1$

			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {

				try {
					monitor.beginTask(Messages.ExportingAnnotationsAsStandOff, 100);

					monitor.setTaskName(NLS.bind(Messages.WritingAnnotationsInTheTableFileP0, resultFile));
					/*
					 * if (MessageDialog.openConfirm(Display.getCurrent().getActiveShell(), "Before saving annotations...", "All editors using this corpus are going to be closed. Continue ?")) {
					 * return false;
					 * }
					 */

					///lecture de tous les fichiers un par un
					///chaque annotation est récupérée et selon l'annotateur, écrite dans un fichier spécifique à celui-ci

					if (!am.exportAnnotationsToTable(resultFile)) {
						Log.warning(Messages.ErrorWhileExportingAnnotations);
						return Status.CANCEL_STATUS;
					}
					monitor.worked(30);

					Log.info(Messages.Done);

				}
				catch (Exception e) {
					Log.warning(NLS.bind(Messages.ErrorWhileSavingAnnotationsP0, e));
					throw e;
				}
				return Status.OK_STATUS;
			}
		};

		jobhandler.startJob(true);
		return true;
	}
}
