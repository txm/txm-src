package org.txm.annotation.kr.rcp;

import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.txm.annotation.kr.core.AnnotationManager;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.preferences.KRAnnotationPreferences;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.rcp.commands.InitializeKnowledgeRepository;
import org.txm.annotation.rcp.editor.AnnotationArea;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.logger.Log;

public abstract class KRBasedAnnotationArea extends AnnotationArea {

	/**
	 * the limit number of annotation when a confirm dialog box is shown
	 */
	protected static final int NALERTAFFECTANNOTATIONS = 100;

	public static final String EMPTYTEXT = ""; //$NON-NLS-1$

	/** The annotation service */
	protected AnnotationManager annotManager;

	protected KnowledgeRepository currentKnowledgeRepository;

	protected CQPCorpus corpus;

	public KRBasedAnnotationArea() {

		// TODO Auto-generated constructor stub
	}

	@Override
	public final boolean install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception {

		if (txmeditor.getResult() instanceof CQPCorpus c) {
			this.corpus = c;
		}
		else {
			this.corpus = txmeditor.getResult().getFirstParent(CQPCorpus.class);
		}

		this.annotManager = KRAnnotationEngine.getAnnotationManager(corpus);

		List<KnowledgeRepository> krs = InitializeKnowledgeRepository.get(corpus.getMainCorpus());
		Log.fine("Corpus KRs: " + krs); //$NON-NLS-1$

		currentKnowledgeRepository = KnowledgeRepositoryManager.getKnowledgeRepository(corpus.getMainCorpus().getID());
		if (currentKnowledgeRepository == null) {
			HashMap<String, HashMap<String, ?>> conf = new HashMap<>();
			currentKnowledgeRepository = KnowledgeRepositoryManager.createKnowledgeRepository(corpus.getMainCorpus().getID(), conf);
		}

		Log.fine(" KR: " + currentKnowledgeRepository); //$NON-NLS-1$


		return _install(txmeditor, ext, parent, position);
	}

	/**
	 * finalize installation UI
	 * 
	 * @param txmeditor
	 * @param ext
	 * @param parent
	 * @param position
	 * @return
	 * @throws Exception
	 */
	public abstract boolean _install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception;



	@Override
	public final boolean hasChanges() {
		AnnotationManager am;
		try {
			am = KRAnnotationEngine.getAnnotationManager(corpus);
			if (am != null && am.hasChanges()) {
				return am.hasChanges();
			}
			else {
				return false;
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public final boolean isDirty() {
		return annotManager.isDirty();
	}

	@Override
	public final boolean needToUpdateIndexes() {
		return KRAnnotationPreferences.getInstance().getBoolean(KRAnnotationPreferences.UPDATE);
	}

	@Override
	public final boolean needToUpdateEditions() {
		return KRAnnotationPreferences.getInstance().getBoolean(KRAnnotationPreferences.UPDATE_EDITION);
	}
}
