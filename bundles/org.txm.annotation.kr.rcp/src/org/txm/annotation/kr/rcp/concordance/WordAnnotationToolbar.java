package org.txm.annotation.kr.rcp.concordance;

import java.util.List;
import java.util.Vector;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TypedEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ListDialog;
import org.txm.annotation.kr.core.AnnotationScope;
import org.txm.annotation.kr.core.preferences.KRAnnotationPreferences;
import org.txm.annotation.kr.core.repository.AnnotationEffect;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.annotation.kr.rcp.views.knowledgerepositories.KRView;
import org.txm.annotation.rcp.editor.AnnotationExtension;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.objects.Match;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.swt.provider.SimpleLabelProvider;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.utils.AsciiUtils;
import org.txm.utils.logger.Log;

public class WordAnnotationToolbar extends ConcordanceBasedAnnotationArea {

	public static final String EMPTYTEXT = ""; //$NON-NLS-1$

	/**
	 * All available annotation types
	 */
	protected List<TypedValue> typeValuesList;

	protected ComboViewer annotationTypesCombo;

	/** add a new type of annotation */
	private Button addAnnotationTypeButton;

	protected Label equalLabel;

	protected Text annotationValuesText;

	protected Button affectAnnotationButton;

	protected Button deleteAnnotationButton;

	private Button affectAllAnnotationButton;

	protected Vector<AnnotationType> typesList = new Vector<>();

	protected AnnotationType tagAnnotationType; // the tag annotation type if annotation mode is simple

	//protected TypedValue value_to_add;

	private Font annotationColummnFont;

	private Font defaultColummnFont;

	private Button addAnnotationTypeLink;

	private Button addTypedValueLink;

	private Combo scopeCombo;

	@Override
	public String getName() {
		return Messages.motsPropritsInfDfaut;
	}

	@Override
	public boolean allowMultipleAnnotations() {
		return true;
	}

	public WordAnnotationToolbar() {
	}

	protected AnnotationType getSelectedAnnotationType() {
		return annotations.getViewAnnotation();
	}

	public void updateAnnotationWidgetStates() {
		if (annotationArea == null) return; // :)

		if (getSelectedAnnotationType() != null) {
			annotationValuesText.setEnabled(true);
			affectAnnotationButton.setEnabled(true);
			deleteAnnotationButton.setEnabled(true);
		}
		else {
			annotationValuesText.setEnabled(false);
			affectAnnotationButton.setEnabled(false);
			deleteAnnotationButton.setEnabled(false);
		}
	}


	@Override
	public boolean __install(TXMEditor<? extends TXMResult> txmeditor, AnnotationExtension ext, Composite parent, int position) throws Exception {

		annotationColumn.setText(Messages.property);
		annotationColumn.setToolTipText(Messages.keywordsAnnotation);

		typesList.clear();



		List<WordProperty> wordProperties = corpus.getProperties();
		for (WordProperty p : wordProperties) {
			if (p.getName().equals("id")) continue; //$NON-NLS-1$
			if (p.getName().startsWith("(") && p.getName().endsWith(")")) continue; //$NON-NLS-1$

			AnnotationType type = currentKnowledgeRepository.getType(p.getName());
			if (type == null) {
				AnnotationType t = currentKnowledgeRepository.addType(p.getName(), p.getName());
				t.setEffect(AnnotationEffect.TOKEN);
			}
			else if (type != null) {
				type.setEffect(AnnotationEffect.TOKEN);
			}
		}

		Log.fine("KR: " + currentKnowledgeRepository); //$NON-NLS-1$
		List<AnnotationType> krtypes = currentKnowledgeRepository.getAllAnnotationTypes();
		for (AnnotationType type : krtypes) {
			if (type.getEffect().equals(AnnotationEffect.TOKEN)) {
				typesList.add(type);
			}
		}

		Log.fine(NLS.bind(Messages.availableAnnotationTypesColonP0, typesList));

		annotationArea.getLayout().numColumns = 14;
		annotationArea.getLayout().horizontalSpacing = 2;

		GridData gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 90;

		new Label(annotationArea, SWT.NONE).setText(TXMCoreMessages.common_property);

		// Display combo with list of Annotation Type
		annotationTypesCombo = new ComboViewer(annotationArea, SWT.READ_ONLY);
		annotationTypesCombo.setContentProvider(new ArrayContentProvider());
		annotationTypesCombo.setLabelProvider(new SimpleLabelProvider() {

			@Override
			public String getColumnText(Object element, int columnIndex) {
				return ((AnnotationType) element).getName();
			}

			@Override
			public String getText(Object element) {
				return ((AnnotationType) element).getName();
			}
		});
		// annotationTypesCombo.getCombo().addKeyListener(new KeyListener() {
		//
		// @Override
		// public void keyReleased(KeyEvent e) {
		// if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
		// String name = annotationTypesCombo.getCombo().getText();
		// if (currentKnowledgeRepository.getType(name) == null) {
		// createNewType(e, name);
		// } else {
		// for (AnnotationType type : typesList) {
		// if (name.equals(type.getId())) {
		// StructuredSelection selection = new StructuredSelection(type);
		// annotationTypesCombo.setSelection(selection, true);
		// break;
		// }
		// }
		// }
		// }
		// }
		//
		// @Override
		// public void keyPressed(KeyEvent e) { }
		// });

		gdata = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gdata.widthHint = 200;

		annotationTypesCombo.getCombo().setLayoutData(gdata);
		annotationTypesCombo.setInput(typesList);
		annotationTypesCombo.getCombo().select(0);
		annotations.setViewAnnotation(typesList.get(0));
		annotationColumn.setText(typesList.get(0).getName());
		// for (int i = 0 ; i < typesList.size(); i++) {
		// String name = typesList.get(i).getName();
		// if ("word".equals(name)) {
		// annotations.setViewAnnotation(typesList.get(i));
		// annotationColumn.setText(name);
		// }
		// }

		annotationTypesCombo.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				onAnnotationTypeSelected(event);
			}
		});

		addAnnotationTypeLink = new Button(annotationArea, SWT.PUSH);
		addAnnotationTypeLink.setToolTipText(Messages.addANewCategory);
		addAnnotationTypeLink.setImage(IImageKeys.getImage(IImageKeys.ACTION_ADD));
		addAnnotationTypeLink.setEnabled(true);
		addAnnotationTypeLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addAnnotationTypeLink.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (currentKnowledgeRepository == null) return;
				if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

				LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;

				InputDialog dialog = new InputDialog(e.widget.getDisplay().getActiveShell(), Messages.newProperty, Messages.enterTheNewPropertyName, "", null);
				if (dialog.open() == InputDialog.OK) {
					String name = dialog.getValue();
					if (name.trim().length() == 0) return;
					String id = AsciiUtils.buildId(name);
					AnnotationType existingType = kr.getType(id);
					if (existingType != null) {
						Log.warning(NLS.bind(Messages.theP0AnnotationTypeAlreadyExistsPleaseUseAnotherOneP1, id, name));
						annotationTypesCombo.setSelection(new StructuredSelection(existingType));
						return;
					}
					AnnotationType type = kr.addType(name, id, "", AnnotationEffect.TOKEN); //$NON-NLS-1$
					typesList.add(type);

					if (annotationTypesCombo != null) annotationTypesCombo.refresh();
					if (typesList.size() == 1) {
						if (annotationTypesCombo != null) annotationTypesCombo.getCombo().select(typesList.size() - 1);

						if (concordance != null) {
							annotations.setViewAnnotation(type);
							annotations.setAnnotationOverlap(true);
							annotationColumn.setText(type.getId());
							editor.fillDisplayArea(false);
						}
						annotationArea.layout(true);

					}
					else {
						if (typesList.size() > 1) {
							if (annotationTypesCombo != null) {
								annotationTypesCombo.setSelection(new StructuredSelection(typesList.get(typesList.size() - 1)));
							}
						}
					}
					annotations.setViewAnnotation(type);
					annotationTypesCombo.setSelection(new StructuredSelection(typesList.get(typesList.size() - 1)));
					annotationArea.layout(true);
					KRView.refresh();
					updateAnnotationWidgetStates();
				}
				else {
					Log.info(Messages.creationAborted);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		equalLabel = new Label(annotationArea, SWT.NONE);
		equalLabel.setText("="); //$NON-NLS-1$
		equalLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));

		annotationValuesText = new Text(annotationArea, SWT.BORDER);
		annotationValuesText.setToolTipText(Messages.enterAValueForAnId);
		GridData gdata2 = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gdata2.widthHint = 200;
		annotationValuesText.setLayoutData(gdata2);
		annotationValuesText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					affectAnnotationToSelection(editor.getTableViewer().getSelection());
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});

		addTypedValueLink = new Button(annotationArea, SWT.PUSH);
		addTypedValueLink.setText("..."); //$NON-NLS-1$
		addTypedValueLink.setToolTipText(Messages.selectAValueAmongTheList);
		addTypedValueLink.setEnabled(true);
		addTypedValueLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addTypedValueLink.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (currentKnowledgeRepository == null) return;
				if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

				AnnotationType type = getSelectedAnnotationType();
				if (type == null) return;

				LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;

				ListDialog dialog = new ListDialog(e.widget.getDisplay().getActiveShell());
				String title = currentKnowledgeRepository.getString(editor.getLocale(), "ConcordancesEditor_100"); //$NON-NLS-1$
				if (title == null) title = Messages.availableValuesForP0;
				dialog.setTitle(ConcordanceUIMessages.bind(title, type.getName()));
				dialog.setContentProvider(new ArrayContentProvider());
				dialog.setLabelProvider(new SimpleLabelProvider() {

					@Override
					public String getColumnText(Object element, int columnIndex) {
						if (element instanceof TypedValue) {
							return ((TypedValue) element).getId();
						}
						return element.toString();
					}
				});
				List<TypedValue> values;
				try {
					values = kr.getValues(type);
				}
				catch (Exception e1) {
					e1.printStackTrace();
					return;
				}
				dialog.setInput(values);
				if (dialog.open() == InputDialog.OK) {
					TypedValue value = (TypedValue) dialog.getResult()[0];
					String name = value.getId();
					if (name.trim().length() == 0) return;

					annotationValuesText.setText(name);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		new Label(annotationArea, SWT.NONE).setText(" "); //$NON-NLS-1$

		affectAnnotationButton = new Button(annotationArea, SWT.PUSH);
		affectAnnotationButton.setText(Messages.oK);
		affectAnnotationButton.setToolTipText(Messages.proceedToAnnotation);
		affectAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		affectAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				affectAnnotationToSelection(editor.getTableViewer().getSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		deleteAnnotationButton = new Button(annotationArea, SWT.PUSH);
		deleteAnnotationButton.setText(Messages.delete);
		deleteAnnotationButton.setToolTipText(Messages.delete);
		deleteAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		deleteAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteAnnotationValues(editor.getTableViewer().getSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		affectAllAnnotationButton = new Button(annotationArea, SWT.PUSH);
		affectAllAnnotationButton.setText(Messages.all);
		affectAllAnnotationButton.setToolTipText(Messages.proceedToAnnotation);
		affectAllAnnotationButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		affectAllAnnotationButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				try {
					List<? extends Match> matches = concordance.getMatches();
					affectAnnotationsToMatches(matches);
				}
				catch (Exception e1) {
					Log.severe(NLS.bind(Messages.errorWhileAnnotatingConcordanceColonP0, e1.getLocalizedMessage()));
					Log.printStackTrace(e1);
					return;
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		new Label(annotationArea, SWT.NONE).setText(Messages.scope);
		scopeCombo = new Combo(annotationArea, SWT.SINGLE | SWT.READ_ONLY);
		scopeCombo.setToolTipText(Messages.scopeLayus);
		scopeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		scopeCombo.setItems(AnnotationScope.FIRSTTARGET.label(), AnnotationScope.FIRST.label(), AnnotationScope.TARGET.label(), AnnotationScope.ALL.label());
		scopeCombo.setText(KRAnnotationPreferences.getInstance().getString(KRAnnotationPreferences.DEFAULT_SCOPE));

		addAnnotationTypeButton = new Button(annotationArea, SWT.PUSH);
		addAnnotationTypeButton.setLayoutData(new GridData(GridData.END, GridData.CENTER, true, false));
		addAnnotationTypeButton.setToolTipText(Messages.addANewCategory);
		addAnnotationTypeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_ADD));
		addAnnotationTypeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					extension.openAnnotationMode(new WordAnnotationToolbar(), -1);
				}
				catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		Button closeButton = new Button(annotationArea, SWT.PUSH);
		closeButton.setToolTipText(Messages.closeTheToolbarWithoutSaving);
		closeButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));

		closeButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				extension.closeArea(WordAnnotationToolbar.this, true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		updateAnnotationWidgetStates();

		editor.layout(true);

		//		ext.getSaveButton().setEnabled(true);
		//		viewer.refresh();
		return true;
	}

	protected void onAnnotationTypeSelected(SelectionChangedEvent event) {
		IStructuredSelection sel = (IStructuredSelection) annotationTypesCombo.getSelection();
		AnnotationType type = (AnnotationType) sel.getFirstElement();
		if (type == null) return;

		if (type.getSize() != AnnotationType.ValuesSize.LARGE) {
			try {
				KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(type.getKnowledgeRepository());
				typeValuesList = kr.getValues(type);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		annotationColumn.setText(type.getName());

		annotations.setViewAnnotation(type);
		annotations.setAnnotationOverlap(true);
		try {
			notifyStartOfRefresh();
			annotationColumnViewer.getViewer().refresh();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		updateAnnotationWidgetStates();
	}

	protected void createNewType(TypedEvent e, String typeName) {

		if (currentKnowledgeRepository == null) return;
		if (!(currentKnowledgeRepository instanceof LocalKnowledgeRepository)) return;

		LocalKnowledgeRepository kr = (LocalKnowledgeRepository) currentKnowledgeRepository;
		if (typeName == null) typeName = ""; //$NON-NLS-1$
		InputDialog dialog = new InputDialog(e.widget.getDisplay().getActiveShell(), Messages.nouvelleProprit, Messages.nomDeLaPropritPatternEqualsazaz09Plus, typeName, null);
		if (dialog.open() == InputDialog.OK) {
			String name = dialog.getValue();
			if (name.trim().length() == 0) return;
			String baseid = AsciiUtils.buildId(name);
			String id = AsciiUtils.buildId(name);
			int c = 1;
			while (kr.getType(id) != null) {
				id = baseid + "_" + (c++); //$NON-NLS-1$
			}
			AnnotationType type = kr.addType(name, id, "", AnnotationEffect.TOKEN); //$NON-NLS-1$
			typesList.add(type);
			annotationTypesCombo.refresh();
			StructuredSelection selection = new StructuredSelection(type);
			annotationTypesCombo.setSelection(selection, true);

			// if (annotationTypesCombo != null) annotationTypesCombo.refresh();
			// if (typesList.size() == 1) {
			// if (annotationTypesCombo != null) annotationTypesCombo.getCombo().select(0);
			// annotations.setViewAnnotation(type);
			// annotations.setAnnotationOverlap(true);
			// try {
			// editor.refresh(false);
			// } catch (Exception e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }
			// annotationArea.layout(true);
			// } else {
			// if (typesList.size() > 1) {
			// if (annotationTypesCombo != null) {
			// annotationTypesCombo.getCombo().select(typesList.size()-1);
			// }
			// }
			// }
			// KRView.refresh();
			// updateAnnotationWidgetStates();

			// onAnnotationTypeSelected(null);
		}
		else {
			Log.info(Messages.creationAborted);
		}
	}

	@Override
	public boolean notifyStartOfCompute() throws Exception {
		if (annotationArea == null) return false;
		if (annotationArea.isDisposed()) return false;

		AnnotationType type = getSelectedAnnotationType();
		if (type != null) {
			annotations.setViewAnnotation(type);
			annotations.setAnnotationOverlap(true);
		}

		return true;
	}

	@Override
	public boolean notifyEndOfCompute() throws Exception {
		return true;
	}

	@Override
	public void notifyStartOfRefresh() throws Exception {
		if (annotationArea == null) return;
		if (annotationArea.isDisposed()) return;

		List<Line> lines = concordance.getLines(concordance.getTopIndex(), concordance.getTopIndex() + concordance.getNLinePerPage());
		//List<Line> lines = concordance.getLines();
		annotations.computeLines(lines);

		updateAnnotationWidgetStates();

		// update annotation column width
		if (annotationArea != null) {
			//annotationColumn.pack();

			annotationColumn.setResizable(true);
		}
		else {
			annotationColumn.setWidth(0);
		}

		annotationColumnViewer.getViewer().refresh();
	}

	@Override
	protected void _close() {

		if (!annotationColumn.isDisposed()) annotationColumn.dispose();
		if (!annotationArea.isDisposed()) annotationArea.dispose();
		extension.layout();
	}

	@Override
	public void notifyEndOfRefresh() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void notifyStartOfCreatePartControl() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean discardChanges() {
		return false;
	}

	@Override
	protected String getSelectedAnnotationValue() {

		return annotationValuesText.getText();
	}

	@Override
	protected AnnotationScope getSelectedAnnotationScope() {

		return AnnotationScope.fromLabel(scopeCombo.getText());
	}

}
