package org.txm.annotation.kr.rcp.concordance;

import org.txm.annotation.kr.core.Annotation;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.core.functions.LineDecorator;

/**
 * insert "<" ">" in concordance lines rendering at annotations begin and end positions
 * 
 * @author mdecorde
 *
 */
public class AnnotationLineDecorator extends LineDecorator {

	ConcordanceAnnotations annotations;

	public AnnotationLineDecorator(ConcordanceAnnotations annotations) {
		this.annotations = annotations;
	}

	/**
	 * Add "<" or ">" depending of the current position of the match and the annotation start end positions
	 */
	@Override
	public String decorate(Line i, int currentPos, String str) {
		Annotation annotation = annotations.getAnnotationLine(i).getAnnotation();

		if (annotation != null) {
			if (currentPos == annotation.getStart()) {
				return "< " + str; //$NON-NLS-1$
			}
			else if (currentPos == annotation.getEnd()) {
				return str + " >"; //$NON-NLS-1$
			}
		}
		return str;
	}
}
