// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.annotation.kr.rcp.preferences;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.txm.annotation.kr.core.AnnotationScope;
import org.txm.annotation.kr.core.preferences.KRAnnotationPreferences;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.rcp.swt.widget.preferences.BooleanFieldEditor;
import org.txm.rcp.swt.widget.preferences.ComboFieldEditor;

/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modif y preferences only. They are stored in the
 * preference store that belongs to the main plug-in class. That way,
 * preferences can be accessed directly via the preference store.
 */

public class KRAnnotationPreferencePage extends TXMPreferencePage {

	private BooleanFieldEditor updateEditionField;

	private BooleanFieldEditor updateCorpusField;

	private Group updateGroup;

	/**
	 * Instantiates the preference page.
	 */
	public KRAnnotationPreferencePage() {
		super();
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {
		
		String[][] values = {
				{ AnnotationScope.FIRSTTARGET.label(), AnnotationScope.FIRSTTARGET.label() },
				{ AnnotationScope.FIRST.label(), AnnotationScope.FIRST.label() },
				{ AnnotationScope.TARGET.label(), AnnotationScope.TARGET.label() },
				{ AnnotationScope.ALL.label(), AnnotationScope.ALL.label() }
		};
		this.addField(new ComboFieldEditor(KRAnnotationPreferences.DEFAULT_SCOPE, Messages.defaultScope, values, this.getFieldEditorParent()));

		updateCorpusField = new BooleanFieldEditor(KRAnnotationPreferences.UPDATE, Messages.UpdateCorpusAfterWritingTheAnnotations, this.getFieldEditorParent());
		this.addField(updateCorpusField);

		updateGroup = new Group(getFieldEditorParent(), SWT.NONE);
		updateGroup.setText(Messages.UpdateOptions);
		GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData2.horizontalSpan = 3;
		gridData2.verticalIndent = 10;
		updateGroup.setLayoutData(gridData2);
		RowLayout rlayout = new RowLayout(SWT.VERTICAL);
		rlayout.marginHeight = 5;
		updateGroup.setLayout(rlayout);

		updateEditionField = new BooleanFieldEditor(KRAnnotationPreferences.UPDATE_EDITION, Messages.UpdateEdition, updateGroup);
		this.addField(updateEditionField);
		
		this.addField(new BooleanFieldEditor(KRAnnotationPreferences.PRESERVE_ANNOTATIONS, Messages.PreserveAnnotationBetweenTXMSessions, this.getFieldEditorParent()));

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (((FieldEditor) event.getSource()).getPreferenceName().equals(KRAnnotationPreferences.UPDATE)) {
			updateEditionField.setEnabled(updateCorpusField.getBooleanValue(), updateGroup);
		}
	}

	/*
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(KRAnnotationPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setImageDescriptor(IImageKeys.getImageDescriptor(IImageKeys.PENCIL));
	}
}
