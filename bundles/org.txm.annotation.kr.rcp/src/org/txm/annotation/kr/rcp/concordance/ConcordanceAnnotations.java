package org.txm.annotation.kr.rcp.concordance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.txm.annotation.kr.core.Annotation;
import org.txm.annotation.kr.core.AnnotationManager;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.objects.Match;
import org.txm.utils.logger.Log;

/**
 * Annotation values to display in the current concordance shown lines
 * 
 * Call getLines(conclines, from, to)
 * 
 * @author mdecorde
 *
 */
public class ConcordanceAnnotations {

	Concordance concordance;

	private AnnotationType viewAnnotation;

	public ConcordanceAnnotations(Concordance concordance) {
		this.concordance = concordance;
	}

	private boolean overlapAnnotation;

	private HashMap<Line, AnnotationLine> lines;

	private List<Line> concordanceLines;

	public void setAnnotationOverlap(boolean overlap) {
		overlapAnnotation = overlap;
	}

	public boolean getAnnotationOverlap() {
		return overlapAnnotation;
	}

	public HashMap<Line, AnnotationLine> computeLines(List<Line> concordanceLines) throws Exception {

		this.concordanceLines = concordanceLines;

		//		HashMap<Match, Line> matches = new HashMap<Match, Line>();
		//		for (Line line : concordanceLines) {
		//			matches.put(line.getMatch(), line);
		//		}
		//		
		//		ArrayList<Match> sorted_matches = new ArrayList<Match>(matches.keySet());
		//		Collections.sort(sorted_matches);
		//		
		//		List<Annotation> ann = getAnnotationForMatches(sorted_matches, overlapAnnotation);
		//		lines = new HashMap<Line, AnnotationLine>();
		//		
		//		for (int i = 0 ; i < ann.size() ; i++) {
		//			Annotation a = ann.get(i);
		//			AnnotationLine aline = new AnnotationLine(a, getAnnotationTypedValue(a));
		//			lines.put(matches.get(sorted_matches.get(i)), aline);
		//		}
		//		
		//		return lines; // TODO find a way to add with lazy method

		LinkedHashMap<Match, Line> matches = new LinkedHashMap<Match, Line>();
		for (Line line : concordanceLines) {
			matches.put(line.getMatch(), line);
		}

		ArrayList<Match> sorted_matches = new ArrayList<Match>(matches.keySet());
		Collections.sort(sorted_matches);

		List<Annotation> ann = getAnnotationForMatches(sorted_matches, overlapAnnotation);
		lines = new HashMap<Line, AnnotationLine>();

		for (int i = 0; i < ann.size(); i++) {
			Annotation a = ann.get(i);
			AnnotationLine aline = new AnnotationLine(a, getAnnotationTypedValue(a));
			lines.put(matches.get(sorted_matches.get(i)), aline);
		}

		return lines; // TODO find a way to add with lazy method
	}

	private String findAnnotationForLine(Match match, List<Annotation> annots, boolean overlap) {
		String annotationValue = ""; //$NON-NLS-1$
		if (annots != null) {
			if (!annots.isEmpty()) {
				for (Annotation annot : annots) {
					if (overlap) {
						if ((annot.getStart() <= match.getStart() && annot.getEnd() >= match.getEnd()) || (annot.getStart() > match.getStart() && annot.getEnd() < match.getEnd())) {
							annotationValue = annot.getValue();
							// use the first Annotation found, the one stored in the temporary HSQL database
							//should be modified to display a list of annotation values corresponding to the match positions (or wrapped if overlap is admitted)
						}
					}
					else {
						if (annot.getStart() == match.getStart() && annot.getEnd() == match.getEnd()) {
							annotationValue = annot.getValue();
							// use the first Annotation found, the one stored in the temporary HSQL database
						}
					}
				}
			}
		}
		return annotationValue;
	}

	public TypedValue getAnnotationTypedValue(Annotation annotation) {

		TypedValue annotationValue = null;
		if (annotation != null) {
			annotationValue = viewAnnotation.getTypedValue(annotation.getValue());
			if (annotationValue == null) { // the annotation is not stored in the KR type -> the annotation is surely built with the CQPAnnotationManager
				//System.out.println("WARNING: no annotation value found for value id="+annotation.getValue()+" - will be created.");
				annotationValue = new TypedValue(annotation.getValue(), annotation.getValue(), annotation.getType());
			}
		}

		return annotationValue;
	}

	/**
	 * 
	 * @return the current annotation type
	 */
	public AnnotationType getViewAnnotation() {
		return viewAnnotation;
	}


	private List<Annotation> prepareAnnotationsList(List<Match> matches, List<Annotation> annotations, boolean overlap) {
		List<Annotation> annotsList = new ArrayList<Annotation>();

		int iMatch = 0;
		int iAnnotation = 0;
		int nMatch = matches.size();
		int nAnnotations = annotations.size();

		while (iMatch < nMatch && iAnnotation < nAnnotations) {

			if (overlap) {
				if (matches.get(iMatch).getEnd() < annotations.get(iAnnotation).getStart()) {
					iMatch++;
					annotsList.add(null); // nothing for this match
				}
				else if (annotations.get(iAnnotation).getEnd() < matches.get(iMatch).getStart()) {
					iAnnotation++;
				}
				else {
					annotsList.add(annotations.get(iAnnotation));
					iMatch++;
				}
			}
			else {
				if (matches.get(iMatch).getEnd() < annotations.get(iAnnotation).getStart()) {
					iMatch++;
					annotsList.add(null); // nothing for this match
				}
				else if (annotations.get(iAnnotation).getEnd() < matches.get(iMatch).getStart()) {
					iAnnotation++;
				}
				else if (annotations.get(iAnnotation).getStart() == matches.get(iMatch).getStart() &&
						annotations.get(iAnnotation).getEnd() == matches.get(iMatch).getEnd()) {
							annotsList.add(annotations.get(iAnnotation));
							iMatch++;
							iAnnotation++;
						}
				else {
					iAnnotation++;
				}
			}
		}

		while (annotsList.size() < matches.size())
			annotsList.add(null); // fill matches that were not process due to no more annotations

		return annotsList;
	}

	private HashMap<Match, Annotation> prepareAnnotationsMap(List<Match> matches, List<Annotation> annots, boolean overlap) {
		HashMap<Match, Annotation> annotsList = new HashMap<Match, Annotation>();
		for (Match m : matches) {
			boolean hasAnnotations = false;
			if (annots != null) {
				if (!annots.isEmpty()) {
					for (Annotation annot : annots) {
						if (overlap) {
							if ((annot.getStart() <= m.getStart() && m.getStart() <= annot.getEnd() && m.getEnd() >= annot.getEnd()) ||
									(annot.getStart() <= m.getEnd() && m.getEnd() <= annot.getEnd() && m.getStart() <= annot.getStart())
									|| !(annot.getEnd() < m.getStart() || annot.getStart() > m.getEnd())) {
								annotsList.put(m, annot);
								hasAnnotations = true;
							}
						}
						else {
							if (annot.getStart() == m.getStart() && annot.getEnd() == m.getEnd()) {
								annotsList.put(m, annot);
								hasAnnotations = true;
							}
						}
					}
				}
			}
			if (!hasAnnotations) {
				annotsList.put(m, null);
			}
		}
		return annotsList;
	}

	public List<Annotation> getAnnotationForMatches(List<Match> subsetMatch, boolean overlapAnnotation) throws Exception {

		AnnotationManager am = KRAnnotationEngine.getAnnotationManager(concordance.getCorpus());

		//List<Annotation> allAnnotations = null;
		List<Annotation> annotationsPerMatch = new ArrayList<Annotation>();
		if (viewAnnotation != null && am != null) {
			try {
				annotationsPerMatch = am.getAnnotationsForMatches(viewAnnotation, subsetMatch, overlapAnnotation);
				//System.out.println("annotationsPerMatch: "+annotationsPerMatch);
			}
			catch (Exception e) {
				Log.warning(NLS.bind(Messages.ErrorWhileReadingAnnotationP0AndWithP1Matches, viewAnnotation, subsetMatch.size()));
				Log.printStackTrace(e);
			}
		}
		return annotationsPerMatch;
	}

	public void setViewAnnotation(AnnotationType type) {
		viewAnnotation = type;
		//concordance.resetLines();
	}

	/**
	 * 
	 * @param line the relative position of the AnnotationLine in the lines list
	 * 
	 * @return the AnnotationLine pointed by 'i'. May return null if i is wrong.
	 */
	public AnnotationLine getAnnotationLine(Line line) {
		if (lines == null) return null;
		if (!lines.containsKey(line)) return null;
		return lines.get(line);
	}
}
