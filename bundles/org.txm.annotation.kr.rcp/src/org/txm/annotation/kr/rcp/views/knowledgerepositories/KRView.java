// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.annotation.kr.rcp.views.knowledgerepositories;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.part.ViewPart;
import org.txm.annotation.kr.core.repository.AnnotationType;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.core.repository.LocalKnowledgeRepository;
import org.txm.annotation.kr.core.repository.SQLKnowledgeRepository;
import org.txm.annotation.kr.core.repository.TypedValue;
import org.txm.annotation.kr.rcp.commands.InitializeKnowledgeRepository;
import org.txm.annotation.kr.rcp.commands.krview.Informations;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

/**
 * Display the Knowledge repositories currently loaded
 * 
 * author mdecorde.
 */
public class KRView extends ViewPart {

	/** The empty list to filled the view at startup. */
	ArrayList<Object> empty = new ArrayList<Object>();

	/** The composites. */
	Control[] composites = new Control[3];

	/** The tv. */
	TreeViewer tv;

	/** The ID. */
	public static String ID = KRView.class.getName();

	/**
	 * Instantiates a new queries view.
	 */
	public KRView() {
	}

	/**
	 * Refresh.
	 */
	public static void refresh() {
		KRView baseView = (KRView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						KRView.ID);
		if (baseView != null)
			baseView.tv.refresh();
	}

	/**
	 * Reload.
	 */
	public static void reload() {
		KRView baseView = (KRView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						KRView.ID);
		if (baseView != null)
			baseView._reload();
	}

	/**
	 * _reload.
	 */
	public void _reload() {

		KnowledgeRepository[] repos = KnowledgeRepositoryManager.getKnowledgeRepositories();
		if (repos == null) return;
		if (tv != null) {
			tv.setInput(repos);
			refresh();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(new GridLayout(1, true));
		Button refresh = new Button(parent, SWT.PUSH);
		refresh.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		refresh.setText("⟲"); //$NON-NLS-1$
		refresh.setToolTipText("Refresh the available knowledge repositories"); //$NON-NLS-1$
		refresh.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// force loading KR of all corpora
				try {
					for (MainCorpus corpus : CorpusManager.getCorpusManager().getCorpora().values()) {
						for (KnowledgeRepository kr : InitializeKnowledgeRepository.get(corpus.getMainCorpus())) {
							kr.checkConnection();
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
					return;
				}

				_reload();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});

		tv = new TreeViewer(parent);
		tv.getTree().setLayoutData(
				new GridData(GridData.FILL, GridData.FILL, true, true));

		tv.setContentProvider(new KRTreeProvider());
		tv.setLabelProvider(new KRLabelProvider());

		_reload();

		tv.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				TreeSelection selection = (TreeSelection) tv.getSelection();
				Informations.show(selection);
			}
		});

		//		TreeViewerColumn name = new TreeViewerColumn(tv, SWT.NONE);
		//		name.getColumn().setText("name");
		//		TreeViewerColumn desc = new TreeViewerColumn(tv, SWT.NONE);
		//		desc.getColumn().setText("desc");

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tv.getTree());

		// Set the MenuManager
		tv.getTree().setMenu(menu);
		getSite().registerContextMenu(menuManager, tv);
		// Make the selection available
		getSite().setSelectionProvider(tv);

		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("org.txm.rcp.annotation.krview"); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {

	}

	/**
	 * The Class TxmObjectLabelProvider.
	 */
	public class KRLabelProvider extends LabelProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(Object element) {
			if (element instanceof KnowledgeRepository)
				return ((KnowledgeRepository) element).getName();
			else if (element instanceof AnnotationType)
				return ((AnnotationType) element).getId() + ((AnnotationType) element).getEffect().toSuffix();
			else if (element instanceof TypedValue)
				return ((TypedValue) element).getStandardName();
			return NLS.bind(TXMUIMessages.errorWithElementP0, element);
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		@Override
		public final Image getImage(Object element) {
			if (element instanceof SQLKnowledgeRepository)
				return IImageKeys.getImage(IImageKeys.WORLD);
			return null;
		}
	}

	/**
	 * The Class TxmObjectTreeProvider.
	 */
	public class KRTreeProvider implements ITreeContentProvider {

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof KnowledgeRepository) {
				KnowledgeRepository kr = ((KnowledgeRepository) element);
				List<AnnotationType> types = kr.getAllAnnotationTypes();

				return types.toArray();
			}
			else if (element instanceof AnnotationType) {
				AnnotationType t = ((AnnotationType) element);
				KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(t.getKnowledgeRepository());
				if (kr instanceof LocalKnowledgeRepository) {
					LocalKnowledgeRepository krl = (LocalKnowledgeRepository) kr;
					try {
						return krl.getValues(t).toArray();
					}
					catch (Exception e) {
						Log.warning(NLS.bind(Messages.ErrorWhileReadingTheP0KRValuesForTheP1Type, kr, t));
						Log.printStackTrace(e);
					}
				}
				return new Object[0];
			}
			else {
				return new Object[0];
			}
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		@Override
		public Object getParent(Object element) {
			if (element instanceof KnowledgeRepository) {
				return null;
			}
			else if (element instanceof AnnotationType) {
				return ((AnnotationType) element).getKnowledgeRepository();
			}
			else {
				return null;
			}
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof KnowledgeRepository) {
				KnowledgeRepository kr = ((KnowledgeRepository) element);
				List<AnnotationType> types = kr.getAllAnnotationTypes();
				return types.size() > 0;
			}
			else if (element instanceof KnowledgeRepository[]) {
				return ((KnowledgeRepository[]) element).length > 0;
			}
			else if (element instanceof AnnotationType) {
				AnnotationType t = ((AnnotationType) element);
				KnowledgeRepository kr = KnowledgeRepositoryManager.getKnowledgeRepository(t.getKnowledgeRepository());

				if (kr instanceof LocalKnowledgeRepository) {
					LocalKnowledgeRepository krl = (LocalKnowledgeRepository) kr;
					try {
						return krl.hasValues(t);
					}
					catch (Exception e) {
						Log.warning(NLS.bind(Messages.ErrorWhileReadingTheP0KRValuesForTheP1Type, kr, t));
						Log.printStackTrace(e);
					}
				}
				return false;
			}
			else {
				return false;
			}
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		@Override
		public Object[] getElements(Object element) {
			if (element instanceof KnowledgeRepository) {
				KnowledgeRepository kr = ((KnowledgeRepository) element);
				List<AnnotationType> types = kr.getAllAnnotationTypes();
				return types.toArray();
			}
			else if (element instanceof KnowledgeRepository[]) {
				return ((KnowledgeRepository[]) element);
			}
			else {
				return new Object[0];
			}
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		@Override
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	public static void showType(AnnotationType type) {

		TreeViewer tv = getTree();
		if (tv != null) {
			StructuredSelection selection = new StructuredSelection(type);
			tv.expandToLevel(selection, AbstractTreeViewer.ALL_LEVELS);
			tv.setSelection(selection);
		}
		else {
			// call org.eclipse.ui.handlers.ShowViewHandler
			// extends the command to select the type ?
		}
	}

	public static TreeViewer getTree() {
		KRView baseView = (KRView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(
						KRView.ID);
		if (baseView != null)
			return baseView.tv;

		return null;
	}
}
