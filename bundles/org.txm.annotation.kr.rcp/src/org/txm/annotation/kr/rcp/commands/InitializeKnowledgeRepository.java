package org.txm.annotation.kr.rcp.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osgi.service.prefs.BackingStoreException;
import org.txm.annotation.kr.core.KRAnnotationEngine;
import org.txm.annotation.kr.core.repository.KnowledgeRepository;
import org.txm.annotation.kr.core.repository.KnowledgeRepositoryManager;
import org.txm.annotation.kr.rcp.messages.Messages;
import org.txm.rcp.swt.dialog.UsernamePasswordDialog;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

public class InitializeKnowledgeRepository extends AbstractHandler {

	public static final String ID = InitializeKnowledgeRepository.class.getCanonicalName();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (!(sel instanceof IStructuredSelection)) return null;
		IStructuredSelection selection = (IStructuredSelection) sel;

		Object s = selection.getFirstElement();
		if (!(s instanceof MainCorpus))
			return null;

		MainCorpus corpus = (MainCorpus) s;
		return get(corpus);
	}

	/**
	 * Use this method to set eventual login+password needed by SQL knowledge repositories
	 * 
	 * @param corpus
	 * @return
	 */
	public static List<KnowledgeRepository> get(MainCorpus corpus) {
		List<KnowledgeRepository> ret = new ArrayList<KnowledgeRepository>();

		for (String kr_name : KRAnnotationEngine.getKnowledgeRepositoryNames(corpus)) {
			KnowledgeRepository kr;
			try {
				kr = KRAnnotationEngine.getKnowledgeRepository(corpus, kr_name);

				if (kr == null) {
					Log.warning(NLS.bind(Messages.WarningP0KRIsNullPleaseCheckKRConfiguration, kr_name));
					continue;
				}
				//System.out.println("IS CREDENTIAL NEEDED: "+kr_name+" "+corpus);
				boolean[] must = KnowledgeRepositoryManager.mustLoginToKnowledgeRepository(kr_name, corpus);

				if (must[0] || must[1]) {
					Log.info(NLS.bind(Messages.AskingCredentialForP0KRLogin, kr_name));
					UsernamePasswordDialog dialog = new UsernamePasswordDialog(Display.getCurrent().getActiveShell(), must, kr_name);

					if (dialog.open() == UsernamePasswordDialog.OK) {
						Log.info("Temporary save login+password"); //$NON-NLS-1$
						System.setProperty(KnowledgeRepository.LOGIN_KEY + kr_name, dialog.getUser());
						System.setProperty(KnowledgeRepository.PASSWORD_KEY + kr_name, dialog.getPassword());
					}
					else {
						Log.info(NLS.bind(Messages.CancelingLoginForP0KR, kr_name));
						continue; // ignoring
					}
				}

				if (!kr.checkConnection()) {
					Log.warning(NLS.bind(Messages.ErrorDuringConnectionToTheP0KRPleaseCheckLogin, kr_name));
				}
				else {
					ret.add(kr);
				}
			}
			catch (BackingStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return ret;
	}
}
