groovy-all jar version must be the same as the set Eclipse Groovy compiler.

Take it from Eclipse plugins directory :
$ECLIPSE/plugins/org.codehaus.groovy_2.3.3.xx-201406301445-e44/lib

Declare the groovy-all jar in
* project dependencies and exported dependencies
* MANIFEST.MF runtime exported packages
* MANIFEST.MF runtime classpath