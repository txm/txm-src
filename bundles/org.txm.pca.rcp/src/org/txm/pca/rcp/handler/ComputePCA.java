// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.pca.rcp.handler;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.ca.rcp.messages.CAUIMessages;
import org.txm.core.results.TXMResult;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.pca.core.functions.IPCAComputable;
import org.txm.pca.core.functions.PCA;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.logger.Log;

/**
 * Opens a CA editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputePCA extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		final Object selection = this.getCorporaViewSelectedObject(event);

		try {

			LexicalTable lexicalTable = null;
			PCA ca = null;

			// Reopening an existing result
			if (selection instanceof PCA) {
				ca = (PCA) selection;
			}
			// Creating
			else {
				// Creating from Partition
				if (selection instanceof IPCAComputable c) {
					((TXMResult) c).compute(false);
					ca = c.toPCA();
					lexicalTable = ca.getLexicalTable();
					ca.compute();
				}

				// show the lexical table only if the command was called from a lexical table
				if (!(selection instanceof LexicalTable)) {
					if (lexicalTable != null) lexicalTable.setVisible(false);
				}
			}

			try {
				TXMResultEditorInput<PCA> editorInput = new TXMResultEditorInput<>(ca);
				// IWorkbenchPage page = TXMWindows.getActivePage();
				StatusLine.setMessage(CAUIMessages.openingTheCorrespondenceAnalysisResults);
			}
			catch (Throwable e) {
				Log.severe(CAUIMessages.bind(CAUIMessages.error_openingP0, ca));
				Log.printStackTrace(e);
			}
		}
		catch (Throwable e) {
			Log.severe(CAUIMessages.bind(CAUIMessages.error_cannot_compute_withP0, selection));
			Log.printStackTrace(e);
		}
		return null;
	}

}
