package org.txm.treesearch.preferences;

import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.treesearch.rcp.Messages;

/**
 * Syntatic annotations preferences page
 * 
 * @author mdecorde
 *
 */
public class SyntacticAnnotationPreferencePage extends TXMPreferencePage {

	@Override
	public void createFieldEditors() {
		//this.addField(new StringFieldEditor(TreeSearchPreferences.DEFAULT_VISUALISATION, Messages.SyntacticVisualisation, this.getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(TreeSearchPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setDescription(Messages.SyntacticAnnotation);
		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/functions/Tree.png")); //$NON-NLS-1$
	}
}
