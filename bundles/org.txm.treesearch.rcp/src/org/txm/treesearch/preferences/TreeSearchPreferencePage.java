package org.txm.treesearch.preferences;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.treesearch.rcp.Messages;

/**
 * TreeSearch command preferences page
 * 
 * @author mdecorde
 *
 */
public class TreeSearchPreferencePage extends TXMPreferencePage {

	private ComboFieldEditor backtotext_position;

	@Override
	public void createFieldEditors() {

		this.addField(new StringFieldEditor(TreeSearchPreferences.DEFAULT_VISUALISATION, Messages.DefaultSyntacticVisualisation, this.getFieldEditorParent()));

		String[][] values = {
				{ "OVER", "-1" },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "RATIO", "-2" },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "ABOVE", "" + EModelService.ABOVE },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "BELOW", "" + EModelService.BELOW },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "LEFT", "" + EModelService.LEFT_OF },  //$NON-NLS-1$ //$NON-NLS-2$
				{ "RIGHT", "" + EModelService.RIGHT_OF } }; //$NON-NLS-1$ //$NON-NLS-2$

		backtotext_position = new ComboFieldEditor(TreeSearchPreferences.BACKTOTREE_POSITION, Messages.linkedEditorPosition, values, getFieldEditorParent());
		addField(backtotext_position);

		Label note = new Label(getFieldEditorParent(), SWT.NONE);
		note.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		note.setText("Drives the position of the new editor position:\n"
				+ "\tOVER, the origin one\n"
				+ "\tRATIO: depending on the available space the area is slit horizontally or vertically\n"
				+ "\tABOVE, BELOW, on the LEFT or on the RIGHT the origin one\n");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(TreeSearchPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/functions/Tree.png")); //$NON-NLS-1$
	}
}
