// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.treesearch.command;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;
import org.txm.treesearch.function.TreeSearch;

/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 */
public class TreeSearchAdapterFactory extends TXMResultAdapterFactory {

	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(TreeSearchAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(TreeSearchAdapterFactory.class).getSymbolicName() + "/icons/functions/Tree.png"); //$NON-NLS-1$ //$NON-NLS-2$

	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof TreeSearch) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			};
		}
		return null;
	}
}
