// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.treesearch.command;

import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.EditorPart;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Line;
import org.txm.concordance.rcp.editors.ConcordanceEditor;
import org.txm.core.results.TXMResult;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.objects.Match;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.treesearch.editor.TreeSearchEditor;
import org.txm.treesearch.function.TreeSearch;
import org.txm.treesearch.preferences.TreeSearchPreferences;
import org.txm.treesearch.rcp.Messages;
import org.txm.utils.logger.Log;


/**
 * Open the TIGERSearch Editor using the current selection OR a preference node set in the RCP command parameter TXMPreferences.RESULT_PARAMETERS_NODE_PATH
 * 
 * @author mdecorde
 */
public class SendToTreeSearch extends BaseAbstractHandler {

	/** The ID. */
	public static String ID = SendToTreeSearch.class.getName();

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		ITXMResultEditor<? extends TXMResult> editor = SWTEditorsUtils.getActiveEditor(event);

		ISelection s = HandlerUtil.getCurrentSelection(event);

		if (editor instanceof ConcordanceEditor) {
			return concordancetoTree(editor, s);
		}
		else if (editor instanceof SynopticEditionEditor) {
			try {
				return editiontoTree((SynopticEditionEditor) editor, s);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	public Object editiontoTree(SynopticEditionEditor editor, ISelection s) throws Exception {

		if (editor.getEditionPanels().size() == 0) { // broken or no edition
			return null;
		}

		int kp = -1;
		//System.out.println("s="+s.getClass()+" "+s);

		String[] words = editor.getEditionPanel(0).getWordSelection();

		String word_id = null;
		if (words != null && words[0] != null && !words[0].isEmpty()) {
			word_id = words[0];
		}
		else {
			word_id = editor.getEditionPanel(0).getCurrentPage().getWordId();
		}

		if (word_id != null) {
			//WordProperty p_id = editor.getCorpus().getProperty("id");
			Selection sel = CQPSearchEngine.getEngine().query(editor.getCorpus(), new CQLQuery("[id=\"" + word_id + "\" & _.text_id=\"" + CQLQuery.addBackSlash(editor.getResult().getName()) + "\"]"), //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
					"TMP", false);    //$NON-NLS-1$
			if (sel.getNMatch() > 0) {
				kp = sel.getMatches().get(0).getStart();
			}
		}

		if (kp == -1) {
			Log.warning(Messages.noWordFoundToShow);
			return null;
		}

		return toTree(editor, kp, editor.getCorpus());
	}

	public Object concordancetoTree(ITXMResultEditor<? extends TXMResult> editor, ISelection s) {
		TXMResult r = editor.getResult();

		if (!(r instanceof Concordance)) {
			Log.warning(Messages.bind(Messages.ResultIsNotAConcordanceP0, r));
			return null;
		}

		if (!(s instanceof StructuredSelection)) {
			Log.warning(Messages.bind(Messages.SelectionIsNotALineP0, s));
			return null;
		}

		StructuredSelection selection = (StructuredSelection) s;
		List<?> list = selection.toList();

		if (list.size() == 0) {
			Log.warning(Messages.bind(Messages.NoEditorSelectionP0, r));
			return null;
		}

		Concordance concordance = (Concordance) r;
		Object o = list.get(0);
		if (!(o instanceof Line)) {
			Log.warning(Messages.bind(Messages.SelectionIsNotALineP0, o));
			return null;
		}
		Line l = (Line) o;
		//TreeSearchSelector selector = ComputeTreeSearch.getSelectorForCorpus(concordance.getCorpus(), "UD");

		int kp = l.getKeywordPosition();

		return toTree(editor, kp, concordance.getCorpus());
	}

	/**
	 * 
	 * @param editor the source editor
	 * @param kp the token position to focus
	 * @param corpus the parent corpus to use
	 * @return
	 */
	@SuppressWarnings("unused")
	public Object toTree(ITXMResultEditor<? extends TXMResult> editor, int kp, CQPCorpus corpus) {
		TXMResult r = editor.getResult();

		//TreeSearchSelector selector = ComputeTreeSearch.getSelectorForCorpus(concordance.getCorpus(), "UD");

		TreeSearchEditor leditor = editor.getLinkedEditor(TreeSearchEditor.class);
		TreeSearch ts = null;
		if (leditor != null) {
			ts = leditor.getResult();
			ts.setQuery(null);
		}
		else {
			ts = ComputeTreeSearch.getTreeSearchFor(corpus);
			if (ts == null) { // no search engine available
				return null;
			}
			ts.setQuery(null);
		}

		if (ts == null) {
			Log.warning(Messages.noSyntacticIndexFound);
			return null;
		}
		//ts.setQuery(concordance.getQuery());

		try {
			boolean c = ts.compute();

			//			System.out.println("KP="+kp);
			if (c) {
				Selection sel = ts.getSelection();
				if (sel != null) {
					int i = 0;
					List<? extends Match> matches = ts.getSelection().getMatches();

					for (Match m : matches) {
						if (m.getStart() <= kp && kp <= m.getEnd()) {
							//							System.out.println("set index "+(i+1));
							ts.setIndexAndSubIndex(i, 1);
							break;
						}
						i++;
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		if (leditor == null) {

			TreeSearchEditor openedEditor = ComputeTreeSearch.openEditor(ts);

			if (openedEditor != null) {
				editor.addLinkedEditor(openedEditor);

				int position = TreeSearchPreferences.getInstance().getInt(TreeSearchPreferences.BACKTOTREE_POSITION);
				if (position == -2 && editor instanceof TXMEditor) {
					Point p = ((TXMEditor) editor).getParent().getSize();
					if (p.x < p.y) {
						position = EModelService.ABOVE;
					}
					else {
						position = EModelService.RIGHT_OF;
					}
				}
				if (editor != null && position >= 0 && editor instanceof EditorPart) {
					SWTEditorsUtils.moveEditor((EditorPart) editor, openedEditor, position);
				}
			}
			return openedEditor;
		}
		else {
			try {
				leditor.refresh(true);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return leditor;
	}
}
