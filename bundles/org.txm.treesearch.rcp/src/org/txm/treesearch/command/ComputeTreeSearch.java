// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.treesearch.command;

import java.util.HashMap;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.treesearch.editor.TreeSearchEditor;
import org.txm.treesearch.function.TreeSearch;
import org.txm.treesearch.function.TreeSearchSelector;
import org.txm.treesearch.preferences.TreeSearchPreferences;
import org.txm.treesearch.rcp.Messages;
import org.txm.utils.logger.Log;


/**
 * Open the TIGERSearch Editor using the current selection OR a preference node set in the RCP command parameter TXMPreferences.RESULT_PARAMETERS_NODE_PATH
 * 
 * @author mdecorde.
 */
public class ComputeTreeSearch extends BaseAbstractHandler {

	/** The ID. */
	public static String ID = ComputeTreeSearch.class.getName();

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		Object s = null;

		// From link: creating from parameter node
		String parametersNodePath = event.getParameter(TXMPreferences.RESULT_PARAMETERS_NODE_PATH);
		if (parametersNodePath != null && !parametersNodePath.isEmpty()) {
			//s = TXMResult.getResult(parametersNodePath); // a TreeSearch or a CQPCorpus
			s = parametersNodePath;
		}
		else { // From view result node
			IStructuredSelection selection = this.getCorporaViewSelection(event);
			s = selection.getFirstElement();
		}

		return openEditor(s);
	}

	public static HashMap<String, TreeSearchSelector> getSelectorsForCorpus(CQPCorpus corpus) {

		HashMap<String, TreeSearchSelector> ret = new HashMap<String, TreeSearchSelector>();
		IConfigurationElement[] contributions = RegistryFactory.getRegistry().getConfigurationElementsFor("org.txm.treesearch.TreeSearchSelector"); //$NON-NLS-1$

		for (int i = 0; i < contributions.length; i++) {
			try {
				@SuppressWarnings("unchecked")
				TreeSearchSelector s = (TreeSearchSelector) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$

				Log.finest(TXMCoreMessages.bind("Initializing selector: {0}...", s)); //$NON-NLS-1$

				if (s.canComputeWith(corpus)) {
					ret.put(s.getEngine(), s);
				}
			}
			catch (Exception e) {
				Log.severe(Messages.bind(Messages.ErrorFailedToInstantiateP0, contributions[i].getName()));
				e.printStackTrace();
			}
		}

		return ret;
	}

	/**
	 * 
	 * @param corpus
	 * @return an available TreeSearch for a corpus (if any)
	 */
	public static TreeSearchSelector getSelectorForCorpus(CQPCorpus corpus, String defaultEngine) {

		TreeSearchSelector found = null;

		IConfigurationElement[] contributions = RegistryFactory.getRegistry().getConfigurationElementsFor("org.txm.treesearch.TreeSearchSelector"); //$NON-NLS-1$

		for (int i = 0; i < contributions.length; i++) {
			try {
				@SuppressWarnings("unchecked")
				TreeSearchSelector s = (TreeSearchSelector) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$

				Log.finest(TXMCoreMessages.bind("Initializing selector: {0}...", s)); //$NON-NLS-1$

				if (s.canComputeWith(corpus)) {
					if (s.getEngine().equals(defaultEngine) || defaultEngine == null) { // it is the default engine, return it !
						return s;
					}
					found = s; //Store 
				}
			}
			catch (Exception e) {
				Log.severe(Messages.bind(Messages.ErrorFailedToInstantiateP0, contributions[i].getName()));
				e.printStackTrace();
			}
		}

		return found;
	}

	public static TreeSearch getTreeSearchFor(CQPCorpus corpus) {

		Preferences customNode = corpus.getProject().getPreferencesScope().getNode(TreeSearchPreferences.getInstance().getPreferencesNodeQualifier());
		String defaultEngine = customNode.get(TreeSearchPreferences.DEFAULT_VISUALISATION, TreeSearchPreferences.getInstance().getString(TreeSearchPreferences.DEFAULT_VISUALISATION));

		TreeSearchSelector s = getSelectorForCorpus(corpus, defaultEngine);
		if (s == null) s = getSelectorForCorpus(corpus, null);
		if (s != null) return s.getTreeSearch(corpus);

		return null;
	}

	public static TreeSearchEditor openEditor(Object obj) {

		TreeSearch ts = null;

		if (obj instanceof CQPCorpus) {

			CQPCorpus corpus = (CQPCorpus) obj;
			ts = getTreeSearchFor(corpus);
		}
		else if (obj instanceof TreeSearch) {

			ts = (TreeSearch) obj;
		}
		else if (obj instanceof String) {

			//TXMResult.getResult((String)obj);
			Preferences node = TXMPreferences.getNode((String) obj);
			try {
				String[] keys = node.keys();
				System.out.println("keys: " + keys);
			}
			catch (BackingStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String parentPath = node.get(TXMPreferences.PARENT_PARAMETERS_NODE_PATH, null);
			TXMResult parent = TXMResult.getResult(parentPath);

			if (parent instanceof CQPCorpus) {
				CQPCorpus corpus = (CQPCorpus) parent;
				TreeSearchSelector s = getSelectorForCorpus(corpus, TreeSearchPreferences.getInstance().getString(TreeSearchPreferences.DEFAULT_VISUALISATION));
				ts = s.getTreeSearch((String) obj, corpus);
			}
		}
		else {
			Log.warning(Messages.bind(Messages.SelectionIsNotACorpusP0, obj));
			return null;
		}

		try {
			TXMEditor<?> editor = TreeSearchEditor.openEditor(ts, TreeSearchEditor.ID);
			if (editor instanceof TreeSearchEditor) {
				return (TreeSearchEditor) editor;
			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return null;
	}
}
