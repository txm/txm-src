package org.txm.treesearch.editor;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PartInitException;
import org.txm.Toolbox;
import org.txm.concordance.rcp.messages.ConcordanceUIMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.edition.rcp.editors.SynopticEditionEditor;
import org.txm.edition.rcp.handlers.OpenEdition;
import org.txm.edition.rcp.preferences.SynopticEditionPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.LargeQueryField;
import org.txm.rcp.swt.widget.NewNavigationWidget;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.QueriesView;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.treesearch.command.ComputeTreeSearch;
import org.txm.treesearch.function.TreeSearch;
import org.txm.treesearch.function.TreeSearchSelector;
import org.txm.treesearch.preferences.TreeSearchPreferences;
import org.txm.treesearch.rcp.Messages;
//import org.txm.stat.engine.r.RDevice;
import org.txm.utils.logger.Log;
//import org.txm.functions.queryindex.QueryIndex;


public class TreeSearchEditor extends TXMEditor<TreeSearch> {

	/** The Constant ID. */
	public static final String ID = TreeSearchEditor.class.getName();

	public Object source;

	public CQPCorpus corpus;

	File svgFile;

	Browser svgPanel;

	@Parameter(key = TXMPreferences.INDEX)
	NewNavigationWidget sentSpinner;

	@Parameter(key = TXMPreferences.SUB_INDEX)
	NewNavigationWidget subSpinner;

	Button okButton;

	private HashMap<String, TreeSearch> tsPerRepresentation = null;

	private TreeSearchSelector currentSelector = null;

	@Parameter(key = TXMPreferences.QUERY)
	private LargeQueryField queryArea;

	//	private Label subCounterLabel;
	//	
	//	private Label sentCounterLabel;

	@Parameter(key = TreeSearchPreferences.NTFEATURE)
	private org.eclipse.swt.widgets.List NTCombo;

	@Parameter(key = TreeSearchPreferences.TFEATURE)
	private org.eclipse.swt.widgets.List TCombo;

	private Combo representationCombo;

	private Label representationLabel;

	@Parameter(key = TreeSearchPreferences.VISUALISATION)
	private Combo visuCombo;

	private Label visuLabel;

	private Label queryAreaLabel;

	@Override
	public void _createPartControl() {

		getMainParametersComposite().getLayout().numColumns = 5;
		getMainParametersComposite().getLayout().makeColumnsEqualWidth = false;

		SelectionListener selChangedListener = new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				HashMap<String, TreeSearchSelector> selectors = ComputeTreeSearch.getSelectorsForCorpus(corpus);

				boolean updateFields = false;
				if (representationCombo.getText().equals("TIGER") && !currentSelector.getEngine().equals("TIGER")) { //$NON-NLS-1$ //$NON-NLS-2$
					currentSelector = selectors.get("TIGER"); //$NON-NLS-1$
					if (!tsPerRepresentation.containsKey("TIGER")) { //$NON-NLS-1$
						tsPerRepresentation.put("TIGER", currentSelector.getTreeSearch(corpus)); //$NON-NLS-1$
					}
					updateFields = true;
				}
				else if (representationCombo.getText().equals("UD") && !currentSelector.getEngine().equals("UD")) { //$NON-NLS-1$ //$NON-NLS-2$
					currentSelector = selectors.get("UD"); //$NON-NLS-1$
					if (!tsPerRepresentation.containsKey("UD")) { //$NON-NLS-1$
						tsPerRepresentation.put("UD", currentSelector.getTreeSearch(corpus)); //$NON-NLS-1$
					}
					updateFields = true;
				}

				if (updateFields) {

					TreeSearch currentTreeSearch = tsPerRepresentation.get(currentSelector.getEngine());

					queryArea.setQueryClass(currentSelector.getQueryClass());
					initializeFields();

					getEditorInput().setResult(tsPerRepresentation.get(currentSelector.getEngine()));
					representationLabel.setImage(IImageKeys.getImage(tsPerRepresentation.get(currentSelector.getEngine()).getIconPath()));

					// if (e.widget == sentSpinner && tsPerRepresentation.get(currentSelector.getEngine()).hasSubMatchesStrategy()) {
					//subSpinner.setSelection(0);
				}


				//subSpinner.setMaximum(tsPerRepresentation.get(currentSelector.getEngine()).getNSubMatch());
				//				subCounterLabel.setText("/" + tsPerRepresentation.get(currentSelector.getEngine()).getNSubGraph(sentSpinner.getSelection())); //$NON-NLS-1$
				// }

				TreeSearchEditor.this.compute(true);
				// reloadGraph();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		representationLabel = new Label(getMainParametersComposite(), SWT.NONE);
		representationLabel.setImage(IImageKeys.getImage(tsPerRepresentation.get(currentSelector.getEngine()).getIconPath()));
		representationLabel.setToolTipText(Messages.CurrentRepresentation);

		representationCombo = new Combo(getMainParametersComposite(), SWT.READ_ONLY);
		GridData gdataR = new GridData(SWT.FILL, SWT.CENTER, true, true);
		representationCombo.setLayoutData(gdataR);
		representationCombo.addSelectionListener(selChangedListener);

		// System.out.println(parent.getLayout());
		Composite queryPanel = this.getExtendedParametersGroup();

		// fill query Area
		GridLayout qlayout = new GridLayout(1, false);
		qlayout.horizontalSpacing = 0;
		// qlayout.verticalSpacing = 0;
		queryPanel.setLayout(qlayout);

		visuLabel = new Label(getMainParametersComposite(), SWT.NONE);
		visuLabel.setToolTipText(Messages.visualisationMode);
		visuLabel.setText(Messages.visualisation);

		visuCombo = new Combo(getMainParametersComposite(), SWT.READ_ONLY);
		GridData gdataV = new GridData(SWT.FILL, SWT.CENTER, false, false);
		visuCombo.setLayoutData(gdataV);
		visuCombo.addSelectionListener(selChangedListener);

		Button editionButton = new Button(getMainParametersComposite(), SWT.PUSH);
		editionButton.setImage(IImageKeys.getImage(IImageKeys.EDITION));
		editionButton.setToolTipText("Read the text of the current node");
		editionButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				readText();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// first line
		GLComposite line1 = new GLComposite(queryPanel, SWT.NONE, "line1"); //$NON-NLS-1$
		line1.getLayout().numColumns = 13;
		line1.getLayout().horizontalSpacing = 5;
		line1.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		// second line
		GLComposite line2 = new GLComposite(queryPanel, SWT.NONE, "line2"); //$NON-NLS-1$
		line2.getLayout().numColumns = 4;
		line2.getLayout().horizontalSpacing = 5;
		line2.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));

		Label l1 = new Label(line2, SWT.NONE);
		l1.setText("T "); //$NON-NLS-1$
		l1.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));

		TCombo = new org.eclipse.swt.widgets.List(line2, SWT.READ_ONLY | SWT.MULTI | SWT.V_SCROLL);
		GridData gdata = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gdata.heightHint = 100;
		TCombo.setLayoutData(gdata);
		//TCombo.addSelectionListener(selChangedListener);

		Label l2 = new Label(line2, SWT.NONE);
		l2.setText("NT "); //$NON-NLS-1$
		l2.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));

		NTCombo = new org.eclipse.swt.widgets.List(line2, SWT.READ_ONLY | SWT.MULTI | SWT.V_SCROLL);
		gdata = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gdata.heightHint = 100;
		NTCombo.setLayoutData(gdata);
		//NTCombo.addSelectionListener(selChangedListener);

		// bottom toolbar

		GLComposite navigationAreaComposite = getBottomToolbar().installGLComposite(ConcordanceUIMessages.navigation, 12, false);
		navigationAreaComposite.getLayout().verticalSpacing = 5;
		navigationAreaComposite.getLayout().horizontalSpacing = 5;
		navigationAreaComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		Button search = new Button(navigationAreaComposite, SWT.PUSH);
		search.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
		search.setToolTipText("Find matches of the query.");
		search.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				GridData queryAreaLayoutData = (GridData) queryArea.getLayoutData();
				if (queryArea.isVisible()) { // need to close
					search.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
					queryArea.setVisible(false);
					queryAreaLabel.setVisible(false);
					queryAreaLayoutData.heightHint = 0;
					queryAreaLayoutData.minimumHeight = 0;
					queryAreaLayoutData.grabExcessVerticalSpace = false;
					queryAreaLayoutData.grabExcessHorizontalSpace = false;
					queryAreaLayoutData.minimumWidth = 0;
				}
				else {
					search.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
					queryArea.setVisible(true);
					queryAreaLabel.setVisible(true);
					queryAreaLayoutData.grabExcessVerticalSpace = true;
					queryAreaLayoutData.grabExcessHorizontalSpace = true;
					if (currentSelector.getEngine().equals("UD")) { //$NON-NLS-1$
						queryAreaLayoutData.minimumHeight = 0;
						queryAreaLayoutData.heightHint = 00;
					}
					else {
						queryAreaLayoutData.minimumHeight = 80;
						queryAreaLayoutData.heightHint = 80;
					}
					queryAreaLayoutData.minimumWidth = 150;
				}
				queryArea.layout();
				navigationAreaComposite.getParent().getParent().layout();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		queryAreaLabel = new Label(navigationAreaComposite, SWT.NONE);
		queryAreaLabel.setText(TXMUIMessages.query); //$NON-NLS-1$
		queryAreaLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
		queryAreaLabel.setVisible(false);

		queryArea = new LargeQueryField(navigationAreaComposite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL, currentSelector.getQueryClass());
		GridData queryAreaLayoutData = new GridData(GridData.FILL, GridData.FILL, true, true);
		queryAreaLayoutData.horizontalSpan = 2;

		queryArea.setLayoutData(queryAreaLayoutData);
		queryArea.setVisible(false);

		queryArea.addListener(SWT.KeyDown, new Listener() {

			@Override
			public void handleEvent(Event e) {

				if (e.stateMask == SWT.CTRL) {
					if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
						e.doit = false;
						compute(false);
					}
				}
			}
		});

		search.setImage(IImageKeys.getImage(IImageKeys.ACTION_SEARCH));
		queryArea.setVisible(false);
		queryAreaLabel.setVisible(false);

		// fill param Area
		Label l3 = new Label(navigationAreaComposite, SWT.NONE);
		l3.setText("Sentence "); //$NON-NLS-1$
		gdata = new GridData(SWT.FILL, SWT.CENTER, false, false);
		l3.setLayoutData(gdata);

		sentSpinner = new NewNavigationWidget(navigationAreaComposite, SWT.NONE);
		sentSpinner.setMinimum(1);
		sentSpinner.setMaximum(10000000);
		sentSpinner.setSelection(0);
		sentSpinner.refresh();

		gdata = new GridData(SWT.FILL, SWT.CENTER, false, false);
		sentSpinner.setLayoutData(gdata);
		sentSpinner.addFirstListener(selChangedListener);
		sentSpinner.addPreviousListener(selChangedListener);
		sentSpinner.addNextListener(selChangedListener);
		sentSpinner.addLastListener(selChangedListener);


		//		sentCounterLabel = new Label(navigationAreaComposite, SWT.NONE);
		//		gdata = new GridData(SWT.FILL, SWT.CENTER, false, false);
		//		gdata.minimumWidth = 100;
		//		sentCounterLabel.setLayoutData(gdata);

		new Label(navigationAreaComposite, SWT.NONE);

		// if (tsPerRepresentation.get(currentSelector.getEngine()).hasSubMatchesStrategy()) {
		Label l4 = new Label(navigationAreaComposite, SWT.NONE);
		l4.setText("Match "); //$NON-NLS-1$
		gdata = new GridData(SWT.FILL, SWT.CENTER, false, false);
		l4.setLayoutData(gdata);

		subSpinner = new NewNavigationWidget(navigationAreaComposite, SWT.NONE);
		subSpinner.setMinimum(1);
		subSpinner.setMaximum(100000000);
		subSpinner.setSelection(0);
		gdata = new GridData(SWT.FILL, SWT.CENTER, false, false);
		subSpinner.setLayoutData(gdata);
		subSpinner.addFirstListener(selChangedListener);
		subSpinner.addPreviousListener(selChangedListener);
		subSpinner.addNextListener(selChangedListener);
		subSpinner.addLastListener(selChangedListener);
		subSpinner.refresh();

		//		subCounterLabel = new Label(navigationAreaComposite, SWT.NONE);
		//		gdata = new GridData(SWT.FILL, SWT.CENTER, false, false);
		//		subCounterLabel.setLayoutData(gdata);
		// }

		Composite mainPanel = this.getResultArea();
		svgPanel = new Browser(mainPanel, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		svgPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		svgPanel.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {

				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					if (e.count > 0) {
						TXMBrowserEditor.zoomIn(svgPanel);
					}
					else {
						TXMBrowserEditor.zoomOut(svgPanel);
					}
				}
			}
		});

		//		svgPanel.addMenuDetectListener(new MenuDetectListener() {
		//			
		//			@Override
		//			public void menuDetected(MenuDetectEvent e) {
		//				
		//				Menu menu = svgPanel.getMenu();
		//				System.out.println(menu);
		//				MenuItem editionItem = new MenuItem(menu, SWT.PUSH);
		//				editionItem.setText("Read text");
		//				editionItem.addSelectionListener(new SelectionListener() {
		//					
		//					@Override
		//					public void widgetSelected(SelectionEvent e) {
		//						readText();
		//					}
		//					
		//					@Override
		//					public void widgetDefaultSelected(SelectionEvent e) { }
		//				});
		//			}
		//		});


		// this.getTopToolbar().setVisible(COMPUTING_PARAMETERS_GROUP_ID, true);
		initializeFields();
	}

	protected void readText() {
		try {
			String[] currentMatch = TreeSearchEditor.this.getResult().getTextAndWordIDSOfCurrentSentence();
			if (currentMatch == null || currentMatch.length != 2 || currentMatch[0] == null || currentMatch[1] == null) {
				return;
			}
			String textId = currentMatch[0];
			String wordId = currentMatch[1];

			SynopticEditionEditor editor = OpenEdition.openEdition(corpus, null, textId, null, wordId);
			int position = SynopticEditionPreferences.getInstance().getInt(SynopticEditionPreferences.BACKTOTEXT_POSITION);
			if (position == -2) {
				Point s = editor.getParent().getSize();
				if (s.x < s.y) {
					position = EModelService.ABOVE;
				}
				else {
					position = EModelService.RIGHT_OF;
				}
			}
			if (editor != null && position >= 0) {
				SWTEditorsUtils.moveEditor(editor, TreeSearchEditor.this, position);
			}
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	protected void initializeFields() {
		// String id = corpus.getID();

		// if (ts.isReady()) {
		TreeSearch ts = tsPerRepresentation.get(currentSelector.getEngine());

		this.queryArea.setText(ts.getQuery());

		String[] tFeatures = ts.getAvailableTProperties();
		if (tFeatures.length > 0) {
			TCombo.setItems(tFeatures);
			//TCombo.setText(ts.getT());
			if (ts.getT() != null) {
				TCombo.setSelection(ts.getT().toArray(new String[ts.getT().size()]));
			}
		}

		String[] ntFeatures = ts.getAvailableNTProperties();
		if (ntFeatures.length > 0) {
			NTCombo.setItems(ntFeatures);
			//NTCombo.setText(ts.getNT());
			//NTCombo.setSelection(Arrays.binarySearch(ntFeatures, ts.getNT()));
			if (ts.getNT() != null) {
				NTCombo.setSelection(ts.getNT().toArray(new String[ts.getNT().size()]));
			}
			NTCombo.setEnabled(true);
		}
		else {
			NTCombo.removeAll();
			//NTCombo.setText("");
			NTCombo.setEnabled(false);
		}

		String[] visuFeatures = ts.getAvailableVisualisations();
		if (visuFeatures != null && visuFeatures.length > 0) {
			visuCombo.setItems(visuFeatures);
			visuCombo.setText(ts.getVisualisation());
			visuCombo.setEnabled(true);
			visuCombo.setVisible(true);
			visuLabel.setVisible(true);
		}
		else {
			visuCombo.removeAll();
			visuCombo.setText(""); //$NON-NLS-1$
			visuCombo.setEnabled(false);
			visuCombo.setVisible(false);
			visuLabel.setVisible(false);
		}

		if (representationCombo.getItemCount() == 0) {
			HashMap<String, TreeSearchSelector> selectors = ComputeTreeSearch.getSelectorsForCorpus(corpus);
			Log.fine(Messages.bind(Messages.SyntaxRepresentationP0, selectors));
			String[] values = selectors.keySet().toArray(new String[selectors.size()]);
			representationCombo.setItems(values);

			String defaultEngine = ts.getEngine();// TreeSearchPreferences.getInstance().getString(TreeSearchPreferences.DEFAULT_REPRESENTATION);

			representationCombo.select(Arrays.binarySearch(values, defaultEngine));

			if (representationCombo.getItemCount() == 1) {
				representationCombo.setVisible(false);
			}
		}

		if (ts.isComputed()) {
			queryArea.setText(ts.getQuery());

			//			sentCounterLabel.setText("/" + ts.getNSentences()); //$NON-NLS-1$
			//			if (ts.getNSentences() > 0) {
			//				subCounterLabel.setText("/" + ts.getNSubGraph(0)); //$NON-NLS-1$
			//			}
		}
		else {
			//			sentCounterLabel.setText(Messages.NoSentence);
			//			if (ts.hasSubMatchesStrategy()) subCounterLabel.setText(Messages.NoSubGraph);
		}

		if (!ts.canBeDrawn()) {
			sentSpinner.setSelection(ts.getCurrentMatchIndex());
			sentSpinner.setMaximum(ts.getNMatchingSentences());
			// if (ts.hasSubMatchesStrategy()) {
			subSpinner.setSelection(ts.getCurrentSubMatchIndex());
			subSpinner.setMaximum(ts.getNSubMatch());
			// }

			try {
				reloadGraph();
			}
			catch (Exception e) {
				Log.severe(Messages.bind(Messages.ErrorDuringTreeSearchInitializationP0, e));
				Log.printStackTrace(e);
			}
		}
		else {
			sentSpinner.setSelection(0);
			if (ts.hasSubMatchesStrategy()) subSpinner.setSelection(0);
			if (ts.isComputed()) {
				sentSpinner.setMaximum(ts.getNMatchingSentences());
				if (ts.hasSubMatchesStrategy()) subSpinner.setMaximum(ts.getNSubMatch());
			}
			else {
				sentSpinner.setMaximum(1);
				if (ts.hasSubMatchesStrategy()) subSpinner.setMaximum(1);
			}
		}
		// }
		// else {
		// System.out.println("Corpus '" + id + "' is not ready.");
		// return;
		// }

		//		sentCounterLabel.getParent().layout();
		getMainParametersComposite().getParent().layout();

	}

	/**
	 * 
	 * @return 1..N sentences
	 */
	public int getNSent() {

		return this.sentSpinner.getSelection();
	}

	public File getSVGFile() {

		return svgFile;
	}

	@Override
	public void updateResultFromEditor() {

		String query = this.queryArea.getText();
		this.queryArea.setText(query.trim());
	}

	@Override
	public void updateEditorFromResult(boolean update) {
		// if (update == false) {
		// //initializeFields();
		// }
		// final String query = queryArea.getText().trim();
		// if (query.length() == 0) {
		// System.out.println("No query, aborting.");
		// return;
		// }

		// JobHandler jobhandler = new JobHandler("", this.svgPanel.getParent()) {
		// @Override
		// protected IStatus run(IProgressMonitor monitor) {
		try {
			TreeSearch ts = tsPerRepresentation.get(currentSelector.getEngine());
			if (ts.getNMatchingSentences() > 0) {

				//				sentCounterLabel.setText("/" + ts.getNSentences()); //$NON-NLS-1$
				//				if (ts.hasSubMatchesStrategy()) subCounterLabel.setText("/" + ts.getNSubGraph(0)); //$NON-NLS-1$
				sentSpinner.setEnabled(true);
				subSpinner.setEnabled(true);
				sentSpinner.setSelection(ts.getCurrentMatchIndex());
				sentSpinner.setMaximum(ts.getNMatchingSentences());
				sentSpinner.refresh();
				subSpinner.setSelection(ts.getCurrentSubMatchIndex());
				subSpinner.setMaximum(ts.getNSubMatch());
				subSpinner.refresh();
				reloadGraph();
			}
			else {
				svgPanel.setText(""); //$NON-NLS-1$
				sentSpinner.setEnabled(false);
				subSpinner.setEnabled(false);
			}

			CorporaView.refresh();
			CorporaView.expand(corpus);
			QueriesView.refresh();
		}
		catch (Exception e) {
			Log.severe(Messages.bind(Messages.ErrorWhileComputingTheTreeP0, e));
			Log.printStackTrace(e);
		}
		// JobsTimer.stopAndPrint();
		// return Status.OK_STATUS;
		// }
		// };
		// jobhandler.startJob();
	}

	/**
	 * Reload graph.
	 */
	private boolean reloadGraph() {

		//		List<String> T = null;
		//		if (TCombo.getSelection().length() == 0) {
		//			T = new ArrayList<String>();
		//		} else {
		//			
		//		}
		List<String> T = Arrays.asList(TCombo.getSelection());
		List<String> NT = Arrays.asList(NTCombo.getSelection());
		int sent = sentSpinner.getSelection();
		int sub = 0;

		TreeSearch ts = tsPerRepresentation.get(currentSelector.getEngine());

		if (ts.hasSubMatchesStrategy()) sub = subSpinner.getSelection();

		ts.setIndexAndSubIndex(sent, sub);

		if (!ts.isComputed()) return false;

		try {
			if (svgFile != null && svgFile.exists()) svgFile.delete();

			File resultDir = new File(Toolbox.getTxmHomePath(), "results"); //$NON-NLS-1$
			resultDir.mkdirs();
			svgFile = this.getResult().createTemporaryFile(resultDir); // File.createTempFile("TreeSearch", ".svg", resultDir); //$NON-NLS-1$ //$NON-NLS-2$
			if (ts.toSVG(svgFile, sent, sub, T, NT)) {
				Log.finer("SVG file: " + svgFile); //$NON-NLS-1$
				svgPanel.setUrl(svgFile.toURI().toURL().toString());
				svgPanel.layout();
				svgPanel.redraw();
			}
			else {
				Log.warning(Messages.bind(Messages.ErrorFailToGenerateSVGFileP0, svgFile));
			}
		}
		catch (Exception e1) {
			Log.printStackTrace(e1);
			return false;
		}
		return true;
	}

	@Override
	public void _setFocus() {

		if (queryArea != null)
			queryArea.forceFocus();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void _init() throws PartInitException {


		File resultDir = new File(Toolbox.getTxmHomePath(), "results"); //$NON-NLS-1$
		resultDir.mkdirs();
		svgFile = this.getResult().createTemporaryFile(resultDir); // File.createTempFile("TreeSearch", ".svg", resultDir); //$NON-NLS-1$ //$NON-NLS-2$
		Log.fine(svgFile.toString());

		TXMResultEditorInput<TreeSearch> ii = (TXMResultEditorInput<TreeSearch>) getEditorInput();
		tsPerRepresentation = new HashMap<>();
		tsPerRepresentation.put(ii.getResult().getEngine(), ii.getResult());
		currentSelector = ii.getResult().getSelector();
		corpus = (CQPCorpus) ii.getResult().getCorpus();
	}
}
