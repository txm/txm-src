// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.treesearch.editor;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.txm.rcp.Application;
import org.txm.rcp.IImageKeys;
import org.txm.treesearch.function.TreeSearch;

/**
 * Simple editor input to query the TIGERSearch search engine
 *
 * @author mdecorde
 */
public class TreeSearchEditorInput implements IEditorInput {

	/** The ca. */
	private Object source;

	private TreeSearch ts;

	/**
	 * Instantiates a new correspondance analysis editor input.
	 *
	 * @param src the source
	 */
	public TreeSearchEditorInput(Object src, TreeSearch ts) {
		super();
		this.source = src;
		this.ts = ts;
	}

	/**
	 * Exists.
	 *
	 * @return true, if successful
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		return false;
	}

	/**
	 * Gets the image descriptor.
	 *
	 * @return the image descriptor
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		return AbstractUIPlugin.imageDescriptorFromPlugin(
				Application.PLUGIN_ID,
				IImageKeys.ACTION_WORDCLOUD);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		return "TreeSearch"; //$NON-NLS-1$
	}

	/**
	 * Gets the persistable.
	 *
	 * @return the persistable
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	/**
	 * Gets the tool tip text.
	 *
	 * @return the tool tip text
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		return getName();
	}

	/**
	 * Gets the adapter.
	 *
	 * @param arg0 the arg0
	 * @return the adapter
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	@SuppressWarnings("unchecked") //$NON-NLS-1$
	public Object getAdapter(Class arg0) {
		return null;
	}

	/**
	 * Gets the cA.
	 *
	 * @return the Correspondance analysis underlying object
	 */
	public Object getSource() {
		return source;
	}

	public TreeSearch getTreeSearch() {
		return ts;
	}
}
