package org.txm.treesearch.rcp;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.treesearch.rcp.messages"; //$NON-NLS-1$

	public static String CurrentRepresentation;

	public static String ErrorDuringTreeSearchInitializationP0;

	public static String ErrorFailedToInstantiateP0;

	public static String ErrorFailToGenerateSVGFileP0;

	public static String ErrorP0;

	public static String ErrorWhileComputingTheTreeP0;

	public static String NoEditorSelectionP0;

	public static String NoSentence;

	public static String NoSubGraph;

	public static String ResultIsNotAConcordanceP0;

	public static String SelectionIsNotACorpusP0;

	public static String SelectionIsNotALineP0;

	public static String SyntacticAnnotation;

	public static String SyntacticTree;

	public static String DefaultSyntacticVisualisation;

	public static String linkedEditorPosition;

	public static String noSyntacticIndexFound;

	public static String noWordFoundToShow;

	public static String SyntacticVisualisation;

	public static String SyntaxRepresentationP0;

	public static String visualisation;

	public static String visualisationMode;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
