package org.txm.treesearch.rcp.tester;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.treesearch.command.ComputeTreeSearch;

/**
 * PropertyTester to check if a corpus has TreeSearchTester indexes
 * 
 * @author mdecorde
 *
 */
public class TreeSearchTester extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "org.txm.rcp.testers"; //$NON-NLS-1$

	public static final String TREESEARCH_READY = "TreeSearchReady"; //$NON-NLS-1$

	public TreeSearchTester() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {

		if (!TREESEARCH_READY.equals(property)) return false;

		if (receiver == null) {
			return false;
		}

		if (receiver instanceof List) {
			List l = ((List) receiver);
			if (l.size() == 0) {
				return false;
			}

			receiver = l.get(0);
			if (!(receiver instanceof CQPCorpus)) return false;

			CQPCorpus corpus = (CQPCorpus) receiver;
			if (!(receiver instanceof CQPCorpus)) {
				return false;
			}

			if (corpus.getProjectDirectory() == null) return false;

			return ComputeTreeSearch.getSelectorForCorpus(corpus, null) != null; // any TreeSearch available ?
		}
		return false;
	}
}
