package org.txm.chartsengine.raster.rcp;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends TXMCoreMessages {

	private static final String BUNDLE_NAME = "org.txm.chartsengine.raster.rcp.messages"; //$NON-NLS-1$

	public static String CantLoadAChartWhichIsNotAFileP0;

	public static String LoadingRasterImageFromFileP0;
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
