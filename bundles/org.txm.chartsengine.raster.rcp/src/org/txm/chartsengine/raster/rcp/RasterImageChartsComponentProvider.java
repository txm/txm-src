package org.txm.chartsengine.raster.rcp;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.raster.rcp.swt.RasterComposite;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.swt.ChartComposite;
import org.txm.rcp.commands.OpenBrowser;
import org.txm.rcp.editors.TXMBrowserEditor;
import org.txm.rcp.utils.OpenURI;

/**
 * Raster image/SWT WebBrowserEditor charts component provider.
 * 
 * @author sjacquot
 *
 */
// FIXME : this class is for test purpose only
// FIXME : first of all : need to see if we still need these formats support in the RCP directly in the view (they will be available via export entry)
// FIXME : the PS and PDF should be moved to a new plugin ExternalChartComponentProvider that launches external system file viewer
public class RasterImageChartsComponentProvider extends SWTChartsComponentsProvider {


	/**
	 * 
	 */
	public RasterImageChartsComponentProvider() {
		super();
	}



	// @Override
	// public ChartEditorInput createChartEditorInput(ChartResult result) {
	// ChartCreator chartCreator = result.getChartCreator();
	// File file = chartCreator.createChartFile(result);
	// // FIXME : to temporary restore the PS and PDF formats : need to be recoded if we decide to keep the PDF and PS view else to remove if we decide to keep only the feature as export
	// if(this.chartsEngine.getOutputFormat() == ChartsEngine.OUTPUT_FORMAT_PS || this.chartsEngine.getOutputFormat() == ChartsEngine.OUTPUT_FORMAT_PDF) {
	// OpenURI.open(file);
	// return null;
	// }
	//
	// //return (ChartEditor) this.openFile(editorInputName, null, file);
	//
	// ChartEditorInput chartEditorInput = new ChartEditorInput(result);
	//
	//
	// /// FIXME: to restore layer
	// // this.createChartComponent(chartEditorInput);
	//
	// // sets and updates the local preferences node qualifier from the result data
	// //editor.getEditorInput().syncLocalPreferencesNode();
	//
	// return chartEditorInput;
	//
	// }

	// @Override
	// public boolean createChartComponent(ChartsEngine chartsEngine, ChartEditorInput chartEditorInput) {
	//
	// Object chart = chartEditorInput.getChart();
	//
	// if(chart != null) {
	// chartEditorInput.setChartComponent(chart);
	//
	// // sets and updates the local preferences node qualifier from the result data
	// chartEditorInput.syncLocalPreferencesNode();
	//
	// // FIXME: temporary solution
	// try {
	// chartEditorInput.getResult().compute();
	// }
	// catch(Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	//
	// return true;
	// }
	//
	//
	// return false;
	// }

	// public ChartEditor openEditor(ChartEditorInput chartEditorInput, String editorPartId) {
	//
	// ChartEditor chartEditor = ChartEditor.openEditor(null, editorPartId);
	//
	// // FIXME : to temporary restore the PS and PDF formats : need to be recoded if we decide to keep the PDF and PS view else to remove if we decide to keep only the feature as export
	// // directly close the opened editor after the external viewer is opened
	// // The best would be to create a plugin as dummyexternalviewer implementing SWT charts components extension?
	// if(this.chartsEngine.getOutputFormat() == ChartsEngine.OUTPUT_FORMAT_PS || this.chartsEngine.getOutputFormat() == ChartsEngine.OUTPUT_FORMAT_PDF) {
	// IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	// IWorkbenchPage page = window.getActivePage();
	// page.closeEditor(chartEditor, false);
	// }
	//
	// return chartEditor;
	// }

	@Override
	public ChartComposite createComposite(ChartEditor<? extends ChartResult> chartEditor, Composite parent) {
		RasterComposite composite = new RasterComposite(chartEditor, parent, SWT.NONE);

		// Link the composite to the chart editor (need to be done before initializing the AWT delegation events)
		// FIXME:
		// chartEditor.setChartComposite(composite);

		// FIXME : to temporary restore the PS and PDF formats : need to be recoded if we decide to keep the PDF and PS view else to remove if we decide to keep only the feature as export
		// The best would be to create a plugin as dummyexternalviewer implementing SWT charts components extension?
		// Opens an external viewer
		if (this.chartsEngine.getOutputFormat() == ChartsEngine.OUTPUT_FORMAT_PS || this.chartsEngine.getOutputFormat() == ChartsEngine.OUTPUT_FORMAT_PDF) {
			OpenURI.open((File) chartEditor.getResult().getChart());
		}
		// else {
		// // Load the chart in composite
		// composite.loadChart();
		// }

		return composite;
	}



	/**
	 * 
	 * @param file
	 * @param editorname
	 * @param titleImage
	 * @return
	 */
	// FIXME : first of all : need to see if we still need these formats support in the RCP directly in the view (they will be available via export entry)
	// FIXME : need to create a ChartEditor for the RasterImageChartsComponentProvider ? at least it would be used to stock the result (Progression, CA, Partition, etc.) but is it useful in this case
	// ?
	// And yes if we really still need these format PNG, JPG, the (ChartEditor) cast doesn't work for the CA chart
	// FIXME : need to globally check this class, it has been quickly developed for retrocompatiblity with PNG and JPEG formats
	public IEditorPart openFile(String editorname, Image titleImage, File file) {
		TXMBrowserEditor editor = OpenBrowser.openfile(file.getAbsolutePath(), editorname);
		editor.setTitleImage(titleImage);
		return editor;
	}


	// @Override
	// public ArrayList<String> getEditorSupportedExportFileFormats() {
	// // FIXME : no export from web browser PNG, JPEG, see if needed
	// return new ArrayList<String>();
	// }

	@Override
	public ArrayList<String> getChartsEngineSupportedExportFileFormats() {
		return this.chartsEngine.getSupportedOutputFileFormats();
	}



}
