package org.txm.chartsengine.raster.rcp.swt;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.raster.rcp.Messages;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.events.EventCallBackHandler;
import org.txm.chartsengine.rcp.swt.ChartComposite;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.logger.Log;


/**
 * Raster Image SWT composite.
 * 
 * @author sjacquot
 *
 */
public class RasterComposite extends ChartComposite {

	/**
	 * The loaded raster image file.
	 */
	protected File file;

	protected Image image;


	protected Label imageLabel;

	/**
	 * Creates a Raster image composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public RasterComposite(ChartEditor<? extends ChartResult> chartEditor, Composite parent, int style) {
		super(chartEditor, parent, style);

		// this.setLayout(new GridLayout(1, false));

		this.file = null;

		// ScrolledComposite sc = new ScrolledComposite( parent, SWT.H_SCROLL | SWT.V_SCROLL );
		// GridData layoutData = new GridData( GridData.FILL_HORIZONTAL );
		// layoutData.horizontalSpan = 2;
		// layoutData.heightHint = 400;
		// sc.setLayoutData( layoutData );
		//

		this.imageLabel = new Label(this, SWT.NONE);

		// FIXME: SJ: for debugging purpose
		//this.imageLabel.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));

		// this.imageLabel = new Label(this, SWT.NONE);
		// this.imageLabel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		// this.imageLabel.setText("Nothing loaded yet.");
		// // imgLabel.setImage( image );
		// this.imageLabel.setSize(this.imageLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		// sc.setContent( this.imageLabel );

		// Label l = new Label( shell, SWT.NONE );
		// l.setText( "Nom actuel :" );
		// layoutData = new GridData();
		// layoutData.verticalIndent = 20;
		// l.setLayoutData( layoutData );
		//
		// Text text = new Text( shell, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY );
		// text.setText( "The image name" );
		// layoutData = new GridData( GridData.FILL_HORIZONTAL );
		// layoutData.verticalIndent = 20;
		// text.setLayoutData( layoutData );


		// Frame frame = SWT_AWT.new_Frame(this);
		//
		// // Need to add an AWT Panel to fix mouse events and mouse cursors changes on Swing JPanel
		// Panel rootPanel = new Panel(new BorderLayout());
		// this.chartComponent = new SVGPanel(chartEditor, frame);
		// rootPanel.add(this.getPanel());
		// frame.add(rootPanel);
		// frame.pack();

		parent.layout();
	}


	@Override
	public IChartComponent _createChartComponent() {

		Object chart = this.chartEditor.getChart();

		if (chart == null) {
			// FIXME: temporary solution
			try {
				this.chartEditor.getResult().compute();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return (IChartComponent) chart;
		}

		return null;
	}



	@Override
	public void loadChart(Object chart) {
		if (chart instanceof File) {
			this.loadRasterFile((File) chart);
		}
		else {
			Log.warning(Messages.bind(Messages.CantLoadAChartWhichIsNotAFileP0, chart));
		}
	}

	@Override
	public void loadChart() {
		// this.file = (File) this.chartComponent;
		Point size = getSize();
		int w = size.x;
		int h = size.y;
		if (size.x != 0 && size.y != 0) {
			// the widget size might not be ready at this moment, see ChartEditor.loadChart() method
			// alternative solution is to wrap this in a Display.asyncExec call to be sure the size is ready
			w = 800;
			h = 600;
		}
		this.file = this.getChartsEngine().exportChartResultToFile(this.getChartResult(), w, h, this.getChartsEngine().getOutputFormat());
		this.loadRasterFile(this.file);
	}



	/**
	 * Loads a raster image from the specified file.
	 * 
	 * @param file the file to load
	 */
	public void loadRasterFile(File file) {

		Log.info(TXMCoreMessages.bind(Messages.LoadingRasterImageFromFileP0, file));

		this.file = file;
		if (this.image != null) {
			this.image.dispose();
		}

		ImageData imgData = new ImageData(file.getAbsolutePath());
		this.image = new Image(Display.getCurrent(), imgData);
		this.imageLabel.setImage(this.image);
		this.imageLabel.setSize(this.imageLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		// this.imageLabel.setSize(this.imageLabel.computeSize(this.getSize().x, this.getSize().y));

		// this.imageLabel.getParent().layout();
	}



	@Override
	public void copyChartViewToClipboard() {
		// TODO Auto-generated method stub
		System.err.println("RasterComposite.copyChartViewToClipboard(): not implemented."); //$NON-NLS-1$
	}



	@Override
	public EventCallBackHandler getMouseCallBackHandler() {
		// return super.getMouseCallBackHandler(this.getPanel());
		return null;
	}


	@Override
	public EventCallBackHandler getKeyCallBackHandler() {
		return null;
		// return super.getKeyCallBackHandler(this.getPanel());
	}



	@Override
	public void clearChartItemsSelection() {
		// TODO Auto-generated method stub
		System.err.println("RasterComposite.clearChartItemsSelection(): not implemented."); //$NON-NLS-1$
	}

	@Override
	public File exportView(File file, String fileFormat) {
		System.out.println("RasterComposite.exportView(): not yet implemented."); //$NON-NLS-1$
		return null;
	}


	@Override
	public ArrayList<String> getEditorSupportedExportFileFormats() {
		// FIXME : no export from web browser PNG, JPEG, see if needed
		return new ArrayList<>();
	}


	@Override
	public void initEventsListeners() {
		// TODO Auto-generated method stub

	}

}
