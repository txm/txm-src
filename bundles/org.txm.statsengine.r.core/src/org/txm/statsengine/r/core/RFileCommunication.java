// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-05-22 09:06:15 +0200 (Fri, 22 May 2015) $
// $LastChangedRevision: 2973 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.OSDetector;


// TODO: Auto-generated Javadoc
/**
 * The Class RFileCommunication.
 */
public class RFileCommunication {

	/** The transfertfile. */
	File transfertfile;

	/** The transfertfilepath. */
	String transfertfilepath;

	/**
	 * Instantiates a new r file communication.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public RFileCommunication() throws IOException {
		createTempFile();

	}

	/**
	 * Creates the temp file.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void createTempFile() throws IOException {
		transfertfile = File.createTempFile("Rtransfert", "file"); //$NON-NLS-1$ //$NON-NLS-2$
		//transfertfile = new File("~/Bureau/RTRANSFERT.txt");
		transfertfilepath = transfertfile.getAbsolutePath();
		if (OSDetector.isFamilyWindows()) //$NON-NLS-1$ //$NON-NLS-2$
		{
			//System.out.println("patch path");
			transfertfilepath = transfertfilepath.replace("\\", "\\\\"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		//System.out.println("Transfert file: "+transfertfilepath);
	}

	/**
	 * Assign.
	 *
	 * @param variablename the variablename
	 * @param matrix the matrix
	 * @param nrow the nrow
	 * @param ncol the ncol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void assign(String variablename, int[] matrix, int nrow, int ncol) throws RWorkspaceException {
		writeVector(matrix);
		RWorkspace.getRWorkspaceInstance().eval("source(\"" + transfertfilepath + "\");"); //$NON-NLS-1$ //$NON-NLS-2$
		RWorkspace.getRWorkspaceInstance().eval(variablename + " <- matrix(vectorint, nrow=" + nrow + ", ncol=" + ncol + ", byrow=T);"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * Assign.
	 *
	 * @param variablename the variablename
	 * @param matrix the matrix
	 * @param nrow the nrow
	 * @param ncol the ncol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void assign(String variablename, double[] matrix, int nrow, int ncol) throws RWorkspaceException {
		writeVector(matrix);
		RWorkspace.getRWorkspaceInstance().eval("source(\"" + transfertfilepath + "\");"); //$NON-NLS-1$ //$NON-NLS-2$
		RWorkspace.getRWorkspaceInstance().eval(variablename + " <- matrix(vectordouble, nrow=" + nrow + ", ncol=" + ncol + ", byrow=T);"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * Assign.
	 *
	 * @param variablename the variablename
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void assign(String variablename, int[] vector) throws RWorkspaceException {
		//System.out.println("assign vector "+variablename+" size "+vector.length);
		writeVector(vector);
		RWorkspace.getRWorkspaceInstance().eval("source(\"" + transfertfilepath + "\");"); //$NON-NLS-1$ //$NON-NLS-2$
		RWorkspace.getRWorkspaceInstance().eval(variablename + " <- vectorint;"); //$NON-NLS-1$
	}

	/**
	 * Assign.
	 *
	 * @param variablename the variablename
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void assign(String variablename, double[] vector) throws RWorkspaceException {
		//System.out.println("assign vector "+variablename+" size "+vector.length);
		writeVector(vector);
		RWorkspace.getRWorkspaceInstance().eval("source(\"" + transfertfilepath + "\");"); //$NON-NLS-1$ //$NON-NLS-2$
		RWorkspace.getRWorkspaceInstance().eval(variablename + " <- vectordouble;"); //$NON-NLS-1$

	}

	/**
	 * Assign.
	 *
	 * @param variablename the variablename
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void assign(String variablename, String[] vector) throws RWorkspaceException {
		//System.out.println("assign vector "+variablename+" size "+vector.length);
		writeVector(vector);
		RWorkspace.getRWorkspaceInstance().eval("source(\"" + transfertfilepath + "\");"); //$NON-NLS-1$ //$NON-NLS-2$
		RWorkspace.getRWorkspaceInstance().eval(variablename + " <- vectorstr;"); //$NON-NLS-1$
	}

	/*
	 * public boolean writeMatrix(int[][] matrix)
	 * {
	 * //System.out.println("WRITE MATRIX");
	 * try {
	 * BufferedWriter writer = new BufferedWriter(new FileWriter(transfertfilepath));
	 * for(int i = 0 ; i < matrix.length ; i++)
	 * {
	 * StringBuffer str = new StringBuffer();
	 * for(int j = 0 ; j < matrix[i].length ; j++)
	 * if (j == matrix[i].length -1)
	 * str.append(matrix[i][j]);
	 * else
	 * str.append(""+matrix[i][j]+"\t");
	 * writer.write(str.toString()+"\n");
	 * }
	 * writer.close();
	 * } catch (IOException e) {
	 * org.txm.utils.logger.Log.printStackTrace(e);
	 * return false;
	 * }
	 * return true;
	 * }
	 * public boolean writeMatrix(double[][] matrix)
	 * {
	 * //System.out.println("WRITE MATRIX");
	 * try {
	 * BufferedWriter writer = new BufferedWriter(new FileWriter(transfertfilepath));
	 * for(int i = 0 ; i < matrix.length ; i++)
	 * {
	 * StringBuffer str = new StringBuffer();
	 * for(int j = 0 ; j < matrix[i].length ; j++)
	 * if (j == matrix[i].length -1)
	 * str.append(matrix[i][j]);
	 * else
	 * str.append(""+matrix[i][j]+"\t");
	 * writer.write(str.toString()+"\n");
	 * }
	 * writer.close();
	 * } catch (IOException e) {
	 * org.txm.utils.logger.Log.printStackTrace(e);
	 * return false;
	 * }
	 * return true;
	 * }
	 */
	/**
	 * Write vector.
	 *
	 * @param vector the vector
	 * @return true, if successful
	 */
	private boolean writeVector(String[] vector) {
		//System.out.println("WRITE VECTOR");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(transfertfilepath));

			StringBuffer str = new StringBuffer();
			str.append("vectorstr <- c("); //$NON-NLS-1$
			for (int j = 0; j < vector.length; j++)
				if (j == vector.length - 1)
					str.append("\"" + vector[j] + "\""); //$NON-NLS-1$ //$NON-NLS-2$
				else
					str.append("\"" + vector[j] + "\", "); //$NON-NLS-1$ //$NON-NLS-2$
			str.append(");"); //$NON-NLS-1$
			writer.write(str.toString());

			writer.close();
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * Write vector.
	 *
	 * @param vector the vector
	 * @return true, if successful
	 */
	public boolean writeVector(int[] vector) {
		//System.out.println("WRITE VECTOR");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(transfertfilepath));

			StringBuffer str = new StringBuffer();
			str.append("vectorint <- as.integer(c("); //$NON-NLS-1$
			for (int j = 0; j < vector.length; j++)
				if (j == vector.length - 1)
					str.append(vector[j]);
				else
					str.append("" + vector[j] + ", "); //$NON-NLS-1$ //$NON-NLS-2$
			str.append("));"); //$NON-NLS-1$
			writer.write(str.toString());
			writer.close();
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * Write vector.
	 *
	 * @param vector the vector
	 * @return true, if successful
	 */
	public boolean writeVector(double[] vector) {
		//System.out.println("WRITE VECTOR");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(transfertfilepath));

			StringBuffer str = new StringBuffer();
			str.append("vectordouble <- c("); //$NON-NLS-1$
			for (int j = 0; j < vector.length; j++) {
				if (j == vector.length - 1)
					str.append(vector[j]);
				else
					str.append("" + vector[j] + ", "); //$NON-NLS-1$ //$NON-NLS-2$
			}
			str.append(");"); //$NON-NLS-1$
			writer.write(str.toString());
			writer.close();
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		double[][] matrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		RFileCommunication comm;
		try {
			comm = new RFileCommunication();
		}
		catch (IOException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return;
		}

		//comm.writeMatrix(matrix);
	}
}
