// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (Mon, 06 May 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core.rcolt;

import cern.colt.function.IntIntDoubleFunction;
import cern.colt.matrix.DoubleMatrix2D;

// TODO: Auto-generated Javadoc
/**
 * Method for transformation of colt matrix into various array.
 * 
 * @author sloiseau
 * 
 */
public final class RColt {

	/**
	 * Double matrix2 d2 int int array.
	 *
	 * @param matrix the matrix
	 * @return the int[][]
	 */
	public final static int[][] doubleMatrix2D2IntIntArray(DoubleMatrix2D matrix) {
		int rows = matrix.rows();
		int columns = matrix.columns();
		int[][] values = new int[rows][columns];
		for (int row = rows; --row >= 0;) {
			int[] currentRow = values[row];
			for (int column = columns; --column >= 0;) {
				currentRow[column] = (int) matrix.getQuick(row, column);
			}
		}
		return values;
		// final IntIntDoubleFunction fun = new CreateIntIntArray(matrix.rows(),
		// matrix.columns());
		// matrix.forEachNonZero(fun);
		// return ((CreateIntIntArray)fun).getIntIntArray();
	}

	/**
	 * Double matrix2 d2 double double array.
	 *
	 * @param matrix the matrix
	 * @return the double[][]
	 */
	public final static double[][] doubleMatrix2D2DoubleDoubleArray(
			DoubleMatrix2D matrix) {
		matrix.trimToSize();
		return matrix.toArray();
	}

	/*
	 * public final static int[][] intMatrix2D2IntIntArray (IntMatrix2D matrix)
	 * { matrix.trimToSize(); return matrix.toArray(); }
	 */

	/**
	 * Transform a Colt {@link DoubleMatrix2D} into an array of <code>int</code>
	 * .
	 * 
	 * The value of the matrix are stored by column (the values of the first
	 * column, then the values of the second column, etc.)
	 *
	 * @param matrix the matrix
	 * @return an array of <code>int</code>
	 *         //TODO use {@link DoubleMatrix2D#trimToSize()} and
	 *         {@link DoubleMatrix2D#toArray()}
	 */
	public final static int[] doubleMatrix2D2IntArray(DoubleMatrix2D matrix) {
		final IntIntDoubleFunction fun = new CreateIntArray(matrix.rows(),
				matrix.columns());
		matrix.forEachNonZero(fun);
		return ((CreateIntArray) fun).getIntArray();
	}

	// public final static REXP DoubleMatrix2D2REXP(DoubleMatrix2D matrix,
	// Rengine engine, String varName) {
	// final IntIntDoubleFunction fun = new CreateIntIntArray(matrix.rows(),
	// matrix.columns());
	// matrix.forEachNonZero(fun);
	// int[][] m = ((CreateIntIntArray)fun).getIntIntArray();
	// engine.assign(varName, m);
	// return ((CreateIntIntArray)fun).getIntIntArray();
	// }

	/**
	 * The Class CreateIntIntArray.
	 */
	static class CreateIntIntArray implements IntIntDoubleFunction {

		/** The array. */
		private final int[][] array;

		/**
		 * Instantiates a new creates the int int array.
		 *
		 * @param rows the rows
		 * @param columns the columns
		 */
		public CreateIntIntArray(int rows, int columns) {
			array = new int[rows][columns];
			for (int i = 0; i < array.length; i++) {
				for (int j = 0; j < array[i].length; j++) {
					array[i][j] = 0;
				}
			}
		}

		/*
		 * (non-Javadoc)
		 * @see cern.colt.function.IntIntDoubleFunction#apply(int, int, double)
		 */
		@Override
		public double apply(int row, int col, double value) {
			array[row][col] = (int) value;
			return value;
		}

		/**
		 * Gets the int int array.
		 *
		 * @return the int int array
		 */
		public int[][] getIntIntArray() {
			return array;
		}
	}

	/**
	 * The Class CreateIntArray.
	 */
	static class CreateIntArray implements IntIntDoubleFunction {

		/** The array. */
		private final int[] array;

		/** The rows. */
		private final int rows;

		/**
		 * Instantiates a new creates the int array.
		 *
		 * @param rows the rows
		 * @param columns the columns
		 */
		public CreateIntArray(int rows, int columns) {
			array = new int[rows * columns];
			for (int i = 0; i < array.length; i++) {
				array[i] = 0;
			}
			this.rows = rows;
		}

		/*
		 * (non-Javadoc)
		 * @see cern.colt.function.IntIntDoubleFunction#apply(int, int, double)
		 */
		@Override
		public double apply(int row, int col, double value) {
			array[(col * rows) + row] = (int) value;
			return value;
		}

		/**
		 * Gets the int array.
		 *
		 * @return the int array
		 */
		public int[] getIntArray() {
			return array;
		}
	}

}
