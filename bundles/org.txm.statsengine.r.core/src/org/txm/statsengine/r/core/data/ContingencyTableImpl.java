// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (Mon, 06 May 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core.data;

import org.rosuda.REngine.REXPMismatchException;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.ContingencyTable;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

import cern.colt.matrix.DoubleMatrix2D;

// TODO: Auto-generated Javadoc
/**
 * Implementation of the {@link ContingencyTable} interface, wrapping a R
 * matrix.
 * 
 * @author sloiseau
 */
public abstract class ContingencyTableImpl extends MatrixImpl implements ContingencyTable {

	/**
	 * Create a new contingency table after a {@link DoubleMatrix2D}.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public ContingencyTableImpl(DoubleMatrix2D matrix)
			throws RWorkspaceException {
		super(matrix);
	}

	/**
	 * Create a new contingency table after a {@link DoubleMatrix2D}, with row
	 * names and column names.
	 *
	 * @param matrix the matrix
	 * @param rowNames row names.
	 * @param columnNames the column names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public ContingencyTableImpl(DoubleMatrix2D matrix, String[] rowNames,
			String[] columnNames) throws RWorkspaceException {
		super(matrix, rowNames, columnNames);
	}

	/**
	 * Instantiates a new contingency table impl.
	 *
	 * @param matrix the matrix
	 * @param formNames the form names
	 * @param partNames the part names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public ContingencyTableImpl(int[][] matrix, String[] formNames,
			String[] partNames) throws RWorkspaceException {
		super(matrix, formNames, partNames);
	}

	/**
	 *
	 * @param symbol the name of the object
	 * @throws RWorkspaceException the r workspace exception
	 */
	public ContingencyTableImpl(String symbol) throws RWorkspaceException {
		super(symbol);
	}



	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.ContingencyTable#getColMargin()
	 */
	public Vector getColMargin() throws RWorkspaceException {
		String colName = createSymbole(Vector.class);
		rw.voidEval(colName + "<- colSums(" + this.symbol + ");"); //$NON-NLS-1$ //$NON-NLS-2$
		return new VectorImpl(colName);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.ContingencyTable#getRowMargin()
	 */
	@Override
	public Vector getRowMarginsVector() throws StatException {
		String colName = createSymbole(Vector.class);
		rw.voidEval(colName + "<- rowSums(" + this.symbol + ");"); //$NON-NLS-1$ //$NON-NLS-2$
		return new VectorImpl(colName);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.ContingencyTable#getRowMargin()
	 */
	@Override
	public Vector getColMarginsVector() throws StatException {
		String rowName = createSymbole(Vector.class);
		try {
			rw.voidEval(rowName + "<- colSums(" + this.symbol + ");"); //$NON-NLS-1$ //$NON-NLS-2$
			return new VectorImpl(rowName);
		}
		catch (RWorkspaceException e) {
			throw new StatException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.ContingencyTable#getTotal()
	 */
	@Override
	public int getTotal() throws RWorkspaceException {
		int total;
		try {
			total = rw.eval("sum(" + this.symbol + ");").asInteger(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (REXPMismatchException e) {
			throw new RWorkspaceException(e);
		}
		return total;
	}

	// /**
	// * Gets the col.
	// *
	// * @param index the index
	// *
	// * @return the col
	// */
	// public DoubleMatrix1D getCol(int index) {
	// if (index < 0 || index >= matrix.columns()) {
	// throw new IllegalArgumentException("Column index (" + index +
	// ") too hight (max: " + matrix.columns() + ")");
	// }
	// return matrix.viewColumn(index);
	// }
	//
	// /**
	// * Gets the sum.
	// *
	// * @return the sum
	// */
	// public int getSum() {
	// if (sum == -1) {
	// sum = (int) this.matrix.zSum();
	// }
	// return sum;
	// }
	//
	//	
	// /**
	// * Row margin.
	// *
	// * @return the sum over the rows
	// */
	// public DoubleMatrix1D getRowSum() {
	// if (rowSum==null){
	// DoubleMatrix1D ones = DoubleFactory1D.dense.make(matrix.rows(),1);
	// rowSum = matrix.viewDice().zMult(ones, null);
	// }
	// return rowSum;
	// }
	//	
	//
	//
	// /**
	// * Col margin.
	// *
	// * @return the sum over the columns
	// */
	// public DoubleMatrix1D getColSum() {
	// if (colSum==null){
	// DoubleMatrix1D ones = DoubleFactory1D.dense.make(matrix.columns(),1);
	// colSum = matrix.zMult(ones, null);
	// }
	// return colSum;
	// }

	/**
	 * Apply a four paramters-function on the whole table.
	 * 
	 * For each non zero value, the function will received four parameters:
	 * <ul>
	 * <li>The sum of the values in the matrix</li>
	 * <li>The sum of the row of the given value</li>
	 * <li>The sum of the column of the given value</li>
	 * <li>The value</li>
	 * </ul>
	 * 
	 * @param fun
	 *            the fun
	 * 
	 * @return the matrix
	 */
	// public Matrix apply(final AbstractMatrixIntIntIntDoubleFunction fun) {
	// cacheColSum();
	// cacheRowSum();
	// cacheSum();
	// return new Matrix(
	// matrix.copy().forEachNonZero(
	// new IntIntDoubleFunction() {
	// public double apply(int row, int col, double value) {
	// return fun.apply(sum, rowSum[row], colSum[col], value);
	// }
	// }
	// )
	// );
	// }

	/**
	 * Apply a function on the given row.
	 * 
	 * For each non zero value, the function will received four parameters:
	 * <ul>
	 * <li>The sum of the values in the matrix</li>
	 * <li>The sum of the row of the given value</li>
	 * <li>The sum of the column of the given value</li>
	 * <li>The value</li>
	 * </ul>
	 * 
	 * @param fun
	 *            the fun
	 * @param rowIndex
	 *            the row index
	 * 
	 * @return the variable
	 */
	// public NumericalVector applyOnRowWithMargin(final
	// AbstractMatrixIntIntIntDoubleFunction fun, int rowIndex) {
	// cacheColSum();
	// cacheRowSum();
	// cacheSum();
	// IntArrayList indexList = new IntArrayList();
	// DoubleArrayList valueList = new DoubleArrayList();
	// matrix.viewRow(rowIndex).getNonZeros(indexList, valueList);
	//
	// DoubleArrayList result = new DoubleArrayList(indexList.size());
	//
	// for (int i = 0; i < indexList.size(); i++) {
	// int column = indexList.getQuick(i);
	// double value = valueList.getQuick(i);
	// result.setQuick(i, fun.apply(sum, rowSum[rowIndex], colSum[column],
	// value));
	// }
	//
	// return new NumericalVector(result);
	// }

	/**
	 * Apply a function on the given column.
	 * 
	 * For each non zero value, the function will received four parameters:
	 * <ul>
	 * <li>The sum of the values in the matrix</li>
	 * <li>The sum of the row of the given value</li>
	 * <li>The sum of the column of the given value</li>
	 * <li>The value</li>
	 * </ul>
	 * 
	 * @param fun
	 *            the fun
	 * @param columnIndex
	 *            the column index
	 * 
	 * @return the variable
	 */
	// public NumericalVector applyOnColumnWithMargin(final
	// AbstractMatrixIntIntIntDoubleFunction fun, int columnIndex) {
	// cacheColSum();
	// cacheRowSum();
	// cacheSum();
	// IntArrayList indexList = new IntArrayList();
	// DoubleArrayList valueList = new DoubleArrayList();
	// matrix.viewColumn(columnIndex).getNonZeros(indexList, valueList);
	//
	// DoubleArrayList result = new DoubleArrayList(indexList.size());
	//
	// for (int i = 0; i < indexList.size(); i++) {
	// int row = indexList.getQuick(i);
	// double value = valueList.getQuick(i);
	// result.setQuick(i, fun.apply(sum, rowSum[row], colSum[columnIndex],
	// value));
	// }
	// return new NumericalVector(result);
	// }

	// /**
	// * For each non zero value in the matrix, get its row in the
	// * first list, its column in the second list, and its value
	// * in the third list.
	// *
	// * This is a strict delegation to {@link
	// DoubleMatrix2D#getNonZeros(IntArrayList, IntArrayList, DoubleArrayList)}.
	// *
	// * @param rowList
	// * @param columnList
	// * @param valueList
	// */
	// public void getNonZeros(IntArrayList rowList, IntArrayList columnList,
	// DoubleArrayList valueList) {
	// matrix.getNonZeros(rowList, columnList, valueList);
	// }

	/**
	 * For each non zero value in the matrix, get its row in the first list, its
	 * column in the second list, its value in the third list, the total of the
	 * column of the value in the fourth list, and the total of the row of the
	 * value in the firth list.
	 * 
	 * @param rowList
	 * @param columnList
	 * @param valueList
	 * @param columnMargin
	 * @param rowMargin
	 */
	// public void getNonZerosWithMargin(
	// IntArrayList rowList,
	// IntArrayList columnList,
	// DoubleArrayList valueList,
	// IntArrayList columnMargin,
	// IntArrayList rowMargin
	// ) {
	// I'm not sure to understand what this function does, but I'm pretty sure
	// it's buggy
	// cacheColSum();
	// cacheRowSum();
	// matrix.getNonZeros(rowList, columnList, valueList);
	// columnMargin.setSize(rowList.size());
	// rowMargin.setSize(rowList.size());
	// for (int i = 0; i < rowList.size(); i++) {
	// columnMargin.setQuick(i, colSum[columnList.getQuick(i)]);
	// rowMargin.setQuick(i, rowSum[rowList.getQuick(i)]);
	// }
	// }
}
