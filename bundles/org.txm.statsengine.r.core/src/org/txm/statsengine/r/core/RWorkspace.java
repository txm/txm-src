// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-06-15 15:29:16 +0200 (Mon, 15 Jun 2015) $
// $LastChangedRevision: 2989 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPLogical;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.txm.Toolbox;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.QuantitativeDataStructure;
import org.txm.statsengine.core.utils.VectorizeArray;
import org.txm.statsengine.r.core.exceptions.RException;
import org.txm.statsengine.r.core.exceptions.RObjectAlreadyExist;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.statsengine.r.core.messages.RCoreMessages;
import org.txm.statsengine.r.core.preferences.RPreferences;
import org.txm.statsengine.r.core.rcolt.RColt;
import org.txm.utils.OSDetector;
import org.txm.utils.StreamHog;
import org.txm.utils.logger.Log;
import org.txm.utils.network.SystemProxyDetector;

import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import cern.colt.matrix.DoubleMatrix2D;

/**
 * RWorkspace is the wrapper on top of the R Engine. All data sent to and
 * retrieve from R are using this class. One instance of this class must be
 * created in a program.
 * 
 * This class throw exception RWorkspace exception, encapsulating lower level
 * exception such as <code>RserveException</code>,
 * <code>REXPMismatchException</code> or <code>REngineException</code>.
 * 
 * This class offers many methods avoiding other classes in the toolbox to
 * contain R code. The goal is to concentrate R code, as much as possible, here.
 * See {@link #extractItemFromListByName(String, String)} for instance, and
 * other "extract..." methods.
 * 
 * methods in this class may be organized in the following group:
 * <ul>
 * <li>1/ initialization, connection to R, shutdown, etc.</li>
 * <li>2/ adding object to the R workspace (methods prefixed with "add").</li>
 * <li>3/ methods encapsulating R code for common fonctions: methods prefixed
 * with "assign...", "extract..." for instance.</li>
 * <li>4/ methods for evaluating R expression ({@link #safeEval(String)},
 * {@link #eval(String)}, {@link #evalToDouble(String)}, etc.)</li>
 * <li>5/ Static methods for converting result of evaluation in type (
 * {@link #toDouble(REXP)}, etc.)</li>
 * <li>6/ methods for checking existence of R object in the R workspace,
 * removing object from the R workspace, etc.
 * {@link #checkForDuplicateVariable(String)}, {@link #clearWorkspace()},
 * {@link #containsVariable(String)},
 * {@link #removeVariableFromWorkspace(String)}, etc.)</li>
 * <li>7/ methods for plotting.</li>
 * </ul>
 * 
 * TODO : assign/remove... in a textometrie environment, when Rengine.assign()
 * will be able to deal with R environment.
 * 
 * TODO : remove unused variables from workspace.
 * 
 * @author Sylvain Loiseau &lt;sloiseau@ens-lsh.fr&gt;
 * 
 */

public final class RWorkspace {

	/** The comm. */
	private static RFileCommunication comm;

	/** The connection. */
	private static RConnection connection = null;

	/** The eval logs. */
	static ArrayList<String> evalLogs = new ArrayList<>();

	/** The filecommunication. */
	private static boolean filecommunication = false;

	// --------------------------------------
	/** The last safeeval expr. */
	static String lastSafeevalExpr = ""; //$NON-NLS-1$

	/** The Rserve process. */
	private static Process RserveProcess = null;

	/** The workspace. */
	private static RWorkspace workspace = null;

	/** The error logger. */
	private StreamHog errorLogger;

	/** The input logger. */
	private StreamHog inputLogger;

	/** The loggin. */
	private boolean logging;

	/** The DEFAUL t_ symbol. */
	// private String DEFAULT_SYMBOL = "txmresult"; //$NON-NLS-1$

	/**
	 * Folder where to create the R working directory
	 */
	protected File userdir = null;

	/**
	 * protected on purpose. See {@link #getRWorkspaceInstance()}.
	 *
	 * @throws RWorkspaceException the r workspace exception
	 */
	private RWorkspace(File userDir) throws RWorkspaceException {
		this.userdir = userDir;
		this.setLog(isLoggingEvalCommandLines());
	}

	/**
	 * Append to eval log.
	 *
	 * @param cmd the cmd
	 */
	public static void appendtoEvalLog(String cmd) {
		evalLogs.add(cmd);
	}

	/**
	 * Close the connection.
	 *
	 * @throws RWorkspaceException the r workspace exception
	 */
	public static final void closeConnection() throws RWorkspaceException {
		// If no R object have been created during the lif ecycle of the
		// application,
		// the connection has never been initialized.
		if (connection != null) {
			connection.close();
		}
	}

	/**
	 * Connect to Rserve.
	 *
	 * @param host the host
	 * @param port the port
	 * @return true if success
	 * @throws RWorkspaceException the r workspace exception
	 */
	public static boolean connect(String host, int port) throws RWorkspaceException {
		try {
			Log.fine(RCoreMessages.bind(RCoreMessages.info_connectingToRAtP0P1, host, port));
			connection = new RConnection(host, port);
			connection.setStringEncoding("utf8"); // mandatory ! //$NON-NLS-1$
			return isConnected();
		}
		catch (RserveException x) {
			throw new RWorkspaceException(x);
		}

	}

	/**
	 * Connect to Rserve.
	 *
	 * @param host the host
	 * @param port the port
	 * @param user the user
	 * @param password the password
	 * @return true, if successful
	 */
	public static boolean connect(String host, int port, String user, String password) {
		try {
			Log.fine(TXMCoreMessages.bind(RCoreMessages.info_connectingToRAtP0P1WithUserP2, new Object[] { host, port, user }));
			connection = new RConnection(host, port);
			connection.login(user, password);
			connection.setStringEncoding("utf8"); // mandatory ! //$NON-NLS-1$
			return isConnected();

		}
		catch (RserveException x) {
			Log.severe(TXMCoreMessages.bind(RCoreMessages.error_failedToConnectToTheRWorkspaceP0AtP1WithUserP2, host, port, user));
		}
		return false;
	}

	/**
	 * Gets the last safe eval expr.
	 *
	 * @return the last safe eval expr
	 */
	public static String getLastSafeEvalExpr() {
		return lastSafeevalExpr;
	}


	/**
	 * 
	 * @return
	 */
	public static String getRandomSymbol() {
		return "Random" + UUID.randomUUID(); //$NON-NLS-1$ ;
	}

	/**
	 * Entering the R workspace.
	 *
	 * @return the r workspace instance
	 * @throws RWorkspaceException the r workspace exception
	 */
	public static final RWorkspace getRWorkspaceInstance() throws RWorkspaceException {
		if (workspace == null) {
			// TODO: this method uses the right directory ?
			// Bundle bundle = FrameworkUtil.getBundle(RWorkspace.class);
			// File userdir = new File(bundle.getLocation());
			File userDir = new File(Toolbox.getTxmHomePath(), "R"); //$NON-NLS-1$
			// System.out.println("BUNDLE LOCATION OF '" + bundle.getSymbolicName() + "': " + userDir);
			workspace = new RWorkspace(userDir);
		}
		return workspace;
	}

	/**
	 * Post initialisation of Rserve : loads libraries.
	 * 
	 * @deprecated The extensions will load themself the packages they need
	 * @throws RWorkspaceException the r workspace exception
	 */
	@Deprecated
	private static void init() throws RWorkspaceException {
		String[] packagesToLoad = {};
		for (int i = 0; i < packagesToLoad.length; i++) {
			String name = packagesToLoad[i];
			try {
				// System.out.println("load R lib : " + name);
				connection.eval(("library(" + name + ")")); //$NON-NLS-1$ //$NON-NLS-2$
				// System.out.println("Done : load R lib : " + name);
			}
			catch (RserveException e) {
				throw new RWorkspaceException(NLS.bind(RCoreMessages.error_failedToLoadTheP0LibraryP1, name, e));
			}
			catch (Exception e) {
				org.txm.utils.logger.Log.printStackTrace(e);
			}
		}
	}


	// --------------------------------------
	/**
	 * check R path
	 * check if Rserve has already been launched
	 * if not try to launch it.
	 *
	 * @param pathToRExecutable the path to r executable
	 * @param rServeArgs
	 * @return true if Rserve is launched
	 * @throws RWorkspaceException the r workspace exception
	 */
	private static boolean initRserve(String pathToRExecutable, int port, boolean debug, String rargs, String rServeArgs)
			throws RWorkspaceException {
		boolean isRServerOk;

		Log.fine(TXMCoreMessages.bind("RWorkspace.initRserve(): checking file existence of {0}...", pathToRExecutable)); //$NON-NLS-1$

		File execFile = new File(pathToRExecutable);
		if (pathToRExecutable == null || pathToRExecutable.trim().length() == 0) {
			throw new RWorkspaceException(RCoreMessages.error_rservePathNotSet);
		}
		else if (!execFile.exists()) {
			throw new RWorkspaceException(RCoreMessages.bind(RCoreMessages.error_cantFindRServeInTheP0Path, pathToRExecutable));
		}
		else if (!execFile.isFile()) {
			throw new RWorkspaceException(RCoreMessages.bind(RCoreMessages.error_P0RservePathNotPointingAFile, pathToRExecutable));
		}
		else if (!execFile.canExecute()) {
			throw new RWorkspaceException(RCoreMessages.bind(RCoreMessages.error_cantExecuteTheP0RServeFile, pathToRExecutable));
		}
		else {
			isRServerOk = StartRserve.launchRserve(pathToRExecutable, port, debug, rargs, rServeArgs);
			// System.out.println("ap launchRserve");
			if (!isRServerOk) {
				throw new RWorkspaceException(NLS.bind(RCoreMessages.rserveNotStartedUsingTheP0Path, pathToRExecutable));
				// isRServerOk = StartRserve.checkLocalRserve(port, debug, rargs, rServeArgs);
			}
		}

		if (isRServerOk) {
			Log.finest("RSERVE_ACTIVATED"); //$NON-NLS-1$
			// System.out.println(RCoreMessages.info_connected);
			RserveProcess = StartRserve.rProcess;
		}
		return isRServerOk;
	}

	/**
	 * Checks if is connected.
	 *
	 * @return true if the RWorkspace is connected to RServe
	 */
	private static boolean isConnected() {
		if (!connection.isConnected()) {
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * 
	 * @return The executable path set in the R Preferences
	 */
	public static String getExecutablePath() {
		return RPreferences.getInstance().getString(RPreferences.PATH_TO_EXECUTABLE);
	}

	/**
	 * Checks if is file tranfert.
	 *
	 * @return true, if is file tranfert
	 */
	public static boolean isFileTranfert() {
		return filecommunication;
	}



	/**
	 * Load eval log.
	 *
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void loadEvalLog(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String cmd = reader.readLine();
		while (cmd != null) {
			evalLogs.add(cmd);
			cmd = reader.readLine();
		}
		reader.close();
	}

	/**
	 * Plot in a SVG file.
	 *
	 * @param file the file
	 * @param expr the expr
	 * @throws REXPMismatchException the rEXP mismatch exception
	 * @throws StatException the stat exception
	 */
	public File plot(File file, String expr) throws REXPMismatchException, StatException, RserveException {
		return plot(file, expr, "svg"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @param file to file to process
	 * @return a valid file path for R 
	 */
	public static String fileToRPath(File file) {

		if (OSDetector.isFamilyWindows()) {
			try {
//				return file.getCanonicalPath().replaceAll("\\\\", "/");
				return StringEscapeUtils.escapeJava(file.getCanonicalPath());
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //$NON-NLS-1$ //$NON-NLS-2$
		}
		return file.getAbsolutePath();
	}
	
	/**
	 * Plot.
	 *
	 * @param file the file
	 * @param expr the expr
	 * @param devicename the device (svg, png, jpeg, ...)
	 * @throws REXPMismatchException the rEXP mismatch exception
	 * @throws StatException the stat exception
	 * @throws RserveException
	 */
	public File plot(File file, String expr, String devicename) throws REXPMismatchException, StatException, RserveException {

		try {
			file.createNewFile();

			if (!file.canWrite()) {
				Log.severe(NLS.bind(TXMCoreMessages.error_errorColonCanNotWriteInFileP0, file)); //$NON-NLS-1$
				return null;
			}

			String path = fileToRPath(file);

			if (devicename.equals("devSVG")) safeEval("library(RSvgDevice);"); //$NON-NLS-1$ //$NON-NLS-2$

			// FIXME : test for window dimensions so the large plots can be entirely displayed. Need to pass width and height to RWorkspace.plot() so
			// RChartsEngine will be able to dynamically compute the width from the number of bars in a barplot for example.
			// REXP xp = eval("try("+devicename+"(\"" + name + "\", width=20, height=10))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			REXP xp = eval("try(" + devicename + "(\"" + path + "\"))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			if (xp.isString() && xp.asString() != null) { // if there's a string
				// then we have a
				// problem, R sent an
				// error
				// System.out.println("Can't open svg graphics device:\n"+xp.asString());
				// this is analogous to 'warnings', but for us it's sufficient to
				// get just the 1st warning
				REXP w = eval("if (exists(\"last.warning\") && length(last.warning)>0) names(last.warning)[1] else 0"); //$NON-NLS-1$
				// if (w.asString()!=null) System.out.println(w.asString());
				throw new StatException(w.asString());
			}

			// ok, so the device should be fine - let's plot
			safeEval(expr);
			safeEval("dev.off()"); //$NON-NLS-1$

			return file;
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Prints the last safe eval expr.
	 */
	public static void printLastSafeEvalExpr() {
		Log.fine(RCoreMessages.bind(RCoreMessages.info_lastSafeEvalExpressionP0, lastSafeevalExpr));
	}

	/**
	 * Save eval log.
	 *
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void saveEvalLog(File file) throws IOException {
		FileWriter writer = new FileWriter(file);
		for (String cmd : evalLogs)
			writer.write(cmd + "\n"); //$NON-NLS-1$
		writer.flush();
		writer.close();
	}

	/**
	 * if state is true, the matrix will be send to R with a File.
	 *
	 * @param state the state
	 * @return true if success
	 */
	public static boolean setUseFileCommunication(boolean state) {
		filecommunication = state;
		if (filecommunication) {
			try {
				comm = new RFileCommunication();
			}
			catch (IOException e) {
				filecommunication = false;
				Log.severe(RCoreMessages.bind(RCoreMessages.error_failedToInitializeFileTransfertP0, Log.toString(e)));
				return false;
			}
		}
		return true;
	}

	/**
	 * Close the connection and destroy the Rserve process.
	 *
	 * @throws RWorkspaceException the r workspace exception
	 */
	public static final void shutdown() throws RWorkspaceException {
		try {
			// If no R object have been created during the lif ecycle of the
			// application,
			// the connection has never been initialized.

			if (connection != null && connection.isConnected()) {
				// connection.close(); // No: invoquing "close()" make
				// "shutdown()" to throw a "not connected" exception.
				connection.shutdown();
				// TODO test if Rserve connection is actually closed connection.isConnected();
				connection.close();
				connection = null; // warning, after shuting down the
				// server, the connection appears still
				// ok but using it yield to a
				// "Broken pipe" exception.
			}
		}
		catch (RserveException e) {
			throw new RWorkspaceException(e);
		}
	}

	/**
	 * Start exec.
	 *
	 * @param pathToRExecutable the path to r executable
	 * @return true, if successful
	 */
	public static boolean startExec(String pathToRExecutable, int port, boolean debug, String rArgs, String rServeArgs) {
		// System.out.println("Start Exec");
		try {
			return initRserve(pathToRExecutable, port, debug, rArgs, rServeArgs);
		}
		catch (RWorkspaceException e) {
			Log.severe(RCoreMessages.error_failedToStartRServe);
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}

	/**
	 * To double.
	 *
	 * @param rexp the rexp
	 * @return the double[]
	 * @throws RWorkspaceException the r workspace exception
	 */
	public static final double[] toDouble(REXP rexp) throws RWorkspaceException {
		try {
			return rexp.asDoubles();
		}
		catch (Exception e) {
			throw new RWorkspaceException(e);
		}
	}



	/**
	 * Add a matrix into the workspace and link it to a name. The inner arrays
	 * of the <code>matrix</code> parameters are the <strong>row</strong> of the
	 * resulting R matrix.
	 *
	 * @param variableName the name to be used in the R workspace.
	 * @param matrix the data to be bound to the name. In the form
	 *            <code>matrix[row][column]</code>, with exactly the same number
	 *            of column in every row.
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addMatrixToWorkspace(String variableName, double[][] matrix) throws RWorkspaceException {
		// System.out.println("matrix len: "+matrix.length+" test="+(matrix.length == 0));
		if (matrix.length == 0) {
			new RWorkspaceException(RCoreMessages.error_matrixIsEmpty);
		}
		int ncol = matrix[0].length;
		int nrow = matrix.length;

		addMatrixToWorkspace(variableName, matrix, ncol, nrow);
	}

	/**
	 * Add a matrix into the workspace and link it to a name. The inner arrays
	 * of the <code>matrix</code> parameters are the <strong>row</strong> of the
	 * resulting R matrix.
	 *
	 * @param variableName the name to be used in the R workspace.
	 * @param matrix the data to be bound to the name. In the form
	 *            <code>matrix[row][column]</code>, with exactly the same number
	 *            of column in every row.
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addMatrixToWorkspace(String variableName, double[][] matrix, int ncol, int nrow)
			throws RWorkspaceException {

		double[] vector = VectorizeArray.vectorizeByInner(matrix);

		if (!filecommunication) {
			try {
				connection.assign(variableName, vector);
			}
			catch (REngineException e) {
				throw new RWorkspaceException(e);
			}
			try {
				connection.voidEval(variableName
						+ "<- matrix(" + variableName + ", nrow=" + nrow + ", ncol=" + ncol + ", byrow=T)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			catch (RserveException e) {
				throw new RWorkspaceException(e);
			}
		}
		else {
			comm.assign(variableName, vector, nrow, ncol);
		}
		Log.finest("MATRIX_ADDED_TO_WORKSPACE" + new Object[] { nrow, ncol, variableName }); //$NON-NLS-1$
	}

	/**
	 * Add a matrix into the workspace and link it to a name. Intended to be
	 * efficient for sparse matrix.
	 *
	 * @param variableName the name to be used in the R workspace.
	 * @param matrix the data to be bound to the name.
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addMatrixToWorkspace(String variableName, DoubleMatrix2D matrix)
			throws RWorkspaceException {

		double[][] data = RColt.doubleMatrix2D2DoubleDoubleArray(matrix);
		addMatrixToWorkspace(variableName, data, matrix.columns(), matrix.rows());

		// // IntArrayList x = new IntArrayList();
		// // IntArrayList y = new IntArrayList();
		// // DoubleArrayList val = new DoubleArrayList();
		// //
		// // matrix.trimToSize();
		// // matrix.getNonZeros(x, y, val);
		// // x.trimToSize();
		// // y.trimToSize();
		// // val.trimToSize();
		// //
		// // for (int i = 0; i < x.size(); i++) {
		// // x.setQuick(i, x.getQuick(i) + 1);
		// // }
		// //
		// // for (int i = 0; i < y.size(); i++) {
		// // y.setQuick(i, y.getQuick(i) + 1);
		// // }
		// //
		// // System.out.println("colt");
		// // System.out.println(x);
		// // System.out.println(y);
		// // System.out.println(val);
		//
		// int nrow = matrix.rows();
		// int ncol = matrix.columns();
		// // System.out.println(matrix);
		// // System.out.println(Arrays.toString(vald));
		// try {
		// // connection.eval(variableName + "<- matrix(0, nrow=" + nrow +
		// ", ncol=" + ncol + " );");
		// connection.assign(variableName, m);
		// } catch (RserveException e) {
		// throw new RWorkspaceException(e);
		// }
		// try {
		// connection.assign(variableName + ".nonzero", val.elements());
		// connection.assign(variableName + ".x", x.elements());
		// connection.assign(variableName + ".y", y.elements());
		// } catch (REngineException e) {
		// throw new RWorkspaceException(e);
		// }
		// try {
		// // connection.voidEval("dim(" + variableName + ") <- c(" + nrow +
		// ", " + ncol + ")");
		// // connection.voidEval(variableName + "[" + variableName + ".x, " +
		// variableName + ".y" + " ] <- " + variableName + ".nonzero");
		// // for (int i = 0; i < x.size(); i++) {
		// // for (int j = 0; j < y.size(); j++) {
		// // connection.voidEval(variableName + "[" + x.getQuick(i) + ", " +
		// y.getQuick(j) + "] <- " + val.getQuick(i * 5 + * j));
		// // }
		// // }
		//
		// } catch (RserveException e) {
		// throw new RWorkspaceException(e);
		// }
		//
		// removeVariableFromWorkspace(variableName + ".nonzero");
		// removeVariableFromWorkspace(variableName + ".x");
		// removeVariableFromWorkspace(variableName + ".y");
		//
		//
		// System.out.println("--------------matrice d'origine----------");
		// System.out.println(matrix);
		//
		// // DoubleFactory2D.dense.make(arg0)
		// System.out.println("--------------matrice issue de R----------");
		// DenseDoubleMatrix2D m = null;
		// try {
		// m = new
		// DenseDoubleMatrix2D(connection.eval(variableName).asDoubleMatrix());
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// org.txm.utils.logger.Log.printStackTrace(e);
		// } catch (RserveException e) {
		// // TODO Auto-generated catch block
		// org.txm.utils.logger.Log.printStackTrace(e);
		// }
		// System.out.println(m);

	}

	// TODO: is addLexiconToWorkspace used ?
	// /**
	// * Adds the lexicon to workspace.
	// *
	// * @param variableName the variable name
	// * @param lexicon the lexicon
	// * @throws RWorkspaceException the r workspace exception
	// */
	// public void addLexiconToWorkspace(String variableName, Lexicon lexicon)
	// throws RWorkspaceException {
	// int[] freqs = lexicon.getFreq();
	// String[] forms = lexicon.getForms();
	// try {
	// addVectorToWorkspace(variableName, freqs);
	// addVectorToWorkspace(variableName + "_names", forms); //$NON-NLS-1$
	// connection
	// .voidEval("names(" + variableName + ") <- " + variableName + "_names"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	// connection.voidEval("rm(" + variableName + "_names" + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	// } catch (RserveException e) {
	// throw new RWorkspaceException(e);
	// }
	// Log.finest("LEXICON_ADDED" + new Object[] { lexicon.nbrOfType() + variableName }); //$NON-NLS-1$
	// }

	/**
	 * Add a matrix into the workspace and link it to a name. The inner arrays
	 * of the <code>matrix</code> parameters are the <strong>row</strong> of the
	 * resulting R matrix.
	 *
	 * @param variableName the name to be used in the R workspace.
	 * @param matrix the data to be bound to the name. In the form
	 *            <code>matrix[row][column]</code>, with exactly the same number
	 *            of column in every row.
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addMatrixToWorkspace(String variableName, int[][] matrix) throws RWorkspaceException {
		int ncol = 0;
		int nrow = 0;
		if (matrix.length > 0) {
			nrow = matrix.length;
			ncol = matrix[0].length;
		}

		addMatrixToWorkspace(variableName, matrix, ncol, nrow);
	}

	/**
	 * Add a matrix into the workspace and link it to a name. The inner arrays
	 * of the <code>matrix</code> parameters are the <strong>row</strong> of the
	 * resulting R matrix.
	 *
	 * @param variableName the name to be used in the R workspace.
	 * @param matrix the data to be bound to the name. In the form
	 *            <code>matrix[row][column]</code>, with exactly the same number
	 *            of column in every row.
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addMatrixToWorkspace(String variableName, int[][] matrix, int ncol, int nrow) throws RWorkspaceException {

		int[] vector = VectorizeArray.vectorizeByInner(matrix);
		if (!filecommunication) {
			try {
				connection.assign(variableName, vector);
			}
			catch (REngineException e) {
				throw new RWorkspaceException(e);
			}
			try {
				String command = variableName + "<- matrix(" + variableName + ", nrow=" + nrow + ", ncol=" + ncol + ", byrow=T)"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

				Log.finest("MATRIX_ADDED_TO_WORKSPACE: " + command); //$NON-NLS-1$

				connection.voidEval(command);
			}
			catch (RserveException e) {
				throw new RWorkspaceException(e);
			}
		}
		else {
			comm.assign(variableName, vector, nrow, ncol);
		}
		Log.finest("MATRIX_ADDED_TO_WORKSPACE" + new Object[] { nrow, ncol, variableName }); //$NON-NLS-1$
	}

	/**
	 * Adds the vector to workspace.
	 *
	 * @param variableName the variable name
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addVectorToWorkspace(String variableName, double[] vector) throws RWorkspaceException {
		if (filecommunication) {
			comm.assign(variableName, vector);
		}
		else {
			try {
				connection.assign(variableName, vector);
			}
			catch (REngineException e) {
				throw new RWorkspaceException(e);
			}
		}

		Log.finest("DOUBLE_VECTOR_ADDED_TO_WORKSPACE" + new Object[] { vector.length, variableName }); //$NON-NLS-1$
	}

	/**
	 * Add a vector into the workspace and link it to a name.
	 *
	 * @param variableName the variable name
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addVectorToWorkspace(String variableName, DoubleArrayList vector) throws RWorkspaceException {

		vector.trimToSize();
		double[] vald = vector.elements();
		if (filecommunication) {
			comm.assign(variableName, vald);
		}
		else {
			try {
				connection.assign(variableName, vald);
			}
			catch (REngineException e) {
				throw new RWorkspaceException(e);
			}
		}
		Log.finest("DOUBLE_VECTOR_ADDED_TO_WORKSPACE" + new Object[] { vald.length, variableName }); //$NON-NLS-1$
	}

	/**
	 * Adds the vector to workspace.
	 *
	 * @param variableName the variable name
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addVectorToWorkspace(String variableName, int[] vector) throws RWorkspaceException {
		// checkForDuplicateVariable(variableName);
		if (filecommunication) {
			comm.assign(variableName, vector);
		}
		else {
			try {
				connection.assign(variableName, vector);
			}
			catch (REngineException e) {
				throw new RWorkspaceException(e);
			}
		}

		Log.finest("INT_VECTOR_ADDED_TO_WORKSPACE" + new Object[] { vector.length, variableName }); //$NON-NLS-1$
	}

	/**
	 * Adds the vector to workspace.
	 *
	 * @param variableName the variable name
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addVectorToWorkspace(String variableName, IntArrayList vector) throws RWorkspaceException {
		vector.trimToSize();
		int[] vald = vector.elements();
		if (filecommunication) {
			comm.assign(variableName, vald);
		}
		else {
			try {
				connection.assign(variableName, vald);
			}
			catch (REngineException e) {
				throw new RWorkspaceException(e);
			}
		}

		Log.finest("INT_VECTOR_ADDED_TO_WORKSPACE" + new Object[] { vald.length, variableName }); //$NON-NLS-1$
	}

	// --------------------------------------

	/**
	 * Adds the vector to workspace.
	 *
	 * @param variableName the variable name
	 * @param vector the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void addVectorToWorkspace(String variableName, String[] vector) throws RWorkspaceException {

		try {
			connection.assign(variableName, vector);
		}
		catch (REngineException e) {
			throw new RWorkspaceException(e);
		}

		Log.finest("CHAR_VECTOR_ADDED_TO_WORKSPACE" + new Object[] { vector.length, variableName }); //$NON-NLS-1$
	}

	/**
	 * Assign col names to matrix.
	 *
	 * @param matrix the matrix
	 * @param colNames the col names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void assignColNamesToMatrix(String matrix, String colNames) throws RWorkspaceException {
		voidEval("colnames(" + matrix + ") <- " + colNames); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Assign row names to matrix.
	 *
	 * @param matrix the matrix
	 * @param rowNames the row names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void assignRowNamesToMatrix(String matrix, String rowNames)
			throws RWorkspaceException {
		voidEval("rownames(" + matrix + ") <- " + rowNames); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Builds the string args.
	 *
	 * @param arguments the arguments
	 * @return the string[]
	 * @throws RWorkspaceException the r workspace exception
	 */
	private String[] buildStringArgs(QuantitativeDataStructure[] arguments) throws RWorkspaceException {
		String[] arguments_as_string = new String[arguments.length];
		for (int i = 0; i < arguments_as_string.length; i++) {
			if (arguments[i] == null) { // null may be used for specif ying
				// "NULL" R object.
				arguments_as_string[i] = "NULL"; //$NON-NLS-1$
			}
			else {
				if (!containsVariable(arguments[i].getSymbol())) {
					throw new RWorkspaceException(NLS.bind(RCoreMessages.error_requestedP0ObjectNotFoundInR, arguments[i].getSymbol()));
				}
				arguments_as_string[i] = arguments[i].getSymbol();
			}
		}
		return arguments_as_string;
	}

	/**
	 * Call a function, with an array of {@link QuantitativeDataStructure}.
	 * 
	 * The <code>QuantitativeDataStructure</code> array is turned into a list of
	 * non-named parameters, and {@link #callFunction(String, String[])} is
	 * called.
	 * 
	 * If one of the <code>QuantitativeDataStructure</code> is <code>null</code>
	 * , then the string "NULL" is used (= R null).
	 *
	 * @param functionName the function name
	 * @param arguments of the function, as non-named parameters.
	 * @return a REXP object
	 * @throws RWorkspaceException the r workspace exception
	 */
	public REXP callFunction(String functionName, QuantitativeDataStructure[] arguments) throws RWorkspaceException {
		String[] arguments_as_string = buildStringArgs(arguments);
		return callFunction(functionName, arguments_as_string);
	}

	/**
	 * Call a function, with an array of {@link QuantitativeDataStructure}.
	 * 
	 * The <code>QuantitativeDataStructure</code> array is turned into a list of
	 * non-named parameters, and {@link #callFunction(String, String[])} is
	 * called.
	 * 
	 * If one of the <code>QuantitativeDataStructure</code> is <code>null</code>
	 * , then the string "NULL" is used (= R null).
	 *
	 * @param functionName the function name
	 * @param arguments of the function, as non-named parameters.
	 * @param symbol the symbol
	 * @return a REXP object
	 * @throws RWorkspaceException the r workspace exception
	 */
	public REXP callFunction(String functionName, QuantitativeDataStructure[] arguments, String symbol) throws RWorkspaceException {
		String[] arguments_as_string = buildStringArgs(arguments);
		return callFunctionAndAffect(functionName, arguments_as_string, symbol);
	}

	/**
	 * See {@link #callFunction(String, String[])}.
	 *
	 * @param functionName the function name
	 * @param argument the argument
	 * @return the rEXP
	 * @throws RWorkspaceException the r workspace exception
	 */
	public REXP callFunction(String functionName, String argument) throws RWorkspaceException {
		return callFunction(functionName, new String[] { argument });
	}

	/**
	 * Call a function, with a list of non-named parameters, and return the
	 * result as a <code>REXP</code> object.
	 *
	 * @param functionName the function name
	 * @param arguments of the function, as non-named parameters.
	 * @return a REXP object
	 * @throws RWorkspaceException the r workspace exception
	 */
	public REXP callFunction(String functionName, String[] arguments)
			throws RWorkspaceException {
		// return eval(makeFunctionCall(functionName, arguments));
		return callFunctionAndAffect(functionName, arguments, "txmresult"); //$NON-NLS-1$
	}

	/**
	 * Same as {@link #callFunction(String, String[])}, but affect the result to
	 * a R name in the R workspace.
	 *
	 * @param functionName the function name
	 * @param arguments the arguments
	 * @param symbol the name to which the result of the function will be affected.
	 * @return the rEXP
	 * @throws RWorkspaceException the r workspace exception
	 */
	public REXP callFunctionAndAffect(String functionName, String[] arguments, String symbol) throws RWorkspaceException {
		// symbol must be well formed
		if (symbol.length() == 0) {
			symbol = "txmresult"; //$NON-NLS-1$
		}
		StringBuffer sb = new StringBuffer();
		sb.append(symbol);
		sb.append(RConstant.AFFECTATION);
		sb.append(makeFunctionCall(functionName, arguments));
		return eval(sb.toString());
	}

	/**
	 * Check for duplicate variable.
	 *
	 * @param variableName the variable name
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void checkForDuplicateVariable(String variableName)
			throws RWorkspaceException {
		if (containsVariable(variableName)) {
			throw new RObjectAlreadyExist("Duplicate variable name: " + variableName + ". Existing variables: " + Arrays.toString(this.getExistingVariableName())); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}


	/**
	 * Remove all variables from workspace.
	 *
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void clearWorkspace() throws RWorkspaceException {
		try {
			connection.voidEval("rm(list=ls(), inherits=FALSE)"); //$NON-NLS-1$
		}
		catch (RserveException e) {
			throw new RWorkspaceException(e);
		}
		Log.finest("R WORKSPACE_PURGED"); //$NON-NLS-1$
	}

	/**
	 * connect to Rserve using the defaults address and port 127.0.0.1:6311
	 *
	 * @return true if success
	 * @throws RWorkspaceException the r workspace exception
	 */
	private boolean connect() throws RWorkspaceException {
		return connect("127.0.0.1", 6311); //$NON-NLS-1$
	}

	/**
	 * Test if a variable exists in the R workspace. A variable exists in the R
	 * workspace if the R function <code>exists</code> return true on that
	 * variable name.
	 *
	 * @param variableName the variable supposed to exists in the R workspace.
	 * @return <code>true</code> if the variable exist, <code>false</code>
	 *         otherwise.
	 * @throws RWorkspaceException the r workspace exception
	 */
	public boolean containsVariable(String variableName)
			throws RWorkspaceException {
		REXP res = null;
		try {
			res = connection.eval("exists(\"" + variableName + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (RserveException e) {
			throw new RWorkspaceException(e);
		}
		if (res.isLogical() && ((REXPLogical) res).isTRUE()[0]) {
			return true;
		}
		return false;
	}

	/**
	 * Bar delegate call to Rengine.eval(String).
	 * 
	 * It is up to the caller to check if everything goes well (testing if the
	 * return value is not of type XT_NULL).
	 *
	 * @param exp the exp
	 * @return the rEXP
	 * @throws RWorkspaceException the r workspace exception
	 */
	public synchronized REXP eval(String exp) throws RWorkspaceException {
		REXP res = null;
		try {
			res = safeEval(exp);
		}
		catch (RserveException e) {
			RWorkspace.printLastSafeEvalExpr();
			throw new RWorkspaceException(e.getMessage() + "; " + exp.trim(), e); //$NON-NLS-1$
		}
		catch (Exception e) {
			throw new RException(e.getMessage() + "; " + exp.trim(), e); //$NON-NLS-1$
		}

		return res;
	}

	/**
	 * Eval to double.
	 *
	 * @param exp the exp
	 * @return the double[]
	 * @throws RWorkspaceException the r workspace exception
	 */
	public double[] evalToDouble(String exp) throws RWorkspaceException {
		double[] res = null;
		try {
			res = eval(exp).asDoubles();
		}
		catch (Exception e) {
			throw new RWorkspaceException(e);
		}
		return res;
	}

	// public REXP safeVoidEval(String s) throws RserveException, RException,
	// REXPMismatchException {
	// REXP r = connection.voidEval("try({" + s + "}, silent=TRUE)");
	// if (r.inherits("try-error")) throw new RException(r.asString());
	// return r;
	// }

	/**
	 * Eval to double2 d.
	 *
	 * @param exp the exp
	 * @return the double[][]
	 * @throws RWorkspaceException the r workspace exception
	 */
	public double[][] evalToDouble2D(String exp) throws RWorkspaceException {
		double[][] res = null;
		try {
			res = eval(exp).asDoubleMatrix();
		}
		catch (Exception e) {
			throw new RWorkspaceException(e);
		}
		return res;
	}

	/*
	 * Eval to int 2d.
	 * @param exp the exp
	 * @return the double[][]
	 * @throws RWorkspaceException the r workspace exception
	 */
	public int[][] evalToInt2D(String exp) throws RWorkspaceException {
		int[][] res = null;
		try {
			int[] dims = eval("dim(" + exp + ")").asIntegers(); //$NON-NLS-1$ //$NON-NLS-2$
			int ncol = dims[1];
			int nrow = dims[0];
			int[] tmp = eval("c(" + exp + ")").asIntegers(); //$NON-NLS-1$ //$NON-NLS-2$

			int c = 0;
			res = new int[nrow][ncol];
			for (int i = 0; i < ncol; i++) {
				for (int j = 0; j < nrow; j++) {
					res[j][i] = tmp[c++];
				}
			}
		}
		catch (Exception e) {
			throw new RWorkspaceException(e);
		}
		return res;
	}

	/**
	 * Eval to string.
	 *
	 * @param exp the exp
	 * @return the string[]
	 * @throws RWorkspaceException the r workspace exception
	 */
	public String[] evalToString(String exp) throws RWorkspaceException {
		String[] res = null;
		try {
			res = eval(exp).asStrings();
		}
		catch (Exception e) {
			throw new RWorkspaceException(e);
		}
		return res;
	}

	/**
	 * Extract item from list by name.
	 *
	 * @param list the list
	 * @param item_name the item_name
	 * @return the rEXP
	 * @throws RWorkspaceException the r workspace exception
	 */
	public REXP extractItemFromListByName(REXP list, String item_name) throws RWorkspaceException {
		try {
			return list.asList().at(item_name);
		}
		catch (Exception e) {
			throw new RWorkspaceException(RCoreMessages.error_errorDuringItemsListExtraction
					+ e.getMessage());
		}
	}

	/**
	 * Extract item from list by name.
	 *
	 * @param list_symbol the list_symbol
	 * @param item_name the item_name
	 * @return the rEXP
	 * @throws RWorkspaceException the r workspace exception
	 */
	public REXP extractItemFromListByName(String list_symbol, String item_name) throws RWorkspaceException {
		return eval(list_symbol + RConstant.LIST_EXTRACTOR + item_name);
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public RConnection getConnection() {
		return connection;
	}

	public StreamHog getErrorLogger() {
		return errorLogger;
	}

	// --------------------------------------

	/**
	 * Gets the existing variable name.
	 *
	 * @return the existing variable name
	 * @throws RWorkspaceException the r workspace exception
	 */
	private String[] getExistingVariableName() throws RWorkspaceException {
		REXP res;
		try {
			res = connection.eval("ls()"); //$NON-NLS-1$
		}
		catch (RserveException e) {
			throw new RWorkspaceException(e);
		}
		try {
			return res.asStrings();
		}
		catch (Exception e) {
			throw new RWorkspaceException(e);
		}
	}

	// --------------------------------------

	public StreamHog getInputLogger() {
		return inputLogger;
	}

	/**
	 * Gets the last error line.
	 *
	 * @return the last error line
	 */
	public String getLastErrorLine() {
		return this.errorLogger.lastline;
	}

	/**
	 * Gets the last log line.
	 *
	 * @return the last log line
	 */
	public String getLastLogLine() {
		return this.inputLogger.lastline;
	}

	/**
	 * Initializes the R connection configuration (time out and proxy configuration, if needed).
	 * 
	 * @param conf
	 * @throws RserveException
	 */
	public void initializeRWorkspaceConnectionConfiguration(SystemProxyDetector conf) throws RserveException {

		// reduce time out
		Log.fine("Setting R connection time out to 3 seconds."); //$NON-NLS-1$
		this.getConnection().eval("options(timeout = 3);"); //$NON-NLS-1$

		// configure the HTTP proxy
		if (conf.isSystemProxyDetected()) {
			String proxyUrl = conf.getHttpProxyUrl();
			Log.fine("Setting R proxy configuration from system proxy configuration to: " + proxyUrl); //$NON-NLS-1$
			this.getConnection().eval("Sys.setenv(http_proxy=\"" + proxyUrl + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * Checks if is logging.
	 *
	 * @return true, if is logging
	 */
	public boolean isLogging() {
		// TODO Auto-generated method stub
		return logging;
	}

	// --------------------------------------

	/**
	 * Make function call.
	 *
	 * @param functionName the function name
	 * @param arguments the arguments
	 * @return the string
	 */
	private String makeFunctionCall(String functionName, String[] arguments) {
		StringBuffer sb = new StringBuffer();
		sb.append(functionName);
		sb.append(RConstant.OPEN_PAREN);
		for (int i = 0; i < arguments.length; i++) {
			sb.append(arguments[i]);
			if ((i + 1) < arguments.length) {
				sb.append(", "); //$NON-NLS-1$
			}
		}
		sb.append(RConstant.CLOSE_PAREN);
		return sb.toString();
	}

	@SuppressWarnings("rawtypes")
	/**
	 * Post configuration.
	 *
	 * @param rPackagesPath the r packages path
	 * @return true, if successful
	 */
	public boolean postConfiguration(String extraPackagesPath) {
		// System.out.println("R post configuration");
		try {
			// more logs
			connection.eval("try(options(max.print=5000))"); //$NON-NLS-1$

			// set TXM R workspace directory
			userdir.mkdirs();
			if (!userdir.exists()) {
				System.out.println("TXM R workspace directory not found: " + userdir);
			}
			else {
				Log.fine("Setting TXM R workspace directory: " + userdir.getAbsolutePath()); //$NON-NLS-1$
				connection.eval("setwd(\"" + userdir.getAbsolutePath().replace("\\", "/") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}

			// set the libraries directory to install new R packages
			File RLibrariesWorkspaceDirectory = new File(userdir, "libraries"); //$NON-NLS-1$
			RLibrariesWorkspaceDirectory.mkdir();
			if (!RLibrariesWorkspaceDirectory.exists()) {
				System.out.println("TXM R libraries workspace directory not found: " + RLibrariesWorkspaceDirectory);
			}
			else {
				Log.fine("Setting TXM R libraries workspace directory: " + RLibrariesWorkspaceDirectory.getAbsolutePath()); //$NON-NLS-1$

				File rRootDir = RPreferences.getRRootDir();

				ArrayList<String> paths = new ArrayList<String>(Arrays.asList(
						RLibrariesWorkspaceDirectory.getAbsolutePath().replace("\\", "/"), //$NON-NLS-1$
						new File(rRootDir, "library").getAbsolutePath().replace("\\", "/"))); //$NON-NLS-1$

				if (extraPackagesPath != null && extraPackagesPath.length() > 0) {
					paths.add(extraPackagesPath.replace("\\", "/")); //$NON-NLS-1$ //$NON-NLS-2$
				}

				try {
					connection.assign("txmpaths", paths.toArray(new String[paths.size()])); //$NON-NLS-1$
					//connection.eval(".libPaths(c(\"" + RLibrariesWorkspaceDirectory.getAbsolutePath().replace("\\", "/") + "\",.libPaths()))");//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					connection.eval(".libPaths(txmpaths)");//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				}
				catch (REngineException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// set the default R repository
			String repos = RPreferences.getInstance().getString(RPreferences.DEFAULT_REPOS);
			if (repos.length() > 0) {
				Log.fine("Set the 'repos' R option to " + repos); //$NON-NLS-1$
				connection.eval("options(repos=\"" + repos + "\")");//$NON-NLS-1$ //$NON-NLS-2$
			}

			// set proxy configuration using detected system proxy if exists
			SystemProxyDetector conf = new SystemProxyDetector();
			this.initializeRWorkspaceConnectionConfiguration(conf);

			// load R libraries: this is now done by extensions/commands themself
			try {
				init(); // load libs
			}
			catch (RWorkspaceException e) {
				org.txm.utils.logger.Log.printStackTrace(e);
				return false;
			}

			// connection.eval("try(.libPaths(\"" + rPackagesPath + "\"))"); //$NON-NLS-1$ //$NON-NLS-2$

			return true;
		}
		catch (RserveException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}

	/**
	 * Start logger.
	 *
	 * @param streamHog the stream hog
	 * @param streamHog2 the stream hog2
	 * @return the r workspace
	 */
	public RWorkspace registerLogger(StreamHog streamHog, StreamHog streamHog2) {
		this.errorLogger = streamHog;
		this.inputLogger = streamHog2;
		return null;
	}

	/**
	 * Remove a variable from the workspace. The content is lost, and the name
	 * refers to nothing.
	 *
	 * @param variableName the variable name
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void removeVariableFromWorkspace(String variableName) throws RWorkspaceException {
		if (!isConnected()) return;
		if (variableName == null) return;

		try {
			connection.voidEval("rm(" + variableName + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (RserveException e) {
			throw new RWorkspaceException(e);
		}
		Log.finest("R VARIABLE_REMOVED: " + new Object[] { variableName }); //$NON-NLS-1$
	}

	/**
	 * Casts the specified REXP to a displayable object.
	 * 
	 * @param rexp
	 * @return the casted object : String, int, etc.
	 */
	public Object getCastedREXP(REXP rexp) {
		try {
			if (rexp instanceof org.rosuda.REngine.REXPString) {
				if (rexp.isVector()) {
					return StringUtils.join(rexp.asStrings(), ", "); //$NON-NLS-1$
				}
				else {
					return rexp.asString();
				}
			}
			else if (rexp instanceof org.rosuda.REngine.REXPDouble) {
				if (rexp.isVector()) {
					return Arrays.toString(rexp.asDoubles());
				}
				else {
					return rexp.asDouble();
				}
			}
			else if (rexp instanceof org.rosuda.REngine.REXPInteger) {
				if (rexp.isVector()) {
					return Arrays.toString(rexp.asIntegers());
				}
				else {
					return rexp.asInteger();
				}
			}
			else if (rexp instanceof org.rosuda.REngine.REXPNull) {
				return "NULL"; //$NON-NLS-1$
			}
		}
		catch (REXPMismatchException e) {
			e.printStackTrace();
		}
		return rexp;
	}


	/**
	 * Safe eval.
	 *
	 * @param expr the expr
	 * @return the rEXP
	 * @throws RserveException the rserve exception
	 * @throws RException the r exception
	 * @throws REXPMismatchException the rEXP mismatch exception
	 */
	public synchronized REXP safeEval(String expr) throws RserveException, RException, REXPMismatchException {
		if (logging && inputLogger != null) {
			inputLogger.printMessage(expr);
		}
		lastSafeevalExpr = expr;

		expr = "try({" + expr + "}, silent=FALSE)"; //$NON-NLS-1$ //$NON-NLS-2$

		if (isLoggingEvalCommandLines()) {
			// TODO SJ: for debuggin puprose
			// Log.info("R safeEval: " + expr, 8); //$NON-NLS-1$
			Log.info("R safeEval: " + expr); //$NON-NLS-1$
		}

		REXP r = null;
		try {
			r = connection.eval(expr);
			if (isLoggingEvalCommandLines()) {
				Log.info(" return: " + this.getCastedREXP(r)); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			//			Log.warning(NLS.bind("** {0}", e.getMessage()));
			//			Log.printStackTrace(e);
			throw e;//new RException(expr, e.getMessage());
		}
		finally {
			if (r != null && r.inherits("try-error")) { //$NON-NLS-1$
				throw new RException(expr, r.asString());
			}
		}
		return r;
	}


	/**
	 * 
	 * Bar delegate call to {@link RConnection#voidEval(String)}.
	 * 
	 * It is up to the caller to check if everything goes well (testing if the
	 * return value is not of type XT_NULL).
	 *
	 * @param exp the exp
	 * @return the string
	 * @throws RWorkspaceException the r workspace exception
	 */
	public String userEval(String exp) throws RWorkspaceException {
		if (exp.trim().length() == 0)
			return ""; //$NON-NLS-1$
		// if (loggin)
		// System.out.println("R>"+exp); //$NON-NLS-1$

		if (exp.endsWith(";")) //$NON-NLS-1$
			exp = exp.substring(0, exp.length() - 1);

		// String trybegin = "try("; //$NON-NLS-1$
		// String tryend = ")"; //$NON-NLS-1$

		String out = null;
		try {
			if (isLoggingEvalCommandLines()) {
				Log.info("R userEval: " + exp); //$NON-NLS-1$
			}
			// out = connection.eval("paste(capture.output(print(" + trybegin + trybegin +"("+ exp+")" + tryend + tryend + ")),collapse=\"\\n\")").asString(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			// //$NON-NLS-4$
			// String cmd = "paste(capture.output(print(" + trybegin + "("+ exp+")" + tryend + ")),collapse=\"\\n\")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			REXP ret = connection.eval(exp);
			// REXP ret = connection.eval(exp);

			if (ret instanceof org.rosuda.REngine.REXPString) {
				if (ret.isVector()) {
					out = "result: " + StringUtils.join(ret.asStrings(), ", "); //$NON-NLS-1$ //$NON-NLS-2$
				}
				else {
					out = "result: " + ret.asString(); //$NON-NLS-1$
				}
			}
			else if (ret instanceof org.rosuda.REngine.REXPDouble) {
				if (ret.isVector()) {
					out = "result: " + ret.asDouble(); //$NON-NLS-1$
				}
				else {
					out = "result: " + Arrays.toString(ret.asDoubles()); //$NON-NLS-1$
				}
			}
			else if (ret instanceof org.rosuda.REngine.REXPInteger) {
				if (ret.isVector()) {
					out = "result: " + ret.asInteger(); //$NON-NLS-1$
				}
				else {
					out = "result: " + Arrays.toString(ret.asIntegers()); //$NON-NLS-1$
				}
			}
			else if (ret instanceof org.rosuda.REngine.REXPNull) {
				out = "NULL"; //$NON-NLS-1$
			}
			else {
				out = "result: " + ret; //$NON-NLS-1$
			}

		}
		catch (RserveException e) {
			System.out.println(NLS.bind(RCoreMessages.error_evaluationErrorP0P1, e.getMessage(), exp));
			Log.printStackTrace(e);
		}
		catch (Exception e) {
			System.out.println("REXPMismatchException: " + e);
			Log.printStackTrace(e);
		}

		return out;
	}

	/**
	 * Bar delegate call to {@link RConnection#voidEval(String)}.
	 * 
	 * It is up to the caller to check if everything goes well (testing if the
	 * return value is not of type XT_NULL).
	 *
	 * @param exp the exp
	 * @throws RWorkspaceException the r workspace exception
	 */
	public boolean voidEval(String exp) throws RWorkspaceException {

		if (exp.endsWith(";")) //$NON-NLS-1$
			exp = exp.substring(0, exp.length() - 1);

		try {
			safeEval(exp);
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return false;
		}
		return true;
	}


	/**
	 * Checks if the eval command lines preference is set
	 * 
	 * @return
	 */
	public static boolean isLoggingEvalCommandLines() {
		return RPreferences.getInstance().getBoolean(RPreferences.SHOW_EVAL_LOGS);
	}

	/**
	 * Checks if the debug mode preference is set.
	 * 
	 * @return
	 */
	public static boolean isDebugMode() {
		return RPreferences.getInstance().getBoolean(RPreferences.DEBUG);
	}

	/**
	 * Sets the log.
	 *
	 * @param start the new log
	 */
	public void setLog(boolean start) {
		logging = start;
		if (errorLogger != null)
			this.errorLogger.setPrint(start);
		if (inputLogger != null)
			this.inputLogger.setPrint(start);
	}

	/**
	 * To txt.
	 *
	 * @param file the file
	 * @param symbol the symbol
	 * @throws StatException the stat exception
	 */
	public void toTxt(File file, String symbol) throws StatException {
		eval("write.table(" + symbol + file.getAbsolutePath().replace("\\\\", "/")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}



	/**
	 * Loads a package library from its name.
	 * 
	 * @param p
	 * @throws RWorkspaceException
	 */
	public void loadPackage(String p) throws RWorkspaceException {
		this.eval("library(\"" + p + "\")"); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
