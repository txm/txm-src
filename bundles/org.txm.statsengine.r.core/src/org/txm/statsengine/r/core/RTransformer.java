package org.txm.statsengine.r.core;

import org.txm.core.results.TXMResult;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

/**
 * a RTransformer allows to transfert data from TXM/R to R/TXM
 * 
 * RTransformer are registered in the RStatengine and used by the SendToR command
 * 
 * @author mdecorde
 *
 * @param <T>
 */
public abstract class RTransformer<T extends TXMResult> {

	/**
	 * the key used to store the previous symbol name
	 */
	public static final String PREFERENCE_KEY = "previous_rsymbol";

	/**
	 * 
	 * @param symbol the R symbol pointing to the R variable to transfert in TXM (Java part)
	 * @return the resulting TXMResult
	 * @throws RWorkspaceException
	 */
	public abstract T fromRtoTXM(String symbol) throws RWorkspaceException;

	/**
	 * 
	 * @param o the result to tranfert in R
	 * @return the created R variable symbol
	 * @throws RWorkspaceException
	 */
	public String fromTXMtoR(T o) throws RWorkspaceException {


		String symbol = getTMPSymbol(o);

		symbol = _fromTXMtoR(o, symbol);

		setTMPSymbol(o, symbol);

		return symbol;
	}

	public void setTMPSymbol(T o, String symbol) {
		if (symbol == null) return;
		if (symbol.length() == 0) return;

		o.setTemporaryData(PREFERENCE_KEY, symbol);
		//		try {
		//			try {
		//				IScopeContext preferences = DefaultScope.INSTANCE;
		//				preferences.getNode(o.getParametersNodePath()).put(PREFERENCE_KEY, symbol);
		//			}
		//			catch (Exception e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//			
		//		}
		//		catch (Exception e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
	}
	
	public static boolean hasRSymbol(TXMResult o) {
		if (o == null) return false;
		return o.getTemporaryData(PREFERENCE_KEY) != null;
	}

	public static String getTMPSymbol(TXMResult o) {
		if (o == null) return null;
		if (o.getTemporaryData(PREFERENCE_KEY) == null) return null;
		return o.getTemporaryData(PREFERENCE_KEY).toString();

		//		try {
		//			IScopeContext preferences;
		//			try {
		//				preferences = DefaultScope.INSTANCE;
		//				String p = o.getParametersNodePath();
		//				IEclipsePreferences pnode = preferences.getNode(p);
		//				return pnode.get(PREFERENCE_KEY, null);
		//			}
		//			catch (Exception e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//			
		//		}
		//		catch (Exception e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		return null;
	}

	/**
	 * 
	 * @param o the result to tranfert in R
	 * @param symbol the symbol to use OR null (and the RTransformer will have to generate one)
	 * @return the created R variable symbol
	 * @throws RWorkspaceException
	 */
	public abstract String _fromTXMtoR(T o, String symbol) throws RWorkspaceException;
}
