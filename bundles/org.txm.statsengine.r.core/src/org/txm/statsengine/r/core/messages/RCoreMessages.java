package org.txm.statsengine.r.core.messages;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.messages.Utf8NLS;

/**
 * R statistics engine and tools core messages.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class RCoreMessages extends TXMCoreMessages {

	private static final String BUNDLE_NAME = "org.txm.statsengine.r.core.messages.messages"; //$NON-NLS-1$

	public static String error_cantFindRServeInTheP0Path;

	public static String error_connectionFailed;

	public static String error_errorDuringItemsListExtraction;

	public static String error_errorP0WhileEvaluatingP1;

	public static String error_evaluationErrorP0P1;

	public static String error_failedToConnectToTheRWorkspaceP0AtP1WithUserP2;

	public static String error_failedToInitializeFileTransfertP0;

	public static String error_failedToLoadTheP0LibraryP1;

	public static String error_failedToRunREG;

	public static String error_failedToStartRServe;

	public static String error_failedToStartRServeWithCommand;

	public static String error_matrixIsEmpty;

	public static String error_requestedP0ObjectNotFoundInR;

	public static String error_rserveIsAlreadyRunningOnTheP0Port;

	public static String error_rservePathNotSet;

	public static String error_youNeedRSoftware;

	public static String info_connectingToRAtP0P1;

	public static String info_connectingToRAtP0P1WithUserP2;

	public static String info_lastSafeEvalExpressionP0;

	public static String info_rPathNotSetTryingToFindIt;

	public static String info_savingChartToTheP0File;

	public static String info_startingRWithCommandLineP0;

	public static String info_startingStatsEngineP0P1P2P3;

	public static String info_statisticsEngineLaunched;

	public static String info_tryingToStartRFromP0;

	public static String info_tryingToStartRWithRPath;

	public static String runningP0;

	public static String doneP0;

	public static String runningTextSelection;

	public static String errorWhileRunningRScriptP0ColonP1;

	public static String p0RScriptDoesntExist;

	public static String error_unexpectedErrorInRStatisticsEngineP0;

	public static String rserveNotStartedUsingTheP0Path;

	public static String error_cantExecuteTheP0RServeFile;

	public static String error_P0RservePathNotPointingAFile;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, RCoreMessages.class);
	}

	private RCoreMessages() {
	}
}
