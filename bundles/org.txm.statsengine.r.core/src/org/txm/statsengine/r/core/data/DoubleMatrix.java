// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (Mon, 06 May 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core.data;

import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

import cern.colt.matrix.DoubleMatrix2D;

// TODO: Auto-generated Javadoc
/**
 * A double matrix, backed to the R engine.
 * 
 * @author sloiseau
 */
public class DoubleMatrix extends MatrixImpl {

	/**
	 * Instantiates a new double matrix.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public DoubleMatrix(DoubleMatrix2D matrix) throws RWorkspaceException {
		super(matrix);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new double matrix.
	 *
	 * @param mat the mat
	 * @throws RWorkspaceException the r workspace exception
	 */
	public DoubleMatrix(double[][] mat) throws RWorkspaceException {
		super(mat);
	}

	/**
	 * Instantiates a new double matrix.
	 *
	 * @param mat the mat
	 * @throws RWorkspaceException the r workspace exception
	 */
	public DoubleMatrix(double[][] mat, int ncol, int nrow) throws RWorkspaceException {
		super(mat, ncol, nrow);
	}

	/**
	 * Instantiates a new double matrix.
	 *
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public DoubleMatrix(String symbol) throws RWorkspaceException {
		super(symbol);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new double matrix.
	 *
	 * @param matrix the matrix
	 * @param rowNames the row names
	 * @param columnNames the column names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public DoubleMatrix(DoubleMatrix2D matrix, String[] rowNames,
			String[] columnNames) throws RWorkspaceException {
		super(matrix, rowNames, columnNames);
		// TODO Auto-generated constructor stub
	}

}
