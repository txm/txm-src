// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.statsengine.r.core;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;

// TODO: Auto-generated Javadoc
/**
 * The Class ConnectToRserve.
 */
// not used? class of test?
class ConnectToRserve {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String args[]) {
		try {
			RConnection c = new RConnection();
			String userhome = System.getProperty("user.home"); //$NON-NLS-1$
			RList ret = c.eval("source('" + userhome + "/Bureau/R/Rdata')").asList(); //$NON-NLS-1$ //$NON-NLS-2$

			System.out.println("source ret : " + ret.get(1)); //$NON-NLS-1$
			System.out.println(ret);

			REXP ret2 = c.eval("print(lexicaltable)"); //$NON-NLS-1$
			System.out.println("lexicaltable :" + ret2); //$NON-NLS-1$
		}
		catch (Exception e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
