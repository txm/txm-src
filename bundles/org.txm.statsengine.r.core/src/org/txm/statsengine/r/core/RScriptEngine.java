package org.txm.statsengine.r.core;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.txm.core.engines.ScriptEngine;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.statsengine.r.core.messages.RCoreMessages;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

public class RScriptEngine extends ScriptEngine {

	public RScriptEngine() {
		super("R", "R"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	/**
	 * Execute text.
	 *
	 * @param text the text
	 * @return the object
	 */
	public IStatus executeText(String text, HashMap<String, Object> env) {
		RWorkspace rw = null;
		try {
			rw = RWorkspace.getRWorkspaceInstance();
		}
		catch (RWorkspaceException e1) {
			Log.printStackTrace(e1);
			return Status.CANCEL_STATUS;
		}
		boolean initState = rw.isLogging();
		rw.setLog(true);

		try {
			System.out.println(RCoreMessages.runningTextSelection);
			long time = System.currentTimeMillis();
			//execute a file line per line

			//String str = rw.userEval(text);//evaluate line in R
			rw.eval(text);
			//			if (str != null && rw.isLogging() && str.length() > 0)
			//				RConsole.printServer(str);
			RWorkspace.appendtoEvalLog(text);

			System.out.println(TXMCoreMessages.bind(RCoreMessages.doneP0, (System.currentTimeMillis() - time)));

		}
		catch (Exception e) {
			System.out.println(NLS.bind(RCoreMessages.errorWhileRunningRScriptP0ColonP1, text, Log.toString(e)));
			//			StatusLine.error(NLS.bind(TXMUIMessages.ExecuteRText_1, text, Log.toString(e))); 
			Log.printStackTrace(e);
		}

		rw.setLog(initState);
		return Status.OK_STATUS;
	}

	@Override
	/**
	 * Execute script.
	 *
	 * @param rFile the R script
	 */
	// FIXME: this method should call the method above : executeText();
	public IStatus executeScript(File rFile, HashMap<String, Object> env, List<String> stringArgs) {
		if (!rFile.exists()) {
			System.err.println(NLS.bind(RCoreMessages.p0RScriptDoesntExist, rFile));
			return Status.CANCEL_STATUS;
		}

		RWorkspace rw = null;
		try {
			rw = RWorkspace.getRWorkspaceInstance();
		}
		catch (RWorkspaceException e1) {
			Log.printStackTrace(e1);
			return Status.CANCEL_STATUS;
		}
		boolean initState = rw.isLogging();
		rw.setLog(true);
		try {
			System.out.println(NLS.bind(RCoreMessages.runningP0, rFile.getName()));
			long time = System.currentTimeMillis();
			//execute a file line per line

			//if (TxmPreferences.getBoolean(RPreferencePage.R_REMOTE)) {


			//if (rw.isLogging() && allScript.length() > 0) RConsole.printUser(allScript);

			//String str = rw.userEval(allScript); // evaluate line in R
			String allScript = IOUtils.getText(rFile);
			rw.eval(allScript);
			//if (str != null && rw.isLogging() && str.length() > 0)
			//RConsole.printServer(str);
			//			} else {
			//				String cmd = "source(\""+rFile.getAbsolutePath().replace("\\", "/")+"\")";
			//				if (rw.isLogging())
			//					RConsole.printUser)(cmd);
			//				String str = rw.userEval(cmd);//evaluate line in R
			//				if (rw.isLogging() && str.length() > 0)
			//					RConsole.printServer(cmd);
			//			}

			System.out.println(TXMCoreMessages.bind(RCoreMessages.doneP0, (System.currentTimeMillis() - time)));

		}
		catch (Exception e) {
			System.out.println(NLS.bind(RCoreMessages.errorWhileRunningRScriptP0ColonP1, rFile, e.getMessage()));
			Log.printStackTrace(e);
			return Status.CANCEL_STATUS;
		}
		finally {
			rw.setLog(initState);
		}

		return Status.OK_STATUS;

	}
}
