// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-08-29 14:23:55 +0200 (Thu, 29 Aug 2013) $
// $LastChangedRevision: 2519 $
// $LastChangedBy: nilskredens $ 
//
package org.txm.statsengine.r.core.data;

import java.util.List;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.core.utils.ArrayIndex;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.logger.Log;

/**
 * Implementation of the {@link Vector} interface, wrapping a R vector.
 * 
 * @author sloiseau
 */
public class VectorImpl extends QuantitativeDataStructureImpl implements Vector {

	/**
	 * This is final in order to be sure that a length is always defined, and
	 * defined only once, on a vector.
	 */
	private int length;

	/**
	 * Instantiates a new vector.
	 *
	 * @param data the data
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(int[] data) throws RWorkspaceException {
		super();
		rw.addVectorToWorkspace(symbol, data);
		length = data.length;
	}

	/**
	 * Instantiates a new vector impl.
	 *
	 * @param data the data
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(double[] data) throws RWorkspaceException {
		super();
		rw.addVectorToWorkspace(symbol, data);
		length = data.length;
	}

	/**
	 * Instantiates a new vector impl.
	 *
	 * @param data the data
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(String[] data) throws RWorkspaceException {
		super();
		rw.addVectorToWorkspace(symbol, data);
		length = data.length;
	}

	/**
	 * Instantiates a new vector impl.
	 *
	 * @param data the data
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(List<String> data) throws RWorkspaceException {
		super();
		rw.addVectorToWorkspace(symbol, data.toArray(new String[data.size()]));
		length = data.size();
	}

	/**
	 * Instantiates a new vector.
	 *
	 * @param data the data
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(int[] data, String symbol) throws RWorkspaceException {
		super();
		this.symbol = symbol;
		rw.addVectorToWorkspace(symbol, data);
		length = data.length;
	}

	/**
	 * Instantiates a new vector impl.
	 *
	 * @param data the data
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(double[] data, String symbol) throws RWorkspaceException {
		super();
		this.symbol = symbol;
		rw.addVectorToWorkspace(symbol, data);
		length = data.length;
	}

	/**
	 * Instantiates a new vector impl.
	 *
	 * @param data the data
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(String[] data, String symbol) throws RWorkspaceException {
		super();
		this.symbol = symbol;
		rw.addVectorToWorkspace(symbol, data);
		length = data.length;
	}

	/**
	 * Constructing a Vector when the R object already exists in the workspace:
	 * we only want to record its symbol in a java object.
	 *
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	public VectorImpl(String symbol) throws RWorkspaceException {
		super(symbol);
		try {
			length = rw.eval("length(" + symbol + ")").asInteger(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (Exception e) {
			throw new RWorkspaceException(e);
		}
	}

	/**
	 * Sets the r names.
	 *
	 * @param vals the new r names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void setRNames(String[] vals) throws RWorkspaceException {
		Vector names = new VectorImpl(vals);

		StringBuffer exp = new StringBuffer();
		exp.append("names("); //$NON-NLS-1$
		exp.append(symbol);
		exp.append(")"); //$NON-NLS-1$
		exp.append("<-"); //$NON-NLS-1$
		exp.append(names.getSymbol());
		exp.append(";"); //$NON-NLS-1$

		rw.voidEval(exp.toString());
		rw.voidEval("rm(" + names.getSymbol() + ");"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Gets the r names.
	 * 
	 * @param string the string
	 * 
	 * @throws RWorkspaceException the r workspace exception
	 */
	public void getRNames(String[] string) throws RWorkspaceException {
		getAttribute("names"); //$NON-NLS-1$
	}

	/**
	 * Get a new vector with a copy of the value at the specified index in this
	 * vector.
	 *
	 * @param index the index
	 * @return the vector
	 * @throws RWorkspaceException the r workspace exception
	 */
	@Override
	public Vector get(int[] index) throws RWorkspaceException {
		int[] rIndex = ArrayIndex.zeroToOneBasedIndex(index, this.length);
		// TODO Auto-generated method stub
		String name_index = createSymbole(Vector.class);
		rw.addVectorToWorkspace(name_index, rIndex);
		String name = createSymbole(Vector.class);
		rw.voidEval(name + " <- " + symbol + "[ " + name_index + "];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return new VectorImpl(name);
	}

	/**
	 * Throw a {@link RWorkspaceException} if the vector is not of type integer.
	 *
	 * @return the int[]
	 * @throws RWorkspaceException the r workspace exception
	 */
	@Override
	public int[] asIntArray() throws RWorkspaceException {
		try {
			REXP rexp = rw.eval(symbol);
			if (rexp.isNull()) {
				return new int[0];
			}
			return rexp.asIntegers();
		}
		catch (REXPMismatchException e) {
			throw new RWorkspaceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#asStringsArray()
	 */
	@Override
	public String[] asStringsArray() throws StatException {
		try {
			REXP rexp = rw.eval(symbol);
			if (rexp.isNull()) {
				return new String[0];
			}
			return rexp.asStrings();
		}
		catch (REXPMismatchException e) {
			throw new RWorkspaceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#asDoubleArray()
	 */
	@Override
	public double[] asDoubleArray() throws StatException {
		try {
			REXP rexp = rw.eval(symbol);
			if (rexp.isNull()) {
				return new double[0];
			}
			return rexp.asDoubles();
		}
		catch (REXPMismatchException e) {
			throw new RWorkspaceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#getLength()
	 */
	@Override
	public int getLength() throws StatException {
		return length;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#remove(int)
	 */
	@Override
	public boolean remove(int index) {
		try {
			if (index < this.length) {
				rw.eval(symbol + " <- " + symbol + "[-" + (index + 1) + "];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				length--;
			}
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#setString(int, java.lang.String)
	 */
	@Override
	public void setString(int index, String value) {
		try {
			if (index < this.length) {
				rw.eval(symbol + "[" + (index + 1) + "] <-\"" + value + "\";"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#setDouble(int, double)
	 */
	@Override
	public void setDouble(int index, double value) {
		try {
			if (index < this.length) {
				rw.eval(symbol + "[" + (index + 1) + "] <-" + value + ";"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#setInt(int, int)
	 */
	@Override
	public void setInt(int index, int value) {
		try {
			if (index < this.length) {
				rw.eval(symbol + "[" + (index + 1) + "] <-" + value + ";"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#sort(java.lang.Boolean)
	 */
	@Override
	public void sort(Boolean reverse) {
		try {
			// System.out.println("R sort rownames Reverse" + reverse);
			REXP r = rw
					.eval(symbol
							+ "<- " + symbol + "[order(" + symbol + ", decreasing = " + reverse.toString().toUpperCase() + ")];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#setOrder(java.util.List, java.lang.Boolean)
	 */
	@Override
	public void setOrder(List<Integer> neworder, Boolean reverse) {
		if (neworder.size() != this.length)
			return;
		String order = "c("; //$NON-NLS-1$
		if (reverse) {
			for (int i = neworder.size() - 1; i >= 0; i--)
				order += "" + (neworder.get(i) + 1) + ", "; //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			for (Integer i : neworder)
				order += "" + (i + 1) + ", "; //$NON-NLS-1$ //$NON-NLS-2$
		}
		order = order.substring(0, order.length() - 2);
		order += ")"; //$NON-NLS-1$
		try {
			// System.out.println("R sort rownames Reverse"+reverse);
			REXP r = rw.eval(symbol + "<- " + symbol + "[" + order + "];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Vector#cut(int)
	 */
	@Override
	public void cut(int before, int after) {
		try {
			if (before < 1) before = 1;

			// cut nlines
			REXP r = rw.eval(symbol + " <- " + symbol + "[" + before + ":" + after + "];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}
}
