package org.txm.statsengine.r.core;

import java.io.File;

import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

//FIXME: SJ: find a better way to manage this, eg. define a getInternalId() method in TXMResult
// Using this create a too strong link between result and engine
public interface RResult {

	/**
	 * 
	 * @return the R result symbol if any (null if not linked to a R result)
	 */
	public String getRSymbol();

	public static boolean exportRdataOrRDS(RResult result, File file) throws RWorkspaceException {

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();

		if (file.getName().endsWith(".Rdata")) { //$NON-NLS-1$
			rw.eval("save(" + result.getRSymbol() + ",file=\"" + file.getAbsolutePath().replace("\\", "\\\\") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			return true;
		}
		else if (file.getName().endsWith(".rds")) { //$NON-NLS-1$
			rw.eval("saveRDS(" + result.getRSymbol() + ",file=\"" + file.getAbsolutePath().replace("\\", "\\\\") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			return true;
		}

		return false;
	}

	public static boolean importRdataOrRDS(RResult result, File file) throws RWorkspaceException {

		RWorkspace rw = RWorkspace.getRWorkspaceInstance();

		if (file.getName().endsWith(".Rdata")) { //$NON-NLS-1$
			rw.eval(result.getRSymbol() + " <- load(" + result.getRSymbol() + ",file=\"" + file.getAbsolutePath().replace("\\", "\\\\") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			return true;
		}
		else if (file.getName().endsWith(".rds")) { //$NON-NLS-1$
			rw.eval(result.getRSymbol() + " <- readRDS(file=\"" + file.getAbsolutePath().replace("\\", "\\\\") + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			return true;
		}

		return false;
	}
}
