// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (Mon, 06 May 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core.exceptions;


// TODO: Auto-generated Javadoc
/**
 * When one try to create an object with a symbol already existing in the
 * workspace.
 * 
 * @author sloiseau
 */
public class RObjectAlreadyExist extends RWorkspaceException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3386015668080138287L;

	/**
	 * Instantiates a new r object already exist.
	 */
	public RObjectAlreadyExist() {
		super();
	}

	/**
	 * Instantiates a new r object already exist.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 */
	public RObjectAlreadyExist(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Instantiates a new r object already exist.
	 *
	 * @param arg0 the arg0
	 */
	public RObjectAlreadyExist(String arg0) {
		super(arg0);
	}

	/**
	 * Instantiates a new r object already exist.
	 *
	 * @param arg0 the arg0
	 */
	public RObjectAlreadyExist(Throwable arg0) {
		super(arg0);
	}

}
