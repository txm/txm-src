// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-05-22 09:06:15 +0200 (Fri, 22 May 2015) $
// $LastChangedRevision: 2973 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.txm.libs.msoffice.WriteExcel;
import org.txm.libs.office.WriteODS;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Matrix;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.core.messages.StatsEngineCoreMessages;
import org.txm.statsengine.core.utils.ArrayIndex;
import org.txm.statsengine.r.core.RResult;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RException;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.OSDetector;
import org.txm.utils.TableReader;
import org.txm.utils.logger.Log;

import cern.colt.matrix.DoubleMatrix2D;

/**
 * Implementation of the {@link Matrix} interface, wrapping a R matrix.
 * 
 * define the methods to edit/delete elements in the matrix
 * 
 * @author sloiseau
 */
public class MatrixImpl extends QuantitativeDataStructureImpl implements Matrix {

	public static int[][] asIntMatrix(REXP r) throws REXPMismatchException {

		int[] ct = r.asIntegers();
		if (ct == null) return null;
		REXP dim = r.getAttribute("dim"); //$NON-NLS-1$
		int[] ds = dim.asIntegers();
		if (ds == null || ds.length != 2)
			return null; // matrix must be 2-dimensional

		int m = ds[0], n = ds[1];
		int[][] rez = new int[m][n];

		// R stores matrices as matrix(c(1,2,3,4),2,2) = col1:(1,2), col2:(3,4)
		// we need to copy everything, since we create 2d array from 1d array
		int i = 0, k = 0;
		while (i < n) {
			int j = 0;
			while (j < m) {
				rez[j++][i] = ct[k++];
			}
			i++;
		}
		return rez;
	}

	// /** The ncol. */
	// protected int ncol;
	//
	// /** The nrow. */
	// protected int nrow;

	/**
	 * Instantiates a new matrix impl.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public MatrixImpl(double[][] matrix) throws RWorkspaceException {
		super();
		rw.addMatrixToWorkspace(symbol, matrix);
		// this.nrow = matrix.length;
		// this.ncol = matrix[0].length;
		// this.rows = null;
		// this.cols = null;
	}

	/**
	 * Instantiates a new matrix impl.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public MatrixImpl(double[][] matrix, int ncol, int nrow) throws RWorkspaceException {
		super();
		rw.addMatrixToWorkspace(symbol, matrix, ncol, nrow);
		// this.nrow = matrix.length;
		// this.ncol = matrix[0].length;
		// this.rows = null;
		// this.cols = null;
	}

	/**
	 * Instantiates a new matrix impl.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public MatrixImpl(DoubleMatrix2D matrix) throws RWorkspaceException {
		super();
		rw.addMatrixToWorkspace(symbol, matrix);
		// this.nrow = matrix.rows();
		// this.ncol = matrix.columns();
		// this.rows = null;
		// this.cols = null;
	}

	/**
	 * Create a new matrix given a {@link DoubleMatrix2D}, with row and col
	 * names.
	 *
	 * @param matrix the matrix
	 * @param rowNames row names.
	 * @param columnNames the column names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public MatrixImpl(DoubleMatrix2D matrix, String[] rowNames,
			String[] columnNames) throws RWorkspaceException {
		this(matrix);

		if (rowNames == null) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.rowNamesOfAContingencyTableCannotBeNull);
		}
		// if (rowNames.length != nrow) {
		// throw new IllegalArgumentException(Messages.MatrixImpl_5);
		// }
		// this.rows = new VectorImpl(rowNames);
		rw.assignRowNamesToMatrix(symbol, new VectorImpl(rowNames).getSymbol());

		if (columnNames == null) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.columnNamesOfAContingencyTableCannotBeNull);
		}
		// if (columnNames.length != ncol) {
		// throw new IllegalArgumentException(Messages.MatrixImpl_7
		// + columnNames.length + ", " + ncol + ")."); //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
		// }
		// this.cols = new VectorImpl(columnNames);
		rw.assignColNamesToMatrix(symbol, new VectorImpl(columnNames).getSymbol());

	}

	/**
	 * Instantiates a new matrix impl.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public MatrixImpl(int[][] matrix, int ncol, int nrow) throws RWorkspaceException {
		super();
		rw.addMatrixToWorkspace(symbol, matrix, ncol, nrow);
		// this.nrow = matrix.length;
		// if (matrix.length > 0)
		// this.ncol = matrix[0].length;
		// else
		// this.ncol = 0;
		// this.rows = null;
		// this.cols = null;
	}

	/**
	 * Instantiates a new matrix impl.
	 *
	 * @param matrix the matrix
	 * @throws RWorkspaceException the r workspace exception
	 */
	public MatrixImpl(int[][] matrix) throws RWorkspaceException {
		super();
		rw.addMatrixToWorkspace(symbol, matrix);
		// this.nrow = matrix.length;
		// if (matrix.length > 0)
		// this.ncol = matrix[0].length;
		// else
		// this.ncol = 0;
		// this.rows = null;
		// this.cols = null;
	}

	@Override
	public void exchangeColumns(int c1, int c2) {
		try {
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("tmp <- " + symbol + "[," + c1 + "]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			rw.eval(symbol + "[," + c1 + "] <- " + symbol + "[," + c2 + "]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			rw.eval(symbol + "[," + c2 + "] <- tmp"); //$NON-NLS-1$ //$NON-NLS-2$
			rw.eval("tmp <- colnames(" + symbol + ")[" + c1 + "]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			rw.eval("colnames(" + symbol + ")[" + c1 + "] <- colnames(" + symbol + ")[" + c2 + "]"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			rw.eval("colnames(" + symbol + ")[" + c2 + "] <- tmp"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (RWorkspaceException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Instantiates a new matrix impl.
	 *
	 * @param matrix the matrix
	 * @param lineNames the form names
	 * @param partNames the part names
	 * @throws RWorkspaceException the r workspace exception
	 */
	public MatrixImpl(int[][] matrix, String[] lineNames, String[] partNames) throws RWorkspaceException {
		this(matrix, partNames != null ? partNames.length : 0, lineNames != null ? lineNames.length : 0);

		if (lineNames == null) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.rowNamesOfAContingencyTableCannotBeNull);
		}

		if (partNames == null) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.columnNamesOfAContingencyTableCannotBeNull);
		}

		// if (lineNames.length != nrow) {
		// throw new IllegalArgumentException(Messages.MatrixImpl_5);
		// }
		// this.rows = ;
		rw.assignRowNamesToMatrix(symbol, new VectorImpl(lineNames).getSymbol());


		// if (partNames.length != ncol) {
		// throw new IllegalArgumentException(Messages.MatrixImpl_7
		// + partNames.length + ", " + ncol + ")."); //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
		// }
		// this.cols = new VectorImpl(partNames);
		rw.assignColNamesToMatrix(symbol, new VectorImpl(partNames).getSymbol());

	}

	/**
	 * Create a matrix wrapping data already existing into R with the given
	 * symbol.
	 * 
	 * This method is protected on purpose : only classes in this package,
	 * dealing with the R workspace, may have direct access to R objects.
	 *
	 * @param symbol the name of R object existing in the workspace.
	 * @throws RWorkspaceException the r workspace exception
	 */
	protected MatrixImpl(String symbol) throws RWorkspaceException {
		super(symbol);
	}

	/**
	 * Removes the row with a negative index.
	 * 
	 * @param row the row [1..N]
	 */
	public void _removeRow(int row) {
		// dans R theMatrixWithoutRow5 = theMatrix[-5,]
		try {
			REXP r = rw.eval(symbol
					+ " <- " + symbol + "[ " + row + ",];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			// nrow--;
		}
		catch (RWorkspaceException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		// de la liste rows
	}

	/**
	 * Filter.
	 *
	 * @param nlines the nlines
	 * @param fmin the fmin
	 * @throws REXPMismatchException
	 * @throws RWorkspaceException
	 */
	public void filter(int nlines, int fmin, int fmax) throws RWorkspaceException, REXPMismatchException {
		RWorkspace rw = RWorkspace.getRWorkspaceInstance();

		if (nlines < 1) nlines = 1;
		if (fmax < fmin) fmax = fmin;

		// first sort rows
		rw.eval("freqs <- rowSums(" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		int[] freqs = rw.eval("freqs").asIntegers(); //$NON-NLS-1$
		rw.eval("order <- sort(freqs, decreasing= TRUE, index.return= TRUE)"); //$NON-NLS-1$
		// rw.eval("print(order)");

		// get table sort indexes : from max to min
		int[] order = rw.eval("order$ix").asIntegers(); //$NON-NLS-1$

		// sort the table to cut later
		rw.eval(symbol + "<- " + symbol + "[order$ix, ];"); //$NON-NLS-1$ //$NON-NLS-2$

		// find first fmin line
		int ifmin = order.length;
		int ifmax = 1;
		for (int i = 0; i < order.length; i++) { // iterate from max to min freqs
			// System.out.print("\t"+i+" freqs["+order[i]+"]"+freqs[order[i]-1]);
			if (freqs[order[i] - 1] < fmin) { // minus 1 because of R indexes
				ifmin = i; // the ith line
				break;
			}
		}
		// ifmin = Math.min(order.length - 1, ifmin);

		for (int i = ifmin - 1; i >= 0; i--) { // iterate from min to max freqs
			// System.out.print("\t"+i+" freqs["+order[i]+"]"+freqs[order[i]-1]);
			// System.out.println("i=" + i + " o=" + order[i] + " f=" + freqs[order[i] - 1]);
			if (freqs[order[i] - 1] > fmax) { // minus 1 because of R indexes
				ifmax = i + 1 + 1; // the previous ith line + fix index for R
				break;
			}
		}

		ifmin = Math.max(1, ifmin);
		ifmax = Math.max(1, ifmax);

		if ((ifmin - ifmax) >= nlines) { // too much lines, cutting fmin lines
			ifmin = ifmax + nlines - 1;
		}
		// cut

		if (ifmax <= ifmin) {
			cut(ifmax, ifmin); // ifmax is always < to fmin, because of the decreasing loop index
		}
	}

	/**
	 * Cut.
	 *
	 * @param before lines before 'before' are removed
	 * @param after lines after 'after' are removed
	 */
	public void cut(int before, int after) {

		if (after > this.getNRows()) {
			after = this.getNRows();
			if (after == 0) {
				return; // nothing to do
			}
		}

		if (before < 1) before = 1;

		try {
			// // cut nlines
			if (before != after) {
				REXP r = rw.eval(symbol + "<- " + symbol + "[" + before + ":" + (after) + ", ];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			else {
				REXP r = rw.eval(symbol + "<- " + "as.matrix(" + symbol + "[" + before + ", ]);"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			}
			// getRowNames().cut(before, after);
			// this.nrow = nlines;
		}
		catch (RWorkspaceException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	public void sortByFreqs(boolean reverse) throws RWorkspaceException, REXPMismatchException {
		rw.eval("freqs <- rowSums(" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		String DECREASING = "FALSE"; //$NON-NLS-1$
		if (reverse) DECREASING = "TRUE"; //$NON-NLS-1$
		rw.eval("order <- sort(freqs, decreasing= " + DECREASING + ", index.return= TRUE)"); //$NON-NLS-1$ //$NON-NLS-2$
		rw.eval(symbol + "<- " + symbol + "[order$ix, ];"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Fusion rows.
	 *
	 * @param rows the rows
	 * @throws RWorkspaceException
	 */
	public boolean mergeRows(int[] rows) throws RWorkspaceException {
		if (rows.length < 2) return false;
		Arrays.sort(rows);
		// System.out.println("fusion rows: "+Arrays.toString(rows)); //$NON-NLS-1$
		int rowToChange = rows[0] + 1;
		rw.addVectorToWorkspace("rowsToMerge", ArrayIndex.zeroToOneBasedIndex(rows, rows.length)); //$NON-NLS-1$
		RWorkspace.getRWorkspaceInstance().eval("newrow <- colSums(" + symbol + "[rowsToMerge,])"); //$NON-NLS-1$ //$NON-NLS-2$
		RWorkspace.getRWorkspaceInstance().eval(symbol + "[" + rowToChange + ",] <- newrow"); //$NON-NLS-1$ //$NON-NLS-2$

		this.removeRows(Arrays.copyOfRange(rows, 1, rows.length)); // remove rows minus the first row to remove

		return true;
	}

	/**
	 * Fusion rows.
	 *
	 * @param cols the cols to merge
	 * @throws StatException
	 */
	public boolean mergeCols(int[] cols) throws StatException {
		if (cols.length < 2) return false;
		Arrays.sort(cols);
		// System.out.println("fusion rows: "+Arrays.toString(rows)); //$NON-NLS-1$
		int colToChange = cols[0] + 1;
		rw.addVectorToWorkspace("colsToMerge", ArrayIndex.zeroToOneBasedIndex(cols, cols.length)); //$NON-NLS-1$
		RWorkspace.getRWorkspaceInstance().eval("newcol <- rowSums(" + symbol + "[,colsToMerge])"); //$NON-NLS-1$ //$NON-NLS-2$
		RWorkspace.getRWorkspaceInstance().eval(symbol + "[," + colToChange + "] <- newcol"); //$NON-NLS-1$ //$NON-NLS-2$

		this.removeCols(Arrays.copyOfRange(cols, 1, cols.length)); // remove rows minus the first row to remove

		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#get(int, int)
	 */
	@Override
	public double get(int row, int col) throws RException, RWorkspaceException {
		int ncol = this.getNColumns();
		int nrow = this.getNRows();

		if (row < 0 || row >= nrow) {
			// throw new IllegalArgumentException(Messages.MatrixImpl_10 + row + Messages.MatrixImpl_11 + nrow + ")"); //$NON-NLS-3$ //$NON-NLS-1$ //$NON-NLS-1$
		}
		if (col < 0 || col >= ncol) {
			// throw new IllegalArgumentException(Messages.MatrixImpl_13 + col + Messages.MatrixImpl_14 + ncol + ")"); //$NON-NLS-3$ //$NON-NLS-1$ //$NON-NLS-1$
		}
		REXP r = rw.eval(symbol
				+ "[" + ArrayIndex.zeroToOneBasedIndex(row) + ", " + ArrayIndex.zeroToOneBasedIndex(col) + " ];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (r.isInteger()) {
			try {
				return r.asInteger();
			}
			catch (REXPMismatchException e) {
				throw new RWorkspaceException(e);
			}
		}
		else if (r.isComplex()) {
			try {
				return r.asDouble();
			}
			catch (REXPMismatchException e) {
				throw new RWorkspaceException(e);
			}
		}
		else if (r.isNumeric()) {
			try {
				return r.asDouble();
			}
			catch (REXPMismatchException e) {
				throw new RWorkspaceException(e);
			}
		}
		else if (r.isNull()) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.rObjectEvaluatedToNull);
		}
		else {
			// System.out.println("row: " + row);
			// System.out.println("col: " + col);
			// System.out.println("nrow: " + nrow);
			// System.out.println("ncol: " + ncol);
			// System.out.println(rw.eval("print(" + getSymbol() + ");"));
			throw new IllegalArgumentException(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.unknownTypeP0, r.getAttribute(StatsEngineCoreMessages.mode)));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getCol(int)
	 */
	@Override
	public Vector getCol(int index) throws RException, RWorkspaceException {
		int ncol = this.getNColumns();

		if (index < 0 || index >= ncol) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.columnIndex, index, ncol));
		}
		String colName = createSymbole(Vector.class);
		rw.voidEval(colName + "<- " + symbol + "[ ," + (index + 1) + "];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return new VectorImpl(colName);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getCol(java.lang.String)
	 */
	@Override
	public Vector getCol(String column) throws RWorkspaceException {
		String colName = createSymbole(Vector.class);
		rw.voidEval(colName + "<- " + symbol + "[ ,\"" + column + "\"];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return new VectorImpl(colName);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getColNames()
	 */
	@Override
	public Vector getColNames() {
		try {
			return new VectorImpl("colnames(" + this.getSymbol() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (RWorkspaceException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}
	}

	public int[][] getIntData() {
		try {
			REXP rexp = RWorkspace.getRWorkspaceInstance().eval(this.symbol);
			return asIntMatrix(rexp);
		}
		catch (RWorkspaceException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (REXPMismatchException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getNColumns()
	 */
	@Override
	public int getNColumns() {
		try {
			return RWorkspace.getRWorkspaceInstance().eval("dim(" + symbol + ")[2]").asInteger(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (Exception e) {
			Log.severe(StatsEngineCoreMessages.bind("Matrix: failed to get nrow: {0}.", e));
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getNRows()
	 */
	@Override
	public int getNRows() {
		try {
			return RWorkspace.getRWorkspaceInstance().eval("dim(" + symbol + ")[1]").asInteger(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (Exception e) {
			Log.severe(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.matrixColonFailedToGetNrowP0, e));
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getRow(int)
	 */
	@Override
	public Vector getRow(int index) throws RException, RWorkspaceException {
		int nrow = getNRows();
		if (index < 0 || index >= nrow) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.rowIndex, index, nrow));
		}
		String rowName = createSymbole(Vector.class);
		rw.voidEval(rowName + "<- " + symbol + "[" + (index + 1) + ", ];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return new VectorImpl(rowName);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getRow(java.lang.String)
	 */
	@Override
	public Vector getRow(String row) throws RWorkspaceException {
		String rowName = createSymbole(Vector.class);
		rw.voidEval(rowName + "<- " + symbol + "[ \"" + row + "\" , ];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return new VectorImpl(rowName);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.Matrix#getRowNames()
	 */
	@Override
	public Vector getRowNames() {
		try {
			return new VectorImpl("rownames(" + this.getSymbol() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (RWorkspaceException e) {
			org.txm.utils.logger.Log.printStackTrace(e);
			return null;
		}
	}


	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.ContingencyTable#getRowMargin()
	 */
	public int[] getRowMargins() throws StatException {
		try {
			return rw.eval("rowSums(" + this.symbol + ");").asIntegers(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (REXPMismatchException e) {
			throw new StatException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.stat.data.ContingencyTable#getRowMargin()
	 */
	public int[] getColMargins() throws StatException {
		try {
			return rw.eval("colSums(" + this.symbol + ");").asIntegers(); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (Exception e) {
			throw new StatException(e);
		}
	}

	/**
	 * Import data.
	 *
	 * @param f the f
	 * @throws RWorkspaceException
	 * @throws REXPMismatchException
	 */
	public boolean importData(File f, String encoding, String colseparator, String txtseparator) throws RWorkspaceException, REXPMismatchException {

		if (this instanceof RResult rez && (f.getName().endsWith(".rds") || f.getName().endsWith(".Rdata"))) {
			return RResult.importRdataOrRDS(rez, f);
		}

		String path = f.getAbsolutePath();

		if (OSDetector.isFamilyWindows()) {
			path = path.replace("\\", "\\\\"); //$NON-NLS-1$ //$NON-NLS-2$
		}

		/*
		 * t2 <- as.matrix(read.table(file = "/home/mdecorde/TEMP/export (copie).csv", row.names=1, skip=1));
		 * header <-scan(file = "/home/mdecorde/TEMP/export (copie).csv", nlines=1, sep="\t", fileEncoding="UTF-8", what=character())
		 * header <- header[2:length(header)];
		 * t3 <- t2[1:nrow(t2),];
		 * colnames(t3) <- header;
		 */
		//		rw.voidEval("tmpmat <- as.matrix(read.table(file=\"" + path + "\", sep=\"" + colseparator + "\", row.names=1, skip=1, fileEncoding=\"" + encoding + "\"));"); //$NON-NLS-1$ //$NON-NLS-2$
		//		rw.voidEval("tmpheader <- scan(file=\"" + path + "\", what = character(), nlines=1, sep=\"" + colseparator + "\", fileEncoding=\"" + encoding + "\");"); //$NON-NLS-1$ //$NON-NLS-2$
		//		rw.voidEval("tmpheader <- tmpheader[2:length(tmpheader)];"); //$NON-NLS-1$
		//		rw.voidEval("tmpmat <- tmpmat[1:nrow(tmpmat),];"); //$NON-NLS-1$
		//		rw.voidEval("colnames(tmpmat) <- tmpheader"); //$NON-NLS-1$
		//		rw.eval(symbol + "<- tmpmat"); //$NON-NLS-1$

		try {
			TableReader reader = new TableReader(f, null, colseparator, encoding);

			reader.readHeaders();
			String[] header = reader.getHeaders();
			
			int ncols = header.length;
			for (int i = 0 ; i < header.length ; i++) { // ensure all header cells are set
				if (header[i] == null) {
					ncols = i;
					break;
				}
			}

			int nValidColumns = 0;
			for (int i = 1; i < ncols; i++) { // ignore first column
				if (header[i].length() > 0) nValidColumns++;
			}
			String[] rheader = new String[nValidColumns];
			int nInserted = 0;
			for (int i = 1; i < ncols; i++) { // ignore first column
				if (header[i].length() > 0) rheader[nInserted++] = header[i];
			}
			if (ncols == 1) {
				Log.warning("LexicalTable data import : no column found in: " + f);
				return false;
			}
			rw.voidEval("tmpmat <- matrix(ncol=" + (nValidColumns) + ");"); //$NON-NLS-1$ //$NON-NLS-2$
			int nInsertedRows = 0;
			ArrayList<String> rownames = new ArrayList<>();
			int nRead = 0;
			while (reader.readRecord()) {
				LinkedHashMap<String, String> record = reader.getRecord();
				nRead++;
				if (record.isEmpty()) continue;
				
				double[] values = new double[nValidColumns];
				int i = 1;
				nInserted = 0;
				for (String h : rheader) {
					String r = record.get(h);

					if (r != null && r.length() > 0) {

						try {
							values[nInserted] = Double.parseDouble(r);
							nInserted++;
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							if (r.length() == 0) break; // not a valid line
						}
					} else {
						break; // not a valid line
					}
				}

				if (nInserted == nValidColumns) {
					nInsertedRows++;
					//System.out.println("values="+Arrays.toString(values)); //$NON-NLS-1$
					rw.addVectorToWorkspace("tmpvec", values); //$NON-NLS-1$
					rw.voidEval("tmpmat <- rbind(tmpmat, tmpvec);"); //$NON-NLS-1$
					rownames.add(record.get(header[0]));
				} else if (nInserted > 0) {
					System.out.println("Warning: wrong line: "+record);
				}
			}
			
			if (nInsertedRows == 0) {
				Log.warning(NLS.bind("No rows inserted with header=", Arrays.toString(header)));
				return false;
			}
			
			rw.voidEval("tmpmat <- tmpmat[-1,];"); // $NON-NLS-1$ remove the null first line //$NON-NLS-1$
			rw.addVectorToWorkspace("tmprownames", rownames.toArray(new String[rownames.size()])); //$NON-NLS-1$
			rw.addVectorToWorkspace("tmpcolnames", rheader); //$NON-NLS-1$
			rw.assignRowNamesToMatrix("tmpmat", "tmprownames"); //$NON-NLS-1$ //$NON-NLS-2$
			rw.assignColNamesToMatrix("tmpmat", "tmpcolnames"); //$NON-NLS-1$ //$NON-NLS-2$

			rw.voidEval(symbol + "<- tmpmat;"); //$NON-NLS-1$
			//rw.voidEval("rm(tmpmat);"); //$NON-NLS-1$
			reader.close();
		}
		catch (Exception e) {
			Log.warning(NLS.bind("LexicalTable data import of {0} error: {1}", f, e));
			Log.printStackTrace(e);
			return false;
		}

		return true;
	}

	public void print() {

		try {
			String[] rowNames = this.getRowNames().asStringsArray();
			String[] colNames = this.getColNames().asStringsArray();
			int[][] data = getIntData();

			// col names
			for (String col : colNames)
				System.out.print("\t" + col); //$NON-NLS-1$
			System.out.println();
			for (int i = 0; i < rowNames.length; i++) {
				System.out.print(rowNames[i]);
				for (int j = 0; j < colNames.length; j++) {
					System.out.print("\t" + data[i][j]); //$NON-NLS-1$
				}
				System.out.println();
			}
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/**
	 * Removes the col.
	 *
	 * @param col the col
	 */
	public void removeCol(int col) {
		// System.out.println("rmv col " + col);
		try {
			REXP r = rw.eval(symbol + " <- " + symbol + "[,-" + ArrayIndex.zeroToOneBasedIndex(col) + "];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Removes the col and delete empty rows.
	 *
	 * @param col the col
	 * @param checkEmptyLines the check empty lines
	 * @throws StatException
	 */
	public void removeCol(int col, boolean checkEmptyLines) throws StatException {
		// System.out.println("rmv col " + col);
		removeCol(col);

		// we remove the empty lineslines
		if (checkEmptyLines) {
			removeEmptyRows();
		}
	}

	/**
	 * Removes the cols.
	 *
	 * @param cols the cols
	 * @throws StatException
	 */
	public void removeCols(int[] cols) throws StatException {
		Arrays.sort(cols);
		int index = 0;
		for (int i = cols.length - 1; i >= 0; i--) {
			index = cols[i];
			removeCol(index, false);
		}
	}

	public void removeCols(int[] cols, boolean removeEmptyLines) throws StatException {
		removeCols(cols);
		if (removeEmptyLines) {
			removeEmptyRows();
		}
	}

	/**
	 * Removes the cols.
	 *
	 * @param cols the cols
	 * @throws StatException
	 */
	public void removeCols(List<Integer> cols) throws StatException {
		Collections.sort(cols);
		int index = 0;
		for (int i = cols.size() - 1; i >= 0; i--) {
			index = cols.get(i);
			removeCol(index, false);
		}
		removeEmptyRows();
	}

	//	public void removeCols(List<Integer> cols, boolean removeEmptyLines) throws RWorkspaceException {
	//		removeCols(cols);
	//		if (removeEmptyLines) {
	//			removeEmptyRows();
	//		}
	//	}

	private void removeEmptyCols() throws StatException {
		try {
			int[] colsums = rw.eval("apply(" + symbol + ",2,sum)").asIntegers();// get a vector of line sums //$NON-NLS-1$ //$NON-NLS-2$
			ArrayList<Integer> removecols = new ArrayList<>();
			// get the empty line index
			for (int i = 0; i < colsums.length; i++) {
				if (colsums[i] == 0) {
					removecols.add(i);
				}
			}

			if (removecols.size() > 0) {
				removeCols(removecols);
			}
		}
		catch (REXPMismatchException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Removes the empty lines.
	 * 
	 * @throws StatException
	 */
	public void removeEmptyRows() throws StatException {
		int[] rowsums = this.getRowMargins();
		ArrayList<Integer> rowIndexes = new ArrayList<>();
		// get the empty line index
		for (int i = 0; i < rowsums.length; i++) {
			if (rowsums[i] == 0) {
				rowIndexes.add(i);
			}
		}

		if (rowIndexes.size() > 0) {
			removeRows(rowIndexes);
		}
	}

	/**
	 * Removes the row.
	 *
	 * @param row the row
	 */
	public void removeRow(int row) {
		// dans R theMatrixWithoutRow5 = theMatrix[-5,]
		try {
			REXP r = rw.eval(symbol
					+ " <- " + symbol + "[ -" + ArrayIndex.zeroToOneBasedIndex(row) + ",];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
		// de la liste rows
	}

	/**
	 * Removes the rows.
	 *
	 * @param from the from
	 * @param to the to
	 */
	public void removeRows(int from, int to) {
		int nrow = this.getNRows();
		if (0 <= from && from <= to && to < nrow) {
			for (int i = to; i >= from; i--) {
				removeRow(i);
			}
		}
	}

	/**
	 * Removes the rows indexes.
	 *
	 * @param rows the rows
	 */
	public void removeRows(int[] rows) {
		if (rows.length == 0) return;
		int nrow = this.getNRows();
		try {
			Arrays.sort(rows);
			for (int i = 0; i < rows.length; i++) {
				rows[i] = -(rows[i] + 1);
			}
			int idx = rows[0];
			if (rows.length > 1) {
				int[] subrows = new int[rows.length - 1];
				System.arraycopy(rows, 1, subrows, 0, rows.length - 1);
				rw.addVectorToWorkspace("linesToDelete", subrows); //$NON-NLS-1$
				// System.out.println("all lines to delete: "+Arrays.toString(rows));
				// System.out.println("lines to delete: "+Arrays.toString(subrows));
				REXP r = rw.eval(symbol + " <- " + symbol + "[ linesToDelete,];"); //$NON-NLS-1$ //$NON-NLS-2$
				nrow -= rows.length - 1;
			}

			if (nrow == 2) {
				// 2 lines left
				int last = 1;
				if (idx == -1) last = 2;
				String[] rowNames;
				try {
					rowNames = getRowNames().asStringsArray();
				}
				catch (StatException e) {
					System.out.println(StatsEngineCoreMessages.failedToGetRowNames);
					org.txm.utils.logger.Log.printStackTrace(e);
					return;
				}
				String lastRowName = rowNames[last - 1];
				REXP r = rw.eval(this.symbol + "<-matrix(" + this.symbol + "[" + idx + ",], nrow=1, dimnames = list(rownames=\"" + lastRowName + "\", colnames(" + this.symbol + ")))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				// _removeRow(idx);
				nrow = 1;
			}
			else {
				// System.out.println("remove last row: "+idx);
				_removeRow(idx);
			}
		}
		catch (RWorkspaceException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	public void removeRows(int[] rows, boolean removeEmptyCols) throws StatException {
		removeRows(rows);
		if (removeEmptyCols) {
			removeEmptyCols();
		}
	}

	/**
	 * Removes the rows.
	 *
	 * @param rows the row
	 */
	public void removeRows(List<Integer> rows) {

		Collections.sort(rows);
		// System.out.println("del rows " + row);
		int index = 0;
		int nrow = this.getNRows();
		int diff = Math.abs(rows.size() - nrow);
		if (diff > 1) {
			for (int i = rows.size() - 1; i >= 0; i--) {
				index = rows.get(i);
				removeRow(index);
			}
		}
		else if (diff == 1) {
			for (int i = rows.size() - 2; i >= 0; i--) {
				index = rows.get(i);
				removeRow(index);
			}
			// 2 lines left
			int idx = rows.get(rows.size() - 1);
			int last = 0;
			if (idx == 0)
				last = 1;
			String[] rowNames;
			try {
				rowNames = getRowNames().asStringsArray();
			}
			catch (StatException e) {
				System.out.println(StatsEngineCoreMessages.failedToGetRowNames);
				org.txm.utils.logger.Log.printStackTrace(e);
				return;
			}
			String lastRowName = rowNames[last];
			System.out.println("table<-matrix(table[c(-" + idx + "),],nrow=1,dimnames = list(rownames=" + lastRowName + ",colnames(table)))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		else {
			System.out.println(StatsEngineCoreMessages.cannotDeleteAllLines);
		}
	}

	public void removeRows(List<Integer> rows, boolean removeEmptyCols) throws StatException {
		this.removeRows(rows);
		if (removeEmptyCols) {
			removeEmptyCols();
		}
	}

	/**
	 * Sets the.
	 *
	 * @param row the row
	 * @param col the col
	 * @param value the value
	 */
	public void set(int row, int col, double value) {
		int nrow = this.getNRows();
		int ncol = this.getNColumns();
		if (row < 0 || row >= nrow) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.rowIndex, row, nrow));
		}
		if (col < 0 || col >= ncol) {
			throw new IllegalArgumentException(StatsEngineCoreMessages.bind(StatsEngineCoreMessages.colIndex, col, ncol));
		}
		// appel à R
		try {
			REXP r = rw.eval(symbol
					+ "[" + ArrayIndex.zeroToOneBasedIndex(row) + ", " + ArrayIndex.zeroToOneBasedIndex(col) + " ] <- " + value + ";"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Sets the order.
	 *
	 * @param neworder the neworder
	 * @param reverse the reverse
	 */
	public void setOrder(List<Integer> neworder, Boolean reverse) {
		int nrow = this.getNRows();
		if (neworder.size() != nrow) {
			// System.out.println("MatrixImpl.setOrder: error new order size "+neworder.size()+" != nrow "+nrow);
			return;
		}
		String order = "c("; //$NON-NLS-1$
		if (reverse) {
			for (int i = neworder.size() - 1; i >= 0; i--) {
				order += "" + (neworder.get(i) + 1) + ", "; //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		else {
			for (Integer i : neworder) {
				order += "" + (i + 1) + ", "; //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		order = order.substring(0, order.length() - 2);
		order += ")"; //$NON-NLS-1$
		try {
			// System.out.println("R sort rownames Reverse"+reverse);
			REXP r = rw.eval(symbol + "<- " + symbol + "[" + order + ", ];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			// REXP r2 = rw.eval("rownames("+symbol+") <- rownames(" + symbol + ")[" + order + "];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Sort.
	 *
	 * @param col the col
	 * @param reverse the reverse
	 */
	public void sort(int col, Boolean reverse) {
		try {
			// System.out.println("R sort "+col+" Reverse"+reverse);
			REXP r = rw
					.eval(symbol
							+ "<- " + symbol + "[order(" + symbol + "[ ," + (col + 1) + "], decreasing = " + reverse.toString().toUpperCase() + "), ];;"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Sort row names.
	 *
	 * @param reverse the reverse
	 */
	public void sortRowNames(Boolean reverse) {

		try {
			// System.out.println("R sort rownames Reverse"+reverse);
			REXP r = rw
					.eval(symbol
							+ "<- " + symbol + "[order(rownames(" + symbol + "), decreasing = " + reverse.toString().toUpperCase() + "), ];"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			getRowNames().sort(reverse);
		}
		catch (RWorkspaceException e) {
			Log.printStackTrace(e);
		}
	}

	/**
	 * Export data.
	 *
	 * @param f the file to be written
	 * @param colseparator the colseparator
	 * @param txtseparator the txtseparator
	 */
	public boolean toTxt(File f, String encoding, String colseparator, String txtseparator) {
		try {
			if (f.getName().endsWith(".ods")) { //$NON-NLS-1$
				return toODS(f);
			}
			else if (f.getName().endsWith(".xlsx")) { //$NON-NLS-1$
				return toXLSX(f);
			}
			else {
				String path = f.getAbsolutePath().replace("\\", "\\\\"); //$NON-NLS-1$ //$NON-NLS-2$
				org.txm.statsengine.r.core.RWorkspace.getRWorkspaceInstance().safeEval(
						"write.table(" + symbol + ", file=\"" + path + "\""//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						+ ", col.names = NA, row.names = TRUE, fileEncoding=\"" + encoding + "\"" //$NON-NLS-1$ //$NON-NLS-2$
						+ ", sep=\"" + colseparator + "\");"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		catch (Exception e1) {
			Log.printStackTrace(e1);
			RWorkspace.printLastSafeEvalExpr();
			return false;
		}
		return true;
	}

	/**
	 * Export data in ODS format.
	 *
	 * @param f the file to be written
	 */
	public boolean toXLSX(File f) {
		try {
			WriteExcel wo = new WriteExcel(f);

			ArrayList<String> header = new ArrayList<String>();
			header.add("names"); //$NON-NLS-1$
			header.addAll(Arrays.asList(this.getColNames().asStringsArray()));
			wo.writeHeader(header);

			String[] rownames = this.getRowNames().asStringsArray();
			for (int i = 0; i < rownames.length; i++) {
				wo.writeNamedLine(rownames[i], this.getRow(i).asIntArray());
			}
			wo.save();
		}
		catch (Exception e1) {
			Log.printStackTrace(e1);
			return false;
		}
		return true;
	}

	/**
	 * Export data in ODS format.
	 *
	 * @param f the file to be written
	 */
	public boolean toODS(File f) {
		try {
			WriteODS wo = new WriteODS(f);

			ArrayList<String> header = new ArrayList<String>();
			header.add("names"); //$NON-NLS-1$
			header.addAll(Arrays.asList(this.getColNames().asStringsArray()));
			wo.writeLine(header);

			String[] rownames = this.getRowNames().asStringsArray();
			ArrayList<Object> line = new ArrayList<Object>();
			for (int i = 0; i < rownames.length; i++) {
				line.clear();
				line.add(rownames[i]);
				for (int v : this.getRow(i).asIntArray()) {
					line.add(v);
				}
				wo.writeLine(line);
			}
			wo.save();
		}
		catch (Exception e1) {
			Log.printStackTrace(e1);
			return false;
		}
		return true;
	}
}
