// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-06-15 15:29:16 +0200 (Mon, 15 Jun 2015) $
// $LastChangedRevision: 2989 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.eclipse.osgi.util.NLS;
import org.rosuda.REngine.Rserve.RConnection;
import org.txm.statsengine.r.core.messages.RCoreMessages;
import org.txm.utils.OSDetector;
import org.txm.utils.StreamHog;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * Helper class that consumes output of a process. In addition, it filter output
 * of the REG command on Windows to look for InstallPath registry entry which
 * specif ies the location of R.
 * 
 * @author RServe library
 */

/**
 * simple class that start Rserve locally if it's not running already - see
 * mainly <code>checkLocalRserve</code> method. It spits out quite some
 * debugging outout of the console, so feel free to modif y it for your
 * application if desired.
 * <p>
 * <i>Important:</i> All applications should shutdown every Rserve that they
 * started! Never leave Rserve running if you started it after your application
 * quits since it may pose a security risk. Inform the user if you started an
 * Rserve instance.
 */
public class StartRserve {

	/** shortcut to <code>launchRserve(cmd, "--no-save --slave", "--no-save --slave", false)</code>. */
	public static Process rProcess;

	/**
	 * Launch rserve.
	 *
	 * @param cmd the cmd
	 * @return true, if successful
	 */
	protected static boolean launchRserve(String cmd, int port, boolean debug, String rArgs, String rServeArgs) {
		if (new File(cmd).exists() && !new File(cmd).isDirectory()
				&& new File(cmd).canExecute())
			return launchRserve(cmd,
					"--vanilla --encoding utf8 " + rArgs, "--RS-encoding utf8 --vanilla --encoding utf8 " + rServeArgs, port, debug); //$NON-NLS-1$ //$NON-NLS-2$
		return false; // checkLocalRserve(port, debug, rArgs, rServeArgs);
	}

	/**
	 * attempt to start Rserve. Note: parameters are <b>not</b> quoted, so avoid
	 * using any quotes in arguments
	 *
	 * @param cmd command necessary to start R
	 * @param rargs arguments are are to be passed to R
	 * @param rsrvargs arguments to be passed to Rserve
	 * @param debug the debug
	 * @return <code>true</code> if Rserve is running or was successfully
	 *         started, <code>false</code> otherwise.
	 */
	protected static boolean launchRserve(String cmd, String rargs, String rsrvargs, int port, boolean debug) {
		// System.out.println("LAUNCH R SERVER "+cmd+" "+port+" "+debug);
		try {
			String[] cmdline = null;

			// FIXME: don't forget to remove this Window dedicated code if we manage to use http_proxy command line argument
			// Windows OS
			boolean isWindows = false;
			if (OSDetector.isFamilyWindows()) {
				isWindows = true; /* Windows startup */


				// TODO: old tests for setting proxy in R
				// Windows : set --internet2 option to manage proxy if any

				// // set Proxy configuration using Eclipse RCP stored values
				// String proxy_conf = "";
				// ProxyConf conf = new ProxyConf(RWorkspace.getRWorkspaceInstance());
				// if (conf.mustSetProxyConfiguration()) {
				// proxy_conf = "http_proxy="+conf.getHttpProxyUrl();
				// rsrvargs += " "+proxy_conf; // add the http_proxy parameter
				// Log.info("Run Rserve with proxy configuration: "+proxy_conf);
				// }

				cmdline = new String[] {
						cmd,
						rargs,
						"-e", //$NON-NLS-1$
						"library(Rserve);run.Rserve(" //$NON-NLS-1$
								+ (debug ? "TRUE" : "FALSE")  //$NON-NLS-1$ //$NON-NLS-2$
								+ ",port=" + port + "" //$NON-NLS-1$ //$NON-NLS-2$
								+ ",args='" //$NON-NLS-1$
								+ rsrvargs + "')\" " //$NON-NLS-1$
				};
			}
			// Mac, Linux
			else {
				cmdline = new String[] {
						"/bin/sh", //$NON-NLS-1$
						"-c", //$NON-NLS-1$
						"echo 'library(Rserve);run.Rserve(" //$NON-NLS-1$
								+ (debug ? "TRUE" : "FALSE") //$NON-NLS-1$ //$NON-NLS-2$
								+ ",port=" + port + "" //$NON-NLS-1$ //$NON-NLS-2$
								+ ",args=\"" + rsrvargs //$NON-NLS-1$
								+ "\")'|" + cmd //$NON-NLS-1$
								+ " " + rargs }; //$NON-NLS-1$
			}

			// ProcessBuilder builder = new ProcessBuilder(cmdline);
			// Rserveprocess = builder.start();

			Runtime runtime = Runtime.getRuntime();
			Log.fine(RCoreMessages.bind(RCoreMessages.info_startingRWithCommandLineP0, StringUtils.join(cmdline, " "))); //$NON-NLS-1$
			rProcess = runtime.exec(cmdline);

			// Log.info(Messages.StartRserve_0+Arrays.toString(cmdline));
			StreamHog errStream = new StreamHog(rProcess.getErrorStream(), RWorkspace.isLoggingEvalCommandLines());
			errStream.setName("R-err"); //$NON-NLS-1$
			StreamHog inStream = new StreamHog(rProcess.getInputStream(), RWorkspace.isLoggingEvalCommandLines());
			inStream.setName("R"); //$NON-NLS-1$
			RWorkspace.getRWorkspaceInstance().registerLogger(errStream, inStream);

			if (!rProcess.isAlive()) {
				Log.severe(RCoreMessages.error_failedToStartRServeWithCommand + "process is not alive: " + rProcess.pid());
				return false;
			}
			// if (!debug && !isWindows) Rserveprocess.waitFor();
			// System.out.println("call terminated, let us try to connect ...");
		}
		catch (Exception x) {
			Log.severe(RCoreMessages.error_failedToStartRServeWithCommand + x.getMessage());
			return false;
		}

		Log.fine(RCoreMessages.info_statisticsEngineLaunched);

		try {
			Thread.sleep(200);
		}
		catch (InterruptedException ix) {
		}

		int attempts = 10;
		while (attempts > 0) {
			try {
				// System.out.print("."); //$NON-NLS-1$
				RConnection c = new RConnection("127.0.0.1", port); //$NON-NLS-1$
				c.close();
				return true;
			}
			catch (Exception e2) {
				try {
					Thread.sleep(1500);
				}
				catch (InterruptedException ix) {
				}
			}

			attempts--;
		}
		Log.fine("R connection attemps done."); //$NON-NLS-1$
		return false;
	}

	/**
	 * checks whether Rserve is running and if that's not the case it attempts
	 * to start it using the defaults for the platform where it is run on. This
	 * method is meant to be set-and-forget and cover most default setups. For
	 * special setups you may get more control over R with <code>launchRserve</code> instead.
	 * 
	 * @param rServeArgs
	 * @param rargs
	 *
	 * @return true, if successful
	 */
	protected static boolean checkLocalRserve(int port, boolean debug, String rargs, String rServeArgs) {
		Log.fine(RCoreMessages.info_rPathNotSetTryingToFindIt);
		if (isRserveRunning(port)) {
			Log.fine(NLS.bind(RCoreMessages.error_rserveIsAlreadyRunningOnTheP0Port, port));
			return true;
		}
		// Windows OS
		// if (OSDetector.isFamilyWindows()) {
		// // System.out.println("Windows: query registry to find where R is installed ...");
		// String installPath = null;
		// try {
		// Process rp = Runtime.getRuntime().exec("reg query HKLM\\Software\\R-core\\R"); //$NON-NLS-1$
		// StreamHog regHog = new StreamHog(rp.getInputStream(), true);
		// rp.waitFor();
		// regHog.join();
		// installPath = regHog.getInstallPath();
		// } catch (Exception rge) {
		// Log.severe(RCoreMessages.error_failedToRunREG + rge);
		// return false;
		// }
		// if (installPath == null) {
		// Log.severe(RCoreMessages.error_youNeedRSoftware);
		// return false;
		// }
		// // System.out.println(" Found R in : "+installPath +
		// // "\\bin\\R.exe");
		// return launchRserve(installPath + "\\bin\\R.exe", port, debug, rargs, rServeArgs); //$NON-NLS-1$
		// }

		Log.fine(RCoreMessages.info_tryingToStartRWithRPath);
		if (launchRserve("R", port, debug, rargs, rServeArgs)) { //$NON-NLS-1$
			return true;
		}

		// R not in the PATH env, try with R common installation paths
		String[] paths = {
				"/Library/Frameworks/R.framework/Resources/bin/R", //$NON-NLS-1$
				"/usr/local/lib/R/bin/R", //$NON-NLS-1$
				"/usr/lib/R/bin/R", //$NON-NLS-1$
				"/usr/bin/R", //$NON-NLS-1$
				"/usr/local/bin/R", //$NON-NLS-1$
				"/sw/bin/R", //$NON-NLS-1$
				"/usr/common/bin/R", //$NON-NLS-1$
				"/opt/bin/R" }; //$NON-NLS-1$

		for (String path : paths) {
			File execFile = new File(path);
			Log.fine(RCoreMessages.bind(RCoreMessages.info_tryingToStartRFromP0, path));
			if (execFile.exists() && launchRserve(path, port, debug, rargs, rServeArgs)) {
				return true;
			}
		}

		return false;
		//
		// return (launchRserve("R", port, debug) //$NON-NLS-1$
		// || /* try some common unix locations of R */
		// (( && launchRserve("/Library/Frameworks/R.framework/Resources/bin/R", port, debug)) //$NON-NLS-1$
		// || ((new File("/usr/local/lib/R/bin/R")).exists() && launchRserve("/usr/local/lib/R/bin/R")) //$NON-NLS-1$ //$NON-NLS-2$
		// || ((new File("/usr/lib/R/bin/R")).exists() && launchRserve("/usr/lib/R/bin/R")) //$NON-NLS-1$ //$NON-NLS-2$
		// || ((new File("/usr/local/bin/R")).exists() && launchRserve("/usr/local/bin/R")) //$NON-NLS-1$ //$NON-NLS-2$
		// || ((new File("/sw/bin/R")).exists() && launchRserve("/sw/bin/R")) //$NON-NLS-1$ //$NON-NLS-2$
		// || ((new File("/usr/common/bin/R")).exists() && launchRserve("/usr/common/bin/R")) || ((new File( //$NON-NLS-1$ //$NON-NLS-2$
		// "/opt/bin/R")).exists() && launchRserve("/opt/bin/R"))); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * check whether Rserve is currently running (on local machine and default
	 * port).
	 * 
	 * @return <code>true</code> if local Rserve instance is running,
	 *         <code>false</code> otherwise
	 */
	protected static boolean isRserveRunning(int port) {
		try {
			new RConnection("127.0.0.1", port); //$NON-NLS-1$
			// System.out.println("Rserve is running.");
			// c.close();
			return true;
		}
		catch (Exception e) {
			// System.out.println("First connect try failed with: "+
			// e.getMessage());
		}
		return false;
	}


	public static void main(String[] args) {
		System.out.println("result=" + checkLocalRserve(8212, false, "", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		try {
			System.out.println("Start RServe"); //$NON-NLS-1$
			RConnection c = new RConnection("localhost", 8212); //$NON-NLS-1$
			System.out.println("Stop RServe"); //$NON-NLS-1$
			c.eval("print(1+1)"); //$NON-NLS-1$
			c.eval("2+2"); //$NON-NLS-1$
			c.shutdown();
			System.out.println("Done"); //$NON-NLS-1$
		}
		catch (Exception x) {
			x.printStackTrace();
		}
	}
}
