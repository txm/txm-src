package org.txm.statsengine.r.core.preferences;


import java.io.File;

import org.osgi.framework.Version;
import org.osgi.service.prefs.Preferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.StreamHog;
import org.txm.utils.logger.Log;

/**
 * Default preferences initializer.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class RPreferences extends TXMPreferences {


	public static final String PREFERENCES_PREFIX = "r_"; //$NON-NLS-1$

	/** The Constant R_REMOTE. */
	public static final String IS_MANDATORY = PREFERENCES_PREFIX + "is_mandatory"; //$NON-NLS-1$

	/** The Constant R_PATH_TO_EXECUTABLE. */
	public static final String PATH_TO_EXECUTABLE = PREFERENCES_PREFIX + "path_to_executable"; //$NON-NLS-1$

	/** The Constant R_REMOTE. */
	public static final String REMOTE = PREFERENCES_PREFIX + "remote"; //$NON-NLS-1$

	public static final String DEBUG = PREFERENCES_PREFIX + "debug"; //$NON-NLS-1$

	/** The Constant R_SERVER_ADDRESS. */
	public static final String SERVER_ADDRESS = PREFERENCES_PREFIX + "server_address"; //$NON-NLS-1$

	/** The Constant R_PORT. */
	public static final String PORT = PREFERENCES_PREFIX + "port"; //$NON-NLS-1$

	/** The Constant R_USER. */
	public static final String USER = PREFERENCES_PREFIX + "user"; //$NON-NLS-1$

	/** The Constant R_PASSWORD. */
	public static final String PASSWORD = PREFERENCES_PREFIX + "password"; //$NON-NLS-1$

	// FIXME: SJ: not used anymore?
	/** The Constant R_PACKAGES_PATH. */
	public static final String PACKAGES_PATH = PREFERENCES_PREFIX + "packages_path"; //$NON-NLS-1$

	public static final String RARGS = PREFERENCES_PREFIX + "rargs"; //$NON-NLS-1$

	public static final String RSERVEARGS = PREFERENCES_PREFIX + "rserveargs"; //$NON-NLS-1$

	/** The Constant R_DISABLE. */
	public static final String DISABLE = PREFERENCES_PREFIX + "disable"; //$NON-NLS-1$

	/** The Constant R_FILE_TRANSFERT. */
	public static final String FILE_TRANSFERT = PREFERENCES_PREFIX + "file_transfert"; //$NON-NLS-1$

	public static final String SVG_DEVICE = PREFERENCES_PREFIX + "svg_device"; //$NON-NLS-1$

	/** Last version since R Preferences have been set -> the setting is done only ONE time. */
	public static final String VERSION = "version"; //$NON-NLS-1$

	/**
	 * To show or not not the R eval command lines.
	 */
	public static final String SHOW_EVAL_LOGS = "show_eval_logs"; //$NON-NLS-1$

	public static final String DEFAULT_REPOS = "default_repos"; //$NON-NLS-1$


	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(RPreferences.class)) {
			new RPreferences();
		}
		return TXMPreferences.instances.get(RPreferences.class);
	}

	/**
	 * 
	 * @return
	 */
	public static File getRRootDir() {

		String RFRAGMENT = "org.txm.statsengine.r.core." + System.getProperty("osgi.os"); //$NON-NLS-1$ //$NON-NLS-2$

		// ensure plugin binary files rights are ok
		String os = ""; //$NON-NLS-1$
		String ext = ""; //$NON-NLS-1$
		String osName = System.getProperty("os.name").toLowerCase(); //$NON-NLS-1$
		String extraSubPath = ""; //$NON-NLS-1$
		if (osName.contains("windows")) { //$NON-NLS-1$
			os = "win32"; //$NON-NLS-1$
			extraSubPath = "x64/"; //$NON-NLS-1$
			ext = ".exe"; //$NON-NLS-1$
		}
		else if (osName.contains("mac")) { //$NON-NLS-1$
			os = "macosx"; //$NON-NLS-1$
		}
		else {
			os = "linux32";  //$NON-NLS-1$
			if (System.getProperty("os.arch").contains("64")) //$NON-NLS-1$ //$NON-NLS-2$
				os = "linux64";  //$NON-NLS-1$
		}

		File bundleDir = BundleUtils.getBundleFile(RFRAGMENT);
		if (bundleDir == null) {
			Log.severe(TXMCoreMessages.bind("Error while retrieving {0} bundle directory.", RFRAGMENT));
			return null;
		}
		File rpluginDir = new File(bundleDir, "res"); //$NON-NLS-1$
		File rRootDir = new File(rpluginDir, os);

		return rRootDir;
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		// FIXME: SJ: some code in this method should be done only at TXM first run. MD: maybe move the rights section after the plugin VERSION test. The R files path might changed from a plugin
		// version to another ? The current implementation ensure R is always executable.

		Preferences preferences = this.getDefaultPreferencesNode();

		String RFRAGMENT = "org.txm.statsengine.r.core." + System.getProperty("osgi.os"); //$NON-NLS-1$ //$NON-NLS-2$

		Log.fine("RPreferences.initializeDefaultPreferences()"); //$NON-NLS-1$

		String saved = this.getString(RPreferences.VERSION);
		Version currentVersion = BundleUtils.getBundleVersion(RFRAGMENT); // the RFRAGMENT plugin contains the right version

		// ensure plugin binary files rights are ok
		String os = ""; //$NON-NLS-1$
		String ext = ""; //$NON-NLS-1$
		String osName = System.getProperty("os.name").toLowerCase(); //$NON-NLS-1$
		String extraSubPath = ""; //$NON-NLS-1$
		if (osName.contains("windows")) { //$NON-NLS-1$
			os = "win32"; //$NON-NLS-1$
			extraSubPath = "x64/"; //$NON-NLS-1$
			ext = ".exe"; //$NON-NLS-1$
		}
		else if (osName.contains("mac")) { //$NON-NLS-1$
			os = "macosx"; //$NON-NLS-1$
		}
		else {
			os = "linux32";  //$NON-NLS-1$
			if (System.getProperty("os.arch").contains("64")) //$NON-NLS-1$ //$NON-NLS-2$
				os = "linux64";  //$NON-NLS-1$
		}

		File bundleDir = BundleUtils.getBundleFile(RFRAGMENT);
		if (bundleDir == null) {
			Log.severe(TXMCoreMessages.bind("Error while retrieving {0} bundle directory.", RFRAGMENT));
			return;
		}
		File rpluginDir = new File(bundleDir, "res"); //$NON-NLS-1$
		File rRootDir = new File(rpluginDir, os);
		File rBinRootDir = new File(rRootDir, "bin"); //$NON-NLS-1$
		File execFile = new File(rBinRootDir, extraSubPath + "R" + ext); //$NON-NLS-1$ //$NON-NLS-2$
		File execFile2 = new File(rBinRootDir, "exec/" + extraSubPath + "R" + ext); //$NON-NLS-1$ //$NON-NLS-2$
		if (osName.contains("windows")) { // R second exec file path is different //$NON-NLS-1$
			execFile2 = new File(rBinRootDir, "x64/" + extraSubPath + "R" + ext); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (!execFile.canExecute() && !execFile.setExecutable(true)) { // first test if file is executable then try setting it executable
			Log.warning(TXMCoreMessages.bind("Error while setting execution file rights to: {0}.", execFile.getAbsolutePath()));
		}

		if (!execFile2.canExecute() && !execFile2.setExecutable(true)) { // first test if file is executable then try setting it executable
			Log.warning(TXMCoreMessages.bind("Error while setting execution file rights to: {0}.", execFile2.getAbsolutePath()));
		}

		if (!osName.toLowerCase().contains("windows")) {  //$NON-NLS-1$
			try {
				Log.fine(TXMCoreMessages.bind("Setting execution file rights to: {0}.", rBinRootDir.getAbsolutePath()));
				Process p = Runtime.getRuntime().exec("chmod -R +x " + rBinRootDir.getAbsolutePath()); //$NON-NLS-1$
				new StreamHog(p.getInputStream(), false);
				new StreamHog(p.getErrorStream(), false);
				p.waitFor();
			}
			catch (Exception e) {
				Log.severe(TXMCoreMessages.bind("Error while setting execution file rights to: {0}.", rBinRootDir.getAbsolutePath()));
				e.printStackTrace();
				return;
			}
		}

		if (!execFile.exists()) {
			Log.severe(TXMCoreMessages.bind("Can not find R executable file: {0}.", execFile));
			return;
		}

		if (!execFile.canExecute()) {
			Log.severe(TXMCoreMessages.bind("File rights setting error, can not execute: {0}.", execFile));
			return;
		}

		preferences.put(DEFAULT_REPOS, "https://cloud.r-project.org"); //$NON-NLS-1$
		preferences.put(PATH_TO_EXECUTABLE, execFile.getAbsolutePath());
		preferences.putBoolean(IS_MANDATORY, false);
		preferences.put(SERVER_ADDRESS, "127.0.0.1"); //$NON-NLS-1$
		preferences.putBoolean(REMOTE, false);
		preferences.putBoolean(DISABLE, false);
		preferences.putBoolean(DEBUG, false);
		preferences.put(PORT, "6311"); //$NON-NLS-1$
		preferences.put(USER, ""); //$NON-NLS-1$
		preferences.put(PASSWORD, ""); //$NON-NLS-1$
		preferences.put(RARGS, ""); //$NON-NLS-1$
		preferences.put(RSERVEARGS, ""); //$NON-NLS-1$
		preferences.putBoolean(FILE_TRANSFERT, false);
		preferences.put(SVG_DEVICE, "svg"); //$NON-NLS-1$
		preferences.putBoolean(SHOW_EVAL_LOGS, false);

		if (saved != null && saved.length() > 0) {
			Version savedVersion = new Version(saved);
			if (currentVersion.compareTo(savedVersion) <= 0) {
				Log.fine("No post-installation of R plugin to do."); //$NON-NLS-1$
				return; // nothing to do
			}
		}
		Log.fine(TXMCoreMessages.bind("Updating R stats engine preferences for plugin version: {0}...", currentVersion)); //$NON-NLS-1$

		put(RPreferences.VERSION, currentVersion.toString());

		Log.fine(TXMCoreMessages.bind("R stats engine preferences set with: {0} and {1}.", rRootDir.getAbsolutePath(), execFile.getAbsolutePath())); //$NON-NLS-1$
	}

}
