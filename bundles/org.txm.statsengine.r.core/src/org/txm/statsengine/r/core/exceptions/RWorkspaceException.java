// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (Mon, 06 May 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core.exceptions;

import org.txm.statsengine.core.StatException;

/**
 * Super-class of all exception related to the R engine.
 * 
 * @author sloiseau
 */
public class RWorkspaceException extends StatException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1630828823146927953L;

	/**
	 * Instantiates a new r workspace exception.
	 */
	public RWorkspaceException() {
		super();
	}

	/**
	 * Instantiates a new r workspace exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public RWorkspaceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new r workspace exception.
	 *
	 * @param message the message
	 */
	public RWorkspaceException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new r workspace exception.
	 *
	 * @param cause the cause
	 */
	public RWorkspaceException(Throwable cause) {
		super(cause);
	}

	public String getMessage() {
		return "R error: " + super.getMessage(); //$NON-NLS-1$
	}

}
