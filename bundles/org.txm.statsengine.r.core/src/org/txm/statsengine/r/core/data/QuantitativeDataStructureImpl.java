// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2013-05-06 17:38:43 +0200 (Mon, 06 May 2013) $
// $LastChangedRevision: 2386 $
// $LastChangedBy: mdecorde $ 
//
package org.txm.statsengine.r.core.data;

import org.rosuda.REngine.REXP;
import org.txm.statsengine.core.data.QuantitativeDataStructure;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.core.messages.StatsEngineCoreMessages;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RObjectDoesNotExist;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;

// TODO: Auto-generated Javadoc
/**
 * The root of the hierarchy for all classes wrapping R data.
 * 
 * @author Sylvain Loiseau &lt;sloiseau@ens-lsh.fr&gt;
 */
public abstract class QuantitativeDataStructureImpl implements QuantitativeDataStructure {

	/** The counter. */
	private static int counter = 1;

	/**
	 * An immutable symbol for the name of this data structure in the R
	 * workspace.
	 */
	protected String symbol;

	/** The R workspace. */
	protected final RWorkspace rw;

	/**
	 * This constructor is called by every sub-classes for initializing final
	 * fields header and symbol.
	 *
	 * @throws RWorkspaceException the r workspace exception
	 */
	protected QuantitativeDataStructureImpl() throws RWorkspaceException {
		this.symbol = createSymbole();
		this.rw = RWorkspace.getRWorkspaceInstance();
	}

	/**
	 * This constructor is called by every sub-classes for initializing final
	 * fields header and symbol.
	 *
	 * @param symbol the symbol
	 * @throws RWorkspaceException the r workspace exception
	 */
	protected QuantitativeDataStructureImpl(String symbol)
			throws RWorkspaceException {
		this.rw = RWorkspace.getRWorkspaceInstance();
		/*
		 * if (!rw.containsVariable(symbol)) {
		 * throw new RObjectDoesNotExist(
		 * Messages.QuantitativeDataStructureImpl_0 + symbol
		 * + Messages.QuantitativeDataStructureImpl_1);
		 * }
		 */
		this.symbol = symbol;
	}

	/**
	 * Create an arbitrary symbol for naming this quantitative data structure
	 * into the R workspace.
	 * 
	 * @return a symbol.
	 */
	protected final String createSymbole() {
		return createSymbole(getClass());
	}

	/**
	 * Create an arbitrary symbol for naming this quantitative data structure
	 * into the R workspace given a class name.
	 *
	 * @param aClass the a class
	 * @return the symbol.
	 */
	public final static String createSymbole(Class<?> aClass) {
		int hash = counter++;
		// warning : always use the absolute value since the hashCode may
		// be a negative value; if turned into string, a negative value will
		// start with "-"
		// and produce an illegal R variable name. SL, 2009 05 22.
		String prefix = aClass.getSimpleName();
		if (prefix.endsWith("Impl")) //$NON-NLS-1$
			prefix = prefix.substring(0, prefix.length() - 4);
		String s = prefix + String.valueOf(hash);
		return s;
	}

	/**
	 * Remove the object from the R workspace.
	 *
	 * @throws RWorkspaceException the r workspace exception
	 */
	protected void removeFromRWorkspace() throws RWorkspaceException {
		if (!rw.containsVariable(symbol)) {
			throw new RObjectDoesNotExist(
					StatsEngineCoreMessages.cannotDeleteANonexistingObject);
		}
		rw.voidEval("rm(" + symbol + ")"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	// @Override
	// /**
	// * Provide information about the header of this object and basic snapshot
	// of its content using the <code>str</code> R function.
	// */
	// public String toString() {
	// StringBuffer sb = new StringBuffer();
	// sb.append(header.toString());
	// // String representation = "";
	// // try {
	// // NO
	// // representation = rw.eval("str(" + symbol + ")").toString();
	// // } catch (REXPMismatchException e) {
	// // representation = " [No representation available for this object.]";
	// // } catch (RWorkspaceException e) {
	// // representation = " [No representation available for this object.]";
	// // }
	// return sb.toString();
	// }

	/**
	 * Add the given key-value attribute pair to the wrapped R object.
	 *
	 * @param name the name
	 * @param vals the vals
	 * @throws RWorkspaceException the r workspace exception
	 */
	protected void setAttribute(String name, String[] vals)
			throws RWorkspaceException {
		Vector attr = new VectorImpl(vals);

		StringBuffer exp = new StringBuffer();
		exp.append("attr("); //$NON-NLS-1$
		exp.append(symbol);
		exp.append(","); //$NON-NLS-1$
		exp.append(name);
		exp.append(")"); //$NON-NLS-1$
		exp.append("<-"); //$NON-NLS-1$
		exp.append(attr.getSymbol());
		exp.append(";"); //$NON-NLS-1$

		rw.voidEval(exp.toString());
		rw.voidEval("rm(" + attr.getSymbol() + ");"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Get an attribute on a R object.
	 *
	 * @param name of the attribute
	 * @return the attribute as a REXP object, since the attribute may be every
	 *         data type.
	 * @throws RWorkspaceException the r workspace exception
	 */
	protected REXP getAttribute(String name) throws RWorkspaceException {
		StringBuffer exp = new StringBuffer();
		exp.append("attr("); //$NON-NLS-1$
		exp.append(symbol);
		exp.append(","); //$NON-NLS-1$
		exp.append(name);
		exp.append(")"); //$NON-NLS-1$
		exp.append(";"); //$NON-NLS-1$

		return rw.eval(exp.toString());
	}

	/**
	 * Gets the symbol, ie. the name of the data structure in R.
	 * 
	 * @return the symbol
	 */
	@Override
	public String getSymbol() {
		return symbol;
	}
}
