package org.txm.statsengine.r.core;

import java.io.File;

import org.txm.PostTXMHOMEInstallationStep;
import org.txm.Toolbox;
import org.txm.objects.Workspace;
import org.txm.utils.BundleUtils;

public class InstallRUserPart extends PostTXMHOMEInstallationStep {

	@Override
	public boolean do_install(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());
		File dir = new File(txmhomedir, "R/libraries"); //$NON-NLS-1$
		dir.mkdirs();

		File scriptsDirectory = new File(dir, "scripts"); //$NON-NLS-1$
		scriptsDirectory.mkdirs();
		BundleUtils.copyFiles("org.txm.statsengine.r.core", "", "", "R/", scriptsDirectory, true); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		BundleUtils.getBundleFile("org.txm.statsengine.r.core"); //$NON-NLS-1$

		return dir.exists() && scriptsDirectory.exists();
	}

	@Override
	public String getName() {
		return "R (org.txm.statengine.r.core)"; //$NON-NLS-1$
	}

	@Override
	public boolean needsReinstall(Workspace workspace) {
		File txmhomedir = new File(Toolbox.getTxmHomePath());
		File dir = new File(txmhomedir, "R/libraries"); //$NON-NLS-1$
		return !dir.exists();
	}
}
