package org.txm.statsengine.r.core;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.results.TXMResult;
import org.txm.statsengine.core.StatsEngine;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.statsengine.r.core.messages.RCoreMessages;
import org.txm.statsengine.r.core.preferences.RPreferences;
import org.txm.utils.OSDetector;
import org.txm.utils.StreamHog;
import org.txm.utils.logger.Log;

/**
 * R statistics engine. Contains methods to start, stop, kill, etc. the R and Rserve process.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class RStatsEngine extends StatsEngine {


	/** The state of the stats engine. */
	private static boolean started = false;


	private static boolean mandatory = false;


	public RStatsEngine() {
		// no instantiation
	}


	/**
	 * Starts the stats engine if the engine is not remote then launch Rserve.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean start(IProgressMonitor monitor) {
		try {
			if (monitor != null) {
				monitor.subTask("Starting R Statistics Engine...");
			}

			if (RPreferences.getInstance().getBoolean(RPreferences.DISABLE)) {
				Log.severe("Warning, R Statistics Engine is disabled.");
				return false;
			}

			// try launching R server and connecting to it
			int port = RPreferences.getInstance().getInt(RPreferences.PORT);
			String user = RPreferences.getInstance().getString(RPreferences.USER);
			String password = RPreferences.getInstance().getString(RPreferences.PASSWORD);
			boolean remote = RPreferences.getInstance().getBoolean(RPreferences.REMOTE);
			boolean debug = RPreferences.getInstance().getBoolean(RPreferences.DEBUG);
			mandatory = RPreferences.getInstance().getBoolean(RPreferences.IS_MANDATORY);

			Log.fine(RCoreMessages.bind(RCoreMessages.info_startingStatsEngineP0P1P2P3, new Object[] { user, remote, port, debug }));
			// System.out.println("test remote :" + R_PATH_TO_EXECUTABLE + ":" + properties.getProperty(R_PATH_TO_EXECUTABLE));
			if (!remote) {
				// start Rserve executable
				started = RWorkspace.startExec(
						RPreferences.getInstance().getString(RPreferences.PATH_TO_EXECUTABLE),
						port,
						debug,
						RPreferences.getInstance().getString(RPreferences.RARGS),
						RPreferences.getInstance().getString(RPreferences.RSERVEARGS));

				if (started) { // try connecting to R witout login
					started = RWorkspace.connect("127.0.0.1", port); //$NON-NLS-1$
				}
			}
			else { // try connecting to R, with login and password
				started = RWorkspace.connect(RPreferences.getInstance().getString(RPreferences.SERVER_ADDRESS), port, user, password);
			}

			if (started) { // post configuration of R
				String rPackagesPath = RPreferences.getInstance().getString(RPreferences.PACKAGES_PATH);
				started = RWorkspace.getRWorkspaceInstance().postConfiguration(rPackagesPath);
			}

			// System.out.println("try to set R_FILE_TRANSFERT to " + RPreferences.getInstance().getBoolean(RPreferences.R_FILE_TRANSFERT));
			RWorkspace.setUseFileCommunication(RPreferences.getInstance().getBoolean(RPreferences.FILE_TRANSFERT));
			// System.out.println("success");
			// System.out.println("file transfert ? "+RWorkspace.isFileTranfert());

			loadRTransformers();
		}
		catch (RWorkspaceException e) {
			Log.severe(RCoreMessages.error_connectionFailed);
			Log.severe(e.getMessage());
			started = false;
		}

		return started;
	}

	public HashMap<String, RTransformer<? extends TXMResult>> getTransformers() {
		return transformers;
	}

	HashMap<String, RTransformer<? extends TXMResult>> transformers = new HashMap<>();

	private void loadRTransformers() {

		transformers.clear();

		IConfigurationElement[] contributions = RegistryFactory.getRegistry().getConfigurationElementsFor(RTransformer.class.getName());

		Log.fine(TXMCoreMessages.bind("Looking for R transformers contributions with extension point id ''{0}''...", RTransformer.class.getName()));
		Log.fine(TXMCoreMessages.bind("{0} transformers(s) found.", contributions.length));

		for (int i = 0; i < contributions.length; i++) {
			try {
				@SuppressWarnings("unchecked")
				RTransformer<? extends TXMResult> transformer = (RTransformer<? extends TXMResult>) contributions[i].createExecutableExtension("class"); //$NON-NLS-1$

				Log.finer(TXMCoreMessages.bind("Registering R transformer: {0}...", transformer));

				transformers.put(contributions[i].getAttribute("type"), transformer); //$NON-NLS-1$
			}
			catch (Exception e) {
				Log.severe("Error: failed to instantiate " + contributions[i].getName() + ".");
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean stop() {
		try {
			transformers.clear();
			RWorkspace.shutdown();
			RStatsEngine.kill();
			started = false;
		}
		catch (Exception e) {
			Log.severe("Error while closing R.");
			e.printStackTrace();
			return false;
		}
		return true;
	}



	/**
	 * kill Rserve process.
	 */
	public static void kill() {

		if (!RPreferences.getInstance().getBoolean(RPreferences.REMOTE)) {

			try {
				if (StartRserve.rProcess != null) {
					Log.finest("Quiting R process..."); //$NON-NLS-1$
					StartRserve.rProcess.destroy();
				}
			}
			catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// FIXME: SJ: became useless ?
			// FIXME: SJ, later: actually it seems R and RTerm are not always terminated on Windows, need to check again before removing this code
			// // Windows OS
			if (OSDetector.isFamilyWindows()) {
				try {
					String execName = RPreferences.getInstance().getString(RPreferences.PATH_TO_EXECUTABLE);
					File f = new File(execName);
					// only use taskkill if the process is running

					String cmd = "tasklist | find /i \"" + f.getName() + "\" && taskkill /IM " + f.getName() + " /F"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					//String cmd = "cmd /c FOR /F \"tokens=5 delims= \" %P IN ('netstat -ano ^| findstr :6330 ^| findstr LISTENING') DO taskkill /F /PID %P"; //$NON-NLS-1$
					Log.finest("Executing command: " + cmd); //$NON-NLS-1$

					Process p = Runtime.getRuntime().exec(cmd);

					// consumes streams
					StreamHog errStream = new StreamHog(p.getErrorStream(), true);
					errStream.setName("ts-kill-r-err"); //$NON-NLS-1$
					StreamHog inStream = new StreamHog(p.getInputStream(), true);
					inStream.setName("ts-kill-r"); //$NON-NLS-1$

					p.waitFor();
					started = false;
				}
				catch (IOException e) {
					Log.warning("Error: " + Log.toString(e));
					try {

						String cmd = NLS.bind("cmd /c FOR /F \"tokens=5 delims= \" %P IN (''netstat -ano ^| findstr :{0} ^| findstr LISTENING'') DO tskill %P", //$NON-NLS-1$
								RPreferences.getInstance().getInt(RPreferences.PORT));
						Log.finest("Executing command: " + cmd); //$NON-NLS-1$

						Process p = Runtime.getRuntime().exec(
								//"tskill Rserve.exe"); //$NON-NLS-1$
								cmd);
						p.waitFor();
						started = false;
					}
					catch (Exception e3) {
						Log.printStackTrace(e3);
					}
				}
				catch (InterruptedException e) {
					Log.severe(NLS.bind("Error while closing R: {0}", e.getLocalizedMessage()));
					Log.printStackTrace(e);
				}
			}
			// Mac, Linux
			else {
				try {
					Process p = Runtime.getRuntime().exec(
							NLS.bind("kill -9 `lsof -t -a -c Rserve-bin.so -i : {0}`", RPreferences.getInstance().getInt(RPreferences.PORT))); //$NON-NLS-1$ //$NON-NLS-2$
					p.waitFor();
					started = false;
				}
				catch (Exception e) {
					Log.severe(NLS.bind("Error while closing R: {0}", e.getLocalizedMessage()));
					org.txm.utils.logger.Log.printStackTrace(e);
				}
			}
		}
	}



	/**
	 * Returns the running state of the stats engine.
	 */
	public static boolean isStarted() {
		return started;
	}


	/**
	 * Checks if the engine is mandatory.
	 * 
	 * @return the mandatory
	 */
	public static boolean isMandatory() {
		return mandatory;
	}


	@Override
	public boolean isRunning() {
		return started;
	}


	@Override
	public boolean initialize() {
		return true;
	}

	@Override
	public String getName() {
		return "R"; //$NON-NLS-1$
	}
}
