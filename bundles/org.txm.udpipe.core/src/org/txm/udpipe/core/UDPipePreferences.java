package org.txm.udpipe.core;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.osgi.framework.Version;
import org.osgi.service.prefs.Preferences;
import org.txm.Toolbox;
import org.txm.core.preferences.TXMPreferences;
import org.txm.utils.BundleUtils;
import org.txm.utils.io.FileCopy;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

/**
 * UDPipe preferences initializer.
 * 
 * @author mdecorde
 *
 */
public class UDPipePreferences extends TXMPreferences {

	/**
	 * contains the last bundle version setting the TreeTagger models directory
	 */
	public static final String INSTALLED_MODELS_VERSION = "installed_models_version"; //$NON-NLS-1$

	/**
	 * Models path.
	 */
	public static final String MODELS_PATH = "models_path"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(UDPipePreferences.class)) {
			new UDPipePreferences();
		}
		return TXMPreferences.instances.get(UDPipePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		// FIXME: SJ: some code in this method should only be done at first run or update (eg. modifying chmod, etc.)

		// Default preferences if no org.txm.udpipe.core fragment is found
		Preferences preferences = this.getDefaultPreferencesNode();

		// FIXME: SJ: became useless since we embed it in fragments?
		String installPath = "/usr/lib/UDPipe"; // "System.getProperty("osgi.user.area") + "/TXM/udpipe"; //$NON-NLS-1$
		if (System.getProperty("os.name").contains("Windows")) { //$NON-NLS-1$
			installPath = "C:/Program Files/UDPipe"; //$NON-NLS-1$
		}
		else if (System.getProperty("os.name").contains("Mac")) { //$NON-NLS-1$
			installPath = "/Applications/UDPipe"; //$NON-NLS-1$
		}

		preferences.put(MODELS_PATH, installPath + "/models"); //$NON-NLS-1$

		// FIXME: need to validate this code + need to check if it's still useful
		String mversion = UDPipePreferences.getInstance().getString(INSTALLED_MODELS_VERSION);

		// if TXM is launch for the first time bversion and mversion valus are empty
		if (mversion == null || mversion.equals("")) {

			// Restore previous TreeTagger preferences
			File previousPreferenceFile = new File(System.getProperty("java.io.tmpdir"), "org.txm.rcp.prefs"); //$NON-NLS-1$ //$NON-NLS-2$

			if (System.getProperty("os.name").indexOf("Mac") >= 0) { //$NON-NLS-1$ //$NON-NLS-2$
				previousPreferenceFile = new File("/tmp/org.txm.rcp.prefs"); //$NON-NLS-1$
			}

			if (previousPreferenceFile.exists()) {
				try {
					System.out.println("Restoring preferences (from " + previousPreferenceFile + ")."); //$NON-NLS-1$ //$NON-NLS-2$
					Properties previousProperties = new Properties();
					BufferedReader reader = IOUtils.getReader(previousPreferenceFile, "ISO-8859-1"); //$NON-NLS-1$
					previousProperties.load(reader);

					String[] keys = { MODELS_PATH };
					for (String k : keys) {
						if (previousProperties.getProperty(previousProperties.getProperty(k)) != null) {
							preferences.put(k, installPath);
							UDPipePreferences.getInstance().put(k, previousProperties.getProperty(k));
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}

			UDPipePreferences.getInstance().put(INSTALLED_MODELS_VERSION, "0.0.0"); //$NON-NLS-1$

			mversion = UDPipePreferences.getInstance().getString(INSTALLED_MODELS_VERSION);
		}

		// look for org.txm.udpipe.core.<osname> fragment
		String mfragmentid = "org.txm.udpipe.core.models"; //$NON-NLS-1$
		String bfragmentid = "org.txm.udpipe.core"; //$NON-NLS-1$
		String osname = System.getProperty("os.name").toLowerCase(); //$NON-NLS-1$
		if (osname.contains("windows")) { //$NON-NLS-1$
			osname = "win32"; //$NON-NLS-1$
		}
		else if (osname.contains("mac os x")) { //$NON-NLS-1$
			osname = "macosx"; //$NON-NLS-1$
		}
		else {
			osname = "linux"; //$NON-NLS-1$
		}
		bfragmentid += "." + osname; //$NON-NLS-1$

		Version currentMVersion = new Version(mversion);

		Version modelsFragmentVersion = BundleUtils.getBundleVersion(mfragmentid);
		if (modelsFragmentVersion != null && modelsFragmentVersion.compareTo(currentMVersion) >= 0) { // udpate models path!
			Log.fine("Updating TreeTagger models path..."); //$NON-NLS-1$
			File path = BundleUtils.getBundleFile(mfragmentid);

			File installModelsDir = new File(path, "res/models"); //$NON-NLS-1$
			File modelsDir = new File(Toolbox.getTxmHomePath(), "udpipe-models"); //$NON-NLS-1$
			modelsDir.mkdirs();
			try {
				FileCopy.copyFiles(installModelsDir, modelsDir);
				preferences.put(MODELS_PATH, modelsDir.getAbsolutePath());
				UDPipePreferences.getInstance().put(INSTALLED_MODELS_VERSION, modelsFragmentVersion.toString());
				Log.fine("Done."); //$NON-NLS-1$
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
