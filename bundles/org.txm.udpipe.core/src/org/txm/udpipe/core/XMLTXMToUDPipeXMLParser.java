// Copyright © 2010-2013 ENS de Lyon.
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate: 2017-01-24 18:11:42 +0100 (mar. 24 janv. 2017) $
// $LastChangedRevision: 3400 $
// $LastChangedBy: mdecorde $
//
package org.txm.udpipe.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cz.cuni.mff.ufal.udpipe.Sentence;
import cz.cuni.mff.ufal.udpipe.Sentences;
import cz.cuni.mff.ufal.udpipe.Word;


/**
 * The Class XMLTXMToUDPipeXMLParser to convert XML-TXM files to src files ready to be processed by UDPipe.
 *
 * @author mdecorde
 *         build the UDPipe source
 */

public class XMLTXMToUDPipeXMLParser {

	/** The url. */
	private URL url;

	/** The input data. */
	private InputStream inputData;

	/** The factory. */
	private XMLInputFactory factory;

	/** The parser. */
	private XMLStreamReader parser;

	private Sentences sentences;

	private Sentence sentence;

	private int nSentenceTagFound = 0;

	/**
	 * Instantiates a new builds the tt src.
	 * uses XML-TXM V2
	 *
	 * @param url the url of the file to process
	 */
	public XMLTXMToUDPipeXMLParser(URL url) {
		try {
			this.url = url;
			inputData = url.openStream();
			factory = XMLInputFactory.newInstance();
			parser = factory.createXMLStreamReader(inputData);
			sentences = new Sentences();
			sentence = new Sentence();
		}
		catch (XMLStreamException ex) {
			System.out.println(ex);
		}
		catch (IOException ex) {
			System.out.println("IOException while parsing "); //$NON-NLS-1$
		}
	}

	/**
	 * Process.
	 *
	 * @param formtype if multiple form, use this param to choose the correct one, if null takes the first form found
	 * @return true if successful
	 */
	public boolean process(String formtype) {

		boolean inW = false;
		String wordId = "";
		boolean flagform = false; // to catch the content of the form tag
		boolean firstform = false; // to know if its the first form of the w element
		String form = ""; // the content of the form tag
		String lastopenlocalname = "";
		String localname = "";
		boolean inS = false;
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
					case XMLStreamConstants.PROCESSING_INSTRUCTION:
						//System.out.println("PI target="+parser.getPITarget()+" data="+parser.getPIData());
						if (!inS && "txm".equals(parser.getPITarget()) && "</s>".equals(parser.getPIData())) { //$NON-NLS-1$
							if (sentence != null && sentence.getWords().size() > 0) {
								sentences.add(sentence);
							}
							sentence = new Sentence();
							nSentenceTagFound++;
						}
						break;
					case XMLStreamConstants.START_ELEMENT:
						localname = parser.getLocalName();

						switch (localname) {
							case "w": //$NON-NLS-1$
								// firstform = true;
								inW = true;
								wordId = parser.getAttributeValue(null, "id"); //$NON-NLS-1$
								break;
							case "form": //$NON-NLS-1$
								if (inW) {
									// if (firstform) {
									// if (formtype != null) {
									// if(parser.getAttributeCount() > 0
									// && parser.getAttributeValue(0).equals(formtype)) // only one attribute in form, type
									// flagform = true;
									// }
									// else
									flagform = true;
									form = "";
									firstform = false;
									// }
								}
								break;
							case "s": // TreeTagger can use s tags //$NON-NLS-1$
								if (sentence != null && sentence.getWords().size() > 0) {
									sentences.add(sentence);
								}
								sentence = new Sentence();
								nSentenceTagFound++;
								inS = true;
								break;
						}
						break;
					case XMLStreamConstants.END_ELEMENT:
						localname = parser.getLocalName();
						switch (localname) {
							case "w": //$NON-NLS-1$
								inW = false;
								break;
							case "form": //$NON-NLS-1$
								if (inW) { // ensure to process a form inside a w
									flagform = false;
									form = form.trim();
									form = form.replace("\n", "").replace("<", "&lt;"); //$NON-NLS-1$
									Word word = new Word();
									word.setForm(form);
									word.setMisc(UDPipeEngine.XMLIDMISC + wordId);
									sentence.getWords().add(word);
								}
								break;

							case "s": //$NON-NLS-1$
								inS = false;
								break;
						}
						break;

					case XMLStreamConstants.CHARACTERS:
						if (flagform) {
							if (parser.getText().length() > 0)
								form += parser.getText();
						}
						break;
				}
			}

			if (sentence != null && sentence.getWords().size() > 0) {
				sentences.add(sentence);
			}

			parser.close();
			inputData.close();
		}
		catch (Exception ex) {
			System.out.println(ex);
			return false;
		}

		return true;
	}

	public int getNSentenceTagFound() {
		return nSentenceTagFound;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException {

		String rootDir = "~/xml/rgaqcj/"; //$NON-NLS-1$
		// new File(rootDir+"/identity/").mkdir();

		ArrayList<String> milestones = new ArrayList<>();// the tags who
		// you want them
		// to stay
		// milestones
		milestones.add("tagUsage"); //$NON-NLS-1$
		milestones.add("pb"); //$NON-NLS-1$
		milestones.add("lb"); //$NON-NLS-1$
		milestones.add("catRef"); //$NON-NLS-1$

		File srcfile = new File(rootDir + "anainline/", "roland-p5.xml"); //$NON-NLS-1$
		File resultfile = new File(rootDir + "ttsrc/", "roland-p5.tt"); //$NON-NLS-1$
		System.out.println("build ttsrc file : " + srcfile + " to : " + resultfile); //$NON-NLS-1$

		XMLTXMToUDPipeXMLParser builder = new XMLTXMToUDPipeXMLParser(srcfile.toURI().toURL());
		builder.process(null);
		builder.getSentences();
		return;
	}

	public Sentences getSentences() {
		return sentences;
	}
}
