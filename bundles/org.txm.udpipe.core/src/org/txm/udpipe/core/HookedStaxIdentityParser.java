package org.txm.udpipe.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.xml.stream.XMLStreamException;

import org.txm.importer.StaxIdentityParser;

public class HookedStaxIdentityParser extends StaxIdentityParser {

	Hook activeHook = null;

	LinkedHashMap<String, Hook> hooks = new LinkedHashMap<>();

	public HookedStaxIdentityParser(File xmlFile, ArrayList<Hook> hooks) throws IOException, XMLStreamException {
		super(xmlFile);
	}

	@Override
	protected void processNamespace() throws XMLStreamException {

		super.processNamespace();
	}


	@Override
	protected void processStartElement() throws XMLStreamException, IOException {
		super.processStartElement();
	}

	@Override
	protected void processCharacters() throws XMLStreamException {
		super.processCharacters();
	}

	@Override
	protected void processProcessingInstruction() throws XMLStreamException {
		super.processProcessingInstruction();
	}

	@Override
	protected void processDTD() throws XMLStreamException {
		super.processDTD();
	}

	@Override
	protected void processCDATA() throws XMLStreamException {
		super.processCDATA();
	}

	@Override
	protected void processComment() throws XMLStreamException {
		super.processComment();
	}

	@Override
	protected void processEndElement() throws XMLStreamException {
		super.processEndElement();
	}

	@Override
	protected void processEndDocument() throws XMLStreamException {
		super.processEndDocument();
	}

	@Override
	protected void processEntityReference() throws XMLStreamException {
		super.processEntityReference();
	}

	public static class Hook {

		String name;

		HookedStaxIdentityParser parser;

		public Hook(HookedStaxIdentityParser parser, String name) {
			this.parser = parser;
			this.name = name;
		}

		@Override
		public String toString() {
			return this.getClass().toString() + ":" + name; //$NON-NLS-1$
		}

		/**
		 * 
		 * @return true if the hook must be activated
		 */
		public boolean activate() {
			return false;
		}

		public boolean desactivate() {
			return false;
		}
	}
}
