package org.txm.udpipe.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.txm.utils.io.IOUtils;

import cz.cuni.mff.ufal.udpipe.InputFormat;
import cz.cuni.mff.ufal.udpipe.Model;
import cz.cuni.mff.ufal.udpipe.OutputFormat;
import cz.cuni.mff.ufal.udpipe.Sentence;
import cz.cuni.mff.ufal.udpipe.Sentences;
import cz.cuni.mff.ufal.udpipe.Version;
import cz.cuni.mff.ufal.udpipe.Word;
import cz.cuni.mff.ufal.udpipe.Words;

public class UDPipeJavaUtils {

	public static Sentences process(String modelPath, String... strings) {

		Model model = Model.load(modelPath);

		InputFormat tokenizer = model.newTokenizer(""); //$NON-NLS-1$

		// parse(sentence& s, const string& options, string& error)

		Sentences sentences = new Sentences();
		Sentence tmp = new Sentence();
		for (String string : strings) {
			tokenizer.setText(string);
			while (tokenizer.nextSentence(tmp)) {
				sentences.add(tmp);
				tmp = new Sentence();
			}
		}

		for (int iSentence = 0; iSentence < sentences.size(); iSentence++) {
			Sentence sent = sentences.get(iSentence);

			model.parse(sent, "");
			model.tag(sent, "");
			//			System.out.println(toString(sent));
		}

		return sentences;
	}

	public static String toString(Sentences sentences) {
		StringBuilder buffer = new StringBuilder();
		for (int iSentence = 0; iSentence < sentences.size(); iSentence++) {
			if (iSentence > 0) buffer.append("\n"); //$NON-NLS-1$
			buffer.append(toString(sentences.get(iSentence)));
		}
		return buffer.toString();
	}

	public static String toString(Sentence sent) {
		StringBuilder buffer = new StringBuilder();
		Words words = sent.getWords();
		for (int i = 0; i < words.size(); i++) {
			Word w = words.get(i);
			if (i > 0) buffer.append(" "); //$NON-NLS-1$

			buffer.append(w.getForm());
			buffer.append("/" + w.getLemma()); //$NON-NLS-1$
			buffer.append("/" + w.getUpostag()); //$NON-NLS-1$
			buffer.append("/" + w.getXpostag()); //$NON-NLS-1$
			buffer.append("/" + w.getFeats()); //$NON-NLS-1$
			buffer.append("/" + w.getDeps()); //$NON-NLS-1$
			buffer.append("/" + w.getDeprel()); //$NON-NLS-1$
			buffer.append("/" + w.getHead()); //$NON-NLS-1$
			buffer.append("/" + w.getMisc()); //$NON-NLS-1$
		}
		return buffer.toString();
	}

	public static void toCoNLLUFile(File resultCoNLLUFile, File model, String string) throws UnsupportedEncodingException, FileNotFoundException {
		Sentences sentences = process(model.getAbsolutePath(), string);
		OutputFormat of = OutputFormat.newConlluOutputFormat();

		PrintWriter writer = IOUtils.getWriter(resultCoNLLUFile);

		for (int iSentence = 0; iSentence < sentences.size(); iSentence++) {
			Sentence sent = sentences.get(iSentence);
			String content = of.writeSentence(sent);
			writer.println(content);
		}

		writer.println(of.finishDocument());
		writer.close();
	}

	public static String toString(Word w) {
		StringBuilder buffer = new StringBuilder();
		buffer.append(w.getForm());
		buffer.append("/" + w.getLemma()); //$NON-NLS-1$
		buffer.append("/" + w.getUpostag()); //$NON-NLS-1$
		buffer.append("/" + w.getXpostag()); //$NON-NLS-1$
		buffer.append("/" + w.getFeats()); //$NON-NLS-1$
		buffer.append("/" + w.getDeps()); //$NON-NLS-1$
		buffer.append("/" + w.getDeprel()); //$NON-NLS-1$
		buffer.append("/" + w.getHead()); //$NON-NLS-1$
		buffer.append("/" + w.getMisc()); //$NON-NLS-1$
		return buffer.toString();
	}

	public static String toCoNLLUString(File model, String string) {
		Sentences sentences = process(model.getAbsolutePath(), string);
		OutputFormat of = OutputFormat.newConlluOutputFormat();

		StringBuilder sb = new StringBuilder();

		for (int iSentence = 0; iSentence < sentences.size(); iSentence++) {
			Sentence sent = sentences.get(iSentence);
			String content = of.writeSentence(sent);
			sb.append(content);
			sb.append("\n"); //$NON-NLS-1$
		}

		sb.append(of.finishDocument());

		return sb.toString();
	}

	public static void main(String[] args) {
		//		try {
		//			toCoNLLUFile(new File("/tmp/result.conllu"), new File("/home/mdecorde/SOFTWARE/udpipe-1.2.0-bin/bin-linux64/french-gsd-ud-2.4-190531.udpipe"),
		//					"Et un petit test... En deux phrases ? ou trois.");
		//		}
		//		catch (UnsupportedEncodingException | FileNotFoundException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		UDPipeEngine.initializeUDLib(new File("workspace047/org.txm.libs.udpipe")); //$NON-NLS-1$
		System.out.println("VERSION: " + Version.current().toString()); //$NON-NLS-1$
		// /usr/lib/UDPipe/models/fr.udpipe
		// /home/mdecorde/SOFTWARE/udpipe-1.2.0-bin/bin-linux64/french-gsd-ud-2.4-190531.udpipe
		for (String p : new String[] { "SOFTWARE/udpipe-1.2.0-bin/bin-linux64/french-gsd-ud-2.4-190531.udpipe", //$NON-NLS-1$
				"SOFTWARE/udpipe-1.2.0-bin/bin-linux64/french-partut-ud-2.4-190531.udpipe", //$NON-NLS-1$
				"SOFTWARE/udpipe-1.2.0-bin/bin-linux64/french-sequoia-ud-2.4-190531.udpipe", //$NON-NLS-1$
				"SOFTWARE/udpipe-1.2.0-bin/bin-linux64/french-spoken-ud-2.4-190531.udpipe" }) { //$NON-NLS-1$
			System.out.println("M=" + p); //$NON-NLS-1$
			process(p, "Et un petit test... En deux phrases ? ou trois."); //$NON-NLS-1$
		}
	}

	public static void processSentences(String modelPath, Sentences sentences) {
		processSentences(Model.load(modelPath), sentences);
	}

	public static void processSentences(Model model, Sentences sentences) {
		//System.out.println("Processing sent ("+sentences.size()+") with model: " + model);
		for (int iSentence = 0; iSentence < sentences.size(); iSentence++) {
			Sentence sent = sentences.get(iSentence);

			model.tag(sent, ""); //$NON-NLS-1$
			model.parse(sent, ""); //$NON-NLS-1$
			//System.out.println(toString(sent));
		}
	}

	/**
	 * fill a map with values of "id", "form", "lemma", "upos", "xpos", "feats", "head", "deprel", "deps", "misc"
	 * 
	 * @param word
	 * @param prefix
	 * @return
	 */
	public static HashMap<String, String> wordToHashMap(Word word, String prefix) {
		if (prefix == null) prefix = "";

		HashMap<String, String> properties = new HashMap<>();
		properties.put(prefix + "id", Integer.toString(word.getId())); //$NON-NLS-1$
		properties.put(prefix + "form", word.getForm()); //$NON-NLS-1$
		properties.put(prefix + "lemma", word.getLemma()); //$NON-NLS-1$
		properties.put(prefix + "upos", word.getUpostag()); //$NON-NLS-1$
		properties.put(prefix + "xpos", word.getXpostag()); //$NON-NLS-1$
		properties.put(prefix + "feats", word.getFeats()); //$NON-NLS-1$
		properties.put(prefix + "head", Integer.toString(word.getHead())); //$NON-NLS-1$
		properties.put(prefix + "deprel", word.getDeprel()); //$NON-NLS-1$
		properties.put(prefix + "deps", word.getDeps()); //$NON-NLS-1$
		properties.put(prefix + "misc", word.getMisc()); //$NON-NLS-1$

		return properties;
	}
}
