package org.txm.udpipe.core;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Bundle;
import org.txm.core.results.TXMResult;
import org.txm.nlp.core.NLPEngine;
import org.txm.tokenizer.StringTokenizer;
import org.txm.utils.BundleUtils;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;
import org.txm.xml.xmltxm.XMLTXMWordPropertiesInjection;

import cz.cuni.mff.ufal.udpipe.Sentence;
import cz.cuni.mff.ufal.udpipe.Sentences;
import cz.cuni.mff.ufal.udpipe.Version;
import cz.cuni.mff.ufal.udpipe.Word;
import cz.cuni.mff.ufal.udpipe.Words;
import cz.cuni.mff.ufal.udpipe.udpipe_java;


public class UDPipeEngine extends NLPEngine {

	public static final String XMLIDMISC = "XmlID=";

	private File libFile;

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {

		Bundle bundle = BundleUtils.getBundle("org.txm.libs.udpipe");
		File bundleDir = BundleUtils.getBundleFile("org.txm.libs.udpipe");
		if (bundleDir.isDirectory()) {
			libFile = initializeUDLib(bundleDir);
		}
		else {
			URL entry = bundle.getEntry("lib/libudpipe_java.so");
			libFile = new File(entry.toURI().getPath());
			udpipe_java.setLibraryPath(entry.toString());
		}
		return getDetails() != null;
	}

	public static File initializeUDLib(File bundleDir) {
		File libFile;
		if (OSDetector.isFamilyWindows()) {
			libFile = new File(bundleDir, "lib/libudpipe_java.dll");
		}
		else if (OSDetector.isFamilyMac()) {
			libFile = new File(bundleDir, "lib/libudpipe_java.dylib");
		}
		else {
			libFile = new File(bundleDir, "lib/libudpipe_java.so");
		}
		udpipe_java.setLibraryPath(libFile.getAbsolutePath());
		return libFile;
	}

	@Override
	public StringTokenizer getStringTokenizer(String lang) throws Exception {
		return new UDStringTokenizer(lang);
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return Version.current() != null;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public void notify(TXMResult r, String state) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getDetails() {
		return "UDPipe - " + Version.current().toString();
	}

	@Override
	public String getName() {
		return "UDPipe";
	}

	/**
	 * the UD property suffixes, will be used to create the CQP properties like propertiesPrefix + suffix
	 */
	public static String[] propNames = { "id", "form", "lemma", "upos", "xpos", "feats", "head", "deprel", "deps", "misc" };


	@Override
	public boolean processFile(File xmlFile, File binaryCorpusDirectory, HashMap<String, Object> parameters) {
		if (!isRunning()) return false;

		String lang = null;
		Object ps = parameters.get("langs");
		Object p = parameters.get("lang");

		if (p == null && ps == null) {
			Log.warning("Warning: can't annotate. No 'lang' (String) or 'langs' (Map<String, String>) parameter specified in " + parameters);
			return false;
		}

		if (ps != null && ps instanceof Map) {
			Map<?, ?> map = (Map<?, ?>) ps;
			String text_id = xmlFile.getName();
			if (map.get(text_id) != null) {
				lang = map.get(text_id).toString().toLowerCase();
				if (!canAnnotateLang(lang)) {
					Log.warning("Warning: can't annotate text_id=${text_id} with $lang, will use the default lang=$p");
					return false;
				}
			}
		}

		if (lang == null && p == null) {
			System.out.println(NLS.bind("** Error: no 'lang' parameter given: {0}. Aborting TreeTagger annotation.", parameters));
			return false;
		}
		else {
			lang = p.toString();
		}

		if (!canAnnotateLang(lang)) {
			return false;
		}

		File modelsDirectory = new File(UDPipePreferences.getInstance().getString(UDPipePreferences.MODELS_PATH)); // default models directory is set in the Toolbox
		File modelFile = new File(modelsDirectory, lang + ".udpipe");
		//System.out.println("model="+modelFile.getAbsolutePath());

		try {
			// get words
			XMLTXMToUDPipeXMLParser wparser = new XMLTXMToUDPipeXMLParser(xmlFile.toURI().toURL());
			if (!wparser.process(null)) {
				Log.warning("Error while parsing: " + xmlFile);
				return false;
			}

			Sentences sentences = wparser.getSentences();

			// System.out.println("SENTENCES PARSED: " + UDPipeJavaUtils.toString(sentences));
			// tag
			UDPipeJavaUtils.processSentences(modelFile.getAbsolutePath(), sentences);

			// System.out.println("SENTENCES RESULT: " + UDPipeJavaUtils.toString(sentences));

			// update XML-TXM files
			XMLTXMWordPropertiesInjection injector = new XMLTXMWordPropertiesInjection(xmlFile);
			HashMap<String, HashMap<String, String>> rules = new HashMap<>();
			for (int iSentence = 0; iSentence < sentences.size(); iSentence++) {
				Sentence sentence = sentences.get(iSentence);
				Words words = sentence.getWords();
				for (int iWord = 0; iWord < words.size(); iWord++) {
					Word word = words.get(iWord);
					String form = word.getForm();
					if ("<root>".equals(form)) continue;

					String misc = word.getMisc();
					int idx = misc.indexOf(XMLIDMISC);
					int idx2 = misc.indexOf("|", idx + 6);
					if (idx2 < 0) idx2 = misc.length();
					String id = misc.substring(idx + 6, idx2);
					if (id != null && id.length() > 0) {
						HashMap<String, String> properties = UDPipeJavaUtils.wordToHashMap(word, "#ud-");
						rules.put(id, properties);
					}
				}
			}
			File outFile = new File(binaryCorpusDirectory, xmlFile.getName() + ".tmp");
			injector.setProperties(rules);
			if (injector.process(outFile) && outFile.exists()) {
				xmlFile.delete();
				outFile.renameTo(xmlFile);
			}
			else {
				Log.warning("Error while processing: " + xmlFile);
			}
		}
		catch (IOException | XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// update xml-txm files
		return true;
	}

	public static boolean canAnnotateLang(String lang) {
		if (lang == null) {
			Log.warning("** Error: not lang given (null)");
			return false;
		}

		File modelsDirectory = new File(UDPipePreferences.getInstance().getString(UDPipePreferences.MODELS_PATH)); // default models directory is set in the Toolbox
		File modelfile = new File(modelsDirectory, lang + ".udpipe");
		if (!"??".equals(lang) && !modelfile.exists()) {
			Log.warning(NLS.bind("** Error: no {0} model file found for the {1} lang.", modelfile, lang));
			return false;
		}
		return true;
	}
}
