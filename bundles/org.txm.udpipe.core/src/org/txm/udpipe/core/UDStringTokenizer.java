package org.txm.udpipe.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.txm.tokenizer.StringTokenizer;

import cz.cuni.mff.ufal.udpipe.InputFormat;
import cz.cuni.mff.ufal.udpipe.Model;
import cz.cuni.mff.ufal.udpipe.Sentence;
import cz.cuni.mff.ufal.udpipe.Words;


public class UDStringTokenizer implements StringTokenizer {

	protected Model model;

	protected InputFormat tokenizer;


	public UDStringTokenizer(String lang) {
		File modelsDirectory = new File(UDPipePreferences.getInstance().getString(UDPipePreferences.MODELS_PATH)); // default models directory is set in the Toolbox
		File modelFile = new File(modelsDirectory, lang + ".udpipe"); //$NON-NLS-1$
		model = Model.load(modelFile.getAbsolutePath());
		if (model == null) {
			throw new IllegalStateException("Model not constructed for file: " + modelFile); //$NON-NLS-1$
		}
		tokenizer = model.newTokenizer("");
	}

	@Override
	public List<List<String>> processText(String text) {
		ArrayList<List<String>> result = new ArrayList<>();


		tokenizer.setText(text);
		Sentence sent = new Sentence();
		while (tokenizer.nextSentence(sent)) {
			List<String> sresult = new ArrayList<>();
			Words words = sent.getWords();
			for (int iWord = 0; iWord < words.size(); iWord++) {
				sresult.add(words.get(iWord).getForm());
			}
			if (sresult.size() > 0) {
				sresult.remove(0); // remove the <root> element
			}
			if (sresult.size() > 0) {
				result.add(sresult);
			}
		}

		return result;
	}

	@Override
	public boolean doSentences() {

		return true;
	}
}
