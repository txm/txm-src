package org.txm.udpipe.core;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.xml.stream.XMLStreamException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.txm.importer.StaxIdentityParser;
import org.txm.utils.io.IOUtils;

public class UDPipeClientUtils {

	public static void printAll(WebTarget target, String string) {
		String r = target.path("models").request().accept(MediaType.TEXT_XML).get(String.class); //$NON-NLS-1$
		System.out.println(r);

		JSONObject obj;
		try {
			JSONObject models = new JSONObject(r);
			JSONArray names = models.getJSONObject("models").names(); //$NON-NLS-1$
			for (int i = 0; i < names.length(); i++) {
				System.out.println(names.get(i));

				System.out.println(process(target, names.get(i).toString(), string));
			}
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String process(WebTarget target, String model, String text) throws JSONException {
		Form form = new Form();
		form.param("data", text); //$NON-NLS-1$
		form.param("model", model); //$NON-NLS-1$
		form.param("tokenizer", ""); //$NON-NLS-1$ //$NON-NLS-2$
		form.param("tagger", ""); //$NON-NLS-1$ //$NON-NLS-2$
		form.param("parser", ""); //$NON-NLS-1$ //$NON-NLS-2$

		String r = target.path("process").request().accept(MediaType.TEXT_XML).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); //$NON-NLS-1$
		// System.out.println(r.toString());
		JSONObject obj;
		obj = new JSONObject(r);

		return obj.getString("result"); //$NON-NLS-1$
	}


	public static boolean processXMLTEI(final WebTarget target, final String model, File xmlFile, File outFile) throws JSONException, IOException, XMLStreamException {

		StaxIdentityParser parser = new StaxIdentityParser(xmlFile) {

			StringBuilder toProcess = new StringBuilder();

			boolean doProcess = false;

			int s_n = 1;

			@Override
			protected void processCharacters() throws XMLStreamException {
				if (doProcess) {
					String text = parser.getText();
					toProcess.append(text);
				}
				else {
					super.processCharacters();
				}
			}

			@Override
			protected void processStartElement() throws XMLStreamException, IOException {
				tokenizeAndWrite();
				super.processStartElement();

				if (parser.getLocalName().equals("text")) doProcess = true; //$NON-NLS-1$
			}

			@Override
			protected void processEndElement() throws XMLStreamException {
				tokenizeAndWrite();
				super.processEndElement();

				if (parser.getLocalName().equals("text")) doProcess = false; //$NON-NLS-1$
			}

			protected void tokenizeAndWrite() throws XMLStreamException {
				if (!doProcess) {
					toProcess.setLength(0);
					return;
				}
				if (toProcess.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				String str = toProcess.toString().trim();
				if (str.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				try {
					String conllu = UDPipeClientUtils.process(target, model, str);
					// System.out.println(conllu);

					String lines[] = conllu.split("\n"); //$NON-NLS-1$
					boolean sopen = false;
					for (String line : lines) {
						// System.out.println(line);

						if (line.startsWith("# newdoc")) { //$NON-NLS-1$

						}
						else if (line.startsWith("# newpar")) { //$NON-NLS-1$

						}
						else if (line.startsWith("# text")) { //$NON-NLS-1$

						}
						else if (line.startsWith("# sent_id")) { //$NON-NLS-1$
							if (sopen) {
								writer.writeEndElement(); // s
								writer.writeCharacters("\n"); //$NON-NLS-1$
							}
							writer.writeStartElement("s"); //$NON-NLS-1$
							writer.writeAttribute("n", "" + s_n++); //$NON-NLS-1$ //$NON-NLS-2$
							writer.writeCharacters("\n"); //$NON-NLS-1$
							sopen = true;
						}
						else {

							String[] cols = line.split("\t"); //$NON-NLS-1$
							if (cols.length <= 5) continue;

							writer.writeStartElement("w"); //$NON-NLS-1$
							writer.writeAttribute("n", cols[0]); //$NON-NLS-1$
							writer.writeAttribute(model + "-lemma", cols[2]); //$NON-NLS-1$
							writer.writeAttribute(model + "-pos", cols[3]); //$NON-NLS-1$
							writer.writeAttribute(model + "-syntax", cols[7]); //$NON-NLS-1$
							writer.writeCharacters(cols[1]);
							writer.writeEndElement();
							writer.writeCharacters("\n"); //$NON-NLS-1$
						}
					}

					if (sopen) {
						writer.writeEndElement(); // s
						writer.writeCharacters("\n"); //$NON-NLS-1$
					}
					toProcess.setLength(0);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		return parser.process(outFile);
	}

	public static boolean processXML(final WebTarget target, final String model, File xmlFile) throws JSONException, IOException, XMLStreamException {

		StaxIdentityParser parser = new StaxIdentityParser(xmlFile) {

			StringBuilder toProcess = new StringBuilder();

			@Override
			protected void processCharacters() throws XMLStreamException {
				String text = parser.getText();
				toProcess.append(text);
			}

			@Override
			protected void processStartElement() throws XMLStreamException, IOException {
				tokenizeAndWrite();
				super.processStartElement();
			}

			@Override
			protected void processEndElement() throws XMLStreamException {
				tokenizeAndWrite();
				super.processEndElement();
			}

			protected void tokenizeAndWrite() throws XMLStreamException {
				if (toProcess.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				String str = toProcess.toString().trim();
				if (str.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				try {
					String conllu = UDPipeClientUtils.process(target, model, str);
					// System.out.println(conllu);

					String lines[] = conllu.split("\n"); //$NON-NLS-1$
					boolean sopen = false;
					for (String line : lines) {
						// System.out.println(line);

						if (line.startsWith("# newdoc")) { //$NON-NLS-1$

						}
						else if (line.startsWith("# newpar")) { //$NON-NLS-1$

						}
						else if (line.startsWith("# text")) { //$NON-NLS-1$

						}
						else if (line.startsWith("# sent_id")) { //$NON-NLS-1$
							if (sopen) {
								writer.writeEndElement(); // s
								writer.writeCharacters("\n"); //$NON-NLS-1$
							}
							writer.writeStartElement("s"); //$NON-NLS-1$
							writer.writeAttribute("n", line.substring("# sent_id".length() + 1)); //$NON-NLS-1$ //$NON-NLS-2$
							writer.writeCharacters("\n"); //$NON-NLS-1$
							sopen = true;
						}
						else {

							String[] cols = line.split("\t"); //$NON-NLS-1$
							if (cols.length <= 5) continue;

							writer.writeStartElement("w"); //$NON-NLS-1$
							writer.writeAttribute("n", cols[0]); //$NON-NLS-1$
							writer.writeAttribute(model + "lemma", cols[2]); //$NON-NLS-1$
							writer.writeAttribute(model + "pos", cols[3]); //$NON-NLS-1$
							writer.writeAttribute(model + "syntax", cols[7]); //$NON-NLS-1$
							writer.writeCharacters(cols[1]);
							writer.writeEndElement();
							writer.writeCharacters("\n"); //$NON-NLS-1$
						}
					}

					if (sopen) {
						writer.writeEndElement(); // s
						writer.writeCharacters("\n"); //$NON-NLS-1$
					}
					toProcess.setLength(0);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		return parser.process(new File(xmlFile.getParentFile(), "out.xml")); //$NON-NLS-1$
	}

	public static void processTXT(WebTarget target, String model, File txtFile) throws JSONException, IOException {
		Form form = new Form();
		form.param("data", IOUtils.getText(txtFile)); //$NON-NLS-1$
		form.param("model", model); //$NON-NLS-1$
		form.param("tokenizer", ""); //$NON-NLS-1$
		form.param("tagger", ""); //$NON-NLS-1$
		form.param("parser", ""); //$NON-NLS-1$

		String r = target.path("process").request().accept(MediaType.TEXT_XML).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class); //$NON-NLS-1$
		// System.out.println(r.toString());
		JSONObject obj;
		obj = new JSONObject(r);
		String result = obj.getString("result"); //$NON-NLS-1$
		System.out.println(result);
	}
}
