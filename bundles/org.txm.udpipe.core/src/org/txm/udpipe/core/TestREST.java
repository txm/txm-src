package org.txm.udpipe.core;


import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.txm.importer.StaxIdentityParser;
import org.txm.scripts.importer.StaxParser;
import org.txm.utils.io.IOUtils;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TestREST {

	public void workflow(File xmlFile) {
		/*
		 * buildSRC()
		 * --> list of path
		 */
	}

	public static boolean processXMLTEIOpti(final WebTarget target, final String model, File xmlFile, File outFile) throws JSONException, IOException, XMLStreamException,
			ParserConfigurationException {

		StringBuilder srcContent = new StringBuilder();
		ArrayList<String> limits = new ArrayList<>();

		HashSet<String> divTags = new HashSet<>();
		divTags.addAll(Arrays.asList("text", "div", "ab", "p", "lg"));

		StaxParser parser = new StaxParser(xmlFile) {

			StringBuilder toProcess = new StringBuilder();

			StringBuilder currentXPath = new StringBuilder();

			boolean doProcess = false;

			String SLASH = "/";

			@Override
			protected void processStartElement() {
				currentXPath.append(SLASH);
				currentXPath.append(parser.getLocalName());

				if (divTags == null || divTags.size() == 0 || divTags.contains(parser.getLocalName())) tokenizeAndWrite();
				super.processStartElement();

				if (parser.getLocalName().equals("text")) doProcess = true;
			}

			@Override
			protected void processEndElement() {
				if (divTags == null || divTags.size() == 0 || divTags.contains(parser.getLocalName())) tokenizeAndWrite();
				super.processEndElement();

				if (parser.getLocalName().equals("text")) doProcess = false;

				currentXPath.setLength(currentXPath.length() - parser.getLocalName().length() - 1);
			}

			@Override
			protected void processCharacters() {
				if (doProcess) {
					String text = parser.getText();
					toProcess.append(text);
				}
				else {
					// super.processCharacters();
				}
			}

			protected void tokenizeAndWrite() {
				if (!doProcess) {
					toProcess.setLength(0);
					return;
				}
				if (toProcess.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				String str = toProcess.toString().trim();
				if (str.length() == 0) {
					toProcess.setLength(0);
					return;
				}

				srcContent.append(str + "\n\n");
				limits.add(currentXPath.toString());
				toProcess.setLength(0);
			}
		};
		System.out.println("PREPARING SRC...");
		parser.process();

		System.out.println("APPLYING UDPIPE...");
		String conllu = TestREST.process(target, model, srcContent.toString());
		String[] newpars = conllu.split("# newpar\n");
		System.out.println("newpars: " + newpars.length);

		Document tempDoc = DomUtils.emptyDocument();
		Element currentElement = tempDoc.getDocumentElement();

		System.out.println("INJECTING ANNOTATIONS...");
		StaxIdentityParser finalParser = new StaxIdentityParser(xmlFile) {

			StringBuilder toProcess = new StringBuilder();

			boolean doProcess = false;

			int n = 1;

			int s_n = 1;

			boolean inW = false;

			boolean inS = false;

			@Override
			protected void processCharacters() throws XMLStreamException {
				// if (inBlock) {
				if ("form".equals(localname)) {
					appendForm(parser.getText());
				}
				// }
				// else {
				//
				// }
			}

			private void appendForm(String surface) {
				// TODO Auto-generated method stub

			}

			@Override
			protected void processStartElement() throws XMLStreamException, IOException {
				if (divTags == null || divTags.size() == 0 || divTags.contains(parser.getLocalName())) tokenizeAndWrite();

				if ("w".equals(localname)) {
					inW = true;
				}
				else if ("s".equals(localname)) {
					inS = true;
				}
				else {
					super.processStartElement();
				}

				if (parser.getLocalName().equals("text")) doProcess = true;
			}

			@Override
			protected void processEndElement() throws XMLStreamException {
				if (divTags == null || divTags.size() == 0 || divTags.contains(parser.getLocalName())) tokenizeAndWrite();

				if ("w".equals(localname)) {
					inW = false;
				}
				else if ("s".equals(localname)) {
					inS = false;
				}
				else {
					super.processEndElement();
				}

				if (parser.getLocalName().equals("text")) doProcess = false;
			}

			protected void tokenizeAndWrite() throws XMLStreamException {
				if (!doProcess) {
					toProcess.setLength(0);
					return;
				}
				if (toProcess.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				String str = toProcess.toString().trim();
				if (str.length() == 0) {
					toProcess.setLength(0);
					return;
				}

				String conllu = newpars[n++];
				// System.out.println(conllu);
			}
		};

		return finalParser.process(outFile);
	}

	public static boolean processXMLTEI(final WebTarget target, final String model, File xmlFile, File outFile) throws JSONException, IOException, XMLStreamException {

		StaxIdentityParser parser = new StaxIdentityParser(xmlFile) {

			StringBuilder toProcess = new StringBuilder();

			boolean doProcess = false;

			int s_n = 1;

			@Override
			protected void processCharacters() throws XMLStreamException {
				if (doProcess) {
					String text = parser.getText();
					toProcess.append(text);
				}
				else {
					super.processCharacters();
				}
			}

			@Override
			protected void processStartElement() throws XMLStreamException, IOException {
				tokenizeAndWrite();
				super.processStartElement();

				if (parser.getLocalName().equals("text")) doProcess = true;
			}

			@Override
			protected void processEndElement() throws XMLStreamException {
				tokenizeAndWrite();
				super.processEndElement();

				if (parser.getLocalName().equals("text")) doProcess = false;
			}

			protected void tokenizeAndWrite() throws XMLStreamException {
				if (!doProcess) {
					toProcess.setLength(0);
					return;
				}
				if (toProcess.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				String str = toProcess.toString().trim();
				if (str.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				try {
					String conllu = TestREST.process(target, model, str);
					// System.out.println(conllu);

					String lines[] = conllu.split("\n");
					boolean sopen = false;
					for (String line : lines) {
						// System.out.println(line);

						if (line.startsWith("# newdoc")) {

						}
						else if (line.startsWith("# newpar")) {

						}
						else if (line.startsWith("# text")) {

						}
						else if (line.startsWith("# sent_id")) {
							if (sopen) {
								writer.writeEndElement(); // s
								writer.writeCharacters("\n");
							}
							writer.writeStartElement("s");
							writer.writeAttribute("n", "" + s_n++);
							writer.writeCharacters("\n");
							sopen = true;
						}
						else {

							String[] cols = line.split("\t");
							if (cols.length <= 5) continue;

							writer.writeStartElement("w");
							writer.writeAttribute("n", cols[0]);
							writer.writeAttribute(model + "-lemma", cols[2]);
							writer.writeAttribute(model + "-pos", cols[3]);
							writer.writeAttribute(model + "-syntax", cols[7]);
							writer.writeCharacters(cols[1]);
							writer.writeEndElement();
							writer.writeCharacters("\n");
						}
					}

					if (sopen) {
						writer.writeEndElement(); // s
						writer.writeCharacters("\n");
					}
					toProcess.setLength(0);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		return parser.process(outFile);
	}

	public static boolean processXML(final WebTarget target, final String model, File xmlFile) throws JSONException, IOException, XMLStreamException {

		StaxIdentityParser parser = new StaxIdentityParser(xmlFile) {

			StringBuilder toProcess = new StringBuilder();

			@Override
			protected void processCharacters() throws XMLStreamException {
				String text = parser.getText();
				toProcess.append(text);
			}

			@Override
			protected void processStartElement() throws XMLStreamException, IOException {
				tokenizeAndWrite();
				super.processStartElement();
			}

			@Override
			protected void processEndElement() throws XMLStreamException {
				tokenizeAndWrite();
				super.processEndElement();
			}

			protected void tokenizeAndWrite() throws XMLStreamException {
				if (toProcess.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				String str = toProcess.toString().trim();
				if (str.length() == 0) {
					toProcess.setLength(0);
					return;
				}
				try {
					String conllu = TestREST.process(target, model, str);
					// System.out.println(conllu);

					String lines[] = conllu.split("\n");
					boolean sopen = false;
					for (String line : lines) {
						// System.out.println(line);

						if (line.startsWith("# newdoc")) {

						}
						else if (line.startsWith("# newpar")) {

						}
						else if (line.startsWith("# text")) {

						}
						else if (line.startsWith("# sent_id")) {
							if (sopen) {
								writer.writeEndElement(); // s
								writer.writeCharacters("\n");
							}
							writer.writeStartElement("s");
							writer.writeAttribute("n", line.substring("# sent_id".length() + 1));
							writer.writeCharacters("\n");
							sopen = true;
						}
						else {

							String[] cols = line.split("\t");
							if (cols.length <= 5) continue;

							writer.writeStartElement("w");
							writer.writeAttribute("n", cols[0]);
							writer.writeAttribute(model + "lemma", cols[2]);
							writer.writeAttribute(model + "pos", cols[3]);
							writer.writeAttribute(model + "syntax", cols[7]);
							writer.writeCharacters(cols[1]);
							writer.writeEndElement();
							writer.writeCharacters("\n");
						}
					}

					if (sopen) {
						writer.writeEndElement(); // s
						writer.writeCharacters("\n");
					}
					toProcess.setLength(0);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		return parser.process(new File(xmlFile.getParentFile(), "out.xml"));
	}

	public static void processTXT(WebTarget target, String model, File txtFile) throws JSONException, IOException {
		Form form = new Form();
		form.param("data", IOUtils.getText(txtFile));
		form.param("model", model);
		form.param("tokenizer", "");
		form.param("tagger", "");
		form.param("parser", "");

		String r = target.path("process").request().accept(MediaType.TEXT_XML).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class);
		// System.out.println(r.toString());
		JSONObject obj;
		obj = new JSONObject(r);
		String result = obj.getString("result");
		System.out.println(result);
	}

	public static String process(WebTarget target, String model, String text) throws JSONException {
		Form form = new Form();
		form.param("data", text);
		form.param("model", model);
		form.param("tokenizer", "");
		form.param("tagger", "");
		form.param("parser", "");

		String r = target.path("process").request().accept(MediaType.TEXT_XML).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class);
		// System.out.println(r.toString());
		JSONObject obj;
		obj = new JSONObject(r);

		return obj.getString("result");
	}

	public static void main(String[] args) throws JSONException, IOException, XMLStreamException {
		ClientConfig config = new ClientConfig();

		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(getBaseURI());

		// System.out.println(process(target, "fr-gsd", "un test simple"));

		// printAll(target, "un test simple");

		// processTXT(target, "fr-sequoia", new File("/home/mdecorde/xml/voeuxtxt/0001.txt"));

		long time = System.currentTimeMillis();
		File inputDirectory = new File(System.getProperty("user.home"), "xml/ELTECFRAORIG");
		File outDirectory = new File(inputDirectory.getAbsolutePath() + "-UDPIMPED");
		outDirectory.mkdirs();
		File[] xmlFiles = inputDirectory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xml");
			}
		});
		System.out.println(inputDirectory.getAbsolutePath());
		for (File xmlFile : xmlFiles) {
			System.out.println("Processing " + xmlFile);
			processXMLTEI(target, "fr-sequoia", xmlFile, new File(outDirectory, xmlFile.getName()));
		}
		time = System.currentTimeMillis() - time;
		System.out.println("TIME= " + (time));
		System.out.println("TIME PER TEXT= " + (time / xmlFiles.length));

		// long time2 = System.currentTimeMillis();
		// processXMLTEIOpti(target, "fr-sequoia", new File("/home/mdecorde/xml/tdm80j/tdm80j.xml"), new File("/home/mdecorde/xml/tdm80j/tdm80j-out-opti.xml"));
		// System.out.println("TIME2= " + (System.currentTimeMillis() - time2));
	}

	public static void printAll(WebTarget target, String string) {
		String r = target.path("models").request().accept(MediaType.TEXT_XML).get(String.class);
		System.out.println(r);

		JSONObject obj;
		try {
			JSONObject models = new JSONObject(r);
			JSONArray names = models.getJSONObject("models").names();
			for (int i = 0; i < names.length(); i++) {
				System.out.println(names.get(i));

				System.out.println(process(target, names.get(i).toString(), string));
			}
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:7878").build();
	}
}
