package org.txm.udpipe.core;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONException;
import org.txm.utils.io.IOUtils;
import org.txm.xml.DOMIdentityHook;
import org.txm.xml.LocalNamesHookActivator;
import org.txm.xml.SimpleHook;
import org.txm.xml.XMLParser;
import org.txm.xml.XMLProcessor;
import org.txm.xml.XPathsHookActivator;

public class UDPipeProcessor {

	// REST
	private ClientConfig config;

	private Client client;

	private WebTarget target;

	// 1- prepare source
	StringBuilder srcContent = new StringBuilder(); // final SRC content for UDPipe

	ArrayList<String> blockPaths = new ArrayList<>(); // stores the block starting paths -> for further processing

	// 2- store result
	private String[] newpars;

	// 3- reinsject result

	public UDPipeProcessor() {
		this(getBaseURI());
	}

	public UDPipeProcessor(URI adress) {
		config = new ClientConfig();
		client = ClientBuilder.newClient(config);
		target = client.target(getBaseURI());
	}

	public WebTarget getTarget() {
		return target;
	}

	public boolean buildSRC(File xmlFile, List<String> limits) throws IOException, XMLStreamException {
		XMLParser blocksConstructor = new XMLParser(xmlFile);

		LocalNamesHookActivator activator = new LocalNamesHookActivator(null, limits);

		new SimpleHook<XMLParser>("src builder", activator, blocksConstructor) {

			StringBuilder toProcess = new StringBuilder(); // temporally stores text to build blocks

			private String startingPath;

			@Override
			protected void processStartElement() {
			}

			@Override
			protected void processEndElement() {
			}

			@Override
			protected void processCharacters() {
				String text = this.parser.getText();
				toProcess.append(text);
			}

			@Override
			public boolean deactivate() {

				if (toProcess.length() == 0) {
					toProcess.setLength(0);
					return true; // nothing was done
				}
				String str = toProcess.toString().trim();
				if (str.length() == 0) {
					toProcess.setLength(0);
					return true; // really nothing was done
				}

				blockPaths.add(startingPath);
				srcContent.append(str.replaceAll("[\n]++", " ") + "\n\n"); // add content

				toProcess.setLength(0); // reset the buffer
				return true;
			}

			@Override
			public boolean _activate() {
				startingPath = parentParser.getCurrentPath();
				return true;
			}
		};

		if (blocksConstructor.process()) {
			System.out.println("N blocks: " + blockPaths.size());
			IOUtils.write(new File("/home/mdecorde/TEMP/src.txt"), srcContent.toString());
			return true;
		}
		else {
			return false;
		}
	}

	public boolean apply(String model) throws JSONException {
		System.out.println("APPLYING UDPIPE...");
		String conllu = UDPipeJavaUtils.toCoNLLUString(new File(model), srcContent.toString());

		try {
			IOUtils.write(new File("/home/mdecorde/TEMP/applyraw.txt"), conllu);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		newpars = conllu.split("# newpar");
		System.out.println("N newpars: " + newpars.length);

		String[] newpars2 = new String[newpars.length - 1];
		System.arraycopy(newpars, 1, newpars2, 0, newpars.length - 1); // the first newpar is empty (contains only #newdoc)
		newpars = newpars2;
		System.out.println("N newpars: " + newpars.length);
		try {
			IOUtils.write(new File("/home/mdecorde/TEMP/apply.txt"), StringUtils.join(newpars, "\n\n\n"));
			IOUtils.write(new File("/home/mdecorde/TEMP/apply0.txt"), newpars[0]);
			IOUtils.write(new File("/home/mdecorde/TEMP/apply1.txt"), newpars[newpars.length - 1]);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	// Arrays.asList("text", "div", "ab", "p", "lg")
	public boolean inject(File xmlFile, File outFile) throws JSONException, IOException, XMLStreamException {

		XMLProcessor injector = new XMLProcessor(xmlFile);
		XPathsHookActivator activator = new XPathsHookActivator(null, blockPaths);
		DOMIdentityHook hook = new DOMIdentityHook("injector", activator, injector) {

			int n = 0;

			@Override
			public void processDom() {
				// System.out.println("" + n + ": injecting " + newpars[n++] + " in words");
			}
		};

		return injector.process(outFile);
	}
	// System.out.println("INJECTING ANNOTATIONS...");
	// StaxIdentityParser finalParser = new StaxIdentityParser(xmlFile) {
	//
	// StringBuilder toProcess = new StringBuilder();
	//
	// boolean doProcess = false;
	//
	// int n = 1;
	//
	// int s_n = 1;
	//
	// @Override
	// protected void processCharacters() throws XMLStreamException {
	// if (doProcess) {
	// String text = parser.getText();
	// toProcess.append(text);
	// }
	// else {
	// super.processCharacters();
	// }
	// }
	//
	// @Override
	// protected void processStartElement() throws XMLStreamException, IOException {
	// if (divTags == null || divTags.size() == 0 || divTags.contains(parser.getLocalName())) tokenizeAndWrite();
	// super.processStartElement();
	//
	// if (parser.getLocalName().equals("text")) doProcess = true;
	// }
	//
	// @Override
	// protected void processEndElement() throws XMLStreamException {
	// if (divTags == null || divTags.size() == 0 || divTags.contains(parser.getLocalName())) tokenizeAndWrite();
	// super.processEndElement();
	//
	// if (parser.getLocalName().equals("text")) doProcess = false;
	// }
	//
	// protected void tokenizeAndWrite() throws XMLStreamException {
	// if (!doProcess) {
	// toProcess.setLength(0);
	// return;
	// }
	// if (toProcess.length() == 0) {
	// toProcess.setLength(0);
	// return;
	// }
	// String str = toProcess.toString().trim();
	// if (str.length() == 0) {
	// toProcess.setLength(0);
	// return;
	// }
	//
	// String conllu = newpars[n++];
	// // System.out.println(conllu);
	//
	// String lines[] = conllu.split("\n");
	// boolean sopen = false;
	// for (String line : lines) {
	// // System.out.println(line);
	//
	// if (line.startsWith("# newdoc")) {
	//
	// }
	// else if (line.startsWith("# newpar")) {
	//
	// }
	// else if (line.startsWith("# text")) {
	//
	// }
	// else if (line.startsWith("# sent_id")) {
	// if (sopen) {
	// writer.writeEndElement(); // s
	// writer.writeCharacters("\n");
	// }
	// writer.writeStartElement("s");
	// writer.writeAttribute("n", "" + s_n++);
	// writer.writeCharacters("\n");
	// sopen = true;
	// }
	// else {
	// String[] cols = line.split("\t");
	// if (cols.length <= 5) continue;
	//
	// writer.writeStartElement("w");
	// writer.writeAttribute("n", cols[0]);
	// writer.writeAttribute(model + "-lemma", cols[2]);
	// writer.writeAttribute(model + "-pos", cols[3]);
	// writer.writeAttribute(model + "-syntax", cols[7]);
	// writer.writeCharacters(cols[1]);
	// writer.writeEndElement();
	// writer.writeCharacters("\n");
	// }
	// }
	//
	// if (sopen) {
	// writer.writeEndElement(); // s
	// writer.writeCharacters("\n");
	// }
	// toProcess.setLength(0);
	// }
	// };
	//
	// return finalParser.process(outFile);
	// }

	public static void main(String[] args) throws JSONException, IOException, XMLStreamException {

		File i = new File("/home/mdecorde/xml/tdm80j/tdm80j.xml");
		File o = new File("/home/mdecorde/xml/tdm80j/tdm80j-out.xml");

		UDPipeProcessor upp = new UDPipeProcessor();

		// UDPipeUtils.printAll(upp.getTarget(), "un test");

		System.out.println("PREPARE: " + upp.buildSRC(i, Arrays.asList("div", "ab", "p", "lg")));

		System.out.println("APPLY: " + upp.apply("fr-sequoia"));

		System.out.println("INJECTION: " + upp.inject(i, o));

		// System.out.println(process(target, "fr-gsd", "un test simple"));

		// printAll(target, "un test simple");

		// processTXT(target, "fr-sequoia", new File("/home/mdecorde/xml/voeuxtxt/0001.txt"));

		// long time = System.currentTimeMillis();
		// processXMLTEI(target, "fr-sequoia", new File("/home/mdecorde/xml/tdm80j/tdm80j.xml"), new File("/home/mdecorde/xml/tdm80j/tdm80j-out.xml"));
		// System.out.println("TIME1= " + (System.currentTimeMillis() - time));
		//
		// long time2 = System.currentTimeMillis();
		// processXMLTEIOpti(target, "fr-sequoia", new File("/home/mdecorde/xml/tdm80j/tdm80j.xml"), new File("/home/mdecorde/xml/tdm80j/tdm80j-out-opti.xml"));
		// System.out.println("TIME2= " + (System.currentTimeMillis() - time2));
	}

	/**
	 * to use if udpipe server was started with :
	 * ./udpipe_server 7878 fr-sequoia fr-sequoia french-sequoia-ud-2.4-190531.udpipe ""
	 * 
	 * @return default UDPipe URL
	 */
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:7878").build();
	}
}
