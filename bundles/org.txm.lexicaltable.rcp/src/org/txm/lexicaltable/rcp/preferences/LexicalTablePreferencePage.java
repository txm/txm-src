// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.preferences;

import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.lexicaltable.core.messages.LexicalTableCoreMessages;
import org.txm.lexicaltable.core.preferences.LexicalTablePreferences;
import org.txm.lexicaltable.rcp.adapters.LexicalTableAdapterFactory;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * Lexical table preferences page.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class LexicalTablePreferencePage extends TXMPreferencePage {


	@Override
	public void createFieldEditors() {

		IntegerFieldEditor fMin = new IntegerFieldEditor(TXMPreferences.F_MIN, TXMCoreMessages.common_fMin, this.getFieldEditorParent());
		fMin.getTextControl(this.getFieldEditorParent()).setToolTipText(TXMCoreMessages.minimumFrequencyThresholdCommaAllValuesBelowFminAreIgnored);
		fMin.setValidRange(1, Integer.MAX_VALUE);
		this.addField(fMin);
		
		
		IntegerFieldEditor vMax = new IntegerFieldEditor(TXMPreferences.V_MAX, TXMCoreMessages.common_vMax, this.getFieldEditorParent());
		vMax.getTextControl(this.getFieldEditorParent()).setToolTipText(TXMCoreMessages.listTruncationThresholdCommaOnlyTheMostFrequentFirstVmaxLinesAreRetained);
		vMax.setValidRange(1, Integer.MAX_VALUE);
		this.addField(vMax);
		
	}

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(LexicalTablePreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(LexicalTableCoreMessages.RESULT_TYPE);
		this.setImageDescriptor(LexicalTableAdapterFactory.ICON);
	}

}
