// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.part.EditorPart;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.editors.ITablableEditorInput;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.editors.input.AbstractTablableEditorInput;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.statsengine.core.StatException;

// TODO: Auto-generated Javadoc
/**
 * The Class LexicalTableEditorInput.
 *
 * @author sloiseau
 */
public class ___LexicalTableEditorInput extends TXMResultEditorInput {

	/** The editors. */
	private List<EditorPart> editors;

	/** The inputs. */
	private List<IEditorInput> inputs;

	/**
	 * Instantiates a new lexical table editor input.
	 *
	 * @param table the table
	 */
	public ___LexicalTableEditorInput(final LexicalTable table) {
		super(table);

		editors = new ArrayList<EditorPart>();
		editors.add(new ___TableEditor());

		inputs = new ArrayList<IEditorInput>();

		class LexicalTableLabelProvider extends LabelProvider implements
				ITableLabelProvider {

			@Override
			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}

			@Override
			public String getColumnText(Object element, int columnIndex) {
				int[] val = (int[]) element;
				return String.valueOf(val[columnIndex]);
			}

		}

		ITablableEditorInput asTable = new AbstractTablableEditorInput() {

			@Override
			public String[] getColumnTitles() {
				return new String[] { TXMUIMessages.form,
						TXMCoreMessages.common_frequency };
			}

			@Override
			public String[] getColumnTypes() {
				return new String[] { TXMUIMessages.string,
						TXMUIMessages.integer };
			}

			@Override
			public IStructuredContentProvider getContentProvider() {
				return new IStructuredContentProvider() {

					@Override
					public Object[] getElements(Object inputElement) {
						LexicalTable res = (LexicalTable) inputElement;
						List<int[]> row = new ArrayList<int[]>();
						try {
							for (int i = 0; i < res.getNRows(); i++) {
								int[] v = null;
								try {
									v = res.getRow(i).asIntArray();
								}
								catch (StatException e) {
									System.err.println(NLS.bind(TXMCoreMessages.errorColonP0, e));
								}
								row.add(v);
							}
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return row.toArray();
					}

					@Override
					public void dispose() {
						// TODO Auto-generated method stub
					}

					@Override
					public void inputChanged(Viewer viewer, Object oldInput,
							Object newInput) {
						// TODO Auto-generated method stub
					}

				};
			}

			@Override
			public Object getInput() {
				return table;
			}

			@Override
			public LabelProvider getLabelProvider() {
				return new LexicalTableLabelProvider();
			}

		};
		inputs.add(asTable);

	}


	@Override
	public LexicalTable getResult() {
		return (LexicalTable) this.result;
	}

	/**
	 * Gets the property.
	 *
	 * @return the property
	 */
	public Property getProperty() {
		return this.getResult().getProperty();
	}

}
