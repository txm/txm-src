package org.txm.lexicaltable.rcp.editors;

import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * The listener interface for receiving columnSelection events.
 * The class that is interested in processing a columnSelection
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addColumnSelectionListener<code> method. When
 * the columnSelection event occurs, that object's appropriate
 * method is invoked.
 *
 * @see org.eclipse.swt.events.SelectionListener
 */
@Deprecated
public class ColumnSelectionListener implements SelectionListener {

	LexicalTableEditor editor;

	/** The col. */
	TableViewerColumn col;

	/** The index. */
	int columnIndex;

	/**
	 * Instantiates a new column selection listener.
	 *
	 * @param formColumn the form column
	 * @param index the index
	 */
	public ColumnSelectionListener(LexicalTableEditor editor, TableViewerColumn formColumn, int index) {
		this.editor = editor;
		this.col = formColumn;
		this.columnIndex = index;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		StatusLine.setMessage(TXMUIMessages.sortingColumn);
		try {
			this.editor.sort(this.col, this.columnIndex);
			StatusLine.setMessage(""); //$NON-NLS-1$
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
}
