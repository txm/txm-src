// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.editors;

import java.util.Locale;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.txm.rcp.editors.ITablableEditorInput;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableSorter;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.messages.TXMUIMessages;

// TODO: Auto-generated Javadoc
/**
 * Generic editor of tableable objects with ONE TableViewer
 *
 * @author sloiseau
 */
public class ___TableEditor extends EditorPart implements TableResultEditor {

	/** The ID. */
	static public String ID = "org.txm.rcp.editors.TableEditor"; //$NON-NLS-1$

	/** The viewer. */
	private TableViewer viewer;

	/** The i editor input. */
	private IEditorInput iEditorInput;

	/** The table sorter. */
	private TableSorter tableSorter;

	private TableKeyListener tableKeyListener;

	/**
	 * Linking with the OutlineView.
	 *
	 * @param monitor the monitor
	 */
	// private OverviewOutlinePage overviewOutlinePage;

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		this.iEditorInput = input;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new FillLayout());

		viewer = new TableViewer(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION | SWT.VIRTUAL);
		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		//		tableKeyListener.installSearchGroup(this);
		//		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		if (!(iEditorInput instanceof ITablableEditorInput)) {
			throw new IllegalArgumentException(TXMUIMessages.bind(TXMUIMessages.canNotCreateATableWithTheProvidedInformationColonP0, iEditorInput));
		}

		ITablableEditorInput tableInput = (ITablableEditorInput) iEditorInput;

		String[] titles = tableInput.getColumnTitles();

		for (int i = 0; i < titles.length; i++) {
			final int index = i;
			final TableViewerColumn column = new TableViewerColumn(viewer,
					SWT.NONE);

			column.getColumn().setText(titles[i]);
			column.getColumn().setWidth(100);
			column.getColumn().setResizable(true);
			column.getColumn().setMoveable(true);

			column.getColumn().addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					tableSorter.setColumn(index);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == column.getColumn()) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					}
					else {
						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(column.getColumn());
					viewer.refresh();
				}
			});
		}

		viewer.setContentProvider(tableInput.getContentProvider());
		viewer.setLabelProvider(tableInput.getLabelProvider());
		viewer.setInput(tableInput.getInput());

		tableSorter = new TableSorter();
		tableSorter.setColumnTypes(tableInput.getColumnTypes());
		if (this.iEditorInput instanceof ___LexicalTableEditorInput) {
			___LexicalTableEditorInput ed = (___LexicalTableEditorInput) this.iEditorInput;
			tableSorter.setLocale(new Locale(ed.getResult().getPartition().getParent().getLang()));
		}
		viewer.setSorter(tableSorter);

		createContextMenu(viewer);
	}

	/**
	 * Creates the context menu.
	 *
	 * @param tableViewer the table viewer
	 */
	private void createContextMenu(TableViewer tableViewer) {

		MenuManager menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(tableViewer.getTable());

		// Set the MenuManager
		tableViewer.getTable().setMenu(menu);
		getSite().registerContextMenu(menuManager, tableViewer);
		// Make the selection available
		getSite().setSelectionProvider(tableViewer);
	}

	/**
	 * Gets the table sorter.
	 *
	 * @return the table sorter
	 */
	public TableSorter getTableSorter() {
		return tableSorter;
	}

	/**
	 * Gets the table viewer.
	 *
	 * @return the table viewer
	 */
	public TableViewer getTableViewer() {
		return viewer;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}


	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { viewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}
}
