// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.lexicaltable.rcp.editors;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TableColumn;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.rcp.handlers.DeleteLines;
import org.txm.lexicaltable.rcp.handlers.MergeLines;
import org.txm.lexicaltable.rcp.messages.LexicalTableUIMessages;
import org.txm.rcp.StatusLine;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.preferences.RCPPreferences;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.jface.TableViewerSeparatorColumn;
import org.txm.rcp.swt.widget.ThresholdsGroup;
import org.txm.rcp.swt.widget.structures.PropertiesComboViewer;
import org.txm.rcp.utils.IOClipboard;
import org.txm.rcp.views.QueriesView;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.statsengine.core.StatException;
import org.txm.statsengine.core.data.Vector;
import org.txm.statsengine.r.rcp.views.RVariablesView;
import org.txm.utils.logger.Log;

/**
 * display the lexical table tools to merge/delete lines or columns and display
 * the lexical table too
 *
 * @author mdecorde
 */
public class LexicalTableEditor extends TXMEditor<LexicalTable> implements TableResultEditor {

	public static final String ID = LexicalTableEditor.class.getName();

	/** The table. */
	LexicalTable lexicalTable;

	// params
	/** The line table viewer. */
	TableViewer leftViewer;

	/** The line table viewer. */
	TableViewer rightViewer;

	/** The form column. */
	TableViewerColumn unitColumn;

	/** The freq column. */
	TableViewerColumn freqColumn;

	/** The cols. */
	private List<double[]> cols;

	/** The rows. */
	private List<String> rows;

	/** The collist. */
	List<String> collist;

	/** The previous sorted col. */
	private int previousSortedCol;

	boolean DEFAULTREVERSE = false;

	boolean reverse = DEFAULTREVERSE;

	private Button infoLine;

	/**
	 * Button to merge or delete some rows.
	 */
	protected Button mergeDeleteRowsButton;

	/**
	 * Button to merge or delete some columns.
	 */
	protected Button mergeDeleteColumnsButton;



	// FIXME: SJ: viewer comparator tests
	// /**
	// * Viewer comparator for sorting.
	// */
	// protected LexicalTableLinesViewerComparator viewerComparator;



	/**
	 * Unit property.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTY)
	protected PropertiesComboViewer unitPropertyComboViewer;


	/**
	 * Minimum frequency filter spinner.
	 */
	@Parameter(key = TXMPreferences.F_MIN)
	protected Spinner fMinSpinner;

	/**
	 * Minimum frequency filter spinner.
	 */
	@Parameter(key = TXMPreferences.F_MAX)
	protected Spinner fMaxSpinner;

	/**
	 * Maximum numbers of lines spinner.
	 */
	@Parameter(key = TXMPreferences.V_MAX)
	protected Spinner vMaxSpinner;

	private TableKeyListener tableKeyListener;

	/**
	 * Creates the part control.
	 *
	 * @see "org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)"
	 */
	@Override
	public void _createPartControl() {

		try {
			lexicalTable = this.getResult();

			// Main parameters
			GLComposite mainParametersArea = this.getMainParametersComposite();
			mainParametersArea.getLayout().numColumns = 2;

			// unit property
			new Label(mainParametersArea, SWT.NONE).setText(TXMCoreMessages.common_property);
			ArrayList<WordProperty> availablesProperties = new ArrayList<>();
			CQPCorpus corpus = CQPCorpus.getFirstParentCorpus(this.getResult());
			if (corpus != null) {
				availablesProperties.addAll(corpus.getOrderedProperties());
				availablesProperties.addAll(corpus.getVirtualProperties());
			}
			this.unitPropertyComboViewer = new PropertiesComboViewer(
					mainParametersArea,
					this,
					false,
					availablesProperties,
					this.getResult().getProperty(),
					false);
			unitPropertyComboViewer.getCombo().setToolTipText(LexicalTableUIMessages.tooltip_propertyOfWordsToCount);

			// disable the property selection on Lexical table created from partition index
			if (this.getResult().getParent() instanceof PartitionIndex) {
				this.unitPropertyComboViewer.getCombo().setEnabled(false);
			}


			// Extended parameters
			Composite extendedParametersArea = this.getExtendedParametersGroup();

			// FIXME: SJ: became useless? Apply button
			// Button keepTop = new Button(extendedParametersArea, SWT.PUSH);
			// keepTop.setText(LexicalTableUIMessages.LexicalTableEditor_4);
			// keepTop.setToolTipText(LexicalTableUIMessages.LexicalTableEditor_4);
			// keepTop.addSelectionListener(new SelectionListener() {
			// @Override
			// public void widgetSelected(SelectionEvent e) {
			// StatusLine.setMessage(LexicalTableUIMessages.LexicalTableEditor_16);
			// System.out.println(NLS.bind(LexicalTableUIMessages.LexicalTableEditor_5, vMaxFilterSpinner.getSelection(), fMinFilterSpinner.getSelection()));
			//
			// MessageBox messageBox = new MessageBox(e.display.getActiveShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			// messageBox.setMessage(TXMUIMessages.common_areYouSure);
			// int response = messageBox.open();
			// if (response != SWT.YES) {
			// return;
			// }
			//
			// try {
			//// updateResultFromEditor();
			//// refresh(false);
			// // force the recomputing
			// getResult().setDirty();
			// compute(true);
			// } catch (Exception e1) {
			// // TODO Auto-generated catch block
			// org.txm.utils.logger.Log.printStackTrace(e1);
			// StatusLine.setMessage("LexicalTable: sorting error."); //$NON-NLS-1$
			// }
			// }
			//
			// @Override
			// public void widgetDefaultSelected(SelectionEvent e) {
			// }
			// });


			// thresholds
			ThresholdsGroup thresholdsGroup = new ThresholdsGroup(this.getExtendedParametersGroup(), SWT.NONE, this, true, true);
			this.fMinSpinner = thresholdsGroup.getFMinSpinner();
			this.fMaxSpinner = thresholdsGroup.getFMaxSpinner();
			this.vMaxSpinner = thresholdsGroup.getVMaxSpinner();
			this.vMaxSpinner.setMinimum(2); // LT won't work with 1 line only

			if (corpus != null) {
				this.vMaxSpinner.setMaximum(corpus.getSize());
				this.fMaxSpinner.setMaximum(corpus.getSize());
				this.fMinSpinner.setMaximum(corpus.getSize());
			}

			// Merge or delete columns button
			mergeDeleteColumnsButton = new Button(extendedParametersArea, SWT.PUSH);
			mergeDeleteColumnsButton.setText(LexicalTableUIMessages.mergeOrDeleteColumns);
			mergeDeleteColumnsButton.setToolTipText("Merge of delete columns in the lexical table. Selection is done via the column names");
			mergeDeleteColumnsButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					StatusLine.setMessage(LexicalTableUIMessages.mergingLines);
					ArrayList<Object> selection = new ArrayList<>();
					MergeDeleteDialog d = new MergeDeleteDialog(e.display.getActiveShell(), new ArrayList<>(collist), selection, -1);

					if (d.open() == Window.OK) {
						List<Integer> colindices = new ArrayList<>();
						for (int i = 0; i < collist.size(); i++) {
							if (selection.contains(collist.get(i))) {
								colindices.add(i);
							}
						}

						try {

							if (d.doMerge()) {

								doMergeColumns(lexicalTable, d.getMergeName(), colindices, cols);

								// refresh stuff
								collist = new ArrayList<>();// update col name
								// list
								for (String colname : lexicalTable.getColNames().asStringsArray()) {
									collist.add(colname);
								}

								Collections.sort(colindices);// update table viewer cols
								for (int i = colindices.size() - 1; i >= 1; i--) {
									rightViewer.getTable().getColumns()[colindices.get(i)].dispose();// +3 = separator,
									// form, freq
								}
								rightViewer.getTable().getColumns()[colindices.get(0)].setText(d.getMergeName());
								compute(false);
							}
							// delete
							else {
								Log.fine(NLS.bind("Delete columns with indices: {0}.", colindices));

								lexicalTable.getData().removeCols(colindices);
								lexicalTable.setAltered();

								collist = new ArrayList<>();
								Vector colnames = lexicalTable.getColNames();
								for (String colname : colnames.asStringsArray()) {
									collist.add(colname);
								}
								Collections.sort(colindices);
								for (int i = colindices.size() - 1; i >= 0; i--) {
									rightViewer.getTable().getColumns()[colindices.get(i)].dispose();
								}
								compute(false);
							}
						}
						catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
							return;
						}
					}
					StatusLine.setMessage(""); //$NON-NLS-1$

				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			// Merge or delete lines button
			mergeDeleteRowsButton = new Button(extendedParametersArea, SWT.PUSH);
			mergeDeleteRowsButton.setText(LexicalTableUIMessages.mergeOrDeleteRows);
			mergeDeleteRowsButton.setToolTipText("Merge of delete rows in the lexical table. Selection is done via the row names");
			mergeDeleteRowsButton.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {

					ArrayList<Object> selection = new ArrayList<>();
					MergeDeleteDialog d = new MergeDeleteDialog(e.display
							.getActiveShell(), new ArrayList<Object>(rows),
							selection, -1);
					int count = 0;
					if (d.open() == Window.OK) {
						int[] rowindices = new int[selection.size()];
						for (int i = 0; i < rows.size(); i++) {
							if (selection.contains(rows.get(i))) {
								rowindices[count] = i;
								count++;
							}
						}
						try {

							if (d.doMerge()) {
								MergeLines.mergeLines(LexicalTableEditor.this, d.getMergeName(), rowindices);
							}
							else {
								lexicalTable.getData().removeRows(rowindices);
								compute(false);
							}
							lexicalTable.setAltered();
						}
						catch (Exception ex) {
							Log.warning(ex.getMessage());
							Log.printStackTrace();
						}
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});

			// Result area
			GLComposite resultArea = this.getResultArea();
			resultArea.getLayout().numColumns = 2;
			resultArea.getLayout().makeColumnsEqualWidth = false;

			leftViewer = new TableViewer(resultArea, SWT.VIRTUAL | SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
			leftViewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, true));
			leftViewer.getTable().setLinesVisible(true);
			leftViewer.getTable().setHeaderVisible(true);

			KeyListener keyListener = new KeyListener() {

				@Override
				public void keyReleased(KeyEvent e) {

					if (e.keyCode == SWT.DEL) {
						try {
							DeleteLines.deleteLexicalTableLines(LexicalTableEditor.this);
						}
						catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}

				@Override
				public void keyPressed(KeyEvent e) {
				}
			};
			leftViewer.getTable().addKeyListener(keyListener);
			//			String fontName = getResult().getProject().getFont();
			//			if (fontName != null && fontName.length() > 0) {
			//				Font old = viewer.getTable().getFont();
			//				FontData fD = old.getFontData()[0];
			//				// Font f = new Font(old.getDevice(), corpus.getFont(), fD.getHeight(), fD.getStyle());
			//				Font font = new Font(Display.getCurrent(), fontName, fD.getHeight(), fD.getStyle());
			//				viewer.getTable().setFont(font);
			//			}

			// viewer.setContentProvider(ArrayContentProvider.getInstance());

			// FIXME: viewer comparator tests
			// creates the viewer comparator
			// this.viewerComparator = new LexicalTableLinesViewerComparator(Toolbox.getCollator(this.getResult()));
			// viewer.setComparator(this.viewerComparator);

			// FIXME: we must use this line rather than the LineContentProvider class
			// viewer.setContentProvider(ArrayContentProvider.getInstance());
			leftViewer.setContentProvider(new LineContentProvider());

			// first dummy column
			TableViewerSeparatorColumn.newSeparator(leftViewer);

			// property column
			unitColumn = new TableViewerColumn(leftViewer, SWT.LEFT);
			unitColumn.getColumn().addSelectionListener(new ColumnSelectionListener(this, unitColumn, -3));
			unitColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					return rows.get((Integer) element);
				}
			});

			// frequency column
			freqColumn = new TableViewerColumn(leftViewer, SWT.RIGHT);
			freqColumn.getColumn().setText("T=" + getResult().getData().getTotal() + TableUtils.headersNewLine + "F");
			freqColumn.getColumn().addSelectionListener(new ColumnSelectionListener(this, freqColumn, -2));
			freqColumn.setLabelProvider(new ColumnLabelProvider() {

				@Override
				public String getText(Object element) {
					try {
						return Integer.toString(getResult().getData().getRowMargins()[(Integer) element]);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return "error";
					}
				}
			});

			// add a separator empty column
			TableViewerSeparatorColumn.newSeparator(leftViewer);

			// adjust UI to the original sorting
		//	leftViewer.getTable().setSortColumn(freqColumn.getColumn());
		//	leftViewer.getTable().setSortC(SWT.UP);

			rightViewer = new TableViewer(resultArea, SWT.VIRTUAL | SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
			rightViewer.getTable().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
			rightViewer.getTable().setLinesVisible(true);
			rightViewer.getTable().setHeaderVisible(true);

			rightViewer.getTable().addKeyListener(keyListener);

			rightViewer.setContentProvider(new LineContentProvider());

			tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(leftViewer, rightViewer);
			tableKeyListener.installSearchGroup(this);
			tableKeyListener.installMenuContributions(this);
			TableUtils.installColumnResizeMouseWheelEvents(leftViewer, rightViewer);
			TableUtils.installVerticalSynchronisation(leftViewer, rightViewer);
			TableUtils.installTooltipsV2OnSelectionChangedEvent(leftViewer, rightViewer);

			// Register the context menu
			TXMEditor.initContextMenu(this.leftViewer.getTable(), this.getSite(), this.leftViewer); // $NON-NLS-1$
			TXMEditor.initContextMenu(this.rightViewer.getTable(), this.getSite(), this.leftViewer); // $NON-NLS-1$

			// separator
			getBottomToolbar().getSubWidgetComposite().getLayout().numColumns++;
			new Label(getBottomToolbar().getSubWidgetComposite(), SWT.NONE).setText("\t");

			getBottomToolbar().getSubWidgetComposite().getLayout().numColumns++;
			infoLine = new Button(getBottomToolbar().getSubWidgetComposite(), SWT.PUSH);
			infoLine.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					IOClipboard.write(infoLine.getText());
					Log.info("Copied text: "+infoLine.getText());
				}
			});
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void doMergeColumns(LexicalTable lexicalTable, String newname, List<Integer> colindices, List<double[]> cols) throws Exception {

		int nrows = lexicalTable.getNRows();

		Log.fine(NLS.bind(LexicalTableUIMessages.mergeColsColonP0, colindices));

		if (cols == null) {
			cols = LexicalTable.tableToColumnsList(lexicalTable);
		}

		double[] firstcol = cols.get(colindices.get(0));

		// merge selected cols into the first one
		for (int i = 1; i < colindices.size(); i++) {
			for (int j = 0; j < nrows; j++) {
				firstcol[j] += cols.get(colindices.get(i))[j];
			}
		}

		// update first col of the Lexical table
		for (int j = 0; j < nrows; j++) {
			lexicalTable.getData().set(j, colindices.get(0), firstcol[j]);
		}

		// and its name
		lexicalTable.getColNames().setString(colindices.get(0), newname);

		// keep only the first col
		List<Integer> coltodelete = colindices.subList(1, colindices.size());
		lexicalTable.getData().removeCols(coltodelete);
		lexicalTable.setAltered();
	}

	@Override
	public void setContentDescription(String desc) {
		if (desc == null) {
			desc = "";
		}
		infoLine.setText(desc);
		infoLine.update();
		infoLine.getParent().layout();
	}

	/**
	 * Refreshes the description area.
	 */
	public void refreshInfos() {
		try {
			this.setContentDescription(TXMCoreMessages.bind(LexicalTableUIMessages.tP0vP1fminP2fmaxP3,
					lexicalTable.getData().getTotal(),
					lexicalTable.getData().getNRows(),
					lexicalTable.getData().getFMin(),
					lexicalTable.getData().getFMax()));
		}
		catch (Exception e3) {
			org.txm.utils.logger.Log.printStackTrace(e3);
		}
	}

	public void refreshTable() {
		this.refreshTable(false);
	}

	/**
	 * Refreshes table.
	 */
	// FIXME: SJ: not optimized, need to mix the update/not update code and do not remove column each computing, only when needed
	// FIXME: SJ: + usine à gaz... virer la variable "collist"
	public void refreshTable(boolean update) {

		this.leftViewer.getControl().setRedraw(false);
		this.rightViewer.getControl().setRedraw(false);

		boolean doUpdateColumns = true;

		if (collist == null) {
			doUpdateColumns = true;
		}
		else {
			String colNamesString = collist.toString();

			StringBuffer colNamesString2 = new StringBuffer();
			colNamesString2.append("[");
			for (TableColumn tmpCol : rightViewer.getTable().getColumns()) {
				if (tmpCol.getText().trim().length() == 0) continue;
				if (colNamesString2.length() > 1) colNamesString2.append(", ");
				colNamesString2.append(tmpCol.getText());
			}
			colNamesString2.append("]");

			doUpdateColumns = !colNamesString.toString().equals(colNamesString2.toString());
		}

		if (doUpdateColumns) {
			// removing old columns
			while (rightViewer.getTable().getColumns().length > 0) {
				rightViewer.getTable().getColumns()[0].dispose();
			}

			try {
				collist = new ArrayList<>();

				if (this.lexicalTable.getProperty() != null) {
					unitColumn.getColumn().setText(TableUtils.headersNewLine + this.lexicalTable.getProperty().getName());
				}

				freqColumn.getColumn().setText("T=" + getResult().getData().getTotal() + TableUtils.headersNewLine + "F");

				// creating the columns
				String[] colNames;
				int[] colMargins = null;
				// from lexical table if they exist
				if (lexicalTable.getData() != null) {
					colNames = lexicalTable.getColNames().asStringsArray();
					colMargins = lexicalTable.getColMarginsVector().asIntArray();
				}
				// otherwise from partition parts names
				else {
					colNames = this.getResult().getPartition().getPartNames().toArray(new String[0]);
				}

				int MAX_NUMBER_OF_COLUMNS = RCPPreferences.getInstance().getInt(RCPPreferences.MAX_NUMBER_OF_COLUMNS);
				for (int i = 0; i < colNames.length; i++) {

					if (i >= MAX_NUMBER_OF_COLUMNS) {
						Log.warning(NLS.bind(LexicalTableUIMessages.WarningTheNumberofColumnsExceedsP0P1TheTableHasBeenCut, MAX_NUMBER_OF_COLUMNS, colNames.length));
						break;
					}
					TableViewerColumn column = new TableViewerColumn(rightViewer, SWT.RIGHT);
					column.getColumn().addSelectionListener(new ColumnSelectionListener(this, column, i));
					final int index = i;
					column.setLabelProvider(new ColumnLabelProvider() {

						@Override
						public String getText(Object element) {
							try {
								return Integer.toString((int) cols.get(index)[(Integer) element]);
							}
							catch (Exception ee) {
								System.out.println(ee);
								return "error"; //$NON-NLS-1$
							}
						}
					});

					String colName = "";
					if (colMargins != null) {
						colName += "t=" + colMargins[i] + TableUtils.headersNewLine; //$NON-NLS-1$
					}
					colName += colNames[i];

					column.getColumn().setToolTipText(colName);
					collist.add(colName);
					column.getColumn().setText(colName);
				}

				// add a separator empty column
				TableViewerSeparatorColumn.newSeparator(rightViewer);
			}
			catch (StatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (this.lexicalTable.hasBeenComputedOnce()) {

			//			this.rightViewer.setLabelProvider(new RightLineLabelProvider(lexicalTable, rightViewer.getTable()));
			LineLabelProvider labelprovider = new LineLabelProvider(this.lexicalTable, leftViewer.getTable());
			//			this.leftViewer.setLabelProvider(labelprovider);

			this.cols = labelprovider.getCols();
			this.rows = labelprovider.getRows();
			this.rightViewer.setInput(this.lexicalTable);
			this.leftViewer.setInput(this.lexicalTable);

			// initial sorting
			if (!update) {
				try {
					previousSortedCol = -2;
					this.sort(freqColumn, -2);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		this.leftViewer.getControl().setRedraw(true);
		this.rightViewer.getControl().setRedraw(true);

		// Refresh and pack the columns
		TableUtils.packColumns(this.leftViewer);
		TableUtils.packColumns(this.rightViewer);

		getResultArea().layout();

		this.leftViewer.getTable().deselectAll();
		this.rightViewer.getTable().deselectAll();
		this.rightViewer.getTable().setFocus();
	}


	/**
	 * Sets the col comparator.
	 *
	 * @param comp the comp
	 * @param col the col
	 */
	//FIXME: SJ, 2024-12-12: useless?
	private void setColComparator(Comparator comp, TableColumn col) {
		/*
		 * currentComparator = comp;
		 * if (lineTableViewer.getTable().getSortColumn()!=col){
		 * lineTableViewer.getTable().setSortColumn(col);
		 * lineTableViewer.getTable().setSortDirection(SWT.UP); } else
		 * if (lineTableViewer.getTable().getSortDirection() ==SWT.UP){
		 * lineTableViewer.getTable().setSortDirection(SWT.DOWN);
		 * currentComparator = new ReverseComparator(currentComparator); } else
		 * lineTableViewer.getTable().setSortDirection(SWT.UP);
		 * currentComparator.initialize(cooc.getCorpus());
		 */
	}

	/**
	 * Gets the corpus.
	 *
	 * @return the corpus
	 */
	public CQPCorpus getCorpus() {
		return null;
	}

	/**
	 * Gets the line table viewer.
	 *
	 * @return the line table viewer
	 */
	public TableViewer getTableViewer() {
		return leftViewer;
	}

	/**
	 * Gets the lexical table.
	 *
	 * @return the lexical table
	 */
	public LexicalTable getLexicalTable() {
		return lexicalTable;
	}

	/**
	 * Gets the cols.
	 *
	 * @return the cols
	 */
	public List<double[]> getCols() {
		return cols;
	}

	/**
	 * Export data.
	 *
	 * @param file the file
	 */
	@Deprecated
	public void exportData(File file) {

		String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
		String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
		String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);

		lexicalTable.getData().toTxt(file, encoding, colseparator, txtseparator); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Import data.
	 *
	 * @param file the file
	 */
	@Deprecated
	public boolean importData(File file) {
		try {
			String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
			String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
			String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);

			if (lexicalTable.getData().importData(file, encoding, colseparator, txtseparator)) {
				lexicalTable.setAltered();
				// fix col names
				String[] colnames = lexicalTable.getColNames().asStringsArray();
				TableColumn[] cols = rightViewer.getTable().getColumns();
				int ncol = cols.length;
				if (colnames.length != ncol) {
					// Log.severe(LexicalTableUIMessages.bind(LexicalTableUIMessages.errorColonDifferentColumnsNumberColonBeforeP0AfterP1, ncol, colnames.length));
					LexicalTable r = getResult();
					close();
					TXMEditor.openEditor(r, LexicalTableEditor.ID);
					return true;
				}
				for (int i = 0; i < colnames.length; i++) {
					// System.out.println("set col name "+colnames[i]);
					cols[4 + i].setText(colnames[i]);
				}
				leftViewer.setInput(lexicalTable);
				leftViewer.refresh();
				rightViewer.setInput(lexicalTable);
				refreshTable(false);
				return true;
			}
			else {
				Log.severe(LexicalTableUIMessages.failedToImportLexicalTable);
			}
		}
		catch (Exception e) {
			Log.severe(LexicalTableUIMessages.bind(LexicalTableUIMessages.errorWhileImportingDataColonP0, e));
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}



	/**
	 * Sort.
	 *
	 * @param col the col
	 * @param index the index
	 * @throws Exception
	 * @throws StatException
	 */
	// FIXME: SJ: try to use a ViewerSorter instead of this but also manage the result sorting for export
	public void sort(TableViewerColumn col, int index) throws StatException, Exception {

		List currentSelection = leftViewer.getStructuredSelection().toList();
		HashSet<String> selectedEntries = new HashSet<String>();
		String[] rowNames = this.getResult().getRowNames().asStringsArray();
		for (Object o : currentSelection) {
			int i = (int) o;
			selectedEntries.add(rowNames[i]);
		}

		if (index == previousSortedCol) {
			reverse = !reverse;
		}
		else {
			reverse = DEFAULTREVERSE;
		}

		if (index == -3) {// rownames
			lexicalTable.getData().sortRowNames(reverse);
		}
		else if (index == -2) { // freqs
			try {
				lexicalTable.getData().sortByFreqs(reverse);
				StatusLine.setMessage(TXMUIMessages.sortDone);
			}
			catch (Exception e2) {
				org.txm.utils.logger.Log.printStackTrace(e2);
			}
		}
		else if (index >= 0) {
			lexicalTable.getData().sort(index, reverse);
		}

		rowNames = this.getResult().getRowNames().asStringsArray(); // get updated rowNames (new order)
		List<Integer> newSelection = new ArrayList<Integer>();
		if (selectedEntries.size() > 0) {
			for (int i = 0; i < rowNames.length; i++) {
				if (selectedEntries.remove(rowNames[i])) {
					newSelection.add(i);

				}
				if (selectedEntries.size() == 0) break;
			}
		}

		previousSortedCol = index;

		refreshTable(true);
		
		if (index < 0) {
			// viewer.getTable().setSortColumn(col.getColumn());
			leftViewer.getTable().setSortColumn(col.getColumn());

			if (reverse) {
				leftViewer.getTable().setSortDirection(SWT.DOWN);
			}
			else {
				leftViewer.getTable().setSortDirection(SWT.UP);
			}
			rightViewer.getTable().setSortColumn(null);
		}
		else {
			rightViewer.getTable().setSortColumn(col.getColumn());

			if (reverse) {
				rightViewer.getTable().setSortDirection(SWT.DOWN);
			}
			else {
				rightViewer.getTable().setSortDirection(SWT.UP);
			}
			leftViewer.getTable().setSortColumn(null);
		}

		// Refresh and pack the columns
		TableUtils.refreshColumns(this.leftViewer);
		TableUtils.refreshColumns(this.rightViewer);

		leftViewer.setSelection(new StructuredSelection(newSelection));
		rightViewer.setSelection(new StructuredSelection(newSelection));
		
	}


	@Override
	public void updateEditorFromResult(boolean update) {

		// enable/disable the edition buttons according to the computed state
		if (this.lexicalTable.getData() != null) {
			this.mergeDeleteColumnsButton.setEnabled(true);
			this.mergeDeleteRowsButton.setEnabled(true);

			this.refreshInfos();
		}
		else {
			this.mergeDeleteColumnsButton.setEnabled(false);
			this.mergeDeleteRowsButton.setEnabled(false);
		}


		try {
			ArrayList newColList = new ArrayList<>();// update col name
			// list
			for (String colname : getResult().getColNames().asStringsArray()) {
				newColList.add(colname);
			}

			if (!newColList.equals(collist)) {
				collist = null; // force update of columns
			}
		}
		catch (StatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.refreshTable(update);

		QueriesView.refresh();
		RVariablesView.refresh();
	}


	@Override
	public void updateResultFromEditor() {
		// FIXME: SJ: needs to manage here the merge/deletion of rows and columns
	}

	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { leftViewer, rightViewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}
}
