// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Table;
import org.txm.lexicaltable.core.functions.LexicalTable;

/**
 * The Class LineLabelProvider.
 * 
 * we're using ColumnLabelProviders instead because the column labels are no more attached to a column order
 */
@Deprecated
public class LineLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	/** The table. */
	LexicalTable table;

	/** The rownames. */
	String[] rownames;

	/** The cols. */
	List<double[]> cols = new ArrayList<>();

	/** The freqs. */
	int[] freqs;

	/** The Nrows. */
	int Nrows = 0;

	/** The Ncols. */
	int Ncols = 0;

	/** The tot. */
	int tot = 0;

	/** The max. */
	int max = 0;

	private Table swtTable;

	/**
	 * Instantiates a new line label provider.
	 *
	 * @param table the table
	 */
	public LineLabelProvider(LexicalTable table, Table swtTable) {
		super();
		this.table = table;
		this.swtTable = swtTable;
		try {
			rownames = table.getRowNames().asStringsArray();
			Nrows = table.getNRows();
			cols = LexicalTable.tableToColumnsList(table);
			Ncols = cols.size();
			freqs = table.getData().getRowMargins();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	public static final String EMPTY = ""; //$NON-NLS-1$

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	@Override
	public String getColumnText(Object element, int columnIndex) {

		if (columnIndex == 0 || columnIndex == 3 || columnIndex == cols.size() + 4) {
			return EMPTY;
		}
		else if (columnIndex == 1) {
			return rownames[(Integer) element];
		}
		else if (columnIndex == swtTable.getColumnCount() - 1) {
			return EMPTY;
		}
		else if (columnIndex == 2) {
			return Integer.toString(freqs[(Integer) element]);
		}
		else {
			// if (columnIndex == Ncols + 3) {
			// return EMPTY;
			// }
			// else {
			try {
				return Integer.toString((int) cols.get(columnIndex - 4)[(Integer) element]);
			}
			catch (Exception ee) {
				System.out.println(ee);
				System.out.println(ee);
				;
				return "error"; //$NON-NLS-1$
			}
			// }
		}
	}

	/**
	 * Gets the cols.
	 *
	 * @return the cols
	 */
	public List<double[]> getCols() {
		return cols;
	}

	/**
	 * Gets the freqs.
	 *
	 * @return the freqs
	 */
	public int[] getFreqs() {
		return freqs;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public int getTotal() {
		return tot;
	}

	/**
	 * Gets the max.
	 *
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * Gets the rows.
	 *
	 * @return the rows
	 */
	public List<String> getRows() {
		try {
			return Arrays.asList(rownames);
		}
		catch (Exception e) {
			return new ArrayList<>();
		}
	}
}
