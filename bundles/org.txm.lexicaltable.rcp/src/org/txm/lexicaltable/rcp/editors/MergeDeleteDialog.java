// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.editors;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.rcp.messages.TXMUIMessages;
import org.txm.rcp.swt.dialog.ObjectSelectionDialog;

// TODO: Auto-generated Javadoc
/**
 * The Class MergeDeleteDialog.
 */
public class MergeDeleteDialog extends ObjectSelectionDialog {

	/** The merge radio. */
	Button mergeRadio;

	/** The delete radio. */
	Button deleteRadio;

	/** The newname. */
	Text newname;

	/** The do merge. */
	boolean doMerge;

	/** The mergename. */
	private String mergename;

	/**
	 * Instantiates a new merge delete dialog.
	 *
	 * @param shell the shell
	 * @param available the available
	 * @param selected the selected
	 * @param max the max
	 */
	public MergeDeleteDialog(Shell shell, List<Object> available, List<Object> selected, int max) {
		super(shell, available, selected, max);
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.swt.dialog.ObjectSelectionDialog#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(TXMUIMessages.mergeDelete);
	}


	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.swt.dialog.ObjectSelectionDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		super.createDialogArea(parent);

		mergeRadio = new Button(mainArea, SWT.RADIO);
		mergeRadio.setText(TXMUIMessages.merge);
		mergeRadio.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		mergeRadio.setSelection(true);

		deleteRadio = new Button(mainArea, SWT.RADIO);
		deleteRadio.setText(TXMCoreMessages.common_delete);
		deleteRadio.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

		newname = new Text(mainArea, SWT.SINGLE | SWT.BORDER);
		newname.setText(TXMUIMessages.mergeResultName);
		newname.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 4, 1));

		return mainArea;
	}

	/*
	 * (non-Javadoc)
	 * @see org.txm.rcp.swt.dialog.ObjectSelectionDialog#okPressed()
	 */
	@Override
	public void okPressed() {
		doMerge = mergeRadio.getSelection();
		mergename = newname.getText();
		super.okPressed();
	}

	/**
	 * Do merge.
	 *
	 * @return true, if successful
	 */
	public boolean doMerge() {
		return doMerge;
	}

	/**
	 * Do delete.
	 *
	 * @return true, if successful
	 */
	public boolean doDelete() {
		return !doMerge;
	}

	/**
	 * Gets the merge name.
	 *
	 * @return the merge name
	 */
	public String getMergeName() {
		return mergename;
	}
}
