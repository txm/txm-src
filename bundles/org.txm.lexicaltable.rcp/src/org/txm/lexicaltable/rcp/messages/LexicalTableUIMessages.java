package org.txm.lexicaltable.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Lexical table UI messages.
 *
 * @author sjacquot
 *
 */
public class LexicalTableUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.lexicaltable.rcp.messages.messages"; //$NON-NLS-1$

	public static String tooltip_propertyOfWordsToCount;

	public static String errorCannotBuildALexicalTableWithLessThan2Lines;

	public static String errorColonDifferentColumnsNumberColonBeforeP0AfterP1;

	public static String mergeOrDeleteColumns;

	public static String mergeColsColonP0;

	public static String mergeOrDeleteRows;

	public static String failedToImportLexicalTable;

	public static String errorWhileImportingDataColonP0;

	public static String mergingLines;

	public static String userIndexOccurrences;

	public static String selectWhichMarginsYouWantToUse;

	public static String useAllOccurrences;

	public static String openingMargeConfigurationDialog;

	public static String canNotCreateALexicalTableWithAnIndexCreatedOnACorpus;

	public static String DataImportedP0ColumnsAndP1Rows;

	public static String ErrorDataImportFailedP0;

	public static String PleaseReopenTheLexicalTableToSeeTheModifications;

	public static String vocabulariesMustShareTheSamePropertiesColonP0;

	public static String vocabulariesMustShareTheSamePartitionColonP0;

	public static String tP0vP1fminP2fmaxP3;

	public static String WarningTheNumberofColumnsExceedsP0P1TheTableHasBeenCut;



	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, LexicalTableUIMessages.class);
	}
}
