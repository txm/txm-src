package org.txm.lexicaltable.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.functions.intertextualdistance.InterTextDistance;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.utils.logger.Log;

public class ComputeInterTextDistance extends BaseAbstractHandler {

	private IStructuredSelection selection;

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {
		super.addHandlerListener(handlerListener);
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		selection = this.getCorporaViewSelection(event);

		Object s = selection.getFirstElement();
		if (!(s instanceof LexicalTable))
			return null;
		LexicalTable table = (LexicalTable) s;
		InterTextDistance dist = new InterTextDistance(table.getData());

		try {
			dist._compute();
		}
		catch (Exception e) {
			System.out.println(TXMCoreMessages.bind(TXMCoreMessages.error_errorP0, e.getLocalizedMessage()));
			Log.printStackTrace(e);
		}
		return null;
	}

	@Override
	public boolean isEnabled() {
		return super.isEnabled();
	}

	@Override
	public boolean isHandled() {
		return super.isHandled();
	}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {
		super.removeHandlerListener(handlerListener);
	}
}
