// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.handlers;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.rcp.editors.LexicalTableEditor;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * The Class MergeCols.
 */
// FIXME: this class is not used?
public class ___MergeCols extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getActivePart();
		if (editor instanceof LexicalTableEditor) {
			LexicalTableEditor LTeditor = (LexicalTableEditor) editor;
			int[] selection = LTeditor.getTableViewer().getTable()
					.getSelectionIndices();
			if (selection.length == 0)
				return null;

			String newname = ""; //$NON-NLS-1$
			InputDialog d = new InputDialog(shell, TXMUIMessages.newName, TXMUIMessages.editTheNameOfTheNewColumnCreatedByTheMergedColumns, "", null); //$NON-NLS-1$
			if (d.open() == Window.OK)
				newname = d.getValue();
			else
				return null;
			if (newname.trim().equals("")) //$NON-NLS-1$
				return null;

			// sum the cols
			int[] selectedCols = new int[3];
			List<double[]> cols = LTeditor.getCols();
			double[] firstcol = cols.get(selectedCols[0]);

			LexicalTable table = LTeditor.getLexicalTable();
			int nrows;
			try {
				nrows = table.getNRows();

				for (int i = 1; i < selectedCols.length; i++) {
					for (int j = 0; j < nrows; j++) {
						firstcol[j] += cols.get(selectedCols[i])[j];
					}
				}

				// update table
				for (int j = 0; j < nrows; j++) {
					table.getData().set(j, selectedCols[0], firstcol[j]);
				}

				// keep only the first col
				int[] coltodelete = new int[selectedCols.length - 1];
				System.arraycopy(selectedCols, 1, coltodelete, 0, selectedCols.length - 1);
				table.getData().removeCols(coltodelete);
				table.setAltered();
				// reset col name
				table.getColNames().setString(selectedCols[0], newname);

				System.out.println(NLS.bind(TXMUIMessages.namesP0, Arrays.toString(table.getRowNames().asStringsArray())));
			}
			catch (Exception e1) {
				// TODO Auto-generated catch block
				org.txm.utils.logger.Log.printStackTrace(e1);
				return null;
			}

			System.out.println(TXMUIMessages.endOfColumnMerge);
			LTeditor.refreshTable();
		}
		return null;
	}
}
