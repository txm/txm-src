// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.handlers;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.lexicaltable.rcp.editors.LexicalTableEditor;
import org.txm.rcp.handlers.export.ExportResultDialog;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.rcp.swt.dialog.LastOpened;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportTable.
 */
public class ExportLexicalTable extends AbstractHandler {

	private static final String ID = "org.txm.rcp.commands.editor.ExportLexicalTable"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
		if (editor instanceof LexicalTableEditor) {
			LexicalTableEditor LTeditor = (LexicalTableEditor) editor;
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			ExportResultDialog dialog = new ExportResultDialog(shell, LTeditor.getResult());
			dialog.setFilePath(LastOpened.getFolder(ID), LTeditor.getResult().getValidFileName());
			
			if (dialog.open() == dialog.OK) {
				String path = dialog.getFile().getAbsolutePath();
				File file = new File(path);
				LastOpened.set(ID, file.getParent(), file.getName());
				if (file.getParentFile().exists()) {
					try {
						LTeditor.getResult().toTxt(file, "UTF-8");
						EditFile.openfile(path);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return event;

	}
}
