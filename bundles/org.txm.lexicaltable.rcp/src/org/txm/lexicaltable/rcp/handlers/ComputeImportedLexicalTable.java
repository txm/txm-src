// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.handlers;

import java.io.File;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TBXPreferences;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.functions.LexicalTableFromFile;
import org.txm.lexicaltable.rcp.editors.LexicalTableEditor;
import org.txm.lexicaltable.rcp.messages.LexicalTableUIMessages;
import org.txm.objects.Laboratory;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.utils.logger.Log;

/**
 * Opens a lexical table editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeImportedLexicalTable extends BaseAbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		Object sel = this.getCorporaViewSelectedObject(event);

		if (sel instanceof MainCorpus corpus) {

			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			FileDialog d = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ComputeImportedLexicalTable.class.getName()) != null) {
				d.setFilterPath(LastOpened.getFolder(ComputeImportedLexicalTable.class.getName()));
				d.setFileName(LastOpened.getFile(ComputeImportedLexicalTable.class.getName()));
			}
			String path = d.open();
			if (path != null) {
				File file = new File(path);
				LexicalTable lt = create(corpus, file);
				if (lt != null) {
					lt.setUserName(file.getName());
					
					TXMEditor.openEditor(lt, LexicalTableEditor.class.getName());
					
					CorporaView.refreshObject(lt);
					CorporaView.reveal(lt);
				}
				else {
					return null;
				}
			}
		}
		return null;
	}

	public static LexicalTable create(MainCorpus corpus, File file) {
		if (!file.exists() || file.isDirectory()) return null;

		String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
		String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
		String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);

		//Log.info("Importing lexical table using encoding="+encoding+" colsep="+colseparator+" txtsep="+txtseparator);
		try {
			Laboratory sandbox = corpus.getFirstChild(Laboratory.class);
			if (sandbox == null) {
				sandbox = new Laboratory(corpus);
			}

			LexicalTableFromFile lt = new LexicalTableFromFile(sandbox);
			lt.setSourceFile(file);
			lt.compute(false);
			return lt;
		}
		catch (Exception e) {
			Log.severe(LexicalTableUIMessages.bind(LexicalTableUIMessages.ErrorDataImportFailedP0, e));
			Log.printStackTrace(e);
		}
		return null;
	}

}
