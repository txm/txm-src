// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.rcp.editors.LexicalTableEditor;
import org.txm.lexicaltable.rcp.messages.LexicalTableUIMessages;
import org.txm.rcp.StatusLine;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * Allows the user to merge Lexical Table lines columns values are added to the first and the others lines are deleted.
 * 
 * @author mdecorde
 */
public class MergeLines extends AbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();

		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();

		if (editor instanceof LexicalTableEditor) {
			LexicalTableEditor LTeditor = (LexicalTableEditor) editor;
			int[] selection = LTeditor.getTableViewer().getTable().getSelectionIndices();

			if (selection.length == 0) {
				return null;
			}
			String newname = ""; //$NON-NLS-1$
			InputDialog d = new InputDialog(shell, TXMUIMessages.newName,
					TXMUIMessages.editTheNameOfTheNewLineCreatedByTheMergedLines, newname, null); //$NON-NLS-1$

			if (d.open() == Window.OK) {
				newname = d.getValue();
			}
			else {
				return null;
			}

			if (newname.trim().equals("")) { //$NON-NLS-1$
				return null;
			}

			mergeLines(LTeditor, newname, selection);

		}
		return null;
	}

	/**
	 * Merges lines of the table.
	 *
	 * @param LTeditor the l teditor
	 * @param newname the newname
	 * @param selection the selection
	 */
	static public void mergeLines(LexicalTableEditor LTeditor, String newname, int[] selection) {
		// sum the lines
		StatusLine.setMessage(LexicalTableUIMessages.mergingLines);
		try {
			List<double[]> cols = LTeditor.getCols();
			LexicalTable table = LTeditor.getLexicalTable();

			mergeLines(table, newname, selection, cols);
			LTeditor.compute(false);

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StatusLine.setMessage(""); //$NON-NLS-1$
	}

	/**
	 * Merges lines of the table.
	 *
	 * @param LTeditor the l teditor
	 * @param newname the newname
	 * @param selection the selection
	 */
	static public void mergeLines(LexicalTable table, String newname, int[] selection, List<double[]> cols) {

		try {

			if (cols == null) {
				cols = LexicalTable.tableToColumnsList(table);
			}
			double[] fuz = new double[cols.size()];
			for (int j = 0; j < cols.size(); j++) {
				fuz[j] = 0;
			}
			for (int i : selection) {
				for (int j = 0; j < cols.size(); j++) {
					fuz[j] += cols.get(j)[i];
				}
			}
			// set values in the first row of the selection
			for (int j = 0; j < cols.size(); j++) {
				table.getData().set(selection[0], j, fuz[j]);
			}

			// keep only the first selected line
			int[] lineToDelete = new int[selection.length - 1];
			System.arraycopy(selection, 1, lineToDelete, 0, selection.length - 1);
			// System.out.println("Lines to delete : "+Arrays.toString(lineToDelete));
			table.getData().removeRows(lineToDelete);
			table.setAltered();
			// reset line name

			table.getRowNames().setString(selection[0], newname);

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
