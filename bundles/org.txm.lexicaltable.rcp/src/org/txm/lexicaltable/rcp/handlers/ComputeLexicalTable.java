// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.ListDialog;
import org.eclipse.ui.handlers.HandlerUtil;
//import org.txm.functions.queryindex.QueryIndex;
import org.txm.index.core.functions.PartitionIndex;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.rcp.editors.LexicalTableEditor;
import org.txm.lexicaltable.rcp.messages.LexicalTableUIMessages;
import org.txm.rcp.StatusLine;
import org.txm.rcp.TXMWindows;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.utils.logger.Log;

/**
 * Opens a lexical table editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeLexicalTable extends BaseAbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkStatsEngine()) {
			return null;
		}

		Object selection = this.getCorporaViewSelectedObject(event);
		LexicalTable lexicalTable = null;

		// Creating from PartitionIndex
		if (selection instanceof PartitionIndex) {

			IWorkbenchWindow window = TXMWindows.getActiveWindow();
			// FIXME: SJ: why using a list here? is the command should be launchable from more than one index?
			// In not, clean and simplify the code
			List<PartitionIndex> partitionIndexes = ((IStructuredSelection) HandlerUtil.getCurrentSelection(event)).toList();

			ArrayList<String> choices = new ArrayList<>();
			choices.add(LexicalTableUIMessages.useAllOccurrences);
			choices.add(LexicalTableUIMessages.userIndexOccurrences);
			ListDialog dialog = new ListDialog(window.getShell());
			dialog.setInput(choices);
			dialog.setLabelProvider(new LabelProvider());
			dialog.setContentProvider(new ArrayContentProvider());
			dialog.setTitle(LexicalTableUIMessages.selectWhichMarginsYouWantToUse);

			int ret = dialog.open();
			boolean useAllOccurrences = false;
			if (ret == ListDialog.OK) {
				if (dialog.getResult().length == 0) {
					return null;
				}
				String sel = (String) dialog.getResult()[0];
				if (sel.equals(LexicalTableUIMessages.useAllOccurrences)) {
					useAllOccurrences = true;
				}
			}
			else {
				return false;
			}

			if (partitionIndexes.size() == 0) {
				return false;
			}

			partitionIndexes = partitionIndexes.subList(0, 1);
			for (PartitionIndex partitionIndex : partitionIndexes) {
				if (!partitionIndex.isComputedWithPartition()) {
					Log.warning(LexicalTableUIMessages.canNotCreateALexicalTableWithAnIndexCreatedOnACorpus);
					StatusLine.setMessage(LexicalTableUIMessages.canNotCreateALexicalTableWithAnIndexCreatedOnACorpus);
					return false;
				}
			}

			List<WordProperty> properties = partitionIndexes.get(0).getProperties();
			for (PartitionIndex partitionIndex : partitionIndexes) {
				if (!properties.equals(partitionIndex.getProperties())) {
					Log.warning(NLS.bind(LexicalTableUIMessages.vocabulariesMustShareTheSamePropertiesColonP0, properties));
					return false;
				}
			}

			final PartitionIndex firstIndex = partitionIndexes.get(0);

			if (!useAllOccurrences && firstIndex.size() <= 1) {
				Log.warning(LexicalTableUIMessages.errorCannotBuildALexicalTableWithLessThan2Lines);
				return false;
			}

			Partition firstPartition = firstIndex.getPartition();
			for (PartitionIndex partitionIndex : partitionIndexes) {
				if (!firstPartition.equals(partitionIndex.getPartition())) {
					Log.warning(NLS.bind(LexicalTableUIMessages.vocabulariesMustShareTheSamePartitionColonP0, firstPartition));
					return false;
				}
			}

			final WordProperty property = properties.get(0);

			lexicalTable = new LexicalTable(firstIndex);
			lexicalTable.setUseAllOccurrences(useAllOccurrences);
			lexicalTable.setUnitProperty(property);
			// transmit index fmin/fmax parameters
			lexicalTable.setFMinFilter(firstIndex.getFilterFmin());
			if (useAllOccurrences && firstIndex.getFilterVmax() < Integer.MAX_VALUE) {
				lexicalTable.setVMaxFilter(firstIndex.getFilterVmax() + 1); // the #REST line is added, we need to add a line to vmax
			}
			else {
				lexicalTable.setVMaxFilter(firstIndex.getFilterVmax());
			}
		}
		// Creating from Partition
		else if (selection instanceof Partition) {
			lexicalTable = new LexicalTable((Partition) selection);
		}
		// Reopening from existing result
		else if (selection instanceof LexicalTable) {
			lexicalTable = (LexicalTable) selection;
		}
		// Error
		else {
			this.logCanNotExecuteCommand(selection);
		}

		TXMEditor.openEditor(lexicalTable, LexicalTableEditor.class.getName());

		return true;
	}

	// /**
	// * Compute with partition.
	// *
	// * @param partition the partition
	// */
	// private void computeWithLexicalTableAble(final LexicalTable lexicalTableAble) {
	//
	// JobHandler jobhandler = new JobHandler(
	// RCPMessages.ComputeLexicalTable_0) {
	// @Override
	// protected IStatus run(IProgressMonitor monitor) {
	// this.runInit(monitor);
	// try {
	// JobsTimer.start();
	// monitor.beginTask(NLS.bind(RCPMessages.ComputeLexicalTable_1, lexicalTableAble.getName(), "none"), 100);
	//
	// this.acquireSemaphore();
	// //table = lexicalTableAble.toLexicalTable();
	// table = lexicalTableAble;
	// this.releaseSemaphore();
	//
	// monitor.worked(45);
	//
	// lexicalTableAble.getParent().addChild(table);
	//
	// monitor.worked(5);
	//
	// //monitor.subTask(Messages.ComputeSpecifities_10);
	// syncExec(new Runnable() {
	// @Override
	// public void run() {
	// IWorkbenchPage page = window.getActivePage();
	// LexicalTableEditorInput editorInput = new LexicalTableEditorInput(table);
	// try {
	// StatusLine.setMessage(RCPMessages.ComputeLexicalTable_10);
	// page.openEditor(editorInput, LexicalTableEditor.ID);
	// } catch (PartInitException e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	// }
	// });
	//
	// monitor.worked(50);
	// if (monitor.isCanceled())
	// return Status.CANCEL_STATUS;
	//
	// //monitor.subTask(Messages.ComputeSpecifities_2);
	// syncExec(new Runnable() {
	// @Override
	// public void run() {
	// CorporaView.refresh();
	// CorporaView.expand(table.getParent());
	// QueriesView.refresh();
	// RVariablesView.refresh();
	// }
	// });
	//
	// monitor.worked(100);
	// } catch (ThreadDeath td) {
	// return Status.CANCEL_STATUS;
	// } catch (Exception e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// } finally {
	// monitor.done();
	// JobsTimer.stopAndPrint();
	// }
	// return Status.OK_STATUS;
	// }
	// };
	// jobhandler.startJob();
	//
	// return;
	// }



	// /**
	// * Compute with query index.
	// *
	// * @param fullvocabularies the fullvocabularies
	// * @return true, if successful
	// */
	// private boolean computeWithQueryIndexes(List<QueryIndex> oqindexes) {
	// if (oqindexes.size() == 0) {
	// return false;
	// }
	//
	//
	// final List<QueryIndex> qindexes = oqindexes.subList(0, 1);
	// for (QueryIndex voc : qindexes)
	// if (!voc.isComputedWithPartition()) {
	// System.out.println(Messages.ComputeLexicalTable_5);
	// StatusLine.setMessage(Messages.ComputeLexicalTable_5);
	// return false;
	// }
	//
	// Partition firstPartition = qindexes.get(0).getPartition();
	// for (QueryIndex voc : qindexes)
	// if (!firstPartition.equals(voc.getPartition())) {
	// Log.warning(NLS.bind(Messages.ComputeLexicalTable_7, firstPartition));
	// return false;
	// }
	//
	// final Partition partition = ((QueryIndex) selection.getFirstElement())
	// .getPartition();
	// //final Property property = properties.get(0);
	//
	// JobHandler jobhandler = new JobHandler(Messages.ComputeSpecificities_0) {
	// @Override
	// protected IStatus run(IProgressMonitor monitor) {
	// this.runInit(monitor);
	// try {
	// monitor.beginTask(NLS.bind(Messages.ComputeLexicalTable_1, partition.getName(), Messages.ComputeLexicalTable_2), 100);
	// this.acquireSemaphore();
	// copy = LexicalTableImpl.createLexicalTableImpl(qindexes,
	// QuantitativeDataStructureImpl
	// .createSymbole(LexicalTableImpl.class));
	// monitor.worked(45);
	// this.releaseSemaphore();
	// if (copy == null)
	// return Status.CANCEL_STATUS;
	//
	// partition.storeResult(copy);
	// monitor.worked(5);
	//
	// monitor.subTask(Messages.ComputeLexicalTable_10);
	// syncExec(new Runnable() {
	// @Override
	// public void run() {
	// IWorkbenchPage page = window.getActivePage();
	// LexicalTableEditorInput editorInput = new LexicalTableEditorInput(
	// copy, partition, null);
	// try {
	// StatusLine.setMessage(Messages.ComputeLexicalTable_10);
	// page.openEditor(editorInput,
	// "org.txm.rcp.editors.lexicaltable.LexicalTableEditor"); //$NON-NLS-1$
	// } catch (PartInitException e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	// }
	// });
	//
	// monitor.worked(50);
	// if (monitor.isCanceled())
	// return Status.CANCEL_STATUS;
	//
	// monitor.subTask(Messages.ComputeSpecifities_2);
	// syncExec(new Runnable() {
	// @Override
	// public void run() {
	// CorporaView.refresh();
	// CorporaView.expand(copy.getPartition());
	// QueriesView.refresh();
	// RVariablesView.refresh();
	// }
	// });
	//
	// monitor.worked(100);
	// } catch (ThreadDeath td) {
	// return Status.CANCEL_STATUS;
	// } catch (Exception e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// } finally {
	// monitor.done();
	// }
	// return Status.OK_STATUS;
	// }
	// };
	// jobhandler.startJob();
	//
	// return true;
	// }

	// /**
	// * Compute with vocabularies.
	// *
	// * @param fullvocabularies the fullvocabularies
	// * @return true, if successful
	// */
	// private boolean computeWithVocabularies(List<Index> fullvocabularies, final boolean useAllOccurrences) {
	// if (fullvocabularies.size() == 0) {
	// return false;
	// }
	//
	// final List<Index> vocabularies = fullvocabularies.subList(0, 1);
	// for (Index voc : vocabularies) {
	// if (!voc.isComputedWithPartition()) {
	// System.out.println(RCPMessages.ComputeLexicalTable_5);
	// StatusLine.setMessage(RCPMessages.ComputeLexicalTable_5);
	// return false;
	// }
	// }
	//
	// List<Property> properties = vocabularies.get(0).getProperties();
	// for (Index voc : vocabularies) {
	// if (!properties.equals(voc.getProperties())) {
	// Log.warning(NLS.bind(RCPMessages.ComputeLexicalTable_6, properties));
	// return false;
	// }
	// }
	//
	// final Index firstIndex = vocabularies.get(0);
	// Partition firstPartition = firstIndex.getPartition();
	// for (Index voc : vocabularies) {
	// if (!firstPartition.equals(voc.getPartition())) {
	// Log.warning(NLS.bind(RCPMessages.ComputeLexicalTable_7, firstPartition));
	// return false;
	// }
	// }
	//
	// final Property property = properties.get(0);
	//
	// JobHandler jobhandler = new JobHandler(RCPMessages.bind(RCPMessages.ComputeLexicalTable_0, firstIndex.getName())) {
	// @Override
	// protected IStatus run(IProgressMonitor monitor) {
	// this.runInit(monitor);
	// try {
	// monitor.beginTask("Computing Lexical table with Index: "+firstIndex+" property: "+property, 100);
	// this.acquireSemaphore();
	// table = new LexicalTable(firstIndex);
	// table.setParameters(null, null, null, useAllOccurrences);
	// table.compute(true, monitor);
	// monitor.worked(45);
	// this.releaseSemaphore();
	// if (monitor.isCanceled() || table == null)
	// return Status.CANCEL_STATUS;
	//
	// firstIndex.addChild(table);
	// monitor.worked(5);
	//
	// monitor.subTask(RCPMessages.ComputeLexicalTable_10);
	// syncExec(new Runnable() {
	// @Override
	// public void run() {
	// IWorkbenchPage page = window.getActivePage();
	// LexicalTableEditorInput editorInput = new LexicalTableEditorInput(table);
	// try {
	// StatusLine.setMessage(RCPMessages.ComputeLexicalTable_10);
	// page.openEditor(editorInput,
	// "org.txm.rcp.editors.lexicaltable.LexicalTableEditor"); //$NON-NLS-1$
	// } catch (PartInitException e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	// }
	// });
	//
	// monitor.worked(50);
	// if (monitor.isCanceled())
	// return Status.CANCEL_STATUS;
	//
	// monitor.subTask(RCPMessages.ComputeSpecifities_2);
	// syncExec(new Runnable() {
	// @Override
	// public void run() {
	// CorporaView.refresh();
	// CorporaView.expand(table.getParent());
	// QueriesView.refresh();
	// RVariablesView.refresh();
	// }
	// });
	//
	// monitor.worked(100);
	// } catch (ThreadDeath td) {
	// return Status.CANCEL_STATUS;
	// } catch (Exception e) {
	// org.txm.utils.logger.Log.printStackTrace(e);
	// } finally {
	// monitor.done();
	// }
	// return Status.OK_STATUS;
	// }
	// };
	// jobhandler.startJob();
	//
	// return true;
	// }



}
