// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.rcp.editors.LexicalTableEditor;
import org.txm.rcp.messages.TXMUIMessages;

/**
 * Allows the user to delete lines in a Lexical Table.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class DeleteLines extends AbstractHandler {

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();
		// ISelection selection =
		// HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();

		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event)
				.getActivePage().getActivePart();
		if (editor instanceof LexicalTableEditor) {
			LexicalTableEditor LTeditor = (LexicalTableEditor) editor;
			try {
				deleteLexicalTableLines(LTeditor);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 
	 * @param editor
	 * @throws Exception
	 */
	public static void deleteLexicalTableLines(LexicalTableEditor editor) throws Exception {
		int[] selection = editor.getTableViewer().getTable().getSelectionIndices();
		if (selection.length == 0)
			return;

		if (selection.length == editor.getLexicalTable().getNRows()) {
			System.out.println(TXMUIMessages.theTableMustContainAtLeastOneLine);
			return;
		}

		Display d = Display.getCurrent();
		if (d != null) {
			String[] rows = editor.getResult().getRowNames().asStringsArray();
			StringBuffer buffer = new StringBuffer();
			for (int i : selection) {
				if (buffer.length() > 0) buffer.append(", ");
				buffer.append("'" + rows[i] + "'");
			}
			if (!MessageDialog.openQuestion(d.getActiveShell(), TXMUIMessages.common_areYouSure, "Do you really want to delete the following lines: " + buffer)) {
				return;
			}
		}

		deleteLexicalTableLines(editor.getLexicalTable(), selection);

		editor.compute(false);
	}

	/**
	 * 
	 * @param editor
	 * @throws Exception
	 */
	public static void deleteLexicalTableLines(LexicalTable lt, int[] selection) throws Exception {

		lt.getData().removeRows(selection);
		lt.setAltered();
	}

}
