// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.lexicaltable.rcp.handlers;

import java.io.File;
import java.util.Arrays;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TBXPreferences;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.rcp.editors.LexicalTableEditor;
import org.txm.lexicaltable.rcp.messages.LexicalTableUIMessages;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.utils.FileUtils;
import org.txm.utils.logger.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class ImportTable.
 */
public class ImportTable extends BaseAbstractHandler {

	private static final String ID = "org.txm.rcp.commands.editor.ImportTable"; //$NON-NLS-1$
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IWorkbenchPart editor = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();

		Object sel = this.getCorporaViewSelectedObject(event);

		String encoding = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_ENCODING);
		String colseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_COL_SEPARATOR);
		String txtseparator = TBXPreferences.getInstance().getString(TBXPreferences.EXPORT_TXT_SEPARATOR);

		if (editor instanceof LexicalTableEditor LTeditor) {
			//LexicalTableEditor LTeditor = (LexicalTableEditor) editor;
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			FileDialog d = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				d.setFilterPath(LastOpened.getFolder(ID));
				d.setFileName(LastOpened.getFile(ID));
			}
			d.setFilterExtensions(LTeditor.getResult().getExportTXTExtensions());

			String path = d.open();
			if (path != null) {
				File file = new File(path);

				String ext = "*." + FileUtils.getExtension(file);
				if (Arrays.binarySearch(LTeditor.getResult().getImportTXTExtensions(), ext) < 0) {
					Log.warning(NLS.bind("Import file format is not managed: ''{0}''.", ext));
					return false;
				}

				LastOpened.set(ID, file.getParent(), file.getName());
				if (file.exists()) {
					try {

						LTeditor.getLexicalTable().getData().importData(file, encoding, colseparator, txtseparator);
						SWTEditorsUtils.closeEditors(LTeditor.getLexicalTable(), true);
						Log.info(LexicalTableUIMessages.PleaseReopenTheLexicalTableToSeeTheModifications);
					}
					catch (Exception e) {
						Log.severe(LexicalTableUIMessages.bind(LexicalTableUIMessages.ErrorDataImportFailedP0, e));
						Log.printStackTrace(e);
					}
				}
			}
		}
		else if (sel instanceof LexicalTable lt) {
			//LexicalTable lt = (LexicalTable) sel;
			Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
			FileDialog d = new FileDialog(shell, SWT.OPEN);
			if (LastOpened.getFile(ID) != null) {
				d.setFilterPath(LastOpened.getFolder(ID));
				d.setFileName(LastOpened.getFile(ID));
			}
			d.setFilterExtensions(lt.getImportTXTExtensions());

			String path = d.open();
			if (path != null) {
				File file = new File(path);

				String ext = FileUtils.getExtension(file);
				if (Arrays.binarySearch(lt.getImportTXTExtensions(), "*." + ext) < 0) {
					Log.warning(NLS.bind("Import file format is not managed: ''{0}'' waiting for {1}.", ext, Arrays.toString(lt.getImportTXTExtensions())));
					return false;
				}

				LastOpened.set(ID, file.getParent(), file.getName());
				if (file.exists()) {
					try {
						lt.compute();
						if (!lt.getData().importData(file, encoding, colseparator, txtseparator)) {
							Log.warning(NLS.bind("Import failed: file=''{0}''", file));
							return false;
						}
						lt.setAltered();
						System.out.println(NLS.bind(LexicalTableUIMessages.DataImportedP0ColumnsAndP1Rows, lt.getNColumns(), lt.getNRows()));
						SWTEditorsUtils.closeEditors(lt, true);
						Log.info(LexicalTableUIMessages.PleaseReopenTheLexicalTableToSeeTheModifications);
					}
					catch (Exception e) {
						Log.severe(LexicalTableUIMessages.bind(LexicalTableUIMessages.ErrorDataImportFailedP0, e));
						Log.printStackTrace(e);
					}
				}
			}
		}
		return event;
	}
}
