// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.lexicaltable.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.lexicaltable.core.functions.LexicalTable;
import org.txm.lexicaltable.core.functions.LexicalTableFromFile;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;


/**
 * A factory for creating Adapter objects.
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class LexicalTableAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(LexicalTableAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(LexicalTableAdapterFactory.class).getSymbolicName() + "/icons/lexicaltable.png"); //$NON-NLS-1$ //$NON-NLS-2$

	public static final ImageDescriptor ICON2 = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(LexicalTableAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(LexicalTableAdapterFactory.class).getSymbolicName() + "/icons/lexicaltable_file.png"); //$NON-NLS-1$ //$NON-NLS-2$


	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof LexicalTableFromFile) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON2;
				}
			};
		}
		else if (this.canAdapt(adapterType) && adaptableObject instanceof LexicalTable) {
			return new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			};
		}
		return null;
	}


}
