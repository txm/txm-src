package org.txm.searchengine.ts;

//Copyright © - ANR Textométrie - http://textometrie.ens-lyon.fr
//
//This file is part of the TXM platform.
//
//The TXM platform is free software: you can redistribute it and/or modif y
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//The TXM platform is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with the TXM platform.  If not, see <http://www.gnu.org/licenses/>.
//
// @author Nicolas Mazziotta <nicolas.mazziotta -at- ulg.ac.be>
//
//$LastChangedDate: $
//$LastChangedRevision:  $
//$LastChangedBy: $ 
import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.txm.core.messages.TXMCoreMessages;

/**
 * CommandLine to execute TIGERSearch
 * 
 * @author Nicolas Mazziotta <nicolas.mazziotta -at- ulg.ac.be>
 *
 */
public class TSCmd {

	/**
	 * @param args
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {
		Options options = new Options();
		options.addOption("c", true, "config path (dflt: $HOME/TigerSearch/config/tigersearch.logprop)"); //$NON-NLS-1$ //$NON-NLS-2$
		options.addOption("r", true, "registry path (dflt: $HOME/TigerSearch/TIGERCorpora)"); //$NON-NLS-1$ //$NON-NLS-2$
		options.addOption("i", true, "corpus id (COMPULSORY)"); //$NON-NLS-1$ //$NON-NLS-2$
		options.addOption("q", true, "query string (dflt: [])"); //$NON-NLS-1$ //$NON-NLS-2$
		options.addOption("o", true, "output path (dflt: ."); //$NON-NLS-1$ //$NON-NLS-2$
		options.addOption("h", false, "print help and exit"); //$NON-NLS-1$ //$NON-NLS-2$
		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		HelpFormatter formatter = new HelpFormatter();

		try {
			cmd = parser.parse(options, args);
		}
		catch (ParseException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		String homedir = System.getenv("HOME"); //$NON-NLS-1$
		File configdir = new File(homedir, "TigerSearch/config/tigersearch.logprop"); //$NON-NLS-1$
		File registrydir = new File(homedir, "TigerSearch/TIGERCorpora"); //$NON-NLS-1$
		String outdir = "."; //$NON-NLS-1$
		String id = ""; //$NON-NLS-1$
		String query = "[]"; //$NON-NLS-1$
		if (cmd.hasOption("h")) {  //$NON-NLS-1$
			formatter.printHelp("TSCmd -i Corpus Id [OTHER OPTIONS]", options); //$NON-NLS-1$
			return;
		}
		if (cmd.hasOption("c")) { //$NON-NLS-1$
//$NON-NLS-0$
			configdir = new File(cmd.getOptionValue("c")); //$NON-NLS-1$
		}  
		if (cmd.hasOption("r")) { //$NON-NLS-1$
//$NON-NLS-0$
			registrydir = new File(cmd.getOptionValue("r")); //$NON-NLS-1$
		}  
		if (cmd.hasOption("i")) { //$NON-NLS-1$
//$NON-NLS-0$
			id = cmd.getOptionValue("i"); //$NON-NLS-1$
		}
		else {  
			formatter.printHelp("TSCmd", options); //$NON-NLS-1$
			return;
		}
		if (cmd.hasOption("q")) { //$NON-NLS-1$
//$NON-NLS-0$
			query = cmd.getOptionValue("q"); //$NON-NLS-1$
		}  
		if (cmd.hasOption("o")) { //$NON-NLS-1$
//$NON-NLS-0$
			outdir = cmd.getOptionValue("o"); //$NON-NLS-1$
		}  


		TSCorpusManager manager = new TSCorpusManager(registrydir, configdir);

		if (manager.isInitialized()) {

			TSCorpus corpus = manager.getCorpus(id);
			TSResult result = corpus.query(query);

			System.out.println(TXMCoreMessages.queryWasColon + query);
			System.out.println(String.valueOf(result.getNumberOfMatch()) + TXMCoreMessages.matchesFound);

			// write SVG of all matches
			for (int i = 0; i < result.getNumberOfMatch(); i++) {
				File svgfile = new File(outdir, "result" + String.valueOf(i) + ".svg"); //$NON-NLS-1$ //$NON-NLS-2$
				result.getMatch(i).toSVGFile(svgfile);
			}

		}

		return;

	}
}
