// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.ts;

import ims.tiger.corpus.Header;
import ims.tiger.export.ExportException;
import ims.tiger.export.ExportStopException;
import ims.tiger.export.QueryToSvg;
import ims.tiger.gui.tigergraphviewer.forest.ResultForest;
import ims.tiger.query.api.MatchResult;
import ims.tiger.query.api.QueryEvaluationException;
import ims.tiger.query.api.QueryFilterException;
import ims.tiger.query.api.QueryIndexException;
import ims.tiger.query.api.QueryNormalizationException;
import ims.tiger.query.api.QueryOptimizationException;
import ims.tiger.query.internalapi.InternalCorpusQueryManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.eclipse.osgi.util.NLS;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TBXPreferences;
import org.txm.export.ts.ConcordanceBlocks;
import org.txm.export.ts.MatchInject;
import org.txm.importer.ApplyXsl2;
import org.txm.utils.logger.Log;

/**
 * The Class TSResult.
 */
public class TSResult {

	/** The result. */
	MatchResult result;

	/** The forest. */
	ResultForest forest;

	/** The header. */
	Header header;

	/** The ts corpus. */
	TSCorpus tsCorpus;

	/** The matches. */
	TSMatch[] matches;

	// /** The current match. */
	// int currentMatchNo = 0;
	// TSMatch currentMatch;

	/** The query. */
	private String query;

	/** The manager. */
	private InternalCorpusQueryManager manager;

	/** The corpus. */
	// private TSCorpus corpus;

	/** The tsquerytosvg. */
	QueryToSvg tsquerytosvg = null;

	/**
	 * Instantiates a new tS result.
	 *
	 * @param query the query
	 * @param tsCorpus the ts corpus
	 * @throws QueryFilterException
	 * @throws QueryOptimizationException
	 * @throws QueryIndexException
	 * @throws QueryEvaluationException
	 * @throws QueryNormalizationException
	 * @throws Exception
	 */
	public TSResult(String query, TSCorpus tsCorpus, int sent_min, int sent_max, int match_max) throws Exception {

		this.query = query;
		this.manager = tsCorpus.manager;
		// this.corpus = tsCorpus;
		this.tsCorpus = tsCorpus;

		result = tsCorpus.manager.processQuery(query, sent_min, sent_max, match_max);

		if (result.size() > 0) {
			forest = new ResultForest(result, tsCorpus.manager);
			header = forest.getHeader();

			matches = new TSMatch[result.size()];
			tsquerytosvg = new QueryToSvg(manager, result, forest, header, tsCorpus.config);
		}
		else {
			matches = new TSMatch[0];
		}
	}

	public ResultForest getForest() {
		return forest;
	}

	public MatchResult getMatchResult() {
		return result;
	}

	public InternalCorpusQueryManager getManager() {
		return manager;
	}

	/**
	 * Gets the number of match.
	 *
	 * @return the number of match
	 */
	public int getNumberOfMatch() {
		return result.size();
	}

	// /**
	// * return the no of match, begins with 1.
	// *
	// * @return the current match no
	// */
	// public int getCurrentMatchNo() {
	// return currentMatchNo;
	// }
	//
	// /**
	// * return the no of match, begins with 1.
	// *
	// * @return the current match no
	// */
	// public TSMatch getCurrentMatch() {
	// return currentMatch;
	// }

	// /**
	// * return the no of match, begins with 1.
	// *
	// * @return the current sentence no
	// */
	// public int getCurrentSentenceNo() {
	// System.out.println(TXMCoreMessages.numberOfMatch+this.getNumberOfMatch());
	// System.out.println("current match "+this.getCurrentMatchNo()); //$NON-NLS-1$
	// System.out.println(TXMCoreMessages.numberOfSubMatch+this.getCurrentMatch().getNumberOfSubGraph());
	// System.out.println("current sub graph "+this.getCurrentMatch().getCurrentSubMatchNo()); //$NON-NLS-1$
	// return result.getSentenceNumberAt(this.currentMatchNo); // + 1 ?
	// }

	/**
	 * Gets the match.
	 *
	 * @param matchNo the no
	 * @return the match
	 */
	public TSMatch getMatch(int matchNo) {
		if (matches[matchNo] == null) {
			TSMatch m = new TSMatch(matchNo, this.tsquerytosvg, this);
			matches[matchNo] = m;
		}

		return matches[matchNo];
	}

	public void setDisplayProperties(List<String> tprops, String feature) {

		tsCorpus.setDisplayProperties(this.header, tprops, feature);
	}

	/**
	 * Gets the first.
	 *
	 * @return the first
	 */
	public TSMatch getFirst() {
		if (result.size() > 0) {
			// currentMatchNo = 0;
			// currentMatch = getMatch(0);
			return getMatch(0);
		}
		else {
			return null;
		}
	}

	/**
	 * Gets the last.
	 *
	 * @return the last
	 */
	public TSMatch getLast() {
		if (result.size() > 0) {
			// currentMatchNo = result.size() -1;
			// currentMatch = getMatch(result.size() -1);
			return getMatch(result.size() - 1);
		}
		else {
			return null;
		}
	}
	//
	// /**
	// * Gets the next.
	// *
	// * @return the next
	// */
	// public TSMatch getNext()
	// {
	// int next = currentMatchNo + 1;
	// if (result.size() > next) {
	// currentMatchNo = next;
	// currentMatch = getMatch(next);
	// return currentMatch;
	// } else {
	// return null;
	// }
	// }
	//
	// /**
	// * Gets the previous.
	// *
	// * @return the previous
	// */
	// public TSMatch getPrevious()
	// {
	// int next = currentMatchNo - 1;
	// if (next >= 0 && result.size() > 0) {
	// currentMatchNo = next;
	// currentMatch = getMatch(next);
	// return currentMatch;
	// } else {
	// return null;
	// }
	// }
	//
	// public TSMatch setCurrentMatch(int graphNo) {
	// currentMatch = this.getMatch(graphNo);
	// currentMatchNo = graphNo;
	// return currentMatch;
	// }

	public boolean toXml(File outfile, File xmlFile, File xslFile) throws ExportException, ExportStopException, IOException, TransformerException {
		return toXml(outfile, xmlFile, xslFile, false, 30, new ArrayList<String>(), new ArrayList<String>());
	}

	//TODO move this code somewhere
	public static String CONCSIMPLE = "concordance_simple"; //$NON-NLS-1$

	public static String CONCMOTPIVOT = "concordance_mot-pivot"; //$NON-NLS-1$

	public static String CONCBLOCKS = "concordance_blocks"; //$NON-NLS-1$

	public static String[] EXPORTMETHODS = { CONCSIMPLE, CONCMOTPIVOT, CONCBLOCKS };

	public boolean toConcordance(File csvFile, String method, int cx, List<String> ntTypes, List<String> tTypes, boolean punct) throws Exception {

		if (!Arrays.asList(EXPORTMETHODS).contains(method)) {
			Log.severe(NLS.bind("Cannot find method={0} in the available ones={1}", method, EXPORTMETHODS));
			return false;
		}

		File xmlFile = File.createTempFile(csvFile.getName(), "EXPORTBRUT.xml", csvFile.getParentFile()); //$NON-NLS-1$
		boolean rez = false;
		if (punct) {
			// export match
			//System.out.println("save matches in "+xmlFile);
			this.toXml(xmlFile, false, true, false, false, true, false, 0);
			//FileCopy.copy(xmlFile, new File(xmlFile.getParentFile(), "EXPORTBRUT.xml"));
			// merge with TigerXMLPOSPNC
			File tmp = File.createTempFile("txm", "AFTERMINJECT.xml", xmlFile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$
			File tigerXml = new File(tsCorpus.tsmanager.getRegistryPath(), "TigerPnc.xml"); //$NON-NLS-1$
			//System.out.println("TIGER XML: "+tigerXml);
			if (!tigerXml.exists()) {
				System.out.println(NLS.bind("Error no TIGER XML {0}", tigerXml.getAbsolutePath()));
				return false;
			}
			//System.out.println("Match inject: in "+tmp);
			new MatchInject().script(tigerXml, xmlFile, tmp);
			xmlFile.delete();
			tmp.renameTo(xmlFile);
			//FileCopy.copy(xmlFile, new File(xmlFile.getParentFile(), "AFTERMINJECT.xml"));
		}
		else {
			this.toXml(xmlFile); // export match + corpus
			//FileCopy.copy(xmlFile, new File(xmlFile.getParentFile(), "FULLEXPORT.xml"));
		}

		if (!xmlFile.exists()) {
			System.out.println(NLS.bind("Error result file not written by the MatchInject process {0}", xmlFile.getAbsolutePath()));
			return false;
		}

		if (method.equals("concordance_blocks")) { //$NON-NLS-1$
			ConcordanceBlocks builder = new ConcordanceBlocks();
			rez = builder.process(xmlFile, csvFile, cx, ntTypes, tTypes);
		}
		else { // XSL method
			File xslDir = new File(TBXPreferences.getInstance().getString(TBXPreferences.USER_TXM_HOME), "xsl"); //$NON-NLS-1$
			File xslFile = new File(xslDir, method + ".xsl"); //$NON-NLS-1$
			if (!xslFile.exists()) {
				Log.severe(NLS.bind("Error XSL File not found: {0}", xslFile));
				return false;
			}

			rez = toXml(csvFile, xmlFile, xslFile, punct, cx, ntTypes, tTypes);
		}
		xmlFile.delete(); // no more needed
		return rez;
	}

	public boolean toXml(File outfile) throws ExportException, ExportStopException {
		toXml(outfile, true, true);
		return true;
	}

	public boolean toXml(File outfile, boolean includeNonMatch, boolean includeMatch,
			boolean includeXmlHeader, boolean includeXMLSentenceStructure,
			boolean includeXMLMatchInformation, boolean refineSchema, int referSchema) throws ExportException, ExportStopException {
		this.tsCorpus.exporter.setConfiguration(includeXmlHeader,
				includeXMLSentenceStructure,
				includeXMLMatchInformation,
				refineSchema,
				referSchema);
		this.tsCorpus.exporter.saveMatchAsXML(result, outfile, includeNonMatch, includeMatch);
		return true;
	}

	public boolean toXml(File outfile, boolean includeNonMatch, boolean includeMatch) throws ExportException, ExportStopException {
		boolean includeXmlHeader = true;
		boolean includeXMLSentenceStructure = true;
		boolean includeXMLMatchInformation = true;
		boolean refineSchema = false;
		int referSchema = 0;

		return toXml(outfile, includeNonMatch, includeMatch, includeXmlHeader, includeXMLSentenceStructure, includeXMLMatchInformation, refineSchema, referSchema);
	}

	// @Deprecated
	// private void injectPunct(File xmlfile)
	// {
	// System.out.println(TXMCoreMessages.TSResult_18);
	// String corpus = this.tsCorpus.id.toUpperCase();
	// try {
	// Document dom = DomUtils.load(xmlfile);
	// //System.out.println("Getting words of "+corpus);
	// Object words = PunctInject.getWords(corpus, ""); //$NON-NLS-1$
	//
	// // int i = 0;
	// // for(String[] word : (ArrayList<String[]>)words)
	// // {
	// // if(i++ % 10 == 0) System.out.println();
	// // System.out.print("[\""+word[0]+"\", \""+word[1].replace("\"", "\\\"")+"\"], ");
	// // }
	// File outfile = File.createTempFile("punct", ".xml", xmlfile.getParentFile()); //$NON-NLS-1$ //$NON-NLS-2$
	// //System.out.println("Processing "+xmlfile+" to "+outfile);
	// Document doc = (Document) new PunctInject().process(dom, words);
	// //System.out.println("Saving file");
	// DomUtils.save(doc, outfile);
	//
	// // FileCopy.copy(outfile, new File(outfile.getParentFile(), "afterinject.xml"));
	//
	// xmlfile.delete();
	// outfile.renameTo(xmlfile);
	// } catch (UnsupportedEncodingException e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// } catch (FileNotFoundException e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// } catch (ParserConfigurationException e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// } catch (SAXException e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// org.txm.utils.logger.Log.printStackTrace(e);
	// }
	//
	//
	// }

	public boolean toXml(File outFile, File xmlFile, File xslFile, boolean punct, int cxsize, List<String> list, List<String> list2) throws ExportException, ExportStopException, IOException,
			TransformerException {
		ApplyXsl2 xslProc = new ApplyXsl2(xslFile);
		xslProc.setParam("cx", cxsize); //$NON-NLS-1$
		xslProc.setParam("ntTypes", list.toString().replaceAll("[\\[\\],]", "")); // ["pos", "lem"] //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		xslProc.setParam("tTypes", list2.toString().replaceAll("[\\[\\],]", "")); // ["truc", "machin"] //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (xslProc.process(xmlFile, outFile)) {
			return outFile.exists();
		}
		else {
			return false;
		}
	}
}
