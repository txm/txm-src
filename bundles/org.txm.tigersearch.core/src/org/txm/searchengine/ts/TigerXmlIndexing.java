package org.txm.searchengine.ts;

import ims.tiger.index.writer.IndexBuilderErrorHandler;
import ims.tiger.index.writer.SimpleErrorHandler;
import ims.tiger.index.writer.XMLIndexing;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.txm.core.messages.TXMCoreMessages;
import org.xml.sax.SAXException;

public class TigerXmlIndexing {

	/* =================================================== */
	/* MAIN */
	/* =================================================== */

	public static void main(String args[]) {

		BasicConfigurator.configure();

		String userdir = System.getProperty("user.home"); //$NON-NLS-1$
		File master = new File(userdir, "TXM/corpora/roland/tiger/master_pos.xml"); //$NON-NLS-1$
		if (!master.exists()) System.out.println(TXMCoreMessages.eRRORColonExists);
		if (!master.canRead()) System.out.println(TXMCoreMessages.eRRORColonRead);

		String uri = master.getAbsolutePath();
		File outdir = new File(userdir, "TXM/corpora/roland/tiger/data"); //$NON-NLS-1$
		outdir.delete();
		String dest = outdir.getAbsolutePath();

		//    String uri = "sources/tiger.xml";
		//    String dest = "/projekte/TIGER/java/testdir/work/TIGERCorpora/TIGER-250/";

		try {
			IndexBuilderErrorHandler handler = new SimpleErrorHandler(dest);
			XMLIndexing indexing = new XMLIndexing("TEST", uri, dest, handler, false); //$NON-NLS-1$
			indexing.startIndexing();
		}
		catch (IOException e) {
			System.out.println(TXMCoreMessages.iOColon + e.getMessage());
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (SAXException e) {
			System.out.println(TXMCoreMessages.sAXColon + e.getMessage());
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}
}
