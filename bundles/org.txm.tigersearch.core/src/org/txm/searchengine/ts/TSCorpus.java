// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.ts;

import ims.tiger.corpus.Header;
import ims.tiger.export.ExportManager;
import ims.tiger.gui.tigergraphviewer.TIGERGraphViewerConfiguration;
import ims.tiger.index.reader.Index;
import ims.tiger.index.reader.IndexException;
import ims.tiger.query.internalapi.InternalCorpusQueryManager;
import ims.tiger.query.processor.CorpusQueryProcessor;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.List;

import org.txm.core.messages.TXMCoreMessages;
import org.txm.utils.io.IOUtils;

/**
 * The Class TSCorpus.
 */
public class TSCorpus {

	/** The id. */
	String id;

	/** The managers. */
	public TSCorpusManager tsmanager;

	public InternalCorpusQueryManagerLocal2 manager = null;

	ExportManager exporter;

	/** The config. */
	TIGERGraphViewerConfiguration config;

	/** The initok. */
	boolean initok = false;

	/** The results. */
	HashMap<String, TSResult> results = new HashMap<>();

	// Additional data for corpus alignment with TXM base corpus (CQP corpus)
	RandomAccessFile offsetsRAFile = null;

	FileChannel offsetsFileChannel = null;

	/**
	 * offset to pass from TIGER to CQP positions
	 */
	MappedByteBuffer offsetsMapped = null; // one offset per tiger position

	/**
	 * 
	 */
	RandomAccessFile presencesRAFile = null;

	FileChannel presencesFileChannel = null;

	/**
	 * 
	 * 1 if the TIGER position has a position in CQP ; 0 if not
	 */
	MappedByteBuffer presencesMapped = null; // one 0/1 boolean per tiger position

	private int[] sentence_starts;

	@Override
	public void finalize() {
		try {
			close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Instantiates a new TS corpus.
	 *
	 * @param corpusId the corpus id
	 * @param tsmanager the tsmanager
	 */
	public TSCorpus(String corpusId, TSCorpusManager tsmanager) {

		String regpath = tsmanager.getRegistryPath();
		String confpath = tsmanager.getconfPath();
		try {
			this.tsmanager = tsmanager;

			manager = new InternalCorpusQueryManagerLocal2(regpath);

			manager.getQueryProcessor();
			config = new TIGERGraphViewerConfiguration(confpath, confpath, confpath);
			this.id = corpusId;
			initok = opencorpus();
			exporter = new ExportManager(manager, ""); //$NON-NLS-1$

			File offsetsFile = new File(regpath, corpusId + "/offsets.data"); //$NON-NLS-1$
			if (offsetsFile.exists()) {
				offsetsRAFile = new RandomAccessFile(offsetsFile, "rw"); //$NON-NLS-1$
				offsetsFileChannel = offsetsRAFile.getChannel();
				offsetsMapped = offsetsFileChannel.map(FileChannel.MapMode.READ_ONLY, 0, offsetsFileChannel.size());
			}

			// out.putInt(positions[i])

			File presencesFile = new File(regpath, corpusId + "/presences.data"); //$NON-NLS-1$

			if (presencesFile.exists()) {
				presencesRAFile = new RandomAccessFile(presencesFile, "rw"); //$NON-NLS-1$
				presencesFileChannel = presencesRAFile.getChannel();
				presencesMapped = presencesFileChannel.map(FileChannel.MapMode.READ_ONLY, 0, presencesFileChannel.size());
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
	}

	public String getID() {
		return this.id;
	}

	public void close() {
		try {
			if (presencesRAFile != null) presencesRAFile.close();
			if (presencesFileChannel != null) presencesFileChannel.close();
			if (offsetsRAFile != null) offsetsRAFile.close();
			if (offsetsFileChannel != null) offsetsFileChannel.close();
			if (sentence_starts != null) sentence_starts = null;
			if (results != null) results.clear();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @return TIGER position shifted to CQP position
	 */
	public int getOffset(int tigerPosition) {
		if (offsetsMapped != null) {
			return offsetsMapped.getInt(tigerPosition * Integer.BYTES);
		}
		else {
			return 0;
		}
	}

	/**
	 * @return TIGER positions shifted to CQP positions
	 */
	public int[] getOffsets(int tigerPositions[]) {
		int[] ret = new int[tigerPositions.length];
		if (offsetsMapped != null) {
			for (int i = 0; i < tigerPositions.length; i++) {
				ret[i] = offsetsMapped.getInt(tigerPositions[i] * Integer.BYTES);
			}
		}

		return ret;
	}

	/**
	 * offset to add when passing to TIGER index to CQP index
	 * 
	 * @return
	 */
	public MappedByteBuffer getOffsetsMapped() {
		return offsetsMapped;
	}

	/**
	 * 
	 * @return 1 if the TIGER position has a position in CQP ; 0 if not
	 */
	public MappedByteBuffer getPresencesMapped() {
		return presencesMapped;
	}

	/**
	 * 
	 * @param tigerPosition
	 * @return 1 if the TIGER position has a position in CQP ; 0 if not
	 */
	public byte getPresence(int tigerPosition) {
		if (presencesMapped != null) {
			return presencesMapped.get(tigerPosition);
		}
		else {
			return 0;
		}
	}

	/**
	 * 
	 * @param tigerPositions
	 * @return 1 if the TIGER position has a position in CQP ; 0 if not
	 */
	public byte[] getPresences(int tigerPositions[]) {
		byte[] ret = new byte[tigerPositions.length];
		if (presencesMapped != null) {
			for (int i = 0; i < tigerPositions.length; i++) {
				ret[i] = presencesMapped.get(tigerPositions[i] * Integer.BYTES);
			}
		}

		return ret;
	}

	public static boolean createLogPropFile(File directory) {
		directory.mkdirs();
		File logprop = new File(directory, "tigersearch.logprop"); //$NON-NLS-1$
		try {
			IOUtils.write(logprop, "# Default log configuration of the TIGERSearch suite\n" + //$NON-NLS-1$
					"log4j.rootLogger=SEVERE,Logfile\n" + //$NON-NLS-1$
					"log4j.logger.ims.tiger.gui.tigersearch.TIGERSearch=SEVERE\n" + //$NON-NLS-1$
					"log4j.appender.Logfile=org.apache.log4j.RollingFileAppender\n" + //$NON-NLS-1$
					"log4j.appender.Logfile.File=\\${user.home}/tigersearch/tigersearch.log\n" + //$NON-NLS-1$
					"log4j.appender.Logfile.MaxFileSize=500KB\n" + //$NON-NLS-1$
					"log4j.appender.Logfile.MaxBackupIndex=1\n" + //$NON-NLS-1$
					"log4j.appender.Logfile.layout=org.apache.log4j.PatternLayout\n" + //$NON-NLS-1$
					"log4j.appender.Logfile.layout.ConversionPattern=%5r %-5p [%t] %c{2} - %m%n"); //$NON-NLS-1$
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void setDisplayProperties(Header header, List<String> tprops, String ntprop) {

		if (tprops != null && tprops.size() > 0) {
			config.setDisplayedTFeatures(header, tprops);
		}
		else {
			config.setDisplayedTFeatures(header, header.getAllTFeatureNames().subList(0, 1));
		}
		if (ntprop != null && ntprop.length() > 0) {
			config.setDisplayedNTFeature(header, ntprop);
		}
		else {
			config.setDisplayedNTFeature(header, header.getAllNTFeatureNames().get(0).toString());
		}
	}

	public InternalCorpusQueryManager getInternalManager() {
		return manager;
	}

	public List<String> getNTFeatures() {

		return manager.getHeader().getAllNTFeatureNames();

	}

	public List<String> getTFeatures() {
		return manager.getHeader().getAllTFeatureNames();
	}

	/**
	 * contains a lot of informations about the corpus
	 * 
	 * @return
	 */
	public Header getHeader() {
		return manager.getHeader();
	}

	/**
	 * Opencorpus.
	 *
	 * @return true, if successful
	 */
	public boolean opencorpus() {
		try {
			manager.loadCorpus(id);
			return true;
		}
		catch (Exception e) {
			System.out.println(TXMCoreMessages.couldntReadCorpusColon + e.getMessage());
		}
		return false;
	}

	/**
	 * Query.
	 *
	 * @param query the query
	 * @return the tS result
	 * @throws Exception
	 */
	public TSResult query(String query) throws Exception {
		return query(query, -1, -1, -1);
	}

	/**
	 * Query.
	 *
	 * @param query the query
	 * @return the tS result
	 * @throws Exception
	 */
	public TSResult query(String query, int sent_min, int sent_max, int match_max) throws Exception {
		String skey = query + " " + sent_min + " " + sent_max + " " + match_max; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (results.containsKey(skey)) {
			return results.get(skey);
		}

		TSResult rez = new TSResult(query, this, sent_min, sent_max, match_max);
		results.put(skey, rez);
		return rez;
	}

	/**
	 * Checks if is ok.
	 *
	 * @return true, if is ok
	 */
	public boolean isOk() {
		return initok;
	}

	public Index getIndex() {
		InternalCorpusQueryManagerLocal2 tigermanager = this.manager;
		CorpusQueryProcessor processor = tigermanager.getQueryProcessor();
		return processor.getIndex();
	}

	public int[] getSentenceStartPositions() throws IndexException {
		if (sentence_starts != null) {
			return sentence_starts;
		}
		Index index = getIndex();

		sentence_starts = new int[index.getNumberOfGraphs()];
		for (int i = 0; i < index.getNumberOfGraphs(); i++) {
			sentence_starts[i] = 0;
			if (i > 0) {
				sentence_starts[i] += index.getNumberOfTNodes(i - 1) + sentence_starts[i - 1];
			}
		}

		return sentence_starts;
	}

	public TSProperty getTProperty(String name) {
		return new TSProperty(this, name, true);
	}

	public TSProperty getNTProperty(String name) {
		return new TSProperty(this, name, false);
	}
}
