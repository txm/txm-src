// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.ts;

import java.io.File;
import java.util.HashMap;

import org.txm.core.messages.TXMCoreMessages;


/**
 * The Class TSCorpusManager.
 */
public class TSCorpusManager {

	/** The corpora. */
	HashMap<String, TSCorpus> corpora = new HashMap<String, TSCorpus>();

	/** The registrydir. */
	private File registrydir;

	/** The configdir. */
	private File configFile;

	/** The initok. */
	private boolean initok = false;

	/**
	 * Instantiates a new tS corpus manager.
	 *
	 * @param registrydir the registrydir
	 * @param configdir the configdir
	 */
	public TSCorpusManager(File registrydir, File configdir) {

		this.registrydir = registrydir;
		this.configFile = configdir;

		initok = true;

		if (!(registrydir.exists() && registrydir.canRead())) {
			System.out.println(TXMCoreMessages.errorRegDirColon + registrydir);
			initok = false;
		}

		if (!(configdir.exists() && configdir.canRead())) {
			System.out.println(TXMCoreMessages.errorConfigdirDirColon + configdir);
			initok = false;
		}
	}

	/**
	 * Gets the registry path.
	 *
	 * @return the registry path
	 */
	public String getRegistryPath() {

		return registrydir.getAbsolutePath();
	}

	/**
	 * Gets the conf path.
	 *
	 * @return the conf path
	 */
	public String getconfPath() {

		return configFile.getAbsolutePath();
	}

	/**
	 * Checks if is initialized.
	 *
	 * @return true, if is initialized
	 */
	public boolean isInitialized() {

		return initok;
	}

	/**
	 * Gets the corpus.
	 *
	 * @param id the id
	 * @return the corpus
	 */
	public TSCorpus getCorpus(String id) {

		if (!corpora.containsKey(id)) {
			TSCorpus corpus = new TSCorpus(id, this);
			if (corpus.isOk()) {
				corpora.put(id, corpus);
			}
		}

		return corpora.get(id);
	}
}
