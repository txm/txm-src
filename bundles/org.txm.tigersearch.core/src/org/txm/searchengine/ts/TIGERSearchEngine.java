package org.txm.searchengine.ts;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.core.results.TXMResult;
import org.txm.objects.CorpusBuild;
import org.txm.objects.Match;
import org.txm.objects.Project;
import org.txm.searchengine.core.EmptySelection;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEngine;
import org.txm.searchengine.core.SearchEngineProperty;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.tigersearch.preferences.TigerSearchPreferences;
import org.txm.tigersearch.preferences.TigerSearchTreePreferences;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.DeleteDir;
import org.txm.utils.io.IOUtils;
import org.txm.utils.logger.Log;

import cern.colt.Arrays;
import ims.tiger.corpus.Feature;
import ims.tiger.corpus.Header;
import ims.tiger.corpus.Sentence;
import ims.tiger.corpus.T_Node;
import ims.tiger.index.reader.Index;
import ims.tiger.index.reader.IndexException;
import ims.tiger.index.writer.IndexBuilderErrorHandler;
import ims.tiger.index.writer.SimpleErrorHandler;
import ims.tiger.index.writer.XMLIndexing;
import ims.tiger.query.api.MatchResult;
import ims.tiger.query.api.QueryIndexException;
import ims.tiger.query.processor.CorpusQueryProcessor;

public class TIGERSearchEngine extends SearchEngine {

	public static final String NAME = "TIGER"; //$NON-NLS-1$

	HashMap<CorpusBuild, TSCorpus> corpora = null;

	public TSCorpus getTSCorpus(CorpusBuild corpus) {

		CorpusBuild root = corpus.getRootCorpusBuild();
		TSCorpus tscorpus = corpora.get(root);
		if (tscorpus != null) {
			return tscorpus;
		}

		File tigerDirectory = new File(root.getProjectDirectory(), "tiger"); //$NON-NLS-1$
		File configfile = new File(tigerDirectory, "tigersearch.logprop"); //$NON-NLS-1$
		if (!tigerDirectory.exists()) {
			return null;
		}
		if (!configfile.exists()) {
			return null;
		}

		TSCorpusManager manager = new TSCorpusManager(tigerDirectory, configfile);
		tscorpus = manager.getCorpus(root.getID());
		if (tscorpus != null) {
			corpora.put(root, tscorpus);
			return tscorpus;
		}
		else {
			return null;
		}
	}

	public TSCorpus removeTSCorpus(CorpusBuild corpus) {
		CorpusBuild root = corpus.getRootCorpusBuild();
		return corpora.remove(root);
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	/**
	 * 
	 * @param cqpCorpus the targeted CQPCorpus
	 * @return the first sentence and last sentence id (from 0 to N, N the number of sentences). WARNING: this is not the list of sentences in the targeted CQPCorpus. unless the CQPcorpus is contigues
	 * 
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 */
	public static int[] getSentMinMax(CQPCorpus cqpCorpus) throws UnexpectedAnswerException, IOException, CqiServerError, CqiClientException {
		List<? extends org.txm.objects.Match> matches = cqpCorpus.getMatches();
		if (matches.size() == 0) {
			return new int[] { 0, 0 };
		}

		return getSentMinMax(cqpCorpus, matches.get(0).getStart(), matches.get(matches.size() - 1).getEnd());
	}

	/**
	 * 
	 * @param cqpCorpus the targeted CQPCorpus
	 * @return the first sentence and last sentence id (from 0 to N, N the number of sentences). WARNING: this is not the list of sentences in the targeted CQPCorpus. unless the CQPcorpus is contigues
	 * 
	 * @throws UnexpectedAnswerException
	 * @throws IOException
	 * @throws CqiServerError
	 * @throws CqiClientException
	 */
	public static int[] getSentMinMax(CQPCorpus cqpCorpus, int start, int end) throws UnexpectedAnswerException, IOException, CqiServerError, CqiClientException {
		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
		List<? extends org.txm.objects.Match> matches = cqpCorpus.getMatches();
		if (matches.size() == 0) {
			return new int[] { 0, 0 };
		}
		int[] cpos = new int[] { start, end };
		int[] structs = CQI.cpos2Struc(cqpCorpus.getStructuralUnit("s").getProperty("n").getQualifiedName(), cpos); //$NON-NLS-1$ //$NON-NLS-2$
		if (structs.length == 0) {
			return new int[] { 0, 0 };
		}
		int sent_min = structs[0];
		int sent_max = structs[structs.length - 1];
		return new int[] { sent_min, sent_max };
	}

	/**
	 * 
	 * @return true because TIGER queries are frequently multi lines
	 */
	@Override
	public boolean hasMultiLineQueries() {
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		corpora = new HashMap<>();
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		if (corpora != null) {
			for (TSCorpus corpus : corpora.values()) {
				corpus.close(); // free memory (mmap, etc.)
			}
			corpora.clear();
		}
		return true;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Selection query(CorpusBuild corpus, IQuery query, String name, boolean saveQuery) throws Exception {

		TSCorpus tcorpus = this.getTSCorpus(corpus);
		TSResult result = queryTIGER(corpus, query);

		if (result == null) { // no result
			return new EmptySelection(query);
		}

		return fromTIGERMatchToSelection(tcorpus, result, query, corpus);
	}

	public TSResult queryTIGER(CorpusBuild corpus, IQuery query) throws Exception {

		TSCorpus tcorpus = this.getTSCorpus(corpus);
		TSResult result = null;
		if (corpus == corpus.getRootCorpusBuild() || !(corpus instanceof CQPCorpus)) { // root corpus or something not a CQPCorpus
			result = tcorpus.query(query.getQueryString().replace("\n", " ")); //$NON-NLS-1$ //$NON-NLS-2$
		}
		else {
			CQPCorpus cqpCorpus = (CQPCorpus) corpus;
			AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
			List<? extends Match> matches = cqpCorpus.getMatches();
			if (matches.size() == 0) {
				return result;
			}
			int[] cpos = { matches.get(0).getStart(), matches.get(matches.size() - 1).getEnd() };

			//apply shifts on start and end positions
			MappedByteBuffer offsetsMapped = tcorpus.getOffsetsMapped();
			if (offsetsMapped != null) {
				cpos[0] -= offsetsMapped.getInt(cpos[0] * Integer.BYTES);
				cpos[1] -= offsetsMapped.getInt(cpos[1] * Integer.BYTES);
			}

			int[] starts = tcorpus.getSentenceStartPositions();
			int sent_max = starts.length;
			int sent_min = 0;
			for (int i = 0; i < starts.length; i++) {
				if (starts[i] < cpos[0]) {
					sent_min = i;
				}
				else if (starts[i] > cpos[1]) {
					sent_max = i;
					break;
				}
			}

			Log.finest("QUERYING sentences: " + sent_min + " -> " + sent_max); //$NON-NLS-1$ //$NON-NLS-2$
			result = tcorpus.query(query.getQueryString().replace("\n", " "), sent_min, sent_max, -1); //$NON-NLS-1$ //$NON-NLS-2$
		}

		return result;
	}

	public static Selection fromTIGERMatchToSelection(TSCorpus tcorpus, TSResult result, IQuery query, CorpusBuild corpus) throws IndexException {

		MatchResult mresult = result.getMatchResult();
		int size = mresult.size();
		// System.out.println("size: "+size);
		int subsize = mresult.submatchSize();
		if (size == 0 || subsize == 0) {
			return new EmptySelection(query);
		}

		Index index = tcorpus.getIndex();

		// compute sentence positions
		// TODO move it to TSCorpus
		int[] starts = tcorpus.getSentenceStartPositions();

		LinkedHashSet<TIGERMatch> tigerMatchesList = new LinkedHashSet<>();

		List<String> variables = java.util.Arrays.asList(mresult.getVariableNames());
		// System.out.println("Variables: "+variables+" iPivot="+variables.indexOf("pivot"));
		int iPivot = variables.indexOf("pivot"); //$NON-NLS-1$

		MappedByteBuffer offsetsMapped = tcorpus.getOffsetsMapped();
		MappedByteBuffer presenceMapped = tcorpus.getPresencesMapped();
		// MappedByteBuffer offsetsMapped = tcorpus.getOffsetsMapped();

		ArrayList<String> warnings = new ArrayList<String>();

		boolean useSubMatches = TigerSearchTreePreferences.getInstance().getBoolean(TigerSearchTreePreferences.USESUBMATCHES);

		// System.out.println("submatchSize: "+subsize);
		for (int isentMatch = 0; isentMatch < size; isentMatch++) { // the matching sentences
			int sent = mresult.getSentenceNumberAt(isentMatch);
			// Sentence sentence = tcorpus.manager.getSentence(sent);

			// System.out.println(" sent: "+sent);
			int sent_submatch = mresult.getSentenceSubmatchSize(sent);

			// System.out.println(" sent submatch size: "+sent_submatch);
			for (int iSubMatch = 0; iSubMatch < sent_submatch; iSubMatch++) { // the matches in the sentence
				int[] match = mresult.getSentenceSubmatchAt(sent, iSubMatch);

				int sent_start = starts[sent];

				// System.out.println(" sent="+sent_start+ " matches="+Arrays.toString(match)+" ipivot="+iPivot);
				for (int i = 0; i < match.length; i++) {

					if (iPivot != -1 && i != iPivot) continue; // skip match that are not 'pivot'

					int left = sent_start + index.getLeftCorner(sent, match[i]);
					int right = sent_start + index.getRightCorner(sent, match[i]);

					// test if the match position is also in the CQP positions
					if (presenceMapped.get(left) > 0 && presenceMapped.get(right) > 0) {

						if (offsetsMapped != null && presenceMapped != null) { // the TIGER token is not in the CQP corpus
							if (presenceMapped.get(left) > 0) {
								left += offsetsMapped.getInt(left * Integer.BYTES);
							}
							// System.out.println("left="+left+" offset="+offsetsMapped.getInt(left*Integer.BYTES));
						}

						if (offsetsMapped != null && presenceMapped != null) { // the TIGER token is not in the CQP corpus
							if (presenceMapped.get(right) > 0) {
								right += offsetsMapped.getInt(right * Integer.BYTES);
							}
						}
						// System.out.println(" M="+match[i]+" ("+left+", "+right+")");

						TIGERMatch tigerMatch = new TIGERMatch(left, right);

						// System.out.println(" ajusted="+(tigerMatch));
						tigerMatchesList.add(tigerMatch);

						if (!useSubMatches) { // use only the first submatch
							break;
						}
					}
					else {
						warnings.add("<" + left + ", " + right + ">");
					}
				}
			}
		}

		if (warnings.size() > 0) {
			Log.warning("Some TIGER matches are not in the CQP corpus: " + StringUtils.join(warnings, ", "));
		}

		// intersect with corpus matches
		List<? extends Match> result2 = Match.intersect(corpus.getMatches(), new ArrayList<>(tigerMatchesList), true);

		return new TIGERSelection(query, result2);
	}

	@Override
	public Query newQuery() {
		return new TIGERQuery();
	}

	@Override
	public boolean hasIndexes(CorpusBuild corpus) {
		if (corpus == null) return false;

		// TODO implement a corpora of TIGER corpus
		CorpusBuild root = corpus.getRootCorpusBuild();
		File buildDirectory = new File(root.getProjectDirectory(), "tiger"); //$NON-NLS-1$
		return new File(buildDirectory, "tigersearch.logprop").exists() && //$NON-NLS-1$
				new File(buildDirectory, root.getID()).exists();
	}

	@Override
	public void notify(TXMResult r, String state) {
		//		if (r instanceof MainCorpus && "clean".equals(state)) { // the CQP corpus has been deleted by the user
		//			MainCorpus c = (MainCorpus) r;
		//			File buildDirectory = new File(c.getProjectDirectory(), "tiger/" + c.getID());
		//			if (buildDirectory.exists()) {
		//				DeleteDir.deleteDirectory(buildDirectory);
		//			}
		//		}
		//		else
		if (r instanceof Project && "clean".equals(state)) {  // the Project has been deleted by the user //$NON-NLS-1$
			Project c = (Project) r;
			File buildDirectory = new File(c.getProjectDirectory(), "tiger"); //$NON-NLS-1$
			if (buildDirectory.exists()) {
				DeleteDir.deleteDirectory(buildDirectory);
			}
		}
	}

	@Override
	public String getValueForProperty(Match match, SearchEngineProperty property) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getValuesForProperty(Match match, SearchEngineProperty property) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 * @param sourceDirectory must contain the main.xml file
	 * @param binaryDirectory used to create the "tiger" directory
	 * @param corpusName
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static boolean buildTIGERCorpus(File sourceDirectory, File binaryDirectory, String corpusName) throws UnsupportedEncodingException, FileNotFoundException {

		File tigerDir = new File(binaryDirectory, "tiger"); //$NON-NLS-1$
		tigerDir.mkdir();

		File logprop = new File(tigerDir, "tigersearch.logprop"); //$NON-NLS-1$

		PrintWriter writer = IOUtils.getWriter(logprop, "UTF-8"); //$NON-NLS-1$
		writer.println("# Default log configuration of the TIGERSearch suite" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.rootLogger=INFO,Logfile" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.logger.ims.tiger.gui.tigersearch.TIGERSearch=SEVERE" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.appender.Logfile=org.apache.log4j.RollingFileAppender" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.appender.Logfile.File=" + logprop.getAbsolutePath() + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.appender.Logfile.MaxFileSize=500KB" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.appender.Logfile.MaxBackupIndex=1" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.appender.Logfile.layout=org.apache.log4j.PatternLayout" + "\n" + //$NON-NLS-1$ //$NON-NLS-2$
				"log4j.appender.Logfile.layout.ConversionPattern=%5r %-5p [%t] %c{2} - %m%n"); //$NON-NLS-1$
		writer.close();
		BasicConfigurator.configure();

		String driverFilename = TigerSearchPreferences.getInstance().getString(TigerSearchPreferences.DRIVER_FILENAME);

		File master = new File(sourceDirectory, driverFilename);
		if (!master.exists()) {
			//			File[] xmlFiles = sourceDirectory.listFiles(new FileFilter() {
			//				public boolean accept(File file) {
			//					if (file.isDirectory()) return false;
			//					if (file.isHidden()) return false;
			//					String filename = file.getName();
			//					if (filename.equals("import.xml")) return false;
			//					if (!filename.endsWith(".xml")) return false;
			//
			//					return true;
			//				}
			//			});
			//
			//			if (xmlFiles == null) {
			System.out.println("No master file found in: " + sourceDirectory);
			return false;
			//			}
			//			master = xmlFiles[0];
		}
		String uri = master.getAbsolutePath(); // TIGER corpus source root file
		File tigerBinDir = new File(tigerDir, corpusName);
		tigerBinDir.mkdir();
		try {
			LinkedHashMap<String, ArrayList<String>> errorsPerTIGERSubcorpus = new LinkedHashMap<>();
			IndexBuilderErrorHandler handler = new SimpleErrorHandler(tigerBinDir.getAbsolutePath()) {

				String currentSubcorpus = "";

				public void setMessage(String message) {
					if (message.startsWith("Reading subcorpus ")) {
						currentSubcorpus = message.substring("Reading subcorpus ".length());
					}
				}

				public void newSentenceError(String sid, String message) {
					if (!errorsPerTIGERSubcorpus.containsKey(currentSubcorpus)) errorsPerTIGERSubcorpus.put(currentSubcorpus, new ArrayList<String>());
					errorsPerTIGERSubcorpus.get(currentSubcorpus).add("Error at " + sid + ": " + message);
				}

				public void newSentenceWarning(String sid, String message) {
					if (!errorsPerTIGERSubcorpus.containsKey(currentSubcorpus)) errorsPerTIGERSubcorpus.put(currentSubcorpus, new ArrayList<String>());
					errorsPerTIGERSubcorpus.get(currentSubcorpus).add("Warning at " + sid + ": " + message);
				}

				public void setNumberOfSentences(int number) {
				}

				public void setProgressBar(int value) {
				}
			};

			XMLIndexing indexing = new XMLIndexing(corpusName, uri, tigerBinDir.getAbsolutePath(), handler, false);
			indexing.startIndexing();
			if (errorsPerTIGERSubcorpus.size() > 0) {
				for (String s : errorsPerTIGERSubcorpus.keySet()) {
					Log.info("In " + s);
					for (String s2 : errorsPerTIGERSubcorpus.get(s)) {
						Log.info("	" + s2);

					}
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}


	public static int writeOffsetDataFiles(MainCorpus corpus, String wordIdAttribute, File tigerCorpusDirectory, File tigerDirectory, File tigerCorpusExistingDirectory)
			throws IndexException, QueryIndexException, UnexpectedAnswerException, IOException, CqiServerError, CqiClientException {

		// TXM corpus files
		File configfile = new File(tigerDirectory, "tigersearch.logprop"); //$NON-NLS-1$

		TSCorpusManager manager = new TSCorpusManager(tigerCorpusDirectory.getParentFile(), configfile);
		TSCorpus tcorpus = manager.getCorpus(tigerCorpusDirectory.getName());
		InternalCorpusQueryManagerLocal2 tigermanager = tcorpus.manager;
		CorpusQueryProcessor processor = tigermanager.getQueryProcessor();
		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();

		Index index = processor.getIndex();
		int size = 0;
		for (int nr = 0; nr < index.getNumberOfGraphs(); nr++) {
			size += index.getNumberOfTNodes(nr);
		}

		if (size == 0) {
			Log.warning("No word found in the TIGERSearch corpus: " + tigerCorpusDirectory + ". Aborting.");
			return 0;
		}

		Log.info("Importing " + size + " word annotations...");

		// compute start position of sentences
		int[] starts = new int[index.getNumberOfGraphs()];
		for (int i = 0; i < index.getNumberOfGraphs(); i++) {
			starts[i] = 0;
			if (i > 0) {
				starts[i] += index.getNumberOfTNodes(i - 1) + starts[i - 1];
			}
		}

		File offsetsFile = new File(tigerCorpusExistingDirectory, "offsets.data"); //$NON-NLS-1$
		RandomAccessFile offsetsRAFile = new RandomAccessFile(offsetsFile, "rw"); //$NON-NLS-1$
		FileChannel offsetsFileChannel = offsetsRAFile.getChannel();
		MappedByteBuffer offsetsMapped = offsetsFileChannel.map(FileChannel.MapMode.READ_WRITE, 0, size * Integer.BYTES);
		// out.putInt(positions[i])

		File presencesFile = new File(tigerCorpusExistingDirectory, "presences.data"); //$NON-NLS-1$
		RandomAccessFile presencesRAFile = new RandomAccessFile(presencesFile, "rw"); //$NON-NLS-1$
		FileChannel presencesFileChannel = presencesRAFile.getChannel();
		MappedByteBuffer presencesMapped = presencesFileChannel.map(FileChannel.MapMode.READ_WRITE, 0, size);

		int numberOfWordsAnnotated = 0;

		// for each sentence
		ConsoleProgressBar cpb = new ConsoleProgressBar(index.getNumberOfGraphs());
		for (int nr = 0; nr < index.getNumberOfGraphs(); nr++) {
			cpb.tick();
			int sent_size = index.getNumberOfTNodes(nr);
			Sentence sent = tcorpus.manager.getSentence(nr);

			String[] ids = new String[sent_size];
			int[] tigerPositions = new int[sent_size];
			for (int t = 0; t < sent_size; t++) {
				T_Node terminal = (T_Node) sent.getTerminalAt(t);
				ids[t] = terminal.getFeature(wordIdAttribute);

				// try fixing ID
				if (ids[t].startsWith("w")) { //$NON-NLS-1$
					if (!ids[t].startsWith("w_")) { //$NON-NLS-1$
						ids[t] = "w_" + ids[t].substring(1); //$NON-NLS-1$
					}
				}
				else {
					ids[t] = "w_" + ids[t]; //$NON-NLS-1$
				}
				tigerPositions[t] = starts[nr] + t;
				// System.out.println("T id="+terminal.getID());
			}

			int[] ids_idx = CQI.str2Id(corpus.getProperty("id").getQualifiedName(), ids); //$NON-NLS-1$
			Integer[] cqpPositions = new Integer[sent_size];
			Integer[] offsets = new Integer[sent_size];
			boolean error = false;
			for (int t = 0; t < sent_size; t++) {
				if (ids_idx[t] >= 0) {
					int[] positions = CQI.id2Cpos(corpus.getProperty("id").getQualifiedName(), ids_idx[t]); //$NON-NLS-1$
					if (positions.length > 1) {
						Log.warning("Warning: multiple CQP positions for word_id=" + ids[t]);
					}
					cqpPositions[t] = positions[0]; // take the first position
				}
				else { // word not in the CQP corpus
					Log.warning("Could not find word for id=" + ids[t]);

					cqpPositions[t] = null;
					error = true;
				}

				if (cqpPositions[t] != null) {
					offsets[t] = cqpPositions[t] - tigerPositions[t];
				}
				else {
					offsets[t] = null;
				}
			}
			if (error) {
				Log.warning("	IDS      =" + " " + ids.length + " " + Arrays.toString(ids));
				Log.warning("	IDS_IDX  =" + " " + ids_idx.length + " " + Arrays.toString(ids_idx));
				Log.warning("	CQP      =" + " " + cqpPositions.length + " " + Arrays.toString(cqpPositions));
				Log.warning("	TIGER    =" + " " + tigerPositions.length + " " + Arrays.toString(tigerPositions));
				Log.warning("	OFFSET   =" + " " + offsets.length + " " + Arrays.toString(offsets));
			}
			// System.out.println("ids="+Arrays.toString(ids));
			// System.out.println("cqp indexes="+Arrays.toString(ids_idx));
			// System.out.println("tiger positions="+Arrays.toString(tigerPositions));
			// System.out.println("cqp positions="+Arrays.toString(cqpPositions));
			// System.out.println("offsets="+Arrays.toString(offsets));

			// writing data to offset and presences files
			for (int t = 0; t < sent_size; t++) {

				if (offsets[t] != null) {
					numberOfWordsAnnotated++;
					presencesMapped.put((byte) 1);
					offsetsMapped.putInt(offsets[t]);
				}
				else {
					presencesMapped.put((byte) 0);
					offsetsMapped.putInt(0);
				}
			}
		}
		cpb.done();

		offsetsFileChannel.close();
		offsetsRAFile.close();
		presencesFileChannel.close();
		presencesRAFile.close();

		return numberOfWordsAnnotated;
	}

	public String hasAdditionalDetailsForResult(TXMResult result) {
		if (result instanceof CQPCorpus) {
			return hasIndexes((CQPCorpus) result) ? "Syntax" : null;
		}
		return null;
	}

	public String getAdditionalDetailsForResult(TXMResult result) {
		if (result instanceof CQPCorpus) {
			TSCorpus tscorpus = this.getTSCorpus((CQPCorpus) result);
			Index index = tscorpus.getIndex();
			Header header = tscorpus.getHeader();
			StringBuilder buffer = new StringBuilder();
			buffer.append("<h3>TIGERSearch informations</h3>\n");
			buffer.append("<h4>Commons</h4>\n");
			buffer.append("<p>Name: " + header.getCorpus_Name() + "</p>\n"); //$NON-NLS-2$
			buffer.append("<p>Id: " + header.getCorpus_ID() + "</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buffer.append("<p>Date: " + header.getCorpus_Date() + "</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$
			buffer.append("<p>Format: " + header.getCorpus_Format() + "</p>\n"); //$NON-NLS-2$
			buffer.append("<p>History: " + header.getCorpus_History() + "</p>\n"); //$NON-NLS-2$
			buffer.append("<h4>Statiticss</h4>\n");
			buffer.append("<p>Edges: " + header.getNumberOfEdges() + "</p>\n"); //$NON-NLS-2$
			buffer.append("<p>Sentences: " + header.getNumberOfSentences() + "</p>\n"); //$NON-NLS-2$
			buffer.append("<p>NT Nodes: " + header.getNumberOfNTNodes() + "</p>\n"); //$NON-NLS-2$
			buffer.append("<p>T Nodes: " + header.getNumberOfTNodes() + "</p>\n"); //$NON-NLS-2$
			buffer.append("<h4>Terminals</h4>\n");
			//buffer.append("<p>"+header.getAllTerminalFeatures()+"</p>\n");
			buffer.append("<p>" + header.getAllTerminalFeaturesSize() + " " + StringUtils.join(header.getAllTFeatureNames(), ", ") + "</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			if (header.getAllTFeatureNames().size() > 0) {
				buffer.append("<ul>\n"); //$NON-NLS-1$
				for (Object name : header.getAllTFeatureNames()) {
					Feature f = header.getTFeature(name.toString());
					buffer.append("<li>" + f.getName()); //$NON-NLS-1$
					List<?> desc = f.getDescriptions();
					List<?> vals = f.getItems();
					if (f.getItems().size() > 0) {
						buffer.append(":"); //$NON-NLS-1$
						for (int i = 0; i < vals.size(); i++) {
							if (i > 0) buffer.append(","); //$NON-NLS-1$
							buffer.append(" (" + vals.get(i) + ": " + desc.get(i) + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						}
					}
					else { // not defined
						try {
							LinkedHashSet<String> values = new LinkedHashSet<String>();
							for (int p = 0; p < 20; p++) {
								values.add(index.getTFeatureValueAt(name.toString(), p));
							}
							if (values.size() > 0) {
								buffer.append(": " + StringUtils.join(values, ", ")); //$NON-NLS-1$ //$NON-NLS-2$
							}
						}
						catch (IndexException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					buffer.append("</li>\n"); //$NON-NLS-1$
				}
				buffer.append("</ul>\n"); //$NON-NLS-1$
			}

			buffer.append("<h4>Non-Terminals</h4>\n");
			//buffer.append("<p>"+header.getAllNonterminalFeatures()+"</p>\n");
			buffer.append("<p>" + header.getAllNonterminalFeaturesSize() + " " + StringUtils.join(header.getAllNTFeatureNames(), ", ") + "</p>\n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			if (header.getAllNTFeatureNames().size() > 0) {
				buffer.append("<ul>\n"); //$NON-NLS-1$
				for (Object name : header.getAllNTFeatureNames()) {
					Feature f = header.getNTFeature(name.toString());
					buffer.append("<li>" + f.getName()); //$NON-NLS-1$
					List<?> desc = f.getDescriptions();
					List<?> vals = f.getItems();
					if (f.getItems().size() > 0) {
						buffer.append(":"); //$NON-NLS-1$
						for (int i = 0; i < vals.size(); i++) {
							if (i > 0) buffer.append(","); //$NON-NLS-1$
							if (vals.get(i).equals(desc.get(i))) {
								buffer.append(" " + vals.get(i)); //$NON-NLS-1$
							}
							else {
								buffer.append(" " + vals.get(i) + " (" + desc.get(i) + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							}
						}
					}
					buffer.append("</li>\n"); //$NON-NLS-1$
				}
				buffer.append("</ul>\n"); //$NON-NLS-1$
			}

			return buffer.toString();
		}
		return null;
	}

	public static TIGERSearchEngine getInstance() {
		return (TIGERSearchEngine) SearchEnginesManager.getTIGERSearchEngine();
	}
}
