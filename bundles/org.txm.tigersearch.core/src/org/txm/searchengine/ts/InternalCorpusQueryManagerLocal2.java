package org.txm.searchengine.ts;

import ims.tiger.query.api.CorpusQueryManagerException;
import ims.tiger.query.internalapi.InternalCorpusQueryManagerLocal;
import ims.tiger.query.processor.CorpusQueryProcessor;

public class InternalCorpusQueryManagerLocal2 extends InternalCorpusQueryManagerLocal {

	public InternalCorpusQueryManagerLocal2(String corpus_base_directory) throws CorpusQueryManagerException {
		super(corpus_base_directory);
	}

	public CorpusQueryProcessor getQueryProcessor() {
		return query_processor;
	}
}
