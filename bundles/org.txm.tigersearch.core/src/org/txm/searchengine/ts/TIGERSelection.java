package org.txm.searchengine.ts;

import java.util.ArrayList;
import java.util.List;

import org.txm.objects.Match;
import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Selection;

public class TIGERSelection extends Selection {

	List<? extends Match> matches;

	IQuery query;

	public TIGERSelection(IQuery query, List<? extends Match> result2) {
		this.query = query;
		matches = result2;
	}

	@Override
	public List<? extends Match> getMatches() throws Exception {
		return matches;
	}

	@Override
	public List<? extends Match> getMatches(int from, int to) throws Exception {
		if (to > matches.size()) to = matches.size();
		return matches.subList(from, to + 1); // +1 because Match.getMatches is start-end inclusive and List.subList is start inclusif and end exclusif
	}

	@Override
	public int getNMatch() throws Exception {
		return matches.size();
	}

	@Override
	public IQuery getQuery() {
		return query;
	}

	@Override
	public boolean isTargetUsed() throws Exception {
		return false;
	}

	@Override
	public void drop() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean delete(int iMatch) {
		if (iMatch < 0) return false;
		if (iMatch >= matches.size()) return false;
		return matches.remove(iMatch) != null;
	}

	@Override
	public boolean delete(Match match) {
		return matches.remove(match);
	}

	@Override
	public boolean delete(ArrayList<Match> matchesToRemove) {
		return matches.removeAll(matchesToRemove);
	}
}
