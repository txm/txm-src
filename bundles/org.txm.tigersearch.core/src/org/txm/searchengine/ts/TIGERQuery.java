package org.txm.searchengine.ts;

import org.txm.searchengine.core.IQuery;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEnginesManager;

public class TIGERQuery extends Query {

	public TIGERQuery() {
		super("", SearchEnginesManager.getTIGERSearchEngine()); //$NON-NLS-1$
	}

	public TIGERQuery(String query) {

		super(query, SearchEnginesManager.getTIGERSearchEngine());
	}

	@Override
	public boolean isEmpty() {
		return queryString != null && queryString.isEmpty();
	}

	@Override
	public IQuery fixQuery(String lang) {
		return this;
	}

	public boolean canBeMultiLine() {
		return true;
	}

//	@Override
//	public boolean equals(IQuery q) {
//		return this.getClass().equals(q.getClass()) && this.getQueryString().equals(q.getQueryString());
//	}
}
