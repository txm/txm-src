package org.txm.searchengine.ts;

import org.txm.searchengine.core.SearchEngineProperty;
import org.txm.searchengine.cqp.corpus.query.Match;

import ims.tiger.index.reader.Index;
import ims.tiger.query.processor.CorpusQueryProcessor;

public class TSProperty implements SearchEngineProperty {

	TSCorpus tcorpus;

	String name;

	boolean T;

	public TSProperty(TSCorpus corpus, String name, boolean T) {
		this.tcorpus = corpus;
		this.name = name;
		this.T = T;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFullName() {
		return tcorpus.getHeader().getCorpus_ID() + "_" + name; //$NON-NLS-1$
	}

	public String getValue(Match m) {
		if (m == null) return null;

		InternalCorpusQueryManagerLocal2 tigermanager = tcorpus.manager;
		CorpusQueryProcessor processor = tigermanager.getQueryProcessor();
		Index index = processor.getIndex();

		//TODO not finished

		return null;
	}

}
