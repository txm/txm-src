// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.searchengine.ts;

import ims.tiger.export.QueryToSvg;
import ims.tiger.gui.tigergraphviewer.draw.Display_Sentence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * The Class TSMatch.
 */
public class TSMatch {

	/** The rez. */
	QueryToSvg svgBuilder;

	TSResult result;

	/** The sentid. */
	int matchNo;

	int currentSubGraph = 0;

	int nbOfSubGraph = 0;

	/**
	 * Instantiates a new tS match.
	 *
	 * @param matchNo the sentid
	 * @param svgBuilder the SVG builder
	 */
	public TSMatch(int matchNo, QueryToSvg svgBuilder, TSResult result) {
		this.matchNo = matchNo;
		this.svgBuilder = svgBuilder;
		this.result = result;
		currentSubGraph = 0;
		result.forest.setSubMatchNumber(0);
		nbOfSubGraph = result.result.getSentenceSubmatchSize(result.result.getSentenceNumberAt(matchNo));
	}

	public TSResult getResult() {
		return result;
	}

	public boolean isTerminal() {
		Display_Sentence s = result.forest.getCurrentMatch();
		return s.getTerminals().size() > 0;
	}

	public Display_Sentence getDisplaySentence() {
		return result.forest.getCurrentMatch();
		//return result.forest.getCurrentMatch();
	}

	public void nextSubGraph() {
		if (currentSubGraph < nbOfSubGraph - 1) {
			currentSubGraph++;
			result.forest.setSubMatchNumber(currentSubGraph);
		}
	}

	public void previousSubGraph() {
		if (currentSubGraph > 0) {
			currentSubGraph--;
			result.forest.setSubMatchNumber(currentSubGraph);
		}
	}

	public void firstSubGraph() {
		currentSubGraph = 0;
		result.forest.setSubMatchNumber(currentSubGraph);
	}

	public void lastSubGraph() {
		if (nbOfSubGraph > 0) {
			currentSubGraph = nbOfSubGraph - 1;
		}
		else {
			currentSubGraph = 0;
		}
		result.forest.setSubMatchNumber(currentSubGraph);
	}

	public void setSubGraph(int subGraphNo) {
		this.currentSubGraph = subGraphNo;
		result.forest.setSubMatchNumber(currentSubGraph);
	}

	/**
	 * To html.
	 *
	 * @return the string
	 */
	public String toHTML() {
		return svgBuilder.getHTMLText(matchNo + 1, currentSubGraph);
	}

	public int getNumberOfSubGraph() {
		return nbOfSubGraph;
	}

	public int getCurrentSubMatchNo() {
		return currentSubGraph;
	}

	/**
	 * To svg. Don't manage sub graph
	 *
	 * @return the string
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String toSVG() throws FileNotFoundException, IOException {
		return svgBuilder.toSVG(matchNo + 1);
	}

	/**
	 * To svg file.
	 *
	 * @param svgfile the svgfile
	 * @return true, if successful
	 */
	public boolean toSVGFile(File svgfile) {
		try {
			svgBuilder.toSVG(svgfile, false, matchNo + 1, currentSubGraph);
			return true;
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			org.txm.utils.logger.Log.printStackTrace(e);
		}
		return false;
	}
}
