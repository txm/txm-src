#! /usr/bin/groovy
package org.txm.export.ts;

import groovy.xml.XmlSlurper
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

//import groovy.util.slurpersupport.NodeChild

/*
 * The script takes the <matches/> elements from file "Tiger_match.xml"
 * and inserts them at the end of the <s/> element bearing the same ID.
 * Inputs: three file names 
 * - TsInputName --- the TS file WITHOUT matches
 * - MatchInputName --- the TS file containing only matches.
 * - OutputFileName ---required output file.
 * To pass these arguments from within an application, call script() directly.
 */

// Filename variables
// def TsInputName = '/home/tomr/Documents/Work/lyon12/srcmf/groovy/MatchInject/ts_input.xml'
// def MatchInputName = '/home/tomr/Documents/Work/lyon12/srcmf/groovy/MatchInject/match_input.xml'
// def OutputFileName = '/home/tomr/Documents/Work/lyon12/srcmf/groovy/MatchInject/test.xml'

// Main code: checks for correct number of arguments if run from cmd line.
if (args && args.size() == 3) {
    script(args[0], args[1], args[2])
} else {
    println '''Incorrect number of arguments: three strings required.

USAGE:
******
groovy MatchInject.groovy TsInput.xml MatchInput.xml OutputFile.xml'''}

def script(String tsInputName, String matchInputName, String outputFileName) {
	script(new File(tsInputName), new File(matchInputName, new File(outputFileName)));
}

// The script.
def script(File tsInputFile, File matchInputFile, File outputFile) {
	//println "loading TsInput..."
    def TsInput = new XmlSlurper().parse(tsInputFile)
	//println "loading matchInputFile..."
	System.setProperty("org.xml.sax.driver", "com.sun.org.apache.xerces.internal.parsers.SAXParser");
	def xmlReader = XMLReaderFactory.createXMLReader();
	xmlReader.setFeature('http://xml.org/sax/features/namespaces', false)
    XmlSlurper mslurper = new XmlSlurper(xmlReader);
	def MatchInput = mslurper.parse(matchInputFile)
	//println "building OutputFile... size="+matchInputFile.length()
	
	def inputSentences = TsInput.'**'.findAll {it.name() == 's'};
	def matcheSentences = MatchInput.'**'.findAll { it.name() == 's' }
//	println "MATCHES"
//	for(NodeChild match : matcheSentences) {
//		//println match.getClass()
//		match.namespacePrefix = ""
//		match.namespaceMap = [:]
//		//println match
//	}
	int count = 0;
	//println("nb of input sentences: "+inputSentences.size());
	
    def markup = {
		mkp.xmlDeclaration()
		//mkp.declareNamespace("svg":"xmlns=\"http://www.w3.org/2000/svg\"")
		
        corpus(id:'TSOut') {
            body {
                inputSentences.each { sPath ->
					//if (count++%200 == 0) println((100*count/inputSentences.size()))
                    s(id:"${sPath.'@id'}") { 
                        mkp.yield(sPath.graph)
                        def sMatches = matcheSentences.find {
                            it.'@id'.toString() == sPath.'@id'.toString()
                        }
						
                        if (sMatches) {
                            mkp.yield(sMatches.matches)
                        }
						//matcheSentences.removeAll(sMatches)
                    }
                }
            }
        }
    }
    def processor = new groovy.xml.StreamingMarkupBuilder().bind(markup)
    outputFile.withWriter { it << groovy.xml.XmlUtil.serialize(processor)}
}