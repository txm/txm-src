package org.txm.export.ts;

import javax.xml.parsers.DocumentBuilderFactory

class ConcordanceBlocks {
	def codec = 'UTF-8'

	int cx = 30;
	def ntTypes = [];
	def tTypes = [];
	File xmlfile, outfile;

	public boolean process(File xmlfile, File outfile, int cx, def ntTypes, def tTypes) {
		this.cx = cx;
		this.xmlfile = xmlfile;
		this.outfile = outfile;
		this.ntTypes = ntTypes;
		this.tTypes = tTypes;

		Writer writer = outfile.newPrintWriter(codec)

		println 'Reading XML File'
		def factory = DocumentBuilderFactory.newInstance()
		factory.setXIncludeAware(true)
		def builder = factory.newDocumentBuilder()
		def records = builder.parse(xmlfile).documentElement
		println 'done.'

		println 'done.  Pre-treating file to combine results with the same pivot...'
		records = matchCombine(records)
		println 'done.'

		println 'Calculating max. number of blocks...'
		def nBlock = 0

		def allMatches = toList(records.getElementsByTagName('match'))

		for (def match : allMatches) {
			nBlock = [
				nBlock,
				toList(match.getElementsByTagName('variable')).findAll{
					it.getAttribute('name').startsWith('#block')
				}.size()
			].max()
		}

		println "done ( $nBlock )"

		// Table headers
		println 'Writing concordance...'

		def header = ['sId', 'LeftCxOutsideSnt', 'LeftCxInsideSnt']

		for (int i = nBlock ; i > 0 ; i--) {

			header.add("${i}BlockBeforePivot")

			for (int j = 0 ; j < [ntTypes.size(), tTypes.size()].max() ; j++) {

				header.add("${i}BlockBeforePivotType${j+1}")

			}
		}

		header.add('Pivot')

		for (int j = 0 ; j < [ntTypes.size(), tTypes.size()].max() ; j++) {

			header.add("PivotType${j+1}")

		}

		for (int i = 1 ; i <= nBlock ; i++) {

			header.add("${i}BlockAfterPivot")

			for (int j = 0 ; j < [ntTypes.size(), tTypes.size()].max() ; j++) {

				header.add("${i}BlockAfterPivotType${j+1}")

			}
		}

		header.addAll(['RightCxInsideSnt', 'RightCxOutsideSnt', 'Warnings'])

		writer.write(header.join("\t")+"\n");

		def allTerminals = toList(records.getElementsByTagName('t'))
		int tenPercentile = 0

		int i = 0;
		for (def aMatchNode : allMatches) {
			if ((int)(((float)++i / allMatches.size()) * 10) > tenPercentile)
				println ""+(++tenPercentile * 10)+ ' percent complete...'

			def (rowDict, inSntCxLengthLeft, inSntCxLengthRight) = match2CSVrow(aMatchNode, header);
			
			// Add out-of-sentence context
			def sNode = aMatchNode.parentNode.parentNode; // sentence
			def terminals = toList(sNode.getElementsByTagName('t'));
			def firstTInS = terminals[0] // get first node of the sentence
			def lastTInS = terminals[-1] // get last node of the sentence
			def firstTInSIx = allTerminals.indexOf(firstTInS) // get its position in the text
			def lastTInSIx = allTerminals.indexOf(lastTInS) // get its position in the text

			// Left context
			def lexs = []
			int start = Math.max(firstTInSIx - cx + inSntCxLengthLeft, 0);
			int end = firstTInSIx;
			if(start < end)
			for (def tNode : allTerminals.subList(start, end)){
				lexs.add(tNode.getAttribute('word'))
				if (toList(tNode.parentNode.getElementsByTagName('t'))[-1] == tNode)
					lexs.add('/')
			}
			rowDict['LeftCxOutsideSnt'] = lexs.join(" ")

			// Right context
			lexs = []

			start = lastTInSIx + 1;
			end = Math.min(allTerminals.size(), lastTInSIx + cx - inSntCxLengthRight)

			if(start < end) {
				for (def tNode : allTerminals.subList(start, end)){
					lexs.add(tNode.getAttribute('word'))
					if (toList(tNode.parentNode.getElementsByTagName('t'))[-1] == tNode)
						lexs.add('/')
				}}
			rowDict["RightCxOutsideSnt"] = lexs.join(" ")

			// Right rowDict to CSV

			//			CSVWriter.writerow(dict(zip([k for k in rowDict.iterkeys()], \
			//        [ v.encode('utf-8') for v in rowDict.itervalues() ] )))

			String line = "";
			for (int ii = 0 ; ii < header.size() ; ii++){
				String h = header.get(ii)
				String val = rowDict.getAt(h);
				if (val == null)
					line += "--"
				else
					line += val
				if (ii < header.size() -1)
					line +="\t"
			}
			writer.write(line+"\n")
			writer.flush()

		}
		writer.close();
		return true;
	}

	private def match2CSVrow(def aMatchNode, def header) {
		// Step 1: Build a LIST of DICTIONARIES to describe the variables:
		// dict(name='varname_minus_the_hash'
		//      parent='nt_nodes'
		//      terminals='t_nodes in a list')
		
		def sNode = aMatchNode.parentNode.parentNode // get the sentence node

		def tNodesInSentence = toList(sNode.getElementsByTagName('t')) // get all terminal nodes of the sentence

		def varDetails = []

		for ( def aVariable : aMatchNode.getElementsByTagName('variable')){
			if ( aVariable.getAttribute('name') == '#pivot' ||
			aVariable.getAttribute('name').startsWith('#block')) {
				def varParent = idKey(sNode, aVariable.getAttribute('idref'))

				varDetails.add([
							'name' : aVariable.getAttribute('name').substring(1),
							'parent': varParent,
							'terminals': toList(getTNodes(varParent)),
							'lexform': writeLexForm(varParent),
						])

				// Add types

				for (int j = 0 ; j < [ntTypes.size(), tTypes.size()].max() ; j++) {

					def a = ''

					if (j < ntTypes.size() && varParent.getAttribute(ntTypes[0])
					) {
						a = varParent.getAttribute(ntTypes[j])
					} else if (j < tTypes.size() && varParent.getAttribute(tTypes[0])
					) {
						a = varParent.getAttribute(tTypes[j])
					}

					varDetails[-1]["type${j+1}"] = a

				}

				// write min idx in sentence of terminal nodes used
				varDetails[-1]['terminalsIx'] = varDetails[-1]['terminals'].collect{tNodesInSentence.indexOf(it)}
			}
		}

		// Sort varDetails by the start ID of the word

		varDetails =  varDetails.sort{it['terminalsIx'].min()}

		def pivotBlockPosition = 0;
		pivotBlockPosition = varDetails.findIndexOf{it['name'] == 'pivot'}

		// Write the table
		def rowDict = [:]
		rowDict['sId'] = sNode.getAttribute('id')

		// write the pivot
		rowDict['Pivot'] = varDetails[pivotBlockPosition]['lexform']

		for (int j = 0 ; j < [ntTypes.size(), tTypes.size()].max() ; j++) {

			rowDict["PivotType${j+1}"] = varDetails[pivotBlockPosition]["type${j+1}"]
		}

		def startNextId = (varDetails[pivotBlockPosition]['terminalsIx']).min()
		def endPrevId = (varDetails[pivotBlockPosition]['terminalsIx']).max()

		// Write the Pre-pivot blocks

		def i = 0

		//println "rowDict: $rowDict"
		if ( pivotBlockPosition > 0)	{
			for ( int j = pivotBlockPosition -1 ; j >= 0 ; j--) {
				
				i++;
				def block = varDetails.get(j)
				//println "process block: $block"

				rowDict["${i}BlockBeforePivot"] = block['lexform']

				for ( int k = 0 ; k < [ntTypes.size(), tTypes.size()].max() ; k++ ) {
					rowDict["${i}BlockBeforePivotType${k+1}"] = block["type${k+1}"]
				}

				// Add any intervening words to the right edge.

				def rightEdge = (block['terminalsIx'].findAll{ it < startNextId }).max();
				//println ""+block['terminalsIx']+"   rightEdge: $rightEdge"

				for (int ix = rightEdge + 1 ; ix < startNextId ; ix++) // add word to reach the pivot
				{
					rowDict["${i}"+"BlockBeforePivot"] += ' {' + tNodesInSentence[ix].getAttribute('word') + '}';
				}

				startNextId = (block['terminalsIx']).min()
			}
		}
		
		// Write the in-sentence left context

		def lexs = tNodesInSentence.subList(0, startNextId).collect {it.getAttribute('word')}
		rowDict['LeftCxInsideSnt'] = lexs.join(" ")

		def inSntCxLengthLeft = startNextId

		// Write the Post-pivot blocks

		i = 0
		for (def block in varDetails.subList(pivotBlockPosition + 1, varDetails.size())){
			i++
			rowDict["${i}BlockAfterPivot"] = block['lexform']

			for (int j = 0 ; j < [ntTypes.size(), tTypes.size()].max() ; j++) {
				rowDict["${i}BlockAfterPivotType${j+1}"] = block["type${j+1}"]
			}

			// Add any intervening words to the left edge.
			// Note that this isn't always possible, in particular
			// if the preceding element is discontinuous.

			def leftEdgeList = block['terminalsIx'].findAll{it > endPrevId }

			if ( leftEdgeList.size() > 0) {
				def leftEdge = leftEdgeList.min()
				for (int ix = leftEdge - 1 ; ix > endPrevId ; ix--) {
					rowDict["${i}BlockAfterPivot"] = '{' + tNodesInSentence[ix].getAttribute('word') + '} ' + rowDict["${i}BlockAfterPivot"]
				}
			}
			else {
				rowDict["${i}BlockAfterPivot"] = '{?} ' + rowDict["${i}BlockAfterPivot"]
			}
			endPrevId = block['terminalsIx'].max()
		}

		// Write the in-sentence right-context

		lexs = tNodesInSentence.subList(endPrevId + 1, tNodesInSentence.size()).collect {it.getAttribute('word')}
		rowDict['RightCxInsideSnt'] = lexs.join(" ")

		def inSntCxLengthRight = tNodesInSentence.size() - endPrevId

		return [rowDict, inSntCxLengthLeft, inSntCxLengthRight]
		return null;
	}

	public def toList(def iterable) {
		return iterable.findAll {true};
	}

	/**
	 * return the children of sNode with the id anId 
	 */
	public def idKey(def sNode, String anId) {
		return sNode.getElementsByTagName("*").find{it.getAttribute("id") == anId}
	}

	/**
	 * return a list of all terminal nodes of the node.
	 * if the node is a non-terminal, iterate over children and so on
	 */
	def getTNodes(theNode) {
		def terminals = []
		def unprocessed = [theNode]

		while( unprocessed.size() > 0) {
			def aNode = unprocessed.pop()
			def edges = toList(aNode.getElementsByTagName('edge'))
			if (edges.size() == 0)
				terminals.add(aNode)
			else
				for (def anEdge : edges)
					unprocessed.add(idKey(theNode.parentNode.parentNode, anEdge.getAttribute('idref')))
		}
		return terminals;
	}

	/**
	 * 
	 * @param theNode
	 * @return the join of the terminal nodes value in theNode a varaible node
	 */
	def writeLexForm(theNode) {
		def sNode = theNode.parentNode.parentNode // get the sentence of the variable
		def allTNodes = sNode.getElementsByTagName('t') // get all sentence children
		def tNodesInTheNode = getTNodes(theNode) // get the terminal nodes pointed by idref
		def begunNode = false
		def lexs = []
		def lexBuffer = []
		for (def aTNode : allTNodes){
			if ( aTNode in tNodesInTheNode) {
				begunNode = true
				lexs.addAll(lexBuffer)
				lexs.add(aTNode.getAttribute('word'))
				lexBuffer = []
			}
			if ( begunNode && !tNodesInTheNode.contains(aTNode))
				lexBuffer.add('[' + aTNode.getAttribute('word') + ']')
		}
		return lexs.join(" ");
	}

	def matchCombine(theDOM) {

		def matchesNodes = theDOM.getElementsByTagName('matches')

		for (def aMatchesNode : matchesNodes){

			def pivotNodes = toList(
					aMatchesNode.getElementsByTagName('variable')
					).findAll{it.getAttribute('name') == '#pivot'}

			def checkedPivots = []

			while (pivotNodes){

				def aPivotNode = pivotNodes.remove(0)

				def matchingPivotList = checkedPivots.findAll{
					it.getAttribute('idref') == aPivotNode.getAttribute('idref')
				}

				if (matchingPivotList) {

					// duplicate pivot; copy all variables


					for (def node : toList(aPivotNode.getParentNode()
					.getElementsByTagName('variable')
					)) {

						matchingPivotList[0].getParentNode().appendChild(node)

					}

					def variables = toList(
							matchingPivotList[0].getParentNode()
							.getElementsByTagName('variable')
							)

					// remove duplicates

					def checkedVariables = []

					while (variables) {

						def aVariableNode = variables.remove(0)

						def matchingVariableList = checkedVariables.findAll{
							(
									it.getAttribute('idref') == aVariableNode.getAttribute('idref')
									&& (
									it.getAttribute('name') == aVariableNode.getAttribute('name')
									|| (
									it.getAttribute('name').startsWith('#block')
									&& aVariableNode.getAttribute('name').startsWith('#block')
									)
									)
									)
						}

						if (matchingVariableList) {

							// remove variable node

							aVariableNode.getParentNode().removeChild(aVariableNode)

						}

						else {

							checkedVariables.add(aVariableNode)

						}
					}
				}
				else {

					// not the same pivot

					checkedPivots.add(aPivotNode)
				}
			}
			// Tidying up: remove empty <match /> nodes
			def matchNodes = toList(aMatchesNode.getElementsByTagName('match'))

			for (def matchNode : matchNodes) {

				if ( !toList(matchNode.getElementsByTagName('variable'))) {

					aMatchesNode.removeChild(matchNode)
				}
			}
		}

		return theDOM
	}

	public static void usage() {
		println 'concordance_blocks [OPTIONS] inputfile.xml [outputfile.csv]'
		println 'OPTIONS'
		println '-h, --help           Displays this message.'
		println '-c --context [length] Sets the number of words in context.'
	}

	static main(args) {
		if(args.length == 0)
			usage()
		int cx = 30;
		def ntTypes = ['cat'];
		def tTypes = ['pos'];
		File xmlfile = new File(args[0])
		File outfile = new File(args[1])
		ConcordanceBlocks p = new ConcordanceBlocks();
		println "START"
		p.process(xmlfile, outfile, cx, ntTypes, tTypes)
		println "END"
	}
}
