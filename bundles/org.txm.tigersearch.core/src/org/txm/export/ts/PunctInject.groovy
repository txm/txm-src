// Script to restore punctuation etc. to results of TS query.
// Inputs:
// --- Tiger-XML Document node
// --- Java array:
// --- --- String [index][type] where:
// --- --- --- type == 0 gives the xml:id
// --- --- --- type == 1 gives the word form
// Process:
// --- Injects punctuation.
// Returns:
// --- Tiger-XML Document node.
package org.txm.export.ts;

import javax.xml.parsers.DocumentBuilderFactory
import org.txm.searchengine.cqp.CqpDataProxy;
import org.txm.searchengine.cqp.corpus.CQPCorpus
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.QueryResult;
import org.txm.searchengine.cqp.corpus.query.CQLQuery

public class PunctInject
{
	public process (def tigerXml, def txmIdWordTable) {

		def allTs = toList(tigerXml.getElementsByTagName('t'))

		def tAttrs = getTAttrNames(allTs[0])

		def tNodeIdPrefix = allTs[0].getAttribute('id').tokenize('#').first()

		def addedWordIds = []

		int tIx = 0

		while (txmIdWordTable) {

			def word = txmIdWordTable.remove(0)

			if (tIx == allTs.size()) {
				// End of TS file, but still words left in the BFM file.
				addTNode(word, allTs.last(), tAttrs, tNodeIdPrefix, 'append')
			}

			else {

				def tNode = allTs[tIx]

				def tId = getTNodeId(tNode)

				if (tId == word[0] && tNode.getAttribute('word') == word[1]) {

					// alles gut

					tIx += 1

				}

				else if (tId == word[0]) {

					println("Mismatched Ids! ($tId)")
					tIx += 1
				}

				else if (['#', '*'].contains(tNode.getAttribute('word')) ) {

					// SRCMF duplicata; try comparing word against the next tNode next time
					// around.

					txmIdWordTable.add(0, word)

					tIx += 1

				}
				
				// Check that the SRCMF corpus doesn't have a bug in it...

				else if ( !(word[1] =~ /[\,\.\?\!\:\;\(\)\[\]\{\}]/)
				&& (allTs[0..tIx - 1].find{
					it.getAttribute('id') == "$tNodeIdPrefix#$tId"
				})) {

					println "Warning: word ${tNode.getAttribute('word')}, id $tId appears twice in corpus!"

					txmIdWordTable.add(0, word)

					tIx += 1

				}
				
				// Check that there's not an extra word in the SRCMF corpus (rare, usu. a tokenisation change)
				
				else if ( !(word[1] =~ /[\,\.\?\!\:\;\(\)\[\]\{\}]/)
				&& (allTs[tIx..-1].find{
					it.getAttribute('id') == "$tNodeIdPrefix#${word[0]}"
				})) {
			
					println "Warning: word ${tNode.getAttribute('word')}, id $tId does not appear in BFM!"

					txmIdWordTable.add(0, word)
					
					tIx += 1
				}

				else if (addedWordIds.contains(tId)) {

					println "Warning: word ${tNode.getAttribute('word')}, id ${tId} out of sequence in SRCMF corpus!"

					txmIdWordTable.add(0, word)

					tIx += 1

				}

				else {

					// Insert word.  In the first instance, it will have the same parent as
					// the tNode before which it's being inserted.

					addTNode(word, allTs[tIx], tAttrs, tNodeIdPrefix, 'before')

					addedWordIds.add(word[0])

				}

			}

		}

		// Second phase: move punctuation into previous sentence,
		// dependent on sequence.

		def allTerminalses = toList(tigerXml.getElementsByTagName('terminals'))

		for (def i = 1 ; i < allTerminalses.size() ; i++) {

			def ts = toList(allTerminalses[i].getElementsByTagName('t'))

			def startPunc = true

			def puncStack = []

			while (ts && startPunc) {

				if ((ts[0].getAttribute('word') =~ /[A-zÀ-ÿ0-9]/).size() == 0) {

					puncStack.add(ts.remove(0))

				}

				else {

					startPunc = false

				}

			}

			// Now, treat the punctuation stack at the beginning of the sentence

			if ( puncStack ) {

				int moveLeft = 0

				// First, identify LAST instance of sentence-final punctuation.

				def puncString = puncStack.collect{ it.getAttribute('word')[0] }.join('')

				def matches = puncString =~ /[\.\,\;\:\!\?\)\]\}»”’]/

				if (matches.size() > 0) {

					moveLeft = puncString.lastIndexOf(matches[-1]) + 1

				}

				// Second, split pairs of straight quotes

				matches = puncString =~ /(""|'')/ //"

				if (matches.size() > 0) {

					moveLeft = [moveLeft, puncString.lastIndexOf(matches[-1][0]) + 1].max()
				}

				// Now, move moveLeft punctuation nodes to the end of the prev. sentence

				ts = toList(allTerminalses[i].getElementsByTagName('t'))

				for (def j = 0 ; j < moveLeft ; j++ ) {

					allTerminalses[i - 1].appendChild(ts[j])

				}

			}
		}
		return tigerXml

	}

	private addTNode(word, tNode, tAttrs, tNodeIdPrefix, where) {

		def newTNode = tNode.getOwnerDocument().createElement('t')

		for (def anAttr : tAttrs) {

			if (anAttr == 'id') {

				newTNode.setAttribute('id', "${tNodeIdPrefix}#${word[0]}")

			}

			else if (anAttr == 'word') {

				newTNode.setAttribute('word', word[1])

			}

			else {

				newTNode.setAttribute(anAttr, '--')

			}

		}

		if (where == 'before') {

			tNode.getParentNode().insertBefore(newTNode, tNode)

		}

		else if (where == 'append') {

			tNode.getParentNode().appendChild(newTNode)

		}

		else {

			throw new IllegalArgumentException('Bad before value')

		}

	}

	public getTAttrNames(tNode) {

		def nodeMap = tNode.attributes

		def nameList = []

		for ( def i = 0 ; i < nodeMap.getLength() ; i++ ) {
			nameList.add( nodeMap.item(i).nodeName )
		}

		return nameList

	}

	public getTNodeId(tNode) {

		return tNode.getAttribute('id').tokenize('#').last()

	}

	public def toList(def iterable) {
		return iterable.findAll {true};
	}

	public static def getWords(String corpusname, String query)
	{
		CorpusManager cm = CorpusManager.getCorpusManager();
		CQPCorpus corpus = cm.getCorpus(corpusname);
		def word_property = corpus.getProperty("word")
		def id_property = corpus.getProperty("id")

		def wordCache = cm.getCorpusProxies(corpus).get(word_property);
		def idCache = cm.getCorpusProxies(corpus).get(id_property);

		def positions = new int[corpus.getSize()];
		for(int i = 0 ; i< corpus.getSize() ; i++)
			positions[i] = i;
		def word_values = wordCache.getData(positions)
		def id_values = idCache.getData(positions)
		ArrayList<String[]> words = new ArrayList<String[]>(corpus.getSize());
		for(int p : positions)
		{
			if(id_values[p].startsWith("w"))
			{
				words.add(new String[2])
				words[p][0] = id_values[p]
				words[p][1] = word_values[p]
			}
		}

		return words;
	}

	public static void main(String[] args)
	{
		def words = [
			["w203_1", "Dominedeu"],
			["w203_2", "devemps"],
			["w203_3", "lauder"],
			["w203_4", "et"],
			["w203_5", "a"],
			["w203_6", "sus"],
			["w203_7", "sancz"],
			["w203_8", "honor"],
			["w203_9", "porter"],
			["w203_10", "»"],
			["w203_10.2", ")"],
			["w203_10.3", '.'],
			["w203_10.5", '"'],
			["w203_10.7", '"'],
			["w203_11", "in"],
			["w203_12", "su'"],
			["w203_13", "amor"],
			["w203_14", "cantomps"],
			["w203_15", "del"],
			["w203_16", "·sanz"],
			["w203_17", "quae"],
			["w203_18", "por"],
			["w203_19", "lui"],
			["w203_20", "augrent"],
			["w203_21", "granz"],
			["w203_22", "aanz"],
			["w203_23", "."],
		];
		File tigerXml = new File(args[0]);
		def factory = DocumentBuilderFactory.newInstance()
		factory.setXIncludeAware(true)
		def builder = factory.newDocumentBuilder()
		def THEDOM = builder.parse(tigerXml).documentElement

		println THEDOM.getClass()

		def NEWDOM = new PunctInject().process(THEDOM, words);

		println NEWDOM

		// File outfile = new File("outfile.xml")
		// println outfile
		//outfile.withWriter("iso-8859-1"){writer ->
		//writer.write(NEWDOM.toString())
		// }
	}
}