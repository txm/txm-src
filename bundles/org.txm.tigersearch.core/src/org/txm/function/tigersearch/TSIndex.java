package org.txm.function.tigersearch;

import ims.tiger.corpus.NT_Node;
import ims.tiger.corpus.Node;
import ims.tiger.corpus.Sentence;
import ims.tiger.corpus.T_Node;
import ims.tiger.gui.tigergraphviewer.forest.ResultForest;
import ims.tiger.query.api.MatchResult;
import ims.tiger.query.internalapi.InternalCorpusQueryManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Line;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.searchengine.ts.TSCorpus;
import org.txm.searchengine.ts.TSCorpusManager;
import org.txm.searchengine.ts.TSResult;
import org.txm.utils.logger.Log;

public class TSIndex extends Index {

	public static final String EDITIONID = "editionId"; //$NON-NLS-1$

	public TSIndex(CQPCorpus corpus, List<WordProperty> props)
			throws CqiClientException, IOException, CqiServerError {
		super(corpus);
		this.setParameters(new CQLQuery("[]"), props, null, null, null, null); //$NON-NLS-1$
	}

	public TSIndex(CQPCorpus corpus, CQLQuery query, List<WordProperty> props)
			throws CqiClientException, IOException, CqiServerError {
		super(corpus);
		this.setParameters(query, props, null, null, null, null);
	}

	// public TSIndex(Partition partition, CQLQuery query, List<WordProperty> props)
	// throws CqiClientException, IOException, CqiServerError {
	// super(partition);
	// this.setParameters(query, props, null, null, null, null);
	// }


	boolean isTigerInitialized = false;

	private TSCorpus tscorpus;

	private TSResult tsresult;

	private boolean initTIGERSearch() {
		if (isTigerInitialized) return isTigerInitialized;

		String id = getCorpus().getMainCorpus().getName();
		File configfile = new File(getCorpus().getProjectDirectory(), "tiger/tigersearch.logprop"); //$NON-NLS-1$
		File registrydir = new File(getCorpus().getProjectDirectory(), "tiger"); //$NON-NLS-1$

		TSCorpusManager manager = new TSCorpusManager(registrydir, configfile);
		if (manager.isInitialized()) {
			tscorpus = manager.getCorpus(id);
			if (tscorpus == null) {
				System.out.println("TIGERSearch corpus not found in " + registrydir); //$NON-NLS-1$
				isTigerInitialized = false;
			}

			isTigerInitialized = true;
		}
		return isTigerInitialized;
	}

	@Override
	/**
	 * count tokens.
	 *
	 * @param corp the corp
	 * @return true, if successful
	 * @throws CqiClientException
	 * @throws CqiServerError
	 * @throws IOException
	 */
	protected boolean scanCorpus(CQPCorpus corp) throws CqiClientException, IOException, CqiServerError {

		if (!initTIGERSearch()) return false;

		// get the cqp result of the query
		// long time = System.currentTimeMillis();
		// QueryResult result = corp.query(getQuery(), "index", true); //$NON-NLS-1$
		boolean isTargetUsed = false;// CQPSearchEngine.getCqiClient().subCorpusHasField(result.getQualifiedCqpId(), ICqiClient.CQI_CONST_FIELD_TARGET);

		// System.out.println("nLines : "+nLines);
		List<Match> matches;
		try {
			matches = getMatchesFromTSQuery(corp);
		}
		catch (Exception e1) {
			Log.severe(Messages.bind(Messages.ErrorWhileQueingTIGERSearchP0, e1));
			Log.printStackTrace(e1);
			return false;
		}
		int nbresults = matches.size();
		if (nbresults == 0) {
			return false;
		}
		this.nTotalTokens += nbresults; // get number of tokens

		// count matches
		// time = System.currentTimeMillis();
		List<Integer> allpositions = new ArrayList<>();
		for (int j = 0; j < nbresults; j++) {
			Match match = matches.get(j);
			// beginingOfKeywordsPositions.add(match.getStart()); // get the
			// first index
			// lengthOfKeywords.add(match.size());// get the last index
			if (isTargetUsed) {
				allpositions.add(match.getTarget());
			}
			else {
				for (int i = match.getStart(); i <= match.getEnd(); i++)
					allpositions.add(i);
			}
		}

		int[] allpositionsarray = new int[allpositions.size()];
		int pcount = 0;
		for (int p : allpositions)
			allpositionsarray[pcount++] = p;

		// time = System.currentTimeMillis();
		HashMap<Property, int[]> propsId = new HashMap<>();
		for (Property property : pProperties) {
			try {
				if (property instanceof StructuralUnitProperty) {
					int[] structs = CorpusManager.getCorpusManager()
							.getCqiClient().cpos2Struc(
									property.getQualifiedName(),
									allpositionsarray);
					propsId.put(property, structs);
				}
				else {
					int[] indices = CorpusManager.getCorpusManager()
							.getCqiClient().cpos2Id(
									property.getQualifiedName(),
									allpositionsarray);
					propsId.put(property, indices);
				}
			}
			catch (Exception e) {
				Log.severe(Messages.bind(Messages.ErrorWhilePropertiesProjectionP0, e));
				Log.printStackTrace(e);
				return false;
			}
		}
		// System.out.println("Time recup indices "+(System.currentTimeMillis()-time));
		int currentIndex = 0;
		// time = System.currentTimeMillis();
		for (int i = 0; i < nbresults; i++) {

			Line line = new Line();
			Match match = matches.get(i);
			int size = match.size();
			if (isTargetUsed) size = 1;
			for (int p = 0; p < pProperties.size(); p++) {

				Property property = pProperties.get(p);
				int[] allprosids = propsId.get(property);
				int[] ids = new int[size];
				System.arraycopy(allprosids, currentIndex, ids, 0, size);
				line.putIds(property, ids);
			}
			currentIndex += size;

			Object signature = line.getSignature();

			if (counts.containsKey(signature)) { // if the counts contains the
				// signature, increment its
				// corresponding value
				while (counts.get(signature).size() <= currentpartid) {
					counts.get(signature).add(0);
				}

				int c = counts.get(signature).get(currentpartid) + 1;
				counts.get(signature).set(currentpartid, c);
			}
			else // else initialize count of the signature to 1
			{
				// System.out.println("add new sign "+signature+" of line "+line.toString());
				ArrayList<Integer> tmp = new ArrayList<>();
				for (int j = 0; j < currentpartid + 1; j++)
					tmp.add(0);
				counts.put(signature, tmp);
				counts.get(signature).set(currentpartid, 1);

				lines.add(line);
			}
		}
		// System.out.println("Time count lines "+(System.currentTimeMillis()-time));
		// System.out.println("took "+(System.currentTimeMillis()-time));
		return true;

	}

	private List<Match> getMatchesFromTSQuery(CQPCorpus corp) throws Exception {
		ArrayList<Match> matches = new ArrayList<>();
		tsresult = tscorpus.query(pQueries.get(0).toString());
		boolean debug = false;
		ResultForest forest = tsresult.getForest();
		MatchResult result = tsresult.getMatchResult();

		int var_ok = 0;
		String label = "@|pivot"; //$NON-NLS-1$
		Pattern p = Pattern.compile(label);
		for (String var : result.getVariableNames()) {
			if (p.matcher(var).matches()) var_ok++;
		}
		if (var_ok == 0) {
			System.out.println(Messages.ErrorInTSIndexNoLabelEtc);
			return matches;
		}
		else if (var_ok > 1) {
			System.out.println(Messages.ErrorInTSIndexTooManyLabelsEtc);
			return matches;
		}

		InternalCorpusQueryManager manager = tsresult.getManager();
		// Display_Sentence match = null;
		//
		// if (forest.isNextMatch()) {
		// match = forest.nextMatch();
		// } else {
		// match = null;
		// }

		HashSet<String> matchingTnodesID = new HashSet<>();

		int size = forest.getForestSize();
		for (int match_no = 0; match_no < size; match_no++) {

			if (debug) System.out.println("Match " + match_no); //$NON-NLS-1$
			int sentno = result.getSentenceNumberAt(match_no);
			Sentence sentence = manager.getSentence(sentno);

			result.orderSentenceSubmatches(sentno);
			int n = result.getSentenceSubmatchSize(sentno);

			for (int iSubMatch = 0; iSubMatch < n; iSubMatch++) {

				if (debug) System.out.println(" Sub " + iSubMatch); //$NON-NLS-1$
				int[] var_values = result.getSentenceSubmatchAt(sentno, iSubMatch);
				int value;
				for (int j = 0; j < var_values.length; j++) {
					if (debug) System.out.println("  Var " + j + " name=" + result.getVariableName(j)); //$NON-NLS-1$ //$NON-NLS-2$
					value = var_values[j];
					if (value >= 0) {
						String var_name = result.getVariableName(j);
						Node referred = sentence.getNode(value);
						if (p.matcher(var_name).matches()) {
							if (referred instanceof T_Node) {
								if (debug) System.out.println("    Node: " + referred.getFeature(EDITIONID)); //$NON-NLS-1$
								matchingTnodesID.add(referred.getFeature(EDITIONID));
							}
							else if (referred instanceof NT_Node) {
								for (T_Node c : getTerminals((NT_Node) referred, sentence)) {
									matchingTnodesID.add(c.getFeature(EDITIONID));
								}
							}
						}
					}
				}
			}
		}


		// while (match != null) {
		// if (debug) System.out.println("Match: "+forest.getCurrentMatchNumber()+" Nsub="+forest.getSubMatchesSize());
		// Display_Sentence sub = null;
		// int[] var_values = match.getSentenceSubmatchAt(sentno,i);
		//
		// sub = match;
		// int nlabel;
		// while (sub != null) {
		// if (debug) System.out.println(" Sub: "+forest.getCurrentSubMatchNumber());
		// Node matchingNode = sub.getMatchSubgraphNode();
		// //if (debug) System.out.println(matchingNode.getIncomingEdgeLabel());
		// if (matchingNode instanceof T_Node) {
		// T_Node node = (T_Node)matchingNode;
		// if (debug) System.out.println(" T node: "+node.getFeature(EDITIONID)+" "+node.getFeature("word"));
		// matchingTnodesID.add(node.getFeature(EDITIONID));
		// } else if (matchingNode instanceof NT_Node) {
		// NT_Node ntnode = (NT_Node)matchingNode;
		// if (debug) System.out.println(" NT node: "+ntnode.getChilds());
		// for (T_Node node : getTerminals(ntnode, sub)) {
		// if (debug) System.out.println(" T node: "+node.getFeature(EDITIONID)+" "+node.getFeature("word"));
		// matchingTnodesID.add(node.getFeature(EDITIONID));
		// }
		// }
		//
		// if (forest.isNextSubMatch()) {
		// sub = forest.nextSubMatch();
		// } else {
		// sub = null;
		// }
		// }
		//
		// if (forest.isNextMatch()) {
		// match = forest.nextMatch();
		// } else {
		// match = null;
		// }
		// }

		HashSet<String> matchingTnodesIDFiltered = new HashSet<>();
		for (String s : matchingTnodesID) {
			if (s == null || s.length() == 0 || s.endsWith("_dupl")) { //$NON-NLS-1$

			}
			else {
				matchingTnodesIDFiltered.add(s);
			}
		}

		if (debug) System.out.println("Matching ids: " + matchingTnodesIDFiltered); //$NON-NLS-1$
		String[] strings = new String[matchingTnodesIDFiltered.size()];
		int i = 0;
		for (String s : matchingTnodesIDFiltered)
			strings[i++] = s;
		matchingTnodesIDFiltered = null;

		matches = buildMatches_CQI(strings);
		strings = null;

		// one word per match ! the word is inside one of the CQP (sub-)Corpus matches
		List<Match> finalMatches = new ArrayList<>();
		List<? extends org.txm.objects.Match> corpusMatches = corp.getMatches();
		int iCorpusStart = 0;
		for (int iMatch = 0; iMatch < matches.size(); iMatch++) {
			Match m1 = matches.get(iMatch);
			for (int iCorpus = iCorpusStart; iCorpus < corpusMatches.size(); iCorpus++) {
				org.txm.objects.Match m2 = corpusMatches.get(iCorpus);
				if (m2.getStart() <= m1.getStart() && m1.getEnd() <= m2.getEnd()) {
					finalMatches.add(m1);
					iCorpusStart = iCorpus; // optimizing ! \o/
					break;
				}
			}
		}
		matches = null;
		corpusMatches = null;
		return finalMatches;

	}

	private ArrayList<Match> buildMatches_CQI(String[] strings) throws CqiClientException, IOException, CqiServerError {
		boolean debug = false;
		ArrayList<Match> matches = new ArrayList<>();
		AbstractCqiClient CQI = CQPSearchEngine.getCqiClient();
		Property idProperty = getCorpus().getProperty("id"); //$NON-NLS-1$

		if (debug) System.out.println("Call CQI.str2Id with strings.len=" + strings.length); //$NON-NLS-1$
		if (debug) System.out.println(Arrays.toString(strings));
		if (debug) System.out.flush();

		int[] ids = CQI.str2Id(idProperty.getQualifiedName(), strings);

		if (debug) System.out.println("Call CQI.idList2Cpos with strings.len=" + ids.length); //$NON-NLS-1$
		if (debug) System.out.println(Arrays.toString(ids));
		if (debug) System.out.flush();

		HashSet<Integer> positionsSet = new HashSet<>();
		for (int id : ids) {
			int[] positions = CQI.id2Cpos(idProperty.getQualifiedName(), id);
			for (int p : positions)
				positionsSet.add(p);
		}

		int[] positions = new int[positionsSet.size()];
		int i = 0;
		for (int p : positionsSet)
			positions[i++] = p;
		positionsSet = null;
		Arrays.sort(positions);
		if (debug) System.out.println("Positions: " + Arrays.toString(positions)); //$NON-NLS-1$

		for (int p : positions)
			matches.add(new Match(p, p, -1));

		// build matches and regroups matches
		// int previous = -999;
		// int start = -1, end = -1, target = -1;
		// for (int p : positions) {
		// //System.out.println("P="+p+" start="+start+" end="+end+" target="+target+" Prev="+previous);
		// if (p - previous > 1) {
		// if (start >= 0) {
		// matches.add(new Match(start, end, target));
		// }
		// start = p;
		// }
		// end = p;
		// previous = p;
		// }
		// System.out.println(matches);
		return matches;
	}


	protected static ArrayList<T_Node> EMPTYTERMINALS = new ArrayList<>();

	public static ArrayList<T_Node> getTerminals(NT_Node ntnode, Sentence sub) {
		if (ntnode.getChilds().size() == 0) return EMPTYTERMINALS;

		ArrayList<T_Node> terminals = new ArrayList<>();
		for (Object o : ntnode.getChilds()) {
			int n = (Integer) o;
			Node node = sub.getNode(n);
			if (node instanceof T_Node) {
				terminals.add((T_Node) node);
			}
			else if (node instanceof NT_Node) {
				terminals.addAll(getTerminals((NT_Node) node, sub));
			}
		}
		return terminals;
	}
}
