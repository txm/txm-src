package org.txm.function.tigersearch;

import org.txm.searchengine.core.Query;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.ts.TIGERQuery;
import org.txm.treesearch.function.TreeSearch;
import org.txm.treesearch.function.TreeSearchSelector;

public class TIGERTreeSearchSelector extends TreeSearchSelector {

	public static String TIGER = "TIGER"; //$NON-NLS-1$

	@Override
	public String getEngine() {

		return TIGER;
	}

	@Override
	public boolean canComputeWith(CQPCorpus corpus) {

		return SearchEnginesManager.getTIGERSearchEngine().hasIndexes(corpus);
	}

	@Override
	public TreeSearch getTreeSearch(CQPCorpus corpus) {

		return new TIGERSearch(corpus);
	}

	@Override
	public Class<? extends Query> getQueryClass() {

		return TIGERQuery.class;
	}

	@Override
	public TreeSearch getTreeSearch(String resultnodepath, CQPCorpus corpus) {

		return new TIGERSearch(resultnodepath, corpus);
	}
}
