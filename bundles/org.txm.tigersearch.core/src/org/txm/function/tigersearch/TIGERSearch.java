package org.txm.function.tigersearch;

import java.io.File;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.txm.searchengine.core.SearchEnginesManager;
import org.txm.searchengine.core.Selection;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.ts.TIGERQuery;
import org.txm.searchengine.ts.TIGERSearchEngine;
import org.txm.searchengine.ts.TSCorpus;
import org.txm.searchengine.ts.TSCorpusManager;
import org.txm.searchengine.ts.TSMatch;
import org.txm.searchengine.ts.TSResult;
import org.txm.treesearch.function.TreeSearch;
import org.txm.treesearch.function.TreeSearchSelector;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.logger.Log;

import ims.tiger.index.reader.Index;
import ims.tiger.index.reader.IndexException;

public class TIGERSearch extends TreeSearch {

	protected TSCorpusManager manager;

	protected TSCorpus tscorpus;

	protected boolean ready = false;

	protected TSResult tsresult;

	private Selection tigerSelection;

	//protected String T, NT;

	public TIGERSearch(CQPCorpus corpus) {
		this(null, corpus);
	}

	public TIGERSearch(String parentNodePath, CQPCorpus corpus) {
		super(parentNodePath, corpus);

		this.corpus = getParent();
	}

	public TIGERSearch(String parentNodePath) {
		this(parentNodePath, null);
	}

	public TSResult getTSResult() {
		return tsresult;
	}

	public boolean isComputed() {

		return tsresult != null;
	}

	public boolean canBeDrawn() {

		return tsresult != null && T != null && NT != null;
	}

	public boolean toSVG(File svgFile, int sent, int sub, List<String> T, List<String> NT) {

		if (tsresult == null) return false; // no result

		this.T = T;
		this.NT = NT;

		if (tsresult.getNumberOfMatch() == 0) return false;

		if (NT.size() > 0) {
			tsresult.setDisplayProperties(T, NT.get(0));
		}
		else {
			tsresult.setDisplayProperties(T, null);
		}

		try {
			// iterate to the sub th subgraph
			TSMatch match = tsresult.getMatch(sent);
			match.setSubGraph(sub);
			svgFile.delete();
			match.toSVGFile(svgFile);
			if (!svgFile.exists()) {
				Log.severe(Messages.bind(Messages.FailToRenderSVGMatchForSentEqualsP0AndSubEqualsP1, sent, sub));
				return false;
			}
		}
		catch (Exception e) {
			Log.printStackTrace(e);
		}
		return true;
	}

	public String[] getAvailableTProperties() {

		if (tscorpus == null) return new String[0];
		List<String> tFeatures = tscorpus.getTFeatures();
		return tFeatures.toArray(new String[tFeatures.size()]);
	}

	public String[] getAvailableNTProperties() {

		if (tscorpus == null) return new String[0];
		List<String> ntFeatures = tscorpus.getNTFeatures();
		return ntFeatures.toArray(new String[ntFeatures.size()]);
	}

	@Override
	public CQPCorpus getParent() {
		return (CQPCorpus) super.getParent();
	}

	public boolean isReady() {
		return SearchEnginesManager.getTIGERSearchEngine().hasIndexes(this.getParent().getMainCorpus());
	}

	public int getNMatchingSentences() {
		if (tsresult == null) return 0;
		return tsresult.getNumberOfMatch();
	}

	public int getNSubMatch() {
		if (tsresult == null) return 0;
		if (tsresult.getMatch(pIndex) == null) return 0;

		return tsresult.getMatch(pIndex).getNumberOfSubGraph();
	}

	//	// public int getSent() {
	//	// if (tsresult == null) return 0;
	//	// return tsresult.getCurrentMatchNo();
	//	// }
	//	
	//	public int getSub(int matchNo) {
	//		if (tsresult == null) return 0;
	//		if (tsresult.getMatch(matchNo) == null) return 0;
	//		
	//		return tsresult.getMatch(matchNo).getCurrentSubMatchNo();
	//	}

	/**
	 * @return the query or the generic one
	 */
	public String getQuery() {
		if (pQuery == null || pQuery.getQueryString().length() == 0) {
			return "root(#n1)"; //$NON-NLS-1$
		}
		else {
			return pQuery.getQueryString();
		}
	}

	@Override
	public String toString() {
		if (pQuery != null)
			return pQuery.getQueryString().substring(0, Math.min(20, pQuery.getQueryString().length())).replaceAll("\n", " ") + " T: " + T + " NT: " + NT; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

		return "" + corpus; //$NON-NLS-1$
	}

	@Override
	public boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return tsresult.toXml(outfile);
	}

	@Override
	public void clean() {

	}

	/**
	 * 
	 * @return the array of extensions to show in the FileDialog SWT widget
	 */
	@Override
	public String[] getExportTXTExtensions() {
		return new String[] { "*.xml" }; //$NON-NLS-1$
	}

	@Override
	public boolean canCompute() {
		return corpus != null; // && pQuery != null && pQuery.length() > 0; // if pQuery is empty/null set a default query
	}

	@Override
	public boolean _loadParameters() throws Exception {

		return true;
	}

	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		//if (hasParameterChanged(TreeSearchPreferences.QUERY)) {

		String id = corpus.getMainCorpus().getID();

		File tsRegistryDirectory = new File(corpus.getProjectDirectory(), "tiger"); //$NON-NLS-1$
		File configfile = new File(tsRegistryDirectory, "tigersearch.logprop"); //$NON-NLS-1$
		File tscorpusdir = new File(tsRegistryDirectory, id);

		if (!tscorpusdir.exists()) {
			Log.severe(Messages.bind(Messages.ErrorCantFindCorpusDirectoryP0, tscorpusdir));
			return false;
		}
		if (manager == null) {
			manager = new TSCorpusManager(tsRegistryDirectory, configfile);
			tscorpus = manager.getCorpus(id);
		}

		if (!manager.isInitialized()) {
			return false;
		}

		if (tscorpus == null) {
			Log.severe(Messages.bind(Messages.TIGERSearchCorpusNotFoundInP0, tsRegistryDirectory));
			return false;

		}


		tsresult = null;
		tigerSelection = null;

		if (pQuery == null || pQuery.getQueryString().length() == 0) {
			pQuery = new TIGERQuery("root(#n1)"); //$NON-NLS-1$
		}

		// tsresult = tscorpus.query(pQuery.getQueryString());
		tsresult = TIGERSearchEngine.getInstance().queryTIGER(this.getCorpus(), pQuery);
		tsresult.getFirst();

		pIndex = 0;
		pSubIndex = 0;
		//		} else {
		//			
		//		}

		return true;
	}

	@Override
	public String getResultType() {
		return "TIGER search"; //$NON-NLS-1$
	}

	public void setIndexAndSubIndex(int sent, int sub) {
		this.pIndex = sent;
		this.pSubIndex = sub;
	}

	@Override
	public boolean hasSubMatchesStrategy() {

		return true;
	}

	@Override
	public String getEngine() {

		return TIGERTreeSearchSelector.TIGER;
	}

	@Override
	public TreeSearchSelector getSelector() {

		return new TIGERTreeSearchSelector();
	}

	@Override
	public String[] getTextAndWordIDSOfCurrentSentence() throws Exception {

		String[] textAndWord = new String[2];

		//TIGERSearchEngine engine = (TIGERSearchEngine) SearchEnginesManager.getTIGERSearchEngine();

		Index index = this.tscorpus.getIndex();

		MappedByteBuffer offsetsMapped = tscorpus.getOffsetsMapped();

		int[] starts = tscorpus.getSentenceStartPositions();

		//TSMatch s = tsresult.getMatch(this.index);
		int sent = tsresult.getMatchResult().getSentenceNumberAt(this.pIndex);

		int sent_start = starts[sent];

		int left = sent_start + index.getLeftCorner(sent, 0);
		if (offsetsMapped != null) { // the TIGER token is not in the CQP corpus
			left += offsetsMapped.getInt(left * Integer.BYTES);
			// System.out.println("left="+left+" offset="+offsetsMapped.getInt(left*Integer.BYTES));
		}
		int l = index.getSentence(sent).getTerminalsSize();
		int[] positions = new int[l];
		for (int i = 0; i < l; i++) {
			positions[i] = left + i;
		}
		//		int right = sent_start + index.getRightCorner(sent, index.getSentence(sent).getTerminalsSize());
		//		if (offsetsMapped != null) { // the TIGER token is not in the CQP corpus
		//			right += offsetsMapped.getInt(right * Integer.BYTES);
		//		}
		WordProperty pid = corpus.getProperty("id"); //$NON-NLS-1$
		StructuralUnitProperty ptextid = corpus.getStructuralUnitProperty("text_id"); //$NON-NLS-1$

		List<String> idvalues = CQPSearchEngine.getEngine().getValuesForProperty(positions, pid);
		int[] structid = CQPSearchEngine.getCqiClient().cpos2Struc(ptextid.getQualifiedName(), positions);
		String[] values = CQPSearchEngine.getCqiClient().struc2Str(ptextid.getQualifiedName(), structid);

		textAndWord[0] = values[0];
		textAndWord[1] = StringUtils.join(idvalues, ","); //$NON-NLS-1$

		return textAndWord;
	}

	@Override
	public String getIconPath() {

		return "platform:/plugin/org.txm.tigersearch.rcp/icons/functions/TS.png"; //$NON-NLS-1$
	}

	@Override
	public Selection getSelection() {

		if (tigerSelection == null) {
			try {
				tigerSelection = TIGERSearchEngine.fromTIGERMatchToSelection(this.tscorpus, getTSResult(), pQuery, corpus);
			}
			catch (IndexException e) {
				e.printStackTrace();
				return null;
			}
		}
		return tigerSelection;
	}

	@Override
	public File createTemporaryFile(File resultDir) {

		try {
			return File.createTempFile("TIGERSearch", ".svg", resultDir); //$NON-NLS-1$ //$NON-NLS-2$
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} //$NON-NLS-1$ //$NON-NLS-2$ull;
	}

	@Override
	public String[] getAvailableVisualisations() {

		return null; // only one visualisation
	}

	@Override
	public int getTotalSubMatch() {
		return tsresult.getForest().getSubMatchesSum();
	}
}
