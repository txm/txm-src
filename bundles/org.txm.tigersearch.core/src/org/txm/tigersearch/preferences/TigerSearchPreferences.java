package org.txm.tigersearch.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 *
 */
public class TigerSearchPreferences extends TXMPreferences {


	public static final String DRIVER_FILENAME = "driver_filename"; //$NON-NLS-1$

	public static final String HEADER_FILENAME = "header_filename"; //$NON-NLS-1$

	public static final String FEATURE_VALUES_TO_IGNORE_IN_HEADER = "feature_values_to_ignore"; //$NON-NLS-1$

	public static String VERSION = "version"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(TigerSearchPreferences.class)) {
			new TigerSearchPreferences();
		}
		return TXMPreferences.instances.get(TigerSearchPreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.put(DRIVER_FILENAME, "corpus.xml"); //$NON-NLS-1$
		preferences.put(HEADER_FILENAME, "header.xml"); //$NON-NLS-1$
		preferences.put(FEATURE_VALUES_TO_IGNORE_IN_HEADER, ""); //$NON-NLS-1$
	}
}


