package org.txm.tigersearch.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;
import org.txm.treesearch.preferences.TreeSearchPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author mdecorde
 *
 */
public class TigerSearchTreePreferences extends TreeSearchPreferences {

	public static String USESUBMATCHES = "use_sub_matches"; //$NON-NLS-1$

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(TigerSearchTreePreferences.class)) {
			new TigerSearchTreePreferences();
		}
		return TXMPreferences.instances.get(TigerSearchTreePreferences.class);
	}

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();

		Preferences preferences = this.getDefaultPreferencesNode();

		preferences.putBoolean(USESUBMATCHES, true);
		preferences.put(TFEATURE, "form"); //$NON-NLS-1$
		preferences.put(NTFEATURE, "cat"); //$NON-NLS-1$
	}
}


