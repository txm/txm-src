package org.txm.chartsengine.r.core;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends NLS {

	public static String ErrorNoChartToExport;

	public static String ErrorRChartWasNullInP0;

	public static String RChartEngineExportFormatP0CannotDiffferFromTheCurrent;

	public static String RChartEngineOnlyManageFileChartsP0;

	static {
		Utf8NLS.initializeMessages(Messages.class);
	}

	private Messages() {
	}
}
