package org.txm.chartsengine.r.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.r.core.RChartsEngine;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author sjacquot
 *
 */
public class RChartsEnginePreferences extends TXMPreferences {


	public final static String PREFERENCES_PREFIX = RChartsEngine.NAME + "_"; //$NON-NLS-1$


	public static final String OUTPUT_FORMAT = PREFERENCES_PREFIX + "output_format"; //$NON-NLS-1$



	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(RChartsEnginePreferences.class)) {
			new RChartsEnginePreferences();
		}
		return TXMPreferences.instances.get(RChartsEnginePreferences.class);
	}


	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences defaultPreferences = this.getDefaultPreferencesNode();
		defaultPreferences.put(OUTPUT_FORMAT, ChartsEngine.OUTPUT_FORMAT_SVG);
	}

}
