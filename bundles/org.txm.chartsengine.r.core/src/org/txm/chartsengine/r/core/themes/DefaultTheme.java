package org.txm.chartsengine.r.core.themes;



//FIXME: SJ: this class must be moved to org.txm.chartsengine.core level and used in all charts engine implementations
// Done but we may need to also move the styles, width, etc.
@Deprecated
public class DefaultTheme {

	@Deprecated
	public static String black = "black"; //$NON-NLS-1$

	/** The colors. */
	@Deprecated
	public static String[] colors = { "red", "blue", "green3", "gold", "deeppink1", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"firebrick", "navyblue", "green4", "darkgoldenrod1", "magenta1", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"deepskyblue", "green", "yellow", "cyan", "greenyellow", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"seagreen1", black, "grey" }; //$NON-NLS-1$ //$NON-NLS-2$ 
	//int[] colors16 = {552, 26, 257, 142, 117,133 ,491, 258, 76, 451, 121, 254, 652, 68 ,259, 575};

	/** The monocolors. */
	@Deprecated
	public static String[] monocolors = { black, black, black, black, black,
			black, black, black, black, black,
			black, black, black, black, black, black };
	//int[] monocolors = {24, 24, 24, 24, 24, 24 ,24, 24, 24, 24, 24, 24, 24, 24 ,24, 24};

	// jeu de 16 couleurs :

	// jeu de 10 couleurs :
	@Deprecated
	public static String[] colors10 = { "red", "blue", "green4", "gold", "deeppink1", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"firebrick", "deepskyblue", "greenyellow", "darkgoldenrod1", "magenta1" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

	// jeu de 5 couleurs :
	@Deprecated
	public static String[] colors5 = { "red", "blue", "green4", "gold", "deeppink1" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

	/** The styles. */
	public static int[] styles = {
			1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4 };

	/** The monostyles. */
	public static int[] monostyles = {
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };


	/** The widths. */
	public static int[] widths = {
			1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3 };

	/** The monowidths. */
	public static int[] monoWidths = {
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

	public DefaultTheme() {
		// TODO Auto-generated constructor stub
	}

}
