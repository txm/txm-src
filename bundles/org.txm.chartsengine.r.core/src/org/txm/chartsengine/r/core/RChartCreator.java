package org.txm.chartsengine.r.core;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;

import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.ChartCreator;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.utils.logger.Log;



/**
 * R base chart creator.
 * 
 * Currently the R chart creator can't update a chart = a File. It is always recreated in the updateChart method
 * 
 * An empty file is created in the createChart method
 * 
 * @author sjacquot
 *
 */
public abstract class RChartCreator<R extends ChartResult> extends ChartCreator<RChartsEngine, R> {

	protected String symbol = null;

	/**
	 * creates an empty file. the chart will be written after in the updateChart method
	 */
	@Override
	public Object createChart(R result) {
		if (result.getChart() != null && result.getChart() instanceof File) { // we can re-use the file if already defined
			return result.getChart();
		}
		else {
			return this.getChartsEngine().createChartFile(result, this.getFileNamePrefix());
		}
	}

	/**
	 * Effectively writes the R chart in the given file
	 * 
	 * @param result
	 * @param file the file to write. not null
	 * @return
	 */
	public abstract File createChart(R result, File file);

	@Override
	public void updateChart(R result) {

		Object chart = result.getChart();

		if (chart instanceof File) { // The RChartCreators can't really update a chart -> the chart is always recreated
			createChart(result, (File) chart);
		}
		else {
			Log.warning(NLS.bind(Messages.ErrorRChartWasNullInP0, result));
		}
	}

	@Override
	public ArrayList<Color> getSeriesShapesColors(Object chart) {
		Log.severe(this.getClass() + ".getSeriesShapesColors(): Not yet implemented."); //$NON-NLS-1$
		return null;
	}

	@Override
	public Class<RChartsEngine> getChartsEngineClass() {
		return RChartsEngine.class;
	}

	public String getSymbol() {
		return symbol;
	}


	// @Override
	// public Object createChart(ChartResult result) {
	// //return this.createChartFile(result);
	// // FIXME: SJ: tests. No need to return the chart now? it will be done in updateChart()
	// return null;
	// }
	//
	//
	// @Override
	// public void updateChart(ChartResult result) {
	//
	// Object chart = result.getChart();
	//
	// // creates a new chart but using the same file
	// // FIXME: SJ to MD: this case should never happen, if it happens then a bug must fixed at another location
	// // TODO: find the bug then just keep: this.createChartFile(result, (File)chart);
	// if (chart instanceof File) {
	// this.createChartFile(result, (File)chart);
	// }
	// // creates a new chart using a new file
	// else {
	// result.setChart(this.createChartFile(result));
	// }
	// }
}
