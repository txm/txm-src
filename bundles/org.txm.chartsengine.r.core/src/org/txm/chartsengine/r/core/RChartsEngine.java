package org.txm.chartsengine.r.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.r.core.preferences.RChartsEnginePreferences;
import org.txm.stat.engine.r.RDevice;
import org.txm.stat.engine.r.RWorkspaceRenderer;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.utils.io.FileCopy;
import org.txm.utils.logger.Log;


/**
 * Charts engine providing methods to create charts using R.
 * 
 * @author sjacquot
 *
 */
public class RChartsEngine extends ChartsEngine {



	/**
	 * The charts engine internal name.
	 */
	public final static String NAME = "r_charts_engine"; //$NON-NLS-1$

	/**
	 * The charts engine description.
	 */
	public final static String DESCRIPTION = "R"; //$NON-NLS-1$


	/**
	 * Constants for extra output formats.
	 */
	// FIXME: SJ: for tests or further output formats implementations
	// public final static String OUTPUT_FORMAT_RGRAPHICS = "rgraphics", OUTPUT_FORMAT_IPLOT = "iplots", OUTPUT_FORMAT_JAVAGD = "javagd"; //$NON-NLS-1$ //$NON-NLS-2$


	/**
	 * The R device to use for generating charts.
	 */
	protected RDevice defaultRDevice;


	/**
	 * Creates a R charts engine implementation with the specified output format as current.
	 * 
	 * @param outputFormat
	 */
	public RChartsEngine(String outputFormat) {

		super(DESCRIPTION, outputFormat);
		this.directComputing = false;
		this.setOutputFormat(outputFormat);
	}

	/**
	 * Creates a R charts engine implementation.
	 */
	public RChartsEngine() {

		this(RChartsEnginePreferences.getInstance().getString(RChartsEnginePreferences.OUTPUT_FORMAT));
	}


	@Override
	public File exportChartResultToFile(ChartResult result, File file, int imageWidth, int imageHeight, String outputFormat) {

		Object chart = result.getChart();
		if (chart == null) {
			Log.warning(Messages.ErrorNoChartToExport);
			return null;
		}

		if (!(chart instanceof File)) {
			Log.warning(NLS.bind(Messages.RChartEngineOnlyManageFileChartsP0, chart));
			return null;
		}

		if (this.getOutputFormat().equals(outputFormat)) {//
			if (!((File) chart).exists() || ((File) chart).length() == 0) {
				result.getChartCreator().updateChart(result);
			}

			return exportChartToFile(chart, file, outputFormat, imageWidth, imageHeight);
		}
		else {
			//Log.warning(NLS.bind(Messages.RChartEngineExportFormatP0CannotDiffferFromTheCurrent, outputFormat, this.getOutputFormat()));
			if (result.getChartCreator() instanceof RChartCreator rcCreator) {
				rcCreator.createChart(result, file);
			}
			return file;
			//return result.getChartCreator().export(chart, file, outputFormat, imageWidth, imageHeight);
		}
		//return null;
	}

	@Override
	public File exportChartToFile(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {

		Log.finest(this.getClass() + ".exportChart(): Not fully implemented. Output format, Image dimensions and cropping are not implemented."); //$NON-NLS-1$

		if (file == null || file.equals(chart)) {
			return (File) chart; // no need to redraw the chart
		}
		else {
			try {
				FileCopy.copy((File) chart, file);
				return file;
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Plots the specified expression using R workspace instance and current charts engine output format.
	 * 
	 * the plot code is wrapped with R output format
	 * 
	 * @param file
	 * @param cmd
	 */
	public File plot(File file, String cmd) {
		return this.plot(file, cmd, null, null, null, RDevice.getDeviceForFile(file));
	}

	/**
	 * Plots the specified expression using R workspace instance and current charts engine output format.
	 * 
	 * @param file
	 * @param cmd
	 * @param result
	 * @param title
	 * @param subtitle
	 * @return
	 */
	public File plot(File file, String cmd, ChartResult result, String title, String subtitle, RDevice rDevice) {

		if (rDevice == null) {
			rDevice = defaultRDevice;
		}
		
		try {

			if (!cmd.endsWith(";") && !cmd.endsWith("\n")) { //$NON-NLS-1$ //$NON-NLS-2$
				cmd += ";"; //$NON-NLS-1$
			}

			// FIXME: margin tests
			// cmd += "windows.options(width=1000, height=1000);\n"; //$NON-NLS-1$
			// cmd += "par(mar = c(50,6,4,1) + .1);\n"; //$NON-NLS-1$

			// FIXME: outer margin to not cut of title
			// cmd += "par(oma = c(10,2,2,2));\n"; //$NON-NLS-1$

			// font size
			// cmd += "par(cex.main = 3.5);\n"; //$NON-NLS-1$
			// cex.lab=1.5, cex.axis=1.5, cex.main=1.5, cex.sub=1.5

			if (result != null) {
				// draw grid
				if (result.isGridVisible()) {
					cmd += this.getGridPlotCmd();
				}
				// draw title and subtitle
				if (result.isTitleVisible()) {
					if (title != null) {
						cmd += this.getTitlePlotCmd(title);
					}
					if (subtitle != null) {
						cmd += this.getSubtitlePlotCmd(subtitle);
					}
				}
			}
			file = RWorkspaceRenderer.getInstance().plot(file, cmd, rDevice);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return file;
	}

	/**
	 * Plots the specified expression using R workspace instance and current charts engine output format.
	 * 
	 * @param file
	 * @param cmd
	 * @param result
	 * @param title
	 * @return
	 */
	protected File plot(File file, String cmd, ChartResult result, String title) {
		return this.plot(file, cmd, result, title, null, RDevice.getDeviceForFile(file));
	}


	/**
	 * Returns a string that represents the R command to draw a title.
	 * 
	 * @param title
	 * @return
	 */
	public String getTitlePlotCmd(String title) {

		// FIXME: SJ: old version without truncated title
		// return String.format("title(main = \"%s\");\n", title); //$NON-NLS-1$

		// FIXME: SJ: tests for adaptive not truncated title
		return String.format("title(main = paste(strwrap(\"%s\", width = 50), collapse = \"\n\"));\n", title); //$NON-NLS-1$


	}

	/**
	 * Returns a string that represents the R command to draw a subtitle.
	 * 
	 * @param subtitle
	 * @return
	 */
	public String getSubtitlePlotCmd(String subtitle) {
		// FIXME: SJ: subtitle always drawn the string at bottom of the plot
		// return String.format("title(sub = \"%s\");\n", subtitle); //$NON-NLS-1$
		return String.format("title(main = \"\n\n\n%s\");\n", subtitle); //$NON-NLS-1$
	}


	/**
	 * Returns a string that represents the R command to draw a grid.
	 * 
	 * @return
	 */
	public String getGridPlotCmd() {
		return "grid (NULL,NULL, lty = \"dotted\", col = \"lightgray\");\n"; //$NON-NLS-1$
	}

	/**
	 * Replaces or creates the vector named "colors" in the R workspace from the current charts engine theme palette according to specified rendering mode and items count.
	 * 
	 * @param renderingColorMode
	 * @param itemsCount
	 */
	public void setColors(int renderingColorMode, int itemsCount) {
		this.setColors(this.theme.getPaletteAsHexFor(renderingColorMode, itemsCount));
	}

	/**
	 * Replaces or creates the vector named "colors" in the R workspace filled with the specified hexadecimal colors palette.
	 * 
	 * @param hexPalette
	 */
	public void setColors(String[] hexPalette) {
		try {
			RWorkspace rw = RWorkspace.getRWorkspaceInstance();
			rw.eval("rm(colors)"); //$NON-NLS-1$
			rw.addVectorToWorkspace("colors", hexPalette);  //$NON-NLS-1$
		}
		catch (RWorkspaceException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void setOutputFormat(String outputFormat) {

		this.outputFormat = outputFormat;

		// FIXME: SJ: voir comment gérer cela autrement + supprimer certaines méthodes du RDevice devenues inutiles depuis qu'on stocke outputFormat ?
		if (outputFormat.equals(ChartsEngine.OUTPUT_FORMAT_JPEG)) {
			this.defaultRDevice = RDevice.JPEG;
		}
		else if (outputFormat.equals(ChartsEngine.OUTPUT_FORMAT_PDF)) {
			this.defaultRDevice = RDevice.PDF;
		}
		else if (outputFormat.equals(ChartsEngine.OUTPUT_FORMAT_PNG)) {
			this.defaultRDevice = RDevice.PNG;
		}
		else if (outputFormat.equals(ChartsEngine.OUTPUT_FORMAT_TIFF)) {
			this.defaultRDevice = RDevice.TIFF;
		}
		else if (outputFormat.equals(ChartsEngine.OUTPUT_FORMAT_PS)) {
			this.defaultRDevice = RDevice.PS;
		}
		else if (outputFormat.equals(ChartsEngine.OUTPUT_FORMAT_SVG)) {
			this.defaultRDevice = RDevice.SVG;
		}
	}



	@Override
	public ArrayList<String> getSupportedOutputDisplayFormats() {

		ArrayList<String> formats = new ArrayList<>(1);

		// FIXME: SJ: UI display output format, only SVG
		// formats.add(outputFormatDefinitions.get(JFCChartsEngine.OUTPUT_FORMAT_SVG));

		return formats;
	}


	@Override
	public ArrayList<String> getSupportedOutputFileFormats() {

		ArrayList<String> formats = new ArrayList<>(5);

		formats.add(OUTPUT_FORMAT_SVG);
		formats.add(OUTPUT_FORMAT_PNG);
		formats.add(OUTPUT_FORMAT_JPEG);
		formats.add(OUTPUT_FORMAT_TIFF);
		formats.add(OUTPUT_FORMAT_PDF);
		formats.add(OUTPUT_FORMAT_PS);

		return formats;
	}



	@Override
	public boolean isRunning() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean stop() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getName() {
		return RChartsEngine.NAME;
	}


}
