package org.txm.stat.engine.r;
// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté

// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of

// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate: 2015-06-15 15:29:16 +0200 (Mon, 15 Jun 2015) $
// $LastChangedRevision: 2989 $
// $LastChangedBy: mdecorde $ 
//


import java.awt.Font;
import java.io.File;
import java.io.IOException;

import org.apache.commons.lang.StringEscapeUtils;
import org.rosuda.REngine.REXP;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.statsengine.r.core.RWorkspace;
import org.txm.statsengine.r.core.exceptions.RWorkspaceException;
import org.txm.statsengine.r.core.messages.RCoreMessages;
import org.txm.utils.OSDetector;
import org.txm.utils.logger.Log;

/**
 * Wrap R plot methods
 * 
 * @author mdecorde
 */

public final class RWorkspaceRenderer {

	/** The workspace. */
	private static RWorkspace workspace = null;


	private static RWorkspaceRenderer instance;


	/** The default device. */
	private RDevice defaultDevice = RDevice.SVG;

	/**
	 * protected on purpose. See {@link #getInstance()}.
	 *
	 * @throws RWorkspaceException the r workspace exception
	 */
	protected RWorkspaceRenderer(RWorkspace workspace) throws RWorkspaceException {
		this.workspace = workspace;
	}


	/**
	 * Plot.
	 *
	 * @param file the file
	 * @param expr the expr
	 */
	public File plot(File file, String expr) {
		return plot(file, expr, defaultDevice);
	}

	/**
	 * Plot.
	 *
	 * @param file the file
	 * @param expr the expr
	 * @param device the device
	 */
	public File plot(File file, String expr, RDevice device) {
		try {
			file.createNewFile();
		}
		catch (IOException e1) {
			org.txm.utils.logger.Log.printStackTrace(e1);
			return null;
		}
		if (!file.canWrite()) {
			Log.severe(TXMCoreMessages.bind(Messages.ErrorCanNotWriteInFileP0, file));
		}
		String devicename = device.getName();

		String name;
		Log.info(RCoreMessages.bind(RCoreMessages.info_savingChartToTheP0File, file.getAbsolutePath()));
		if (OSDetector.isFamilyWindows()) {
			try {
				name = StringEscapeUtils.escapeJava(file.getCanonicalPath());
			}
			catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		else {
			name = file.getAbsolutePath();
		}

		try {
			if (devicename.equals("devSVG")) { //$NON-NLS-1$
				workspace.voidEval("library(RSvgDevice);"); //$NON-NLS-1$
			}


			// FIXME: SJ: test for window dimensions so the large plots can be entirely displayed. Need to pass width and height to RWorkspace.plot() so
			// RChartsEngine will be able to dynamically compute the width from the number of bars in a barplot for example.
			// REXP xp = workspace.eval("try("+devicename+"(\"" + name + "\", width=20, height=10))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$


			// workspace.voidEval("par(family = \"mono\")");

			// REXP xp = workspace.eval("try("+devicename+"(\"" + name + "\"))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

			// FIXME: SJ: tests: Unicode and charts engine font preference
			// REXP xp = workspace.eval("try("+devicename+"(\"" + name + "\", family = \"Lucida Sans Unicode\"))"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			Font font = ChartsEngine.createFont();
			//REXP xp = workspace.safeEval(devicename + "(\"" + name + "\", family = \"" + font.getFontName() + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			REXP xp = workspace.safeEval(device.getRCode(device, file, font, 1000, 1000));
			
			// FIXME: SJ: tests to manage the chart export dimension preferences
//			REXP xp = workspace.safeEval(device.getRCode(device, file, font,
//					ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS),
//					ChartsEnginePreferences.getInstance().getInt(ChartsEnginePreferences.EXPORT_WIDTH_IN_PIXELS)));
			// END OF FIXME
			
			// REXP xp = workspace.safeEval(devicename+"(\"" + name + "\", family = \"" + font.getFamily() + "\")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			// FIXME: SJ: later, add the width and height to use when exporting
			// REXP xp = workspace.safeEval(devicename + "(\"" + name + "\", family = \"" + font.getFontName() + "\", width = 1024, height = 1024)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$



			if (xp.isString() && xp.asString() != null) { // if there's a string
				// then we have a
				// problem, R sent an
				// error
				// System.out.println("Can't open svg graphics device:\n"+xp.asString());
				// this is analogous to 'warnings', but for us it's sufficient to
				// get just the 1st warning
				REXP w = workspace.eval("if (exists(\"last.warning\") && length(last.warning)>0) names(last.warning)[1] else 0"); //$NON-NLS-1$
				// if (w.asString()!=null) System.out.println(w.asString());
				Log.severe(w.asString());
			}

			// ok, so the device should be fine - let's plot
			boolean state = workspace.voidEval(expr);
			workspace.voidEval("dev.off()"); //$NON-NLS-1$

			if (!state) {
				file.delete();
				return null;
			}
			else {
				return file;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Sets the default graphic device.
	 *
	 * @param device the new default graphic device
	 */
	public void setDefaultGraphicDevice(RDevice device) {
		this.defaultDevice = device;
	}

	/**
	 * Sets the default graphic device.
	 *
	 * @param devicename the new default graphic device
	 */
	public void setDefaultGraphicDevice(String devicename) {
		RDevice device = RDevice.valueOf(devicename);
		if (device != null)
			this.defaultDevice = device;
	}


	public static RWorkspaceRenderer getInstance() throws RWorkspaceException {
		if (instance == null)
			instance = new RWorkspaceRenderer(RWorkspace.getRWorkspaceInstance());
		return instance;
	}
}
