// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.stat.engine.r;

import java.awt.Font;
import java.io.File;

import org.txm.statsengine.r.core.RWorkspace;
import org.txm.utils.FileUtils;

/**
 * The Enum RDevice.
 */
// FIXME: SJ: voir le problème lié au stockage de la pref export_rdevice au niveau de la RCP qu'il faudrait renommer et amener au niveau TBX
// FIXME: SJ: en fait non ? car l'appel du device utilise ceci : ex : svg(), postscript(), donc le device peut être différent de l'extension du fichier (file format)
public enum RDevice {

	/** The SVG. */
	SVG,

	/** The JPEG. */
	JPEG,

	/** The PDF. */
	PDF,

	/** The PNG. */
	PNG,

	/** The PS. */
	PS,
	
	TIFF
	
	;

	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		if (this.name().equals("SVG")) //$NON-NLS-1$
			return getCurrentSVGDevice(); //$NON-NLS-1$
		else if (this.name().equals("PDF")) //$NON-NLS-1$
			return "pdf"; //$NON-NLS-1$
		else if (this.name().equals("PS")) //$NON-NLS-1$
			return "postscript"; //$NON-NLS-1$
		else if (this.name().equals("PNG")) //$NON-NLS-1$
			return "png"; //$NON-NLS-1$
		else if (this.name().equals("JPEG")) //$NON-NLS-1$
			return "jpeg"; //$NON-NLS-1$
		else if (this.name().equals("TIFF")) //$NON-NLS-1$
			return "tiff"; //$NON-NLS-1$
		return "svg"; //$NON-NLS-1$
	}

	public String getCurrentSVGDevice() {
		//TODO: temporary commented
		String dev = null; //Toolbox.getParam("r_svg_device"); //$NON-NLS-1$
		if (dev == null || dev.length() == 0) dev = "svg"; //$NON-NLS-1$
		return dev;
	}

	/**
	 * Gets the ext.
	 *
	 * @return the ext
	 */
	public String getExt() {
		if (this.name().equals("SVG")) //$NON-NLS-1$
			return ".svg"; //$NON-NLS-1$
		else if (this.name().equals("PDF")) //$NON-NLS-1$
			return ".pdf"; //$NON-NLS-1$
		else if (this.name().equals("PS")) //$NON-NLS-1$
			return ".ps"; //$NON-NLS-1$
		else if (this.name().equals("PNG")) //$NON-NLS-1$
			return ".png"; //$NON-NLS-1$
		else if (this.name().equals("JPEG")) //$NON-NLS-1$
			return ".jpeg"; //$NON-NLS-1$
		else if (this.name().equals("TIFF")) //$NON-NLS-1$
			return ".tiff"; //$NON-NLS-1$
		return ".svg"; //$NON-NLS-1$
	}

	/**
	 * Ext2 device.
	 *
	 * @return the r device
	 */
	public RDevice ext2Device() {
		if (this.name().equals(".svg")) //$NON-NLS-1$
			return SVG;
		else if (this.name().equals(".pdf")) //$NON-NLS-1$
			return PDF;
		else if (this.name().equals(".ps")) //$NON-NLS-1$
			return PS;
		else if (this.name().equals(".png")) //$NON-NLS-1$
			return PNG;
		else if (this.name().equals(".jpeg")) //$NON-NLS-1$
			return JPEG;
		else if (this.name().equals(".tiff")) //$NON-NLS-1$
			return TIFF;
		return SVG;
	}
	
	public static RDevice getDeviceForFile(File file) {
		
		if (file == null) return null;
		String ext = FileUtils.getExtension(file);
		if (ext.equals("svg")) //$NON-NLS-1$
			return SVG;
		else if (ext.equals("pdf")) //$NON-NLS-1$
			return PDF;
		else if (ext.equals("ps")) //$NON-NLS-1$
			return PS;
		else if (ext.equals("png")) //$NON-NLS-1$
			return PNG;
		else if (ext.equals("jpeg")) //$NON-NLS-1$
			return JPEG;
		else if (ext.equals("tiff")) //$NON-NLS-1$
			return TIFF;
		return SVG;
	}
	
	public String getRCode(RDevice device, File file, Font font, int width, int height) {
		StringBuilder builder = new StringBuilder();
		builder.append(device.getName() + "(\""+RWorkspace.fileToRPath(file)+"\""); //$NON-NLS-1$
		if (font != null && device != PDF) {
			builder.append(", family=\""+font.getFamily()+"\""); //$NON-NLS-1$
		}
		if (device == PNG || device == JPEG || device == TIFF) {
			builder.append(", width="+width); //$NON-NLS-1$
			builder.append(", height="+height); //$NON-NLS-1$
		}
		builder.append(")"); //$NON-NLS-1$
		return builder.toString();
	}
}
