package org.txm.libs.office;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableCell;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;

//import org.odftoolkit.simple.SpreadsheetDocument;
//import org.odftoolkit.simple.table.Cell;
//import org.odftoolkit.simple.table.Row;
//import org.odftoolkit.simple.table.Table;

public class WriteODS {
	
	static { // set Log level to WARNING
		try {
			OdfSpreadsheetDocument.newSpreadsheetDocument().close();
			ReadODS.static_set_log_level();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	OdfSpreadsheetDocument spreadsheet;
	
	OdfTable table;
	
	File newODSFile;
	
	int nlines = 0;
	
	public WriteODS(File newODSFile) throws Exception {
		this.newODSFile = newODSFile;
		this.spreadsheet = OdfSpreadsheetDocument.newSpreadsheetDocument();
		this.table = spreadsheet.getTableList().get(0);
		this.table.removeRowsByIndex(1, 0);
	}
	
	public void newTable(String name) {
		if (name != null && name.length() > 0) {
			this.table = OdfTable.newTable(spreadsheet);
			table.setTableName(name);
		}
		else {
			this.table = OdfTable.newTable(spreadsheet);
		}
	}
	
	/**
	 * 
	 * @param nRows rows are inserted if nRows > 0
	 * @param nColumns columns are inserted if nColumns > 0
	 */
	public void declareRowsAndColumns(int nRows, int nColumns) {
		if (table == null) throw new IllegalStateException("no table set with 'newTable(...)'");
		if (nRows > 0) {
			if (nlines == 0) {
				table.appendRows(nRows);
			} else {
				table.appendRows(nRows - 1);
			}
		}
		nlines += nRows;
		
		if (nColumns > 0) {
			table.appendColumns(nColumns);
		}
	}
	
	protected void writeLine(Object[] line, OdfTableRow row) {
		if (table == null) throw new IllegalStateException("no table set with 'newTable(...)'");
		for (int iCol = 0; iCol < line.length; iCol++) {
			OdfTableCell cell = row.getCellByIndex(iCol);
			if (cell != null) {
				writeCell(cell, line[iCol]);
			}
			else {
				throw new IllegalStateException("cannot get col at " + iCol);
			}
		}
	}
	
	public OdfTableRow writeLineAt(Object[] line, int iRow) {
		if (table == null) throw new IllegalStateException("no table set with 'newtTable(...)'");
		OdfTableRow row = table.getRowByIndex(iRow);
		if (row != null) {
			writeLine(line, row);
		}
		else {
			throw new IllegalStateException("cannot get row at " + iRow);
		}
		return row;
	}
	
	public OdfTableRow writeLineAt(ArrayList<?> line, int iRow) {
		
		if (table == null) throw new IllegalStateException("no table set with 'newtTable(...)'");
		
		OdfTableRow row = table.getRowByIndex(iRow);
		if (row != null) {
			writeLine(line.toArray(), row);
		}
		else {
			throw new IllegalStateException("cannot get row at " + iRow);
		}
		
		return row;
	}
	
	public OdfTableRow writeLine(Object[] line) {
		
		if (table == null) throw new IllegalStateException("no table set with 'newTable(...)'");
		
		OdfTableRow row = null;
		if (nlines == 0) { // by default the table already contains the first line
			row = table.getRowByIndex(0);
		} else {
			row = table.appendRow();
		}
		nlines++;
		writeLine(line, row);
		return row;
	}
	
	public OdfTableRow writeLine(ArrayList<?> line) {
		
		if (table == null) throw new IllegalStateException("no table set with 'newTable(...)'");
		
		OdfTableRow row = null;
		if (nlines == 0) {  // by default the table already contains the first line
			row = table.getRowByIndex(0);
		} else {
			row = table.appendRow();
		}
		nlines++;
		writeLine(line.toArray(), row);
		return row;
	}
	
	public void save() throws Exception {
		spreadsheet.save(newODSFile);
	}
	
	public static void write(File newODFFile, Object[][] data) throws Exception {
		
		if (data.length == 0) throw new IllegalArgumentException("no data to write in " + newODFFile);
		
		OdfSpreadsheetDocument spreadsheet = OdfSpreadsheetDocument.newSpreadsheetDocument();
		if (data.length > 0) {
			Object[] firstLine = data[0];
			OdfTable table = spreadsheet.getTableList().get(0);;
			table.appendRows(data.length - 1); // there is already 1 line
			table.appendColumns(firstLine.length);
			
			for (int iRow = 0; iRow < data.length; iRow++) {
				OdfTableRow row = table.getRowByIndex(iRow);
				Object[] line = data[iRow];
				for (int iCol = 0; iCol < line.length; iCol++) {
					OdfTableCell cell = row.getCellByIndex(iCol);
					writeCell(cell, line[iCol]);
				}
			}
		}
		spreadsheet.save(newODFFile);
	}
	
	public static void write(File newODFFile, List<List<?>> data) throws Exception {
		
		if (data.size() == 0) throw new IllegalArgumentException("no data to write in " + newODFFile);
		
		OdfSpreadsheetDocument spreadsheet = OdfSpreadsheetDocument.newSpreadsheetDocument();
		if (data.size() > 0) {
			List<?> firstLine = data.get(0);
			OdfTable table = spreadsheet.getTableList().get(0);;
			table.appendRows(data.size() - 1); // there is already 1 line
			table.appendColumns(firstLine.size());
			
			for (int iRow = 0; iRow < data.size(); iRow++) {
				OdfTableRow row = table.getRowByIndex(iRow);
				
				List<?> line = data.get(iRow);
				for (int iCol = 0; iCol < line.size(); iCol++) {
					OdfTableCell cell = row.getCellByIndex(iCol);
					writeCell(cell, line.get(iCol));
				}
			}
		}
		spreadsheet.save(newODFFile);
	}
	
	private static void writeCell(OdfTableCell cell, Object object) {
		
		if (object == null) cell.removeContent();
		
		if (object instanceof String) {
			cell.setStringValue(object.toString());
		}
		else if (object instanceof Boolean) {
			cell.setBooleanValue((Boolean) object);
		}
		else if (object instanceof Double) {
			cell.setDoubleValue((Double) object);
		}
		else {
			cell.setStringValue(object.toString());
		}
	}
	
	public static void save(OdfSpreadsheetDocument spreadsheet, File outputFile) throws Exception {
		spreadsheet.save(outputFile);
	}
	
	public static void main(String[] args) throws Exception {
		
		File file = new File("test_writing.odt");
		OdfTextDocument doc = OdfTextDocument.newTextDocument();
		
		OdfTable table = OdfTable.newTable(doc, 6, 4);// doc.addTable(keys.size()+1,4);
		
		doc.save(file);
		System.out.println("File: "+file.getAbsolutePath());
	}
	
	public static void main2(String[] args) throws Exception {
		
		File file = new File("test_writing.ods");
		ArrayList<List<?>> table = new ArrayList<>();
		table.add(Arrays.asList("s1","s2", "s3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("1","2", "3"));
		table.add(Arrays.asList("f1","f2", "f3"));
		
		WriteODS.write(file, table);
	}

	public static void save(OdfTextDocument doc, File outputFile) throws Exception {
		doc.save(outputFile);
	}
}
