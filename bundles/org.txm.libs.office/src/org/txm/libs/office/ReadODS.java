package org.txm.libs.office;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;
import org.odftoolkit.odfdom.dom.element.table.TableTableCellElement;
import org.odftoolkit.odfdom.dom.element.table.TableTableRowElement;
import org.odftoolkit.odfdom.pkg.OdfXMLFactory;
import org.w3c.dom.NodeList;

public class ReadODS {

	static {  // set Log level to WARNING
		try {
			OdfSpreadsheetDocument.newSpreadsheetDocument().close();
			static_set_log_level();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	OdfSpreadsheetDocument spreadsheet = null;

	OdfTable table = null;

	String[] rawHeaders;
	
	String[] headers;

	LinkedHashMap<String, String> record = null;

	int iRow = 0;

	int nRows, nCols;

	private List<TableTableRowElement> rawElementList;

	public ReadODS(File tableFile, String sheetname) throws Exception {

		static_set_log_level();
		spreadsheet = OdfSpreadsheetDocument.loadDocument(tableFile);

		// the first access to the document causes a very long process
		int sheetCount = spreadsheet.getTableList(false).size();
		if (sheetCount < 1) {
			System.out.println("** ReadODS: no sheet found in file. Aborting.");
			return;
		}

		if (sheetname != null) {
			table = spreadsheet.getTableByName(sheetname);
		}
		if (table == null) {
			table = spreadsheet.getTableList(false).get(0);
		}
		nRows = 0; // table.getRowCount();
		nCols = 0; // table.getColumnCount();
		rawElementList = table.getRowElementList();
		for (int iLine = 0 ; iLine < rawElementList.size() ; iLine++) {

			TableTableRowElement row = rawElementList.get(iLine);
			int n = 0;
			NodeList nodes = row.getChildNodes();

			for (int i = 0 ; i < nodes.getLength() ; i++) {
				if (nodes.item(i) instanceof TableTableCellElement ttce) {
					// FAKE Cells
					if (i == nodes.getLength() -1 // last cell  
							&& ttce.getRepetition() > 1  // repeated
							&& ttce.getTextContent().length() == 0) { // with no content
						
					} else {
						n += ttce.getRepetition();
					}
				}
			}
			
			if (//iLine == rawElementList.size() -1 // last line element
					row.getRepetition() > 1000 // repeated line 
					&& nodes.getLength() == 1 // one cell element
					&& n == 0 // cell repeated
					&& nodes.item(0) instanceof TableTableCellElement ttce && ttce.getTextContent().length() == 0) { // empty cell
				// FAKE LINes : the last line is repeated, with one repeated empty cell 
				//System.out.println("FAKE LINES");
				continue; // This is the last virtual lines that Excel writes
			}
			
			nRows += row.getRepetition();
			if (nCols < n) nCols = n;
		}
	}

	public static void static_set_log_level() {

		Logger.getLogger(OdfXMLFactory.class.getName()).setLevel(Level.WARNING);
		try {
			java.util.logging.LogManager.getLogManager().readConfiguration(
					new java.io.ByteArrayInputStream("org.odftoolkit.level=WARNING".getBytes(java.nio.charset.StandardCharsets.UTF_8)));
		}
		catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() {
		spreadsheet.close();
	}

	public boolean readHeaders() {

		OdfTableRow row = table.getRowByIndex(iRow);
		if (row == null) return false;
		
		rawHeaders = rowElementToValues(row.getOdfElement());
		while (rawHeaders == null) {
			iRow++;
			if (iRow >= nRows) {
				rawHeaders = null;
				return false;
			}
			row = table.getRowByIndex(iRow);
			if (row == null) return false;
			
			rawHeaders = rowElementToValues(row.getOdfElement());
		}
//		if (rawHeaders.length < nCols) {
//			nCols = rawHeaders.length; // avoid exception later
//		}
		if (rawHeaders == null) return false;
		
		ArrayList<String> collect = new ArrayList<String>();
		HashSet<String> found = new HashSet<String>();
		for (String h : rawHeaders) {
			if (!found.contains(h)) {
				collect.add(h);
				found.add(h);
			}
		}
		headers = collect.toArray(new String[collect.size()]);
		
		iRow++;
		return rawHeaders.length > 0;
	}

	public String[] getHeaders() {
		return headers;
	}

	public boolean readRecord() {
		
		if (iRow < nRows) {
			_getRecord();
			iRow++; // for next
			return record != null;
		}
		else { // end of file
			record = null;
			return false;
		}
	}
	
	protected void _getRecord() {
		
		
		OdfTableRow row = table.getRowByIndex(iRow);
		if (row == null) {
			record = null;
			return;
		}
		String[] values = rowElementToValues(row.getOdfElement());
		while (values == null) { // line is empty, try the next one
			iRow++;
			if (iRow >= nRows) {
				record = null;
				return;
			}
			try {
				row = table.getRowByIndex(iRow);
			} catch(Exception e) {
				record = null;
				return;
			}
			if (row == null) {
				record = null;
				return;
			}
			values = rowElementToValues(row.getOdfElement());
		}
		
		record = new LinkedHashMap<>();
		for (int colIndex = 0; colIndex < nCols; colIndex++) {

			if (values.length <= colIndex) break;
			if (rawHeaders.length <= colIndex) break;
			
			String col = rawHeaders[colIndex];
			
			//if (col.length() == 0) continue; // skip column with no header
			if (record.containsKey(col)) continue; // ignore if already set
			
			record.put(col, values[colIndex]);

			//			OdfTableCell cell = row.getCellByIndex(colIndex);
			//			if (cell != null) {
			//				String value = cell.getStringValue();
			//				if (value == null) {
			//					record.put(col, "");
			//				}
			//				else {
			//					record.put(col, value);
			//				}
			//			}
			//			else {
			//				record.put(col, "");
			//			}
		}

		for (String k : record.keySet()) {
			if (record.get(k) == null) {
				System.out.println("ERROR null value with " + k);
			}
		}
	}

	public LinkedHashMap<String, String> getRecord() {
		return record;
	}

	/**
	 * The first line encoded the header and the next lines are the records
	 * 
	 * @param inputFile
	 * @param sheetname
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<HashMap<String, String>> toHashMap(File inputFile, String sheetname) throws Exception {

		static_set_log_level();

		OdfSpreadsheetDocument spreadsheet = OdfSpreadsheetDocument.loadDocument(inputFile);

		// the first access to the document causes a very long process
		int sheetCount = spreadsheet.getTableList(false).size();
		if (sheetCount < 1) {
			System.out.println("** ReadODS: no sheet found in file. Aborting.");
			return null;
		}

		OdfTable table = null;
		if (sheetname != null) {
			table = spreadsheet.getTableByName(sheetname);
		}
		if (table == null) {
			table = spreadsheet.getTableList(false).get(0);
		}

		ArrayList<HashMap<String, String>> records = new ArrayList<>();

		ArrayList<ArrayList<String>> lines = toTable(inputFile, sheetname); // all lines have the same number of cells
		ArrayList<String> header = lines.get(0);
		for (int i = 1 ; i < lines.size() ; i++) {
			ArrayList<String> line = lines.get(i);
			HashMap<String, String> record = new HashMap<String, String>();
			records.add(record);
			for (int j = 0 ; j < header.size() ; j++) {
				record.put(header.get(j), line.get(j));
			}
		}

		return records;
	}

	/**
	 * 
	 * @param inputFile
	 * @param sheetname
	 * @return list of lines. line = list of cells
	 * @throws Exception
	 */
	public static ArrayList<ArrayList<String>> toTable(File inputFile, String sheetname) throws Exception {

		static_set_log_level();

		OdfSpreadsheetDocument spreadsheet = OdfSpreadsheetDocument.loadDocument(inputFile);

		// the first access to the document causes a very long process
		int sheetCount = spreadsheet.getTableList(false).size();
		if (sheetCount < 1) {
			System.out.println("** ReadODS: no sheet found in file. Aborting.");
			return null;
		}

		OdfTable table = null;
		if (sheetname != null) {
			table = spreadsheet.getTableByName(sheetname);
		}
		if (table == null) {
			table = spreadsheet.getTableList(false).get(0);
		}

		ArrayList<ArrayList<String>> data = new ArrayList<>();

		// int rowCount = table.getRowCount();

		// findout maximum line length
		int sMax = 0;
		//List<OdfTableRow> rows = ;
		for (TableTableRowElement elem : table.getRowElementList()) {
			//TableTableRowElement elem = row.getOdfElement();
			NodeList children = elem.getChildNodes();
			int s = children.getLength(); // FIXME row.getCellCount() takes too much time
			if (sMax < s) sMax = s;
			break;
		}

		for (TableTableRowElement row : table.getRowElementList()) {
			ArrayList<String> dataline = new ArrayList<>();
			String[] values = rowElementToValues(row);
			int l = 0;
			for (int j = 0; j < values.length; j++) { l += values[j].length();}
			if (l == 0) continue; // ignore empty lines

			int size = values.length;
			for (int j = 0; j < sMax; j++) {

				if (j >= size) {
					dataline.add("");
				} else {
					dataline.add(values[j]);
				}
			}

			if (dataline.size() > 0) {
				for (int r = 0; r < row.getRepetition(); r++) {
					data.add(dataline);
				}
			}
		}

		return data;
	}

	private static String[] rowElementToValues(TableTableRowElement row) {

		NodeList nodes = row.getChildNodes();
		int size = nodes.getLength();
		int nNull = 0;
		ArrayList<String> values = new ArrayList<>();
		for (int i = 0 ; i < size ; i++) {
			if (!(nodes.item(i) instanceof TableTableCellElement)) {
				nNull++;
				continue; 
			}
			TableTableCellElement c = (TableTableCellElement) nodes.item(i);
			String value = "";

			if (c.getFirstChild() != null) {
				value = c.getFirstChild().getTextContent();
				if (value == null) {
					value = "";
					nNull++;
				}
			} else {
				nNull++;
			}
		}
		
		if (size == nNull) return null; // all cells were empty
		
		for (int i = 0 ; i < size ; i++) {
			if (!(nodes.item(i) instanceof TableTableCellElement)) continue; 
			TableTableCellElement c = (TableTableCellElement) nodes.item(i);
			String value = "";

			if (c.getFirstChild() != null) {
				value = c.getFirstChild().getTextContent();
				if (value == null) {
					value = "";
				}
			}

			for (int r = 0 ; r < c.getRepetition() ; r++) {
				values.add(value); // add cell repetitions
			}
		}
		
		return values.toArray(new String[values.size()]);
	}

	public static void main2(String[] args) throws Exception {
		File file = new File("metadata.ods");
		System.out.println("To table...");
		ArrayList<ArrayList<String>> table = toTable(file, null);
		System.out.println("Lines...");
		int i = 0;
		for (ArrayList<String> line : table) {
			System.out.println(StringUtils.join(line, "\t"));
			if (i++ > 10) break;
		}
		System.out.println("done.");
		// Metadatas m = new Metadatas(file, )
	}

	public static void main3(String[] args) throws Exception {
		File file = new File("metadata.ods");
		System.out.println("To records...");
		ArrayList<HashMap<String, String>> table = toHashMap(file, null);
		System.out.println("Lines...");
		int i = 0;
		for (HashMap<String, String> line : table) {
			System.out.println(StringUtils.join(line, "\t"));
			if (i++ > 10) break;
		}
		System.out.println("done.");
		// Metadatas m = new Metadatas(file, )
	}

	public static void main1(String[] args) throws Exception {
		File file = new File("/home/mdecorde/Données-Escofier.ods");
		System.out.println("To records...");
		ReadODS reader = new ReadODS(file, null);
		boolean headerok = reader.readHeaders();
		if (headerok) {
			String[] headers = reader.getHeaders();
			System.out.println("HEADER="+Arrays.toString(headers));
			int i = 0;
			while (reader.readRecord()) {
				System.out.println(reader.getRecord());
//				if (i++ > 20) {
//					System.out.println("CUT!!");
//					break;
//				}
			}
		} else {
			System.out.println("No header. Stop.");
		}

	}

	public static void main(String[] args) throws Exception {
		main1(args);
		//main2(args);
		//main3(args);

	}
}
