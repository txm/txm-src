package org.txm.internalview.rcp.editors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.txm.core.messages.TXMCoreMessages;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.Parameter;
import org.txm.internalview.core.functions.StructureView;
import org.txm.internalview.core.messages.StructureViewCoreMessages;
import org.txm.internalview.core.preferences.StructureViewPreferences;
import org.txm.internalview.rcp.messages.StructureViewUIMessages;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TableKeyListener;
import org.txm.rcp.editors.TableResultEditor;
import org.txm.rcp.editors.TableUtils;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.NewNavigationWidget;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.rcp.swt.widget.structures.StructuralUnitsComboViewer;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.Property;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.logger.Log;

/**
 * Internal view editor.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class StructureViewEditor extends TXMEditor<StructureView> implements TableResultEditor {

	public static final String ID = StructureViewEditor.class.getName();

	protected CQPCorpus corpus;

	protected StructureView internalView;


	protected TableViewer viewer;

	protected Table table;

	protected Label structInfos;

	protected Composite navigationPanel;


	/**
	 * For current page parameter synchronization.
	 */
	@Parameter(key = TXMPreferences.CURRENT_PAGE)
	protected NewNavigationWidget navigation;

	/**
	 * Structural unit to explore.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT)
	protected StructuralUnitsComboViewer structuralUnitsComboViewer;

	/**
	 * Word properties to display.
	 */
	@Parameter(key = TXMPreferences.UNIT_PROPERTIES)
	protected PropertiesSelector<WordProperty> propertiesSelector;

	/**
	 * Structural units to display.
	 */
	@Parameter(key = TXMPreferences.STRUCTURAL_UNIT_PROPERTIES)
	protected PropertiesSelector<StructuralUnitProperty> structuralUnitPropertiesSelector;

	/**
	 * Number of lines to display.
	 */
	@Parameter(key = StructureViewPreferences.CUT_STRUCTURE_CONTENT)
	protected Spinner nbOfElementsPerPageSpinner;

	private TableKeyListener tableKeyListener;

	@Override
	public void _createPartControl() {

		internalView = this.getResult();

		// Computing listener
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, true);
		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this);


		// Main parameters
		GLComposite mainParametersArea = this.getMainParametersComposite();
		mainParametersArea.getLayout().numColumns = 2;

		// Structural unit selector
		Label structComboLabel = new Label(mainParametersArea, SWT.NONE);
		structComboLabel.setText(StructureViewUIMessages.structure);
		structComboLabel.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, true));
		this.structuralUnitsComboViewer = new StructuralUnitsComboViewer(mainParametersArea, this, true);
		structuralUnitsComboViewer.getCombo().setToolTipText("The displayed structure");
		// Listener
		this.structuralUnitsComboViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				structuralUnitPropertiesSelector.setProperties(structuralUnitsComboViewer.getSelectedStructuralUnit().getUserDefinedOrderedProperties());
				structuralUnitPropertiesSelector.setSelectedProperty(0);
			}
		});


		// Extended parameters
		Composite parametersArea = getExtendedParametersGroup();
		parametersArea.setLayout(new GridLayout(4, false));

		// Word properties selector
		propertiesSelector = new PropertiesSelector<>(parametersArea);
		propertiesSelector.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, true));
		propertiesSelector.setLayout(new GridLayout(3, false));
		try {
			List<WordProperty> l = internalView.getCorpus().getOrderedProperties();
			l.addAll(internalView.getCorpus().getVirtualProperties());
			propertiesSelector.setProperties(l);
		}
		catch (CqiClientException e) {
			Log.printStackTrace(e);
		}
		// Listener
		propertiesSelector.addSelectionListener(computeSelectionListener);

		// Structural unit properties selector
		structuralUnitPropertiesSelector = new PropertiesSelector<>(parametersArea);
		structuralUnitPropertiesSelector.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, true));
		structuralUnitPropertiesSelector.setLayout(new GridLayout(3, false));
		structuralUnitPropertiesSelector.setTitle(StructureViewUIMessages.structuralProperties);
		structuralUnitPropertiesSelector.addSelectionListener(computeSelectionListener);


		// Number of lines per page
		TXMParameterSpinner nbOfElementsPerPageLabeledSpinner = new TXMParameterSpinner(this.getExtendedParametersGroup(), this, "Number of lines");
		nbOfElementsPerPageSpinner = nbOfElementsPerPageLabeledSpinner.getControl();
		nbOfElementsPerPageSpinner.setMinimum(1);
		nbOfElementsPerPageSpinner.setMaximum(Integer.MAX_VALUE);
		nbOfElementsPerPageSpinner.setToolTipText("Cut the number of lines displayed in the table to this value");

		// Result area
		Composite resultPanel = getResultArea();

		viewer = new TableViewer(resultPanel, SWT.FULL_SELECTION | SWT.VIRTUAL);
		tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(viewer);
		tableKeyListener.installSearchGroup(this);
		tableKeyListener.installMenuContributions(this);
		TableUtils.installColumnResizeMouseWheelEvents(viewer);
		TableUtils.installTooltipsOnSelectionChangedEvent(viewer);

		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setHeaderVisible(true);
		table = viewer.getTable();
		table.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		viewer.setContentProvider(new IStructuredContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}

			@Override
			public void dispose() {
			}

			@Override
			public Object[] getElements(Object inputElement) {
				int nlines = internalView.getSegmentLength();
				nlines = Math.min(nlines, internalView.getNLinesPerPage());
				List<WordProperty> props = internalView.getProperties();

				ArrayList<Object[]> lines = new ArrayList<>();
				for (int i = 0; i < nlines; i++) {
					lines.add(new String[props.size()]);
				}
				if (inputElement instanceof HashMap<?, ?>) {
					@SuppressWarnings("unchecked")
					HashMap<Property, List<String>> l = (HashMap<Property, List<String>>) inputElement;
					for (int i = 0; i < props.size(); i++) {
						Property prop = props.get(i);
						List<String> data = l.get(prop);
						for (int j = 0; j < nlines; j++) {
							lines.get(j)[i] = data.get(j);
						}
					}
				}
				return lines.toArray();
			}
		});

		table.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				//				ISelection selection = viewer.getSelection();
				//				if (selection.isEmpty()) return;
				//				
				//				IStructuredSelection sselection = (IStructuredSelection) selection;
				//				Object first = sselection.getFirstElement();
				//				System.out.println("First="+first);
			}
		});

		// Bottom tool bar 
		navigationPanel = getBottomToolbar().installGLComposite(StructureViewUIMessages.navigation, 4, false);
		navigationPanel.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		// Navigation widget
		navigation = new NewNavigationWidget(navigationPanel);
		navigation.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, true));
		navigation.addGoToKeyListener(computeKeyListener);

		// Listeners
		navigation.addFirstListener(computeSelectionListener);
		navigation.addPreviousListener(computeSelectionListener);
		navigation.addNextListener(computeSelectionListener);
		navigation.addLastListener(computeSelectionListener);


		structInfos = new Label(navigationPanel, SWT.NONE);
		structInfos.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, true));
		structInfos.setText(""); //$NON-NLS-1$

		// Button style = new Button(navigationPanel, SWT.RADIO);
		// style.setText("table");
		// style.setToolTipText("Display mode. Table or Sentence.");

		// Register the context menu
		TXMEditor.initContextMenu(this.viewer.getTable(), this.getSite(), this.viewer);

	}

	// FIXME: to remove later
	protected boolean goToPage(int page) throws CqiClientException {
		internalView.setCurrentPage(page);
		fillResultArea(); // one segment at time

		if (page == 0) {
			navigation.setFirstEnabled(false);
			navigation.setPreviousEnabled(false);
		}
		else {
			navigation.setFirstEnabled(true);
			navigation.setFirstEnabled(true);
		}

		if (page == internalView.getSegmentsCount() - 1) {
			navigation.setNextEnabled(false);
			navigation.setLastEnabled(false);
		}
		else {
			navigation.setNextEnabled(true);
			navigation.setLastEnabled(true);
		}
		return true;
	}


	HashMap<Property, TableViewerColumn> columns = new HashMap<>();

	HashMap<TableViewerColumn, Integer> columnsOrder = new HashMap<>();

	private void initColumns() {

		for (TableViewerColumn col : columns.values()) {
			col.getColumn().dispose();
		}
		columnsOrder = new HashMap<>();
		columns = new HashMap<>();

		if (internalView == null) {
			// System.out.println("REMOVE NON USED COLUMNS");
		}
		else {
			int c = 0;

			for (final Property prop : internalView.getProperties()) {
				if (!columns.containsKey(prop)) {
					final TableViewerColumn col = new TableViewerColumn(viewer, SWT.NONE);
					col.getColumn().setWidth(100);
					col.getColumn().setText(prop.getName());
					col.setLabelProvider(new ColumnLabelProvider() {

						@Override
						public String getText(Object element) {
							if (element instanceof Object[]) {
								Integer idx = columnsOrder.get(col);
								return "" + ((Object[]) element)[idx]; //$NON-NLS-1$
							}
							return TXMCoreMessages.bind(TXMCoreMessages.errorColonP0, element);
						}
					});
					columns.put(prop, col);

				}
				TableViewerColumn col = columns.get(prop);
				columnsOrder.put(col, c++);
			}
		}
	}


	/**
	 * Creates a queries list from the selected table lines.
	 * 
	 * @return a queries list based on the selected lines
	 */
	public List<Query> createQueries(StructuredSelection sselection) {
		List<Query> queries = new ArrayList<>();
		queries.add(this.createQuery(sselection));
		return queries;
	}


	/**
	 * Creates a query string from the selected table lines.
	 * 
	 * @param sselection
	 * @return a query string based on the selected lines
	 */
	public Query createQuery(StructuredSelection sselection) {
		TableColumn[] cols = viewer.getTable().getColumns();
		TableItem[] items = viewer.getTable().getItems();
		int[] indexes = viewer.getTable().getSelectionIndices();
		if (indexes.length == 0) {
			return new CQLQuery(""); //$NON-NLS-1$
		}

		StringBuffer query = new StringBuffer("["); //$NON-NLS-1$
		for (int icol = 0; icol < cols.length; icol++) {
			TableColumn col = cols[icol];
			if (icol > 0) {
				query.append(" & " + col.getText() + "=\""); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else {
				query.append(col.getText() + "=\""); //$NON-NLS-1$
			}
			HashSet<String> values = new HashSet<>();
			for (int i = 0; i < indexes.length; i++) {
				int idx = indexes[i];
				values.add(CQLQuery.addBackSlash(items[idx].getText(icol)));
			}
			boolean firstValue = true;
			for (String value : values) {
				if (firstValue) {
					query.append(value);
					firstValue = false;
				}
				else {
					query.append("|" + value); //$NON-NLS-1$
				}
			}
			query.append("\""); //$NON-NLS-1$
		}
		query.append("]"); //$NON-NLS-1$
		Log.info(NLS.bind(StructureViewUIMessages.queryColonP0, query));
		return new CQLQuery(query.toString());
	}



	public boolean backToText(Match m) {
		// Match m = line.getMatch();
		internalView.getCurrentPage();
		int[] cpos = { m.getStart() };
		int[] n;
		try {
			StructuralUnitProperty sup = null;
			for (StructuralUnitProperty s : internalView.getStructuralUnit().getOrderedProperties()) {
				sup = s;
				break;
			}
			n = CQPSearchEngine.getCqiClient().cpos2Struc(sup.getQualifiedName(), cpos);
			return goToPage(n[0]);
		}
		catch (Exception e) {
			Log.info(NLS.bind(StructureViewUIMessages.errorWhileInternalViewGoBackToP0, m));
			Log.printStackTrace(e);
		}
		return false;
	}



	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}

	public void fillResultArea() throws CqiClientException {
		initColumns();
		viewer.setInput(internalView.getCurrentPageContent());
		String infos = internalView.getStructInfos();
		if (internalView.getProperties().size() > 0) {
			infos = StructureViewCoreMessages.structureInformationsColon + infos;
		}
		structInfos.setText(infos);

		navigation.setMinPosition(1);
		navigation.setCurrentPosition(this.getResult().getCurrentPage());
		navigation.setMaxPosition(this.getResult().getSegmentsCount());

		navigation.refresh();
		navigationPanel.layout();

		if (internalView.getSegmentLength() > internalView.getNLinesPerPage()) {
			Log.info(NLS.bind(StructureViewUIMessages.warningTheNavigatorTableContentHasBeenCutToP0LinesWasP1, internalView.getNLinesPerPage(), internalView.getSegmentLength()));
		}
	}

	@Override
	public void updateEditorFromResult(boolean update) {
		try {
			fillResultArea();
		}
		catch (CqiClientException e) {
			e.printStackTrace();
		}
	}

	@Override
	public TableViewer[] getTableViewers() {

		return new TableViewer[] { viewer };
	}

	@Override
	public TableKeyListener getTableKeyListener() {

		return tableKeyListener;
	}
}
