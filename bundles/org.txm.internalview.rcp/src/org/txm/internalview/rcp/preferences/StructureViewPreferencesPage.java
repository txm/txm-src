package org.txm.internalview.rcp.preferences;

import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.internalview.core.preferences.StructureViewPreferences;
import org.txm.internalview.rcp.messages.StructureViewUIMessages;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

public class StructureViewPreferencesPage extends TXMPreferencePage {

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(StructureViewPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle(StructureViewUIMessages.navigation);
		this.setImageDescriptor(IImageKeys.getImageDescriptor(this.getClass(), "icons/application_form_magnify.png"));
	}

	@Override
	protected void createFieldEditors() {
		
		
		IntegerFieldEditor elementsPerPage = new IntegerFieldEditor(StructureViewPreferences.CUT_STRUCTURE_CONTENT, StructureViewUIMessages.numberOfElementsShown, this.getFieldEditorParent());
		elementsPerPage.getTextControl(this.getFieldEditorParent()).setToolTipText(StructureViewUIMessages.ifTheNumberOfElementsIsBiggerthanThisValueTheNextElementsAreNotShown);
		this.addField(elementsPerPage);
	}
}
