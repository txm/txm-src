package org.txm.internalview.rcp.handlers;

import java.util.Arrays;
import java.util.List;

import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.graphics.Point;
import org.txm.core.preferences.TBXPreferences;
import org.txm.core.results.TXMResult;
import org.txm.internalview.core.functions.StructureView;
import org.txm.internalview.rcp.editors.StructureViewEditor;
import org.txm.internalview.rcp.messages.StructureViewUIMessages;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.TXMResultEditorInput;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.logger.Log;

public class BackToStructureView {

	public void doit(TXMEditor<? extends TXMResult> sourceEditor, CQPCorpus corpus, Match match, List<WordProperty> props, StructuralUnit structuralUnit,
			List<StructuralUnitProperty> structuralUnitProperties) {
		// Last resort edition
		try {
			if (structuralUnit == null) {
				structuralUnit = corpus.getStructuralUnit(TBXPreferences.DEFAULT_STRUCTURAL_UNIT);
			}

			if (props == null || props.size() == 0) {
				props = Arrays.asList(corpus.getWordProperty());
			}

			StructureView iview = new StructureView(corpus);
			iview.setParameters(props, structuralUnit, structuralUnitProperties, null);

			TXMResultEditorInput<StructureView> editorInput = new TXMResultEditorInput<StructureView>(iview);

			StructureViewEditor editor = (StructureViewEditor) SWTEditorsUtils.openEditor(editorInput, StructureViewEditor.class.getName());
			if (match != null) { // FIXME should not work the editor is not ready to navigate. Move this to the StructureView parameters set before
				editor.backToText(match);
			}
			if (editor != null) {
				int position = -2;
				if (position == -2) {
					Point s = sourceEditor.getParent().getSize();
					if (s.x < s.y) {
						position = EModelService.ABOVE;
					}
					else {
						position = EModelService.RIGHT_OF;
					}
				}
				SWTEditorsUtils.moveEditor(sourceEditor, editor, EModelService.ABOVE);
			}
		}
		catch (Exception e) {
			System.err.println(StructureViewUIMessages.bind(StructureViewUIMessages.error_backto_internalviewP0, e));
			Log.printStackTrace(e);
		}
	}
}
