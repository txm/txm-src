package org.txm.internalview.rcp.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.internalview.core.functions.StructureView;
import org.txm.internalview.rcp.editors.StructureViewEditor;
import org.txm.internalview.rcp.messages.StructureViewUIMessages;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.utils.logger.Log;

/**
 * Opens an internal view editor.
 * Computes the result if it contains all required parameters.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public class ComputeStructureView extends BaseAbstractHandler {


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (!this.checkCorpusEngine()) {
			return false;
		}

		Object selection = this.getCorporaViewSelectedObject(event);
		StructureView internalView = null;

		// Creating from Corpus
		if (selection instanceof CQPCorpus) {
			internalView = new StructureView((CQPCorpus) selection);
		}
		// Reopen an existing result
		else if (selection instanceof StructureView) {
			internalView = (StructureView) selection;
		}
		else {
			Log.warning(StructureViewUIMessages.errorColonSelectionIsNotACorpus);
			return null;
		}

		TXMEditor.openEditor(internalView, StructureViewEditor.class.getName());
		return null;
	}


}
