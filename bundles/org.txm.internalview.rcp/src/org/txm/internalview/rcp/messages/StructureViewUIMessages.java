package org.txm.internalview.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

/**
 * Internal view UI messages.
 * 
 * @author sjacquot
 *
 */
public class StructureViewUIMessages extends NLS {

	private static final String BUNDLE_NAME = "org.txm.internalview.rcp.messages.messages"; //$NON-NLS-1$

	public static String warningTheNavigatorTableContentHasBeenCutToP0LinesWasP1;

	public static String errorColonSelectionIsNotACorpus;

	public static String navigation;

	public static String errorWhileInternalViewGoBackToP0;

	public static String queryColonP0;

	public static String structure;

	public static String structuralProperties;

	public static String error_backto_internalviewP0;

	public static String numberOfElementsShown; // "Number of elements shown"

	public static String ifTheNumberOfElementsIsBiggerthanThisValueTheNextElementsAreNotShown;

	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, StructureViewUIMessages.class);
	}
}
