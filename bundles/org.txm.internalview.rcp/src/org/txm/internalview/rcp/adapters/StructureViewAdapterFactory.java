package org.txm.internalview.rcp.adapters;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.internalview.core.functions.StructureView;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;

/**
 * A factory for creating Adapter objects.
 * 
 * @author sjacquot
 */
public class StructureViewAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = AbstractUIPlugin.imageDescriptorFromPlugin(FrameworkUtil.getBundle(StructureViewAdapterFactory.class).getSymbolicName(),
			"platform:/plugin/" + FrameworkUtil.getBundle(StructureViewAdapterFactory.class).getSymbolicName() + "/icons/application_form_magnify.png"); //$NON-NLS-1$ //$NON-NLS-2$

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof StructureView) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			});
		}
		return null;
	}
}
