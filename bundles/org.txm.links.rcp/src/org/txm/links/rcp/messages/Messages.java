package org.txm.links.rcp.messages;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.messages.Utf8NLS;

public class Messages extends NLS {
	
	private static final String BUNDLE_NAME = "org.txm.links.rcp.messages.messages"; //$NON-NLS-1$
	
	public static String noMatchesWasSetCommandCanceled; 
	public static String noQueryWasSetCommandCanceled;

	public static String NoResultFondWithP0; 
	
	static {
		// initialize resource bundle
		Utf8NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() { }
}
