// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.links.rcp.handlers;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.links.rcp.messages.Messages;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.core.Query;
import org.txm.utils.logger.Log;

/**
 * Base command to send a selection to a command that needs one or more queries to compute the new result.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
// FIXME: SJ: improvements:
// solution 1 - create an interface as SelectionSendableAsQueries with method createQueries() or buildQueries() or getQueries()
// - instead of subclassing this class, Editors can implement SeletionSendableAsQueries interface and the method?
// solution 2 - create a unique interface as SelectionSendable with 1 method as createTargetParameters() that directly returns a map of parameters
// eg. {TXMPreferences.QUERY, "[word=faire]"}


public abstract class SendSelectionToQueryable extends BaseAbstractHandler {
	
	/**
	 * Creates the query.
	 * 
	 * @param isel
	 * @return
	 */
	public abstract Query createQuery(ExecutionEvent event, ISelection isel);
	
	/**
	 * Creates the query.
	 * 
	 * @param isel
	 * @return
	 */
	public Integer getIndexResult(ExecutionEvent event, ISelection isel) {
		return null;
	}
	
	/**
	 * Creates the query.
	 * 
	 * @param isel
	 * @return
	 */
	public Integer getSubIndexResult(ExecutionEvent event, ISelection isel) {
		return null;
	}
	
	/**
	 * Creates the queries.
	 * This method is essentially dedicated to be overridden, if needed.
	 * 
	 * @param selection
	 * @return
	 */
	public List<Query> createQueries(ExecutionEvent event, ISelection selection) {
		return Arrays.asList(this.createQuery(event, selection));
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		
		if (selection != null && !selection.isEmpty()) {

			Query query = this.createQuery(event, selection);
			List<Query> queries = this.createQueries(event, selection);
			Integer index = this.getIndexResult(event, selection);
			Integer subindex = this.getSubIndexResult(event, selection);
			
			if ((query == null || query.isEmpty()) && (queries == null || queries.isEmpty())) {
				Log.warning(Messages.noQueryWasSetCommandCanceled);
				return null;
			}
			
			// create a local result node in the project node, store in some parameters and call the linked command
			String id = event.getCommand().getId();
			TXMResult result = this.getResultParent(event, id);
			// FIXME: SJ: we can't put the class name in the result node path here since we does not know the target class
			String resultParametersNodePath = result.getProject().getParametersNodeRootPath() + TXMResult.createUUID() + "_uniqlink"; //$NON-NLS-1$

			TXMPreferences.put(resultParametersNodePath, TXMPreferences.PARENT_PARAMETERS_NODE_PATH, result.getParametersNodePath());
			TXMPreferences.put(resultParametersNodePath, TXMPreferences.QUERY, Query.queryToJSON(query));
			TXMPreferences.put(resultParametersNodePath, TXMPreferences.QUERIES, Query.queriesToJSON(queries));
			TXMPreferences.put(resultParametersNodePath, TXMPreferences.INDEX, index);
			TXMPreferences.put(resultParametersNodePath, TXMPreferences.SUB_INDEX, subindex);
			TXMPreferences.put(resultParametersNodePath, "uniqlink", this.isUniqLink()); //$NON-NLS-1$
			
			Object cmdResult = BaseAbstractHandler.executeSendToCommand(id, resultParametersNodePath);
			// TODO: MD: we should test if there is already a linked editor and used it. To do it we must either A) know which editor class is must be opened B) know why a previous editor has been open
		}
		else {
			this.logCanNotExecuteCommand(selection);
		}
		return null;
	}

	/**
	 * Override this method if you need the link to open only one editor at time
	 * @return
	 */
	public boolean isUniqLink() {
		return false;
	}
}
