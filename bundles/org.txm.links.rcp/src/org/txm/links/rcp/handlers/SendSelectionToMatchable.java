// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.links.rcp.handlers;

import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.core.preferences.TXMPreferences;
import org.txm.core.results.TXMResult;
import org.txm.links.rcp.messages.Messages;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.query.Match;
import org.txm.utils.logger.Log;

/**
 * Base command to send a selection to a command that needs a result of query (List<Match>) to compute the new result.
 * 
 * @author mdecorde
 * @author sjacquot
 * 
 */
public abstract class SendSelectionToMatchable extends BaseAbstractHandler {
	
	/**
	 * Creates the queries.
	 * This method is essentially dedicated to be overridden, if needed.
	 * 
	 * @param selection
	 * @return
	 */
	public abstract List<Match> createMatches(ExecutionEvent event, IStructuredSelection selection);
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
		if (selection != null) {
			
			List<Match> matches = this.createMatches(event, selection);
			
			if (matches.isEmpty()) {
				Log.severe(Messages.noMatchesWasSetCommandCanceled);
				return null;
			}
			
			// create a local result node in the project node, store in some parameters and call the linked command
			TXMResult result = this.getResultParent(event, event.getCommand().getId());
			if (result == null) {
				Log.severe(Messages.bind(Messages.NoResultFondWithP0, event));
				return null;
			}
			// FIXME: SJ: we can't put the class name here since we does not know the target class
			String resultParametersNodePath = result.getProject().getParametersNodeRootPath() + TXMResult.createUUID() + "_link";  //$NON-NLS-1$

			TXMPreferences.put(resultParametersNodePath, TXMPreferences.PARENT_PARAMETERS_NODE_PATH, result.getParametersNodePath());
			TXMPreferences.put(resultParametersNodePath, TXMPreferences.MATCHES, matches.toString());
			
			BaseAbstractHandler.executeSendToCommand(event.getCommand().getId(), resultParametersNodePath);
		}
		else {
			this.logCanNotExecuteCommand(selection);
		}
		return null;
	}
	
}
