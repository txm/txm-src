package org.txm.utils.tests.rcp;

import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;
import org.eclipse.swtbot.eclipse.finder.widgets.SWTBotView;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotMenu;
import org.junit.jupiter.api.Assertions;
import org.txm.core.results.TXMResult;
import org.txm.index.rcp.editors.IndexEditor;
import org.txm.rcp.CorporaSourceProvider;
import org.txm.rcp.editors.ITXMResultEditor;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.rcp.views.corpora.CorporaView;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.utils.tests.InitializeToolboxForTests;

public class IndexHandlerTest {

	private static SWTWorkbenchBot bot = null;

	//@Test
	public void executeExit() {
		if (bot == null) {
			bot = new SWTWorkbenchBot();
		}

		if (!InitializeToolboxForTests.loadTestCorpora()) {
			Assertions.fail("*** TEST CORPORA IS NOT INITIALIZED. INDEX RCP TESTS CANNOT BE DONE ***");
			return;
		}

		try {

			SWTBotView view = bot.viewById(CorporaView.class.getName());

			System.out.println("view=" + view);
			CorporaView corporaView = (CorporaView) view.getViewReference().getView(false);
			System.out.println("corporaView=" + corporaView);

			CorporaView.reload(); // load the corpora in the view
			CorporaView.getInstance().setFocus();
			CorporaView.select(CorpusManager.getCorpusManager().getCorpus("VOEUX"));
			CorporaSourceProvider.updateSelection(CorporaView.getSelection());

			SWTBotMenu toolsMenu = bot.menu("Outils");
			System.out.println("fileMenu=" + toolsMenu);
			Assertions.assertTrue(toolsMenu != null);
			//			
			SWTBotMenu indexMenu = toolsMenu.menu("Index");
			System.out.println("exitMenu=" + indexMenu);
			Assertions.assertTrue(indexMenu != null);

			indexMenu.click();

			ITXMResultEditor<? extends TXMResult> editor = SWTEditorsUtils.getActiveEditor(null);
			if (editor != null && editor instanceof IndexEditor ieditor) {
				ieditor.setQuery("je");
				ieditor.compute(true);
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		IndexHandlerTest eht = new IndexHandlerTest();
		try {
			eht.executeExit();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
