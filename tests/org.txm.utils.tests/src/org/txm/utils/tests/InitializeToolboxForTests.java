package org.txm.utils.tests;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedHashMap;

import org.txm.Toolbox;
import org.txm.core.preferences.TBXPreferences;
import org.txm.utils.DeleteDir;

public class InitializeToolboxForTests {

	public InitializeToolboxForTests() {

		// TODO Auto-generated constructor stub
	}


	public static boolean initialize() {

		if (!Toolbox.isInitialized()) {
			try {
				//				File minimalPreferencesFile = new File("toolbox.properties");
				//				IOUtils.write(minimalPreferencesFile, TBXPreferences.USER_TXM_HOME+"="+minimalPreferencesFile.getParentFile().getAbsolutePath());
				//				System.out.println("prefs: "+minimalPreferencesFile.getAbsolutePath());

				File homedir = new File("../TXMHOME").getAbsoluteFile();

				DeleteDir.deleteDirectory(homedir); // clear previous tests files

				TBXPreferences.getInstance().put(TBXPreferences.USER_TXM_HOME, homedir.getAbsolutePath());
				Toolbox.initialize();
			}
			catch (Exception e) {
				e.printStackTrace();
				System.out.println("*** TOOLBOX COULD NOT BE INITIALIZED.");
				return false;
			}
		}
		return Toolbox.isInitialized();
	}

	public static boolean loadTestCorpora() {

		if (!initialize()) {
			System.out.println("*** Could not load test corpora because toolbox is not initialized.");
			return false;
		}


		File testcorpora = new File("testcorpora");
		if (!testcorpora.exists()) {
			System.out.println("*** Could not load test corpora because testcorpora is not found: " + testcorpora.getAbsolutePath());
			return false;
		}

		LinkedHashMap<File, String> errors = new LinkedHashMap<>();

		for (File txmFile : testcorpora.listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return file.isFile() && file.getName().endsWith(".txm");
			}
		})) {
			try {
				System.out.println("Loading " + txmFile);

				if (Toolbox.workspace.installCorpus(txmFile) == null) {
					errors.put(txmFile, "null project.");
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				errors.put(txmFile, e.getMessage());
			}
		}

		if (errors.size() > 0) {
			for (File txmFile : errors.keySet()) {
				System.out.println("*** " + txmFile + " not loaded: " + errors.get(txmFile));
			}
			return false;
		}
		else {
			return true;
		}
	}
}
