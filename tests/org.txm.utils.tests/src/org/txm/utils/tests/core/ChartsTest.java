package org.txm.utils.tests.core;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.utils.tests.InitializeToolboxForTests;

/**
 * Duplicate this class to create new tests
 * 
 * @author mdecorde
 *
 */
public class ChartsTest {

	public ChartsTest() {

	}

	@SuppressWarnings("unused")
	@Test
	public void test() {

		System.out.println("Charts test: start");

		if (!InitializeToolboxForTests.initialize()) { // initialize the toolbox is necessary
			System.out.println("Charts test: aborted");
			return;
		}
		Integer c = null;
		if (c != null) {
			Assertions.fail("Problem found :-o");
		}
		// ...

		System.out.println("Charts test done");
	}
}
