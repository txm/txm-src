package org.txm.utils.tests.core;

import java.io.File;

import org.junit.Test;

public class ReadExcelTest {

	static int DELAY = 100000;

	public ReadExcelTest() {

	}

	@Test
	public void test() {

		File testFile = new File("table.xlsx");
		ReadTSVTest.doTest(testFile);
	}

	public static void main(String[] args) {
		new ReadExcelTest().test();
	}
}
