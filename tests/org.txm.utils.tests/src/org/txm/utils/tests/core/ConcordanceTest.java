package org.txm.utils.tests.core;

import java.io.File;
import java.util.LinkedHashMap;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.Toolbox;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.functions.Concordance.Format;
import org.txm.concordance.core.preferences.ConcordancePreferences;
import org.txm.objects.Project;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.utils.Diff;
import org.txm.utils.tests.InitializeToolboxForTests;

/**
 * TODO each command must implements its tests in its own plugin
 * 
 * @author mdecorde
 *
 */
public class ConcordanceTest {

	public ConcordanceTest() {

	}

	@Test
	public void test() {

		if (InitializeToolboxForTests.initialize() && Toolbox.workspace.getProject("VOEUX") != null) {
			Toolbox.workspace.getProject("VOEUX").delete();
		}

		if (!InitializeToolboxForTests.loadTestCorpora()) {
			Assertions.fail("*** TEST CORPORA IS NOT INITIALIZED. COMMANDS TESTS CANNOT BE DONE ***");
			return;
		}

		Project voeuxp = Toolbox.workspace.getProject("VOEUX");
		if (voeuxp == null) {
			Assertions.fail("*** VOEUX corpus project not found. COMMANDS TESTS CANNOT BE DONE ***");
			return;
		}

		LinkedHashMap<String, String> errors = new LinkedHashMap<>();

		MainCorpus voeux = voeuxp.getFirstChild(MainCorpus.class);
		if (voeux == null) {
			Assertions.fail("*** VOEUX corpus not found. COMMANDS TESTS CANNOT BE DONE ***");
			return;
		}

		try {
			if (!voeux.compute()) {
				Assertions.fail("*** VOEUX corpus not functionnal. COMMANDS TESTS CANNOT BE DONE ***");
				return;
			}
		}
		catch (InterruptedException e1) {
			e1.printStackTrace();
			errors.put("Corpus", "compute failed");
		}

		File exportResultFile = new File("concordance_result.tsv");
		File referenceResultFile = new File("concordance_reference.tsv");



		// Concordance
		try {
			Concordance p = new Concordance(voeux);
			p.setQuery(new CQLQuery("je"));
			p.setParameter(ConcordancePreferences.RIGHT_CONTEXT_SIZE, 5);
			p.setParameter(ConcordancePreferences.LEFT_CONTEXT_SIZE, 10);
			p.setParameter(ConcordancePreferences.NB_OF_ELEMENTS_PER_PAGE, 20);
			if (!p.compute(false)) {
				errors.put("Concordance", "compute failed");
			}
			else {

				if (!p.toTxt(exportResultFile, Format.CONCORDANCE)) {

					errors.put("Concordance", "export failed");
				}

				if (exportResultFile.exists() && referenceResultFile.exists()) {

					String diff = Diff.diffFile(exportResultFile, referenceResultFile);
					if (diff.length() > 0) {
						Assertions.fail("DIFF error: \n" + diff);
					}
				}
				else if (exportResultFile.exists()) {
					System.out.println("Creating reference file");
					exportResultFile.renameTo(referenceResultFile);
				}
			}

		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Concordance", e.getMessage());
		}

		if (errors.size() > 0) {
			StringBuilder buffer = new StringBuilder();
			for (String k : errors.keySet()) {
				buffer.append("**** " + k + ": " + errors.get(k) + "\n\n");
			}
			Assertions.fail("Errors: \n" + buffer.toString());
		}
		else {
			System.out.println("COMMANDS ARE OK");
		}
	}

	public static void main(String args[]) {
		new ConcordanceTest().test();
	}
}
