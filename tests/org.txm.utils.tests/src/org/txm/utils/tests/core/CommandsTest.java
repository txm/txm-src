package org.txm.utils.tests.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.Toolbox;
import org.txm.concordance.core.functions.Concordance;
import org.txm.concordance.core.preferences.ConcordancePreferences;
import org.txm.index.core.functions.Index;
import org.txm.index.core.functions.Lexicon;
import org.txm.index.core.preferences.IndexPreferences;
import org.txm.objects.Project;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.progression.core.functions.Progression;
import org.txm.progression.core.preferences.ProgressionPreferences;
import org.txm.properties.core.functions.Properties;
import org.txm.properties.core.preferences.PropertiesPreferences;
import org.txm.querycooccurrences.rcp.functions.QueryCooccurrences;
import org.txm.queryindex.core.functions.QueryIndex;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.Partition;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.Subcorpus;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.corpus.query.SubcorpusCQLQuery;
import org.txm.texts.core.functions.TextsView;
import org.txm.textsbalance.core.functions.TextsBalance;
import org.txm.utils.tests.InitializeToolboxForTests;

/**
 * TODO each command must implements its tests in its own plugin
 * 
 * @author mdecorde
 *
 */
public class CommandsTest {

	public CommandsTest() {

	}

	@Test
	public void test() {

		if (InitializeToolboxForTests.initialize() && Toolbox.workspace.getProject("VOEUX") != null) { //$NON-NLS-1$
			Toolbox.workspace.getProject("VOEUX").delete();
		}

		if (!InitializeToolboxForTests.loadTestCorpora()) {
			Assertions.fail("*** TEST CORPORA IS NOT INITIALIZED. COMMANDS TESTS CANNOT BE DONE ***"); //$NON-NLS-1$
			return;
		}

		Project voeuxp = Toolbox.workspace.getProject("VOEUX"); //$NON-NLS-1$
		if (voeuxp == null) {
			Assertions.fail("*** VOEUX corpus project not found. COMMANDS TESTS CANNOT BE DONE ***"); //$NON-NLS-1$
			return;
		}

		LinkedHashMap<String, String> errors = new LinkedHashMap<>();

		MainCorpus voeux = voeuxp.getFirstChild(MainCorpus.class);
		if (voeux == null) {
			Assertions.fail("*** VOEUX corpus not found. COMMANDS TESTS CANNOT BE DONE ***"); //$NON-NLS-1$
			return;
		}

		try {
			if (!voeux.compute()) {
				Assertions.fail("*** VOEUX corpus not functionnal. COMMANDS TESTS CANNOT BE DONE ***"); //$NON-NLS-1$
				return;
			}
		}
		catch (InterruptedException e1) {
			e1.printStackTrace();
			errors.put("Corpus", "compute failed"); //$NON-NLS-1$
		}

		// Subcorpus
		Subcorpus dg = null;
		try {
			dg = new Subcorpus(voeux);
			StructuralUnit text_struct = voeux.getStructuralUnit("text"); //$NON-NLS-1$
			StructuralUnitProperty textloc_prop = text_struct.getProperty("loc"); //$NON-NLS-1$
			dg.setQuery(new SubcorpusCQLQuery(text_struct, textloc_prop, "1 De Gaulle")); //$NON-NLS-1$
			dg.setName("DG"); //$NON-NLS-1$
			if (!dg.compute(false)) {
				errors.put("Subcorpus", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Subcorpus", e.getMessage()); //$NON-NLS-1$
		}

		// Partition
		Partition texts = null;
		try {
			texts = new Partition(voeux);
			//p.setQuery(new CQLQuery("je expand to text"));
			if (!texts.compute(false)) {
				errors.put("Partition", "compute failed");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Partition", e.getMessage()); //$NON-NLS-1$
		}

		// Properties
		try {
			Properties p = new Properties(voeux);
			p.setParameter(PropertiesPreferences.V_MAX, 10);
			if (!p.compute(false)) {
				errors.put("Properties", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Properties", e.getMessage()); //$NON-NLS-1$
		}

		// Properties DG
		try {
			Properties p = new Properties(dg);
			if (!p.compute(false)) {
				errors.put("Properties DG", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Properties DG", e.getMessage()); //$NON-NLS-1$
		}

		// Lexicon
		try {
			Lexicon p = new Lexicon(voeux);
			p.setParameter(IndexPreferences.F_MIN, 5);
			p.setParameter(IndexPreferences.F_MAX, 500);
			p.setParameter(IndexPreferences.V_MAX, 20);
			p.setParameter(IndexPreferences.NB_OF_ELEMENTS_PER_PAGE, 20);
			if (!p.compute(false)) {
				errors.put("Lexicon", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Lexicon", e.getMessage()); //$NON-NLS-1$
		}

		// Lexicon DG
		try {
			Lexicon p = new Lexicon(dg);
			if (!p.compute(false)) {
				errors.put("Lexicon DG", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Lexicon DG", e.getMessage()); //$NON-NLS-1$
		}

		// Index
		try {
			Index p = new Index(voeux);
			p.setQuery(new CQLQuery("je")); //$NON-NLS-1$
			p.setParameter(IndexPreferences.F_MIN, 5);
			p.setParameter(IndexPreferences.F_MAX, 500);
			p.setParameter(IndexPreferences.V_MAX, 20);
			p.setParameter(IndexPreferences.NB_OF_ELEMENTS_PER_PAGE, 20);
			if (!p.compute(false)) {
				errors.put("Index", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Index", e.getMessage()); //$NON-NLS-1$
		}

		// Index DG
		try {
			Index p = new Index(dg);
			p.setQuery(new CQLQuery("je")); //$NON-NLS-1$
			if (!p.compute(false)) {
				errors.put("Index DG", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Index DG", e.getMessage()); //$NON-NLS-1$
		}

		// Concordance
		try {
			Concordance p = new Concordance(voeux);
			p.setQuery(new CQLQuery("je")); //$NON-NLS-1$
			p.setParameter(ConcordancePreferences.RIGHT_CONTEXT_SIZE, 5);
			p.setParameter(ConcordancePreferences.LEFT_CONTEXT_SIZE, 10);
			p.setParameter(ConcordancePreferences.NB_OF_ELEMENTS_PER_PAGE, 20);
			if (!p.compute(false)) {
				errors.put("Concordance", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Concordance", e.getMessage()); //$NON-NLS-1$
		}

		// Concordance
		try {
			Concordance p = new Concordance(dg);
			p.setQuery(new CQLQuery("je")); //$NON-NLS-1$
			if (!p.compute(false)) {
				errors.put("Concordance DG", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Concordance DG", e.getMessage()); //$NON-NLS-1$
		}

		// Progression
		try {
			Progression p = new Progression(voeux);
			p.setQueries(new CQLQuery("je"), new CQLQuery("nous"), new CQLQuery("vous")); //$NON-NLS-1$
			p.setParameter(ProgressionPreferences.BANDE_MULTIPLIER, 1.0f);
			p.setParameter(ProgressionPreferences.CHART_CUMULATIVE, false);
			p.setParameter(ProgressionPreferences.STRUCTURAL_UNIT, voeux.getStructuralUnit("text")); //$NON-NLS-1$
			p.setParameter(ProgressionPreferences.STRUCTURAL_UNIT_PROPERTY, voeux.getStructuralUnit("text").getProperty("annee")); //$NON-NLS-1$
			if (!p.compute(false)) {
				errors.put("Progression", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Progression", e.getMessage()); //$NON-NLS-1$
		}

		// Progression DG
		try {
			Progression p = new Progression(dg);
			p.setQueries(new CQLQuery("je"), new CQLQuery("nous"), new CQLQuery("vous")); //$NON-NLS-1$
			if (!p.compute(false)) {
				errors.put("Progression DG", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("Progression DG", e.getMessage()); //$NON-NLS-1$
		}

		// Dimensions
		try {
			PartitionDimensions p = new PartitionDimensions(texts);
			p.setSortBySize(true);
			if (!p.compute(false)) {
				errors.put("PartitionDimensions", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("PartitionDimensions", e.getMessage()); //$NON-NLS-1$
		}

		// Cooccurrences
		// Lexical Table
		// CA
		// AHC
		// Specificities

		// TextsBalance
		try {
			TextsBalance p = new TextsBalance(voeux);
			if (!p.compute(false)) {
				errors.put("TextsBalance", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("TextsBalance", e.getMessage()); //$NON-NLS-1$
		}

		// TextsView
		try {
			TextsView p = new TextsView(voeux);
			if (!p.compute(false)) {
				errors.put("TextsView", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("TextsView", e.getMessage()); //$NON-NLS-1$
		}

		// QueryIndex
		try {
			QueryIndex p = new QueryIndex(voeux);
			p.addLine("je", new CQLQuery("je")); //$NON-NLS-1$
			p.addLine("nous", new CQLQuery("nous")); //$NON-NLS-1$
			p.addLine("vous", new CQLQuery("vous")); //$NON-NLS-1$
			if (!p.compute(false)) {
				errors.put("QueryIndex", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("QueryIndex", e.getMessage()); //$NON-NLS-1$
		}

		// CoocMotMot
		try {
			QueryCooccurrences p = new QueryCooccurrences(voeux);
			p.setParameters(
					new ArrayList<CQLQuery>(Arrays.asList(new CQLQuery("\"je\""), new CQLQuery("\"tu\""))), //$NON-NLS-1$
					new ArrayList<CQLQuery>(Arrays.asList(new CQLQuery("\"je\""), new CQLQuery("\"tu\""))), //$NON-NLS-1$
					10, 1, "p", "both"); //$NON-NLS-1$
			if (!p.compute(false)) {
				errors.put("QueryIndex", "compute failed"); //$NON-NLS-1$
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			errors.put("QueryIndex", e.getMessage()); //$NON-NLS-1$
		}

		if (errors.size() > 0) {
			StringBuilder buffer = new StringBuilder();
			for (String k : errors.keySet()) {
				buffer.append("**** " + k + ": " + errors.get(k) + "\n\n"); //$NON-NLS-1$
			}
			Assertions.fail("Errors: \n" + buffer.toString()); //$NON-NLS-1$
		}
		else {
			System.out.println("COMMANDS ARE OK"); //$NON-NLS-1$
		}
	}

	public static void main(String args[]) {
		new CommandsTest().test();
	}
}
