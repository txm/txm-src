package org.txm.utils.tests.core;

import java.io.File;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.libs.office.ReadODS;

public class ReadODSTest {

	static int DELAY = 100000;

	public ReadODSTest() {

	}

	@Test
	public void test() {

		// TODO use org.junit.jupiter.api.Assertions.assertTimeout(new Duration(), null);
		File testFile = new File("table.ods");
		ReadTSVTest.doTest(testFile);
	}

	@Test
	public void test2() {

		File testFile = new File("table_with_virtual_lines_cells.ods");
		ReadODS reader;
		try {
			reader = new ReadODS(testFile, "metadata");  // this takes time but should not lock the pipeline
			reader.readHeaders();
			while (reader.readRecord()) {
				reader.getRecord();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail("Error while reading: testFile=" + testFile + ": " + e);
		}

	}

	public static void main(String[] args) {
		new ReadODSTest().test();
		new ReadODSTest().test2();
	}
}

