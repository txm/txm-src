package org.txm.utils.tests.core;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.libs.office.WriteODS;

public class WriteODSTest {

	static int DELAY = 100000;

	public WriteODSTest() {

	}

	@Test
	public void test() {

		// TODO use org.junit.jupiter.api.Assertions.assertTimeout(new Duration(), null);
		try {
			WriteODS.main(null);
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail("Error while WriteODS.main(null);: testFile=" + e);
		}
	}

	@Test
	public void test2() {

		// TODO use org.junit.jupiter.api.Assertions.assertTimeout(new Duration(), null);
				try {
					WriteODS.main2(null);
				}
				catch (Exception e) {
					e.printStackTrace();
					Assertions.fail("Error while WriteODS.main(2null);: testFile=" + e);
				}

	}

	public static void main(String[] args) {
		new WriteODSTest().test();
		new WriteODSTest().test2();
	}
}

