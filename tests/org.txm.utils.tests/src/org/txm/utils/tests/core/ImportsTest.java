package org.txm.utils.tests.core;

import java.io.File;
import java.io.FileFilter;
import java.util.LinkedHashMap;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.Toolbox;
import org.txm.core.engines.ImportEngine;
import org.txm.core.engines.ImportEngines;
import org.txm.objects.Project;
import org.txm.rcp.corpuswizard.ImportWizard;
import org.txm.utils.LogMonitor;
import org.txm.utils.tests.InitializeToolboxForTests;

public class ImportsTest {

	public ImportsTest() {

	}

	@Test
	public void test() {

		File imports = new File("imports");
		if (!imports.exists()) { //
			imports = new File("/HDD800GB/HDD/GIT/txm-src/tests/org.txm.utils.tests/imports");
		}
		if (!imports.exists()) { //
			Assertions.fail("Errors: no import source directory found: " + imports.getAbsolutePath());
		}
		File[] dirs = imports.listFiles(new FileFilter() {

			@Override
			public boolean accept(File f) {
				return f.isDirectory();
			}
		});

		if (!InitializeToolboxForTests.initialize()) {
			System.out.println("*** TOOLBOX IS NOT INITIALIZED. IMPORT TESTS CANNOT BE DONE ***");
			return;
		}


		ImportEngines engines = Toolbox.getImportEngines();

		LinkedHashMap<File, String> errors = new LinkedHashMap<>();
		for (File dir : dirs) {

			try {
				ImportEngine engine = engines.getEngine(dir.getName());
				if (engine == null) continue;

				System.out.println("TEST " + engine.getName() + " WITH " + dir.getAbsolutePath());
				Project p = new Project(Toolbox.workspace, dir.getName().toUpperCase());
				if (p.getProjectDirectory().exists() && p.isOpen()) p.delete();

				//DeleteDir.deleteDirectory(p.getProjectDirectory());
				p = ImportWizard.doCreateProject(dir.getAbsoluteFile(), dir.getName().toUpperCase());
				IStatus rez = engine.build(p, new LogMonitor());
				if (!rez.equals(Status.OK_STATUS)) {
					errors.put(dir, "Import failed: " + p.getDescription() + " " + p.getDetails());
				}

			}
			catch (Throwable e) {
				errors.put(dir, e.getLocalizedMessage());
			}
		}

		if (errors.size() > 0) {
			StringBuilder buffer = new StringBuilder();
			for (File k : errors.keySet()) {
				buffer.append("**** " + k.getPath() + ": " + errors.get(k) + "\n\n");
			}
			Assertions.fail("Errors: \n" + buffer.toString());
		}
		else {
			System.out.println("IMPORTS ARE OK");
		}

		// cleaning files

	}

	public static void main(String args[]) {
		new ImportsTest().test();
	}
}
