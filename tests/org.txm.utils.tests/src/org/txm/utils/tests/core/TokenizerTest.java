package org.txm.utils.tests.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.tokenizer.SimpleStringTokenizer;

public class TokenizerTest {

	public TokenizerTest() {

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	//public void test() {
	public void test() {


		LinkedHashMap<String, LinkedHashMap<String, List<String>>> tests = new LinkedHashMap<>();

		LinkedHashMap<String, List<String>> FRtests = new LinkedHashMap<>();
		LinkedHashMap<String, List<String>> ENtests = new LinkedHashMap<>();
		LinkedHashMap<String, List<String>> DEtests = new LinkedHashMap<>();

		tests.put("fr", FRtests);
		tests.put("en", ENtests);
		tests.put("de", DEtests);

		//FRtests.put("semble-t-il", Arrays.asList("semble", "-t-", "il")); // FIXME "-il" is used instead of "-t-il"
		// FRtests.put("semble-t'il",Arrays.asList("semble", "-t'", "il"));
		//FRtests.put("numbers 2.3.3, 200 000 or 2,4",Arrays.asList("numbers", "2.3.3'", ",", "200 000", "or", "2,4")); //FIXME numbers should be tokenized before breaking by punctuation
		FRtests.put("Bar-le-Duc n'est pas le Mont-d'or", Arrays.asList("Bar-le-Duc", "n'", "est", "pas", "le", "Mont-d'or"));
		FRtests.put("mot composé assemblée_générale", Arrays.asList("mot", "composé", "assemblée_générale"));
		FRtests.put("c'est comme-ci ou comme-là", Arrays.asList("c'", "est", "comme", "-ci", "ou", "comme", "-là"));
		/**
		 * MORE tests to add
		 * La rue de la Goutte-d'Or ou la rue de la Chaussée-d'Antin.
		 * le mot est-il ?
		 * qu'il faut c'est-à-dire
		 * l'apostrophe ne pose pas d'problème !
		 * c'est "la fin" pour aujourd'hui.
		 */
		ENtests.put("I've good eyes.", Arrays.asList("I", "'ve", "good", "eyes", "."));
		ENtests.put("bloom's", Arrays.asList("bloom", "'s"));
		ENtests.put("Bloom's", Arrays.asList("Bloom", "'s"));
		//ENtests.put("Bloom's,", Arrays.asList("Bloom", "'s", ","));
		//ENtests.put("Bloom's)", Arrays.asList("Bloom", "'s", ")"));
		//ENtests.put("Bloom's:", Arrays.asList("Bloom", "'s", ":"));

		//DEtests.put("CDU/CSU-Koalition", Arrays.asList("CDU", "/", "CSU","-", "Koalition")); // FIXME
		// 

		ArrayList<String> errors = new ArrayList<String>();

		for (String lang : tests.keySet()) {
			SimpleStringTokenizer tokenizer = new SimpleStringTokenizer(lang);

			LinkedHashMap<String, List<String>> langtests = tests.get(lang);

			for (String test : langtests.keySet()) {
				List<List<String>> rez = tokenizer.processText(test);

				ArrayList<List<String>> tmp = new ArrayList<List<String>>();
				tmp.add(langtests.get(test));

				if (rez.toString().equals(tmp.toString())) {
					System.out.println("OK lang=" + lang + " test=" + test + " rez=" + rez + " == " + langtests.get(test));
				}
				else {
					System.out.println("KO lang=" + lang + " test=" + test + " rez=" + rez + " != " + langtests.get(test));
					errors.add("lang=" + lang + " test=" + test + " rez=" + rez + " shouldbe=" + langtests.get(test));
				}
			}
		}

		if (errors.size() > 0) {
			Assertions.fail("tokenizer errors: " + errors.size());
		}
	}
}
