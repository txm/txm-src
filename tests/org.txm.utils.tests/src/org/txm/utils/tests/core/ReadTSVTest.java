package org.txm.utils.tests.core;

import java.io.File;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.utils.TableReader;

public class ReadTSVTest {

	static int DELAY = 100000;

	public ReadTSVTest() {

	}

	@Test
	public void test() {

		File testFile = new File("table.tsv");
		doTest(testFile);
	}

	public static void doTest(File testFile) {
		System.out.println("Test file: " + testFile.getAbsolutePath());

		ArrayList<ArrayList<String>> data;
		try {

			data = TableReader.readAsList(testFile, "metadata");
			System.out.println("data size=" + data.size() + " first row=" + data.get(0) + " second row=" + data.get(1));


			if (data.size() != 18) Assertions.fail("Error while reading: num of rows is not 18: " + data.size());
			if (!data.get(0).toString().equals("[int, float, String]")) Assertions.fail("Error while reading: wrong first row: " + data.get(0));
			String s = "\\[1, 2.1, rien\\]";
			if (!data.get(1).toString().matches(s)) Assertions.fail("Error while reading: wrong second row: " + data.get(1) + ". Should be: " + s);

		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assertions.fail("Error while reading: " + e);
		}
	}

	public static void main(String[] args) {
		new ReadTSVTest().test();
	}
}
