package org.txm.utils.tests.core;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.Toolbox;
import org.txm.groovy.core.GSERunner;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.DeleteDir;
import org.txm.utils.tests.InitializeToolboxForTests;

import groovy.lang.Binding;

public class GSERunnerTest {

	public GSERunnerTest() {

	}

	@Test
	public void test() {

		if (!InitializeToolboxForTests.initialize()) {
			//System.out.println("*** TOOLBOX IS NOT INITIALIZED. Groovy TESTS CANNOT BE DONE ***");
			//return;
		}

		System.out.println("COMPILE ALL GROOVY SCRIPTS");

		LinkedHashMap<File, String> errors = new LinkedHashMap<>();
		File groovySrc = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/");
		System.out.println("SRC=" + groovySrc.getAbsolutePath());
		GSERunner gse = GSERunner.buildDefaultGSE(groovySrc);
		System.out.println("GSE=" + gse);
		ArrayList<File> scripts = DeleteDir.scanDirectory(groovySrc, true, true);
		System.out.println("TESTING " + scripts.size() + " scripts.");
		ConsoleProgressBar cpb = new ConsoleProgressBar(scripts.size());
		for (File scriptFile : scripts) {


			if (scriptFile.getName().endsWith("Macro.groovy")) {
				try {
					String scriptRelativePath = scriptFile.getAbsolutePath();
					scriptRelativePath = scriptRelativePath.substring(groovySrc.getAbsolutePath().length() + 1);
					//System.out.println("REL PATH="+scriptRelativePath);

					gse.createScript(scriptRelativePath, new Binding());
					//gse.run(scriptFile, new Binding());
					//System.out.println("script="+script);
				}
				catch (Throwable e) {
					String message = e.getMessage().replace("\n", "  ");
					errors.put(scriptFile, message.substring(0, Math.min(200, message.length())));
				}
			}
			cpb.tick();
		}
		cpb.done();

		if (errors.size() > 0) {
			StringBuilder buffer = new StringBuilder();
			for (File k : errors.keySet()) {
				buffer.append("**** " + k.getPath() + ": " + errors.get(k) + "\n\n");
			}
			Assertions.fail("Error compiling scripts: \n" + buffer.toString());
		}
		else {
			System.out.println("SCRIPTS COMPILATION IS OK");
		}
	}
}
