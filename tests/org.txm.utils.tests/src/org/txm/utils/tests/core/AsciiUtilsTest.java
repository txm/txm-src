package org.txm.utils.tests.core;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.utils.AsciiUtils;

public class AsciiUtilsTest {

	public AsciiUtilsTest() {

	}

	@Test
	public void test() {
		String rez = "";
		String rezok = "";

		String s = "01The result : - - _ тврьдо È,É,Ê,Ë,Û,Ù,Ï,Î,À,Â,Ô,è,é,ê,ë,û,ù,ï,î,à,â,ô,ç  0 1 2 3 4 5 6 7 8 9 10"; //$NON-NLS-1$

		rez = AsciiUtils.convertNonAscii(s);
		rezok = "01Theresult:--_tvrʹdoE,E,E,E,U,U,I,I,A,A,O,e,e,e,e,u,u,i,i,a,a,o,c012345678910";
		if (!rez.equals(rezok)) Assertions.fail("Error convertNonAscii: " + rez + ". Should be: " + rezok);

		String s2 = "w_ТВРЬДОтврьдо_123&é\"'(-è_çà)=/*-+~#{[|`\\^@]}¤;:!§/.?µ%£°"; //$NON-NLS-1$

		rez = AsciiUtils.convertNonAscii(s2);
		rezok = "w_TVRʹDOtvrʹdo_123&e\"'(-e_ca)/*-#{[\\@]};:!§/.?µ%";
		if (!rez.equals(rezok)) Assertions.fail("Error convertNonAscii: " + rez + ". Should be: " + rezok);

		rez = AsciiUtils.buildWordId(s2);
		rezok = "w_TVRDOtvrdo_123ee_ca";
		if (!rez.equals(rezok)) Assertions.fail("Error buildWordId: " + rez + ". Should be: " + rezok);

		rez = AsciiUtils.buildAttributeId(s2);
		rezok = "w-tvrdotvrdo-123e-e-ca-";
		if (!rez.equals(rezok)) Assertions.fail("Error buildAttributeId: " + rez + ". Should be: " + rezok);

	}
}
