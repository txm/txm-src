package org.txm.utils.tests.core;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.txm.utils.tests.InitializeToolboxForTests;

/**
 * Duplicate this class to create new tests
 * 
 * @author mdecorde
 *
 */
public class EmptyTest {

	public EmptyTest() {

	}

	@Test
	public void test() {

		System.out.println("Empty test: start");

		if (!InitializeToolboxForTests.initialize()) { // initialize the toolbox is necessary
			System.out.println("Empty test: aborted");
			return;
		}

		if (1 + 1 != 2) Assertions.fail("1+1 != 2 ! :-o");


		System.out.println("Empty test done");
	}
}
