# TXM platform source code repository

### Wiki

* [Home](https://gitlab.huma-num.fr/txm/txm-src/-/wikis/home)

### Specifications

* TXM
  * [TXM-0.8.4](https://gitlab.huma-num.fr/txm/txm-src/-/wikis/Roadmap/TXM-0.8.4)
* TXM Portal
  * [TXM Portal-0.7](https://gitlab.huma-num.fr/txm/txm-src/-/wikis/Roadmap/TXM-Portal-0.7)

### Milestones (issues)


* [TXM-0.8.5 WIP](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=opened&milestone_title=TXM%200.8.5&first_page_size=100)
  * [types](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1283?milestone_title=TXM%200.8.5)
  * [workflow](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1268?milestone_title=TXM%200.8.5)
  * [priorities](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1275?milestone_title=TXM%200.8.5)
  * [tests](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1642?not[label_name][]=status%3A%3Afailed&not[label_name][]=status%3A%3Adraft&not[label_name][]=status%3A%3Ain%20progress&not[label_name][]=status%3A%3Arejected&milestone_title=TXM%200.8.5)
* [TXM-0.8.4](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=opened&milestone_title=TXM%200.8.4&first_page_size=100) [TXM-0.8.4.1](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=opened&milestone_title=TXM%200.8.4.1&first_page_size=100)
  * [types](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1283?milestone_title=TXM%200.8.4)
  * ~~[workflow](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1268?milestone_title=TXM%200.8.4)~~ [workflow](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1268?milestone_title=TXM%200.8.4.1)
  * [priorities](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1275?milestone_title=TXM%200.8.4)
  * [tests](https://gitlab.huma-num.fr/txm/txm-src/-/boards/1642?not[label_name][]=status%3A%3Afailed&not[label_name][]=status%3A%3Adraft&not[label_name][]=status%3A%3Ain%20progress&not[label_name][]=status%3A%3Arejected&milestone_title=TXM%200.8.4)
* [TXM-0.8.3](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=opened&milestone_title=TXM%200.8.3&first_page_size=100)
* [TXM-0.8.2](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=opened&milestone_title=TXM%200.8.2&first_page_size=100)
* [TXM-0.8.1](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=opened&milestone_title=TXM%200.8.1&first_page_size=100)
* [TXM-0.8.0](https://gitlab.huma-num.fr/txm/txm-src/-/issues/?sort=label_priority&state=opened&milestone_title=TXM%200.8.0&first_page_size=100)

## Roadmaps

- [TXM 0.8.4](https://gitlab.huma-num.fr/txm/txm-src/-/wikis/Roadmap/TXM-0.8.4)

### Sources direct access (.java, .groovy)

* [macros](https://gitlab.huma-num.fr/txm/txm-src/-/tree/main/bundles/org.txm.groovy.core/src/groovy/org/txm/macro)
* [utils](https://gitlab.huma-num.fr/txm/txm-src/-/tree/main/bundles/org.txm.utils.core/src/org/txm/utils)