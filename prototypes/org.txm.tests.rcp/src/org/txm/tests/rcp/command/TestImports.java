// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
// 
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
// 
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
// 
// 
// 
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$ 
//
package org.txm.tests.rcp.command;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;
import org.txm.Toolbox;
import org.txm.objects.Project;
import org.txm.rcp.JobsTimer;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.swt.dialog.LastOpened;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.zip.Zip;

public class TestImports extends AbstractHandler {
	
	public static String ID = "org.txm.rcp.commands.tests.TestImports";
	
	static HashMap<String, String> scripts = new HashMap<String, String>();
	static {
		scripts.clear();
		scripts.put("cnr", "discoursLoader.groovy"); // cnr +csv
		scripts.put("hyperbase", "hyperbaseLoader.groovy"); // old hyperbase
		scripts.put("alceste", "alcesteLoader.groovy"); // alceste
		scripts.put("alceste2", "alcesteLoader.groovy"); // alceste
		scripts.put("bfm", "bfmLoader.groovy"); // xml-tei-bfm
		scripts.put("frantext", "frantextLoader.groovy"); // xml-tei-bfm
		scripts.put("xml", "xmlLoader.groovy"); // xml
		scripts.put("factiva", "factivaLoader.groovy"); // factiva
		scripts.put("xml", "xmlLoader.groovy"); // <w attr="" atrt2="">
		scripts.put("xmlws", "xmlLoader.groovy"); // <w attr="" atrt2="">
		scripts.put("txt", "txtLoader.groovy"); // txt + csv
		scripts.put("transcriber", "transcriberLoader.groovy"); // echant EVS +
		// csv
		scripts.put("xmltxm", "xmltxmLoader.groovy"); // domi
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String txmhome = Toolbox.getTxmHomePath();
		final File scriptsdir = new File(txmhome, "scripts/import");
		// final File sourcesdir = new
		// File(System.getProperty("user.home"),"xml/TESTS");
		final int delta = 100 / scripts.size();
		
		System.out
		.println("Please select the directory containing source directories for the following import modules :");
		System.out.println(scripts.toString());
		
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event)
				.getShell();
		DirectoryDialog dialog = new DirectoryDialog(shell, SWT.OPEN);
		
		if (LastOpened.getFile(ID) != null) {
			dialog.setFilterPath(LastOpened.getFolder(ID));
		} else {
			dialog.setFilterPath(System.getProperty("user.home"));
		}
		String path = dialog.open();
		
		if (path == null)
			return null;
		
		final File sourcesdir = new File(path);
		
		if (!scriptsdir.exists()) {
			System.out.println("Error: TXM is not well installed.");
			System.out.println("Cannot find TXM import scripts directory: "
					+ scriptsdir);
			return null;
		}
		
		if (!sourcesdir.exists()) {
			System.out.println("Cannot find test sources directory: "
					+ sourcesdir);
			return null;
		}
		
		System.out
		.println("Select the directory containing the source directories : "
				+ scripts.keySet());
		JobHandler jobhandler = new JobHandler(
				"Testing TXM - select the directory containing the source dierctories") {
			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {
				
				String txmhome = Toolbox.getTxmHomePath();
				File outDir = new File(txmhome, "results/TESTS-imports");
				outDir.mkdirs();
				ArrayList<File> missings = new ArrayList<File>();
				
				monitor.beginTask("Testing imports", 100);
				JobsTimer.start();
				
				for (String filename : scripts.keySet()) {
					final File source = new File(sourcesdir, filename);
					final File script = new File(scriptsdir,
							scripts.get(filename));
					
					if (source.exists()) {
						
						Project project = new Project(Toolbox.workspace, "TEST-"+source.getName().substring(0, 3).toUpperCase());
						project.setSourceDirectory(source.getAbsolutePath());
						project.setImportModuleName(filename);//$NON-NLS-1$
						System.out.println("USING ROOT DIR : "
								+ project.getSrcdir());
						this.syncExec(new Runnable() {
							public void run() {
								
								try {
									project.compute();
									
									ExecuteImportScript.executeScript(project);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});
					} else {
						System.out.println("Warning: Missing source directory: " + source);
						missings.add(source);
					}
					monitor.worked(delta);
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;
				}
				
				String date = Toolbox.dateformat.format(new java.util.Date());
				File zipFile = new File(outDir.getParentFile(), "TESTS_imports_" + date + ".zip");
				File zipFile2 = new File(outDir.getParentFile(), "TESTS_imports_.odt");
				this.acquireSemaphore();
				zipFile.delete();
				zipFile2.delete();
				this.releaseSemaphore();
				if (missings.size() > 0) {
					System.out.println("Missing sources directories: " + missings);
				}
				monitor.beginTask("Testing imports: results will be compressed in " + zipFile.getAbsolutePath(), 100);
				System.out.println("Testing imports: results will be compressed in " + zipFile.getAbsolutePath());
				
				this.acquireSemaphore();
				Zip.compress(outDir, zipFile2);
				zipFile2.renameTo(zipFile);
				this.releaseSemaphore();
				return Status.OK_STATUS;
			}
		};
		
		jobhandler.startJob();
		return null;
	}
}
