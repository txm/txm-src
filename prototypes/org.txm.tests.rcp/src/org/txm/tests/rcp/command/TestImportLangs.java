// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.tests.rcp.command;

import java.io.File;
import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.txm.Toolbox;
import org.txm.objects.Project;
import org.txm.rcp.JobsTimer;
import org.txm.rcp.handlers.scripts.ExecuteImportScript;
import org.txm.rcp.utils.JobHandler;
import org.txm.utils.io.IOUtils;

public class TestImportLangs extends AbstractHandler {
	
	public static String ID = "org.txm.rcp.commands.tests.TestImports";
	
	static HashMap<String, String> scripts = new HashMap<String, String>();
	static {
		scripts.clear();
		scripts.put("xml", "xmlLoader.groovy"); // xml	
		scripts.put("txt", "txtLoader.groovy"); // txt + csv
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String txmhome = Toolbox.getTxmHomePath();
		final File scriptsdir = new File(txmhome, "scripts/import");
		
		System.out.println("Select the directory containing the source directories : "
				+ scripts.keySet());
		JobHandler jobhandler = new JobHandler(
				"Testing TXM - langs") {
			@Override
			protected IStatus _run(SubMonitor monitor) throws Exception {
				
				final File sourcesdir = new File("/tmp/test-langs");
				sourcesdir.mkdirs();
				
				if (!scriptsdir.exists()) {
					System.out.println("Error: TXM is not well installed.");
					System.out.println("Cannot find TXM import scripts directory: "
							+ scriptsdir);
					return null;
				}
				
				if (!sourcesdir.exists()) {
					System.out.println("Cannot find test sources directory: "
							+ sourcesdir);
					return null;
				}
				
				monitor.beginTask("Testing imports", 100);
				JobsTimer.start();
				
				importXMLCorpus(this, sourcesdir, scriptsdir);
				importTXTCorpus(this, sourcesdir, scriptsdir);
				
				return Status.OK_STATUS;
			}
		};
		
		jobhandler.startJob();
		return null;
	}
	
	protected boolean importXMLCorpus(JobHandler jobhandler, File sourcesdir, File scriptsdir) throws Exception {
		final File source = new File(sourcesdir, "XMLLANGTEST");
		source.mkdirs();
		File oneTextFile = new File(source, "file.xml");
		IOUtils.write(oneTextFile, "<text>This corpus works fine. That's all.</text>");
		final File script = new File(scriptsdir, scripts.get("xml"));
		
		final Project project = new Project(Toolbox.workspace, "TESTXML");
		project.setSourceDirectory(source.getAbsolutePath());
		project.setLang("en");
		project.setAnnotate(true); //$NON-NLS-1$
		project.setImportModuleName("xml");//$NON-NLS-1$
		project.compute();
		
		System.out.println("USING ROOT DIR : "+ project.getSrcdir());
		jobhandler.syncExec(new Runnable() {
			public void run() {
				new ExecuteImportScript();
				JobHandler job = ExecuteImportScript
						.executeScript(project);
				try {
					job.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		return true;	}
	
	protected boolean importTXTCorpus(JobHandler jobhandler, File sourcesdir, File scriptsdir) throws Exception {
		final File source = new File(sourcesdir, "TXTLANGTEST");
		source.mkdirs();
		File oneTextFile = new File(source, "file.txt");
		IOUtils.write(oneTextFile, "<text>C'est !C'est encore une phrase qui ne s'tokenize pas bien. Mais c'est pas drave.");
		
		final File script = new File(scriptsdir, scripts.get("txt"));
		
		final Project project = new Project(Toolbox.workspace, "TESTTXT");
		project.setSourceDirectory(source.getAbsolutePath());
		project.setLang("fr");
		project.setAnnotate(true); //$NON-NLS-1$
		project.setImportModuleName("txt");//$NON-NLS-1$
		project.compute();
		
		System.out.println("USING ROOT DIR : "+ project.getSrcdir());
		jobhandler.syncExec(new Runnable() {
			public void run() {
				new ExecuteImportScript();
				JobHandler job = ExecuteImportScript
						.executeScript(project);
				try {
					job.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		return true;
	}
}
