package org.txm.tests.rcp.editor;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.txm.Toolbox;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.handlers.files.EditFile;
import org.txm.scripts.macro.tests.Test;
import org.txm.scripts.macro.tests.TestBench;

public class TestsEditor extends EditorPart {

	private TestBench bench;
	private Composite parent;
	private HashSet<TestComposite> testComposites = new HashSet<TestComposite>();

	public TestsEditor() {
		File dir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/scripts/tests");
		dir.mkdirs();
		bench = new TestBench(dir);
	}

	@Override
	public void doSave(IProgressMonitor monitor) { }

	@Override
	public void doSaveAs() { }

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		this.setSite(site);
		this.setInput(input);
		this.setPartName("Tests");
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite aparent) {
		this.parent = aparent;
		
		GridLayout glayout = new GridLayout(8, false);
		parent.setLayout(glayout);
		
		
		ToolBar bar = new ToolBar(parent, SWT.HORIZONTAL);
		GridData gdata = new GridData(GridData.FILL, GridData.CENTER, true, false, 8, 1);
		bar.setLayoutData(gdata);
		
		ToolItem addItem = new ToolItem(bar, SWT.PUSH);
		addItem.setText("New test");
		addItem.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				InputDialog d = new InputDialog(e.display.getActiveShell(), "Enter test Name", "Test name", "test", null);
				if (d.open() == InputDialog.OK) {
					String name = d.getValue();
					if (name != null && name.length() > 0) {
						name = name.toLowerCase();
						if (bench.getTest(name) == null) {
							Test test = bench.newTest(name);
							new TestComposite(test);
							parent.layout();
							parent.getParent().layout();
						}
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		ToolItem resetAllItem = new ToolItem(bar, SWT.PUSH);
		resetAllItem.setText("Reset all");
		resetAllItem.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				for (Test test : bench.getTests().values()) {
					try {
						test.reset();
					} catch(Exception ex) {
						ex.printStackTrace();
					}
				}
				
				for (TestComposite tc : testComposites) {
					tc.refresh();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		ToolItem runAllItem = new ToolItem(bar, SWT.PUSH);
		runAllItem.setText("Run all");
		runAllItem.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				for (Test test : bench.getTests().values()) {
					try {
						test.run(0);
					} catch(Exception ex) {
						ex.printStackTrace();
					}
				}
				
				for (TestComposite tc : testComposites) {
					tc.refresh();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		new Label(parent, SWT.NONE).setText("");
		new Label(parent, SWT.NONE).setText("Create");
		new Label(parent, SWT.NONE).setText("edit");
		new Label(parent, SWT.NONE).setText("Export");
		new Label(parent, SWT.NONE).setText("edit");
		new Label(parent, SWT.NONE).setText("Test");
		new Label(parent, SWT.NONE).setText("edit");
		new Label(parent, SWT.NONE).setText(" ");
		
		for (Test test : bench.getTests().values()) {
			test.reset();
			TestComposite tc = new TestComposite(test);
			testComposites.add(tc);
		}
	}

	@Override
	public void setFocus() { }

	public class TestComposite {
		
		Label nameLabel;
		Button runCreateButton, runExportButton, runTestButton;
		Button editCreateButton, editExportButton, editTestButton;
		Button deleteTestButton;
		
		Test test;
		
		public TestComposite(Test atest) {
			
			this.test = atest;
			
			nameLabel = new Label(parent, SWT.NONE);
			nameLabel.setText(test.getName());
			
			
			runCreateButton = new Button(parent, SWT.PUSH);
			runCreateButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					test.run(0);
					refresh();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			editCreateButton = new Button(parent, SWT.PUSH);
			editCreateButton.setText("...");
			editCreateButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					File newfile = new File(test.getDirectory(), "create.groovy");
					try {
						newfile.createNewFile();
					} catch (IOException e1) { }
					EditFile.openfile(newfile);
					refresh();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			
			
			runExportButton = new Button(parent, SWT.PUSH);
			runExportButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					test.run(1);
					refresh();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			editExportButton = new Button(parent, SWT.PUSH);
			editExportButton.setText("...");
			editExportButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					File newfile = new File(test.getDirectory(), "export.groovy");
					try {
						newfile.createNewFile();
					} catch (IOException e1) { }
					EditFile.openfile(newfile);
					refresh();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			
			runTestButton = new Button(parent, SWT.PUSH);
			runTestButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					test.run(2);
					refresh();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			editTestButton = new Button(parent, SWT.PUSH);
			editTestButton.setText("...");
			editTestButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					File newfile = new File(test.getDirectory(), "test.groovy");
					try {
						newfile.createNewFile();
					} catch (IOException e1) { }
					EditFile.openfile(newfile);
					refresh();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			
			
			deleteTestButton = new Button(parent, SWT.PUSH);
			deleteTestButton.setImage(IImageKeys.getImage(IImageKeys.ACTION_DELETE));
			deleteTestButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					nameLabel.dispose();
					runCreateButton.dispose();
					runExportButton.dispose();
					runTestButton.dispose();
					editCreateButton.dispose();
					editExportButton.dispose();
					editTestButton.dispose();
					deleteTestButton.dispose();
					
					bench.deleteTest(test.getName());
					
					parent.layout(true);
					
					testComposites.remove(TestComposite.this);
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) { }
			});
			refresh();
		}
		
		public void refresh() {
			runCreateButton.setEnabled(test.hasCreateStep());
			runCreateButton.setText(test.isCreateStepOK()?" OK ":"NOPE");
			
			runExportButton.setEnabled(test.hasExportStep());
			runExportButton.setText(test.isExportStepOK()?" OK ":"NOPE");
			
			runTestButton.setEnabled(test.hasTestStep());
			runTestButton.setText(test.isTestStepOK()?" OK ":"NOPE");
		}
	}
}
