package org.txm.scripts.macro.tests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.txm.utils.DeleteDir;

public class TestBench {
	HashMap<String, Test> tests = new HashMap<String, Test>();
	
	private File dir;
	
	public TestBench(File dir) {
		this.dir = dir;
		if (dir.exists()) {
			for (File testDir : dir.listFiles()) {
				if (new File(testDir, "create.groovy").exists()) {
					String name = testDir.getName();
					tests.put(name, new Test(dir, name));
				}
			}
		}
	}
	
	public HashMap<String, Test> getTests() {
		return tests;
	}
	
	public Test getTest(String name) {
		return tests.get(name);
	}
	
	public Test newTest(String name) {
		
		File testDir = new File(dir, name);
		testDir.mkdir();
		File create = new File(testDir, "create.groovy");
		try {
			create.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		tests.put(name, new Test(dir, name));
		return tests.get(name);
	}
	
	public Test deleteTest(String name) {
		
		File testDir = new File(dir, name);
		DeleteDir.deleteDirectory(testDir);
		
		return tests.remove(name);
	}
}
