package org.txm.scripts.macro.tests

@Field @Option(name="name", usage="The test name to run", widget="String", required=true, def="test1")
String name

@Field @Option(name="step", usage="step to execute, 0=all 1=export&test 2=test", widget="Integer", required=true, def="0")
int step

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

if (step < 0) step = 0;
if (step > 3) step = 2;

TestBench bench = GroovyTestBench.getTests();
Test test = bench.getTest(name)

if (test.run(step)) {
	println "$name -> OK see "+test.getLogs()
} else {
	println "$name -> KO see "+test.getLogs()
}