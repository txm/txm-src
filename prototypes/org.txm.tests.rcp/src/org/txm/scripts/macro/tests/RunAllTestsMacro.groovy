package org.txm.scripts.macro.tests

@Field @Option(name="step", usage="step to execute, 0=all 1=export&test 2=test", widget="Integer", required=true, def="0")
int step

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

if (step < 0) step = 0;
if (step > 3) step = 2;

TestBench bench = GroovyTestBench.getTests();
for (Test test : bench.getTests().values()) {
	String name = test.getName()
	if (test.run(step)) {
		println "$name -> OK"
	} else {
		println "$name -> KO"
	}
}