package org.txm.scripts.macro.tests;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.txm.groovy.core.GSERunner;

import groovy.lang.Binding;

public class Test {

	String name;
	File directory;
	File create, export, test;
	File create_ok, export_ok, test_ok;
	File create_error, export_error, test_error;
	File[] all = {create_ok, export_ok, test_ok, create_error, export_error, test_error};
	
	private Binding binding;

	public Test(File scriptDir, String name) {
		this.name = name;
		//File scriptDir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/tests");
		scriptDir.mkdirs();

		this.directory = new File(scriptDir, name);
		this.directory.mkdirs();
		
		create = new File(directory, "create.groovy");
		export = new File(directory, "export.groovy");
		test = new File(directory, "test.groovy");

		create_ok = new File(directory, "create.ok");
		export_ok = new File(directory, "export.ok");
		test_ok = new File(directory, "test.ok");

		create_error = new File(directory, "create.error");
		export_error = new File(directory, "export.error");
		test_error = new File(directory, "test.error");
	}
	
	public void reset() {
		create_ok.delete();
		export_ok.delete();
		test_ok.delete();

		create_error.delete();
		export_error.delete();
		test_error.delete();
	}
	
	public List<File> getLogs() {
		ArrayList<File> logs = new ArrayList<File>();
		for (File f : all) {
			if (f.exists()) {
				logs.add(f);
			}
		}
	
		return logs;
	}

	/**
	 * 
	 * @param start_step 0 starting step : all=create&export&test export=export&test test=step
	 * @return
	 */
	public boolean run(int start_step) {
		GSERunner gse = GSERunner.buildDefaultGSE();
		System.out.println("Run "+name+" from "+start_step);
		this.binding = new Binding();
		try {
			if (start_step <= 0) {
				create_ok.delete();
				create_error.delete();
				Object ret_create = gse.run(create, binding);

				if (ret_create != null && Boolean.FALSE != ret_create) {
					binding.setProperty("results", ret_create);
					create_ok.createNewFile();
				} else {
					create_error.createNewFile();
					return false;
				}
			}

			if (export.exists() && start_step <= 1) {
				export_ok.delete();
				export_error.delete();
				Object ret_export = gse.run(export, binding);
				if (ret_export != null && Boolean.FALSE != ret_export) {
					export_ok.createNewFile();
				} else {
					create_error.createNewFile();
					return false;
				}

				if (test.exists() && start_step <= 2) {
					test_ok.delete();
					test_error.delete();
					Object ret_test = gse.run(test, binding);
					if (ret_test != null && Boolean.FALSE != ret_test) {
						test_ok.createNewFile();
					} else {
						test_error.createNewFile();
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	public boolean hasCreateStep() {
		return create.exists();
	}

	public boolean hasExportStep() {
		return export.exists();
	}

	public boolean hasTestStep() {
		return test.exists();
	}

	public boolean isCreateStepOK() {
		return create_ok.exists();
	}

	public boolean isExportStepOK() {
		return export_ok.exists();
	}

	public boolean isTestStepOK() {
		return test_ok.exists();
	}

	public File getCreateStepScript() {
		return create;
	}

	public File getCreateStepLog() {
		if (isCreateStepOK()) return create_ok;
		return create_error;
	}

	public File getExportStepScript() {
		return export;
	}

	public File getExportStepLog() {
		if (isExportStepOK()) return export_ok;
		return export_error;
	}
	
	public File getTestStepScript() {
		return test;
	}

	public File getTestStepLog() {
		if (isTestStepOK()) return test_ok;
		return test_error;
	}

	public String getName() {
		return name;
	}

	public File getDirectory() {
		return directory;
	}
}
