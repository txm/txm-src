package org.txm.scripts.macro.tests

@Field @Option(name="name", usage="The test name to create", widget="String", required=true, def="test1")
String name

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

TestBench bench = GroovyTestBench.getTests();
Test test = bench.getTest(name)

if (test != null) {
	println "Already created."
} else {
	bench.newTest(name)
}