package org.txm.scripts.macro.tests

@Field @Option(name="name", usage="The test name to delete", widget="String", required=true, def="test1")
String name

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

TestBench bench = GroovyTestBench.getTests();
Test test = bench.getTest(name)

if (test != null) {
	bench.deleteTest(test.getName())
} else {
	println "No test with name=$name to delete."
}