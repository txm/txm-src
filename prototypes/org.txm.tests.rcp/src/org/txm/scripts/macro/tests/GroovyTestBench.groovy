package org.txm.scripts.macro.tests

/**
 * Stores the Tests results
 * 
 * @author mdecorde
 *
 */
public class GroovyTestBench {
	
	private static TestBench bench = null;
	
	public TestBench getTests() {
		if (bench != null) return bench;
		
		File dir = new File(Toolbox.getTxmHomePath(), "scripts/groovy/user/org/txm/scripts/tests");
		dir.mkdirs();
		bench = new TestBench(dir);
		
		return bench;
	}
}
