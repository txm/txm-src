package org.txm.scripts.macro.tests;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

//import org.tmatesoft.svn.core.SVNDirEntry;
//import org.tmatesoft.svn.core.SVNURL;
//import org.tmatesoft.svn.core.io.SVNRepository;
//import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
//import org.tmatesoft.svn.core.wc.SVNClientManager;
//import org.tmatesoft.svn.core.wc.SVNInfo;
//import org.tmatesoft.svn.core.wc.SVNRevision;
//import org.tmatesoft.svn.core.wc.SVNWCClient;
import org.txm.utils.io.IOUtils;

/**
 * Write the qualifier in build.properties of all plugins from the SVN revision.
 * @author sjacquot
 *
 */
public class WriteQualifierInBuildPropertiesFromSVN {

	public static File workspace = new File("../"); // SJ

	public static int updatedFilesCount = 0;
	
	/**
	 * fetch  the org.txm projects that ends with core or rcp
	 * @return
	 */
	protected static ArrayList<File> getProjects() {
		ArrayList<File> ret = new ArrayList<File>();
		if (!workspace.exists()) {
			System.err.println("ERROR: "+workspace+" does not exists. Set it in TXMPluginTest.java.");
			return ret;
		} 

		ret.addAll(Arrays.asList(workspace.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathName) {
				return pathName.isDirectory() &&
						(pathName.getName().endsWith(".rcp")
								|| pathName.getName().endsWith(".core")
								|| pathName.getName().endsWith(".feature")
								);
			}
		})));

		Collections.sort(ret);
		return ret;
	}
	
	public static void main(String[] args) {
		ArrayList<File> projectsList = getProjects();
		for (int i = 0; i < projectsList.size(); i++) {
			writeQualifierInBuildProperties(projectsList.get(i));
		}
		
		System.out.println("Done. " + updatedFilesCount + " build.properties files have been updated.");
	}

	
	
	public static void writeQualifierInBuildProperties(File projectDirectory) {
		/*
		try {
		
			SVNWCClient client = SVNClientManager.newInstance().getWCClient();
			SVNInfo info = client.doInfo(projectDirectory, SVNRevision.WORKING);
			SVNURL location = info.getURL();
			SVNRepository repository = SVNRepositoryFactory.create(location);
			
//			long latestRevision = repository.getLatestRevision();
//			System.out.println("[" + location.toString() + "] latest revision: " + latestRevision); 


			SVNDirEntry entry = repository.info(".", -1);
//			System.out.println("[" + location.toString() + "] latest revision: " + entry.getRevision());
//			System.out.println("[" + location.toString() + "] latest date: " + entry.getDate());
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd-HHmm");
			//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
			String date = formatter.format(entry.getDate());
			System.out.println("[" + location.toString() + " / " + projectDirectory + "] latest date: " + date);
//			System.err.println("formatted date: " + format);
			
//			SvnOperationFactory operationFactory = new SvnOperationFactory();
//			SvnLog logOperation = operationFactory.createLog();
//			logOperation.setSingleTarget(SvnTarget.fromFile( projectDirectory )			);
//			logOperation.setRevisionRanges( Collections.singleton(
//			        SvnRevisionRange.create(
//			                SVNRevision.create( 1 ),
//			                SVNRevision.HEAD
//			        )
//			) );
//			Collection<SVNLogEntry> logEntries = logOperation.run( null );
//			System.out.println( "logEntries = " + logEntries );
			
			
			File buildFile = new File(projectDirectory, "build.properties");
			
			if (!buildFile.exists()) {
				System.err.println("No build.properties file in: " + projectDirectory);
				return;
			}
			
			Properties props = new Properties();
			props.load(IOUtils.getReader(buildFile));

			// Debug: deleting all qualifiers
//			props.remove("qualifier");
//			props.store(new FileOutputStream(buildFile), null);
			
			// Write qualifier
			String oldDate = props.getProperty("qualifier");
			if(oldDate == null || !oldDate.equals(date))	{
				
				if(oldDate == null)	{
					oldDate = "none";
				}
				
				updatedFilesCount++;
				System.out.println("Setting qualifier from " + oldDate + " to " + date);
				props.setProperty("qualifier", date);
				props.store(new FileOutputStream(buildFile), null);
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
}