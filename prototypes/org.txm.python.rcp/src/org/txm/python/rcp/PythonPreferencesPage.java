package org.txm.python.rcp;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.python.core.PythonPreferences;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;

/**
 * Index preferences page.
 * 
 * @author mdecorde
 *
 */
public class PythonPreferencesPage extends TXMPreferencePage {

	@Override
	public void init(IWorkbench workbench) {

		this.setPreferenceStore(new TXMPreferenceStore(PythonPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle("Python");
		this.setImageDescriptor(IImageKeys.getImageDescriptor("org.txm.python.rcp", "icon/python.png"));
	}

	@Override
	protected void createFieldEditors() {

		this.addField(new BooleanFieldEditor(PythonPreferences.USE_JYTHON, "Use jython instead of local python", this.getFieldEditorParent()));

		this.addField(new StringFieldEditor(PythonPreferences.HOME, "HOME", this.getFieldEditorParent()));

		this.addField(new StringFieldEditor(PythonPreferences.EXECUTABLE_NAME, "PYTHON executable path", this.getFieldEditorParent()));

		this.addField(new StringFieldEditor(PythonPreferences.ADDITIONAL_PARAMETERS, "PYTHON executable additional parameters", this.getFieldEditorParent()));
	}
}
