// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
// Copyright © 2007-2010 ENS de Lyon, CNRS, INRP, University of
// Lyon 2, University of Franche-Comté, University of Nice
// Sophia Antipolis, University of Paris 3.
//
// The TXM platform is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 2 of the License, or (at your option) any
// later version.
//
// The TXM platform is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General
// Public License along with the TXM platform. If not, see
// http://www.gnu.org/licenses.
//
//
//
// $LastChangedDate:$
// $LastChangedRevision:$
// $LastChangedBy:$
//
package org.txm.translationassist.rcp.handlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Widget;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.utils.SWTEditorsUtils;
import org.txm.utils.logger.Log;

/**
 * Displays all the tooltips of the current active TXMEditor for translation and verification purpose.
 * Press ESC to close the tooltip shell layer.
 *
 * @author sjacquot
 *
 */
//TODO: SJ: improve tooltips overlapping management
//TODO: SJ: the code has been moved from TXMEditor. Need to manage an init phase to create only one unique shell?
public class DisplayAllEditorTooltips extends AbstractHandler implements IHandler2 {

//	public static final String ID = "DisplayAllEditorTooltips"; //$NON-NLS-1$


	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		TXMEditor editor = (TXMEditor) SWTEditorsUtils.getActiveEditor(event);
		
		if(editor != null) {
			
//			Button source = (Button) event.getSource();
			
			Shell shell = new Shell(editor.getParent().getShell(),
					//SWT.ON_TOP
					//| SWT.TOOL | SWT.NO_FOCUS
					//|
					SWT.NO_TRIM
			//    			| SWT.TRANSPARENT

			);

			//shell.setBackgroundMode(SWT.TRANSPARENT);
			shell.setFullScreen(true);
			shell.setLayout(new FillLayout());
			shell.setAlpha(200);
			final Canvas canvas = new Canvas(shell,
					//SWT.NO_REDRAW_RESIZE |
					SWT.NO_BACKGROUND | SWT.TRANSPARENT);


			boolean isInitialized = false;

			int maxPrecisionYOffset = 20;
			

			int baseY = editor.getResultArea().getLocation().y;
			int maxY = baseY + editor.getResultArea().getSize().y;

			// init
			if (!isInitialized) {

				
				
				// first pass, flatten the tree and create the coordinates
				ArrayList<Widget> tmpQueue = new ArrayList<Widget>();
				ArrayList<Widget> widgets = new ArrayList<Widget>();

				tmpQueue.add(editor.getParent());

				while (tmpQueue.size() > 0) {

					// pop
					Widget currentNode = tmpQueue.get(0);
					tmpQueue.remove(0);

					if (
							//currentNode != showAllToolTipsButton && 
							currentNode instanceof Control control && control.isVisible() && control.getToolTipText() != null) {

						// get and store the center coordinates
						Point widgetPos = control.toDisplay(new Point(0, 0));
						widgetPos.x += control.getSize().x / 2;
						widgetPos.y += control.getSize().y / 2;
						control.setData(widgetPos);

						widgets.add(control);

					}
					
					// trying to push Items with their tooltips
					//FIXME: SJ: how to get the position of the item/widget in screen coordinates to draw the line and the tooltip?
					if (currentNode instanceof ToolBar toolbar) {
						for (int i = 0; i < toolbar.getItemCount(); i++) {
							tmpQueue.add(toolbar.getItem(i));
							//System.err.println("------------- TXMEditor.___initTestShowAllTooltips(): " + toolbar.getItem(i).getControl());

						}
					}
					// push children
					if (currentNode instanceof Composite composite) {
						for (int i = 0; i < composite.getChildren().length; i++) {
							tmpQueue.add(composite.getChildren()[i]);
						}
					}


				}


				// sort by y and x coordinates (sort on y and if two widgets has approximatively the same y then sort them on x)
				Comparator<Widget> comparator = new Comparator<Widget>() {

					@Override
					public int compare(Widget widgetA, Widget widgetB) {

						if (widgetA instanceof Control controlA && widgetB instanceof Control controlB) {

							Point widgetPosA = (Point) controlA.getData();
							Point widgetPosB = (Point) controlB.getData();

							// manage a pixels precision factor y offset
							int yComparison = 0;
							
							for (int tmpY = maxPrecisionYOffset; tmpY >= -maxPrecisionYOffset; tmpY--) {

								int yWithOffset = widgetPosB.y + tmpY;

								//Log.finest("TXMEditor.createPartControl(): test pixel precision y offset for widget A y = " + yToFind + " and widget B y = " + yWithOffset);
								yComparison = Integer.compare(widgetPosA.y, yWithOffset);

								if (yComparison == 0) {
									
									// Debug
//									Log.finest("TXMEditor.createPartControl(): comparison found for Widget B tooltip  = " + controlB.getToolTipText() + " / Widget A tooltip = " + controlA.getToolTipText());
//									Log.finest("TXMEditor.createPartControl(): comparison found for offset widget B y = " + yWithOffset + " and original y = " + widgetPosB.y + " / Widget A y = " + widgetPosA.y);
									
									
									widgetPosB.y = yWithOffset;
									break;
								}
							}

							if (yComparison != 0) {
								return yComparison;
							}
							// case of same y, manage order on x coordinate to later browse the widgets that have same y on ascending x
							else {
								return Integer.compare(widgetPosA.x, widgetPosB.x);
							}
						}
						else {
							return 0;
						}

					}
				};


				// second pass, sort on y and if two widgets has approximatively the same y then sort them on x					
				Collections.sort(widgets, comparator);

				
				// Canvas tests
				canvas.addPaintListener(new PaintListener() {

					@Override
					public void paintControl(PaintEvent e) {

						// to manage positions overriding, to group widgets by y coordinate and keep track of
						// FIXME: SJ: need to implement a real 2D bounding distribution algo 
						HashMap<Integer, ArrayList<Widget>> widgetsByApproximativeY = new HashMap();

						Log.finest("TXMEditor.___initTestShowAllTooltips(): number of widgets: " + widgets.size());
						
						for (int i = 0; i < widgets.size(); i++) {

							Widget currentNode = widgets.get(i);

							System.err.println("**** TXMEditor.createPartControl(): processing element: " + currentNode);

							// manage Control type (should manage ToolItem too)
							if (currentNode instanceof Control control) {

								Point widgetPos = (Point) control.getData();
								//widgetPos.x += control.getSize().x / 2;
								//widgetPos.y += control.getSize().y / 2;

								Point toolTipPos = new Point(widgetPos.x, widgetPos.y);
								control.setToolTipText(String.valueOf(i) + " - " + control.getToolTipText());
								//control.setToolTipText(control.getToolTipText());
								Point textSize = e.gc.textExtent(control.getToolTipText());
								// save left border x tooltip coordinate
								toolTipPos.x -= textSize.x / 2;

								Log.finest("TXMEditor.createPartControl(): tooltip = " + control.getToolTipText());
								//System.err.println("ChartEditor._createPartControl(): location " + control.getLocation());
								//Log.finest("TXMEditor.createPartControl(): location " + control.toDisplay(new Point(0,0)));
								//Log.finest("TXMEditor.createPartControl(): center y " + toolTipPos.y);
								//Log.finest("TXMEditor.createPartControl(): size for text: " + toolTipText + " is " + textSize);


//								int lastRightXPosOnSameY = 0;
//								int numberOfTooltipsOnSameY = 0;

								ArrayList<Widget> previousWidgetsOnY = null;

								// baseY to draw tooltip in the result area rather than the parameters and toolbar areas for better readability
								int yOffset = baseY;

								// for bottom toolbar
								if (toolTipPos.y > maxY) {
									yOffset = 20;
								}

								
								// check if a previous widget has the same approximative y coordinate
								//System.err.println("TXMEditor.createPartControl(): testing if previous tooltip exist for y = " + widgetPos.y);
								boolean approximativeYExist = false;
								// manage a pixels precision factor y offset
								//ArrayList<Integer> list = null;
								int yWithOffset = 0;
								for (int tmpY = maxPrecisionYOffset; tmpY >= -maxPrecisionYOffset; tmpY--) {
									yWithOffset = widgetPos.y + tmpY;
									if (widgetsByApproximativeY.containsKey(yWithOffset)) {
										approximativeYExist = true;
										break;
									}
								}

								// a previous widget with approximative same y has been found
//								if (yxPositions.containsKey(widgetPos.y)) {
								if (approximativeYExist) {
									
									// retrieve the data of the last tooltips of same approximative row
									previousWidgetsOnY = widgetsByApproximativeY.get(yWithOffset);
									int j = 0;
									for (; j < previousWidgetsOnY.size(); j++) {
										Control tmpControl = (Control) previousWidgetsOnY.get(j);
										int tmpRightBorderX =  (((Point)tmpControl.getData()).x + e.gc.textExtent(tmpControl.getToolTipText()).x / 2) + 5; // +5px margin

										// DEbug
										Log.finest("TXMEditor.createPartControl(): checking previous right border x against = " + tmpControl.getToolTipText());
										
										if(toolTipPos.x > tmpRightBorderX) {
											// remove the overridden tooltip
											previousWidgetsOnY.set(j, control);
											// replace the overridden tooltip
											//previousWidgetsOnY.remove(j);
											
											break;
										}
									}
									
									
									yOffset += (maxPrecisionYOffset + 30) * j;
									
									//Debug
									//control.setToolTipText(control.getToolTipText() + "|x" + j);
									
//									// update the new data for this y coordinate
//									previousWidgetsOnY.set(0, (toolTipPos.x + textSize.x)); // store the right x coordinate to manage tooltips overriding
									
									System.err.println("+ TXMEditor.createPartControl(): last right x position found for y = " + yWithOffset);
									
									previousWidgetsOnY.add(control);
									
								}
								// new y coordinate
								else {
									previousWidgetsOnY = new ArrayList<Widget>();
									previousWidgetsOnY.add(control);
									widgetsByApproximativeY.put(toolTipPos.y, previousWidgetsOnY);
								}

								
								//Debug
								//toolTipText += "|" + numberOfTooltipsOnSameY;

								
								toolTipPos.y += yOffset;

								
								//Rectangle clientArea = canvas.getClientArea();
								//e.gc.setAlpha(255);
								
								// Line
								e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
								e.gc.drawLine(widgetPos.x, widgetPos.y, toolTipPos.x + textSize.x / 2, toolTipPos.y);

								// Tooltip text
								e.gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
								e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
								//  e.gc.fillOval(x,0,50, 50);
								e.gc.drawString(control.getToolTipText(), toolTipPos.x, toolTipPos.y);
								
								// Border
								e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
								e.gc.drawRectangle(toolTipPos.x - 2, toolTipPos.y, textSize.x + 4, 15);


							}
						}
					}

				});

			}

//			//shell.setLocation(location);
//			shell.setVisible(source.getSelection());
//			//shell.pack();

			
			shell.setVisible(true);
			
			
		}
		
		
		
		return null;
	}
}
