/**
 * 
 */
package org.txm.chartsengine.ece.rcp.swt;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.birt.chart.model.Chart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.events.EventCallBackHandler;
import org.txm.chartsengine.rcp.swt.ChartComposite;

/**
 * ECE chart composite.
 * 
 * @author sjacquot
 *
 */
public class ECEChartComposite extends ChartComposite {

	/**
	 * @param chartEditor
	 * @param parent
	 * @param style
	 */
	public ECEChartComposite(ChartEditor<ChartResult> chartEditor, Composite parent) {
		super(chartEditor, parent, SWT.NO_BACKGROUND);
		//super(chartEditor, parent, SWT.NONE);
		
		this.setLayout(new FillLayout());
		
	}

	@Override
	public void loadChart(Object data) {
		//Log.finest("Loading chart...");
		((ECEChartComponent)this.chartComponent).setChart((Chart) data);
		//Log.finest("Chart loaded.");
	}

	@Override
	public void loadChart() {
		// loads the chart from the result
		Object chart = this.chartEditor.getChart();
		
		if(chart != null) {
			
			// creates components if they not exist
			if(this.chartComponent == null) {

				// recreates the chart if not of right type
				if(this.chartEditor.getResult().isChartDirty())	{
					try {
						this.chartEditor.getResult().clearLastRenderingParameters();
						this.chartEditor.getResult().compute();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}

				this.createChartComponent();
			}
			this.loadChart(this.chartEditor.getChart());
		}
	}
	
	
	@Override
	public void clearChartItemsSelection() {
		// TODO Auto-generated method stub
		System.out.println("ECEChartComposite.clearChartItemsSelection(): not yet implemented.");
	}

	@Override
	protected IChartComponent _createChartComponent() {
		return new ECEChartComponent(this);
	}

	@Override
	public File exportView(File file, String fileFormat) {
		System.out.println("ECEChartComposite.exportView(): not yet implemented.");
		return null;
	}
	
	@Override
	public ArrayList<String> getEditorSupportedExportFileFormats()	{
		return this.getChartsEngine().getSupportedOutputFileFormats();
	}



	@Override
	public void copyChartViewToClipboard() {
		// TODO Auto-generated method stub
		System.out.println("ECEChartComposite.copyChartViewToClipboard(): not yet implemented.");
	}

	@Override
	public EventCallBackHandler getMouseCallBackHandler() {
		System.out.println("ECEChartComposite.getMouseCallBackHandler(): not yet implemented.");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EventCallBackHandler getKeyCallBackHandler() {
		System.out.println("ECEChartComposite.getKeyCallBackHandler(): not yet implemented.");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initEventsListeners() {
		System.out.println("ECEChartComposite.initEventsListeners(): not yet implemented.");
		// TODO Auto-generated method stub
		
	}

	

}
