/*******************************************************************************
 * Copyright (c) 2006, 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Qi Liang (IBM Corporation)
*******************************************************************************/
package org.txm.chartsengine.ece.rcp.swt;

import java.awt.event.MouseEvent;

import org.eclipse.birt.chart.device.ICallBackNotifier;
import org.eclipse.birt.chart.device.IDeviceRenderer;
import org.eclipse.birt.chart.exception.ChartException;
import org.eclipse.birt.chart.factory.GeneratedChartState;
import org.eclipse.birt.chart.factory.Generator;
import org.eclipse.birt.chart.model.Chart;
import org.eclipse.birt.chart.model.attribute.Bounds;
import org.eclipse.birt.chart.model.attribute.CallBackValue;
import org.eclipse.birt.chart.model.attribute.impl.BoundsImpl;
import org.eclipse.birt.chart.util.PluginSettings;
import org.eclipse.birt.core.framework.PlatformConfig;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.txm.chartsengine.rcp.IChartComponent;
import org.txm.chartsengine.rcp.editors.ChartEditor;

/**
 * ECE Chart component.
 */
public class ECEChartComponent extends Composite implements IChartComponent, ICallBackNotifier {

    /**
     * The device render for rendering chart.
     */
    protected IDeviceRenderer render = null;

    /**
     * The chart instance.
     */
    protected Chart chart = null;

    /**
     * The chart state.
     */
    protected GeneratedChartState generatedChartState = null;

    /**
     * The image which caches the chart image to improve drawing performance.
     */
    private Image cachedImage = null;

    
    /**
	 * The parent editor.
	 */
	protected ChartEditor editor;
    
	/**
	 * 
	 * @param parent
	 */
	public ECEChartComponent(Composite parent) {
        super(parent, SWT.NO_BACKGROUND);
//        this.setLayout( new GridLayout( ) );
//       this.setLayoutData( new GridData( GridData.FILL_BOTH ) );
        
        
		PlatformConfig config = new PlatformConfig();
		config.setProperty("STANDALONE", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		final PluginSettings ps = PluginSettings.instance(config);

        
        // initialize the SWT rendering device
		try {
//			PluginSettings ps = PluginSettings.instance();
			render = ps.getDevice("dv.SWT"); //$NON-NLS-1$
			render.setProperty(IDeviceRenderer.UPDATE_NOTIFIER, ECEChartComponent.this);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		this.addPaintListener(new PaintListener() {

			public void paintControl(PaintEvent e) {

				System.out.println("ECEChartComponent.ECEChartComponent(...).new PaintListener() {...}.paintControl()");
				
				Rectangle size = getClientArea();
				Image chartImage = new Image(getDisplay(), size);
				GC imageGC = new GC(chartImage);
				render.setProperty(IDeviceRenderer.GRAPHICS_CONTEXT, imageGC);
				
				Bounds bo = BoundsImpl.create(0, 0, size.width, size.height);
				bo.scale(72d / render.getDisplayServer().getDpiResolution());

				buildChart();

				try {
					Generator.instance().render(render, generatedChartState);
					GC gc = e.gc;
					gc.drawImage(chartImage, size.x, size.y);
					
					//chartImage.dispose(); // added by SJ
//					Display.getCurrent().update();
//					update();
//					layout();

				}
				catch (Exception ce) {
					ce.printStackTrace();
				}
			}
		});

        this.addControlListener(new ControlAdapter() {

            public void controlResized(ControlEvent e) {
                buildChart();
                cachedImage = null;
            }
        });
        
        //this.setSize(1280, 600);
        this.setSize(this.getParent().getSize().x - 5, this.getParent().getSize().y - 5);
        
        // Debug
        //this.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
    }

    /**
     * Builds the chart state. This method should be call when data is changed.
     */
	private void buildChart() {

		if (this.chart == null) {
			return;
		}

		Point size = this.getParent().getSize();
		Bounds bounds = BoundsImpl.create(0, 0, size.x, size.y);
		bounds.scale(72d / this.render.getDisplayServer().getDpiResolution());
		try {
			this.generatedChartState = Generator.instance().build(this.render.getDisplayServer(), this.chart, bounds, null, null, null);
			//
			//this.render.setProperty(IDeviceRenderer.UPDATE_NOTIFIER, new EmptyUpdateNotifier(this.chart, this.generatedChartState.getChartModel()));
			
//			this.generatedChartState.getRunTimeContext().setActionRenderer(new ActionRendererAdapter() {
//				
//				@Override
//				public void processAction(Action action, StructureSource source, RunTimeContext rtc) {
//					// TODO Auto-generated method stub
//					System.out.println("ECEChartComponent.buildChart().new IActionRenderer() {...}.processAction(): " + action.getType());
//				}
//				
//				@Override
//				public void processAction(Action action, StructureSource source) {
//					// TODO Auto-generated method stub
//					System.out.println("ECEChartComponent.buildChart().new IActionRenderer() {...}.processAction2(): " + action.getType());
//				}
//			});
		}
		catch (ChartException ex) {
			ex.printStackTrace();
		}
	}

    /**
     * Draws the chart onto the cached image in the area of the given
     * <code>Rectangle</code>.
     * 
     * @param size
     *            the area to draw
     */
    public void drawToCachedImage(Rectangle size) {
        GC gc = null;
        try {
            if(cachedImage != null) {
                cachedImage.dispose();
            }
            cachedImage = new Image(Display.getCurrent(), size.width, size.height);

            gc = new GC(cachedImage);
            render.setProperty(IDeviceRenderer.GRAPHICS_CONTEXT, gc);
            Generator.instance().render(render, generatedChartState);
        }
        catch (ChartException ex) {
            ex.printStackTrace();
        }
        finally {
            if (gc != null) {
                gc.dispose();
            }
        }
    }

    /**
     * Returns the chart which is contained in this canvas.
     * 
     * @return the chart contained in this canvas.
     */
    public Chart getChart() {
        return chart;
    }

    /**
     * Sets the chart into this canvas. Note: When the chart is set, the cached
     * image will be dopped, but this method doesn't reset the flag
     * <code>cachedImage</code>.
     * 
     * @param chart
     *            the chart to set
     */
    public void setChart(Chart chart) {
        if (cachedImage != null) {
            cachedImage.dispose();
        }

        cachedImage = null;
        this.chart = chart;
        
        this.repaintChart();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.swt.widgets.Widget#dispose()
     */
    public void dispose() {
        if (cachedImage != null)
            cachedImage.dispose();
        super.dispose();
    }


	@Override
	public void resetView() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pan(double srcX, double srcY, double dstX, double dstY, double panFactor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ChartEditor getChartEditor() {
		return this.editor;
	}

	@Override
	public void setChartEditor(ChartEditor editor) {
		this.editor = editor;
	}

	@Override
	public void updateMouseOverItem(MouseEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean hasFocus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void squareOff() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void regenerateChart() {
		this.buildChart();
		this.repaintChart();
	}

	@Override
	public void repaintChart() {
		this.redraw();
	}

	@Override
	public Object peerInstance() {
		return this;
	}

	@Override
	public Chart getDesignTimeModel() {
		return this.chart;
	}

	@Override
	public Chart getRunTimeModel() {
		return this.generatedChartState.getChartModel();
	}

	@Override
	public void callback(Object event, Object source, CallBackValue value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void zoom(double x, double y, boolean zoomIn, boolean range, boolean domain) {
		// TODO Auto-generated method stub
		
	}

}
