/**
 * 
 */
package org.txm.chartsengine.ece.rcp;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;
import org.txm.chartsengine.ece.rcp.swt.ECEChartComposite;
import org.txm.chartsengine.rcp.SWTChartsComponentsProvider;
import org.txm.chartsengine.rcp.editors.ChartEditor;
import org.txm.chartsengine.rcp.swt.ChartComposite;

/**
 * ECE library charts components provider.
 * 
 * @author sjacquot
 *
 */
public class ECESWTChartsComponentsProvider extends SWTChartsComponentsProvider {

	/**
	 * Default constructor. 
	 */
	public ECESWTChartsComponentsProvider() {
		super();
	}

	@Override
	public ChartComposite createComposite(ChartEditor chartEditor, Composite parent) {
		return new ECEChartComposite(chartEditor, parent);
	}

	@Override
	public ArrayList<String> getChartsEngineSupportedExportFileFormats() {
		return this.chartsEngine.getSupportedOutputFileFormats();
	}


}
