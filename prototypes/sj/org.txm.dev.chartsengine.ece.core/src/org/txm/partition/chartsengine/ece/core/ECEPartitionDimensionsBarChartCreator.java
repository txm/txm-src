package org.txm.partition.chartsengine.ece.core;
/**
 * 
 */


import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.birt.chart.model.ChartWithAxes;
import org.eclipse.birt.chart.model.attribute.ActionType;
import org.eclipse.birt.chart.model.attribute.AxisType;
import org.eclipse.birt.chart.model.attribute.IntersectionType;
import org.eclipse.birt.chart.model.attribute.LegendBehaviorType;
import org.eclipse.birt.chart.model.attribute.LegendItemType;
import org.eclipse.birt.chart.model.attribute.LineStyle;
import org.eclipse.birt.chart.model.attribute.TickStyle;
import org.eclipse.birt.chart.model.attribute.TriggerCondition;
import org.eclipse.birt.chart.model.attribute.impl.ColorDefinitionImpl;
import org.eclipse.birt.chart.model.attribute.impl.SeriesValueImpl;
import org.eclipse.birt.chart.model.attribute.impl.TooltipValueImpl;
import org.eclipse.birt.chart.model.component.Axis;
import org.eclipse.birt.chart.model.component.Series;
import org.eclipse.birt.chart.model.component.impl.SeriesImpl;
import org.eclipse.birt.chart.model.data.NumberDataSet;
import org.eclipse.birt.chart.model.data.SeriesDefinition;
import org.eclipse.birt.chart.model.data.TextDataSet;
import org.eclipse.birt.chart.model.data.impl.ActionImpl;
import org.eclipse.birt.chart.model.data.impl.NumberDataSetImpl;
import org.eclipse.birt.chart.model.data.impl.SeriesDefinitionImpl;
import org.eclipse.birt.chart.model.data.impl.TextDataSetImpl;
import org.eclipse.birt.chart.model.data.impl.TriggerImpl;
import org.eclipse.birt.chart.model.impl.ChartWithAxesImpl;
import org.eclipse.birt.chart.model.type.BarSeries;
import org.eclipse.birt.chart.model.type.impl.BarSeriesImpl;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.ece.core.ECEChartCreator;
import org.txm.partition.core.chartsengine.base.Utils;
import org.txm.partition.core.functions.PartitionDimensions;
import org.txm.partition.core.messages.PartitionCoreMessages;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.Part;

/**
 * Partition Dimensions Bar Chart creator.
 * 
 * @author sjacquot
 *
 */
public class ECEPartitionDimensionsBarChartCreator extends ECEChartCreator {

	/**
	 * 
	 */
	public ECEPartitionDimensionsBarChartCreator() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object createChart(ChartResult result) {
		
		PartitionDimensions partitionDimensions = (PartitionDimensions) result;
		
		ChartWithAxes chart = ChartWithAxesImpl.create();
       // chart.setDimension(ChartDimension.TWO_DIMENSIONAL_WITH_DEPTH_LITERAL);

		
		//chart.getInteractivity( ).setLegendBehavior( LegendBehaviorType.HIGHLIGHT_SERIE_LITERAL );

		
		
//        try {
//			Generator.instance().prepare(chart, null, null, ULocale.getDefault());
//		}
//		catch (ChartException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        
        // create legend
//        chart.getLegend().setItemType(LegendItemType.CATEGORIES_LITERAL);
//        chart.getLegend().setVisible(true);
//
//        Axis xAxis = ((ChartWithAxes) chart).getPrimaryBaseAxes()[0];
//
//        xAxis.getTitle().setVisible(true);
//        xAxis.getTitle().getCaption().setValue("Test");
//        
//        xAxis.getMajorGrid().setTickStyle(TickStyle.BELOW_LITERAL);
//        xAxis.setType(AxisType.TEXT_LITERAL);
//        xAxis.getOrigin().setType(IntersectionType.VALUE_LITERAL);
//        
        
//
//        // Apply the color palette
//        SeriesDefinition sdX = SeriesDefinitionImpl.create();
//        sdX.getSeriesPalette().update(1);
		
		return chart;
	}
	
	@Override
	public void updateChart(ChartResult result) {
	
		PartitionDimensions partitionDimensions = (PartitionDimensions) result;
		ChartWithAxes chart = (ChartWithAxes) result.getChart();

		// parameters
		boolean sortPartsBySize = partitionDimensions.isSortingBySize();
		boolean displayPartsCountInTitle = partitionDimensions.isDisplayingPartCountInTitle();

		
		
		// Title
        chart.getTitle().getLabel().getCaption().setValue(Utils.createPartitionDimensionsChartTitle(partitionDimensions) + "\n" + Utils.createPartitionDimensionsChartSubtitle(partitionDimensions, sortPartsBySize, displayPartsCountInTitle));
        chart.getTitle().getLabel().getCaption().getFont().setSize(14);
//        chart.getTitle().getLabel().getCaption().getFont().setName(FONT_NAME);

        // Legend (the type also the colors mapping: by series or categories)
       // chart.getLegend().setItemType(LegendItemType.CATEGORIES_LITERAL);
        chart.getLegend().setItemType(LegendItemType.SERIES_LITERAL);
		chart.getLegend().setVisible(true);
		
		
		// X axis
		Axis xAxis = chart.getPrimaryBaseAxes()[0];

		xAxis.getTitle().setVisible(true);
		xAxis.getTitle().getCaption().setValue(PartitionCoreMessages.part);

		xAxis.getMajorGrid().setTickStyle(TickStyle.BELOW_LITERAL);
		xAxis.setType(AxisType.TEXT_LITERAL);
		xAxis.getOrigin().setType(IntersectionType.VALUE_LITERAL);

		// X dataset
		//System.out.println("ECEPartitionDimensionsBarChartCreator.updateChart()");
		List<Part> parts = partitionDimensions.getParts(sortPartsBySize);
		ArrayList<String> partNames=  new ArrayList<String>();
		
		double[] partsSizes = new double[partitionDimensions.getPartsCount()]; // for Y dataset

		
		try {
			for (int i = 0; i < parts.size(); i++) {
				// Add X axis symbol
				partNames.add(parts.get(i).getName());
				// part size
				partsSizes[i] = parts.get(i).getSize();
				
			}
		}
		catch (CqiClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//String[] names = partNames.toArray(new String[0]); 

        Series series = SeriesImpl.create();
        TextDataSet categoryValues = TextDataSetImpl.create(partNames);
		//TextDataSet categoryValues = TextDataSetImpl.create(new String[] {"rrrr", "aaa"});
        series.setDataSet(categoryValues);


		SeriesDefinition xSeriesDefinitionX = SeriesDefinitionImpl.create();
		xSeriesDefinitionX.getSeries().add(series);
//		// Apply the color palette
//		xSeriesDefinitionX.setSeriesPalette(new Palette);
		//xSeriesDefinitionX.getSeriesPalette().update(1);
//
		xAxis.getSeriesDefinitions().clear();
		xAxis.getSeriesDefinitions().add(xSeriesDefinitionX);
		
//		if(!xAxis.getSeriesDefinitions().isEmpty())	{
//			xAxis.getSeriesDefinitions().set(0, xSeriesDefinitionX);
//			//xAxis.getRuntimeSeries()[0] = seCategory;
//		}
//		else {
////		xAxis.getSeriesDefinitions().clear();
//		xAxis.getSeriesDefinitions().add(xSeriesDefinitionX);
//		}

		// Y axis
		Axis yAxis = chart.getPrimaryOrthogonalAxis(xAxis);

		yAxis.getTitle().setVisible(true);
		yAxis.getTitle().getCaption().setValue(PartitionCoreMessages.numberOfWords);

		yAxis.getMajorGrid().getLineAttributes().setVisible(true);
		yAxis.getMajorGrid().getLineAttributes().setColor(ColorDefinitionImpl.GREY());
		yAxis.getMajorGrid().getLineAttributes().setStyle(LineStyle.DASHED_LITERAL);
		yAxis.getMajorGrid().setTickStyle(TickStyle.LEFT_LITERAL);
		//yAxis.getMajorGrid().setTickCount(5);
		
		yAxis.setType(AxisType.LINEAR_LITERAL);
		yAxis.getOrigin().setType(IntersectionType.VALUE_LITERAL);

		yAxis.getScale().setStep(1000.0);
		
		// Y dataset
		NumberDataSet orthoValuesDataSet1 = NumberDataSetImpl.create(partsSizes);

		BarSeries barSeries = (BarSeries) BarSeriesImpl.create();
		barSeries.setDataSet(orthoValuesDataSet1);
		barSeries.setRiserOutline(null);

		SeriesDefinition sdY = SeriesDefinitionImpl.create();
		yAxis.getSeriesDefinitions().clear();
		yAxis.getSeriesDefinitions().add(sdY);
		sdY.getSeries().add(barSeries);		
		
		
		// event tests
		barSeries.getTriggers( )
		.add( TriggerImpl.create( TriggerCondition.ONMOUSEOVER_LITERAL,
				ActionImpl.create( ActionType.SHOW_TOOLTIP_LITERAL,
						TooltipValueImpl.create( 500, null ) ) ) );
		
		barSeries.getTriggers( )
		.add( TriggerImpl.create( TriggerCondition.ONCLICK_LITERAL,
				ActionImpl.create( ActionType.HIGHLIGHT_LITERAL,
						SeriesValueImpl.create( String.valueOf( series.getSeriesIdentifier( ) ) ) ) ) );

		

		
		
		super.updateChart(result);
	}

	/* (non-Javadoc)
	 * @see org.txm.chartsengine.core.ChartCreator#getResultDataClass()
	 */
	@Override
	public Class getResultDataClass() {
		return PartitionDimensions.class;
	}

	/* (non-Javadoc)
	 * @see org.txm.chartsengine.core.ChartCreator#getSeriesShapesColors(java.lang.Object)
	 */
	@Override
	public ArrayList<Color> getSeriesShapesColors(Object chart) {
		// TODO Auto-generated method stub
		System.err.println("ECEPartitionDimensionsBarChartCreator.getSeriesShapesColors(): not yet implemented.");
		return null;
	}

}
