package org.txm.chartsengine.ece.core.preferences;


import org.osgi.service.prefs.Preferences;
import org.txm.chartsengine.ece.core.ECEChartsEngine;
import org.txm.core.preferences.TXMPreferences;

/**
 * Preferences initializer and manager.
 * 
 * @author sjacquot
 *
 */
public class ECEChartsEnginePreferences extends TXMPreferences {


	public final static String PREFERENCES_PREFIX = ECEChartsEngine.NAME + "_";
	
	public static final String OUTPUT_FORMAT = PREFERENCES_PREFIX + "output_format"; //$NON-NLS-1$
	

	/**
	 * Gets the instance.
	 * @return the instance
	 */
	public static TXMPreferences getInstance()	{
		if (!TXMPreferences.instances.containsKey(ECEChartsEnginePreferences.class)) {
			new ECEChartsEnginePreferences();
		}
		return TXMPreferences.instances.get(ECEChartsEnginePreferences.class);
	}

	
	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		Preferences defaultPreferences = this.getDefaultPreferencesNode();
		defaultPreferences.put(OUTPUT_FORMAT, ECEChartsEngine.OUTPUT_FORMAT_ECE);
	}
	
}
