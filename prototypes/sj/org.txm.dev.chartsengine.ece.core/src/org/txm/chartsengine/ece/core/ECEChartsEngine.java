/**
 * 
 */
package org.txm.chartsengine.ece.core;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.txm.chartsengine.core.ChartsEngine;
import org.txm.chartsengine.core.results.ChartResult;
import org.txm.chartsengine.ece.core.preferences.ECEChartsEnginePreferences;
import org.txm.utils.logger.Log;

/**
 * Eclipse Chart Engine BIRT charts engine.
 * 
 * @author sjacquot
 *
 */
public class ECEChartsEngine extends ChartsEngine {
	
	
	/**
	 * Constants for extra output formats.
	 */
	public final static String OUTPUT_FORMAT_ECE = "ECE/SWT"; //$NON-NLS-1$
	
	
	/**
	 * The charts engine internal name.
	 */
	public final static String NAME = "ece_charts_engine"; //$NON-NLS-1$
	
	/**
	 * The charts engine description.
	 */
	public final static String DESCRIPTION = "Eclipse Chart Engine BIRT/SWT";
	
	
	
	/**
	 * Creates a GraphStream charts engine with output format from the preferences.
	 */
	public ECEChartsEngine() {
		this(ECEChartsEnginePreferences.getInstance().getString(ECEChartsEnginePreferences.OUTPUT_FORMAT));
	}
	
	
	/**
	 * Creates a GraphStream charts engine with the specified output format.
	 * 
	 * @param outputFormat
	 */
	public ECEChartsEngine(String outputFormat) {
		super(DESCRIPTION, outputFormat);
	}
	
	
	
	
	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#getName()
	 */
	@Override
	public String getName() {
		return ECEChartsEngine.NAME;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#getState()
	 */
	@Override
	public boolean isRunning() {
		// TODO Auto-generated method stub
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#start(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.txm.core.engines.Engine#stop()
	 */
	@Override
	public boolean stop() throws Exception {
		// TODO Auto-generated method stub
		return true;
	}
	
	
//	@Override
//	public File exportChart(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	
//	@Override
//	public File exportChart(ChartResult result, File file, int imageWidth, int imageHeight, String outputFormat) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	
	@Override
	public File exportChartResultToFile(ChartResult result, File file, int imageWidth, int imageHeight, String outputFormat) {
		Log.severe("ECEChartsEngine.exportChartResultToFile(): not implemented."); //$NON-NLS-2$
		return null;
	}


	@Override
	public File exportChartToFile(Object chart, File file, String outputFormat, int imageWidth, int imageHeight, int drawingAreaX, int drawingAreaY, int drawingAreaWidth, int drawingAreaHeight) {
		Log.severe("ECEChartsEngine.exportChartResultToFile(): not implemented."); //$NON-NLS-2$
		return null;
	}

	
	
	
	@Override
	public ArrayList<String> getSupportedOutputDisplayFormats() {
		ArrayList<String> formats = new ArrayList<>(1);
		formats.add(OUTPUT_FORMAT_ECE);
		return formats;
	}
	
	
	@Override
	public ArrayList<String> getSupportedOutputFileFormats() {
		System.err.println("ECEChartsEngine.getSupportedOutputFileFormats(): not yet implemented.");
		ArrayList<String> formats = new ArrayList<>(1);
		formats.add(OUTPUT_FORMAT_NONE);
		return formats;
	}
	
	
	
}
