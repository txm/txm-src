package org.txm.chartsengine.ece.core;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;

import org.eclipse.birt.chart.model.Chart;
import org.eclipse.birt.chart.model.ChartWithAxes;
import org.eclipse.birt.chart.model.attribute.LegendItemType;
import org.eclipse.birt.chart.model.attribute.Palette;
import org.eclipse.birt.chart.model.attribute.impl.ColorDefinitionImpl;
import org.txm.chartsengine.core.ChartCreator;
import org.txm.chartsengine.core.preferences.ChartsEnginePreferences;
import org.txm.chartsengine.core.results.ChartResult;



/**
 * ECE base chart creator.
 * 
 * All ECE charts creators should extend this class.
 * The updateChart(ChartResult result) method implementation of the subclasses must call super.update(result) to benefit to the shared system.
 * 
 * @author sjacquot
 *
 */
public abstract class ECEChartCreator extends ChartCreator {
	
	
	
	@Override
	public void updateChart(ChartResult result) {
		// Java object
		if (result.getChart() instanceof Chart) {
			Chart chart = (Chart) result.getChart();
			
			// rendering color mode
			// this.getChartsEngine().getJFCTheme().applySeriesPaint(result);
			
			// multiple line strokes
			// this.getChartsEngine().getJFCTheme().applySeriesStrokes(result);
			
			// title visibility
			if (result.hasParameterChanged(ChartsEnginePreferences.SHOW_TITLE)) {
				chart.getTitle().setVisible(result.isTitleVisible());
			}
			// legend visibility
			if (result.hasParameterChanged(ChartsEnginePreferences.SHOW_LEGEND)) {
				chart.getLegend().setVisible(result.isLegendVisible());
			}
			
			if (chart instanceof ChartWithAxes) {
				ChartWithAxes chartWithAxis = (ChartWithAxes) chart;
				
				// grid visibility
				if (result.hasParameterChanged(ChartsEnginePreferences.SHOW_GRID)) {
					chartWithAxis.getPrimaryOrthogonalAxis(chartWithAxis.getPrimaryBaseAxes()[0]).getMajorGrid().getLineAttributes().setVisible(result.isGridVisible());
				}
				
				// palette
				int itemsCount = ((ArrayList) chartWithAxis.getPrimaryBaseAxes()[0].getSeriesDefinitions().get(0).getSeries().get(0).getDataSet().getValues()).size();
				int colorsCount = 1;
				// colors by categories
				if (chart.getLegend().getItemType() == LegendItemType.CATEGORIES_LITERAL) {
					colorsCount = itemsCount;
				}
				// colors by series
				else {
					colorsCount = chartWithAxis.getPrimaryBaseAxes()[0].getSeriesDefinitions().size();
				}
				
				ArrayList<Color> colors = this.chartsEngine.getTheme().getPaletteFor(result.getRenderingColorsMode(), colorsCount);
				
				Palette palette = chartWithAxis.getPrimaryBaseAxes()[0].getSeriesDefinitions().get(0).getSeriesPalette();
				palette.getEntries().clear(); // note SJ: the clear is not sufficient to use monochrome color so we need to repeat the color
				for (int i = 0; i < itemsCount; i++) {
					Color color = null;
					try {
						color = colors.get(i);
					}
					catch (Exception e) {
						color = colors.get(colors.size() - 1);
					}
					palette.getEntries().add(ColorDefinitionImpl.create(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()));
				}
				
			}
			
			// XYPlot
			// if(plot instanceof XYPlot) {
			//
			// XYPlot typedPlot = (XYPlot)plot;
			// XYItemRenderer renderer = typedPlot.getRenderer();
			//
			// palette = this.chartsEngine.getTheme().getPaletteFor(result.getRenderingColorsMode(), typedPlot.getSeriesCount());
			// for(int i = 0; i < palette.size(); i++) {
			// renderer.setSeriesPaint(i, palette.get(i));
			// }
			// }
			
			
		}
		// // File
		// else if(result.getChart() instanceof File) {
		// // creates a new chart but using the same file
		// this.createChartFile(result, (File)result.getChart());
		//
		// // FIXME: using new file
		// //this.createChartFile(result, preferencesNode);
		//
		// }
	}
	
	
	
	
	@Override
	public Class getChartsEngineClass() {
		return ECEChartsEngine.class;
	}
	
	/**
	 * Convenience method.
	 * 
	 * @return the casted charts engine.
	 */
	@Override
	public ECEChartsEngine getChartsEngine() {
		return (ECEChartsEngine) this.chartsEngine;
	}
	
}
