package org.txm.python.core;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

/**
 * 
 * @author mdecorde
 *
 */
public class PythonPreferences extends TXMPreferences {

	public static final String HOME = "home";

	public static final String EXECUTABLE_NAME = "exec";

	public static final String ADDITIONAL_PARAMETERS = "additional_parameters";

	public static final String USE_JYTHON = "use_jython";

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		// TODO Auto-generated method stub

		Preferences preferences = getDefaultPreferencesNode();

		preferences.put(HOME, "");
		preferences.put(EXECUTABLE_NAME, "python");
		preferences.put(ADDITIONAL_PARAMETERS, "");

		preferences.putBoolean(USE_JYTHON, false);
	}

	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(PythonPreferences.class)) {
			new PythonPreferences();
		}
		return TXMPreferences.instances.get(PythonPreferences.class);
	}
}
