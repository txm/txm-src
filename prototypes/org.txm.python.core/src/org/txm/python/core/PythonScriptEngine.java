package org.txm.python.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.python.core.Py;
import org.python.core.PyString;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;
import org.txm.Toolbox;
import org.txm.core.engines.ScriptEngine;
import org.txm.utils.logger.Log;

public class PythonScriptEngine extends ScriptEngine {

	public PythonScriptEngine() {
		super("python", "py");
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public boolean initialize() throws Exception {
		return true;
	}

	@Override
	public boolean start(IProgressMonitor monitor) throws Exception {
		return true;
	}

	@Override
	public boolean stop() throws Exception {
		return true;
	}

	@Override
	public IStatus executeScript(File script, HashMap<String, Object> env, List<String> stringArgs) {
		ArrayList<String> cmd = new ArrayList<String>();
		cmd.add(script.getAbsolutePath());

		System.out.println("Running python script...");
		boolean useJython = PythonPreferences.getInstance().getBoolean(PythonPreferences.USE_JYTHON);
		if (useJython) {
			return runJython(cmd, env);
		}
		else {
			return runPython(cmd, env);
		}
	}

	@Override
	public IStatus executeText(String str, HashMap<String, Object> env) {
		ArrayList<String> cmd = new ArrayList<String>();
		cmd.add("-c");
		cmd.add(str);
		System.out.println("Running python code...");
		boolean useJython = PythonPreferences.getInstance().getBoolean(PythonPreferences.USE_JYTHON);
		if (useJython) {
			return runJython(cmd, env);
		}
		else {
			return runPython(cmd, env);
		}
	}

	private static IStatus runJython(ArrayList<String> cmd, HashMap<String, Object> env) {

		try {
			PySystemState sys = Py.getSystemState();
			//if (sys.path == null || !sys.path.asString().contains(Toolbox.getTxmHomePath()+"/scripts/python")) {
			File sysdir = new File(Toolbox.getTxmHomePath() + "/scripts/python");
			sysdir.mkdirs();
			sys.path.append(new PyString(Toolbox.getTxmHomePath() + "/scripts/python"));
			//}
			//if (!sys.prefix.asString().equals(Toolbox.getTxmHomePath()+"/scripts/python")) {
			sys.prefix = new PyString(Toolbox.getTxmHomePath() + "/scripts/python");
			//}

			PythonInterpreter interpreter = new PythonInterpreter();

			for (String s : env.keySet()) {
				interpreter.set(s, env.get(s));
			}
			if (cmd.get(0).equals("-c")) {
				interpreter.exec(cmd.get(1));
			}
			else {
				interpreter.execfile(cmd.get(0));
			}
			interpreter.close();
			return Status.OK_STATUS;
		}
		catch (Throwable e) {
			Log.warning("Jython error: " + e);
			e.printStackTrace();
			return Status.CANCEL_STATUS;
		}

	}

	private static IStatus runPython(ArrayList<String> cmd, HashMap<String, Object> env) {
		try {

			String additionalParametersString = PythonPreferences.getInstance().getString(PythonPreferences.ADDITIONAL_PARAMETERS);
			String[] additionalParameters = additionalParametersString.split("\t");
			if (additionalParametersString.length() > 0 && additionalParameters.length > 0) {
				cmd.addAll(0, Arrays.asList(additionalParameters));
			}

			String path = PythonPreferences.getInstance().getString(PythonPreferences.HOME);
			String pythonRunner = PythonPreferences.getInstance().getString(PythonPreferences.EXECUTABLE_NAME);
			if (path != null && path.length() > 0) {
				cmd.add(0, new File(path, pythonRunner).getAbsolutePath());
			}
			else {
				cmd.add(0, pythonRunner);
			}

			ProcessBuilder pb = new ProcessBuilder(cmd);
			pb.redirectErrorStream(true);

			// new org.txm.utils.StreamHog(p.getInputStream(), true).setName("python");
			Process process = pb.start();
			if (process != null) {
				BufferedReader bfr = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line = "";
				while ((line = bfr.readLine()) != null) {
					// display each output line form python script
					System.out.println(line);
				}
				int e = process.waitFor();
				if (e != 0) {
					System.err.println("Process exited abnormally with code=" + e);
					return Status.CANCEL_STATUS;
				}
			}
			else {
				System.out.println("Error: python process not created");
				return Status.CANCEL_STATUS;
			}
			System.out.println("Done.");
		}
		catch (Exception e) {
			Log.warning("Python error: " + e);
			Log.printStackTrace(e);
			return Status.CANCEL_STATUS;
		}
		return Status.OK_STATUS;
	}

	public static void main(String args[]) {
		ArrayList<String> cmd = new ArrayList<String>();
		//		cmd.add("-c");
		//		cmd.add("print('hi')");
		cmd.add("/home/mdecorde/TEMP/test.py");
		runJython(cmd, null);
	}
}
