// Copyright © 2010-2020 ENS de Lyon., University of Franche-Comté
package org.txm.segments.rcp;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.FrameworkUtil;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.adapters.TXMResultAdapter;
import org.txm.rcp.adapters.TXMResultAdapterFactory;
import org.txm.segments.core.Segments;


/**
 * Display an Segments or Lexicon icon dependaing on the Segments used a lexicon to compute
 * 
 * @author mdecorde
 * @author sjacquot
 */
public class SegmentsAdapterFactory extends TXMResultAdapterFactory {


	public static final ImageDescriptor ICON = IImageKeys.getImageDescriptor("icons/segments.png"); //$NON-NLS-1$ //$NON-NLS-2$

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (this.canAdapt(adapterType) && adaptableObject instanceof Segments) {
			return adapterType.cast(new TXMResultAdapter() {

				@Override
				public ImageDescriptor getImageDescriptor(Object object) {
					return ICON;
				}
			});
		}
		return null;
	}


}
