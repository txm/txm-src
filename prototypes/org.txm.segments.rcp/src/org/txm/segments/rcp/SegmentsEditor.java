package org.txm.segments.rcp;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.txm.core.results.Parameter;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.editors.listeners.ComputeKeyListener;
import org.txm.rcp.editors.listeners.ComputeSelectionListener;
import org.txm.rcp.swt.GLComposite;
import org.txm.rcp.swt.widget.PropertiesSelector;
import org.txm.rcp.swt.widget.TXMParameterSpinner;
import org.txm.searchengine.cqp.clientExceptions.CqiClientException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.segments.core.Segment;
import org.txm.segments.core.Segments;
import org.txm.segments.core.SegmentsPreferences;
import org.txm.utils.logger.Log;

/**
 * Internal view editor.
 * 
 * @author mdecorde
 * @author sjacquot
 *
 */
public class SegmentsEditor extends TXMEditor<Segments> {

	public static final String ID = SegmentsEditor.class.getName();

	protected CQPCorpus corpus;

	protected Segments segments;

	protected TreeViewer treeViewer;

	@Parameter(key = SegmentsPreferences.PROPERTY, type = Parameter.COMPUTING)
	PropertiesSelector<WordProperty> propertiesSelector;
	
	@Parameter(key = SegmentsPreferences.MIN_SEGMENTS_SIZE, type = Parameter.COMPUTING)
	TXMParameterSpinner pMinSegmentsize;
	
	@Parameter(key = SegmentsPreferences.MAX_SEGMENTS_SIZE, type = Parameter.COMPUTING)
	TXMParameterSpinner pMaxSegmentsize;
	
	@Parameter(key = SegmentsPreferences.MAX_N_SEGMENTS, type = Parameter.COMPUTING)
	TXMParameterSpinner pMaxNSegmentsSpinner;
	
	@Parameter(key = SegmentsPreferences.MIN_FREQ_SEGMENTS, type = Parameter.COMPUTING)
	TXMParameterSpinner pMinFreqSegments;
	
	@Parameter(key = SegmentsPreferences.REGROUP, type = Parameter.COMPUTING)
	Button regroupButton;
	
	@Parameter(key = SegmentsPreferences.SORT_BY_SIZE, type = Parameter.COMPUTING)
	Button sortBySizeButton;
	
	@Override
	public void _createPartControl() {

		segments = this.getResult();

		// Computing listener
		ComputeSelectionListener computeSelectionListener = new ComputeSelectionListener(this, true);
		ComputeKeyListener computeKeyListener = new ComputeKeyListener(this);

		// Main parameters
		GLComposite mainParametersArea = this.getMainParametersComposite();
		mainParametersArea.getLayout().numColumns = 5;

		// Extended parameters
		Composite parametersArea = getExtendedParametersGroup();
		parametersArea.setLayout(new GridLayout(4, false));

		// Word properties selector
		propertiesSelector = new PropertiesSelector<>(getMainParametersComposite());
		this.propertiesSelector.setMaxPropertyNumber(1);
		propertiesSelector.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, false, true));
		propertiesSelector.setLayout(new GridLayout(3, false));
		try {
			List<WordProperty> l = segments.getParent().getOrderedProperties();
			propertiesSelector.setProperties(l);
		}
		catch (CqiClientException e) {
			Log.printStackTrace(e);
		}
		// Listener
		propertiesSelector.addSelectionListener(computeSelectionListener);

		pMinSegmentsize = new TXMParameterSpinner(getMainParametersComposite(), this, "Min length");
		pMinSegmentsize.setMinimum(1);
		pMinSegmentsize.setMaximum(Integer.MAX_VALUE);
		pMinSegmentsize.setToolTipText("Min segment length");
		
		pMaxSegmentsize = new TXMParameterSpinner(getMainParametersComposite(), this, "Max length");
		pMaxSegmentsize.setMinimum(1);
		pMaxSegmentsize.setMaximum(Integer.MAX_VALUE);
		pMaxSegmentsize.setToolTipText("Max segment length");
		
		regroupButton = new Button(getMainParametersComposite(), SWT.CHECK);
		regroupButton.setText("Group");
		regroupButton.setToolTipText("If set, the sub-segments are included in their bigger segments");
		
		sortBySizeButton = new Button(getMainParametersComposite(), SWT.CHECK);
		sortBySizeButton.setText("Sort by size");
		sortBySizeButton.setToolTipText("If set, the segments are sorted by size");
		
		pMaxNSegmentsSpinner = new TXMParameterSpinner(this.getExtendedParametersGroup(), this, "VMax");
		pMaxNSegmentsSpinner.setMinimum(1);
		pMaxNSegmentsSpinner.setMaximum(Integer.MAX_VALUE);
		pMaxNSegmentsSpinner.setToolTipText("Max number fo segments after beeing sorted");
		
		pMinFreqSegments = new TXMParameterSpinner(this.getExtendedParametersGroup(), this, "Fmin");
		pMinFreqSegments.setMinimum(1);
		pMinFreqSegments.setMaximum(Integer.MAX_VALUE);
		pMinFreqSegments.setToolTipText("Min segments frequency");

		// Result area
		Composite resultPanel = getResultArea();

		treeViewer = new TreeViewer(resultPanel, SWT.FULL_SELECTION | SWT.VIRTUAL);
//		String tableKeyListener = TableUtils.installCopyAndSearchKeyEvents(treeViewer);
//		tableKeyListener.installSearchGroup(this);
//		tableKeyListener.installMenuContributions(this);
//		TableUtils.installColumnResizeMouseWheelEvents(treeViewer);
//		TableUtils.installTooltipsOnSelectionChangedEvent(treeViewer);

		//treeViewer.setComparator(new TableLinesViewerComparator(Collator.getInstance()));
		
		treeViewer.getTree().setLinesVisible(true);
		treeViewer.getTree().setHeaderVisible(true);
		treeViewer.getTree().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true));
		treeViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}

			@Override
			public void dispose() {
			}

			@Override
			public Object[] getElements(Object inputElement) {
				
				return segments.getSegments().keySet().toArray();
			}

			@Override
			public Object[] getChildren(Object parentElement) {
				
				if (parentElement instanceof Segment seg) {
					if (seg.getIncluded().getinnerLists().size() == 0) return new Object[0];
					
					LinkedHashMap<Integer, List> map = new LinkedHashMap<Integer, List>();
					for (int i = 0 ; i < seg.getIncluded().getinnerLists().size() ; i++) {
						if (seg.getIncluded().getinnerLists().get(i).size() > 0) {
							map.put(i+1, seg.getIncluded().getinnerLists().get(i));
						}
					}
					return map.entrySet().toArray();
				} else if (parentElement instanceof ArrayList list) {
					return list.toArray();
				} else if (parentElement instanceof Entry e && e.getValue() instanceof List elist) {
					return elist.toArray();
				}
				return new Object[0];
			}

			@Override
			public Object getParent(Object element) {
				
				return null;
			}

			@Override
			public boolean hasChildren(Object element) {
				
				if (element instanceof Segment seg) {
					return seg.getIncluded().getinnerLists().size() > 0;
				} else if (element instanceof List list) {
					return list.size() > 0;
				} else if (element instanceof Entry e && e.getValue() instanceof List elist) {
					return elist.size() > 0;
				}
				return false;
			}
		});

		treeViewer.getTree().addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {

				//				ISelection selection = viewer.getSelection();
				//				if (selection.isEmpty()) return;
				//				
				//				IStructuredSelection sselection = (IStructuredSelection) selection;
				//				Object first = sselection.getFirstElement();
				//				System.out.println("First="+first);
			}
		});

		TreeViewerColumn segmentColumn = new TreeViewerColumn(treeViewer, SWT.LEFT);
		segmentColumn.getColumn().setText("Segment");
		segmentColumn.getColumn().setWidth(300);
		segmentColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Segment seg) return ""+seg.getIndexes().length+ ": "+ seg.getResolved();
				else if (element instanceof List list) return Integer.toString(list.size());
				else if (element instanceof Entry e) return e.getKey().toString()+": ";
				return element == null ? "" : element.toString();//$NON-NLS-1$
			}
		});
		
		TreeViewerColumn freqColumn = new TreeViewerColumn(treeViewer, SWT.LEFT);
		freqColumn.getColumn().setText("F");
		freqColumn.getColumn().setWidth(300);
		freqColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Segment seg) return Integer.toString(seg.getCount());
				else if (element instanceof List list) return Integer.toString(list.size());
				else if (element instanceof Entry e && e.getValue() instanceof List elist) return ""+elist.size();
				return element == null ? "" : element.toString();//$NON-NLS-1$
			}
		});
		
		// Register the context menu
		TXMEditor.initContextMenu(this.treeViewer.getTree(), this.getSite(), this.treeViewer);
	}

	@Override
	public void updateResultFromEditor() {
		// nothing to do
	}

	public void fillResultArea() throws CqiClientException {
		try {
			segments.resolveStrings();
			treeViewer.setInput(segments);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void updateEditorFromResult(boolean update) {
		try {
			fillResultArea();
		}
		catch (CqiClientException e) {
			e.printStackTrace();
		}
	}
}
