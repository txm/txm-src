package org.txm.segments.rcp;

import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.txm.rcp.IImageKeys;
import org.txm.rcp.preferences.TXMPreferencePage;
import org.txm.rcp.preferences.TXMPreferenceStore;
import org.txm.segments.core.SegmentsPreferences;

public class SegmentsPreferencesPage extends TXMPreferencePage{

	@Override
	public void init(IWorkbench workbench) {
		this.setPreferenceStore(new TXMPreferenceStore(SegmentsPreferences.getInstance().getPreferencesNodeQualifier()));
		this.setTitle("Segments");
		this.setImageDescriptor(IImageKeys.getImageDescriptor("icons/segments.png"));
	}

	@Override
	protected void createFieldEditors() {
		
		this.addField(new IntegerFieldEditor(SegmentsPreferences.MIN_SEGMENTS_SIZE, "Min length", this.getFieldEditorParent()));
		this.addField(new IntegerFieldEditor(SegmentsPreferences.MAX_SEGMENTS_SIZE, "Min length", this.getFieldEditorParent()));
		this.addField(new IntegerFieldEditor(SegmentsPreferences.MIN_FREQ_SEGMENTS, "Min frequency", this.getFieldEditorParent()));
		this.addField(new IntegerFieldEditor(SegmentsPreferences.MAX_N_SEGMENTS, "Max number of segments", this.getFieldEditorParent()));
		
		this.addField(new StringFieldEditor(SegmentsPreferences.PROPERTY, "Property", this.getFieldEditorParent()));
		
	}

}
