package org.txm.segments.rcp;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.txm.rcp.editors.TXMEditor;
import org.txm.rcp.handlers.BaseAbstractHandler;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.segments.core.Segments;

public class ComputeSegments extends BaseAbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		if (!this.checkCorpusEngine()) {
			return false;
		}
		
		Object selection = this.getCorporaViewSelectedObject(event);
		Segments segments = null;
		
		// New editor from corpus
		if (selection instanceof CQPCorpus) {
			segments = new Segments((CQPCorpus) selection);
		}
		// Reopen an existing result
		else if (selection instanceof Segments) {
			segments = (Segments) selection;
		}
		else {
			return super.logCanNotExecuteCommand(selection);
		}
		
		return TXMEditor.openEditor(segments, SegmentsEditor.class.getName());
	}

}
