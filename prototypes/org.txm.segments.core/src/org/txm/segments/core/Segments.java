package org.txm.segments.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.txm.core.results.Parameter;
import org.txm.core.results.TXMResult;
import org.txm.objects.Match;
import org.txm.searchengine.core.Query;
import org.txm.searchengine.cqp.AbstractCqiClient;
import org.txm.searchengine.cqp.CQPSearchEngine;
import org.txm.searchengine.cqp.clientExceptions.UnexpectedAnswerException;
import org.txm.searchengine.cqp.corpus.CQPCorpus;
import org.txm.searchengine.cqp.corpus.WordProperty;
import org.txm.searchengine.cqp.corpus.query.CQLQuery;
import org.txm.searchengine.cqp.serverException.CqiServerError;
import org.txm.utils.ConsoleProgressBar;
import org.txm.utils.TXMProgressMonitor;
import org.txm.utils.io.IOUtils;

public class Segments extends TXMResult {

	@Parameter(key = SegmentsPreferences.MAX_N_SEGMENTS, type = Parameter.COMPUTING)
	Integer pMaxNSegments;

	@Parameter(key = SegmentsPreferences.MIN_FREQ_SEGMENTS, type = Parameter.COMPUTING)
	Integer pMinFreqSegments;

	@Parameter(key = SegmentsPreferences.MIN_SEGMENTS_SIZE, type = Parameter.COMPUTING)
	Integer pMinSegmentsize;

	@Parameter(key = SegmentsPreferences.MAX_SEGMENTS_SIZE, type = Parameter.COMPUTING)
	Integer pMaxSegmentsize;

	@Parameter(key = SegmentsPreferences.PROPERTY, type = Parameter.COMPUTING)
	WordProperty property;

	@Parameter(key = SegmentsPreferences.REGROUP, type = Parameter.COMPUTING)
	Boolean regroup;

	@Parameter(key = SegmentsPreferences.SORT_BY_SIZE, type = Parameter.COMPUTING)
	Boolean sortBySize;

	public Segments(String parametersNodePath) {
		super(parametersNodePath);
	}

	public Segments(CQPCorpus corpus) {
		super(corpus);
	}

	public Segments(String parametersNodePath, CQPCorpus corpus) {
		super(parametersNodePath, corpus);
	}

	public CQPCorpus getParent() {
		return (CQPCorpus) super.getParent();
	}

	public WordProperty getProperty() {
		return property;
	}

	@Override
	public boolean saveParameters() throws Exception {

		if (property != null) {
			this.saveParameter(SegmentsPreferences.PROPERTY, WordProperty.propertiesToString(Arrays.asList(property)));
		}
		return true;
	}

	@Override
	public boolean loadParameters() throws Exception {

		String p = this.getStringParameterValue(SegmentsPreferences.PROPERTY);
		if (p == null || p.length() == 0) p = "word"; //$NON-NLS-1$

		this.property = WordProperty.stringToProperties(getParent(), p).get(0);

		return true;
	}

	@Override
	public String getName() {

		if (segments != null) {
			return ""+segments.size()+" ("+pMinSegmentsize+"-"+pMaxSegmentsize+") "+property.getName()+" segments";
		}
		return "("+pMinSegmentsize+"-"+pMaxSegmentsize+") "+property+" segments";
	}

	@Override
	public String getSimpleName() {

		return getName();
	}

	@Override
	public String getDetails() {

		return getName();
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean canCompute() throws Exception {

		return pMaxNSegments > 1 && (pMinSegmentsize) >= 2 && (pMaxSegmentsize) >= 2 && (pMinSegmentsize <= pMaxSegmentsize);
	}
	LinkedHashMap<Segment, Integer> segments;
	@Override
	protected boolean _compute(TXMProgressMonitor monitor) throws Exception {

		segments = new LinkedHashMap<Segment, Integer>();
		list = null;

		AbstractCqiClient cqi = CQPSearchEngine.getCqiClient();

		List<? extends Match> matches = this.getParent().getMatches();

		int todo = matches.size() * (pMaxSegmentsize - pMinSegmentsize);

		ConsoleProgressBar cpb = new ConsoleProgressBar(todo);
		for (Match m : matches) { /// for all matches

			int[] matchPositions = new int[m.size()];
			for (int i = 0 ; i < m.size() ; i++) {
				matchPositions[i] = m.getStart() + i;
			}

			int[] matchIndexes = cqi.cpos2Id(property.getQualifiedName(), matchPositions);
			//			System.out.println("M="+m+" positions="+Arrays.toString(matchPositions)+" indexes="+Arrays.toString(matchIndexes));
			for (int s = pMinSegmentsize ; s <= pMaxSegmentsize; s++) { // for all segment sizes

				cpb.tick();
				//				System.out.println("s="+s);
				for (int startP = 0 ; startP + s < m.size() ; startP++) { // for all segments in the match

					int[] rawSegment = new int[s];
					//					System.out.println("m="+matchIndexes.length+" srcpos="+ startP +" raw="+ rawSegment.length+" from 0 to "+s);
					System.arraycopy(matchIndexes, startP, rawSegment, 0, s);
					Segment segment = new Segment(rawSegment);

					if (!segments.containsKey(segment)) {
						segments.put(segment, 0);
					}
					segments.put(segment, segments.get(segment) + 1);
				}
			}
		}

		cpb.done();

		HashSet<Segment> toRemove = new HashSet<>();
		for (Segment s : segments.keySet()) {
			if (segments.get(s) < pMinFreqSegments) {
				toRemove.add(s);
			}
		}
		for (Segment s : toRemove) {
			segments.remove(s);
		}
		System.out.println("N removed: "+toRemove.size());


		// resolve strings of segments
		resolveStrings();

		if (regroup) {
			System.out.println("Building Tree of ${segments.size()} segments...");
			cpb = new ConsoleProgressBar(segments.size());

			toRemove.clear();
			for (Segment s : segments.keySet()) {
				cpb.tick();
				for (Segment s2 : segments.keySet()) {
					if (s2.canInclude(s)) {
						s2.addSegment(s);
						toRemove.add(s);
					}
				}
			}
			cpb.done();
			for (Segment s : toRemove) {
				segments.remove(s);
			}
			System.out.println("N moved down: "+toRemove.size());
		}

		// sort segments
		ArrayList<Segment> list = new ArrayList<>();
		list.addAll(segments.keySet());
		if (sortBySize) {
			Collections.sort(list, new Comparator<Segment>() {

				@Override
				public int compare(Segment o1, Segment o2) {
					return o2.count - o1.count;
				}
			});
		} else {
			Collections.sort(list, new Comparator<Segment>() {

				@Override
				public int compare(Segment o1, Segment o2) {
					return o1.resolved.compareTo(o2.resolved);
				}
			});
		}
		// reindex segments
		segments.clear();
		for (Segment segment : list) {
			segments.put(segment, segment.count);
		}

		return true;
	}

	SegmentList list = new SegmentList();
	public SegmentList getSegmentList() {

		return list;
	}

	public void printSegments(int minsize) throws UnexpectedAnswerException, IOException, CqiServerError {

		AbstractCqiClient cqi = CQPSearchEngine.getCqiClient();

		ArrayList<Segment> sorted = new ArrayList<Segment>(segments.keySet());
		Collections.sort(sorted, new Comparator<Segment>( ) {
			public int compare(Segment l1, Segment l2) {
				//				return segments.get(l2) - segments.get(l1);
				return segments.get(l2).toString().compareTo(segments.get(l1).toString()); 
			}
		});

		for (Segment segment : sorted) {

			if (segments.get(segment) < minsize) continue;
			if (segment.resolved == null) {
				segment.resolved = StringUtils.join(cqi.id2Str(property.getQualifiedName(), segment.indexes), " ");
			}
			System.out.println(segment.resolved + "\t" + segments.get(segment));
		}
	}

	public void printSegmentLists(int minsize) throws UnexpectedAnswerException, IOException, CqiServerError {

		AbstractCqiClient cqi = CQPSearchEngine.getCqiClient();

		//		ArrayList<Segment> sorted = new ArrayList<Segment>(segments.keySet());
		//		Collections.sort(sorted, new Comparator<Segment>( ) {
		//			public int compare(Segment l1, Segment l2) {
		////				return segments.get(l2) - segments.get(l1);
		//				return segments.get(l2).toString().compareTo(segments.get(l1).toString()); 
		//			}
		//		});

		for (ArrayList<Segment> inner : list.innerLists) {
			for (Segment segment : inner) {
				if (segment.resolved == null) {
					segment.resolved = StringUtils.join(cqi.id2Str(property.getQualifiedName(), segment.indexes), " ");
				}
			}
		}

		System.out.println(list);
	}

	@Override
	protected boolean _toTxt(File outfile, String encoding, String colseparator, String txtseparator) throws Exception {
		return IOUtils.write(outfile, StringUtils.join(segments,"\n"));
	}

	@Override
	public String getResultType() {
		return "Segments";
	}

	public LinkedHashMap<Segment, Integer> getSegments() {
		return segments;
	}

	public void resolveStrings() throws UnexpectedAnswerException, IOException, CqiServerError {
		if (segments == null) return;

		AbstractCqiClient cqi = CQPSearchEngine.getCqiClient();

		for (Segment segment : segments.keySet()) {
			segment.resolved = StringUtils.join(cqi.id2Str(property.getQualifiedName(), segment.indexes), " ");
			segment.count = segments.get(segment);
			segment.segments = this;
		}
	}

	public String segmentToCQL(Segment segment) {
		StringBuffer b = new StringBuffer();
		try {
			String pname = segment.getSegments().getProperty().getName();
			if ("word".equals(pname)) pname = null;

			String[] strs = CQPSearchEngine.getCqiClient().id2Str(property.getQualifiedName(), segment.getIndexes());
			for (String s : strs) {
				if (b.length() > 0) b.append(" ");
				if (pname == null) {
					b.append("\""+s.replaceAll("\"", "\\\"")+"\"");
				} else {
					b.append("["+pname+"=\""+s.replaceAll("\"", "\\\"")+"\"]");
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b.toString();
	}

	public static Query createQuery(List list2) {

		StringBuffer b = new StringBuffer();
		for (Object e : list2) {
			if (e instanceof Segment segment) {

				if (list2.size() > 1) {
					if (b.length() > 0) b.append("|");
					b.append("("+segment.getSegments().segmentToCQL(segment)+")");
				} else {
					b.append(segment.getSegments().segmentToCQL(segment));
				}
			} else if (e instanceof Entry entry && entry.getValue() instanceof List sublist) {

				for (Object e2 : sublist) {
					if (e2 instanceof Segment segment) {

						if (sublist.size() > 1) {
							if (b.length() > 0) b.append("|");
							b.append("("+segment.getSegments().segmentToCQL(segment)+")");
						} else {
							b.append(segment.getSegments().segmentToCQL(segment));
						}
					}
				}
			}
		}
		//		System.out.println("Create query from: "+list2);
		return new CQLQuery(b.toString());
	}

	public static List<Query> createQueries(List list2) {

		ArrayList<Query> queries = new ArrayList<>();
		for (Object e : list2) {
			if (e instanceof Segment segment) {
				queries.add(new CQLQuery(segment.getSegments().segmentToCQL(segment)));
			} else if (e instanceof Entry entry && entry.getValue() instanceof List sublist) {

				for (Object e2 : sublist) {
					if (e2 instanceof Segment segment) {
						queries.add(new CQLQuery(segment.getSegments().segmentToCQL(segment)));
					}
				}
			}
		}
		return queries;
	}
}
/**
// STANDARD DECLARATIONS
package org.txm.macro

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcp.swt.widget.parameters.*
import org.txm.segments.core.*
import org.txm.searchengine.cqp.corpus.CQPCorpus
// BEGINNING OF PARAMETERS

if (!(corpusViewSelection instanceof CQPCorpus)) {
	println "not a corpus: $corpusViewSelection"
	return
}

@Field @Option(name="min_size", usage="an example integer", widget="Integer", required=false, def="5")
def min_size

@Field @Option(name="max_size", usage="an example integer", widget="Integer", required=false, def="10")
def max_size

@Field @Option(name="property", usage="an example integer", widget="String", required=false, def="word")
def property

if (!ParametersDialog.open(this)) return

def segments = new Segments(corpusViewSelection)
segments.pMinSegmentsize = min_size
segments.pMaxSegmentsize = max_size
segments.property = corpusViewSelection.getProperty(property)

segments.compute()

segments.printSegments(min_size)
 **/
