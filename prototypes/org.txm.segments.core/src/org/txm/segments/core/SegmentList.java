package org.txm.segments.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class SegmentList {

	ArrayList<ArrayList<Segment>> innerLists = new ArrayList<ArrayList<Segment>>(); // kindof a hashmap using the segment size to store&retieve quickly the segments

	/**
	 * 
	 * @param s the segment to try to include
	 * @return the parent segment or NULL if the segment was not included in another segment
	 */
	public boolean includeSegment(Segment s) {

		boolean included = false;
		
		for (int searchStart = s.getIndexes().length; searchStart < innerLists.size() ; searchStart++) { // try to include only in segment longer that s
			
			if (searchStart >= innerLists.size()) continue;
			ArrayList<Segment> innerList = innerLists.get(searchStart);
			if (innerList == null) continue;

			for (int i = 0 ; i < innerList.size() ; i++) {
				Segment t = innerList.get(i);
				if (t.equals(s)) {
					included = true;
				} else if (t.canInclude(s)) {
					Segment o = t.addSegment(s); // include or add
					if (o != null ) {
						included = true;
					}
				}
			}
		}

		return included;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0 ; i < innerLists.size() ; i++) {
			if (i > 0) buffer.append("\n");
			buffer.append(Integer.toString(i+1) + innerLists.get(i));
		}
		return buffer.toString();
	}

	public Segment findSegment(int[] indexes) {

		for (int searchStart = indexes.length - 1; searchStart < innerLists.size() ; searchStart++) {

			if (searchStart >= innerLists.size()) continue;
			ArrayList<Segment> innerList = innerLists.get(searchStart);
			if (innerList == null) continue;

			for (Segment s : innerList) {
				if (Arrays.equals(s.indexes, indexes)) {
					return s;
				} else if (s.canInclude(indexes)) { // maybe in the included segments ??
					Segment f = s.findSegment(indexes);
					if (f != null) return f;
				}
			}
		}

		return null;
	}

	/**
	 * Add a segment and maintain list order
	 * @param s the segment to add
	 */
	public void add(Segment s) {

		if (s == null) return;

		while (innerLists.size() < s.getIndexes().length) {
			innerLists.add(new ArrayList<Segment>());
		}

		ArrayList<Segment> innerList = innerLists.get(s.getIndexes().length - 1); // should not be null

		for (int iSegment = 0 ; iSegment < innerList.size() ; iSegment++) {
			Segment o = innerList.get(iSegment);
			if (o.getIndexes().length > s.getIndexes().length) { // the next segment is longer, the segment must be inserted now
				innerList.add(iSegment, s);
				return;
			} else if (o.getIndexes().length < s.getIndexes().length) { // the next segment is shorter, the segment must be inserted later
				continue;
			} else if (o.equals(s)) { // insert after
				return; // already inserted
			} else { // check the indexes, with same length
				for (int i = 0 ; i < o.getIndexes().length ; i++) {
					if (o.getIndexes()[i] < s.getIndexes()[i]) { // insert after
						break; // no need to test next indexes
					} else if (o.getIndexes()[i] > s.getIndexes()[i]) { // insert now
						innerList.add(iSegment, s);
						return;
					} else { // check next index

					}
				}
			}
		}

		innerList.add(s);
	}

	/**
	 * Add segments and maintain list order. Only add the non-null Segment
	 * @param collection the segments to add
	 */
	public void addAll(Collection<Segment> collection) {

		if (collection == null) return;

		for (Segment s : collection) {
			add(s);
		}
	}
	
	/**
	 * Resolve all inclusions, delete segments included in other segments
	 */
	public void bubble() {
		
		for (int i = 0 ; i < innerLists.size() ; i++) {
			
			ArrayList<Segment> innerList = innerLists.get(i);
			for (int j = 0 ; j < innerList.size() ; j++) {
				
				Segment s = innerList.get(j);
				if (includeSegment(s)) {
					innerList.remove(j--);
				}
			}
		}
		
		for (int i = 0 ; i < innerLists.size() ; i++) {
			
			ArrayList<Segment> innerList = innerLists.get(i);
			for (int j = 0 ; j < innerList.size() ; j++) {
				
				Segment s = innerList.get(j);
				s.included.bubble();
			}
		}
	}

	/**
	 * 
	 * @param s the segment to add or include if possible
	 * @return the parent Segment if included or NULL if just added to the list
	 */
	public Segment addOrInclude(Segment s) {

		// try to include the segment first
		if (includeSegment(s)) {
			return s;
		} else { // if not possible just add the segment at the right position
			add(s);
			return s;
		}
	}

	public static void main(String[] args) {

		ArrayList<Segment> list = new ArrayList<Segment>(Arrays.asList(
				new Segment(new int[] {1,2,3,4,5,6}),
				new Segment(new int[] {1,2,3,4,5}),
				new Segment(new int[] {2,3,4,5}),
				new Segment(new int[] {3,4,6,5}),
				new Segment(new int[] {1, 3,4,6,5}),
				new Segment(new int[] {1,4,6,5})
				));

		Collections.shuffle(list);

		SegmentList segmentsList = new SegmentList();
		segmentsList.addAll(list);

		System.out.println("initial list");
		System.out.println(segmentsList);

		Segment s5avant = new Segment(new int[] {1,3,6,5});
		Segment s5include = new Segment(new int[] {4,6,5});
		Segment s5apres = new Segment(new int[] {1,7,6,5});

		System.out.println("addOrinclude: "+s5avant);
		segmentsList.addOrInclude(s5avant);
		System.out.println(segmentsList);
		System.out.println("addOrinclude: "+s5include);
		segmentsList.add(s5include);
		System.out.println(segmentsList);
		System.out.println("addOrinclude: "+s5apres);
		segmentsList.addOrInclude(s5apres);
		System.out.println(segmentsList);
		
		System.out.println("bubble");
		segmentsList.bubble();
		System.out.println(segmentsList);
		
		//
		//		Segment sautre = new Segment(new int[] {1,7,6,5,9,9});
		//		System.out.println("Find: "+segmentsList.findSegment(s5include.getIndexes()));
		//		System.out.println("Find: "+segmentsList.findSegment(sautre.getIndexes()));
		//
		//		segmentsList.addOrInclude(sautre);
		//		System.out.println("Find: "+segmentsList.findSegment(sautre.getIndexes()));

	}

	int size = 0; 
	public int size() {
		
		return size;
	}

	public ArrayList<ArrayList<Segment>> getinnerLists() {
		return innerLists;
	}
}
