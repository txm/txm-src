package org.txm.segments.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

public class Segment implements Comparable<Object> {
	
	public static int MAX_SIZE = 50;
	
	Segments segments;
	int count = 0;
	String resolved;
	
	int[] indexes;
	int hashcode = -1;
	SegmentList included = new SegmentList();

	public Segment(int[] indexes) {
		this.indexes = indexes;
	}
	
	@Override
	public int compareTo(Object o) {
		if (o == null) return -1;
		if (o instanceof Segment other) {
			
			if (this.indexes.length == other.indexes.length) {
				for (int i = 0 ; i < this.indexes.length ; i++) {
					if (this.indexes[i] == other.indexes[i]) {
						continue;
					} else {
						return this.indexes[i] - other.indexes[i];
					}
				}
				return 0;
			} else {
				return this.indexes.length - other.indexes.length;
			}
		}
		return -1;
	}
	
	public int plusOne() {
		count++;
		return count;
	}
	
	public Segment setResolved(String s) {
		resolved = s;
		return this;
	}
	
	public boolean canInclude(Segment other) {
		
		return canInclude(other.indexes);
	}
	
	public boolean canInclude(int[] indexes) {
		if (this.indexes.length <= indexes.length) return false;
		
		for (int i = 0 ; i + indexes.length <= this.indexes.length; i++) {
			boolean ok = true;
			for (int j = 0 ; j < indexes.length ; j++) {
				if (this.indexes[i+j] != indexes[j]) {
					ok = false;
					break;
				}
			}
			
			if (ok) return ok;
		}
		
		return false;
	}
	
	
	@Override
	public int hashCode() {
		
		if (hashcode > 0) return hashcode;
		
		hashcode = 0;
		for (int i : indexes) hashcode += i;
		return hashcode;
	}
	
	@Override
	public boolean equals(Object other) {
		
		if (other == null) return false;
		if (other instanceof Segment s) {
			if (this.indexes.length != s.indexes.length) {
				return false;
			} else {
				for (int i = 0 ; i < this.indexes.length ; i++) {
					if (this.indexes[i] != s.indexes[i]) {
						return false;
					}
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	public String toString() {
		if (resolved != null) {
			return resolved+" "+Arrays.toString(indexes)+"("+included+")";
		} else {
			return Arrays.toString(indexes)+"("+included+")";
		}
	}
	
	public Segment addSegment(Segment s) {
		if (included.includeSegment(s)) {
			return s; // has been included in a child
		} else {
			included.add(s);
			return this;
		}
	}

	public int[] getIndexes() {
		return indexes;
	}

	public Segment findSegment(int[] indexes) {
		return included.findSegment(indexes);
	}
	
	public static void main(String[] args) {
		
		Segment s1 = new Segment(new int[] {1,2,3,4,5,6});
		
		Segment s1bis = new Segment(new int[] {1,2,3,4,5,6});
		
		Segment s2 = new Segment(new int[] {2,3,4,5});
		
		Segment s3 = new Segment(new int[] {3,4,6,5});
		
		System.out.println("S2 IN S1 "+s1.canInclude(s2));
		System.out.println("S3 IN S1 "+s1.canInclude(s3));
		
		HashSet<Segment> segments = new HashSet<Segment>();
		segments.add(s1);
		segments.add(s1bis);
		segments.add(s2);
		segments.add(s3);
		
		ArrayList<Segment> segmentsList = new ArrayList<Segment>();
		segmentsList.add(s1);
		segmentsList.add(s1bis);
		segmentsList.add(s2);
		segmentsList.add(s3);
		
		Collections.sort(segmentsList);
		
		System.out.println(segmentsList);
		
	}

	public String getResolved() {
		return resolved;
	}

	public int getCount() {
		return count;
	}

	public SegmentList getIncluded() {
		return included;
	}

	public Segments getSegments() {
		return segments;
	}
}
