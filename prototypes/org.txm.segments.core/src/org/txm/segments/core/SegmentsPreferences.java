package org.txm.segments.core;

import org.osgi.service.prefs.Preferences;
import org.txm.core.preferences.TXMPreferences;

public class SegmentsPreferences extends TXMPreferences {
	
	public static final String MAX_SEGMENTS_SIZE = "max_segments_size";
	public static final String MIN_SEGMENTS_SIZE = "min_segments_size";
	public static final String MIN_FREQ_SEGMENTS = "min_freq_segments";
	public static final String MAX_N_SEGMENTS = "max_n_segments";
	public static final String PROPERTY = "segments_property";
	public static final String REGROUP = "regroup_segments";
	public static final String SORT_BY_SIZE = "sort_segments_by_size";

	@Override
	public void initializeDefaultPreferences() {
		super.initializeDefaultPreferences();
		
		Preferences preferences = this.getDefaultPreferencesNode();
		preferences.putInt(MIN_SEGMENTS_SIZE, 5);
		preferences.putInt(MAX_SEGMENTS_SIZE, 7);
		preferences.putInt(MIN_FREQ_SEGMENTS, 5);
		preferences.putInt(MAX_N_SEGMENTS, 1000);
		preferences.putBoolean(REGROUP, true);
		preferences.putBoolean(SORT_BY_SIZE, true);
		preferences.put(PROPERTY, "word");
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static TXMPreferences getInstance() {
		if (!TXMPreferences.instances.containsKey(SegmentsPreferences.class)) {
			new SegmentsPreferences();
		}
		return TXMPreferences.instances.get(SegmentsPreferences.class);
	}
}
