/**
 * 
 */
package org.txm.core.tests.manual;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.txm.Toolbox;
import org.txm.searchengine.cqp.corpus.CQPLexicon;
import org.txm.searchengine.cqp.corpus.CorpusManager;
import org.txm.searchengine.cqp.corpus.MainCorpus;
import org.txm.searchengine.cqp.corpus.StructuralUnit;
import org.txm.searchengine.cqp.corpus.StructuralUnitProperty;
import org.txm.searchengine.cqp.corpus.WordProperty;

/**
 * 
 * Toolbox standalone tests.
 * @author sjacquot
 *
 */
public class ToolBoxTesterRCPApplication implements IApplication {

	/**
	 * 
	 */
	public ToolBoxTesterRCPApplication() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.
	 * IApplicationContext)
	 */
	@Override
	public Object start(IApplicationContext context) throws Exception {
//		System.out.println("Hello RCP World!");
//		final Map<?, ?> args = context.getArguments();
//		final String[] appArgs = (String[]) args.get("application.args");
//		for(final String arg : appArgs) {
//			System.out.println(arg);
//		}

//		System.err.println("ToolBoxTesterApplication.start() runtime node qualifier = " + RPreferences.getNodeQualifier());
//		System.err.println("ToolBoxTesterApplication.start(): " + RPreferences.getString(RPreferences.PATH_TO_EXECUTABLE));

		
		
		System.out.println("ToolBoxTesterApplication.start(): Java version = " + System.getProperty("sun.arch.data.model"));
		
		try {
			Toolbox.initialize();
			
			
			//FIXME: tests
			MainCorpus corpus = CorpusManager.getCorpusManager().getCorpus("BROWN");
			try {
				corpus.getStructuralUnit("text");
			}
			catch(Exception testE) {
				System.err.println("Can't continue, the BROWN corpus is missing");
				System.err.println("You can download it at https://gitlab.huma-num.fr/txm/textometrie/files/corpora/brown/");
				return -1;
			}
			StructuralUnit textUnit = corpus.getStructuralUnit("text");
			StructuralUnitProperty text_type = textUnit.getProperty("type");
			StructuralUnitProperty text_id = textUnit.getProperty("id");
			StructuralUnit sUnit = corpus.getStructuralUnit("s");
			WordProperty word = corpus.getProperty("word");
			WordProperty enpos = corpus.getProperty("enpos");
			WordProperty enlemma = corpus.getProperty("enlemma");

			// CQP Lexicon
			System.out.println("**************************************************");
			System.out.println("** Lexicon tests");
			System.out.println("**************************************************");
			CQPLexicon lex1 = new CQPLexicon(corpus);
			lex1.setProperty(word);
			lex1._compute();
			System.out.println("Details: " + lex1.getDetails());
			
			//FIXME: end of tests
			
		}
		catch(Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return IApplication.EXIT_OK;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

}
