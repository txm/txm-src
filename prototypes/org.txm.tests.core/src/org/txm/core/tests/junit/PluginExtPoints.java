package org.txm.core.tests.junit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.txm.utils.io.IOUtils;

public class PluginExtPoints extends TXMPluginTest {
	
	HashMap<String, HashMap<String, String>> ext_points = new HashMap<>();
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
		
		testDeclaredExtPoints(projectDirectory);
		testImplementedextPoints(projectDirectory);
	}
	
	
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File pluginXML = new File(projectDirectory, "plugin.xml");
		if (!pluginXML.exists()) {
			error("missing file " + pluginXML);
		}
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
		
		testDeclaredExtPoints(projectDirectory);
		testImplementedextPoints(projectDirectory);
	}
	
	private void testDeclaredExtPoints(File projectDirectory) throws IOException {
		File schemaDirectory = new File(projectDirectory, "schema");
		if (schemaDirectory.exists()) {
			for (File schemaXML : schemaDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)) {
				if (schemaXML.isDirectory()) continue;
				ArrayList<String> ids = IOUtils.findWithGroup(schemaXML, "<meta.schema plugin=\"[^\"]+\" id=\"([^\"]+)\" name=\"[^\"]+\"/>", true);
				ArrayList<String> classes = IOUtils.findWithGroup(schemaXML, "<meta.attribute kind=\"java\" basedOn=\"([^:]+):\"/>", true);
				if (ids.size() != classes.size()) {
					continue; // not a Java based extension
				}
				for (int i = 0; i < ids.size(); i++) {
					String id = ids.get(i);
					String clazz = classes.get(i);
					
					if (!ext_points.containsKey(id)) {
						error("ext file id\t" + id + " not found in plugin XML files. schema file " + schemaXML.getAbsolutePath());
					}
					
					File classFile = new File(getJavaSrcDirectory(projectDirectory), clazz.replace(".", "/") + ".java");
					if (!classFile.exists()) {
						error("Java class\t" + classFile + "\tnot found for ext point\t" + id);
					}
				}
			}
		}
	}
	
	private void testImplementedextPoints(File projectDirectory) {
		File pluginXML = new File(projectDirectory, "plugin.xml");
		if (pluginXML.exists()) {
			// find out implemented ext point declared in ext_points
		}
	}
	
	@Override
	public void gatherData() {
		try {
			for (File projectDirectory : projects) {
				File pluginXML = new File(projectDirectory, "plugin.xml");
				if (pluginXML.exists()) {
					ArrayList<String> ids = IOUtils.findWithGroup(pluginXML, "<extension-point id=\"([^\"]+)\"", true);
					ArrayList<String> files = IOUtils.findWithGroup(pluginXML, "schema=\"([^\"]+)\\.exsd\"", true);
					for (int i = 0; i < ids.size(); i++) {
						String id = ids.get(i);
						String file = files.get(i);
						if (ext_points.containsKey(id)) {
							System.out.println("WARNING: duplicated extension point: " + id + " in " + pluginXML.getAbsolutePath());
						}
						ext_points.put(id, new HashMap<String, String>());
						ext_points.get(id).put("file", file);
					}
				}
				
				File schemaDirectory = new File(projectDirectory, "schema");
				if (schemaDirectory.exists()) {
					for (File schemaXML : schemaDirectory.listFiles(IOUtils.HIDDENFILE_FILTER)) {
						if (!schemaXML.isFile()) continue;
						if (!schemaXML.getName().endsWith(".exsd")) continue;
						ArrayList<String> ids = IOUtils.findWithGroup(schemaXML, "<meta.schema plugin=\"[^\"]+\" id=\"([^\"]+)\" name=\"[^\"]+\"/>", true);
						ArrayList<String> classes = IOUtils.findWithGroup(schemaXML, "<meta.attribute kind=\"java\" basedOn=\"([^:]+):\"/>", true);
						
						if (ids.size() != classes.size()) {
							continue; // not a Java based schema
						}
						
						for (int i = 0; i < ids.size(); i++) {
							String id = ids.get(i);
							String clazz = classes.get(i);
							
							if (ext_points.containsKey(id)) {
								ext_points.get(id).put("class", clazz);
							}
						}
					}
				}
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
}
