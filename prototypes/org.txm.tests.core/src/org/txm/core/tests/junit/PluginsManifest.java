package org.txm.core.tests.junit;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.txm.utils.io.IOUtils;

public class PluginsManifest extends TXMPluginTest {
	
	boolean massFix = false;
	
	HashMap<String, HashMap<String, String>> ext_points = new HashMap<>();
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		testDependenciesExport(projectDirectory);
	}
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		testDependenciesExport(projectDirectory);
	}
	
	@Override
	public void FeaturePluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		// testDependenciesExport(projectDirectory);
	}
	
	private void testDependenciesExport(File projectDirectory) throws IOException {
		File manifestFile = new File(projectDirectory, "META-INF/MANIFEST.MF");
		System.out.println("test deps of "+manifestFile);
		if (!manifestFile.exists()) {
			error("missing file " + manifestFile);
			return;
		}
		
		File manifestFixFile = new File(projectDirectory, "META-INF/MANIFEST-FIX.MF");
		
		InputStream inputData = manifestFile.toURI().toURL().openStream();
		Manifest manifest = new Manifest(inputData);
		Attributes attributes = manifest.getMainAttributes();
		for (Object k : attributes.keySet()) {
			if (k.toString().equals("Require-Bundle")) {
				String depsString = attributes.get(k).toString();
				StringBuilder fixed = new StringBuilder("");
				String[] deps = depsString.split(",");
				for (String dep : deps) {
					fixed.append("," + dep);
					if (!dep.contains("visibility:=reexport")) {
						error("not visibility:=reexport: " + dep);
						fixed.append(";visibility:=reexport");
					}
				}
				// System.out.println("FIX="+fixed);
				attributes.put(k, fixed.substring(1));
			}
			else if (k.toString().equals("Bundle-SymbolicName")) {
				String id = attributes.get(k).toString();
				if (id.indexOf(";") > 0) {
					id = id.substring(0, id.indexOf(";"));
				}
				if (!projectDirectory.getName().equals(id)) {
					error("Project directory is not id. id=" + id + " dir=" + projectDirectory);
				}
			}
		}
		if (massFix) {
			manifest.write(IOUtils.getOutputStream(manifestFile));
		}
		inputData.close();
	}
	
	@Override
	public void gatherData() {
		
	}
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		new PluginsManifest().test();
	}
	
}
