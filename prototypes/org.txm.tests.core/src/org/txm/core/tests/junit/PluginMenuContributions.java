package org.txm.core.tests.junit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.txm.utils.io.IOUtils;

/**
 * check that all menu contributions point to an existing menu
 * 
 * @author mdecorde
 *
 */
public class PluginMenuContributions extends TXMPluginTest {
	
	HashSet<String> allMenus = new HashSet<>();
	
	HashSet<String> allSeparators = new HashSet<>();
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
		
		testDeclaredExtPoints(projectDirectory);
	}
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File pluginXML = new File(projectDirectory, "plugin.xml");
		if (!pluginXML.exists()) {
			error("missing file " + pluginXML);
		}
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
		
		testDeclaredExtPoints(projectDirectory);
	}
	
	private void testDeclaredExtPoints(File projectDirectory) throws IOException {
		File pluginXML = new File(projectDirectory, "plugin.xml");
		if (pluginXML.exists()) {
			ArrayList<String> locations = IOUtils.findWithGroup(pluginXML, "<menuContribution locationURI=\"([^\"]+)\"", true);
			// System.out.println("Test locations: "+locations);
			for (String location : locations) {
				String type = "";
				String target = "";
				if (location.startsWith("toolbar:")) {
					type = "toolbar";
					target = location.substring(8);
				}
				else if (location.startsWith("menu:")) {
					type = "menu";
					target = location.substring(5);
				}
				else if (location.startsWith("popup:")) {
					type = "popup";
					target = location.substring(6);
				}
				else {
					error("wrong contribution type (" + type + "): " + location);
				}
				
				String position = "";
				if (target.contains("?")) {
					position = target.substring(target.indexOf("?") + 1);
					if (position.startsWith("after=")) {
						position = position.substring(6);
					}
					else if (position.startsWith("before=")) {
						position = position.substring(7);
					}
					
					target = target.substring(0, target.indexOf("?"));
				}
				// System.out.println(type+" -> "+target+" : "+position);
				if (!allMenus.contains(target)) {
					error("wrong contribution target (" + target + "): " + location);
				}
				
				if (position.length() > 0) {
					if (!allSeparators.contains(position)) {
						// System.out.println("ERROR '"+position+"' not in "+allSeparators);
						error("wrong contribution position (" + position + "): " + location);
					}
				}
			}
			
		}
	}
	
	@Override
	public void gatherData() {
		try {
			allMenus.add("org.eclipse.ui.main.menu"); // eclipse main menu
			allMenus.add("#TextEditorContext"); // text editor context
			for (File projectDirectory : projects) {
				
				File pluginXML = new File(projectDirectory, "plugin.xml");
				if (pluginXML.exists()) {
					ArrayList<String> ids = IOUtils.findWithGroup(pluginXML, "menu id=\"([^\"]+)\"", true);
					allMenus.addAll(ids);
					allSeparators.addAll(ids);
					
					ids = IOUtils.findWithGroup(pluginXML, "menu icon=\"[^\"]+\" id=\"([^\"]+)\"", true);
					allMenus.addAll(ids);
					allSeparators.addAll(ids);
					
					// allowMultiple="false"
					ArrayList<String> views = IOUtils.findWithGroup(pluginXML, "<view category=\"[^\"]+\" class=\"([^\"]+)\"", true);
					for (String view : views) {
						allMenus.add(view);
					}
					
					views = IOUtils.findWithGroup(pluginXML, "<view allowMultiple=\"[^\"]+\" category=\"[^\"]+\" class=\"([^\"]+)\"", true);
					for (String view : views) {
						allMenus.add(view);
					}
					
					views = IOUtils.findWithGroup(pluginXML, "<view class=\"([^\"]+)\"", true);
					for (String view : views) {
						allMenus.add(view);
					}
					
					ArrayList<String> editors = IOUtils.findWithGroup(pluginXML, "<editor class=\"([^\"]+)\"", true);
					for (String editor : editors) {
						allMenus.add(editor);
					}
					
					ArrayList<String> toolbars = IOUtils.findWithGroup(pluginXML, "<toolbar id=\"([^\"]+)\"", true);
					for (String toolbar : toolbars) {
						allMenus.add(toolbar);
					}
					
					ArrayList<String> separators = IOUtils.findWithGroup(pluginXML, "<separator name=\"([^\"]+)\"", true);
					for (String separator : separators) {
						allSeparators.add(separator);
					}
					
					ArrayList<String> commands = IOUtils.findWithGroup(pluginXML, "<command commandId=\"([^\"]+)\"", true);
					for (String command : commands) {
						allSeparators.add(command);
					}
					
				}
			}
			
			System.out.println("all menus: " + allMenus);
			
			System.out.println("all separators: " + allSeparators);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
}
