package org.txm.core.tests.junit;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;

import org.txm.utils.io.IOUtils;

public class PluginXMLIDs extends TXMPluginTest {
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
	}
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File pluginXML = new File(projectDirectory, "plugin.xml");
		if (!pluginXML.exists()) {
			error("missing file " + pluginXML);
		}
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
		
		LinkedHashSet<String> ids = new LinkedHashSet<>();
		ids.addAll(IOUtils.findWithGroup(pluginXML, "id=\"([^\"]+)\""));
		for (String id : ids) {
			if (id.startsWith(name)) {
				// System.out.println("id="+id);
				String path = id.substring(0, id.lastIndexOf(".")).replace(".", "/");
				String className = id.substring(id.lastIndexOf(".") + 1);
				if (className.matches("[A-Z].+")) {
					File classFile = new File(javaSrcDirectory, path + "/" + className + ".java");
					if (!classFile.exists()) {
						error("unbound id	" + id + "	" + classFile);
					}
				}
			}
		}
		
		testClassExistance(pluginXML, javaSrcDirectory, name, "id=\"([^\"]+)\"", "id");
		testClassExistance(pluginXML, javaSrcDirectory, name, "adaptableType=\"([^\"]+)\"", "adaptableType");
		testClassExistance(pluginXML, javaSrcDirectory, name, "class=\"([^\"]+)\"", "class");
		testClassExistance(pluginXML, javaSrcDirectory, name, "class=\"([^\"]+)\"", "commandId");
		testClassExistance(pluginXML, javaSrcDirectory, name, "class=\"([^\"]+)\"", "defaultHandler");
		testClassExistance(pluginXML, javaSrcDirectory, name, "instanceof value=\"([^\"]+)\"", "instanceof");
	}
	
	/**
	 * 
	 * @param pluginXML plugin.xml file
	 * @param javaSrcDirectory the Java source directory (contains the class to find)
	 * @param name the package to test
	 * @param pattern the pattern to match. must contains ONE group
	 * @param type the attribute to test
	 * @throws IOException
	 */
	private void testClassExistance(File pluginXML, File javaSrcDirectory, String name, String pattern, String type) throws IOException {
		LinkedHashSet<String> classes = new LinkedHashSet<>();
		classes.addAll(IOUtils.findWithGroup(pluginXML, pattern, true));
		for (String id : classes) {
			if (id.startsWith(name)) {
				// System.out.println("id="+id);
				String path = id.substring(0, id.lastIndexOf(".")).replace(".", "/");
				String className = id.substring(id.lastIndexOf(".") + 1);
				if (className.matches("[A-Z].+")) {
					File classFile = new File(javaSrcDirectory, path + "/" + className + ".java");
					if (!classFile.exists()) {
						error("unbound " + type + "	" + id + "	" + classFile);
					}
				}
			}
		}
	}
	
	@Override
	public void gatherData() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
}
