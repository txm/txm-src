package org.txm.core.tests.junit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class WorkspaceProjects extends TXMPluginTest {
	
	ArrayList<File> declaredProjects;
	
	@Override
	public void gatherData() {
		
		File projectSetXMLFile = new File(workspace, "org.txm.tests.core/projectSet.psf");
		
		if (!projectSetXMLFile.exists()) {
			error("No project set file found in " + projectSetXMLFile);
		}
		
		declaredProjects = new ArrayList<>();
		try {
			Document doc = DomUtils.load(projectSetXMLFile);
			NodeList nodes = doc.getElementsByTagName("project");
			for (int i = 0; i < nodes.getLength(); i++) {
				Element projectElement = (Element) nodes.item(i);
				String ref = projectElement.getAttribute("reference");
				String[] split = ref.split(",");
				if (split.length >= 3) {
					String name = split[2];
					declaredProjects.add(new File(workspace, name));
				}
				else {
					
				}
			}
		}
		catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			error("Internal error: " + e);
		}
	}
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws Exception {}
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws Exception {}
	
	@Override
	public void finalReport() {
		int size = declaredProjects.size();
		declaredProjects.removeAll(projects);
		if (declaredProjects.size() > 0) { // some projects are absent
			error("Some mandatory projects are missing in your current workspace: " + declaredProjects);
		}
		else {
			info("All the mandatory projects are here: " + size);
		}
	}
}
