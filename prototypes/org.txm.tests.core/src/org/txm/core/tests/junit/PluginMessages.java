package org.txm.core.tests.junit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashSet;
import java.util.Properties;

import org.txm.utils.io.IOUtils;

public class PluginMessages extends TXMPluginTest {
	
	String langs[] = { "" };
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		commonTest(projectDirectory, name, packagePath);
	}
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		commonTest(projectDirectory, name, packagePath);
		
		File pluginXML = new File(projectDirectory, "plugin.xml");
		try {
			// OSGI-INF test
			File OSGI = new File(projectDirectory, "OSGI-INF/l10n");
			for (String lang : langs) {
				File propertiesFile = new File(OSGI, "bundle" + lang + ".properties");
				if (!propertiesFile.exists()) {
					error("No " + propertiesFile.getAbsolutePath());
					return;
				}
				else {
					testKeys(propertiesFile, pluginXML, "=\"%([^\"]+)\"");
				}
			}
			
		}
		catch (IOException e) {
			error(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	public void commonTest(File projectDirectory, String name, String packagePath) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
		
		File messagesPackageDirectory = new File(javaSrcDirectory, packagePath + "/messages");
		if (!messagesPackageDirectory.exists()) {
			error("missing file " + messagesPackageDirectory.getAbsolutePath());
			return;
		}
		
		// test content
		File[] files = messagesPackageDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
		if (files == null) {
			error("No messages class file.");
			return;
		}
		int c = 0;
		
		File classFile = null;
		for (File file : files) {
			if (file.getName().endsWith("Messages.java")) classFile = file;
		}
		
		if (classFile == null) {
			error("missing Java file " + messagesPackageDirectory);
			return;
		}
		
		for (String lang : langs) {
			
			File propertiesFile = new File(messagesPackageDirectory, "messages" + lang + ".properties");
			if (!propertiesFile.exists()) {
				error("missing properties	" + propertiesFile.getAbsolutePath());
				return;
			}
			else {
				testKeys(propertiesFile, classFile, "public static String ([^;]+);");
			}
		}
	}
	
	/**
	 * Compare keys of @param propertiesFile with keys found in @param classFile with @param pattern
	 * 
	 * @param propertiesFile
	 * @param classFile
	 * @param pattern
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected void testKeys(File propertiesFile, File classFile, String pattern) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		Properties props = new Properties();
		props.load(IOUtils.getReader(propertiesFile, "UTF-8"));
		
		LinkedHashSet<String> keys = new LinkedHashSet<>();
		keys.addAll(IOUtils.findWithGroup(classFile, pattern));
		
		// find missing keys in prop file
		for (String key : keys) {
			if (props.getProperty(key) == null) {
				error("missing key	'" + key + "'	" + propertiesFile.getName());
			}
		}
		
		// find unused keys
		for (Object key : props.keySet()) {
			String s = key.toString();
			if (!keys.contains(s)) {
				error("unused key	'" + key + "'	" + propertiesFile.getName());
			}
		}
	}
	
	@Override
	public void gatherData() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
}
