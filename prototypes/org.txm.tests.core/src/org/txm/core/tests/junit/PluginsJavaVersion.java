package org.txm.core.tests.junit;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.osgi.util.NLS;
import org.txm.utils.io.IOUtils;
import org.txm.utils.xml.DomUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PluginsJavaVersion extends TXMPluginTest {
	
	private static final String CURRENT_JAVA_VERSION = "JavaSE-1.8";
	
	private static final String classPathJREPreference = "org.eclipse.jdt.launching.JRE_CONTAINER";
	
	boolean massFix = true;
	
	HashMap<String, HashMap<String, String>> ext_points = new HashMap<>();
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		RCPPluginTest(projectDirectory, name, packagePath);
	}
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		testJavaVersionInManifest(projectDirectory);
		testJavaVersionInClasspath(projectDirectory);
	}
	
	@Override
	public void LIBPluginTest(File projectDirectory, String name, String packagePath) throws Exception {
		RCPPluginTest(projectDirectory, name, packagePath);
	}
	
	@Override
	public void FeaturePluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		// testDependenciesExport(projectDirectory);
	}
	
	private void testJavaVersionInClasspath(File projectDirectory) throws IOException {
		File classpathFile = new File(projectDirectory, ".classpath");
		if (!classpathFile.exists()) {
			error("missing file " + classpathFile);
			return;
		}
		
		try {
			Document doc = DomUtils.load(classpathFile);
			NodeList elements = doc.getElementsByTagName("classpathentry");
			for (int i = 0; i < elements.getLength(); i++) {
				Element element = (Element) elements.item(i);
				String path = element.getAttribute("path");
				if (path.startsWith(classPathJREPreference) && !path.equals(classPathJREPreference)) {
					error(NLS.bind("Wrong Java version in classpath: {0} found {1}", classpathFile, path));
					element.setAttribute("path", classPathJREPreference);
				}
			}
			
			if (massFix) {
				DomUtils.save(doc, classpathFile);
			}
		}
		catch (ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void testJavaVersionInManifest(File projectDirectory) throws IOException {
		File manifestFile = new File(projectDirectory, "META-INF/MANIFEST.MF");
		
		if (!manifestFile.exists()) {
			error("missing file " + manifestFile);
			return;
		}
		
		InputStream inputData = manifestFile.toURI().toURL().openStream();
		Manifest manifest = new Manifest(inputData);
		Attributes attributes = manifest.getMainAttributes();
		for (Object k : attributes.keySet()) {
			if (k.toString().equals("Bundle-RequiredExecutionEnvironment")) {
				String versionString = attributes.get(k).toString();
				if (!(CURRENT_JAVA_VERSION.equals(versionString))) {
					error(NLS.bind("Wrong Java version in manifest: {0} found {1}", manifestFile, attributes.get(k)));
					attributes.put(k, CURRENT_JAVA_VERSION);
				}
			}
		}
		if (massFix) {
			manifest.write(IOUtils.getOutputStream(manifestFile));
		}
		inputData.close();
	}
	
	@Override
	public void gatherData() {
		
	}
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
}
