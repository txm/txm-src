package org.txm.core.tests.junit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Properties;

import org.txm.utils.io.IOUtils;

public class PluginBuildProperties extends TXMPluginTest {
	
	HashMap<String, HashMap<String, String>> ext_points = new HashMap<>();
	
	boolean massfix = false;
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File buildFile = testBuildFile(projectDirectory);
		if (buildFile == null) {
			return;
		}
		
		Properties props = getProperties(buildFile);
		
		testBinIncludes(props, buildFile);
		testQualifier(props, buildFile);
	}
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File buildFile = testBuildFile(projectDirectory);
		if (buildFile == null)
			return;
		
		Properties props = getProperties(buildFile);
		
		testBinIncludes(props, buildFile);
		testQualifier(props, buildFile);
	}
	
	@Override
	public void FeaturePluginTest(File projectDirectory, String name, String packagePath) throws IOException {
		File buildFile = testBuildFile(projectDirectory);
		if (buildFile == null)
			return;
		
		Properties props = getProperties(buildFile);
		
		testQualifier(props, buildFile);
	}
	
	public static Properties getProperties(File buildFile) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		Properties props = new Properties();
		props.load(IOUtils.getReader(buildFile));
		return props;
	}
	
	public void testQualifier(Properties props, File buildFile) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		Object o = props.get("qualifier");
		if (o == null) {
			error("QUALIFIER not set to 'svn': " + buildFile.getAbsolutePath());
		}
		else {
			String value = o.toString();
			if (!value.equals("svn")) {
				error("QUALIFIER not set to 'svn': " + buildFile.getAbsolutePath());
				if (massfix) {
					IOUtils.write(buildFile, IOUtils.getText(buildFile, "UTF-8") + "\nqualifier=svn\n");
				}
			}
		}
	}
	
	public void testBinIncludes(Properties props, File buildFile) {
		String value = (String) props.get("bin.includes");
		// System.out.println("buildFile="+buildFile+" value="+value);
		File pluginXML = new File(buildFile.getParentFile(), "plugin.xml");
		if (pluginXML.exists() && !value.contains("plugin.xml")) {
			error("plugin.xml is not exported: " + buildFile.getAbsolutePath());
		}
		if (!value.contains("META-INF/")) {
			error("META-INF is not exported: " + buildFile.getAbsolutePath());
		}
	}
	
	private File testBuildFile(File projectDirectory) throws IOException {
		File buildFile = new File(projectDirectory, "build.properties");
		
		if (!buildFile.exists()) {
			error("no build.properties: " + buildFile.getAbsolutePath());
			return null;
		}
		
		return buildFile;
	}
	
	@Override
	public void gatherData() {
		
	}
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
}
