package org.txm.core.tests.junit;

import java.io.File;

import org.txm.utils.io.IOUtils;

public class PluginPreferences extends TXMPluginTest {
	
	@Override
	public void RCPPluginTest(File projectDirectory, String name, String packagePath) {
		
	}
	
	@Override
	public void TBXPluginTest(File projectDirectory, String name, String packagePath) {
		File javaSrcDirectory = getJavaSrcDirectory(projectDirectory);
		File preferencePackageDirectory = new File(javaSrcDirectory, packagePath + "/preferences");
		if (!preferencePackageDirectory.exists()) {
			error(preferencePackageDirectory.getAbsolutePath() + " is missing.");
			return;
		}
		
		// test content
		File[] files = preferencePackageDirectory.listFiles(IOUtils.HIDDENFILE_FILTER);
		if (files == null) {
			error("No preference class file.");
			return;
		}
		int c = 0;
		for (File prefFile : files) {
			if (prefFile.getName().endsWith("Preferences.java")) c++;
		}
		if (c == 0) error("No preference class file in " + preferencePackageDirectory);
	}
	
	@Override
	public void gatherData() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void finalReport() {
		// TODO Auto-generated method stub
		
	}
}
