package org.txm.core.tests.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
		PluginPreferences.class,
		PluginMessages.class,
		PluginXMLIDs.class,
		PluginExtPoints.class,
		PluginBuildProperties.class,
		PluginMenuContributions.class,
		PluginsManifest.class,
		WorkspaceProjects.class
})
public class AllTXMPluginTests {
	
}
