package org.txm.core.tests.junit;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

/**
 * Base class to test TXM projects with some services to find out the Java src directory and stuff.
 * 
 * Use the gatherData method to gather informations before testing the coherence of projects between them
 * Use the TBXPluginTest method to test the core projects (no plugin.xml file...)
 * Use the RCPPluginTest method to test the RCP projects (ui stuff)
 * 
 * @author mdecorde
 *
 */
public abstract class TXMPluginTest {
	
	public static int LEVEL = 3; // 1 errors, 2 warnings, 3 infos messages
	
	/**
	 * error counter
	 */
	int error = 0;
	
	protected ArrayList<File> projects;
	
	protected ArrayList<String> messages = new ArrayList<>();
	
	
	public static File workspace;
	static {
		try {
			workspace = new File(System.getProperty("user.dir"), "../../bundles").getCanonicalFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // MD
	}
	// public static File workspace = new File("C:\\Tools\\Coding\\Java\\workspace_txm2"); // SJ
	
	/**
	 * fetch the org.txm projects that ends with core or rcp
	 * 
	 * @return
	 */
	protected static ArrayList<File> getProjects() {
		ArrayList<File> ret = new ArrayList<>();
		if (!workspace.exists()) {
			System.out.println("ERROR: " + workspace + " does not exists. Set it in TXMPluginTest.java.");
			return ret;
		}
		
		ret.addAll(Arrays.asList(workspace.listFiles(new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory() && pathname.getName().startsWith("org.txm");
			}
		})));
		
		Collections.sort(ret);
		return ret;
	}
	
	/**
	 * store and count error messages
	 * 
	 * @param string
	 */
	protected void error(String string) {
		if (LEVEL >= 1) messages.add("	ERROR	" + string);
		error++;
	}
	
	/**
	 * store warning messages
	 * 
	 * @param string
	 */
	protected void warning(String string) {
		if (LEVEL >= 2) messages.add("	WARNING	" + string);
	}
	
	/**
	 * store info messages
	 * 
	 * @param string
	 */
	protected void info(String string) {
		if (LEVEL >= 3) messages.add("	INFO	" + string);
	}
	
	/**
	 * All Test must starts with "super.test();" Usually you don't need to Override this method
	 */
	@Test
	public void test() {
		
		System.out.print("Testing " + this.getClass().getSimpleName() + "...");
		try {
			projects = getProjects();
			
			if (projects.size() == 0) fail("No project found in " + workspace);
			
			gatherData();
			
			int nProjectMessage = 0;
			for (File projectDirectory : projects) {
				messages.clear();
				String plugin_name = projectDirectory.getName();
				String plugin_package = plugin_name.replace(".", "/");
				
				if (projectDirectory.getName().endsWith(".feature")) {
					// plugin_package = plugin_package.substring(0, plugin_package.length()-4);
					FeaturePluginTest(projectDirectory, plugin_name, plugin_package);
				}
				else if (projectDirectory.getName().contains(".core")) {
					// plugin_package = plugin_package.substring(0, plugin_package.length()-5);
					TBXPluginTest(projectDirectory, plugin_name, plugin_package);
				}
				else if (projectDirectory.getName().endsWith(".rcp")) {
					// plugin_package = plugin_package.substring(0, plugin_package.length()-4);
					RCPPluginTest(projectDirectory, plugin_name, plugin_package);
				}
				else if (projectDirectory.getName().startsWith("org.txm.libs.")) {
					// plugin_package = plugin_package.substring(0, plugin_package.length()-4);
					LIBPluginTest(projectDirectory, plugin_name, plugin_package);
				}
				else {
					OTHERPluginTest(projectDirectory, plugin_name, plugin_package);
				}
				
				if (messages.size() > 0) {
					if (nProjectMessage == 0) {
						System.out.println();
						nProjectMessage++;
					}
					System.out.println(" P " + projectDirectory.getName());
					for (String s : messages)
						System.out.println(s);
				}
			}
			
			finalReport();
		}
		catch (Exception e) {
			e.printStackTrace();
			fail("Internal test error: " + e.getLocalizedMessage());
		}
		if (error > 0) {
			fail("Errors during test " + this.getClass().getName());
		}
		else System.out.println("OK!");
	}
	
	public void FeaturePluginTest(File projectDirectory, String plugin_name, String plugin_package) throws Exception {
		
	}
	
	/**
	 * fetch info before testing individually the projects
	 */
	public abstract void gatherData();
	
	/**
	 * final test done after all projects have been tested
	 */
	public abstract void finalReport();
	
	
	/**
	 * 
	 * @param projectDirectory
	 * @return find out the main Java src directory of the project
	 */
	protected static File getJavaSrcDirectory(File projectDirectory) {
		File javaSrcDirectory = new File(projectDirectory, "src/main/java/");
		if (!javaSrcDirectory.exists()) javaSrcDirectory = new File(projectDirectory, "src/java");
		if (!javaSrcDirectory.exists()) javaSrcDirectory = new File(projectDirectory, "src/");
		return javaSrcDirectory;
	}
	
	/**
	 * test a TBX project
	 * 
	 * @param projectDirectory
	 * @param name
	 * @param packagePath
	 * @throws Exception
	 */
	public abstract void TBXPluginTest(File projectDirectory, String name, String packagePath) throws Exception;
	
	/**
	 * test a LIB project
	 * 
	 * @param projectDirectory
	 * @param name
	 * @param packagePath
	 * @throws Exception
	 */
	public void LIBPluginTest(File projectDirectory, String name, String packagePath) throws Exception {
		
	}
	
	/**
	 * test a project that was not recognized
	 * 
	 * @param projectDirectory
	 * @param name
	 * @param packagePath
	 * @throws Exception
	 */
	public void OTHERPluginTest(File projectDirectory, String name, String packagePath) throws Exception {
		
	}
	
	/**
	 * test a RCP project
	 * 
	 * @param projectDirectory
	 * @param name
	 * @param packagePath
	 * @throws Exception
	 */
	public abstract void RCPPluginTest(File projectDirectory, String name, String packagePath) throws Exception;
	
	public static void main(String[] args) {
		
	}
}
