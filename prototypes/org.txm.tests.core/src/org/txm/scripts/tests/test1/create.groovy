package org.txm.scripts.tests.test1

import java.util.Calendar
import java.util.Locale

import org.txm.*
import org.txm.core.results.TXMResult
import org.txm.objects.*
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.properties.core.functions.*

Workspace w = Toolbox.workspace

Project p = w.getProject("NANOBROWN")
if (p != null) {
	p.delete();
}

p = new Project(w, "NANOBROWN", true)
p.setSourceDirectory("")
p.setDescription("Test1 corpus");
p.setLang("en");
p.setImportModuleName("xtz")
if (!p.compute()) {
	return false;
}

MainCorpus c = p.getCorpusBuild("NANOBROWN")
if (c == null) {
	return false;
}

Properties props = new Properties(c)
if (!props.compute()) {
	return false
}

return ["project":p, "corpus":c, "properties":props]