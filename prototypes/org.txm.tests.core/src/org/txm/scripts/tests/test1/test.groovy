package org.txm.scripts.tests.test1

import java.util.Calendar
import java.util.Locale

import org.txm.*
import org.txm.core.results.TXMResult
import org.txm.objects.*
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.utils.Diff
import org.txm.properties.core.functions.*

try {
	println outdir
} catch(Exception e) {return false;}

File previous_export = new File(outdir, "079.txt")
File export = new File(outdir, "080.txt")

if (previous_export.exists() && export.exists()) {
	return false;
}

String diff = Diff.diffFile(previous_export, export)

return diff.length() == 0;