package org.txm.scripts.tests.test1

import java.util.Calendar
import java.util.Locale

import org.txm.*
import org.txm.core.results.TXMResult
import org.txm.objects.*
import org.txm.searchengine.cqp.corpus.MainCorpus
import org.txm.properties.core.functions.*

try {
	println outdir
	println results
} catch(Exception e) {return false;}

Project p = results["project"]
MainCorpus c = results["corpus"]
Properties props = results["properties"]




File outfile = new File(outdir, "080.txt")
outfile.delete()

props.toTxt(outfile)



return outfile.exists()