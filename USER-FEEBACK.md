RETOURS UTILISATEUR
/
USER FEEBACK
=

Les formations sont toujours un moment de rencontre avec des personnes qui découvrent TXM et font des retours naïfs dont il est intéressant de tenir compte pour faciliter l'accès à TXM pour les primo-arrivants.

Il s'agit de notes prises à chaud pendant une formation. Cela peut concerner des bugs, déjà connus ou non, des questions sur tel ou tel choix fait dans TXM ou encore d'autres aspects qui peuvent concerner TXM et sa diffusion.

On note également le profil de participants pour en documenter la diversité.

4 octobre 2024 au matin, délégation régionale CNRS de Villejuif, formation initiation à TXM de 3h
==

- 13 personnes : IR géomatique, Onera - fouille pour biblio, CR histoire petites annonces, doc histoire politique, services numériques bibliothèque Créteil, ressources documentaires, Inist, veille biblio, IA économie, biologie génétique (accélération fouille biblio), état de l'art biblio, neuro-sciences (accélération fouille biblio)

Retours
===

- résulats
  - préserver les résulats entre les sessions de TXM
- documentation
  - lien vers le manuel cassé depuis le menu Documentation : https://txm.gitpages.huma-num.fr/textometrie/txm-manual/ ➡ candidat à test de non régression - JUnit
- ajouter lien vers documentation CQL et regex depuis widgets de champs CQL et depuis menu 'Documentation'
  - ajouter lien vers documentation tagset TT du corpus
- assistant de requête : ajouter l'ajout de modifieurs %c et %d
- table lexicale
  - tri par défaut des colonnes (décroissant)
- index de partition
  - tri des colonnes
- import
  - activer 'Lemmatiser' par défaut si extension TT  et modèle de langue présents
- spécificités
  - bug d'affichage en Windows 10 : le graphique s'étend à l'infini à droite - la légende n'est pas visible
  